<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class GalleryImage extends Model
{
    protected $fillable = [
        'gallery_id',
        'active',
        'file',
        'link_url',
        'start_date',
        'end_date',
        'display_order',
        'mobile_file',
    ];

    public function gallery()
    {
        return $this->belongsTo(\App\Gallery::class, 'gallery_id', 'id');
    }

    public function getFileAttribute($value)
    {
        return Storage::url('gallery_images/'.$value);
    }
}
