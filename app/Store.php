<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

class Store extends Model
{
    protected $fillable = [
        'name',
        'address',
        'address2',
        'latitude',
        'longitude',
        'phone',
        'store_manager',
        'email',
        'store_manager_image',
        'store_hours',
        'brand_id',
        'store_code',
        'tax_rate',
        'price_strategy',
        'shipt_files',
        'shipt_flag',
        'zenreach_code',
        'sendy_list_id',
        'pharmacy_hours',
        'catering_events_hours',
        'deli_hours',
        'bakery_hours',
        'produce_hours',
        'floral_hours',
        'guest_services_hours',
        'state',
        'postal_code',
        'city',
        'status',
        'stripe_key',
        'stripe_secret',
        'paypal_username',
        'paypal_password',
        'paypal_secret',
        'is_cost_plus',
        'online_shopping_phone_number',
        'rsa_sendy_list',
    ];

    /**
     *  Ads Relationship.
     */
    public function ads()
    {
        return $this->belongsToMany(Ad::class, 'ad_store')
            ->where('date_from', '<=', date('Y-m-d'))
            ->where('date_to', '>=', date('Y-m-d'))
            ->where('status', true);
    }

    /**
     * Aisle Relationship.
     */
    public function aisles()
    {
        return $this->hasMany(StoreAisle::class);
    }

    /**
     *  Brands Relationship.
     */
    public function brands()
    {
        return $this->belongsTo(\App\Brand::class, 'brand_id', 'id');
    }

    /**
     *  Employees Relationship.
     */
    public function employees()
    {
        return $this->hasMany(\App\Employee::class);
    }

    /**
     *  Huddles Relationship.
     */
    public function huddles()
    {
        return $this->hasMany(\App\Huddle::class);
    }

    /**
     * Panels relationship.
     */
    public function panels()
    {
        return $this->hasManyThrough(StoreAislePanels::class, StoreAisle::class);
    }

    /**
     *  Promos Relationship.
     */
    public function promos()
    {
        return $this->hasMany(\App\Promo::class);
    }

    /**
     *  Sales Relationship.
     */
    public function sales()
    {
        return $this
            ->hasMany(\App\SalesData::class, 'store_num', 'store_code');
    }

    /**
     *  Set Zenreach Code To Null When Empty.
     */
    public function setZenreachCodeAttribute($value)
    {
        $this->attributes['zenreach_code'] = (string) $value ?: null;
    }

    /**
     *  Set Sendy List ID to Null When Empty.
     */
    public function setSendyListIdAttribute($value)
    {
        $this->attributes['sendy_list_id'] = (string) $value ?: null;
    }

    /**
     *  Pickup Full Path For Store Managers Photos.
     */
    public function getStoreManagerImageAttribute($value)
    {
        if ($value != '') {
            return Storage::url('managers/'.$value);
        } else {
            return URL::to('/img/pending-image.jpg');
        }
    }

    /**
     *   Status Attribute.
     */
    public function getStatusAttribute($value)
    {
        $status = Lang::get('stores.closed');

        if ($value == true) {
            $status = Lang::get('stores.open');
        }

        return $status;
    }

    /**
     *  SCOPES.
     */

    /**
     *  Scope Active.
     */
    public function scopeActive()
    {
        return $this->where('status', 1)->orderBy('store_code');
    }
}
