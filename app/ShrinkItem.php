<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShrinkItem extends Model
{
    protected $fillable = [
        'name',
        'shrink_group_id',
    ];

    public function group()
    {
        return $this->belongsTo(\App\ShrinkGroup::class, 'shrink_group_id', 'id');
    }

    public function shrinks()
    {
        return $this->hasMany(\App\Shrink::class);
    }
}
