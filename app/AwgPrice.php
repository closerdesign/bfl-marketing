<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AwgPrice extends Model
{
    protected $fillable = [
        'obi',
        'item',
        'description',
        'size',
        'pack',
        'excise_tax',
        'case_cost',
        'unit_cost',
        'unit_city',
        'unit_rural',
        'bfl_vendor_id',
        'bfl_dept',
        'upc',
        'qty',
        'amount',
    ];

    /**
     *  GP City.
     */
    public function getCityGpAttribute()
    {
        if ($this->attributes['unit_city'] == 0) {
            return 0;
        }

        $margin = $this->attributes['unit_city'] - $this->attributes['unit_cost'];
        $percentage = $margin / $this->attributes['unit_city'];

        return number_format($percentage * 100, 2);
    }

    /**
     *  GP Rural.
     */
    public function getRuralGpAttribute()
    {
        if ($this->attributes['unit_rural'] == 0) {
            return 0;
        }

        $margin = $this->attributes['unit_rural'] - $this->attributes['unit_cost'];
        $percentage = $margin / $this->attributes['unit_rural'];

        return number_format($percentage * 100, 2);
    }

    /**
     *  BFL Unit Sold.
     */
    public function getBflUnitSoldAttribute()
    {
        return $this->attributes['amount'] / $this->attributes['qty'];
    }

    /**
     *  BFL GP.
     */
    public function getBflGpAttribute()
    {
        $margin = $this->getBflUnitSoldAttribute() - $this->attributes['unit_cost'];

        $gp = $margin / $this->attributes['unit_cost'];

        return $gp * 100;
    }

    /**
     *  BFL GP Color.
     */
    public function getBflGpColorAttribute()
    {
        $color = 'green';

        if ($this->getBflGpAttribute() < 0) {
            $color = 'red';
        }

        return $color;
    }
}
