<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Search extends Model
{
    protected $fillable = [
        'search_string',
        'search_results',
        'store_code',
        'cart_product_id',
        'reviewed',
    ];
}
