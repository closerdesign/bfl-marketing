<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CtwScore extends Model
{
    protected $fillable = [
        'evaluation_id',
        'topic_id',
        'score',
        'comments',
    ];

    /**
     *  Score Label.
     */
    public function getScoreLabelAttribute()
    {
        $label = [
            0 => 'Needs Improvement',
            1 => 'Near Model',
            2 => 'Model',
        ];

        return $label[$this->attributes['score']];
    }

    /**
     *  Score Class.
     */
    public function getScoreClassAttribute()
    {
        $class = [
            0 => 'danger',
            1 => 'warning',
            2 => 'success',
        ];

        return $class[$this->attributes['score']];
    }
}
