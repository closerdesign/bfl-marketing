<?php

namespace App\Console;

use App\Console\Commands\DepartmentsImport;
use App\Console\Commands\LoyaltySignups;
use App\Console\Commands\MovementUpdateForAwgPrices;
use App\Console\Commands\PricingImport;
use App\Console\Commands\PricingPickup;
use App\Console\Commands\RetalixImport;
use App\Console\Commands\RetalixUpdate;
use App\Console\Commands\RocWrap;
use App\Console\Commands\ShoppingAdPrices;
use App\Console\Commands\ShoppingDB;
use App\Console\Commands\SpinsImport;
use App\Console\Commands\TextSubscriberSpecials;
use Aws\Command;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Inspire::class,
        Commands\SendPromos::class,
        Commands\SendHuddles::class,
        Commands\SendShipt::class,
        Commands\CategoriesShipt::class,
        Commands\ZenreachImport::class,
        Commands\ShiptPricing::class,
        Commands\ItemMasterUpdate::class,
        Commands\ShiptFeatured::class,
        PricingPickup::class,
        PricingImport::class,
        TextSubscriberSpecials::class,
        Commands\UserNotifications::class,
        Commands\ProductRequest::class,
        Commands\EventNotifications::class,
        Commands\KeywordProcessing::class,
        Commands\AwsSearch::class,
        Commands\DeleteAWSSearchDocuments::class,
        Commands\ResetAWSSearchFiles::class,
        Commands\AwsProductUpload::class,
        MovementUpdateForAwgPrices::class,
        RetalixImport::class,
        RetalixUpdate::class,
        RocWrap::class,
        SpinsImport::class,
        DepartmentsImport::class,
        LoyaltySignups::class,
        ShoppingAdPrices::class,
        ShoppingDB::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //AWS Cloudsearch
        //Pulling a store file every 10 minutes - Test concept 4/24/19
//        $schedule->command('aws:deleteDocs')->dailyAt('4:00')->timezone('America/Chicago');
//        $schedule->command('aws:reset_search_files')->dailyAt('4:00')->timezone('America/Chicago');
//        $schedule->command('aws:search')->everyTenMinutes();

        $schedule->command('departments:import')->sundays();

        $schedule->command('huddles:send')->cron('0 8 * * *')->timezone('America/Chicago');

        $schedule->command('loyalty:signups')->dailyAt('13:30');

        $schedule->command('movement:update')->dailyAt('20:00');

        $schedule->command('pricing:pickup')->everyTenMinutes();
        $schedule->command('pricing:import')->everyTenMinutes();

        $schedule->command('promos:send')->cron('0 7 * * MON-THU')->timezone('America/Chicago');

//        $schedule->command('retalix:import')->dailyAt('3:00')->timezone('America/Chicago');
//        $schedule->command('retalix:update')->dailyAt('17:00')->timezone('America/Chicago');

        $schedule->command('shipt:send')->dailyAt('6:00')->timezone('America/Chicago');
        $schedule->command('shipt:categories')->dailyAt('6:05')->timezone('America/Chicago');
        $schedule->command('shipt:featured')->dailyAt('6:10')->timezone('America/Chicago');
        $schedule->command('shipt:pricing')->everyTenMinutes();

        $schedule->command('text:specials')->dailyAt('15:00')->timezone('America/Chicago');

        $schedule->command('zenreach:import')->dailyAt('23:30')->timezone('America/Chicago');

        //Text Product requests emails
        $schedule->command('send:product_request')->dailyAt('00:30')->timezone('America/Chicago');

        $schedule->command('spins:import')->dailyAt('17:00');

        $schedule->command('shopping:db')->dailyAt('8:00');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
//        $this->load(__DIR__.'/Commands');
        require base_path('routes/console.php');
    }
}
