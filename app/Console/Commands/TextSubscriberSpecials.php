<?php

namespace App\Console\Commands;

use App\Ad;
use App\Brand;
use App\Jobs\TextSpecial;
use App\TextSubscriber;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Jelovac\Bitly4laravel\Facades\Bitly4laravel;

class TextSubscriberSpecials extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'text:specials';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $specials = Ad::where('date_from', '=', date('Y-m-d'))
        ->where('websites', false)
        ->where('status', true)
        ->get();

        foreach ($specials as $special) {
            $subscribers = TextSubscriber::where('brand_id', $special->brand_id)
                ->where('optin', true)
                ->get();

            $url = action('AdsController@email', $special->id);
            $url = Bitly4laravel::shorten($url);
            $url = $url->data->url;

            $to = ['13479681214'];

            foreach ($subscribers as $subscriber) {
                array_push($to, $subscriber->phone_number);
            }

            $message = 'A brand new exclusive offer is waiting for you: '.$url;

            $data = [
                'User'          => 'buyforlessok', /* change to your EZ Texting username */
                'Password'      => 'C@mbiar123ezte', /* change to your EZ Texting password */
                'PhoneNumbers'  => $to,
                'Subject'       => '',
                'Message'       => $message,
                'MessageTypeID' => 1,
            ];

            $curl = curl_init('https://app.eztexting.com/sending/messages?format=json');
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            $response = curl_exec($curl);
            curl_close($curl);

            $json = json_decode($response);

            switch ($json->Response->Code) {
                case 201:
                    exit('Message Sent');
                case 401:
                    exit('Invalid user or password');
                case 403:
                    $errors = $json->Response->Errors;
                    exit('The following errors occurred: '.implode('; ', $errors));
                case 500:
                    exit('Service Temporarily Unavailable');
            }
        }
    }
}
