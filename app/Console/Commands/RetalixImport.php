<?php

namespace App\Console\Commands;

use App\RetalixPrice;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class RetalixImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'retalix:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $url = Storage::url('retalix-pricing/'.date('Ymd').'/retalix-pricing.csv');

        $file = fopen($url, 'r');

        RetalixPrice::query()->truncate();

        //Output lines until EOF is reached
        while (! feof($file)) {
            $line = fgets($file);

            $price = explode('|', $line);

            if (count($price) == 12) {
                RetalixPrice::create([
                    'upc_ean' => $price[0],
                    'product_description' => $price[1],
                    'prm_store_number' => $price[2],
                    'ip_unit_price' => number_format((float) $price[3], 2),
                    'ip_price_multiple' => intval($price[11]),
                    'descriptive_size' => $price[4],
                    'ip_start_date' => $price[5],
                    'start_date' => date('Y-m-d h:i:s', strtotime($price[5])),
                    'ip_end_date' => $price[6],
                    'end_date' => ($price[6] !== '') ? date('Y-m-d h:i:s', strtotime($price[6])) : null,
                    'price_strategy' => $price[7],
                    'dept_code' => substr($price[8], 0, 3),
                    'scale_flag' => $price[9],
                    'ad_group' => $price[10] ?? '',
                ]);

                echo $line.'<br>';
            } else {
                Log::info('('.count($price).') Line was not imported to Retalix Prices Index: '.$line);
            }
        }

        fclose($file);
    }
}
