<?php

namespace App\Console\Commands;

use App\Imports\ItemMovementImport;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class SpinsImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'spins:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $files = Storage::files('spins');

        if (count($files) > 0) {
            Excel::import(new ItemMovementImport(), $files[0], 's3');

            Storage::move($files[0], 'spins/processed/'.basename($files[0]));
        }
    }
}
