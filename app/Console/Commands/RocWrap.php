<?php

namespace App\Console\Commands;

use App\Roc;
use App\RocEntry;
use Illuminate\Console\Command;

class RocWrap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'roc:wrap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $roc = new Roc([
            'date' => date('Y-m-d'),
        ]);

        $roc->save();

        $entries = RocEntry::where('roc_id', null)
            ->update([
                'roc_id' => $roc->id,
            ]);
    }
}
