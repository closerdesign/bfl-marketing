<?php

namespace App\Console\Commands;

use App\Store;
use Hocza\Sendy\Facades\Sendy;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class ZenreachImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zenreach:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports contact from Zenreach.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $count = 0;

        $stores = Store::whereNotNull('zenreach_code')
            ->whereNotNull('sendy_list_id')
            ->get();

        foreach ($stores as $store) {
            $url = 'https://api.zenreach.com/locations/'.$store->zenreach_code.'/contacts?sort=id:desc';

            $client = new \GuzzleHttp\Client();

            try {
                $response = $client->get($url, [
                    'headers' => [
                        'Authorization' => 'token dbee93d4-e5ce-4199-af83-c566c9442c60',
                    ],
                ]);
            } catch (\Exception $e) {
                $response = 0;
            }

            $headers = $response->getHeaders();

            $total_records = $headers['X-Total-Count'][0];

            $body = $response->getBody();

            $body = json_decode($body);

            $loops = round($total_records / 100, 0, PHP_ROUND_HALF_UP);

            $skip = 0;

            for ($k = 0; $k < $loops; $k++) {
                $url = 'https://api.zenreach.com/locations/'.$store->zenreach_code.'/contacts?sort=id:desc&limit=100&skip='.$skip;

                $client = new \GuzzleHttp\Client();

                try {
                    $response = $client->get($url, [
                        'headers' => [
                            'Authorization' => 'token dbee93d4-e5ce-4199-af83-c566c9442c60',
                        ],
                    ]);
                } catch (\Exception $e) {
                    $response = 0;
                }

                $skip += 100;

                $contacts = json_decode($response->getBody());

                foreach ($contacts as $contact) {
                    $status = Sendy::status($contact->contact_info);

                    if ($status == 'Email does not exist in list') {
                        $subscribe = Sendy::setListId($store->sendy_list_id)->subscribe([
                            'email' => $contact->contact_info,
                        ]);

                        if ($subscribe['status'] == 'subscribed') {
                            $count++;
                        }
                    }
                }
            }
        }

        if ($count > 0) {
            Mail::send('emails.zenreach-import', ['count' => $count], function ($m) use ($count) {
                $m->from('media@buyforlessok.com', 'Media Team');

                $m->to('digital@buyforlessok.com')
                    ->subject('['.date('Y-m-d').'] Zenreach Import Has Been Completed');
            });
        }
    }
}
