<?php

namespace App\Console\Commands;

use App\Product;
use App\Search;
use Illuminate\Console\Command;

class KeywordProcessing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:keyword';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $words = Search::where('keyword_processed', '=', 0)->where('cart_product_id', '!=', 0)->get();

        $this->process_keywords($words);

        $this->mark_finished();
    }

    public function process_keywords($words)
    {
        if (! empty($words)) {
            foreach ($words as $word) {
                $current_keywords = $this->get_keywords($word->cart_product_id);
                if (! empty($current_keywords)) {
                    $keyword_pieces = explode(',', $current_keywords);
                    foreach ($keyword_pieces as $p) {
                        $keyword_pieces[] = trim(strtolower($p));
                    }
                } else {
                    $this->update_keywords('', trim($word->search_string), $word->cart_product_id);
                }

                if (! empty($keyword_pieces)) {
                    if (! in_array(trim(strtolower($word->search_string)), $keyword_pieces)) {
                        //insert previous keywords and current keywords
                        $this->update_keywords($current_keywords, trim($word->search_string), $word->cart_product_id);
                    } else {

                        // If keyword is already in column, mark as reviewed
                        $this->mark_reviewed($word->id);
                    }
                }
            }
        }
    }

    public function get_keywords($upc)
    {
        $product = Product::where('upc', '=', $upc)->first();

        if (! empty($product->keywords)) {
            return $product->keywords;
        } else {
            return;
        }
    }

    public function update_keywords($current_keywords, $keywords, $upc)
    {
        if ($current_keywords == '') {
            $string = strtolower(trim($keywords));
        } else {
            $string = strtolower($current_keywords).', '.strtolower(trim($keywords));
        }

        Product::where('upc', $upc)->update(['keywords' => $string]);
    }

    public function mark_finished()
    {
        Search::where('keyword_processed', '=', 0)->update(['keyword_processed' => 1]);
    }

    public function mark_reviewed($id)
    {
        Search::where('id', $id)->update(['reviewed' => 1]);
    }
}
