<?php

namespace App\Console\Commands;

use App\Product;
use Aws\CloudSearchDomain\CloudSearchDomainClient;
use Illuminate\Console\Command;

class AwsProductUpload extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'aws:product';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Product::query()->update(['aws_search_check' => 0]);
        $count = Product::all()->where('aws_search_check', 0)->count();
        $loops = (ceil($count / 5000) - 1);

        for ($x = 0; $x <= $loops; $x++) {
            $products = Product::all()->where('aws_search_check', 0)->take(5000);

            // $products = json_decode($products);

            $this->put_aws($products);

            foreach ($products as $p) {
                $pu = Product::find($p->id);
                $pu->aws_search_check = 1;
                $pu->save();
            }
        }
    }

    /**
     * @param $row
     * @return array
     * removes blank fields for upload and converts keywords to an array
     */
    public function format_fields($row)
    {
        $fields = [];

        $row_j = json_decode($row);
        foreach ($row_j as $key => $value) {
            //  print_r($value);
            if ($value != '') {
                if (in_array($key, ['incrementing', 'created_at', 'updated_at', 'im_check', 'exists', 'timestamps', 'images', 'aws_search_check', 'categories', 'stores', 'items'])) {
                    continue;
                }
                if ($key == 'keywords') {
                    if ($value != '') {
                        $value = explode(',', $value);
                        $fields[$key] = $value;
                    }
                } else {
                    $fields[$key] = strip_tags(trim($value));
                }
            }
        }
        foreach ($row->categories as $category) {
            $fields['categories'][] = $category->name;
        }
        foreach ($row->items as $i) {
            $fields['stores'][] = $i->store_number;
        }

        return $fields;
    }

    /**
     * @param $products
     * @return array
     */
    public function make_aws_format($products)
    {
        foreach ($products as $p) {
            $batch[] = ['type' => 'add',
                'id'  => $p->upc,
                'fields' => $this->format_fields($p), ];
        }

        return $batch;
    }

    /**
     * @param $products
     */
    public function put_aws($products)
    {
        $batch_file = $this->make_aws_format($products);

        print_r($batch_file);

        $CSclient = CloudSearchDomainClient::factory(['credentials' => ['key' => env('AWSCloudKey'),
            'secret' => env('AWSSecret'), ],
            'endpoint' => 'https://doc-product-name-tgegi4uon56ims5tn3ikb6kanu.us-west-1.cloudsearch.amazonaws.com',
            'validation' => false,
            'version' => 'latest', ]);

        $result = $CSclient->uploadDocuments([
            'documents' => \GuzzleHttp\json_encode($batch_file),
            'contentType' =>'application/json',
        ]);

        print_r($result);
    }
}
