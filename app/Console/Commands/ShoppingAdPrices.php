<?php

namespace App\Console\Commands;

use App\RetalixPrice;
use App\ShoppingProduct;
use Illuminate\Console\Command;

class ShoppingAdPrices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shopping:adprices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Evaluates Ad Pricing to Update Online Shopping Sales';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ad_prices = RetalixPrice::query()
//            ->where('start_date', '<=', date('Y-m-d'))
            ->where('end_date', '>=', date('Y-m-d'))
            ->where('price_strategy', 0)
            ->where(function ($q) {
                $q->where('ad_group', 111)
                    ->orWhere('ad_group', 222);
            })
            ->get();

        $count = 0;

        foreach ($ad_prices as $ad_price) {
            $sku = substr($ad_price->upc_ean, 0, 13);

            $items = ShoppingProduct::where('sku', $sku)
                ->get();

            foreach ($items as $item) {
                if (($item->store_number == 1230) && ($ad_price->ad_group == 111)) {
                    $item->sale_price = $ad_price->ip_unit_price;
                    $item->sale_qty = $ad_price->ip_price_multiple;
                    $item->save();

                    $count++;
                }

                if (($item->store_number == 9515) && ($ad_price->ad_group == 111)) {
                    $item->sale_price = $ad_price->ip_unit_price;
                    $item->sale_qty = $ad_price->ip_price_multiple;
                    $item->save();

                    $count++;
                }

                if (($item->store_number == 3501) && ($ad_price->ad_group == 222)) {
                    $item->sale_price = $ad_price->ip_unit_price;
                    $item->sale_qty = $ad_price->ip_price_multiple;
                    $item->save();

                    $count++;
                }
            }
        }

        echo $count;
    }
}
