<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;

class CategoriesShipt extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shipt:categories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $file = View::make('products.shipt-categories');

        $file = (string) $file;

        $filename = 'bfl-categories-'.date('Y-m-d').'.csv';

        Storage::disk('shipt')->put($filename, $file, 'public');

        Mail::send('emails.products.shipt-categories-notification', ['filename' => $filename, 'file' => $file], function ($m) use ($filename, $file) {
            $m->from('digital@buyforlessok.com', 'BFL Media Team')
            ->to('mellis@buyforlessok.com', 'BFL Media Team')
            ->subject('[SHIPT] Categories File Has Been Processed: '.$filename)
            ->attachData($file, $filename, [
                'mime' => 'text/csv',
            ]);
        });
    }
}
