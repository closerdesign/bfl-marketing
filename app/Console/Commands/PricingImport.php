<?php

namespace App\Console\Commands;

use App\PricingFile;
use App\ShoppingProduct;
use App\Store;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class PricingImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pricing:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $pricing = PricingFile::where('status', 0)->first();

        if ($pricing) {
            $file = Storage::url('pricing-files/'.$pricing->file);

            $content = fopen($file, 'r');

            $pricing->store = substr($pricing->name, -8, 4);

            DB::delete('DELETE FROM shopping_products WHERE store_id = '.$pricing->store);

            $total = 0;

            while (! feof($content)) {
                try {
                    $row = fgets($content);

                    echo $pricing->file.': '.$row;

                    $row = explode('|', $row);

                    if ($row[0] != 'SKU') {
                        $product = new ShoppingProduct([
                            'store_id'         => $pricing->store,
                            'sku'              => $row[0],
                            'brand_name'       => $row[1],
                            'product_name'     => $row[2],
                            'description'      => $row[3],
                            'size'             => $row[4],
                            'uom'              => $row[5],
                            'dept_code'        => $row[6],
                            'scale_flag'       => $row[7],
                            'how_to_sell'      => $row[8],
                            'avg_weight'       => $row[9],
                            'wgt_selector'     => $row[10],
                            'store_number'     => $row[11],
                            'ad_flag'          => $row[12],
                            'cool'             => $row[13],
                            'regular_price'    => $row[14],
                            'sale_price'       => $row[15],
                            'sale_qty'         => $row[16],
                            'promo_start_date' => $row[17],
                            'promo_end_date'   => $row[18],
                            'promo_buy_qty'    => $row[19],
                            'promo_get_qty'    => $row[20],
                            'promo_min_qty'    => $row[21],
                            'promo_limit_qty'  => $row[22],
                            'family_sale_flag' => $row[23],
                            'family_sale_code' => $row[24],
                            'family_sale_desc' => $row[25],
                            'tax_pct'          => $row[26],
                            'bottle_deposit'   => $row[27],
                            'promo_tag'        => $row[28],
                            'movement'         => $row[32],
                        ]);

                        $product->save();
                    }

                    $total += 1;
                } catch (\Exception $e) {
                }
            }

            $pricing->status = 1;

            $pricing->save();

            $shiptFlag = Store::where('store_code', $pricing->store)->update([
                'shipt_flag' => false,
            ]);

            Artisan::call('shopping:adprices');

            DB::table('stores')
                ->where('store_code', $pricing->store)
                ->update([
                    'aws_search_file' => 0,
                ]);

            Mail::send('emails.pricing-files', ['pricing' => $pricing, 'total' => $total], function ($m) use ($pricing, $total) {
                $m->from('jrodriguez@buyforlessok.com', 'Juan Rodriguez');

                $m->to('juanc@closerdesign.co', 'BFL Media Team')
                    ->subject('[Processed Pricing File] '.$pricing->name);
            });
        }
    }
}
