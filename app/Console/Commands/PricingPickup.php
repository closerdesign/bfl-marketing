<?php

namespace App\Console\Commands;

use App\PricingFile;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class PricingPickup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pricing:pickup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pickup files from Retalix folder and drop them off on S3';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $files = Storage::disk('retalix')->files('uploads');

        foreach ($files as $file) {
            $filename = basename($file);

            $extension = explode('.', $filename);

            if ($extension[1] == 'dat') {
                $content = Storage::disk('retalix')->get('uploads/'.$filename);

                Storage::put('pricing-files/'.$filename, $content, 'public');

                $pricing = new PricingFile([
                    'name'   => $filename,
                    'file'   => $filename,
                    'store'  => 9515,
                    'status' => 0,
                ]);

                $pricing->save();

                Storage::disk('retalix')->delete('uploads/'.$filename);
            }
        }
    }
}
