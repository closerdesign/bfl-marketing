<?php

namespace App\Console\Commands;

use App\ShoppingProduct;
use App\Store;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;

class ShiptFeatured extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shipt:featured';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sending Shipt Featured Products';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $stores = Store::where('shipt_files', true)->get();

        foreach ($stores as $store) {
            $products = ShoppingProduct::where('store_number', $store->store_code)
                ->orderBy('movement', 'desc')
                ->take(1500)
                ->get();

            $file = View::make('products.shipt-products', ['products' => $products]);

            $file = (string) $file;

            $filename = 'bfl-featured-'.$store->store_code.'-'.date('Y-m-d').'.csv';

            Storage::disk('shipt')->put($filename, $file, 'public');

            Mail::send('emails.shipt.featured', ['filename' => $filename, 'file' => $file], function ($m) use ($filename, $file) {
                $m->from('digital@buyforlessok.com', 'BFL Media Team');
                $m->to('mellis@buyforlessok.com')
                    ->subject('[SHIPT] Featured Products File Has Been Processed: '.$filename)
                    ->attachData($file, $filename, [
                        'mime' => 'text/csv',
                    ]);
            });
        }
    }
}
