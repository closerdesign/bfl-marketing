<?php

namespace App\Console\Commands;

use App\RsaSignup;
use App\Store;
use GuzzleHttp\Client;
use Hocza\Sendy\Facades\Sendy;
use Illuminate\Console\Command;

class LoyaltySignups extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'loyalty:signups';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $keys = [
            'bfl' => [
                'URL' => 'https://buyforlessok.rsaamerica.com/PartnerApi/SSWebRestApi.svc/GetNewCustomerSignUps',
                'EnterpriseId' => 'A07DC24C-B545-4FED-A840-3632CBD5F0F5',
                'SecurityKey' => 'ED2559CF-6927-4A3B-811A-E223808D98CD',
                'UserToken' => '2C4F6225-1374-487D-AACB-60BCD42E7BEB',
            ],
            'uptown' => [
                'URL' => 'https://uptowngroceryco.rsaamerica.com/partnerapi/SSWebRestApi.svc/GetNewCustomerSignUps',
                'EnterpriseId' => 'C7C9E7CB-579C-4E22-A694-F3347F2C83A3',
                'SecurityKey' => 'D8CE9DA1-526E-4F8E-9604-4C29B8410717',
                'UserToken' => '2C4F6225-1374-487D-AACB-60BCD42E7BEB',
            ],
        ];

        $i = 0;

        foreach ($keys as $key) {
            $client = new Client();

            $start_date = date('Y-m-d', strtotime('-1 days'));
            $end_date = date('Y-m-d', strtotime('-1 days'));

//            $start_date = '2019-01-01';
//            $end_date = '2019-01-31';

            $body = '{
                "EnterpriseId": "'.$key['EnterpriseId'].'", 
                "SecurityKey": "'.$key['SecurityKey'].'", 
                "StartDate":"'.$start_date.' 00:00:00.000",
                "EndDate":"'.$end_date.' 00:00:00.000",
                "UserToken": "'.$key['UserToken'].'"
            }';

            $response = $client->post($key['URL'], [
                'headers' => [
                    'Content-type' => 'application/json; charset=utf-8',
                ],
                'body' => $body,
            ]);

            $signups = json_decode($response->getBody());

            if (isset($signups->NewCustomerSignUps)) {
                foreach ($signups->NewCustomerSignUps as $signup) {
                    $subscriber = new RsaSignup();
                    $subscriber->first_name = $signup->FirstName;
                    $subscriber->last_name = $signup->LastName;
                    $subscriber->sign_up_date = date('Y-m-d', strtotime($signup->SignUpDate));
                    $subscriber->store_code = $signup->StoreCode;
                    $subscriber->store_name = $signup->StoreName;
                    $subscriber->user_name = $signup->UserName;
                    $subscriber->save();

                    if (Store::where('store_code', $subscriber->store_code)->first()->rsa_sendy_list) {
                        Sendy::setListId(Store::where('store_code', $subscriber->store_code)->first()->rsa_sendy_list)
                            ->subscribe([
                                'name' => $subscriber->first_name.' '.$subscriber->last_name,
                                'email' => $subscriber->user_name,
                            ]);
                    }

                    $i++;
                }
            }
        }

        echo $i;
    }
}
