<?php
/**
 * Created by PhpStorm.
 * User: davidmeinke
 * Date: 11/12/18
 * Time: 11:07 AM.
 */

namespace App\Console\Commands;

use App\Huddle;
use App\TextSubscriber;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class SendDailyProductRequest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'product-request:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        // THIS IS WHERE IT WILL BE SENT
        /*
        $emails = $brand->stores->map(function ($e) {
            return $e->email;
        })->toArray();

        $yesterday = date("Y-m-d", strtotime('-1 days'));

        $text_requests = TextSubscriber::where(DB::raw('DATE(created_at)'), $yesterday)->get();

        $m->from('digital@buyforlessok.com', 'BFL Developers');
        $m  ->to($emails)
            ->cc(['levans@buyforlessok.com', 'hbinkowski@buyforlessok.com', 'jrodriguez@buyforlessok.com', 'rmolder@buyforlessok.com'])
            ->replyTo('media@buyforlessok.com');
 */
    }
}
