<?php

namespace App\Console\Commands;

use App\Huddle;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class SendHuddles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'huddles:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Huddles Summary to Our Team';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $yesterday = date('Y-m-d', strtotime('-1 days'));

        $huddles = Huddle::where(DB::raw('DATE(created_at)'), $yesterday)->get();

        if (count($huddles) > 0) {
            Mail::send('emails.huddles.summary', ['huddles' => $huddles, 'yesterday', $yesterday], function ($m) use ($huddles, $yesterday) {
                $m->from('digital@buyforlessok.com', 'BFL Media Team');

                $m->to('supportcenter@buyforlessok.com')
                    ->cc('storemanagers@buyforlessok.com')
                    ->replyTo('digital@buyforlessok.com')
                    ->subject('['.$yesterday.'] Huddles Report');
            });
        }
    }
}
