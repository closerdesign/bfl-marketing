<?php

namespace App\Console\Commands;

use App\Mail\ProductRequestMail;
use App\Store;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class ProductRequest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:product_request';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends product requests to those that need it';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function brand()
    {
        $brand = [
            '14058967933' => 1,
            '14058967965' => 2,
            '14058967983' => 3,
            '14058967969' => 4,
        ];

        return $brand;
    }

    public function get_brand_emails($brand_id)
    {
        $emails = Store::select('email')->where('brand_id', $brand_id)->get()->toArray();

        return $emails;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach ($this->brand() as $number => $id) {
            $requests = \App\ProductRequest::where('created_at', '>', date('Y-m-d', strtotime('yesterday')))->where('to', $number)->get();

            if (count($requests) > 0) {
                $emails = ['levans@buyforlessok.com', 'rmolder@buyforlessok.com'];
                $allEmails = array_merge($emails, $this->get_brand_emails($id));
                Mail::to($allEmails)->send(new \App\Mail\ProductRequestMail($requests));
            }
        }
    }
}
