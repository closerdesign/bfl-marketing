<?php

namespace App\Console\Commands;

use App\AwgPrice;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class MovementUpdateForAwgPrices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'movement:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start = date('Y-m-d', strtotime('-14 weeks'));
        $end = date('Y-m-d');

        $movement = DB::table('item_movements')
            ->select('upc', DB::raw('SUM(qty_sold) as qty'), DB::raw('SUM(amt_sold) as amount'))
            ->whereBetween('week_ending_date', [$start, $end])
            ->groupBy('upc')
            ->get();

        echo $movement->count();

//        foreach ($movement as $item)
//        {
//            AwgPrice::where('upc', $item->upc)
//                ->update([
//                    'qty' => $item->qty,
//                    'amount' => $item->amount
//                ]);
//        }
    }
}
