<?php

namespace App\Console\Commands;

use App\Product;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class ItemMasterUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'im:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pulling product data from Item Master';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function check_digit($value)
    {
        $digits = str_split($value);

        $by = 3;

        $sum_digits = [];

        foreach ($digits as $d) {
            $digit = $d * $by;

            array_push($sum_digits, $digit);

            if ($by == 1) {
                $by = 3;
            } else {
                $by = 1;
            }
        }

        $total = array_sum($sum_digits);

        $rounded = ceil($total / 10) * 10;

        $check = $rounded - $total;

        return $check;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $products = Product::where('image', '')->where('im_check', false)->take(500)->get();

        $total = count($products);

        if ($total == 0) {
            DB::table('products')->update(['im_check' => false]);

            Log::info('Item Master Check Has Been Reset');

            dd('Item Master Check Has Been Reset');
        } else {
            $count = 0;

            foreach ($products as $product) {
                $check = $this->check_digit($product->upc);

                $upc = $product->upc.$check;

                $client = new Client();

                $response = $client->request('GET', 'https://api.itemmaster.com/v2/item?epf=600&upc='.$upc, [
                    'headers' => [
                        'username' => env('ITEMMASTER_USERNAME'),
                        'password' => env('ITEMMASTER_PASSWORD'),
                    ],
                ]);

                $data = $response->getBody();

                $data = new \SimpleXMLElement($data);

                if (($data->attributes()->count == 1) && ($data->item->media->medium->url)) {
                    $img = file_get_contents($data->item->media->medium->url);

                    $filename = uniqid().'.png';

                    Storage::put('product-image/'.$filename, $img, 'public');

                    $product->image = $filename;

                    Log::info('Image imported successfully: '.$upc);

                    $count += 1;
                } else {
                    Log::info('Nothing for '.$upc);
                }

                $product->im_check = true;

                $product->save();
            }

            Log::info('IM Update Process Finished. '.$count.' products updated.');

            dd($count);
        }
    }
}
