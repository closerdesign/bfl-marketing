<?php

namespace App\Console\Commands;

use App\Mail\Notify;
use App\Notification;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class UserNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:notifications';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function emailAddressses($emails)
    {
        if (strpos($emails, ',')) {
            print_r($emails);
            $send = explode(',', $emails);

            return $send;
        } else {
            return $emails;
        }
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $hour = date('H');

        $hour = $hour.':00:00';

        $results = Notification::where('send_time', '=', $hour)->get();

        // Changed the query to the above because MySQL is running off Greenwich Mean Time. This way dates are stored in a readable format
        //$results = DB::select(DB::raw("SELECT * FROM notifications WHERE DATE_FORMAT(send_time, '%H') = DATE_FORMAT(DATE_SUB(CURTIME(), INTERVAL 6 HOUR), '%H')"));

        if (! empty($results)) {
            foreach ($results as $r) {
                $emails = $this->emailAddressses($r->notification_to);
                if ($r->send_date_type == 'dailyAt') {
                    Mail::to($emails)->send(new Notify($r));
                }

                if ($r->send_date_type == 'weeklyOn') {
                    if ($r->send_day == date('N')) {
                        $emails = $this->emailAddressses($r->notification_to);
                        Mail::to($emails)->send(new Notify($r));
                    }
                }

                if ($r->send_date_type == 'monthlyOn') {
                    if ($r->send_day_month == date('j')) {
                        $emails = $this->emailAddressses($r->notification_to);
                        Mail::to($emails)->send(new Notify($r));
                    }
                }
                if ($r->send_date_type = 'once') {
                    $send = DB::select(DB::raw("SELECT DATE_FORMAT(CURDATE(), '%Y-%m-%d') as today  FROM DUAL "));

                    foreach ($send as $s) {
                        echo $s->today;
                        if ($s->today == $r->send_date) {
                            $emails = $this->emailAddressses($r->notification_to);
                            Mail::to($emails)->send(new Notify($r));
                        }
                    }
                }
            }
        }
    }
}
