<?php

namespace App\Console\Commands;

use App\CampaignItem;
use App\Mail\Notification;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class RetalixUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'retalix:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $items = CampaignItem::all();

        $count = 0;

        foreach ($items as $item) {
            $price = 0;

            $retalix = \App\RetalixPrice::where('upc_ean', 'like', '%0'.$item->plu.'00000%')
                ->orderBy('ip_start_date', 'desc')
                ->orderBy('ip_unit_price', 'desc');

            if (($retalix->count() > 0) && ($item->plu !== null)) {
                $price = $retalix->first()->ip_unit_price;
            }

            if ($price > 0) {
                CampaignItem::find($item->id)->update([
                    'price' => number_format($price, 2),
                ]);

                $count++;
            }
        }

        Mail::to('juanc@closerdesign.co')
            ->send(new Notification([
                '['.date('Y-m-d H:i:s').'] '.$count.' Items Processed for the Catering Menu',
                'This is a notification that '.$count.' have been price checked for the catering menu.',
            ]));
    }
}
