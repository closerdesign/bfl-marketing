<?php

namespace App\Console\Commands;

use App\Event;
use App\EventSignup;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class EventNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'event:notify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send event reminder a day before event';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $tomorrow = date('Y-m-d', strtotime('+1 days'));

        $events = Event::where(DB::raw('DATE(`date`)'), $tomorrow)->get();

        foreach ($events as $event) {
            $attendees = EventSignup::where('event_id', $event->id)->get();
            foreach ($attendees as $attendee) {
                Mail::to($attendee->email, $attendee->name)->send(new \App\Mail\EventNotify($event, $attendee));
            }
        }
    }
}
