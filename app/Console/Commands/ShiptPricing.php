<?php

namespace App\Console\Commands;

use App\ShoppingProduct;
use App\Store;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;

class ShiptPricing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shipt:pricing';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate pricing reports for sending to Shipt S3.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $store = Store::where(function ($q) {
            $q->where('shipt_files', 1)
                ->where('shipt_flag', 0);
        })->first();

        if ($store != null) {
            $products = ShoppingProduct::where('store_number', $store->store_code)->get();

            $file = View::make('shipt.pricing', ['products' => $products, 'store' => $store]);

            $file = (string) $file;

            $filename = 'bfl-'.$store->store_code.'-pricing-'.date('Y-m-d').'.csv';

            Storage::disk('s3')->put('shipt/'.$filename, $file, 'public');
            Storage::disk('shipt')->put($filename, $file, 'public');

            $store->shipt_flag = true;

            $store->save();

            Mail::send('emails.shipt.pricing', ['filename' => $filename, 'file' => $file], function ($m) use ($filename, $file) {
                $m->from('digital@buyforlessok.com', 'BFL Media Team');

                $m->to('juanc@closerdesign.co')
                    ->subject('[SHIPT] Pricing File Has Been Processed: '.$filename)
                    ->attachData($file, $filename, [
                        'mime' => 'text/csv',
                    ]);
            });
        }
    }
}
