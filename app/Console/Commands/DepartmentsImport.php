<?php

namespace App\Console\Commands;

use App\Department;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class DepartmentsImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'departments:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::table('departments')->truncate();

        $csv = Storage::get('store-departments.csv');

        $rows = explode('<br />', $csv);

        foreach ($rows as $row) {
            $line = explode(',', $row);

            if (isset($line[0]) && isset($line[1])) {
                Department::create([
                    'code' => (int) $line[0],
                    'name' => $line[1],
                ]);
            }
        }
    }
}
