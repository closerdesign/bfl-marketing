<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ShoppingDB extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shopping:db';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        DB::statement('DROP DATABASE shopping');

        $filename = date('Ymd').'_shopping_db.sql.gz';

        $backup = Storage::disk('s3')->get('shopping-db-backup/'.$filename);
        $backup = Storage::disk('local')->put('shopping-db-backup/'.$filename, $backup);

        //mysql command to restore backup from the selected gzip file
        $command = 'zcat '.storage_path().'/app/shopping-db-backup/'.$filename.' | mysql --user='.env('DB_USERNAME').' --password='.env('DB_PASSWORD').' --host='.env('DB_HOST').' shopping';

        $returnVar = null;
        $output = null;
        exec($command, $output, $returnVar);

        Storage::disk('local')->delete('shopping-db-backup/'.$filename);

        if (! $returnVar) {
            $this->info('Database Restored');
        } else {
            $this->error($returnVar);
        }

        return 0;
    }
}
