<?php

namespace App\Console\Commands;

use App\Ad;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendPromos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'promos:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Promos To Our Team';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $promos = \App\Promo::available()->get();

        $specials = Ad::where('date_to', '>=', date('Y-m-d'))->whereHas('specials', function ($q) {
            $q->where('plu_required', 1);
        })->get();

        if ((count($promos) > 0) || (count($specials) > 0)) {
            \Illuminate\Support\Facades\Mail::send('emails.promos.daily-summary', ['promos' => $promos, 'specials' => $specials], function ($m) use ($promos, $specials) {
                $m->from('digital@buyforlessok.com', 'BFL Media Team');

                $m->to('supportcenter@buyforlessok.com')
                    ->cc('storemanagers@buyforlessok.com')
                    ->replyTo('digital@buyforlessok.com')
                    ->subject('['.date('m-d-Y').'] Here are our Promos for Today!');
            });
        }
    }
}
