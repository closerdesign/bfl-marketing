<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampaignItemSize extends Model
{
    protected $fillable = [
        'name',
        'price',
        'campaign_item_id',
        'plu',
    ];
}
