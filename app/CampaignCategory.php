<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

class CampaignCategory extends Model
{
    protected $fillable = [
        'name',
        'image',
        'description',
        'status',
        'is_sides_category',
        'position',
        'template',
    ];

    public function getImageAttribute($value)
    {
        if ($value == null) {
            return URL::to('img/pending-image.jpg');
        }

        return Storage::url('campaign-categories/'.$value);
    }

    public function getStatusAttribute($value)
    {
        $status = 'Inactive';

        if ($value == 1) {
            $status = 'Active';
        }

        return $status;
    }

    public function campaigns()
    {
        return $this->belongsToMany(\App\Campaign::class, 'campaign_campaign_category');
    }

    public function campaign_items()
    {
        return $this->hasMany(\App\CampaignItem::class)
            ->orderBy('date');
    }

    public function items()
    {
        return $this
            ->belongsToMany(\App\CampaignItem::class, 'campaign_category_campaign_item')
            ->where('status', 1)
            ->where('plu', '!=', 'null')
            ->where('price', '>', 0);
    }

    public function all_items()
    {
        return $this
            ->belongsToMany(\App\CampaignItem::class, 'campaign_category_campaign_item')
            ->where('status', 1);
    }
}
