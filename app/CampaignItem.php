<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class CampaignItem extends Model
{
    protected $fillable = [
        'name',
        'image',
        'description',
        'directions',
        'price',
        'status',
        'date',
        'time',
        'store_id',
        'max',
        'plu',
    ];

    public function getImageAttribute($value)
    {
        return Storage::url('campaign-items/'.$value);
    }

    /**
     *  Status Attribute Mutator.
     */
    public function getStatusAttribute($value)
    {
        $status = 'Unpublished';

        if ($value == 1) {
            $status = 'Published';
        }

        return $status;
    }

    public function campaign_categories()
    {
        return $this->belongsToMany(\App\CampaignCategory::class, 'campaign_category_campaign_item', 'campaign_item_id', 'campaign_category_id');
    }

    public function sizes()
    {
        return $this->hasMany(\App\CampaignItemSize::class);
    }

    public function choices()
    {
        return $this->hasMany(\App\Choice::class);
    }

    /**
     *  Store Getter
     *  Helps when store is empty.
     */
    public function getStore()
    {
        if (! $this->getAttribute('store_id')) {
            return;
        }

        return $this->getAttribute('store');
    }

    /**
     *  Stores Relationship.
     */
    public function store()
    {
        return $this->belongsTo(\App\Store::class);
    }

    /**
     *  Occurrences Relationship.
     */
    public function occurrences()
    {
        return $this->hasMany(\App\CampaignItemOccurrence::class);
    }

    /**
     *  Store ID Mutator.
     */
    public function setStoreIdAttribute($value)
    {
        if ($value == '') {
            return null;
        }

        return $value;
    }

    /**
     *  Retalix Price.
     */
    public function getRetalixPriceAttribute()
    {
        $price = 0;

        if ((\App\RetalixPrice::where('upc_ean', 'like', '%00'.$this->plu.'00000%')->orderBy('ip_start_date', 'desc')->count() > 0) && ($this->plu != null)) {
            $price = \App\RetalixPrice::where('upc_ean', 'like', '%'.$this->plu.'%')->orderBy('ip_start_date', 'desc')->first()->ip_unit_price;
        }

        return number_format($price, 2);
    }
}
