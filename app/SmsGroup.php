<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmsGroup extends Model
{
    protected $fillable = [
        'group_name',
        'welcome_message',
        'keyword',

    ];
}
