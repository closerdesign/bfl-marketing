<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventSignup extends Model
{
    protected $fillable = [

        'event_id',
        'name',
        'email',
        'phone',

    ];

    public function event()
    {
        return $this->belongsTo(\App\Event::class, 'event_id', 'id');
    }
}
