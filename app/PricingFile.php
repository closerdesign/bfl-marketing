<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PricingFile extends Model
{
    public $fillable = [
        'name',
        'file',
        'store',
        'status',
    ];

    public function getStatusAttribute($value)
    {
        $status = 'Pending';

        if ($value == 1) {
            $status = 'Completed';
        }

        return $status;
    }
}
