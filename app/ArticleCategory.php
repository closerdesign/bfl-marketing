<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleCategory extends Model
{
    protected $table = 'articles_categories';

    protected $fillable = [
        'name',
        'parent_id',

    ];

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id');
    }

    /**
     *  Articles Relationship.
     */
    public function articles()
    {
        return $this->belongsToMany(Article::class, 'article_article_category')
            ->orderBy('created_at', 'desc');
    }
}
