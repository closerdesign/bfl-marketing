<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;

class RewardITNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $reward;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($reward)
    {
        $this->reward = $reward;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.reward-it-notification')
            ->subject('A new reward has been added that requires a PLU')
            ->replyTo('digital@buyforlessok.com');
    }
}
