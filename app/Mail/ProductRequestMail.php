<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ProductRequestMail extends Mailable
{
    use Queueable, SerializesModels;

    public $product_requests;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($product_requests)
    {
        $this->product_requests = $product_requests;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.product-requests.daily-summary')
            ->subject('Product Request Daily Summary '.date('m/d/Y'))
            ->replyTo('tmed@buyforlessok.com');
    }
}
