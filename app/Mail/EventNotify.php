<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EventNotify extends Mailable
{
    use Queueable, SerializesModels;

    public $event;

    public $attendee;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($event, $attendee)
    {
        //
        // $this->signup = $signup;
        $this->event = $event;
        $this->attendee = $attendee;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.event-signups.reminder')
            ->subject('Reminder: '.$this->event->name)
            ->replyTo('bvarner@buyforlessok.com', 'Becky Varner');
    }
}
