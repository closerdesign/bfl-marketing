<?php

namespace App\Mail;

use  Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VolunteerConfirmation extends Mailable
{
    use Queueable, SerializesModels;

    public $volunteer;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($volunteer)
    {
        $this->volunteer = $volunteer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.volunteers.confirmation')
            ->subject('Welcome '.$this->volunteer->first_name.' - BFLU Holiday Volunteer')
            ->replyTo('tmed@buyforlessok.com');
    }
}
