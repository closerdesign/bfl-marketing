<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeacherVote extends Model
{
    protected $fillable = [

        'brand',
        'name',
        'email',
        'phone',
        'school',
        'teacher',

    ];
}
