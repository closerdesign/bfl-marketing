<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesData extends Model
{
    protected $connection = 'sales';

    protected $fillable = [
        'store_num',
        'dept_name',
        'dept_sales',
        'sales_date',
    ];

    public function getDeptSalesAttribute($value)
    {
        return (int) $value;
    }

    public function getDeptNameAttribute($value)
    {
        return strtoupper($value);
    }
}
