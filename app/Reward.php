<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

class Reward extends Model
{
    protected $fillable = [
        'name',
        'image',
        'description',
        'plu',
        'promo_number',
        'brand_id',
        'start_date',
        'end_date',
        'status',
        'comments',

    ];

    /**
     *  Brand Relationship.
     */
    public function brand()
    {
        return $this->belongsTo(\App\Brand::class);
    }

    /**
     *  Valid Scope.
     */
    public function scopeValid()
    {
        return $this
            ->where('status', true)
            ->where('start_date', '<=', date('Y-m-d'))
            ->where('end_date', '>=', date('Y-m-d'));
    }

    /**
     *  Image Mutator.
     */
    public function getImageAttribute($value)
    {
        $image = URL::to('img/pending-image.jpg');

        if ($value != '') {
            $image = Storage::url('rewards/'.$value);
        }

        return $image;
    }

    /**
     *  Status Mutator.
     */
    public function getStatusAttribute($value)
    {
        $status = 'Inactive';

        if ($value == true) {
            $status = 'Active';
        }

        return $status;
    }
}
