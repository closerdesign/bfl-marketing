<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = [
        'name',
        'email',
        'phone',
        'employee_id',
        'birthdate',
        'comments',
        'store_id',
        'active',
    ];

    /**
     *  Liquor Licenses Relationship.
     */
    public function licenses()
    {
        return $this->hasMany(\App\LiquorLicenses::class);
    }

    public function cashiernumbers()
    {
        return $this->hasMany(\App\CashierNumbers::class);
    }

    /*
     * Store relationship
     */
    public function store()
    {
        return $this->belongsTo(\App\Store::class);
    }

    public function employeedocuments()
    {
        return $this->hasMany(\App\EmployeeDocument::class);
    }
}
