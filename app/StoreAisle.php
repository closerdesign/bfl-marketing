<?php

namespace App;

use App\Http\Controllers\StoreAisleController;
use Illuminate\Database\Eloquent\Model;

class StoreAisle extends Model
{
    //
    protected $fillable = [
        'aisle_name',
        'store_id',
        ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * Store Relationship
     */
    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function panels()
    {
        return $this->hasMany(StoreAislePanels::class, 'aisle_id', 'id');
    }
}
