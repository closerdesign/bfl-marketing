<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Event extends Model
{
    protected $fillable = [
        'name',
        'image',
        'intro',
        'description',
        'date',
        'end_date',
        'time',
        'end_time',
        'brand_id',
        'status',
        'signups',
        'location',
    ];

    public function getStatusAttribute($value)
    {
        $status = 'Unpublished';

        if ($value == 1) {
            $status = 'Published';
        }

        return $status;
    }

    public function getEndTimeAttribute($value)
    {
        if ($value == '00:00:00') {
            $value = null;
        }

        return $value;
    }

    public function getEndDateAttribute($value)
    {
        if ($value == '0000-00-00') {
            $value = null;
        }

        return $value;
    }

    public function getImageAttribute($value)
    {
        return Storage::url('events/'.$value);
    }

    public function scopeCurrent($query)
    {
        return $query
            ->where('date', '>=', date('Y-m-d'))
            ->orWhere('end_date', '>=', date('Y-m-d'))
            ->where('status', 1)
            ->orderBy('date', 'asc');
    }

    public function brands()
    {
        return $this->belongsTo(\App\Brand::class, 'brand_id', 'id');
    }

    public function signups()
    {
        return $this->hasMany(\App\EventSignup::class);
    }
}
