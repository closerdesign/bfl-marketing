<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TextSubscriber extends Model
{
    protected $fillable = [
        'phone_number',
        'brand_id',
        'optin',
        'text',
    ];

    /**
     *   Setting Text Attribute.
     */
    public function setTextAttribute($value)
    {
        $this->attributes['text'] = strtolower($value);
    }
}
