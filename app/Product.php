<?php

namespace App;

use Aws\CloudSearchDomain\CloudSearchDomainClient;
use http\Client\Request;
use http\Message\Body;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Laravel\Scout\Searchable;
use Nicolaslopezj\Searchable\SearchableTrait;

class Product extends Model
{
    use SearchableTrait;

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        /*
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'products.name' => 5,
            'products.keywords' => 9,
            'products.brand_name' => 3,
            'products.description' => 2,
            'products.size' => 2,
            'shopping_products.description' => 1,
            'products.upc' => 10,
            'products.kwikee_name' => 1,
            'products.kwikee_description' => 1,
            'products.kwikee_brand_name' => 3,

        ],

    ];

    protected $fillable = [
        'name',
        'upc',
        'image',
        'description',
        'brand_name',
        'size',
        'kwikee_name',
        'kwikee_description',
        'kwikee_brand_name',
        'kwikee_size',
        'im_check',
        'keywords',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function categories()
    {
        return $this->belongsToMany(\App\Category::class);
    }

    public function images()
    {
        return $this->hasMany(\App\KwikeeImage::class, 'product_id', 'id');
    }

    public function items()
    {
        return $this->hasMany(\App\ShoppingProduct::class, 'sku', 'upc');
    }

    public function getImageAttribute($value)
    {
        $image = URL::to('img/pending-image.jpg');

        foreach ($this->images as $file) {
            if (($file->type == 'PNG') || ($file->type == 'JPG') || ($file->type == 'GIF')) {
                $image = URL::to('kwikee-products/image-display?url='.urlencode($file->url));
            }
        }

        if ($value != '') {
            $image = Storage::url('product-image/'.$value);
        }

        return $image;
    }

    public function getNameAttribute($value)
    {
        $name = $value;

        if ($name == '') {
            $name = $this->kwikee_name;
        }

        return $name;
    }

    public function getDescriptionAttribute($value)
    {
        $description = $value;

        if ($description == '') {
            $description = $this->kwikee_description;
        }

        return $description;
    }

    public function getBrandNameAttribute($value)
    {
        $brand = $value;

        if ($brand == '') {
            $brand = $this->kwikee_brand_name;
        }

        return $brand;
    }

    public function getSizeAttribute($value)
    {
        $size = $value;

        if ($size == '') {
            $size = $this->kwikee_size;
        }

        return $size;
    }

    public static function aws_search($store, $keyword)
    {
        $url[1230] = 'https://search-store-1230-yuidmmfr7a53ecxlwa6j2moafm.us-west-1.cloudsearch.amazonaws.com/2013-01-01/search?q='.urlencode($keyword);
        $url[9515] = 'https://search-store-9515-5lzbu7736t3sjqnuvi4a5m5xje.us-west-1.cloudsearch.amazonaws.com/2013-01-01/search?q='.urlencode($keyword);
        $url[3501] = 'https://search-store-3501-ngeuq4bh7ihwwfwusextktqw2u.us-west-1.cloudsearch.amazonaws.com/2013-01-01/search?q='.urlencode($keyword);
        $url[1006] = 'https://search-store-1006-abkghiu3osli562jxhevzs52w4.us-west-1.cloudsearch.amazonaws.com/2013-01-01/search?q='.urlencode($keyword);
        $url[2710] = 'https://search-store-2701-nsp7r26btfm5bmj7pdunqksdhq.us-west-1.cloudsearch.amazonaws.com/2013-01-01/search?q='.urlencode($keyword);
        $url['test'] = 'https://search-aws-test-5frdmdrifkfk4fv6bffljyjd5m.us-west-1.cloudsearch.amazonaws.com/2013-01-01/search?q='.urlencode($keyword);

        $client = new \GuzzleHttp\Client();
        $response = $client->get($url[$store]);
        $answer = $response->getBody()->getContents();
        $products = $answer;

        print_r($products);

        return $products;
    }

    public function scopeSaraSearch($query, $keyword)
    {
        return $query
            ->join('shopping_products', 'products.upc', '=', 'shopping_products.sku')
            ->search($keyword)
            ->select(DB::raw('products.*,products.brand_name as brand_name,products.description as description'));
    }

    public function scopeStoreSearch($query, $store, $keyword)
    {
        $keyword = urldecode($keyword);

        $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
        $keyword = preg_replace($regexEmoticons, '', $keyword);
        $keyword = str_replace(['\'', '"', ',', ';', '<', '>', '*'], ' ', $keyword);

        return $query
            ->join('shopping_products', 'products.upc', '=', 'shopping_products.sku')
            ->where('shopping_products.store_number', $store)
            ->search($keyword)
            ->select(DB::raw('*,products.brand_name as brand_name,products.description as description'));
    }

    public function scopeShopping($query, $store, $category)
    {
        return $query
            ->join('shopping_products', 'products.upc', '=', 'shopping_products.sku')
            ->join('category_product', 'products.id', '=', 'category_product.product_id')
            ->where('shopping_products.store_number', $store)
            ->where('category_product.category_id', $category)
            ->orderBy('shopping_products.movement', 'desc')
            ->distinct('products.upc')
            ->select(DB::raw('*,products.brand_name as brand_name,products.description as description'));
    }

    public function scopeSingle($query, $store, $upc)
    {
        return $query
            ->join('shopping_products', 'products.upc', '=', 'shopping_products.sku')
            ->where('products.upc', $upc)
            ->where('shopping_products.store_number', $store)
            ->groupBy('products.upc')
            ->select(DB::raw('*,products.brand_name as brand_name,products.description as description'));
    }

    public function scopeFavorites($query, $store, $upcs)
    {
        $upcs = explode(',', $upcs);

        $upcs = array_map(function ($item) {
            return str_pad($item, 13, 0, STR_PAD_LEFT);
        }, $upcs);

        return $query
            ->join('shopping_products', 'products.upc', '=', 'shopping_products.sku')
            ->where('shopping_products.store_number', $store)
            ->whereIn('upc', $upcs)
            ->distinct('products.upc')
            ->orderBy('shopping_products.movement', 'desc')
            ->select(DB::raw('*,products.brand_name as brand_name'));
    }
}
