<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WeddingComment extends Model
{
    protected $fillable = [
        'comment',
        'file',
    ];

    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }
}
