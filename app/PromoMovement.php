<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PromoMovement extends Model
{
    protected $fillable = [
        'mmbr_prom_id',
        'strt_date',
        'end_date',
        'prom_desc',
        'prom_amt',
        'prom_qty',
        'dt',
        'store',
    ];
}
