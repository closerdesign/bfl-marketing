<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SignageRequest extends Model
{
    protected $fillable = [

        'name',
        'email',
        'store',
        'event',
        'date_needed',
        'amount',
        'size',
        'size_other',
        'receive',
        'contents',
        'comments',

    ];
}
