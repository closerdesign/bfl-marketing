<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = ['location', 'event', 'event_date', 'sale_date'];
}
