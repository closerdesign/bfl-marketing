<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
    protected $fillable = [

        'brand_id',
        'file',
        'date_from',
        'date_to',

    ];

    /**
     *  Brand Relationship.
     */
    public function brands()
    {
        return $this->belongsTo(\App\Brand::class, 'brand_id', 'id', 'brands');
    }
}
