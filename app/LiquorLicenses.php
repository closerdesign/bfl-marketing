<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class LiquorLicenses extends Model
{
    protected $fillable = [
        'license_number',
        'expiration_date',
        'employee_id',
        'file',
        'file_description',
        'notes',
    ];

    /**
     *  Liqour License File.
     */
    public function getFileAttribute($value)
    {
        return Storage::url('liquor-licenses/'.$value);
    }

    public function employee()
    {
        return $this->belongsTo(\App\Employee::class);
    }
}
