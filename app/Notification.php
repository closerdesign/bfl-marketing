<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = [

        'notification_text',
        'notification_to',
        'send_date_type',
        'send_day',
        'send_time',
        'send_day_month',
        'name',
    ];
}
