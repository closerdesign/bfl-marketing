<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KwikeeImage extends Model
{
    protected $fillable = [
        'product_id',
        'type',
        'url',
    ];
}
