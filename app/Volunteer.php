<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Volunteer extends Model
{
    protected $fillable = [
        'first_name',
        'last_name',
        'phone_number',
        'email',
        'birth_date',
        'is_organization',
        'organization_name',
        'referral',
        'referral_other',
        'training_session',
        'availability',
    ];
}
