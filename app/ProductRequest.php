<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductRequest extends Model
{
    protected $fillable = [
        'brand_id',
        'msisdn',
        'to',
        'messageid',
        'text',
        'type',
        'keyword',
        'message_timestamp',
    ];
}
