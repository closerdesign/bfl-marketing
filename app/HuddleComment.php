<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HuddleComment extends Model
{
    protected $fillable = [

        'huddle_id',
        'name',
        'email',
        'comment',

    ];
}
