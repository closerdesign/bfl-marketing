<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class EmployeeDocument extends Model
{
    protected $fillable = [
        'employee_id',
        'file',
        'file_name',
        'employee_id',
        'notes',

    ];

    public function licenses()
    {
        return $this->belongsTo(\App\Employee::class);
    }

    public function getFileAttribute($value)
    {
        return Storage::url('emp_docs/'.$value);
    }
}
