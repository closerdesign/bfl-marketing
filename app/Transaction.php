<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
        'item_id',
        'store_id',
        'qty',
        'type',
        'approved_by',
        'comments',
    ];

    /**
     *  Item Relationship.
     */
    public function item()
    {
        return $this->belongsTo(Item::class);
    }

    /**
     *  Store Relationship.
     */
    public function store()
    {
        return $this->belongsTo(Store::class);
    }
}
