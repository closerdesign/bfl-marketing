<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class SignageShop extends Model
{
    protected $table = 'signage_shop';

    protected $fillable = [

        'brand_id',
        'name',
        'description',
        'start_date',
        'end_date',
        'cover_image',
        'status',

    ];

    /**
     *  Image Attribute.
     */
    public function getImageAttribute($value)
    {
        return Storage::url('signage-shop/'.$value);
    }

    /**
     *  Return Current Items.
     */
    public function scopeCurrent($query)
    {
        return $query
            ->where('start_date', '<=', date('Y-m-d'))
            ->where('end_date', '>=', date('Y-m-d'))
            ->where('status', 1)
            ->orderBy('start_date', 'asc');
    }
}
