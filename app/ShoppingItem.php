<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class ShoppingItem extends Model
{
    protected $fillable = [
        'upc',
        'brand_name',
        'product_name',
        'description',
        'size',
        'uom',
        'department_id',
        'category_id',
        'price',
        'price_strategy',
        'price_type',
        'start_date',
        'end_date',
        'tax',
        'image',
    ];

    public function departments()
    {
        return $this->belongsTo(\App\Department::class, 'department_id', 'id');
    }

    public function getImageAttribute($value)
    {
        return Storage::url('shopping-items/'.$value);
    }
}
