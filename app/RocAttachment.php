<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class RocAttachment extends Model
{
    protected $fillable = [
        'file',
        'roc_entry_id',
    ];

    /**
     *  MUTATORS.
     */

    /**
     *  File Mutator.
     */
    public function getFileAttribute($value)
    {
        return Storage::url('roc-attachment/'.$value);
    }
}
