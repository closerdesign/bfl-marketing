<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpecialUpc extends Model
{
    protected $fillable = [
        'upc',
        'special_id',
    ];

    /**
     *  Product Relationship.
     */
    public function product()
    {
        return $this->belongsTo(\App\Product::class, 'upc', 'upc', 'products');
    }
}
