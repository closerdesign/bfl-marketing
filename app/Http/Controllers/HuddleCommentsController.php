<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Huddle;
use App\HuddleComment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class HuddleCommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $this->validate($request, [
            'name'    => 'required',
            'email'   => 'required',
            'comment' => 'required',
        ]);

        $comment = new HuddleComment($request->all());

        $comment->huddle_id = $id;

        $comment->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Comment added successfully.',
        ]);

        $huddle = Huddle::findOrFail($id);

        $comments = HuddleComment::where('huddle_id', $id)->orderBy('created_at', 'desc')->get();

        $email_list = [$huddle->store->email];

        foreach ($comments as $comment) {
            array_push($email_list, $comment->email);
        }

        Mail::send('emails.huddles.comments', ['huddle' => $huddle, 'comments' => $comments, 'email_list' => $email_list, 'request' => $request], function ($m) use ($huddle, $comments, $email_list, $request) {
            $m->from('digital@buyforlessok.com', 'BFL Media Team');

            $m->to($email_list)
                ->replyTo('digital@buyforlessok.com')
                ->subject(explode(' ', $request->name)[0].' added a comment to your huddle!');
        });

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
