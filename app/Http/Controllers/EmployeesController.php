<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Mail\EmployeeDiscountRequest;
use App\Store;
use Hocza\Sendy\Facades\Sendy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class EmployeesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['discount_request', 'discount_request_process']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::where('active', 1)->orderBy('name')->get();

        return view('employees.index', compact('employees', 'stores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $stores = Store::all();

        return view('employees.create', compact('stores'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $employee = new Employee($request->all());

        $employee->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Employee has been saved successfully',
        ]);

        return redirect()->action('EmployeesController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::findOrFail($id);

        $stores = Store::all();

        return view('employees.edit', compact('employee', 'stores'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $employee = Employee::findOrFail($id);

        $this->validate($request, [
            'name' => 'required',
        ]);

        $active = $employee->active;

        if ($request->has('active')) {
            $active = $request->active;
        } else {
            $active = 0;
        }

        $employee->fill($request->all());

        $employee->active = $active;

        $employee->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Employee has been updated successfully',
        ]);

        return redirect()->action('EmployeesController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Employee::destroy($id);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Employee removed sucessfully',
        ]);

        return redirect()->action('EmployeesController@index');
    }

    /**
     *  Discount Request.
     */
    public function discount_request()
    {
        return view('employees.discount-request');
    }

    /**
     *  Discount Request Process.
     */
    public function discount_request_process(Request $request)
    {
        $this->validate($request, [
            'clock_in_code' => 'required',
            'loyalty_member_id' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
        ]);

        Sendy::setListId('yplgKd3UIUg9fz65EudSRg')
            ->subscribe([
            'email' => $request->email,
            'loyalty' => $request->loyalty_member_id,
            'phone' => $request->phone,
            'clock_in_code' => $request->clock_in_code,
            'name' => $request->name,
        ]);

        Mail::to('help@buyforlessok.com')
            ->send(new EmployeeDiscountRequest($request));

        Session::flash('message', [
            'type' => 'success',
            'message' => 'Thank you! Your request has been sent.',
        ]);

        return back();
    }
}
