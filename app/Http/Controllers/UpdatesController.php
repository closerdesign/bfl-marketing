<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Update;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class UpdatesController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin', ['except' => 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $updates = Update::orderBy('created_at', 'desc')->get();

        return view('updates.index', compact('updates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('updates.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'       => 'required',
            'description' => 'required',
        ]);

        $update = new Update($request->all());

        $author = Auth::user();

        $update->save();

        foreach (User::all() as $user) {
            Mail::send('emails.updates', ['update' => $update, 'user' => $user, 'author' => $author], function ($m) use ($update, $user, $author) {
                $m->from($author->email, $author->name);

                $m->to($user->email, $user->name)
                    ->replyTo(Auth::user()->email)
                    ->subject('[SARA Platform Update] '.$update->title);
            });
        }

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Updated created successfully',
        ]);

        return redirect()->action('UpdatesController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $update = Update::findOrFail($id);

        return view('updates.show', compact('update'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $update = Update::findOrFail($id);

        return view('updates.edit', compact('update'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'       => 'required',
            'description' => 'required',
        ]);

        $update = Update::findOrFail($id);

        $update->fill($request->all());

        $update->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Update updated successfully!',
        ]);

        return redirect()->action('UpdatesController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Update::destroy($id);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Update removed successfully',
        ]);

        return redirect()->action('UpdatesController@index');
    }

    /**
     *  All updates list.
     */
    public function list()
    {
        return view('updates.list');
    }
}
