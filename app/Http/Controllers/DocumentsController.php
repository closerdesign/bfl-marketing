<?php

namespace App\Http\Controllers;

use App\Document;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Response;

class DocumentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $documents = Document::orderBy('name')->paginate(15);

        return view('documents.index', compact('documents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('documents.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'    => 'required',
            'file'    => 'required|mimes:pdf,doc,docx,xls,xlsx,ppt,pptx|max:5000',
            'status'  => 'required|boolean',
        ]);

        $document = new Document($request->all());

        $filename = uniqid().'.'.$request->file('file')->getClientOriginalExtension();

        Storage::put('documents/'.$filename, file_get_contents($request->file('file')->getRealPath()), 'public');

        $document->file = $filename;

        $document->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Document Saved Successfully!',
        ]);

        return redirect()->action('DocumentsController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $document = Document::findOrFail($id);

        return Response::make(file_get_contents($document->file), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="'.Str::slug($document->name).'"',
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $document = Document::find($id);

        return view('documents.edit', compact('document'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'    => 'required',
            'file'    => 'mimes:pdf,doc,docx,xls,xlsx,ppt,pptx|max:5000',
            'status'  => 'required|boolean',
        ]);

        $document = Document::findOrFail($id);

        $filename = basename($document->file);

        $document->fill($request->all());

        if ($request->hasFile('file')) {
            $filename = uniqid().'.'.$request->file('file')->getClientOriginalExtension();

            Storage::put('documents/'.$filename, file_get_contents($request->file('file')->getRealPath()), 'public');
        }

        $document->file = $filename;

        $document->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Document Updated Successfully!',
        ]);

        return redirect()->action('DocumentsController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Document::destroy($id);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Document Removed Successfully!',
        ]);

        return back();
    }

    /**
     *  Public Resources List.
     */
    public function docs_list()
    {
        $docs = Document::where('status', 1)->orderBy('name')->get();

        return view('documents.docs-list', compact('docs'));
    }
}
