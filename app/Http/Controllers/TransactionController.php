<?php

namespace App\Http\Controllers;

use App\Item;
use App\Store;
use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class TransactionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = Transaction::orderBy('created_at', 'desc')->paginate(100);

        return view('transaction.index', compact('transactions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'item_id' => 'required',
            'store_id' => 'required',
            'qty' => 'required',
            'type' => 'required',
        ]);

        $item = Item::findOrFail($request->item_id);

        $store = Store::findOrFail($request->store_id);

        if (($request->type == 'OUT') && ($request->qty > $item->stock)) {
            Session::flash('message', [
                'type' => 'danger',
                'message' => 'Stock for '.$item->name.' is not enough to fulfill your request.',
            ]);

            return back();
        }

        if ($request->type == 'IN') {
            $stock = $item->stock + $request->qty;
        }

        if ($request->type == 'OUT') {
            $stock = $item->stock - $request->qty;
        }

        $item->stock = $stock;

        $item->save();

        $transaction = new Transaction($request->all());

        $transaction->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Transaction has been processed successfully.',
        ]);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        //
    }
}
