<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\ShrinkGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ShrinkGroupsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $group = new ShrinkGroup($request->all());

        $group->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Shrink Group created successfully',
        ]);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $group = ShrinkGroup::findOrFail($id);

        return view('shrink-groups.edit', compact('group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $group = ShrinkGroup::findOrFail($id);

        $group->fill($request->all());

        $group->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Shrink Group updated successfully',
        ]);

        return redirect()->action('ShrinksController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ShrinkGroup::destroy($id);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Shrink Group Removed Successfully',
        ]);

        return back();
    }
}
