<?php

namespace App\Http\Controllers;

use App\Campaign;
use App\CampaignCategory;
use App\CampaignItem;
use App\Http\Requests;
use Illuminate\Auth\Access\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class CampaignCategoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = CampaignCategory::orderBy('position')->get();

        return view('campaign_categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('campaign_categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'        => 'required',
            'image'       => 'image|max:1000',
            'status'      => 'required|boolean',
            'campaign_id' => 'required',
        ]);

        $filename = null;

        if ($request->has('image')) {
            $filename = uniqid().'.'.$request->file('image')->getClientOriginalExtension();

            Storage::put('campaign-categories/'.$filename, file_get_contents($request->file('image')->getRealPath()), 'public');
        }

        $category = new CampaignCategory($request->all());

        $category->image = $filename;

        $category->save();

        $category->campaigns()->attach($request->campaign_id);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Campaign Category created successfully!',
        ]);

        return redirect()->action('CampaignCategoriesController@index');
    }

    /**
     * @param $id
     * @return string
     */
    public function show($id)
    {
        return 'Ok';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = CampaignCategory::findOrFail($id);

        return view('campaign_categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'        => 'required',
            'image'       => 'image|max:1000',
            'status'      => 'required|boolean',
            'campaign_id' => 'required',
        ]);

        $category = CampaignCategory::findOrFail($id);
        $filename = $category->image;

        if ($request->hasFile('image')) {
            $filename = uniqid().'.'.$request->file('image')->getClientOriginalExtension();

            Storage::put('campaign-categories/'.$filename, file_get_contents($request->file('image')->getRealPath()), 'public');
        }

        $category->fill($request->all());

        $category->image = basename($filename);

        $category->is_sides_category = 0;

        if ($request->has('is_sides_category')) {
            $category->is_sides_category = $request->is_sides_category;
        }

        $category->save();

        $category->campaigns()->sync($request->campaign_id);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Campaign category updated successfully!',
        ]);

        return redirect()->action('CampaignCategoriesController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CampaignCategory::destroy($id);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Category removed successfully',
        ]);

        return back();
    }

    /**
     *  Sort Categories.
     */
    public function sort(Request $request)
    {
        parse_str($request->data, $data);

        $order = 1;

        $response = '';

        foreach ($data['item'] as $item) {
            $campaign = CampaignCategory::findOrFail($item);

            $campaign->position = $order;

            $campaign->save();

            $response .= $item.': '.$order.', ';

            $order++;
        }

        return $response;
    }
}
