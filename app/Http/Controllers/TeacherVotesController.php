<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\TeacherVote;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class TeacherVotesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->input();

        $vote = new TeacherVote($input);

        Mail::send('emails.teacher-vote', ['vote' => $vote], function ($m) use ($vote) {
            $m->from('digital@buyforlessok.com', 'BFL Media (Do Not Reply)');

            $m->to($vote->email)
                ->subject('Thanks for your vote!');
        });

        $all = TeacherVote::all();

        $found = false;

        $current = date('Y-m');

        foreach ($all as $i) {
            if ($i->email === $vote->email) {
                $createDate = new DateTime($i->created_at);
                $strip = $createDate->format('Y-m');

                if ($strip === $current) {
                    $found = true;

                    break;
                }
            }
        }

        if ($found === false) {
            $vote->save();
        }

        return response('success', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
