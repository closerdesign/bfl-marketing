<?php

namespace App\Http\Controllers;

use App\Mail\RewardITNotification;
use App\Reward;
use DivArt\ShortLink\Facades\ShortLink;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Shivella\Bitly\Facade\Bitly;

class RewardsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rewards = Reward::all();

        return view('rewards.index', compact('rewards'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('rewards.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'image' => 'image|max:1000',
            'brand_id' => 'required|integer',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'status' => 'required|boolean',
        ]);

        $reward = new Reward($request->all());

        $reward->plu_required = ($request->plu_required == 1) ? true : false;

        if ($request->hasFile('image')) {
            $filename = uniqid().'.'.$request->file('image')->getClientOriginalExtension();

            Storage::put('rewards/'.$filename, file_get_contents($request->file('image')->getRealPath()), 'public');

            $reward->image = $filename;
        }

        if ($reward->plu_required == 1) {
            Mail::to('it@buyforlessok.com')->send(new RewardITNotification($reward));
        }

        $reward->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Reward created successfully!',
        ]);

        return redirect()->action('RewardsController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $reward = Reward::valid()->findOrFail($id);

        return view('rewards.show', compact('reward'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reward = Reward::findOrFail($id);

        return view('rewards.edit', compact('reward'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'image' => 'image|max:1000',
            'brand_id' => 'required|integer',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'status' => 'required|boolean',
        ]);

        $reward = Reward::findOrFail($id);

        $reward->plu_required = ($request->plu_required == 1) ? true : false;

        $filename = basename($reward->image);

        $reward->fill($request->all());

        if ($request->hasFile('image')) {
            $filename = uniqid().'.'.$request->file('image')->getClientOriginalExtension();

            Storage::put('rewards/'.$filename, file_get_contents($request->file('image')->getRealPath()), 'public');
        }

        $reward->image = $filename;

        if ($reward->plu_required == 1 && $reward->plu < 1) {
            Mail::to('it@buyforlessok.com')->send(new RewardITNotification($reward));
        }

        $reward->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Reward updated successfully!',
        ]);

        return redirect()->action('RewardsController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Reward::destroy($id);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Reward Removed Successfully!',
        ]);

        return back();
    }
}
