<?php

namespace App\Http\Controllers;

use App\Gallery;
use App\GalleryCategories;
use App\GalleryImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class GalleryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['getGalleryDisplay', 'billboard']]);
    }

    public function index()
    {
        $galleries = Gallery::orderBy('gallery_name', 'desc')->paginate(15);
        $galleryImage = $this->getgallery();

        return view('gallery.index', compact('galleries', 'galleryImage'));
    }

    public function gallerystore(Request $request)
    {
        $this->validate($request, [
            'gallery_name' => 'required',
          ]);

        $galleryName = new Gallery($request->all());

        $galleryName->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Category saved successfully',
        ]);

        return back();
    }

    public function imagepost(Request $request)
    {
        $this->validate($request, [
                'file'          => 'required',
                'link_url'      => 'required',
                'start_date'    => 'required',
                'end_date'      => 'required',
                'display_order' => 'required',
            ]);

        $image = new GalleryImage($request->all());

        $filename = uniqid().'.'.$request->file('file')->getClientOriginalExtension();

        if ($request->mobile_file != '') {
            $mobile_filename = uniqid().'.'.$request->file('mobile_file')->getClientOriginalExtension();

            Storage::put('gallery_images/'.$mobile_filename, file_get_contents($request->file('mobile_file')->getRealPath()), 'public');

            $image->mobile_file = $mobile_filename;
        }

        Storage::put('gallery_images/'.$filename, file_get_contents($request->file('file')->getRealPath()), 'public');

        $image->file = $filename;

        $image->save();

        Session::flash('message', [
                'type'    => 'success',
                'message' => 'Image has been added successfully.',
            ]);

        return back();
    }

    public function getgallery($gallery_id = '')
    {
        if (! $gallery_id) {
            $gallery_id = 1;
        }

        $gallery = GalleryImage::where('gallery_id', $gallery_id)->get();

        return $gallery;
    }

    public function pullgallery(Request $request)
    {
        $galleryImage = GalleryImage::where('gallery_id', $request->gallery_id)->get();

        $galleries = Gallery::all();
        $thisGallery = Gallery::findOrFail($request->gallery_id);

        return view('gallery.index', compact('galleries', 'galleryImage', 'thisGallery'));
    }

    public function imagedelete($id)
    {
        $galleryImage = GalleryImage::findOrFail($id);
        $galleryImage->destroy($id);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Image has been successfully deleted.',
        ]);

        return back();
    }

    public function imageedit($id)
    {
        $image = GalleryImage::findOrFail($id);
        $galleries = Gallery::all();
        $galleryImage = $this->getgallery();

        return view('gallery.image-edit', compact('galleries', 'galleryImage', 'image'));
    }

    public function imageupdate(Request $request, $id)
    {
        $this->validate($request, [
            'gallery_id'    => 'required',
            'link_url'      => 'required',
            'start_date'    => 'required',
            'end_date'      => 'required',
            'display_order' => 'required',
        ]);

        $image = GalleryImage::findOrFail($id);

        $image->fill($request->all());
        $image->save();

        Session::flash('message', ['type' => 'success', 'message' => 'Updated Successfully']);

        return redirect()->action('GalleryController@show', $image->gallery_id);
    }

    public function getGalleryDisplay($id)
    {
        $date = date('Y-m-d');

        $gallery = GalleryImage::where('gallery_id', $id)
            ->where('start_date', '<=', $date)->where('end_date', '>=', $date)
            ->orderBy('display_order')
            ->get();

        if (isset($gallery)) {
            return response($gallery, 200);
        }
    }

    public function show($id)
    {
        $gallery = Gallery::findOrFail($id);

        $images = GalleryImage::where('gallery_id', $id)->get();

        return view('gallery.edit', compact('gallery', 'images'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'  => 'required',
        ]);

        $gallery = Gallery::findOrFail($id);

        $gallery->gallery_name = $request->name;

        $gallery->save();

        Session::flash('message', [
            'type' => 'success',
            'message' => 'Gallery has been updated.',
        ]);

        return back();
    }

    public function destroy($id)
    {
        $gallery = Gallery::findOrFail($id);

        $gallery->destroy($id);

        Session::flash('message', [
            'type' => 'success',
            'message' => 'Gallery has been successfully deleted.',
        ]);

        return back();
    }

    /**
     *  Billboard.
     */
    public function billboard($id)
    {
        $date = date('Y-m-d');

        $gallery = GalleryImage::where('gallery_id', $id)
            ->where('start_date', '<=', $date)->where('end_date', '>=', $date)
            ->orderBy('display_order')
            ->get();

        if (isset($_GET['api'])) {
            return response($gallery, 200);
        }

        return view('gallery.billboard', compact('gallery'));
    }
}
