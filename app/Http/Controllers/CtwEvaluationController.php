<?php

namespace App\Http\Controllers;

use App\CtwArea;
use App\CtwEvaluation;
use App\CtwScore;
use App\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class CtwEvaluationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $evaluations = CtwEvaluation::paginate(10);

        return view('ctw-evaluation.index', compact('evaluations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ctw-evaluation.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'store_id' => 'required|integer',
        ]);

        $evaluation = new CtwEvaluation(request()->all());

        $evaluation->save();

        return redirect()
            ->route('ctw-evaluation.edit', $evaluation->id)
            ->with(['message' => [
                'type'    => 'success',
                'message' => Lang::get('ctw-evaluation.title').': '.Lang::get('general.saved'),
            ]]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CtwEvaluation  $ctwEvaluation
     * @return \Illuminate\Http\Response
     */
    public function show(CtwEvaluation $ctwEvaluation)
    {
        $evaluation = $ctwEvaluation;

        return view('ctw-evaluation.show', compact('evaluation'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CtwEvaluation  $ctwEvaluation
     * @return \Illuminate\Http\Response
     */
    public function edit(CtwEvaluation $ctwEvaluation)
    {
        $evaluation = $ctwEvaluation;

        return view('ctw-evaluation.edit', compact('evaluation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CtwEvaluation  $ctwEvaluation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CtwEvaluation $ctwEvaluation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CtwEvaluation  $ctwEvaluation
     * @return \Illuminate\Http\Response
     */
    public function destroy(CtwEvaluation $ctwEvaluation)
    {
        //
    }

    /**
     *  Mark As Completed.
     */
    public function mark_as_completed($id)
    {
        $evaluation = CtwEvaluation::find($id);

        if ($evaluation->scores->count() == 0) {
            return back()
                ->with([
                    'message' => [
                        'type' => 'danger',
                        'message' => Lang::get('ctw-evaluation.evaluation-has-no-scores'),
                    ],
                ]);
        }

        $evaluation->status = 1;

        $evaluation->save();

        return redirect()
            ->route('ctw-evaluation.show', $evaluation->id)
            ->with(['message' => [
                'type'    => 'success',
                'message' => Lang::get('ctw-evaluation.evaluation-has-been-marked-as-completed'),
            ]]);
    }

    /**
     *  Heatmap.
     */
    public function heatmap()
    {
        $areas = CtwArea::orderBy('name')->get();

        $stores = Store::active()->get();

        $scores = DB::table('ctw_scores')
            ->join('ctw_topics', 'ctw_scores.topic_id', '=', 'ctw_topics.id')
            ->join('ctw_evaluations', 'ctw_evaluations.id', '=', 'ctw_scores.evaluation_id')
            ->join('ctw_areas', 'ctw_topics.ctw_area_id', '=', 'ctw_areas.id')
            ->join('stores', 'ctw_evaluations.store_id', '=', 'stores.id')
            ->select(
                DB::raw('ctw_scores.id as score_id'),
                DB::raw('stores.store_code as store'),
                DB::raw('ctw_evaluations.id as evaluation'),
                DB::raw('ctw_areas.id as area'),
                DB::raw('ctw_areas.name as area_name'),
                DB::raw('ctw_topics.id as topic'),
                DB::raw('ctw_topics.topic as topic_name'),
                'score'
            )
            ->orderBy('ctw_evaluations.created_at', 'DESC')
            ->get();

//        return $scores;

        return view('ctw-evaluation.heatmap', compact('areas', 'stores', 'scores'));
    }
}
