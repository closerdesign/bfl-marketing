<?php

namespace App\Http\Controllers;

use App\Ad;
use App\Brand;
use App\Charts\TextSubscribers;
use App\DonationRequest;
use App\Event;
use App\Http\Requests;
use App\Mail\StoreLevelConfirm;
use App\Mail\StoreLevelEmail;
use App\Roc;
use App\RocEntry;
use App\Special;
use App\Store;
use Colors\RandomColor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class StoreLevelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['deal_of_the_week_process', 'four_ups_process', 'info_sign_process', 'kraft_promo_process']]);
    }

    public function home()
    {
        $rocEntries = RocEntry::committed()
            ->whereRaw('FIND_IN_SET('.auth()->user()->store_data->id.',stores)')
            ->paginate(10);

        $ads = Ad::coming()
            ->where('brand_id', auth()->user()->store_data->brands->id)
            ->get();

        return view('store-level.home', compact('rocEntries', 'ads'));
    }

    /**
     *  Ads.
     */
    public function ads()
    {
        $brand = Store::where('store_code', Auth::user()->store)->firstOrFail();

        $ads = Ad::where(function ($q) {
            $q->where('date_from', '>=', date('Y-m-d', strtotime('today -7 days')))
                ->where('date_to', '>=', date('Y-m-d'));
        })
            ->where('brand_id', $brand->brand_id)
            ->orderBy('date_from')
            ->get();

        return view('store-level.ads', compact('ads'));
    }

    /**
     * Specials.
     */
    public function specials()
    {
        if (isset(Auth::user()->store)) {
            $brand = Store::where('store_code', Auth::user()->store)->firstOrFail();
        }

        $ads = Ad::where('date_from', '>=', date('Y-m-d', strtotime('today -2 days')))
                 ->where('date_from', '<=', date('Y-m-d', strtotime('today +3 days')));

        if (isset($brand)) {
            $ads = $ads->where('brand_id', $brand->brand_id);
        }

        $ads = $ads->whereIn('name', ['Subscriber Special', 'Subscriber Specials'])
            ->orderBy('date_from')
            ->get();

        $ava = false;

        return view('store-level.specials', compact('ads', 'ava'));
    }

    public function special($id)
    {
        $ad = Ad::findOrFail($id);

        $specials = Special::where('ad_id', $id)->where('featured', 1)->get();

        $lang = 'en';

        if ($ad->brand_id == 4) {
            $lang = 'es';
        }

        return view('emails.weekends', compact('ad', 'specials', 'lang'));
    }

    /**
     * Background Check Request Form.
     */
    public function bg_check_request()
    {
        return view('store-level.bg-check');
    }

    /**
     * Orientation Attendee Form.
     */
    public function orientation_attendee()
    {
        return view('store-level.orientation');
    }

    /**
     *  Signage Request.
     */
    public function signage_request()
    {
        return view('signage.index');
    }

    /**
     *  Termination Request.
     */
    public function termination_request()
    {
        return view('termination.create');
    }

    /**
     * Vacation in Lieu Request Form.
     */
    public function vacation_request()
    {
        return view('store-level.vacation-request');
    }

    /**
     *  Combined Backgrond / Orientation Form.
     */
    public function bg_orientation_request()
    {
        return view('store-level.bg-orientation');
    }

    /**
     * Cash Over / Short Report Form.
     */
    public function cash_report()
    {
        return view('store-level.cash-report');
    }

    /**
     *  Liquor Licenses.
     */
    public function liquor_licenses()
    {
        $stores = Store::orderby('name')->get();

        return view('store-level.liquor-licenses', compact('stores'));
    }

    /**
     *  Forms Processing.
     */
    public function form(Request $request)
    {
        $this->validate($request, [
            'form_name' => 'required',
        ]);

        $to = 'tmed@buyforlessok.com';

        if ($request->form_name == 'Cash Over / Short Report') {
            $to = ['jroby@buyforlessok.com', 'tmiller@buyforlessok.com'];
        } elseif ($request->form_name == 'Vacation in Lieu') {
            $to = 'payroll@buyforlessok.com';
        } elseif ($request->form_name == 'Background Check / Orientation Attendee') {
            $to = 'jobs@buyforlessok.com';
        }

        Mail::to($to)->send(new StoreLevelEmail($request));

        Mail::to(Auth::user()->email)->send(new StoreLevelConfirm($request));

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Great! Your request has been sent successfully!',
        ]);

        return back();
    }

    /**
     *  Deal Of The Week.
     */
    public function deal_of_the_week()
    {
        return view('store-level.deal-of-the-week');
    }

    public function deal_of_the_week_process()
    {
        return view('store-level.deal-of-the-week-template');
    }

    public function four_ups_process()
    {
        return view('store-level.four-ups-template');
    }

    public function deal_of_the_week_pdf(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required',
            'brand_id' => 'required',
            'color' => 'required',
            'type' => 'required',
            'background' => 'required',
        ]);

        if ($request->type === 'Poster') {
            $source = action('StoreLevelController@deal_of_the_week_process',
                [
                    'name' => $request->name,
                    'price' => $request->price,
                    'unit_of_measure' => $request->unit_of_measure,
                    'brand' => $request->brand_id,
                    'background' => $request->background,
                    'fineprint' => $request->fineprint,
                    'color' => $request->color,
                ]
            );

            $landscape = false;

            $curl = curl_init();

            curl_setopt_array($curl, [
                CURLOPT_URL => 'https://api.pdfshift.io/v2/convert/',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => json_encode(['source' => $source, 'landscape' => $landscape, 'use_print' => false, 'format' => 'Tabloid']),
                CURLOPT_HTTPHEADER => ['Content-Type:application/json'],
                CURLOPT_USERPWD => '20c353e4955c4ad68c79d099663ee94f:',
            ]);

            $response = curl_exec($curl);

            $filename = uniqid().'.pdf';

            return \Illuminate\Support\Facades\Response::make($response, 200, [
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; filename="'.$filename.'"',
            ]);
        } else {
            $source = action('StoreLevelController@four_ups_process',
                [
                    'name' => $request->name,
                    'price' => $request->price,
                    'unit_of_measure' => $request->unit_of_measure,
                    'brand' => $request->brand_id,
                    'background' => $request->background,
                    'fineprint' => $request->fineprint,
                    'color' => $request->color,
                ]
            );

            $landscape = true;

            $curl = curl_init();

            curl_setopt_array($curl, [
                CURLOPT_URL => 'https://api.pdfshift.io/v2/convert/',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => json_encode(['source' => $source, 'landscape' => $landscape, 'use_print' => false, 'format' => 'Letter']),
                CURLOPT_HTTPHEADER => ['Content-Type:application/json'],
                CURLOPT_USERPWD => '20c353e4955c4ad68c79d099663ee94f:',
            ]);

            $response = curl_exec($curl);

            $filename = uniqid().'.pdf';

            return \Illuminate\Support\Facades\Response::make($response, 200, [
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; filename="'.$filename.'"',
            ]);
        }
    }

    /**
     * Informational Signage.
     */
    public function info_signs()
    {
        return view('store-level.info-signs');
    }

    public function info_sign_process()
    {
        return view('store-level.info-sign-template');
    }

    public function info_sign_pdf(Request $request)
    {
        $this->validate($request, [
            'first_line' => 'required',
            'type' => 'required',
            'brand' => 'required',
        ]);

        if ($request->landscape !== '1') {
            $request->landscape = '0';
        }

        $source = action('StoreLevelController@info_sign_process',
            [
                'first_line' => $request->first_line,
                'second_line' => $request->second_line,
                'type' => $request->type,
                'brand' => $request->brand,
                'landscape' => $request->landscape,
            ]
        );

        $landscape = false;

        if ($request->landscape == '1') {
            $landscape = true;
        }

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => 'https://api.pdfshift.io/v2/convert/',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => json_encode(['source' => $source, 'landscape' => $landscape, 'use_print' => false, 'format' => 'Letter']),
            CURLOPT_HTTPHEADER => ['Content-Type:application/json'],
            CURLOPT_USERPWD => '20c353e4955c4ad68c79d099663ee94f:',
        ]);

        $response = curl_exec($curl);

        $filename = uniqid().'.pdf';

        return \Illuminate\Support\Facades\Response::make($response, 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="'.$filename.'"',
        ]);
    }

    /**
     * Kraft Promos.
     */
    public function kraft_promos()
    {
        return view('store-level.kraft-promos');
    }

    public function kraft_promo_process()
    {
        return view('store-level.kraft-4up-template');
    }

    public function kraft_promo_pdf(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'size' => 'required',
            'price' => 'required',
            'minus' => 'required',
            'total' => 'required',
            'image' => 'image|max:1000',
        ]);

        if ($request->hasFile('image')) {
            $img = basename($request->image);
            $img = uniqid().'.'.$request->file('image')->getClientOriginalExtension();
            Storage::put('kraft-promos/'.basename($img), file_get_contents($request->file('image')->getRealPath()), 'public');
            $imgurl = 'https://bfl-corp-sara.s3.us-west-2.amazonaws.com/kraft-promos/'.$img;
        } else {
            $imgurl = $request->img_url;
        }

        $source = action('StoreLevelController@kraft_promo_process',
            [
                'name' => $request->name,
                'size' => $request->size,
                'price' => $request->price,
                'minus' => $request->minus,
                'total' => $request->total,
                'image' => $imgurl,
            ]
        );

        $landscape = true;

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => 'https://api.pdfshift.io/v2/convert/',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => json_encode(['source' => $source, 'landscape' => $landscape, 'use_print' => false, 'format' => 'Letter']),
            CURLOPT_HTTPHEADER => ['Content-Type:application/json'],
            CURLOPT_USERPWD => '20c353e4955c4ad68c79d099663ee94f:',
        ]);

        $response = curl_exec($curl);

        $filename = uniqid().'.pdf';

        return \Illuminate\Support\Facades\Response::make($response, 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="'.$filename.'"',
        ]);
    }
}
