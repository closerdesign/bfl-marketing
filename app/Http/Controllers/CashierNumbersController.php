<?php

namespace App\Http\Controllers;

use App\CashierNumbers;
use App\Employee;
use App\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class CashierNumbersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $this->validate($request, [
            'cashier_number'  => 'required|integer',
            'store_id' => 'required',
        ],
        [
            'cashier_number.integer' => 'Cashier Number must be a valid number (No spaces, letters or special characters)',
        ]
        );

        $employee = Employee::findOrFail($id);

        $cashiernumbers = new CashierNumbers($request->all());
        $cashiernumbers->employee_id = $id;

        $cashiernumbers->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Cashier number has been added successfully.',
        ]);

        return redirect()->action('EmployeesController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cashiernumbers = CashierNumbers::findOrFail($id);
        $cashiernumbers->delete();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Cashier number has been successfully deleted.',
        ]);

        return back();
    }

    private function validatePrimaryStore($storeSet, $id)
    {
        if ($storeSet == 1) {
            $hasPrimary = CashierNumbers::where(['employee_id' => $id, 'primary_store' => 1]);
            if (! empty($hasPrimary)) {
                //die(print_r($hasPrimary));
                return true;
            } else {
                return false;
            }
        }
    }
}
