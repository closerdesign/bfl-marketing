<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Training;
use App\TrainingSlide;
use App\TrainingUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class TrainingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show', 'start', 'complete']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $trainings = Training::paginate(15);

        return view('training.index', compact('trainings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('training.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'category' => 'required',
        ]);

        $training = new Training($request->all());

        $training->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Training created successfully.',
        ]);

        return redirect()->action('TrainingController@edit', $training->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $training = Training::findOrFail($id);

        $slides = TrainingSlide::where('training_id', $id)->orderBy('position')->paginate(1);

        return view('training.show', compact('training', 'slides'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $training = Training::findOrFail($id);

        $slides = DB::table('training_slides')->where('training_id', $id)->orderBy('position')->get();

        return view('training.edit', compact(['training', 'slides']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'category' => 'required',
        ]);

        $training = Training::findOrFail($id);

        $training->fill($request->all());

        $training->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Training updated successfully.',
        ]);

        return redirect()->action('TrainingController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $training = Training::findOrFail($id);

        $training->destroy($id);

        Session::flash('message', [
            'type' => 'success',
            'message' => 'Training has been successfully deleted.',
        ]);

        return back();
    }

    /**
     * Start Training.
     */
    public function start(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'store' => 'required',
            'department' => 'required',
        ]);

        $user = new TrainingUser($request->all());

        $user->training_id = $id;

        $user->start = 1;

        $user->save();

        Session::put('training', [
            'id' => $id,
            'user' => $user->id,
        ]);

        return back();
    }

    /**
     * Complete Training.
     */
    public function complete()
    {
        $training = Session::get('training');

        $user = TrainingUser::findOrFail($training['user']);

        $user->complete = 1;

        $user->save();

        Session::forget('training');

        Session::flash('message', [
            'type' => 'success',
            'message' => 'Training has been completed! Thank you!',
        ]);

        return back();
    }

    /**
     * Users Page.
     */
    public function users($id)
    {
        $training = Training::findOrFail($id);

        $users = DB::table('training_users')->where('training_id', $id)->paginate(25);

        return view('training.users', compact('training', 'users'));
    }
}
