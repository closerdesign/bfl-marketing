<?php

namespace App\Http\Controllers;

use App\Ad;
use App\Adfile;
use App\Adpage;
use App\DeliAd;
use App\Exports\SpecialsExport;
use App\GroceryAd;
use App\HbcAd;
use App\Mail\PricePointsNotification;
use App\MeatAd;
use App\ProduceAd;
use App\Special;
use App\Store;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Jelovac\Bitly4laravel\Facades\Bitly4laravel;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use Torann\LaravelMetaTags\Facades\MetaTag;

class AdsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['current', 'current_api', 'pages', 'ad', 'email', 'pdf_template', 'generate_pdf', 'poster', 'poster_pdf']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stores = Store::all();

        $ads = Ad::where('date_to', '>=', date('Y-m-d'))
            ->orderBy('date_from', 'desc')
            ->paginate(25);

        $previous = 0;

        return view('ads.index', compact('stores', 'ads', 'previous'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ads.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'      => 'required',
            'brand_id'     => 'required|numeric',
            'date_from' => 'required|date',
            'date_to'   => 'required|date',
            'websites'  => 'boolean',
        ]);

        $ad = new Ad($request->all());

        $ad->save();

        $stores = Store::where('brand_id', request()->brand_id)->select('id')->pluck('id')->toArray();

        $ad->stores()->attach($stores);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Ad created successfully',
        ]);

        return redirect()->action('AdsController@edit', $ad->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ad = Ad::findOrFail($id);

        $groceries = GroceryAd::where('ad_id', $id)->orderBy('ordering', 'DESC')->get();

        $produces = ProduceAd::where('ad_id', $id)->orderBy('ordering', 'DESC')->get();

        $meats = MeatAd::where('ad_id', $id)->orderBy('ordering', 'DESC')->get();

        $hbcs = HbcAd::where('ad_id', $id)->orderBy('ordering', 'DESC')->get();

        $delis = DeliAd::where('ad_id', $id)->orderBy('ordering', 'DESC')->get();

        return view('ads.show', compact('ad', 'groceries', 'produces', 'meats', 'hbcs', 'delis'));
    }

    /**
     * Display current resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function current($brand)
    {
        $currents = DB::table('ads')
            ->where('brand_id', '=', $brand)
            ->where('date_from', '<=', date('Y-m-d'))
            ->where('date_to', '>=', date('Y-m-d'))
            ->orderBy('created_at', 'DESC')
            ->select('*')
            ->get();

        foreach ($currents as $current) {
            $cover = Adpage::findOrFail($current->id)->where('ad', $current->id)->first();

            $current->cover = $cover->image;
        }

        return view('ads.current', compact('currents'));
    }

    /**
     * Display current resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function current_api($brand)
    {
        $currents = DB::table('ads')
            ->where('brand_id', '=', $brand)
            ->where('ads.date_from', '<=', date('Y-m-d'))
            ->where('ads.date_to', '>=', date('Y-m-d'))
            ->where('status', 1)
            ->where('websites', 1)
            ->orderBy('created_at', 'DESC')
            ->select('*')
            ->get();

        foreach ($currents as $current) {
            $products = Special::products($current->id);

            $current->products = $products;

            $featured = Special::featured($current->id);

            $current->featured = $featured;

            $pages = Adpage::where('ad', $current->id)->get();

            $current->pages = $pages;

            $downloads = Adfile::where('ad', $current->id)->get();

            $current->downloads = $downloads;
        }

        return response($currents, 200);
    }

    /**
     * Display pages of current resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function pages($ad)
    {
        $info = Ad::findOrFail($ad);

        $pages = Adpage::where('ad', $ad)->get();

        $no_page = 0;

        foreach ($pages as $page) {
            $no_page += 1;

            $page->number = $no_page;
        }

        $files = Adfile::where('ad', $ad)->get();

        $brand = $info->brand_id;

        return view('ads.pages', compact('info', 'pages', 'files', 'brand'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ad = Ad::findOrFail($id);

        $departments = DB::table('department_special')
            ->join('specials', 'specials.id', '=', 'special_id')
            ->join('ads', 'ads.id', '=', 'specials.ad_id')
            ->join('departments', 'departments.id', '=', 'department_special.department_id')
            ->where('specials.ad_id', $id)
            ->select('departments.*')
            ->groupBy('departments.id')
            ->get();

        MetaTag::set('title', $ad->brands->name.' - '.$ad->name.' - '.$ad->date_from.' to '.$ad->date_to);

        return view('ads.edit', compact('ad', 'departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'         => 'required',
            'brand_id'     => 'required|numeric',
            'date_from'    => 'required|date',
            'date_to'      => 'required|date',
            'websites'     => 'boolean',
            'header_image' => 'image|max:1000',
            'dotw_image  ' => 'image|max:1000',
        ]);

        $ad = Ad::findOrFail($id);

        // If the ad is about to be approved but has no pages nor files, approval will be rejected.
        if ($ad->status == 0 && request()->status == 1 && ($ad->pages->count() == 0 && $ad->files->count() == 0)) {
            return back()
                ->with([
                    'message' => [
                        'type' => 'danger',
                        'message' => 'Ads with no pages nor files won\'t be approved. Please revise.',
                    ],
                ]);
        }

        $ad->header_image = basename($ad->header_image);
        $ad->dotw_image = basename($ad->dotw_image);

        $status = $ad->status;

        $ad->fill($request->all());

        if ($request->status != 1) {
            $ad->status = 0;
        }

        if ($request->has('websites')) {
            $websites = 1;
        } else {
            $websites = 0;
        }

        $ad->websites = $websites;

        if ($request->hasFile('header_image')) {
            $ad->header_image = uniqid().'.'.$request->file('header_image')->getClientOriginalExtension();

            Storage::put('ads/header-images/'.basename($ad->header_image), file_get_contents($request->file('header_image')->getRealPath()), 'public');
        }

        if ($request->hasFile('dotw_image')) {
            $ad->dotw_image = uniqid().'.'.$request->file('dotw_image')->getClientOriginalExtension();

            Storage::put('ads/dotw-images/'.basename($ad->dotw_image), file_get_contents($request->file('dotw_image')->getRealPath()), 'public');
        }

        $ad->stores()->sync(request()->stores);

        $ad->save();

        if (($status == 0) && (request()->status == 1)) {
            Mail::to('pricepoints@buyforlessok.com')
                ->send(new PricePointsNotification($ad));
        }

        Session::flash('message', [
            'type' => 'success',
            'message' => 'Ad updated successfully',
        ]);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Ad::destroy($id);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Ad deleted successfully',
        ]);

        return back();
    }

    /**
     * Display Banners.
     *
     * return \Illuminate\Http\Response
     */
    public function banners($id)
    {
        $specials = Special::where('ad_id', $id)->where('featured', 1)->where('status', 1)->get();

        $formats = [
            'B300x250',
            'B600x600',
            'B728x90',
            'B160x600',
            'B320x50',

        ];

        return view('ads.banners', compact('specials', 'formats'));
    }

    /**
     *  Specials Email.
     */
    public function email($id)
    {
        $ad = Ad::findOrFail($id);

        $specials = Special::where('ad_id', $id)->where('featured', 1)->get();

        $lang = 'en';

        if ($ad->brand_id == 4) {
            $lang = 'es';
        }

        return view('emails.weekends', compact('ad', 'specials', 'lang'));
    }

    /**
     * Weekly Ad Email.
     */
    public function ad_email($id)
    {
        $ad = Ad::findOrFail($id);

        return view('ads.weekly-ad-email', compact('ad'));
    }

    public function ad_text($id)
    {
        $ad = Ad::findOrFail($id);

        $url = URL::action('AdsController@email', [$id]);
        $url = Bitly4laravel::shorten($url);
        $url = $url->data->url;

        $list = [
            '1' => 'Buy For Less',
            '2' => 'Uptown',
            '3' => 'SmartSaver',
            '4' => 'Supermercado',
        ];
        $message = $list[$ad->brand_id].' A brand new exclusive offer is waiting for you: '.$url;
        if ($list[$ad->brand_id] == 'Supermercado') {
            $message = $list[$ad->brand_id].' Una nueva oferta exclusiva espera por ti: '.$url;
        }

        $group = $list[$ad->brand_id];

        /* prepare data for sending */
        $data = [
            'User'          => 'buyforlessok', /* change to your EZ Texting username */
            'Password'      => 'C@mbiar123ezte', /* change to your EZ Texting password */
          //  "PhoneNumbers"  => array($to),
            'Groups'        => [$group],
            'Subject'       => '',
            'Message'       => $message,
            'MessageTypeID' => 1,
        ];

        /* send message */
        $curl = curl_init('https://app.eztexting.com/sending/messages?format=json');
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($curl);
        curl_close($curl);

        /* parse result of API call */
        $json = json_decode($response);

        switch ($json->Response->Code) {
            case 201:
                Session::flash('message', [
                    'type'    => 'success',
                    'message' => 'Message Sent',
                ]);
              //  exit("Message Sent");
                break;
            case 401:
                Session::flash('message', [
                    'type'    => 'success',
                    'message' => 'Invalid user or password',
                ]);
             //   exit("Invalid user or password");
            case 403:
                $errors = $json->Response->Errors;
                Session::flash('message', [
                    'type'    => 'success',
                    'message' => 'The following errors occurred: '.implode('; ', $errors),
                ]);
            //    exit("The following errors occurred: " . implode('; ', $errors));
            case 500:
                Session::flash('message', [
                    'type'    => 'success',
                    'message' => 'Invalid user or password',
                ]);
               // exit("Service Temporarily Unavailable");
        }

        return back();
    }

    public function pdfshift($id)
    {
        $params = [
            'sandbox' => true,
            'format'  => 'tabloid',
            'source'  => 'https://sara.buyforlessok.com/ads/ad/'.$id,
        ];

        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => 'https://api.pdfshift.io/v2/convert/',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => json_encode($params),
            CURLOPT_HTTPHEADER => ['Content-Type:application/json'],
            CURLOPT_USERPWD => '20c353e4955c4ad68c79d099663ee94f'.':',
        ]);
        $response = curl_exec($curl);
        $error = curl_error($curl);
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        if (! empty($error)) {
            print_r($error);
        } elseif ($statusCode >= 400) {
            $body = json_decode($response, true);
            if (isset($body['error'])) {
                print_r($body['error']);
            } else {
                print_r($response);
            }
        }

        $file = file_put_contents('weeklyad4.pdf', $response);

        //  $headers = ['Content-Type: application/pdf', ];

        //return $response()->download($file, 'weeklyad4.pdf', $headers);
        return 'Complete';
    }

    /**
     *  PDF.
     */
    public function pdf($id)
    {
        $ad = Ad::findOrFail($id);

        return view('ads.pdf', compact('ad'));
    }

    public function ad($id, Request $request)
    {
        $ad = Ad::findOrFail($id);

        $specials = $this->getSpecials($id);

        $items = $this->format_prices($specials);

        if ($request->has('download')) {
            // pass view file
            $pdf = PDF::loadView('ads.html2pdfbootstrap', compact('ad', 'items'))->setPaper('tabloid');
            // download pdf
            return $pdf->download('html2pdfbootstrap.pdf');
        }

        return view('ads.html2pdfbootstrap', compact('ad', 'items'));
    }

    public function getSpecials($id)
    {
        $ad = Ad::findOrFail($id);
        $range_array = [];
        for ($x = 1; $x <= 50; $x++) {
            $range_array[] = $x;
        }

        return $ad->specials->whereIn('position_number', $range_array);
    }

    private function format_prices($specials)
    {
        $item_prices[] = '';

        foreach ($specials as $special) {
            $stripped_price = preg_replace('/[^0-9.]/', '', $special->price);

            $item_prices[$special->position_number] = $stripped_price;
        }

        return $item_prices;
    }

    private function validate_positions($specials, array $position_array)
    {
        $err_array[] = '';

        foreach ($specials as $special) {
            $specials_with_position[] = $special->position_number;
        }
        sort($specials_with_position, SORT_NUMERIC);

        $err_array = array_diff($position_array, $specials_with_position);

        if (empty($err_array)) {
            return true;
        } else {
            return false;
        }
    }

    public function html2pdf($id)
    {
        $ad = Ad::findOrFail($id);

        return view('ads.html2pdf', compact('ad'));
    }

    /**
     *  Custom Report.
     */
    public function custom_report(Request $request, $id)
    {
        $this->validate($request, [
            'categories' => 'required',
        ]);

        $categories = explode(',', $request->categories);

        $custom = Special::where('specials.ad_id', $id)
            ->join('department_special', 'id', '=', 'department_special.special_id')
            ->whereIn('department_special.department_id', $categories)
            ->select('specials.*')
            ->get();

        if ($request->custom_excel == 1) {
            $ad = Ad::findOrFail($id);

            $specials = $custom;

            Excel::create('Ad-Export-'.$ad->brands->name.'-'.$ad->date_from, function ($excel) use ($ad, $specials) {
                $excel->sheet($ad->name, function ($sheet) use ($specials) {
                    $sheet->loadView('ads.export')->with('specials', $specials);
                });
            })->download('xlsx');
        } else {
            Session::flash('custom', $custom);
        }

        return back();
    }

    /**
     *  Display Ads Including The Previous Ones.
     */
    public function all()
    {
        $ads = Ad::orderBy('date_from', 'desc')->paginate(25);

        $previous = 1;

        return view('ads.index', compact('ads', 'previous'));
    }

    /**
     *  XLS Export.
     */
    public function export($id)
    {
        $ad = Ad::findOrFail($id);

        $specials = Special::where('ad_id', $id)->get();

        Excel::create('Ad-Export-'.$ad->brands->name.'-'.$ad->date_from, function ($excel) use ($ad, $specials) {
            $excel->sheet($ad->name, function ($sheet) use ($specials) {
                $sheet->loadView('ads.export')->with('specials', $specials);
            });
        })->download('xlsx');
    }

    /**
     *  CSV Export.
     */
    public function export_csv($id)
    {
        return Excel::download(new SpecialsExport($id), 'specials.xlsx');
    }

    /**
     *  PDF Template.
     */
    public function pdf_template($id)
    {
        $ad = Ad::find($id);
        $view = 'ads.templates.';

        if ($ad->custom_template != null) {
            $view .= $ad->custom_template;
        } else {
            switch ($ad->brand_id) {
                default:
                    $view .= 'buyforless';
                case '4':
                    $view .= 'supermercado-soft-revamp';
                    break;
                case '3':
                    $view .= 'smartsaver';
                    break;
                case '2':
                    if (strtolower($ad->name) == 'specialty ad') {
                        $view .= 'uptown-specialty';
                    } else {
                        $view .= 'uptown';
                    }
                    break;
                case '1':
                    $view .= 'buyforless';
            }
        }

        return view($view, compact('ad'));
    }

    /**
     *  Generate PDF.
     */
    public function generate_pdf($id)
    {
        $source = 'https://sara.buyforlessok.com/ads/pdf-template/'.$id;

        $ad = Ad::findOrFail($id);

        $landscape = false;

        if (($ad->brands->id == 2) || ($ad->custom_template == 'holidays-gm') || ($ad->custom_template == 'uptown-specialty')) {
            $landscape = true;
        }

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => 'https://api.pdfshift.io/v2/convert/',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => json_encode(['source' => $source, 'landscape' => $landscape, 'use_print' => false, 'format' => 'Tabloid']),
            CURLOPT_HTTPHEADER => ['Content-Type:application/json'],
            CURLOPT_USERPWD => '20c353e4955c4ad68c79d099663ee94f:',
        ]);

        $response = curl_exec($curl);

        $filename = uniqid().'.pdf';

        return \Illuminate\Support\Facades\Response::make($response, 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="'.$filename.'"',
        ]);
    }

    /**
     *  Send Ad Link to RSA.
     */
    public function send_to_rsa()
    {
    }

    /**
     *  Ad Duplicate.
     */
    public function duplicate($id)
    {
        $ad = Ad::find($id);

        $new = $ad->replicate();

        $new->name = $ad->name.' (Copy)';

        $new->status = 0;

        $new->save();

        foreach ($ad->specials as $special) {
            $new_special = $special->replicate();

            $new_special->ad_id = $new->id;
            $new_special->position_number = null;
            $new_special->position = null;

            $new_special->save();

            foreach ($special->department() as $department) {
                $new_special->department()->attach($special->department()->select('department_id')->pluck('department_id')->toArray());
            }
        }

        Session::flash('message', [
            'type'  => 'success',
            'message' => 'Ad replicated successfully',
        ]);

        return back();
    }

    /**
     *  Mailing.
     */
    public function mailing($id)
    {
        $ad = Ad::find($id);

        return view('ads.templates.mailing', compact('ad'));
    }

    /**
     * Front Page Poster.
     */
    public function poster($id, $page)
    {
        $ad = Ad::find($id);
        $view = 'ads.templates.posters.';

        switch ($ad->brand_id) {
            default:
                $view .= 'buyforless';
            case '4':
                $view .= 'supermercado'.$page;
                break;
            case '3':
                $view .= 'smartsaver';
                break;
            case '2':
                $view .= 'uptown'.$page;
                break;
            case '1':
                $view .= 'buyforless';
        }

        return view($view, compact('ad'));
    }

    public function poster_pdf($id, $page)
    {
        $source = 'https://sara.buyforlessok.com/ads/poster/'.$id.'/'.$page;

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => 'https://api.pdfshift.io/v2/convert/',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => json_encode(['source' => $source, 'landscape' => false, 'use_print' => false, 'format' => '11inx14in']),
            CURLOPT_HTTPHEADER => ['Content-Type:application/json'],
            CURLOPT_USERPWD => '20c353e4955c4ad68c79d099663ee94f:',
        ]);

        $response = curl_exec($curl);

        $filename = uniqid().'.pdf';

        return \Illuminate\Support\Facades\Response::make($response, 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="'.$filename.'"',
        ]);
    }
}
