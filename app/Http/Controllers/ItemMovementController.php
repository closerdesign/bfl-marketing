<?php

namespace App\Http\Controllers;

use App\Imports\ItemMovementImport;
use App\ItemMovement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ItemMovementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $start = date('Y-m-d', strtotime('-14 weeks'));

        $end = date('Y-m-d');

        $item_movements = ItemMovement::whereBetween('week_ending_date', [$start, $end])
            ->groupBy('upc')
            ->select('*', DB::raw('SUM(qty_sold) as qty'), DB::raw('SUM(amt_sold) as amount'))
            ->paginate(50);

        return view('item-movement.index', compact('item_movements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('item-movement.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'file' => 'required',
        ]);

        Excel::import(new ItemMovementImport(), request()->file('file'));

        return redirect()->action('ItemMovementController@index')->with(['message' => ['type' => 'success', 'message' => 'File imported successfully']]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\item_movement  $item_movement
     * @return \Illuminate\Http\Response
     */
    public function show(ItemMovement $item_movement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\item_movement  $item_movement
     * @return \Illuminate\Http\Response
     */
    public function edit(ItemMovement $item_movement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\item_movement  $item_movement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ItemMovement $item_movement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\item_movement  $item_movement
     * @return \Illuminate\Http\Response
     */
    public function destroy(ItemMovement $item_movement)
    {
        //
    }
}
