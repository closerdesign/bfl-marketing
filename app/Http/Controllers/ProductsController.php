<?php

namespace App\Http\Controllers;

use App\Category;
use App\Department;
use App\Http\Requests;
use App\Product;
use App\Search;
use App\ShoppingProduct;
use App\Update;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Intervention\Image\Facades\Image;
use Maatwebsite\Excel\Facades\Excel;

class ProductsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['shopping_categories', 'shopping_products', 'shopping_product', 'shopping_search', 'aws_search', 'shopping_favorites', 'generate_categories']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $products = Product::join('shopping_products', 'upc', '=', 'shopping_products.sku')->select('products.*')->groupBy('products.upc')->paginate(24);
        $products = Product::paginate(24);

        // Session::forget('keyword');

        if (Session::has('keyword')) {
            $keyword = Session::get('keyword');

            $products = Product::where('products.name', 'like', '%'.$keyword.'%')
                ->orWhere('products.upc', 'like', '%'.$keyword.'%')
                ->orWhere('products.brand_name', 'like', '%'.$keyword.'%')
                ->orWhere('products.kwikee_name', 'like', '%'.$keyword.'%')
                ->orWhere('products.kwikee_brand_name', 'like', '%'.$keyword.'%')
                ->select('products.*')
                ->groupBy('products.upc')
                ->paginate(24);
        }

        return view('products.index', compact('products'));
    }

    public function clear_session()
    {
        Session::forget('keyword');
        Session::forget('size');
        Session::forget('categories');

        return view('products.index-aws');
    }

    public function aws_internal()
    {
        //die('die');
        // $products[] = '';
        // $vars[] = '';
        if (Session::has('keyword') || Session::has('size') || Session::has('categories')) {
            $vars['keyword'] = Session::get('keyword');
            $vars['size'] = Session::get('size');
            $vars['categories'] = Session::get('categories');

            $keyword = '(and ';
            if (! empty(Session::get('keyword'))) {
                $keyword .= "name:'{$vars['keyword']}' ";
            }
            if (! empty(Session::get('size'))) {
                $keyword .= " size:'{$vars['size']}' ";
            }
            if (! empty(Session::get('categories'))) {
                $keyword .= " categories:'{$vars['categories']}' ";
            }
            $keyword .= ')';
            $keyword = urlencode($keyword);
            $keyword = $keyword.'&q.parser=structured';

            $end_point = 'https://search-product-name-tgegi4uon56ims5tn3ikb6kanu.us-west-1.cloudsearch.amazonaws.com/2013-01-01/search?q='.$keyword.'&size=250';

            $client = new \GuzzleHttp\Client();
            $response = $client->get($end_point);
            $products = $response->getBody()->getContents();

            $products = json_decode($products);

            $vars['found'] = $products->hits->found;

            $products = $products->hits->hit;
        }

        return view('products.index-aws', compact('products', 'vars'));
    }

    public function aws_internal_run(Request $request)
    {
        Session::put('keyword', $request->keyword);
        Session::put('size', $request->size);
        Session::put('categories', $request->categories);

        if ($request->keyword || $request->size || $request->categories) {
            $keyword = $request->keyword;

            //  http://search-movies-rr2f34ofg56xneuemujamut52i.us-east-1.cloudsearch.
            //  amazonaws.com/2013-01-01/search?q=(and+(phrase+field='title'+'star wars')+(not+(range+field%3Dyear+{,2000})))&q.parser=structured

            if ($request->size != '' || $request->categories != '') {
                if ($request->size != '') {
                    $size = " size:'$request->size' ";
                }
                if ($request->categories != '') {
                    $categories = " categories:'$request->categories'";
                }
                if ($request->keyword != '') {
                    $name = " name:'$request->keyword' ";
                    $brand_name = " brand_name:'$request->keyword' ";
                }

                $keyword = '(and ';
                if (isset($name)) {
                    $keyword .= $name;
                    //      $keyword .= $brand_name;
                }
                if (isset($size)) {
                    $keyword .= $size;
                }
                if (isset($categories)) {
                    $keyword .= $categories;
                }
                $keyword .= ')';
                $keyword = urlencode($keyword);
                $keyword = $keyword.'&q.parser=structured';
            }

            $end_point = 'https://search-product-name-tgegi4uon56ims5tn3ikb6kanu.us-west-1.cloudsearch.amazonaws.com/2013-01-01/search?q='.$keyword.'&size=250';

            //   die($end_point);

            $client = new \GuzzleHttp\Client();
            $response = $client->get($end_point);
            $products = $response->getBody()->getContents();

            // print_r($products);

            $products = json_decode($products);

            $vars['found'] = $products->hits->found;

            $products = $products->hits->hit;
        }

        return view('products.index-aws', compact('products', 'vars'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'upc'     => 'required|unique:products',
            'name'    => 'required',
            'image'   => 'image|max:16000',
        ]);

        $product = new Product($request->all());

        $product->upc = sprintf('%013d', $product->upc);

        $image = '';

        if ($request->hasFile('image')) {
            $image = uniqid().'.'.$request->file('image')->getClientOriginalExtension();

            $resized = Image::make($request->file('image')->getRealPath());

            $resized->resize('500', null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $resized = $resized->stream();

            Storage::put('product-image-highres/'.$image, file_get_contents($request->file('image')->getRealPath()), 'public');

            Storage::put('product-image/'.$image, $resized->__toString(), 'public');
        }

        $product->image = $image;

        $product->save();

        $product->categories()->attach($request->categories);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Product created successfully!',
        ]);

        return redirect()->action('ProductsController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);

        return view('products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'    => 'required',
            'image'   => 'image|max:16000',
        ]);

        $product = Product::findOrFail($id);

        $image = basename($product->image);

        if (($image == 'pending-image.jpg') || (strpos($product->image, 'image-display') !== false)) {
            $image = '';
        }

        $product->fill($request->all());

        if ($request->hasFile('image')) {
            $image = uniqid().'.'.$request->file('image')->getClientOriginalExtension();

            $resized = Image::make($request->file('image')->getRealPath());

            $resized->resize('500', null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $resized = $resized->stream();

            Storage::put('product-image-highres/'.$image, file_get_contents($request->file('image')->getRealPath()), 'public');

            Storage::put('product-image/'.$image, $resized->__toString(), 'public');
        }

        $product->image = $image;

        $product->save();

        $categories = [];

        if (! empty($request->categories)) {
            $categories = $request->categories;
        }

        $product->categories()->sync($categories);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Product updated successfully!',
        ]);

        if ($request->back == 1) {
            return back();
        }

        return redirect()->action('ProductsController@index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ajax_img(Request $request, $id)
    {
        $this->validate($request, [
            'image'   => 'required|image|max:16000',
        ]);

        $product = Product::findOrFail($id);

        $image = uniqid().'.'.$request->file('image')->getClientOriginalExtension();

        $resized = Image::make($request->file('image')->getRealPath());

        $resized->resize('500', null, function ($constraint) {
            $constraint->aspectRatio();
        });

        $resized = $resized->stream();

        Storage::put('product-image-highres/'.$image, file_get_contents($request->file('image')->getRealPath()), 'public');

        Storage::put('product-image/'.$image, $resized->__toString(), 'public');

        $product->image = $image;

        $product->save();

        return "<img src='".$product->image."' alt='".$product->name."' width='90' />";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ajax_categories(Request $request, $id)
    {
        $this->validate($request, [
            'categories'   => 'required',
        ]);

        $product = Product::findOrFail($id);

        $categories = [];

        if (! empty($request->categories)) {
            $categories = $request->categories;
        }

        $product->categories()->sync($categories);

        $product->save();

        return 1;
    }

    /**
     *  Categories Assignment Upload Form.
     */
    public function categories_upload_form()
    {
        return view('products.categories-upload');
    }

    /**
     *  Categories Assignment Upload.
     */
    public function categories_upload(Request $request)
    {
        $this->validate($request, [
            'file'        => 'required|mimes:xls,xlsx|max:1000',
            'category_id' => 'required',
        ]);

        $results = Excel::load($request->file('file')->getRealPath(), function () {
        })->get();

        $processed = 0;

        $updated = 0;

        foreach ($results as $result) {
            $product = Product::where('upc', sprintf('%013d', $result->upc))->first();

            if (! empty($product)) {
                $categories = [];

                foreach ($product->categories as $category) {
                    array_push($categories, $category->id);
                }

                array_push($categories, $request->category_id);

                $product->categories()->sync($categories);

                $updated += 1;
            }

            $processed += 1;
        }

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'The File Has Been Processed. '.$processed.' items has been processed. '.$updated.' has been affected.',
        ]);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     *  Display Import Products Form.
     */
    public function import()
    {
        return view('products.import');
    }

    /**
     *  Create Products From Imported File.
     */
    public function file_import(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|mimes:xls,xlsx',
        ]);

        $results = Excel::load($request->file('file')->getRealPath(), function ($reader) {
        })->get();

        $processed = 0;

        foreach ($results as $result) {
            $product = Product::firstOrNew(['upc' => sprintf('%013d', $result->upc)]);

            $product->name = $result->name;

            $product->brand_name = $result->brand_name;

            $product->size = $result->size;

            $product->save();

            $processed += 1;
        }

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'File Imported Successfully! '.$processed.' processed items.',
        ]);

        return redirect()->action('ProductsController@index');
    }

    /**
     *  Set Products Filter.
     */
    public function filter(Request $request)
    {
        if ($request->keyword != '') {
            Session::put('keyword', $request->keyword);
        } else {
            Session::remove('keyword');
        }

        return redirect()->action('ProductsController@index');
    }

    /**
     *  Run Database Optimization.
     */
    public function optimize()
    {
        $products = Product::all();

        $total = count($products);

        $optimized = 0;

        $created = 0;

        $issues = 0;

        $output = '<br />';

        foreach ($products as $product) {
            $length = strlen($product->upc);

            if ($length != 13) {
                $issues += 1;

                $output .= 'Item with UPC '.$product->upc.' has issues. Current length is '.$length.'.';

                $upc = ltrim($product->upc, '0');

                $upc = sprintf('%013d', $upc);

                $output .= ' New UPC would be '.$upc;

                // Is there any item with that UPC?

                $check = count(Product::where('upc', $upc)->get());

                $output .= ' There is '.$check.' items with the proposed UPC.';

                // If so, delete the current item.

                if ($check != 0) {
                    Product::destroy($product->id);

                    $output .= ' Item with UPC '.$product->upc.' has been removed from the database.'.'<br />';
                } else {
                    $product->upc = $upc;

                    $product->save();

                    $optimized += 1;

                    $output .= 'Product was successfully optimized.<br />';
                }
            }
        }

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Process finished. '.$total.' products processed. '.$issues.' issues found. '.$optimized.' products optimized. '.$created.' products created.'.$output,
        ]);

        return back();
    }

    /**
     *  Optimize Kwikee Images.
     */
    public static function optimize_kwikee_image()
    {
        $products = DB::table('products')
            ->where('products.image', '')
            ->join('kwikee_images', function ($join) {
                $join->on('products.id', '=', 'kwikee_images.product_id');
            })
//            ->join('shopping_products', function($join){
//                $join->on('products.upc', '=', 'shopping_products.sku');
//            })
            ->where(function ($q) {
                $q->where('kwikee_images.type', 'PNG')
                ->orWhere('kwikee_images.type', 'JPG');
            })
            ->groupBy('products.id')
            ->take(10)
            ->get();

        if (count($products) > 0) {
            $username = env('KWIKEE_USER');

            $password = env('KWIKEE_PASSWORD');

            $context = stream_context_create([
                'http' => [
                    'header'  => 'Authorization: Basic '.base64_encode("$username:$password"),
                ],
            ]);

            $list = [];

            foreach ($products as $product) {
                $image = uniqid().'.'.substr(basename($product->url), -3, 3);

                Storage::put('product-image-highres/'.$image, file_get_contents($product->url, false, $context), 'public');

                $resized = Image::make(Storage::get('product-image-highres/'.$image));

                $resized->resize('500', null, function ($constraint) {
                    $constraint->aspectRatio();
                });

                $resized = $resized->stream();

                Storage::put('product-image/'.$image, $resized->__toString(), 'public');

                Product::where('upc', $product->upc)->update(['image' => $image]);

                array_push($list, $product->upc);
            }

            Mail::send('emails.optimize-kwikee-image', ['list' => $list], function ($m) use ($list) {
                $m->from('jrodriguez@buyforlessok.com', 'Juan Rodriguez');

                $m->to('jrodriguez@buyforlessok.com', 'Juan Rodriguez')
                    ->replyTo('jrodriguez@buyforlessok.com')
                    ->subject('Optimize Kwikee Image');
            });

            return $list;
        }
    }

    /**
     *  Shopping Products.
     */
    public function shopping_products($store, $category)
    {
        $products = Product::shopping($store, $category)->paginate(24);

        $category = Category::find($category);

        return response(['products' => $products, 'category' => $category], 200);
    }

    /**
     * AWS Cloudsearch.
     */
    public static function aws_search($store, $keyword)
    {
        $products = Product::aws_search($store, $keyword);

        return response(['products' => $products], 200);
    }

    /**
     *  Shopping Categories
     *  Pulled from AWS. Generated nightly by the
     *  scheduler.
     */
    public function shopping_categories($store)
    {
        $categories = Category::with('children')->shopping($store)->active()->get();

        return response($categories, 200);
    }

    /**
     *  Shopping Single Product.
     */
    public function shopping_product($store, $upc)
    {
        $product = Product::single($store, $upc)->get();

        return response($product, 200);
    }

    /**
     *  Shopping Search.
     */
    public function shopping_search($store, $keyword)
    {
        $products = Product::StoreSearch($store, $keyword)->paginate(24);

        $search = new Search();
        $search->search_string = urldecode($keyword);
        $search->store_code = $store;
        $search->save();

        return response(['products' => $products], 200);
    }

    /**
     *  Shopping Favorites.
     */
    public function shopping_favorites($store, $upcs)
    {
        $products = Product::favorites($store, $upcs)->paginate(24);

        return response(['products' => $products], 200);
    }

    /**
     *  Shipt Categories Report.
     */
    public function shipt_categories()
    {
        Excel::create('shipt_categories', function ($excel) {
            $excel->sheet('categories', function ($sheet) {
                $sheet->loadView('products.shipt-categories');
            });
        })->download('csv');
    }
}
