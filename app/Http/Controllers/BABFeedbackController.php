<?php

namespace App\Http\Controllers;

use App\BABFeedback;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class BABFeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('bab_feedback/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'last_name'         => 'required',
            'role'              => 'required',
            'store'             => 'required',
            'email'             => 'required',
            'executive_email'   => 'required',
            'location'          => 'required',
            'meeting_date'      => 'required',
            'time_start'        => 'required',
            'time_end'          => 'required',
            'leader'            => 'required',
            'present'           => 'required',
            'absent'            => 'required',
            'praise_report'     => 'required',
            'wins'              => 'required',
            'brand_demo'        => 'required',
            'how_poa'           => 'required',
            'guest_one'         => 'required',
            'guest_two'         => 'required',
            'support_needs'     => 'required',
            'learning_sc'       => 'required',
            'focus_old'         => 'required',
            'focus_new'         => 'required',
            'receipt_ideas'     => 'required',
            'addtl_info'        => 'required',
            'attachment'        => 'file|mimes:pdf,doc,csv,xlsx,xls,docx,jpg,jpeg,png,svg|max:10000',
        ]);

        $filename = '';

        if ($request->attachment !== null) {
            $extension = $request->file('attachment')->getClientOriginalExtension();
            $filename = uniqid().'.'.$extension;
            Storage::put('bab_feedback/'.$filename, file_get_contents($request->file('attachment')->getRealPath()), 'public');
        }

        $feedback = new BABFeedback($request->all());
        $feedback->attachment = $filename;
        $feedback->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Your feedback has been successfully submitted.',
        ]);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
