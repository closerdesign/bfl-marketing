<?php

namespace App\Http\Controllers;

use App\SmsGroup;
use App\SmsSubscriber;
use App\SmsSubscriberGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Jelovac\Bitly4laravel\Facades\Bitly4laravel;
use Twilio\Rest\Client;
use Twilio\Twiml;
use Twilio\TwiML\Messaging\Message;
use Twilio\TwiML\MessagingResponse;

class SmsController extends Controller
{
    private $twillio_sid = 'AC6ae716b12c5d82099c7519eb49dee914';
    private $twillio_token = 'fc5bf3a0a455582538ac295cd0816982';
    private $phone_number = '+14053584491';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sms_groups = SmsGroup::all();

        return view('sms.index', compact('sms_groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function test_subscribe()
    {
        return view('sms.test-subscribe');
    }

    /**
     * @param Request $request
     * @return string
     */
    public function subscribe(Request $request)
    {
        header('content-type: text/xml');

        $body = strtolower(trim($_REQUEST['Body']));
        $phonenumber = $_REQUEST['From'];

        //todo do function that unsubscribes
        if ($body == 'stop') {
            return 'stop';
        }

        $group = SmsGroup::Where(strtolower('keyword'), '=', $body)->first();

        if (! $group) {
            $response = new MessagingResponse();
            $response->message('No groups match your input');

            return  response($response);
        }

        $subscriber = SmsSubscriber::where('phone_number', $phonenumber)->first();
        if (! $subscriber) {
            $subscriber = new SmsSubscriber();
            $subscriber->phone_number = $phonenumber;
            $subscriber->subscribed = true;

            $subscriber->save();
        }

        $inGroup = SmsSubscriberGroup::where('sms_subscriber_id', $subscriber->id)->where('sms_group_id', $group->id)->first();

        if (! $inGroup) {
            $doSubscribe = new SmsSubscriberGroup();
            $doSubscribe->sms_group_id = $group->id;
            $doSubscribe->sms_subscriber_id = $subscriber->id;
            $doSubscribe->save();
        }

        $response = new MessagingResponse();
        $response->message($group->welcome_message);

        return  response($response);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Twilio\Exceptions\TwimlException
     */
    public function register(Request $request)
    {
        $phonenumber = $request->input('From');
        $message = $request->input('Body');
        $outputMessage = $this->createMessage($phonenumber, $message);

        $response = new Twiml();
        $response->message($outputMessage);

        return response($response)->header('Content-Type', 'text/xml');
    }

    /**
     * return all sms groups.
     */
    public function groups()
    {
        $groups = SmsGroup::all();

        return view('sms.groups', compact('groups'));
    }

    /**
     * @param $id
     * group-edit page
     */
    public function group_edit($id)
    {
        $group = SmsGroup::findOrfail($id);

        return view('sms.group-edit', compact('group'));
    }

    /**
     * Send SMS.
     */
    public function send_sms(Request $request)
    {
        $this->validate($request, ['message' => 'required', 'group_id' => 'required']);
        $var = '';

        try {
            if ($request->file != '') {
                $filename = uniqid().'.'.$request->file('file')->getClientOriginalExtension();

                Storage::put('twillio/'.$filename, file_get_contents($request->file('file')->getRealPath()), 'public');

                $filename = 'https://bfl-corp-sara.s3.us-west-2.amazonaws.com/twillio/'.$filename;

                $twillio['mediaUrl'] = [$filename];
            }

            if ($request->url != '') {
                $url = Bitly4laravel::shorten($request->url);
                $url = $url->data->url;
            }

            $twillio['from'] = '+14053584491'; //$this->phone_number;
            $twillio['body'] = $request->message."\n ".$url.' '.$var;

            $client = new Client($this->twillio_sid, $this->twillio_token);

            $n = '+14057605923';

            $number_array = SmsSubscriber::where('subscribed', 1)
                    ->where('sms_subscriber_group.sms_group_id', '=', 2)
                    ->join('sms_subscriber_group', 'sms_subscribers.id', '=', 'sms_subscriber_group.sms_subscriber_id')
                    ->get();

            foreach ($number_array as $n) {
                echo $n->phone_number;
                $client->messages->create($n,
                    $twillio);
            }

//            foreach ($request->group_id as $group) {
//                $var .= $group . " | ";
//
//                $number_array = SmsSubscriber::where('subscribed', 1)
//                    ->where('sms_subscriber_group.sms_group_id', '=', $group)
//                    ->join('sms_subscriber_group', 'sms_subscribers.id', '=', 'sms_subscriber_group.sms_subscriber_id')
//                    ->get();
//
//                //   $sql = "SELECT phone_number FROM sms_subscribers ss JOIN sms_subscriber_group ssg ON ss.id = ssg.sms_subscriber_id WHERE ss.subscribed = 1 AND ssg.sms_group_id = '$group' "
//
//                foreach ($number_array as $n){
//
//                    $client->messages->create($n,
//                        $twillio);
//                }
//                }

            Session::flash('message', [
                'type' => 'success',
                'message' => 'Message Sent Successfully',
            ]);

            return back();
        } catch (Exception $e) {
            echo 'Error : '.$e->getMessage();
        }
    }

    public function reply_sms()
    {
        header('content-type: text/xml');

        $response = new MessagingResponse();
        $response->message('This is a reply');

        return view('sms.index', compact('response'));
    }

    /**
     * Displays the form for creating a texting group.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function sms_group()
    {
        return view('sms.create-group');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function create_group(Request $request)
    {
        $this->validate($request, [
            'group_name'  => 'required',
            'welcome_message' => 'required',
            'keyword' => 'required',
        ]);

        $sms_group = new SmsGroup($request->all());

        $sms_group->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Group saved successfully',
        ]);

        return back();
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Validation\ValidationException
     */
    public function group_update(Request $request, $id)
    {
        $this->validate($request, [
            'group_name'        => 'required',
            'welcome_message'   => 'required',
            'keyword'   => 'required',

        ]);

        $group = SmsGroup::findOrFail($id);

        $group->fill($request->all());

        $group->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Group updated successfully',
        ]);

        $groups = SmsGroup::all();

        return view('sms.groups', compact('groups'));
    }
}
