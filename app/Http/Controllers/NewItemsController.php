<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\NewItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class NewItemsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['display']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = NewItem::orderBy('created_at', 'desc')->paginate(15);

        return view('new-items.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'brand_id'    => 'required',
            'image'       => 'required|mimes:jpg,jpeg,png|max:10000',
            'title'       => 'required',
            'description' => 'required',
            'status'      => 'required',
        ]);

        $filename = '';

        $extension = $request->file('image')->getClientOriginalExtension();

        $filename = uniqid().'.'.$extension;

        Storage::put('new-items/'.$filename, file_get_contents($request->file('image')->getRealPath()), 'public');

        $item = new NewItem($request->all());

        $item->image = $filename;

        $item->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Your item has been added successfully!',
        ]);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = NewItem::findOrFail($id);

        return view('new-items.show', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'brand_id'    => 'required',
            'image'       => 'mimes:jpg,jpeg,png|max:10000',
            'title'       => 'required',
            'description' => 'required',
            'status'      => 'required',
        ]);

        $item = NewItem::findOrFail($id);

        $filename = basename($item->image);

        $item->fill($request->all());

        if ($request->hasFile('image')) {
            $filename = uniqid().'.'.$request->file('image')->getClientOriginalExtension();

            Storage::put('new-items/'.$filename, file_get_contents($request->file('image')->getRealPath()), 'public');
        }

        $item->image = $filename;

        $item->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Item Updated Successfully!',
        ]);

        return redirect()->action('NewItemsController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = NewItem::findOrFail($id);

        $item->destroy($id);

        Session::flash('message', [
            'type' => 'success',
            'message' => 'Item has been successfully deleted.',
        ]);

        return back();
    }

    /**
     *  API call to display on websites.
     */
    public function display($brand)
    {
        $items = NewItem::where('brand_id', $brand)
            ->where('status', '=', 1)
            ->orderBy('created_at', 'desc')
            ->get();

        return response($items, 200);
    }
}
