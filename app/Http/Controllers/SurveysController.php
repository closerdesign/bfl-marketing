<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

class SurveysController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display the Surveys report.
     */
    public function list()
    {
        if (isset($_GET['from']) && isset($_GET['to'])) {
            $surveys = file_get_contents('http://grocerysurvey.us/survey/list/'.$_GET['from'].'/'.$_GET['to']);

            $surveys = json_decode($surveys);

            $count = count($surveys);

            if (empty($count)) {
                $count = 0;
            }

            $total = 0;

            foreach ($surveys as $survey) {
                $tag = [
                    '1' => 'Yes',
                    '0' => 'No',
                ];

                $survey->recommend = $tag[$survey->recommend];

                $total += $survey->rating;
            }
            if ($total > 0 && $count > 0) {
                $rating = $total / $count;
            } else {
                $rating = 1;
            }
        }

        return view('surveys.list', compact('surveys', 'rating', 'count'));
    }
}
