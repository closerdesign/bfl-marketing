<?php

namespace App\Http\Controllers;

use App\ItAsset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ItAssetsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $assets = ItAsset::orderBy('name')->paginate(50);

        return view('it-assets.index', compact('assets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('it-assets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'type' => 'required',
            'name' => 'required',
            'price' => 'required',
            'status' => 'required',
            'employee_id' => 'required_if:status,assigned',
        ]);

        $asset = new ItAsset($request->all());

        $asset->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'IT Asset saved successfully',
        ]);

        return redirect()->action('ItAssetsController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $asset = ItAsset::find($id);

        return view('it-assets.edit', compact('asset'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $asset = ItAsset::find($id);

        $this->validate($request, [
            'type' => 'required',
            'name' => 'required',
            'price' => 'required',
            'status' => 'required',
            'employee_id' => 'required_if:status,assigned',
        ]);

        $asset->fill($request->all());

        $asset->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'IT Asset updated successfully',
        ]);

        return redirect()->action('ItAssetsController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
