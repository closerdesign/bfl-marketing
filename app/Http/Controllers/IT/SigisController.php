<?php

namespace App\Http\Controllers\IT;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SigisController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     *  Upload SIGIS File Form.
     */
    public function upload_form()
    {
        return view('IT.upload-form');
    }

    /**
     *  Upload SIGIS File.
     */
    public function upload(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|mimes:xlsx|max:15000',
        ]);

        $filename = date('mdY').'-'.request()->file('file')->getClientOriginalName();

        Storage::put('sigis-file/'.$filename, file_get_contents(request()->file('file')->getRealPath()));

        return back()->with(['message' => ['type' => 'success', 'message' => 'The file has been uploaded successfully']]);
    }
}
