<?php

namespace App\Http\Controllers;

use App\Exports\ShoppingProductsExport;
use App\PricingFile;
use App\ShoppingProduct;
use Chumper\Zipper\Facades\Zipper;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class ShoppingProductsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['reorder_list', 'product']]);
    }

    public function index()
    {
        $products = ShoppingProduct::doesntHave('product')->groupBy('sku')->paginate(50);

        $total_wo = count(ShoppingProduct::doesntHave('product')->groupBy('sku')->get());

        $total = count(ShoppingProduct::groupBy('sku')->get());

        return view('shopping-products.index', compact('products', 'total_wo', 'total'));
    }

    public function excel()
    {
        return Excel::download(new ShoppingProductsExport(), 'shopping-products.xlsx');
    }

    public function files()
    {
        $client = new Client(['auth' => [
            env('KWIKEE_USER'),
            env('KWIKEE_PASSWORD'),
        ]]);

        $xml = "<?xml version='1.0' encoding='UTF-8'?>\n"
            ."<kwikee_data>\n"
            ."  <service>\n"
            ."    <init>\n"
            ."      <request />\n"
            ."    </init>\n"
            ."  </service>\n"
            .'</kwikee_data>';

        $request = $client->post('https://api.kwikeesystems.com/v1.1/init/xml', [
            'body' => $xml,
        ]);

        $request = $request->getBody();

        $request = new \SimpleXMLElement($request);

        if ($request->service->init->response->response_info->code != 'SUC-001') {
            Session::flash('message', [
                'type'    => 'error',
                'message' => 'There was a problem on the Kwikee connection',
            ]);
        } else {
            $username = env('KWIKEE_USER');

            $password = env('KWIKEE_PASSWORD');

            $context = stream_context_create([
                'http' => [
                    'header'  => 'Authorization: Basic '.base64_encode("$username:$password"),
                ],
            ]);

            foreach ($request->service->init->response->file_list->file as $file) {
                $url = $file->file_url;

                $filename = basename($url);

                copy($url, public_path('resources/kwikee_files/zip/'.$filename), $context);
            }

            Session::flash('message', [
                'type'    => 'success',
                'message' => 'Kwikee Initial Files Imported Successfully',
            ]);
        }

        return back();
    }

    /**
     *  Importing Available Products.
     */
    public static function import()
    {
        $pricing = PricingFile::where('status', 0)->first();

        $file = Storage::url('pricing-files/'.$pricing->file);

        $lines_count = file($file);

        if (count($lines_count) > 100) {
            $content = fopen($file, 'r');

            $pricing->store = substr($pricing->name, -8, 4);

            DB::delete('DELETE FROM shopping_products WHERE store_id = '.$pricing->store);

            $total = 0;

            while (! feof($content)) {
                $row = fgets($content);

                $row = explode('|', $row);

                if ($row[0] != 'SKU') {
                    $product = new ShoppingProduct(
                        [
                            'store_id'         => $pricing->store,
                            'sku'              => $row[0],
                            'brand_name'       => $row[1],
                            'product_name'     => $row[2],
                            'description'      => $row[3],
                            'size'             => $row[4],
                            'uom'              => $row[5],
                            'dept_code'        => $row[6],
                            'scale_flag'       => $row[7],
                            'how_to_sell'      => $row[8],
                            'avg_weight'       => $row[9],
                            'wgt_selector'     => $row[10],
                            'store_number'     => $row[11],
                            'ad_flag'          => $row[12],
                            'cool'             => $row[13],
                            'regular_price'    => $row[14] + ($row[14] * 0.07),
                            'sale_price'       => $row[15],
                            'sale_qty'         => $row[16],
                            'promo_start_date' => $row[17],
                            'promo_end_date'   => $row[18],
                            'promo_buy_qty'    => $row[19],
                            'promo_get_qty'    => $row[20],
                            'promo_min_qty'    => $row[21],
                            'promo_limit_qty'  => $row[22],
                            'family_sale_flag' => $row[23],
                            'family_sale_code' => $row[24],
                            'family_sale_desc' => $row[25],
                            'tax_pct'          => $row[26],
                            'bottle_deposit'   => $row[27],
                            'promo_tag'        => $row[28],
                            'movement'         => $row[32],
                        ]
                    );

                    $product->save();
                }

                $total += 1;
            }

            $pricing->status = 1;

            $pricing->save();

            Artisan::call('shopping:adprices');

            Mail::send('emails.pricing-files', ['pricing' => $pricing, 'total' => $total], function ($m) use ($pricing, $total) {
                $m->from('juanc@closerdesign.co', 'Juan Rodriguez');

                $m->to('shopping@buyforlessok.com', 'BFL Media Team')
                    ->subject('[Processed Pricing File] '.$pricing->name);
            });
        }
    }

    /**
     *  Departments List.
     */
    public function departments($store)
    {
        $departments = DB::table('departments')
            ->select('departments.id', 'departments.name')
            ->join('shopping_products', 'shopping_products.dept_code', '=', 'departments.id')
            ->where('shopping_products.store_id', $store)
            ->groupBy('shopping_products.dept_code')
            ->orderBy('departments.name')
            ->get();

        return response($departments, 200);
    }

    /**
     *  Reorder List.
     */
    public function reorder_list($store, $upcs)
    {
        $upcs = explode(',', $upcs);

        $products = ShoppingProduct::where('store_id', $store)->whereIn('sku', $upcs)->get();

        return response($products, 200);
    }

    public function product($store, $id)
    {
        $product = ShoppingProduct::where('store_id', $store)->where('sku', $id)->get();

        return response($product, 200);
    }
}
