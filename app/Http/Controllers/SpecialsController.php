<?php

namespace App\Http\Controllers;

use App\Ad;
use App\Department;
use App\Gallery;
use App\GalleryImage;
use App\Http\Requests;
use App\Imports\SpecialsImport;
use App\Mail\Notification;
use App\Product;
use App\Promo;
use App\Special;
use App\SpecialUpc;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class SpecialsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'item_description' => 'required',
            'status'           => 'required|boolean',
            'price'            => 'required',
            'size'             => 'required',
            'department_id'    => 'required',
            'image'            => 'image',
        ]);

        $image = '';

        if ($request->hasFile('image')) {
            $image = uniqid().'.'.$request->file('image')->getClientOriginalExtension();

            $resized = Image::make($request->file('image')->getRealPath());

            $resized->resize('500', null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $resized = $resized->stream();

            Storage::put('specials/'.$image, file_get_contents($request->file('image')->getRealPath()), 'public');

            Storage::put('specials-small/'.$image, $resized->__toString(), 'public');
        } elseif ($request->has('sku')) {
            $previous = Special::where('sku', $request->sku)->where('image', '!=', '')->orderBy('created_at', 'desc')->first();

            if ($previous) {
                $image = basename($previous->image);
            }
        }

        $special = new Special($request->all());

        $special->image = $image;

        $special->department_id = 0;

        $special->plu_required = ($request->plu_required == 1) ? true : false;

        $special->save();

        $special->department()->attach($request->department_id);

        if ($special->mobile_app_deal == 1) {
            $msg = [
                'New Mobile App Deal: '.$special->item_description,
                "<p>New special has been marked as Mobile App Deal. <a href='".action('SpecialsController@edit', $special->id)."'>Click here for more information</a>.</p>",
            ];

            Mail::to('media@buyforlessok.com')
                ->send(new Notification($msg));
        }

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Special saved successfully',
        ]);

        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $special = Special::findOrFail($id);
        $departments = Department::where('active', '1')->get();

        return view('specials.edit', compact('special', 'departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        return response(request(), 200);

        $this->validate($request, [
            'item_description' => 'required',
            'status'           => 'required|boolean',
            'price'            => 'required',
            'department_id'    => 'required',
            'image'            => 'image',
        ]);

        $special = Special::findOrFail($id);

        $special->mobile_app_deal = false;

        $special->fill($request->all());

        $image = basename($special->image);

        if ($request->hasFile('image')) {
            $image = uniqid().'.'.$request->file('image')->getClientOriginalExtension();

            Storage::put('specials/'.$image, file_get_contents($request->file('image')->getRealPath()), 'public');
        }

        $special->plu_required = ($request->plu_required == 1) ? true : false;

        $special->featured = ($request->featured == 1) ? true : false;

        $special->image = $image;

        $special->department_id = 0;

        $special->save();

        $special->department()->sync($request->department_id);

        return redirect()
            ->action('AdsController@edit', [
                $special->ad_id,
                'tab' => 'products',
                'subtab' => $special->department()->first()->id,
            ])
            ->with(['message', [
                'type'    => 'success',
                'message' => 'Special updated successfully',
            ],
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Special::destroy($id);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Special deleted successfully',
        ]);

        return back();
    }

    /**
     *  Display the clone view.
     */
    public function clone($id)
    {
        $special = Special::findOrFail($id);

        return view('specials.clone', compact('special'));
    }

    /**
     *  Clone the item into a diferent Ad.
     */
    public function cloning(Request $request, $id)
    {
        $this->validate($request, [
            'ad' => 'required',
        ]);

        $special = Special::find($id);

        $clone = $special->replicate();

        $clone->ad_id = $request->ad;

        $clone->position = null;

        $clone->position_number = null;

        $clone->image = '';

        $image_file = Special::where('sku', $clone->sku)->whereNotIn('image', ['', 'pending-image.jpg'])
                                ->orderBy('created_at', 'desc')->first();

        if (! empty($image_file)) {
            $image = basename($image_file->image);

            $clone->image = $image;
        }

        $clone->save();

        $clone->department()->attach($special->department()->select('department_id')->pluck('department_id')->toArray());

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Item has been cloned successfully!',
        ]);

        return redirect()->action('AdsController@edit', $clone->ad_id);
    }

    /**
     *  Multiple Cloning.
     */
    public function multiple_cloning(Request $request)
    {
        $this->validate($request, [
            'ads' => 'required',
            'clone_items' => 'required',
        ]);

        $items = explode(',', $request->clone_items);

        foreach ($request->ads as $ad) {
            $ad = Ad::find($ad);

            foreach ($items as $item) {
                $old = Special::find($item);

                $new = $old->replicate();

                $new->ad_id = $ad->id;

                $new->position = null;

                $new->position_number = null;

                $new->save();

                $new->department()->attach($old->department()->select('department_id')->pluck('department_id')->toArray());
            }
        }

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Items cloned successfully',
        ]);

        return redirect()->action('AdsController@edit', $ad->id);
    }

    /**
     *  Specials Search.
     */
    public function search()
    {
        if (isset($_GET['keyword'])) {
            $specials = Special::where(function ($q) {
                $q->where('item_description', 'like', '%'.$_GET['keyword'].'%')
                        ->orWhere('sku', 'like', '%'.$_GET['keyword'].'%');
            })
                ->orderBy('created_at', 'desc')
                ->get();
        }

        return view('specials.search', compact('specials'));
    }

    /**
     *  UPCs Search.
     */
    public function upcs_search(Request $request)
    {
        $this->validate($request, [
            'search_term' => 'required',
        ]);

        $products = Product::where(function ($q) use ($request) {
            $q->where('upc', 'like', '%'.$request->search_term.'%')
                ->orWhere('name', 'like', '%'.$request->search_term.'%')
                ->orWhere('brand_name', 'like', '%'.$request->search_term.'%');
        })
            ->get();

        return view('specials.upcs-search', compact('products'));
    }

    /**
     *  Make Promo From Special.
     */
    public function make_promo($id)
    {
        $special = Special::findOrFail($id);
        $product = Product::where('upc', $special->sku)->get();
        $promo = $special;

        $promo->upc = $special->sku;
        $promo->img_ava = $promo->image;

        return view('promos.special-promo', compact('promo'));
    }

    /**
     *  Make Special Image.
     */
    public function make_special_img($id, $img)
    {
        $special = Special::find($id);

        $product = Product::find($img);

        $filename = uniqid().'.'.explode('.', basename($product->image))[1];

        Storage::put('specials/'.$filename, file_get_contents($product->image), 'public');

        $special->image = $filename;

        $special->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Special image updated successfully',
        ]);

        return back();
    }

    /**
     *  Import Form.
     */
    public function import_form()
    {
        return view('specials.import');
    }

    /**
     *  Import From Excel.
     */
    public function import(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|mimes:xls,xlsx',
        ]);

        Excel::import(new SpecialsImport, request()->file('file'));

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Product imported successfully',
        ]);

        return back();
    }

    /**
     *  Digital Signage Image.
     */
    public function digital_signage_image($id, $type, $url = null)
    {
        $special = Special::find($id);

        $product = $special->item_description;

        if ($special->alt_description != null) {
            $product = $special->alt_description;
        }

        $product_name = ['144px', '86px', '36px'];

        if ((strlen($product) > 48)) {
            $product_name = ['86px', '52px', '22px'];
        }

        $format = [

            'digital-signage' => [
                'width' => '1920px',
                'height' => '1080px',
                'image' => '510px',
                'product' => '1410px',
                'product-name' => $product_name[0],
                'price' => '432px',
                'uom' => '72px',
            ],

            'facebook' => [
                'width' => '1200px',
                'height' => '710px',
                'image' => '340px',
                'product' => '860px',
                'product-name' => $product_name[1],
                'price' => '259px',
                'uom' => '43px',
            ],

            'billboard' => [
                'width' => '500px',
                'height' => '300px',
                'image' => '140px',
                'product' => '360px',
                'product-name' => $product_name[2],
                'price' => '108px',
                'uom' => '18px',
            ],

        ];

        $html = "
            
            <div class='digital-signage' style='background-size: cover;'>
                <div class='image' style='background: url(".$special->image."); background-repeat: no-repeat; background-color: white; background-position: center bottom; background-size: cover;'></div>
                <div class='product'>
                    <div class='product-name'>".$product."</div>
                    <div class='price'>".$special->price_tag."</div>
                    <div class='uom'>".$special->size.'</div>
                </div>
            </div>
            
        ';

        $css = '
        
            .digital-signage { 
                width: '.$format[$type]['width'].'; 
                height: '.$format[$type]['height']."; 
                font-family: 'sans-serif'; 
                background: url(https://sara.buyforlessok.com/img/digital-signage/".$special->ad->brands->id.'.jpg);
                position: relative;
            }
                
            .image{
                width: '.$format[$type]['image'].';
                height: '.$format[$type]['height'].';
                position: absolute;
                top: 0;
                left: 0;
            }
            
            .product{
                display: flex;
                justify-content: center;
                flex-direction: column;
                width: '.$format[$type]['product'].';
                height: '.$format[$type]['height'].';
                position: absolute;
                top: 0;
                left: '.$format[$type]['image'].";
                text-align:center;
                font-family: 'Oswald', sans-serif;
                line-height: 1.1;
                text-transform: uppercase;
                font-weight: bold;
            }
            
            .product-name{
                font-size: ".$format[$type]['product-name'].';
            }
            
            .price{
                font-size: '.$format[$type]['price'].';
                color: #c32032;
            }
            
            .uom{
                font-size: '.$format[$type]['uom'].';
            }
            
            sup{
              vertical-align: top;
              font-size: .5em;
            }
            
        ';

        $client = new Client();

        // Retrieve your user_id and api_key from https://htmlcsstoimage.com/dashboard
        $res = $client->request('POST', 'https://hcti.io/v1/image', [
            'auth' => [env('HCTI_API_USER_ID'), env('HCTI_API_KEY')],
            'form_params' => ['html' => $html, 'css' => $css, 'google_fonts' => 'Oswald'],
        ]);

        if ($url != null) {
            return json_decode($res->getBody())->url;
        }

        return redirect()->to(json_decode($res->getBody())->url.'?width='.str_replace('px', '', $format[$type]['width']));
    }

    /**
     *  Export To Gallery.
     *
     *  This exports featured special items and put them into the selected gallery item
     */
    public function export_to_gallery(Request $request)
    {
        $this->validate($request, [
            'ad' => 'required',
            'gallery' => 'required',
            'formatting' => 'required',
        ]);

        $ad = Ad::find(request()->ad);

        $gallery = Gallery::find(request()->gallery);

        $format = request()->formatting;

        $count = 0;

        foreach ($ad->specials->where('featured', 1) as $item) {
            $filename = uniqid().'.png';

            $url = 1;

            Storage::put('gallery_images/'.$filename, file_get_contents($this->digital_signage_image($item->id, $format, $url)), 'public');

            $image = GalleryImage::create([
                'gallery_id' => $gallery->id,
                'file'       => $filename,
                'link_url'   => '#',
                'start_date' => $item->ad->date_from,
                'end_date'   => $item->ad->date_to,
                'display_order' => $item->position_number,
                'mobile_file' => $filename,
            ]);

            $count++;
        }

        return back()
            ->with([
                'message' => [
                    'type'    => 'success',
                    'message' => $count.' items has been exported successfully',
                ],
            ]);
    }
}
