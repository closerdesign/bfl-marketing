<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Product;
use App\Special;
use App\SpecialUpc;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SpecialUpcsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        Special::findOrFail($id);

        $this->validate($request, [
           'upcs' => 'required',
        ]);

        if ($request->ajax == true) {
            $request->upcs = json_decode($request->upcs);

            $ct = Product::where('upc', $request->upcs)->count();

            $chk = SpecialUpc::where('special_id', $id)->where('upc', $request->upcs)->count();

            if (($ct >= 1) && ($chk < 1)) {
                $new = new SpecialUpc([
                    'special_id' => $id,
                    'upc' => $request->upcs,
                ]);

                $new->save();

                return $new;
            }

            return response('success', 200);
        }

        $upcs = explode("\n", trim($request->upcs));

        $count = 0;

        foreach ($upcs as $upc) {
            $upc = preg_replace("/\r|\n/", '', $upc);

            $ct = Product::where('upc', $upc)->count();

            $chk = SpecialUpc::where('special_id', $id)->where('upc', $upc)->count();

            if (($ct >= 1) && ($chk < 1)) {
                $new = new SpecialUpc([
                    'special_id' => $id,
                    'upc' => $upc,
                ]);

                $new->save();

                $count++;
            }
        }

        Session::flash('message', [
            'type'    => 'success',
            'message' => $count.' UPCs Added Successfully',
        ]);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $special = Special::findOrFail($id);

        return view('special-upcs.show', compact('special'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax == 1) {
            $id = $request->upcs;
        }

        SpecialUpc::destroy($id);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'UPC deleted successfully',
        ]);

        if ($request->ajax == 1) {
            return response('success', 200);
        } else {
            return back();
        }
    }
}
