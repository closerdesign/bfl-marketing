<?php

namespace App\Http\Controllers;

use App\DonationRequest;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class DonationRequestsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['store']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $today = date('Y-m-d');

        $requests = DonationRequest::where('event_date', '>', $today)->orderBy('event_date', 'desc')->paginate(15);

        if (isset($_GET['keyword'])) {
            $requests = DonationRequest::where(function ($query) {
                $query->where('brand', 'like', '%'.$_GET['keyword'].'%')
                        ->orwhere('organization', 'like', '%'.$_GET['keyword'].'%')
                        ->orwhere('website', 'like', '%'.$_GET['keyword'].'%')
                        ->orwhere('org_phone', 'like', '%'.$_GET['keyword'].'%')
                        ->orwhere('address', 'like', '%'.$_GET['keyword'].'%')
                        ->orwhere('nonprofit_number', 'like', '%'.$_GET['keyword'].'%')
                        ->orwhere('org_mission', 'like', '%'.$_GET['keyword'].'%')
                        ->orwhere('event', 'like', '%'.$_GET['keyword'].'%')
                        ->orwhere('event_website', 'like', '%'.$_GET['keyword'].'%')
                        ->orwhere('event_date', 'like', '%'.$_GET['keyword'].'%')
                        ->orwhere('event_time', 'like', '%'.$_GET['keyword'].'%')
                        ->orwhere('pickup_date', 'like', '%'.$_GET['keyword'].'%')
                        ->orwhere('event_location', 'like', '%'.$_GET['keyword'].'%')
                        ->orwhere('beneficiaries', 'like', '%'.$_GET['keyword'].'%')
                        ->orwhere('request_type', 'like', '%'.$_GET['keyword'].'%')
                        ->orwhere('items', 'like', '%'.$_GET['keyword'].'%')
                        ->orwhere('additional_info', 'like', '%'.$_GET['keyword'].'%')
                        ->orwhere('first_name', 'like', '%'.$_GET['keyword'].'%')
                        ->orwhere('last_name', 'like', '%'.$_GET['keyword'].'%')
                        ->orwhere('email', 'like', '%'.$_GET['keyword'].'%')
                        ->orwhere('phone', 'like', '%'.$_GET['keyword'].'%')
                        ->orwhere('location', 'like', '%'.$_GET['keyword'].'%')
                        ->orwhere('referral_source', 'like', '%'.$_GET['keyword'].'%');
            })
                ->orderBy('event_date', 'desc')
                ->paginate(15);
        }

        return view('donation-requests.index', compact('requests'));
    }

    /**
     * Show all donation requests, including past.
     */
    public function all()
    {
        $requests = DonationRequest::orderBy('event_date')->paginate(15);

        return view('donation-requests.index', compact('requests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = file_get_contents('php://input');

        $input = json_decode($input, true);

        $donation = new DonationRequest($input);

        Mail::send('emails.donation-requests.confirmation', ['donation' => $donation], function ($m) use ($donation) {
            $m->from('donations@buyforlessok.com', 'Donations and Sponsorship Team (Do Not Reply)');

            $m->to($donation->email)
                ->subject('Buy For Less Corporation – Confirmation of D&S Submission');
        });

        Mail::send('emails.donation-requests.request', ['donation' => $donation], function ($m) use ($donation) {
            $m->from('donations@buyforlessok.com', 'BFL Media');

            $m->to('donations@buyforlessok.com')
                ->subject($donation->brand.' - Donation Request From '.$donation->organization);
        });

        if ($donation->nonprofit == 'Yes') {
            $donation->nonprofit = 1;
        }
        if ($donation->prev_donation == 'Yes') {
            $donation->prev_donation = 1;
        }
        if ($donation->bfl_employees == 'Yes') {
            $donation->bfl_employees = 1;
        }
        if ($donation->advertising == 'Yes') {
            $donation->advertising = 1;
        }

        $donation->save();

        return response('success', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $request = DonationRequest::findOrFail($id);

        return view('donation-requests.show', compact('request'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'status' => 'required',
        ]);

        $status = $request->status;

        $donation = DonationRequest::findOrFail($id);

        $donation->fill($request->all());

        $donation->status = $status;

        $donation->save();

        if ($donation->status == 'Declined') {
            Mail::send('emails.donation-requests.declined', ['donation' => $donation], function ($m) use ($donation) {
                $m->from('donations@buyforlessok.com', 'Donation and Sponsorship Team (Do Not Reply)');

                $m->to($donation->email)
                    ->subject('Buy For Less Corporation – D&S Request Update');
            });
        }

        Session::flash('message', [
            'type'    => 'success',
            'message' => $donation->organization.'\'s Request status was updated successfully.',
        ]);

        return redirect()->action('DonationRequestsController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
