<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Newsletter;
use App\NewsletterImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class NewsletterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['display']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $newsletters = Newsletter::orderBy('created_at', 'desc')->paginate(15);

        return view('newsletters.index', compact('newsletters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'brand_id'  => 'required',
            'date_from' => 'required',
            'date_to'   => 'required',
            'file'      => 'required|mimes:pdf|max:10000',
        ]);

        $filename = '';

        $extension = $request->file('file')->getClientOriginalExtension();

        $filename = uniqid().'.'.$extension;

        Storage::put('newsletters/'.$filename, file_get_contents($request->file('file')->getRealPath()), 'public');

        $newsletter = new Newsletter($request->all());

        $newsletter->file = $filename;

        $newsletter->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Your newsletter has been added.',
        ]);

        return redirect()->action('NewsletterController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $newsletter = Newsletter::findOrFail($id);
        $images = NewsletterImage::where('newsletter_id', $id)->get();

        return view('newsletters.show', compact('newsletter', 'images'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'brand_id'  => 'required',
            'date_from' => 'required',
            'date_to'   => 'required',
            'file'      => 'mimes:pdf|max:10000',
        ]);

        $newsletter = Newsletter::findOrFail($id);

        $filename = basename($newsletter->file);

        $newsletter->fill($request->all());

        if ($request->hasFile('file')) {
            $filename = uniqid().'.'.$request->file('file')->getClientOriginalExtension();

            Storage::put('newsletters/'.$filename, file_get_contents($request->file('file')->getRealPath()), 'public');
        }

        $newsletter->file = $filename;

        $newsletter->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Newsletter Updated Successfully!',
        ]);

        return redirect()->action('NewsletterController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $newsletter = Newsletter::findOrFail($id);

        $newsletter->destroy($id);

        Session::flash('message', [
            'type' => 'success',
            'message' => 'Newsletter has been successfully deleted.',
        ]);

        return back();
    }

    /**
     *  API call to display on websites.
     */
    public function display($brand)
    {
        $newsletters = Newsletter::where('brand_id', $brand)
            ->where('date_from', '<=', date('Y-m-d'))
            ->where('date_to', '>=', date('Y-m-d'))
            ->orderBy('date_from', 'desc')
            ->get();

        return response($newsletters, 200);
    }
}
