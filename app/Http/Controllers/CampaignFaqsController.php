<?php

namespace App\Http\Controllers;

use App\CampaignFaq;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CampaignFaqsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $faqs = CampaignFaq::all();

        return view('campaign-faqs.index', compact('faqs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('campaign-faqs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'question',
            'answer',
        ]);

        $faq = new CampaignFaq($request->all());

        $faq->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'FAQ saved successfully',
        ]);

        return redirect()->action('CampaignFaqsController@index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $faq = CampaignFaq::findOrFail($id);

        return view('campaign-faqs.edit', compact('faq'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'question',
            'answer',
        ]);

        $faq = CampaignFaq::findOrFail($id);

        $faq->fill($request->all());

        $faq->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'FAQ updated successfully',
        ]);

        return redirect()->action('CampaignFaqsController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CampaignFaq::destroy($id);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'FAQ removed successfully',
        ]);

        return back();
    }
}
