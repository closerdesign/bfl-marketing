<?php

namespace App\Http\Controllers;

use App\Event;
use App\EventSignup;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class EventsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::current()->get();

        return view('events.index', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('events.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'        => 'required',
            'image'       => 'mimes:jpeg,png,gif',
            'intro'       => 'required',
            'description' => 'required',
            'date'        => 'required|date',
            'time'        => 'required',
            'brand_id'    => 'required|numeric',
            'status'      => 'required|boolean',
        ]);

        if ($request->hasFile('image')) {
            $filename = uniqid().'.'.$request->file('image')->getClientOriginalExtension();
            Storage::put('events/'.$filename, file_get_contents($request->file('image')->getRealPath()), 'public');
            $event = new Event($request->all());
            $event->image = $filename;
        } else {
            $event = new Event($request->all());
            $event->image = 'pending-image.jpg';
        }

        $event->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Event saved successfully',
        ]);

        return redirect()->action('EventsController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $events = Event::current($id)->where('brand_id', $id)->where('status', 1)->get();

        return response($events, 200);
    }

    public function show_all()
    {
        $events = Event::all();

        return view('events.index', compact('events'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = Event::findOrFail($id);

        return view('events.edit', compact('event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'        => 'required',
            'image'       => 'mimes:jpeg,png,gif',
            'intro'       => 'required',
            'description' => 'required',
            'date'        => 'required|date',
            'time'        => 'required',
            'brand_id'    => 'required|numeric',
            'status'      => 'required|boolean',
        ]);

        $event = Event::findOrFail($id);

        $filename = basename($event->image);

        if ($request->hasFile('image')) {
            $filename = uniqid().'.'.$request->file('image')->getClientOriginalExtension();

            Storage::put('events/'.$filename, file_get_contents($request->file('image')->getRealPath()), 'public');
        }

        $event->fill($request->all());

        $event->image = $filename;

        $event->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Event updated successfully',
        ]);

        return redirect('/events');
    }

    /**
     * Confirm deletion for the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $event = Event::findOrFail($id);

        return view('events.delete', compact('event'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Event::destroy($id);

        Session::flash('message', [
            'type' => 'success',
            'message' => 'Event deleted successfully',
        ]);

        return redirect('/events');
    }

    /**
     *  Clone Event.
     */
    public function clone($id)
    {
        $event = Event::findOrFail($id);

        $clone = new Event($event->toArray());

        $clone->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Event cloned successfully.',
        ]);

        return back();
    }

    public function view_signups($id)
    {
        $event = Event::findOrFail($id);

        $signups = EventSignup::where('event_id', $id)->get();

        $email_list = '';

        foreach ($signups as $signup) {
            $email_list = $email_list.$signup->email.', ';
        }

        return view('events.signups', compact('event', 'signups', 'email_list'));
    }
}
