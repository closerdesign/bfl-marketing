<?php

namespace App\Http\Controllers;

use App\CtwScore;
use Illuminate\Http\Request;

class CtwScoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'evaluation_id' => 'required|integer',
            'topic_id' => 'required|integer',
            'score' => 'required|integer',
        ]);

        $score = CtwScore::updateOrCreate(
            [
                'evaluation_id' => request()->evaluation_id,
                'topic_id' => request()->topic_id,
            ],
            [
                'score' => request()->score,
                'comments' => request()->comments,
            ]
        );

        return $score;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CtwScore  $ctwScore
     * @return \Illuminate\Http\Response
     */
    public function show(CtwScore $ctwScore)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CtwScore  $ctwScore
     * @return \Illuminate\Http\Response
     */
    public function edit(CtwScore $ctwScore)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CtwScore  $ctwScore
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CtwScore $ctwScore)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CtwScore  $ctwScore
     * @return \Illuminate\Http\Response
     */
    public function destroy(CtwScore $ctwScore)
    {
        //
    }

    /**
     *  Check.
     */
    public function check($evaluation, $topic)
    {
        $score = CtwScore::where(['evaluation_id' => $evaluation, 'topic_id' => $topic])->first();

        if ($score) {
            return response($score, 200);
        }

        $score['id'] = 0;
        $score['score'] = '';
        $score['comments'] = '';

        return response(json_encode($score), 200);
    }
}
