<?php

namespace App\Http\Controllers;

use App\AwgPrice;
use App\Imports\AwgPriceImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class AwgPriceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $awg_prices = AwgPrice::where('amount', '>', 0)->get();

        return view('awg-price.index', compact('awg_prices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('awg-price.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'file' => 'required',
        ]);

        DB::table('awg_prices')->truncate();

        Excel::import(new AwgPriceImport(), request()->file('file'));

        return redirect()->action('AwgPriceController@index')->with(['message' => ['type' => 'success', 'message' => 'File imported successfully']]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AwgPrice  $awgPrice
     * @return \Illuminate\Http\Response
     */
    public function show(AwgPrice $awgPrice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AwgPrice  $awgPrice
     * @return \Illuminate\Http\Response
     */
    public function edit(AwgPrice $awgPrice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AwgPrice  $awgPrice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AwgPrice $awgPrice)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AwgPrice  $awgPrice
     * @return \Illuminate\Http\Response
     */
    public function destroy(AwgPrice $awgPrice)
    {
        //
    }
}
