<?php

namespace App\Http\Controllers;

use App\DeliMenuItem;
use Illuminate\Http\Request;

class DeliMenuItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('deli-menu.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DeliMenuItem  $deliMenuItem
     * @return \Illuminate\Http\Response
     */
    public function show(DeliMenuItem $deliMenuItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DeliMenuItem  $deliMenuItem
     * @return \Illuminate\Http\Response
     */
    public function edit(DeliMenuItem $deliMenuItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DeliMenuItem  $deliMenuItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DeliMenuItem $deliMenuItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DeliMenuItem  $deliMenuItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeliMenuItem $deliMenuItem)
    {
        //
    }
}
