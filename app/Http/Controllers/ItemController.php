<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ItemController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::all();

        return view('item.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('item.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'stock' => 'required|integer',
            'unit_cost' => 'required|regex:/^\d+(\.\d{1,2})?$/',
        ]);

        $item = new Item($request->all());

        $item->save();

        Session::flash('message', ['type' => 'success', 'message' => 'The item has been saved.']);

        return redirect()->action('ItemController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        return view('item.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        $this->validate($request, [
            'name' => 'required',
            'stock' => 'required|integer',
            'unit_cost' => 'required|regex:/^\d+(\.\d{1,2})?$/',
        ]);

        $item->fill($request->all());

        $item->save();

        Session::flash('message', ['type' => 'success', 'message' => 'The item has been saved.']);

        return redirect()->action('ItemController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        if ($item->stock > 0) {
            Session::flash('message', ['type' => 'alert', 'message' => 'Items with actual stock can not be removed.']);
        } elseif ($item->has('transactions')) {
            Session::flash('message', ['type' => 'alert', 'message' => 'When items have movement can not be removed.']);
        } else {
            $item->delete();

            Session::flash('message', ['type' => 'success', 'message' => 'Item has been removed.']);
        }

        return back();
    }
}
