<?php

namespace App\Http\Controllers;

use App\Charts\TextSubscribers;
use Colors\RandomColor;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->store != null) {
            return redirect()->action('StoreLevelController@home');
        }

        return view('welcome');
    }
}
