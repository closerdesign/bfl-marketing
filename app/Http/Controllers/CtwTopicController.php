<?php

namespace App\Http\Controllers;

use App\CtwTopic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class CtwTopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $this->validate($request, [
            'topic' => 'required',
        ]);

        $topic = new CtwTopic(request()->all());
        $topic->ctw_area_id = $id;
        $topic->save();

        return back()
            ->with(['message' => [
                'type' => 'success',
                'message' => Lang::get('ctw-topic.topic').': '.Lang::get('general.saved'),
            ]]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CtwTopic  $ctwTopic
     * @return \Illuminate\Http\Response
     */
    public function show(CtwTopic $ctwTopic)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CtwTopic  $ctwTopic
     * @return \Illuminate\Http\Response
     */
    public function edit(CtwTopic $ctwTopic)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CtwTopic  $ctwTopic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CtwTopic $ctwTopic)
    {
        $this->validate($request, [
            'topic' => 'required',
        ]);

        $ctwTopic->fill(request()->all());
        $ctwTopic->save();

        return back()
            ->with([
                'message' => [
                    'type'    => 'success',
                    'message' => Lang::get('ctw-topic.topic').': '.Lang::get('general.updated'),
                ],
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CtwTopic  $ctwTopic
     * @return \Illuminate\Http\Response
     */
    public function destroy(CtwTopic $ctwTopic)
    {
        //
    }
}
