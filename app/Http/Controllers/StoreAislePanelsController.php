<?php

namespace App\Http\Controllers;

use App\Store;
use App\StoreAisle;
use App\StoreAislePanels;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class StoreAislePanelsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $aisle = StoreAisle::where('id', $id)->first();
        $all_aisles = StoreAisle::all();

        return view('panels.index', compact('aisle', 'all_aisles'));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $panel = new StoreAislePanels($request->all());
        $panel->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Aisle added succesfully',
        ]);

        return $this->index($request->aisle_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $panel = StoreAislePanels::findOrFail($id);
        $aisle = StoreAisle::where('id', $panel->aisle_id)->first();
        $all_aisles = StoreAisle::where('store_id', $aisle->store_id)->orderBy('aisle_name')->get();
        $store = Store::findOrFail($aisle->store_id);

        return view('panels.edit', compact('panel', 'all_aisles', 'store'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'panel_name' => 'required',
        ]);

        $panel = StoreAislePanels::findOrFail($id);

        $panel->fill($request->all());

        $panel->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Panel updated successfully',
        ]);

        return redirect()->action('StoreAislePanelsController@index', $panel->aisle_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_panel(Request $request, $id)
    {
        StoreAislePanels::destroy($id);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Panel deleted successfully',
        ]);

        return back();
    }
}
