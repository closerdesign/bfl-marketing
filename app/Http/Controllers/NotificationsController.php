<?php

namespace App\Http\Controllers;

use App\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class NotificationsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $notifications = Notification::where('name', '!=', 'Kitchen Duty')->get();

        $days = $this->days();

        return view('notifications.index', compact('notifications', 'days'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'notification_to' => 'email',
        ]);

        $notification = new  Notification($request->all());

        $notification->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Notification has been saved successfully',
        ]);

        return redirect()->action('NotificationsController@index');
    }

    public function destroy($id)
    {
        Notification::destroy($id);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Notification removed sucessfully',
        ]);

        // return redirect()->action('NotificationsController@index');
        return back();
    }

    public function days()
    {
        $days = [1=>'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday', 7 => 'Sunday'];

        return $days;
    }

    public function kitchen_days()
    {
        $kitchen_notifications = Notification::where('name', 'Kitchen Duty')->where('send_date', '>=', date('Y-m-d'))->get();

        // dd($kitchen_notifications);

        return view('notifications.kitchen', compact('kitchen_notifications'));
    }

    public function store_kitchen(Request $request)
    {
        $notification = new  Notification($request->all());

        $notification->notification_text = 'Greetings, This is a friendly reminder you have been assigned Kitchen Duties for this week.';
        $notification->send_date_type = 'once';
        $notification->send_time = '08:00:00';
        $notification->name = 'Kitchen Duty';
        $notification->send_date = $request->send_date;

        $notification->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Kitchen Duty Added',
        ]);

        return back();
    }
}
