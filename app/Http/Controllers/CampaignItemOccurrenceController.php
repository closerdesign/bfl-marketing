<?php

namespace App\Http\Controllers;

use App\CampaignItem;
use App\CampaignItemOccurrence;
use App\Choice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CampaignItemOccurrenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'campaign_item_id' => 'required|integer',
        ]);

        $item = CampaignItem::findOrFail($request->campaign_item_id);

        $itemOccurrence = new CampaignItemOccurrence($request->all());

        $itemOccurrence->event_date_time = date('Y-m-d H:i', strtotime("$request->event_date $request->event_time"));

        $itemOccurrence->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Occurrence saved successfully',
        ]);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CampaignItemOccurrence::destroy($id);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Occurrence removed successfully',
        ]);

        return back();
    }
}
