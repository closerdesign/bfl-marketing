<?php

namespace App\Http\Controllers;

use App\Roc;
use Illuminate\Http\Request;

class RocController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rocs = Roc::whereHas('entries')->paginate(10);

        return view('roc.index', compact('rocs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Roc  $roc
     * @return \Illuminate\Http\Response
     */
    public function show(Roc $roc)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Roc  $roc
     * @return \Illuminate\Http\Response
     */
    public function edit(Roc $roc)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Roc  $roc
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Roc $roc)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Roc  $roc
     * @return \Illuminate\Http\Response
     */
    public function destroy(Roc $roc)
    {
        //
    }
}
