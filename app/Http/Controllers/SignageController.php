<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\SignageRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use PDFShift\PDFShift;

class SignageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['bins_template', 'bins_pdf']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('signage.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'store' => 'required|integer',
            'date_needed' => 'required',
            'amount' => 'required',
            'size' => 'required',
            'receive' => 'required',
            'contents' => 'required',
        ]);

        $signage_request = new SignageRequest($request->all());

        $signage_request->save();

        Mail::send('emails.signage', ['request' => $request], function ($m) use ($request) {
            $m->from($request->email, $request->name);

            $m->to('help@buyforlessok.com')
                ->replyTo($request->email)
                ->subject('Signage Request for '.$request->store);
        });

        Session::flash('message', [
            'type' => 'success',
            'message' => 'Your request has been sent successfully. Thank you!',
        ]);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     *  Deli Signs Form.
     */
    public function deli_sign()
    {
        return view('signage.deli-sign');
    }

    /**
     *  Deli Template Signs.
     */
    public function deli_template()
    {
        return view('signage.deli-sign-md');
    }

    /**
     *  Deli PDF.
     */
    public function deli_pdf()
    {
        $curl = curl_init();

        $text = $_GET['text'];

        if (isset($_GET['price'])) {
            $text = $text.'<br />'.$_GET['price'];
        }

        $source = action('SignageController@deli_template', ['text' => $text]);

        curl_setopt_array($curl, [
            CURLOPT_URL => 'https://api.pdfshift.io/v2/convert/',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => json_encode(['source' => $source, 'use_print' => false, 'format' => 'Letter']),
            CURLOPT_HTTPHEADER => ['Content-Type:application/json'],
            CURLOPT_USERPWD => env('PDFSHIFT'),
        ]);

        $response = curl_exec($curl);

        $filename = uniqid().'.pdf';

        return \Illuminate\Support\Facades\Response::make($response, 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="'.$filename.'"',
        ]);
    }

    /**
     *  Bins Form.
     */
    public function bins_form()
    {
        return view('signage.bins-form');
    }

    /**
     *  85by11 Template.
     */
    public function bins_template()
    {
        $product_name = $_GET['product_name'];
        $product_description = $_GET['product_description'] ?? '';
        $price = $_GET['price'];
        $size = $_GET['size'];
        $background = $_GET['background'];

        return view('signage.bins-template', compact('product_name', 'product_description', 'price', 'size', 'background'));
    }

    /**
     *  Bins PDF.
     */
    public function bins_pdf()
    {
        $curl = curl_init();

        $product_name = $_GET['product_name'];
        $product_description = $_GET['product_description'] ?? '';
        $price = $_GET['price'];
        $size = $_GET['size'];
        $background = $_GET['background'];

        $source = action('SignageController@bins_template', [
            'product_name' => $product_name,
            'product_description' => $product_description,
            'price' => $price,
            'size' => $size,
            'background' => $background,
        ]);

        curl_setopt_array($curl, [
            CURLOPT_URL => 'https://api.pdfshift.io/v2/convert/',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => json_encode(['source' => $source, 'use_print' => false, 'format' => 'Letter', 'landscape' => true]),
            CURLOPT_HTTPHEADER => ['Content-Type:application/json'],
            CURLOPT_USERPWD => env('PDFSHIFT'),
        ]);

        $response = curl_exec($curl);

        $filename = uniqid().'.pdf';

        return \Illuminate\Support\Facades\Response::make($response, 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="'.$filename.'"',
        ]);
    }
}
