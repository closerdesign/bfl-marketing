<?php

namespace App\Http\Controllers;

use App\PromoMovement;
use Illuminate\Http\Request;

class PromoMovementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $promos = null;
        $start = null;
        $end = null;
        $stores = null;

        if (isset($_GET['promo']) && isset($_GET['start']) && isset($_GET['end'])) {
            $promos = PromoMovement::where('mmbr_prom_id', $_GET['promo'])
                ->whereBetween('dt', [$_GET['start'], $_GET['end']])
                ->get();

            $stores = PromoMovement::where('mmbr_prom_id', $_GET['promo'])
                ->whereBetween('dt', [$_GET['start'], $_GET['end']])
                ->select('store', 'mmbr_prom_id', 'prom_desc', 'strt_date', 'end_date')
                ->distinct('store')
                ->get();
        }

        return view('promo-movement.index', compact('promos', 'start', 'end', 'stores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PromoMovement  $promoMovement
     * @return \Illuminate\Http\Response
     */
    public function show(PromoMovement $promoMovement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PromoMovement  $promoMovement
     * @return \Illuminate\Http\Response
     */
    public function edit(PromoMovement $promoMovement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PromoMovement  $promoMovement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PromoMovement $promoMovement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PromoMovement  $promoMovement
     * @return \Illuminate\Http\Response
     */
    public function destroy(PromoMovement $promoMovement)
    {
        //
    }
}
