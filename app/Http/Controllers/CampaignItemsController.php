<?php

namespace App\Http\Controllers;

use App\CampaignItem;
use App\Http\Requests;
use App\Store;
use function foo\func;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class CampaignItemsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'classes_events']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = CampaignItem::orderBy('name');

        if (isset($_GET['keyword'])) {
            $items = $items->where('name', 'like', '%'.$_GET['keyword'].'%');
        }

        $items = $items->paginate(50);

        return view('campaign_items.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('campaign_items.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'        => 'required',
            'image'       => 'image|max:1000',
            'price'       => 'required',
            'status'      => 'required|boolean',
        ]);

        if ($request->file('image')) {
            $filename = uniqid().'.'.$request->file('image')->getClientOriginalExtension();

            Storage::put('campaign-items/'.$filename, file_get_contents($request->file('image')->getRealPath()), 'public');

            $item = new CampaignItem($request->all());

            $item->image = $filename;
        } else {
            $item = new CampaignItem($request->all());
        }

        $item->save();

        $item->campaign_categories()->attach($request->campaign_category_id);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Item saved successfully',
        ]);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = CampaignItem::findOrFail($id);

        return view('campaign_items.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'        => 'required',
            'image'       => 'image|max:1000',
            'price'       => 'required',
            'status'      => 'required|boolean',
        ]);

        $item = CampaignItem::findOrFail($id);

        $filename = basename($item->image);

        if ($request->file('image')) {
            $filename = uniqid().'.'.$request->file('image')->getClientOriginalExtension();

            Storage::put('campaign-items/'.$filename, file_get_contents($request->file('image')->getRealPath()), 'public');
        }

        $item->fill($request->all());

        $item->image = $filename;

        $item->save();

        $item->campaign_categories()->sync($request->campaign_category_id);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Item updated successfully',
        ]);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CampaignItem::destroy($id);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Item removed successfully',
        ]);

        return back();
    }

    /**
     *  Duplicate Resource.
     */
    public function duplicate($id)
    {
        $item = CampaignItem::find($id);

        $new = $item->replicate();

        $new->status = 0;

        $new->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Item duplicated successfully.',
        ]);

        return back();
    }

    /**
     *  Classes Events.
     */
    public function classes_events($brand)
    {
        $items = CampaignItem::where('date', '!=', null)
            ->where('date', '>=', date('Y-m-d'))
            ->join('stores', 'stores.id', '=', 'campaign_items.store_id')
            ->where('stores.brand_id', '=', $brand)
            ->get();

        return response($items, 200);
    }
}
