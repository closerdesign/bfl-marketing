<?php

namespace App\Http\Controllers;

use App\Store;
use App\StoreAisle;
use App\StoreAislePanels;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class StoreAisleController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * Pass Store ID as all Aisles are dependant on store
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $store = Store::where('id', $id)->with('aisles')->first();

        return view('aisles.index', compact('store'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        StoreAisle::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $aisle = new StoreAisle($request->all());
        $aisle->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Aisle added succesfully',
        ]);

        return $this->index($request->store_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $aisle = StoreAisle::findOrFail($id);

        return view('aisles.edit', compact('aisle'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_aisle(Request $request, $id)
    {
        $this->validate($request, [
            'aisle_name'    => 'required',

        ]);

        $aisle = StoreAisle::findOrFail($id);

        $aisle->fill($request->all());

        $aisle->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Aisle Updated Successfully!',
        ]);

        return redirect()->action('StoreAisleController@index', $aisle->store_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $panels = StoreAislePanels::where('aisle_id', $id)->first();
        if (! empty($panels)) {
            Session::flash('message', [
                'type'    => 'danger',
                'message' => 'Aisle has panels associated with it. Panels must not be associated with an aisle in order for the aisle to be deleted',
            ]);

            return back();
        } else {
            StoreAisle::destroy($id);

            Session::flash('message', [
                'type' => 'success',
                'message' => 'Aisle removed succesfully',
            ]);
        }

        return back();
    }
}
