<?php

namespace App\Http\Controllers;

use App\Cashier;
use App\Http\Requests;
use Illuminate\Http\Request;

class CashiersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cashiers = Cashier::whereIn('type', ['Checkout', 'Supervisor', 'Store manager'])
            ->where('name', 'NOT LIKE', '%card%')
            ->orderBy('name')
            ->paginate('25');

        if (isset($_GET['keyword'])) {
            $cashiers = Cashier::whereIn('type', ['Checkout', 'Supervisor', 'Store manager'])
                ->where(function ($q) {
                    $q->where('name', 'NOT LIKE', '%card%')
                        ->where('name', 'LIKE', '%'.$_GET['keyword'].'%');
                })
                ->orderBy('name')
                ->paginate('25');
        }

        return view('cashiers.index', compact('cashiers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
