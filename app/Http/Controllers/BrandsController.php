<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Http\Requests;
use App\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class BrandsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = Brand::all();

        return view('brands.index', compact('brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('brands.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'    => 'required',
            'logo'    => 'required|image|max:1000',
            'website' => 'required',
        ]);

        $filename = uniqid().'.'.$request->file('logo')->getClientOriginalExtension();

        Storage::put('brands/'.$filename, file_get_contents($request->file('logo')->getRealPath()), 'public');

        $brand = new Brand($request->all());

        $brand->logo = $filename;

        $filename = '';

        if (request()->hasFile('icon')) {
            $filename = uniqid().'.'.request()->file('icon')->getClientOriginalExtension();

            Storage::put('brand/icon/'.$filename, file_get_contents(request()->file('icon')->getRealPath()), 'public');
        }

        $brand->icon = $filename;

        $brand->save();

        return redirect()->route('brands.index')
            ->with([
                'message' => [
                    'type'    => 'success',
                    'message' => Lang::get('brands.title').': '.Lang::get('general.saved'),
                ],
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $brand = Brand::with('stores')->find($id);

        return response($brand, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brand = Brand::findOrFail($id);

        return view('brands.edit', compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'    => 'required',
            'logo'    => 'image|max:1000',
            'website' => 'required',
        ]);

        $brand = Brand::findOrFail($id);

        $logo = basename($brand->logo);

        if ($request->hasFile('logo')) {
            $logo = uniqid().'.'.$request->file('logo')->getClientOriginalExtension();

            Storage::put('brands/'.$logo, file_get_contents($request->file('logo')->getRealPath()), 'public');
        }

        $icon = basename($brand->icon);

        if (request()->hasFile('icon')) {
            $icon = uniqid().'.'.request()->file('icon')->getClientOriginalExtension();

            Storage::put('brand/icon/'.$icon, file_get_contents(request()->file('icon')->getRealPath()), 'public');
        }

        $brand->fill($request->all());
        $brand->logo = $logo;
        $brand->icon = $icon;

        $brand->save();

        Session::flash('message', [
            'type' => 'success',
            'message' => 'Brand updated successfully',
        ]);

        return redirect()->action('BrandsController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $brand = Brand::findOrFail($id);

        $brand->destroy($id);

        Session::flash('message', [
            'type' => 'success',
            'message' => 'The brand has been deleted successfully',
        ]);

        return redirect('/brands');
    }

    /**
     * Stores per brand.
     */
    public function brand_stores($id)
    {
        $stores = Store::where('brand_id', $id)->get();

        return response($stores, 200);
    }

    /**
     * Generate Brand Email.
     */
    public function email($id)
    {
        $brand = Brand::findOrFail($id);
        $url = '/email/'.$brand->id;

        switch ($brand->name) {
            case 'Smart Saver':
                return redirect($url.'/smartsaver_basic');

                break;
            case 'Uptown Grocery Co':
                return redirect($url.'/uptown_basic');

                break;
            default:
                return redirect($url.'/buyforless_basic');
        }
    }
}
