<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Recipe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class RecipesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('recipes.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('recipes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'        => 'required',
            'image'       => 'required|mimes:jpeg,png,gif',
            'ingredients' => 'required',
            'directions'  => 'required',
            'status'      => 'required|boolean',
        ]);

        $filename = uniqid().'.'.$request->file('image')->getClientOriginalExtension();

        Storage::put('recipes/'.$filename, file_get_contents($request->file('image')->getRealPath()), 'public');

        $recipe = new Recipe($request->all());

        $recipe->image = $filename;

        $recipe->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Recipe saved successfully',
        ]);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $recipes = Recipe::orderBy('created_at', 'desc')->where('status', '1')->get();

        return response($recipes, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $recipe = Recipe::findOrFail($id);

        return view('recipes.edit', compact('recipe'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'        => 'required',
            'image'       => 'mimes:jpeg,png,gif',
            'ingredients' => 'required',
            'directions'  => 'required',
            'status'      => 'required|boolean',
        ]);

        $recipe = Recipe::findOrFail($id);

        $filename = $recipe->image;

        if ($request->hasFile('image')) {
            $filename = uniqid().'.'.$request->file('image')->getClientOriginalExtension();

            Storage::put('recipes/'.$filename, file_get_contents($request->file('image')->getRealPath()), 'public');
        }

        $recipe->fill($request->all());

        $recipe->image = $filename;

        $recipe->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Recipe updated successfully',
        ]);

        return redirect('/recipes');
    }

    /**
     * Confirm deletion for the the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $recipe = Recipe::findOrFail($id);

        return view('recipes.delete', compact('recipe'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Recipe::destroy($id);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Recipe removed successfully',
        ]);

        return redirect('/recipes');
    }
}
