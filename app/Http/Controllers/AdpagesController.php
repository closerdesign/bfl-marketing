<?php

namespace App\Http\Controllers;

use App\Ad;
use App\Adpage;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class AdpagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'image' => 'required|image|max:1000',
            'ad'    => 'required|numeric',
        ]);

        $extension = $request->file('image')->getClientOriginalExtension();

        $filename = uniqid().'.'.$extension;

        Storage::put('adpages/'.$filename, file_get_contents($request->file('image')->getRealPath()), 'public');

        $adpage = new Adpage($request->all());

        $adpage->image = $filename;

        $adpage->save();

        Session::flash('message', [
            'type' => 'success',
            'message' => 'Ad page uploaded successfully',
        ]);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ad = Ad::findOrFail($id);

        $pages = Adpage::where('ad', $id)->get();

        return view('adpages.show', compact('ad', 'pages'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Adpage::destroy($id);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Ad page deleted successfully',
        ]);

        return back();
    }
}
