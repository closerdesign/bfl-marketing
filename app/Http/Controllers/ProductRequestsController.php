<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Http\Requests;
use App\ProductRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Nexmo\Laravel\Facade\Nexmo;

class ProductRequestsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['store', 'web']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product_requests = ProductRequest::orderBy('created_at', 'desc')->paginate(25);

        return view('product_requests.index', compact('product_requests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $brand = [
            '14058967933' => 1,
            '14058967965' => 2,
            '14058967983' => 3,
            '14058967969' => 4,
        ];

        $product = new ProductRequest($request->all());

        $product->brand_id = $brand[$request->to];

        $product->save();

        $brand = Brand::findOrFail($product->brand_id);

        Nexmo::message()->send([
            'from' => $request->to,
            'to' => $request->msisdn,
            'text' => 'Thank you for your request. We will do our best in order to supply your favorite '.$brand->name.' store with the requested product.',
        ]);

        Mail::send('emails.product-requests.request', ['product' => $product, 'brand' => $brand, 'request' => $request], function ($m) use ($product, $brand, $request) {
            $emails = $brand->stores->map(function ($e) {
                return $e->email;
            })->toArray();

            $m->from('media@buyforlessok.com', 'Media Team');

            $m->to($emails)
                ->cc(['levans@buyforlessok.com', 'hbinkowski@buyforlessok.com', 'jrodriguez@buyforlessok.com', 'rmolder@buyforlessok.com'])
                ->replyTo('media@buyforlessok.com')
                ->subject('['.$brand->name.'] A New Product Request Has Been Received');
        });
    }

    /**
     * Store from Brand Websites.
     */
    public function web(Request $request)
    {
        $input = $request->input();

        $product = new ProductRequest($input);

        $product->save();

        return response('success', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
