<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\ShrinkItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ShrinkItemsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'            => 'required',
            'shrink_group_id' => 'required|integer',
        ]);

        $item = new ShrinkItem($request->all());

        $item->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Shrink Item created successfully',
        ]);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'            => 'required',
            'shrink_group_id' => 'required|integer',
        ]);

        $item = ShrinkItem::findOrFail($id);

        $item->fill($request->all());

        $item->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Shrink Item updated successfully',
        ]);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ShrinkItem::destroy($id);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Shrink Item deleted successfully',
        ]);

        return back();
    }

    /**
     * Display the list of the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function items_list($id)
    {
        $items = ShrinkItem::where('shrink_group_id', $id)->get();

        $list = "<option value=''>Select...</option>";

        foreach ($items as $item) {
            $list .= '<option value="'.$item->id.'">'.$item->name.'</option>';
        }

        return $list;
    }
}
