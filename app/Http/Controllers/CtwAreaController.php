<?php

namespace App\Http\Controllers;

use App\CtwArea;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class CtwAreaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $areas = CtwArea::orderBy('name')->get();

        return view('ctw-area.index', compact('areas'));
    }

    /**
     *  Create.
     */
    public function create()
    {
        return view('ctw-area.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'brands' => 'required',
        ]);

        $area = new CtwArea(request()->all());

        $area->save();

        $area->brands()->attach(request()->brands);

        return redirect()
            ->route('ctw-area.index')
            ->with([
                'message' => [
                    'type'    => 'success',
                    'message' => Lang::get('ctw-area.title').': '.Lang::get('general.saved'),
                ],
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'brands' => 'required',
        ]);

        $area = CtwArea::find($id);

        $area->brands()->sync(request()->brands);

        return back()
            ->with([
                'message' => [
                    'type'    => 'success',
                    'message' => Lang::get('ctw-area.title').': '.Lang::get('general.updated'),
                ],
            ]);
    }
}
