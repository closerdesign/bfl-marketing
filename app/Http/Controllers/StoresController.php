<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Http\Requests;
use App\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class StoresController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['show', 'display', 'store_by_code', 'stores_with_current_ads']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stores = Store::all();

        $brands = Brand::all();

        return view('stores.index', compact('stores', 'brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brands = Brand::all();

        return view('stores.create', compact('brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'                => 'required',
            'address'             => 'required',
            'address2'            => 'required',
            'latitude'            => 'required',
            'longitude'           => 'required',
            'phone'               => 'required',
            'store_manager'       => 'required',
            'email'               => 'required',
            'store_manager_image' => 'required|image|max:1000',
            'brand_id'            => 'required',
            'tax_rate'            => 'required|numeric',
        ]);

        $filename = uniqid().'.'.$request->file('store_manager_image')->getClientOriginalExtension();

        Storage::put('managers/'.$filename, file_get_contents($request->file('store_manager_image')->getRealPath()), 'public');

        $store = new Store($request->all());

        $store->store_manager_image = $filename;

        $store->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Store saved successfully',
        ]);

        return redirect()->action('StoresController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $stores = Store::where(['brand_id' => $id, 'status' => true])->get();

        return response($stores, 200);
    }

    /**
     * Display the specified resource via API.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function display($id)
    {
        $stores = Store::where('store_code', $id)->with('brands')->get();

        return response($stores, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $store = Store::findOrFail($id);

        $brands = Brand::all();

        return view('stores.edit', compact('brands', 'store'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $store = Store::findOrFail($id);

        $this->validate($request, [
            'name'                => 'required',
            'address'             => 'required',
            'address2'            => 'required',
            'latitude'            => 'required',
            'longitude'           => 'required',
            'phone'               => 'required',
            'store_manager'       => 'required',
            'email'               => 'required',
            'store_manager_image' => 'mimes:jpg,jpeg,png,gif',
            'brand_id'            => 'required',
            'tax_rate'            => 'required|numeric',
        ]);

        $input = $request->all();

        $current_img = $store->store_manager_image;

        $store->fill($input);

        $store->store_manager_image = basename($current_img);

        if ($request->hasFile('store_manager_image')) {
            $filename = uniqid().'.'.$request->file('store_manager_image')->getClientOriginalExtension();

            Storage::put('managers/'.$filename, file_get_contents($request->file('store_manager_image')->getRealPath()), 'public');

            $store->store_manager_image = $filename;
        }

        $store->shipt_files = false;

        if ($request->has('shipt_files')) {
            $store->shipt_files = true;
        }

        $store->save();

        Session::flash('message', [
            'type' => 'success',
            'message' => 'Store updated successfully',
        ]);

        return redirect()->action('StoresController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $store = Store::findOrFail($id);

        $store->destroy($id);

        Session::flash('message', [
            'type' => 'success',
            'message' => 'The store has been deleted successfully',
        ]);

        return redirect('/stores');
    }

    /**
     *  Store By Code.
     */
    public function store_by_code($id)
    {
        $store = Store::where('store_code', $id)
            ->with('brands')
            ->first();

        return response($store, 200);
    }

    /**
     *  Stores With Current Ads.
     */
    public function stores_with_current_ads($ids)
    {
        $stores = explode(',', $ids);

        $ads = Store::whereIn('id', $stores)->with('ads.pages', 'ads.files', 'ads.featured', 'brands')->get();

        return response($ads, 200);
    }
}
