<?php

namespace App\Http\Controllers;

use App\Exports\InventoryExport;
use App\Inventory;
use App\RetalixPrice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Maatwebsite\Excel\Facades\Excel;

class InventoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $inventories = Inventory::where('status', 'in progress');

        if (auth()->user()->store != null) {
            $store = \App\Store::where('store_code', auth()->user()->store)->first();

            $inventories = $inventories->where('store_id', $store->id);
        }

        $inventories = $inventories->get();

        return view('inventory.index', compact('inventories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('inventory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'store_id' => 'required|integer',
            'controller_name' => 'required',
        ]);

        $inventory = new Inventory($request->all());

        $inventory->save();

        return redirect()->action('InventoryController@show', $inventory->id)->with([
            'message' => [
                'type' => 'success',
                'message' => Lang::get('inventory.title').': '.Lang::get('general.saved'), ], ]
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function show(Inventory $inventory)
    {
        $id = $inventory->id;

        return view('inventory.show', compact('inventory', 'id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function edit(Inventory $inventory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Inventory $inventory)
    {
        $inventory->fill(request()->all());

        $inventory->save();

        return redirect()->action('InventoryController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Inventory $inventory)
    {
        //
    }

    /**
     *  List.
     */
    public function list()
    {
        $inventories = Inventory::orderBy('created_at', 'desc');

        if (auth()->user()->store != null) {
            $store = \App\Store::where('store_code', auth()->user()->store)->first();

            $inventories = $inventories->where('store_id', $store->id);
        }

        $inventories = $inventories->paginate(20);

        return view('inventory.list', compact('inventories'));
    }

    /**
     *  Report.
     */
    public function report($id)
    {
        $inventory = Inventory::with('items')->find($id);

        return view('inventory.report', compact('inventory'));
    }

    /**
     *  Pricing Import.
     */
    public function pricing_import($id)
    {
        $inventory = Inventory::find($id);

        foreach ($inventory->items as $item) {
            $price = RetalixPrice::where('upc_ean', 'like', '%'.$item->upc.'%')
                ->where('price_strategy', $inventory->store->price_strategy)
                ->first();

            if (! empty($price)) {
                $item->description = $price->product_description;
                $item->retail_price = $price->ip_unit_price;
                $item->save();
            } else {
                $plu = substr($item->upc, 0, 6).'00000';

                $price = RetalixPrice::where('upc_ean', 'like', '%'.$plu.'%')
                    ->where('price_strategy', $inventory->store->price_strategy)
                    ->first();

                if (! empty($price)) {
                    $catch = (int) substr($item->upc, 8, 11);
                    $catch = $catch / 100;

                    if ($catch > 0) {
                        $retail = $catch;
                    } else {
                        $retail = $price->ip_unit_price;
                    }

                    $item->description = $price->product_description;
                    $item->retail_price = $retail;
                    $item->save();
                }
            }
        }

        return back()->with([
            'message' => [
                'type'    => 'success',
                'message' => 'Pricing Imported',
            ],
        ]);
    }

    /**
     *  Export.
     */
    public function export($id)
    {
        return Excel::download(new InventoryExport($id), 'inventory-'.$id.'.xlsx');
    }
}
