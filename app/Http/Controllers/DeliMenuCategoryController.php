<?php

namespace App\Http\Controllers;

use App\DeliMenuCategory;
use Illuminate\Http\Request;

class DeliMenuCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DeliMenuCategory  $deliMenuCategory
     * @return \Illuminate\Http\Response
     */
    public function show(DeliMenuCategory $deliMenuCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DeliMenuCategory  $deliMenuCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(DeliMenuCategory $deliMenuCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DeliMenuCategory  $deliMenuCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DeliMenuCategory $deliMenuCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DeliMenuCategory  $deliMenuCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeliMenuCategory $deliMenuCategory)
    {
        //
    }

    public function menu_categories($menu_id)
    {
        $cats = DeliMenuCategory::where('menu_id', $menu_id);

        return response($cats);
    }
}
