<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Huddle;
use App\HuddleComment;
use App\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class HuddlesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['create', 'store', 'view']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $huddles = Huddle::orderBy('created_at', 'desc')->paginate(25);

        if (Auth::user()->isStore()) {
            $huddles = Huddle::where('store_id', Store::where('store_code', Auth::user()->store)->first()->id)->orderBy('created_at', 'desc')->paginate(25);
        }

        return view('huddles.index', compact('huddles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('huddles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'store_id' => 'required',
            'details'  => 'required',
            'leader'   => 'required',
        ]);

        $huddle = new Huddle($request->all());

        $huddle->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Huddle saved successfully.',
        ]);

        return redirect()->action('HuddlesController@view', $huddle->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * View after Submit.
     */
    public function view($id)
    {
        $huddle = Huddle::findOrFail($id);

        $comments = HuddleComment::where('huddle_id', $id)->orderBy('created_at', 'desc')->get();

        return view('huddles.view', compact(['huddle', 'comments']));
    }

    /**
     *  Filter Huddles by Store.
     */
    public function filter($store)
    {
        if ($store != 0) {
            $store = Store::where('store_code', $store)->get();
            $huddles = Huddle::where('store_id', $store[0]->id)->orderBy('created_at', 'desc')->paginate(25);
        } else {
            $huddles = Huddle::where('store_id', 0)->orderBy('created_at', 'desc')->paginate(25);
        }

        return view('huddles.index', compact('huddles'));
    }

    /**
     *  Summary.
     */
    public static function summary()
    {
        $yesterday = date('Y-m-d', strtotime('-1 days'));

        $huddles = Huddle::where(DB::raw('DATE(created_at)'), $yesterday)->get();

        if (count($huddles) > 0) {
            Mail::send('emails.huddles.summary', ['huddles' => $huddles], function ($m) use ($huddles) {
                $m->from('digital@buyforlessok.com', 'BFL Media Team');

                $m->to('allcompany@buyforlessok.com')
                    ->replyTo('digital@buyforlessok.com')
                    ->subject('Yesterday\'s Huddles');
            });
        }
    }
}
