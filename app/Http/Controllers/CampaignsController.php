<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Campaign;
use App\CampaignCategory;
use App\CampaignItem;
use App\Http\Requests;
use App\Store;
use Illuminate\Auth\Access\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class CampaignsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['show', 'cooking_classes', 'campaign_by_name']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $campaigns = Campaign::orderBy('name')->get();

        return view('campaigns.index', compact('campaigns'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('campaigns.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'             => 'required',
            'url'              => 'required',
            'background_image' => 'required|image',
            'primary_color'    => 'required',
            'secondary_color'  => 'required',
            'status'           => 'required|boolean',
            'brand_id'         => 'required',
            'pickup_deadline'  => 'required_with:end_date',
        ]);

        $filename = uniqid().'.'.$request->file('background_image')->getClientOriginalExtension();

        Storage::put('campaigns/'.$filename, file_get_contents($request->file('background_image')->getRealPath()), 'public');

        $campaign = new Campaign($request->all());

        $campaign->background_image = $filename;

        $campaign->save();

        $campaign->stores()->attach($request->stores);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Campaign created successfully',
        ]);

        return redirect()->action('CampaignsController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $url = urldecode($id);

        $campaign = Campaign::where([
                ['url', $url],
                ['status', 1],
            ])
            ->where(function ($q) {
                $q->where('end_date', '>=', date('Y-m-d'))
                    ->orWhere('end_date', null);
            })
            ->with(
                'brands',
                'stores',
                'categories',
                'categories.items',
                'categories.items.sizes',
                'categories.items.choices.options',
                'categories.items.store.brands',
                'faqs')
            ->first();

        return response($campaign, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $campaign = Campaign::findOrFail($id);

        return view('campaigns.edit', compact('campaign'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'             => 'required',
            'url'              => 'required',
            'background_image' => 'image',
            'primary_color'    => 'required',
            'secondary_color'  => 'required',
            'status'           => 'required|boolean',
            'brand_id'         => 'required',
            'pickup_deadline'  => 'required_with:end_date',
        ]);

        $campaign = Campaign::findOrFail($id);

        $filename = basename($campaign->background_image);

        if ($request->hasFile('background_image')) {
            $filename = uniqid().'.'.$request->file('background_image')->getClientOriginalExtension();

            Storage::put('campaigns/'.$filename, file_get_contents($request->file('background_image')->getRealPath()), 'public');
        }

        $campaign->fill($request->all());

        $campaign->background_image = basename($filename);

        $campaign->save();

        $campaign->stores()->sync($request->stores);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Campaign updated successfully',
        ]);

        return redirect()->action('CampaignsController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Campaign::destroy($id);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Campaign removed successfully',
        ]);

        return redirect()->action('CampaignsController@index');
    }

    /**
     * Show the orders report.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function orders()
    {
        $orders = file_get_contents('http://sports.uptowngroceryco.com/orders');

        $orders = json_decode($orders);

        return view('campaigns.orders', compact('orders'));
    }

    /**
     * Show the orders report.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function order_details($id)
    {
        $order = file_get_contents('http://sports.uptowngroceryco.com/orders/'.$id);

        $order = json_decode($order);

        $order = $order[0];

        return view('campaigns.order-details', compact('order'));
    }

    /**
     *  Replicate.
     */
    public function replicate($id)
    {
        $campaign = Campaign::find($id);

        $clone = $campaign->replicate();

        $clone->name = '[COPY] '.$campaign->name;

        $clone->save();

        foreach ($campaign->categories as $category) {
            $clone->categories()->attach($category->id);
        }

        Session::flash('message', [
            'type'    => 'success',
            'message' => Lang::get('campaigns.title').': '.Lang::get('general.replicated'),
        ]);

        return back();
    }

    /**
     *  Campaign By Name.
     */
    public function campaign_by_name($name, $store)
    {
        $store = Store::where('store_code', $store)->first();

        $name = urldecode($name);

        $campaign = Campaign::where([
            'name' => $name,
            'brand_id' => $store->brand_id,
        ])->first();

        return response($campaign, 200);
    }
}
