<?php

namespace App\Http\Controllers;

use App\Article;
use App\Event;
use App\Http\Requests;
use App\Promo;
use Illuminate\Http\Request;

class EmailsController extends Controller
{
    public function campaign($brand, $template)
    {
        $events = Event::current($brand)->get();

        $promos = Promo::current($brand)->get();

        $articles = Article::latest();

        $colors = [
            '1' => [
                '#c3002f',
                '#f5c400',
            ],
            '2' => [
                '#ac162c',
                '#c4d600',
            ],
            '3' => [
                '#cf0a2c',
                '#ffd700',
            ],
            '4' => [
                '#c3002f',
                '#f5c400',
            ],
        ];

        $domains = [
            '1' => 'http://www.buyforlessok.com',
            '2' => 'http://uptowngroceryco.com',
            '3' => 'http://smartsaverok.com',
            '4' => 'http://compramaspormenos.com',
        ];

        $color = $colors[$brand];

        $domain = $domains[$brand];

        return view('emails.'.$template, compact('domain', 'color', 'events', 'promos', 'articles'));
    }
}
