<?php

namespace App\Http\Controllers;

use App\SignageShop;
use App\SignageShopItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class SignageShopController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shops = SignageShop::current()->get();

        $items = SignageShopItem::active()->get();

        return view('signage-shop.index', compact('shops', 'items'));
    }

    public function print_request(Request $request, $id)
    {
        $this->validate($request, [
            'store_id' => 'required',
            'qty'      => 'required',
        ]);

        $item = SignageShopItem::findOrFail($id);

        $shop = SignageShop::findOrFail($item->signage_shop_id);

        $user = Auth::user();

        Mail::send('signage-shop.request-email', ['request' => $request, 'item' => $item, 'user' => $user, 'shop' => $shop], function ($m) use ($request, $item, $user, $shop) {
            $m->from($user->email, $user->name);

            $m->to('help@buyforlessok.com')
                ->replyTo($user->email)
                ->subject('Signage Shop Request for '.$user->store);
        });

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Your request was sent successfully!',
        ]);

        return redirect()->action('SignageShopController@index');
    }

    public function create()
    {
        return view('signage-shop.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'brand_id'     => 'required',
            'name'         => 'required',
            'description'  => 'required',
            'start_date'   => 'required',
            'end_date'     => 'required',
            'status'       => 'required',
            'cover_image'  => 'mimes:jpeg,png,gif',
        ]);

        if ($request->hasFile('cover_image')) {
            $filename = uniqid().'.'.$request->file('cover_image')->getClientOriginalExtension();

            Storage::put('signage-shop/'.$filename, file_get_contents($request->file('cover_image')->getRealPath()), 'public');

            $signageshop = new SignageShop($request->all());

            $signageshop->cover_image = $filename;
        } else {
            $signageshop = new SignageShop($request->all());

            $signageshop->cover_image = 'pending-image.jpg';
        }

        $signageshop->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Signage Shop Campaign created successfully!',
        ]);

        return redirect()->action('SignageShopController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $shop = SignageShop::findOrFail($id);

        return view('signage-shop.campaign', compact('shop'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'brand_id'     => 'required',
            'name'         => 'required',
            'description'  => 'required',
            'start_date'   => 'required',
            'end_date'     => 'required',
            'status'       => 'required',
            'cover_image'  => 'mimes:jpeg,png,gif',
        ]);

        $shop = SignageShop::findOrFail($id);

        $filename = basename($shop->cover_image);

        $shop->fill($request->all());

        if ($request->hasFile('cover_image')) {
            $filename = uniqid().'.'.$request->file('cover_image')->getClientOriginalExtension();

            Storage::put('signage-shop/'.$filename, file_get_contents($request->file('cover_image')->getRealPath()), 'public');

            $shop->cover_image = $filename;
        } else {
            $shop->cover_image = $filename;
        }

        $shop->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Campaign updated successfully!',
        ]);

        return redirect()->action('SignageShopController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SignageShop::destroy($id);

        Session::flash('message', [
            'type' => 'success',
            'message' => 'Campaign has been successfully deleted!',
        ]);

        return redirect()->action('SignageShopController@index');
    }

    /**
     * Signage Shop Items.
     */
    public function add_item($id)
    {
        $shop = SignageShop::findOrFail($id);

        return view('signage-shop.add-item', compact('shop'));
    }

    public function create_item(Request $request)
    {
        $this->validate($request, [
            'signage_shop_id' => 'required',
            'name'            => 'required',
            'description'     => 'required',
            'status'          => 'required',
            'file'            => 'mimes:pdf,doc,xls,jpeg,png,gif',
            'cover_image'     => 'mimes:jpeg,png,gif',
        ]);

        $filename = '';

        if ($request->hasFile('file')) {
            $extension = $request->file('file')->getClientOriginalExtension();

            $filename = uniqid().'.'.$extension;

            Storage::put('signage-shop/'.$filename, file_get_contents($request->file('file')->getRealPath()), 'public');
        }

        if ($request->hasFile('cover_image')) {
            $imgname = uniqid().'.'.$request->file('cover_image')->getClientOriginalExtension();

            Storage::put('signage-shop/'.$imgname, file_get_contents($request->file('cover_image')->getRealPath()), 'public');

            $item = new SignageShopItem($request->all());

            $item->cover_image = $imgname;

            $item->file = $filename;

            $item->save();
        } else {
            $item = new SignageShopItem($request->all());

            $item->cover_image = 'pending-image.jpg';

            $item->file = $filename;

            $item->save();
        }

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Item added successfully!',
        ]);

        return redirect()->action('SignageShopController@index');
    }

    public function edit_item($id)
    {
        $item = SignageShopItem::findOrFail($id);

        return view('signage-shop.edit-item', compact('item'));
    }

    public function destroy_item($id)
    {
        SignageShopItem::destroy($id);

        Session::flash('message', [
            'type' => 'success',
            'message' => 'Item has been successfully deleted!',
        ]);

        return redirect()->action('SignageShopController@index');
    }

    public function update_item(Request $request, $id)
    {
        $this->validate($request, [
            'signage_shop_id' => 'required',
            'name'            => 'required',
            'description'     => 'required',
            'status'          => 'required',
            'file'            => 'mimes:pdf,doc,xls,jpeg,png,gif',
            'cover_image'     => 'mimes:jpeg,png,gif',
        ]);

        $item = SignageShopItem::findOrFail($id);

        $filename = basename($item->file);

        $coverimg = basename($item->cover_image);

        $item->fill($request->all());

        if ($request->hasFile('file')) {
            $filename = uniqid().'.'.$request->file('file')->getClientOriginalExtension();

            Storage::put('signage-shop/'.$filename, file_get_contents($request->file('file')->getRealPath()), 'public');

            $item->cover_image = $filename;
        } else {
            $item->cover_image = $filename;
        }

        if ($request->hasFile('cover_image')) {
            $coverimg = uniqid().'.'.$request->file('cover_image')->getClientOriginalExtension();

            Storage::put('signage-shop/'.$coverimg, file_get_contents($request->file('cover_image')->getRealPath()), 'public');

            $item->cover_image = $coverimg;
        } else {
            $item->cover_image = $coverimg;
        }

        $item->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Item updated successfully!',
        ]);

        return redirect()->action('SignageShopController@index');
    }
}
