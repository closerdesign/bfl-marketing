<?php

namespace App\Http\Controllers;

use App\Ad;
use App\Adfile;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class AdfilesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
           'file' => 'required|mimes:pdf|max:10000',
            'ad'  => 'required|numeric',
        ]);

        $extension = $request->file('file')->getClientOriginalExtension();

        $filename = uniqid().'.'.$extension;

        Storage::put('adfiles/'.$filename, file_get_contents($request->file('file')->getRealPath()), 'public');

        $file = new Adfile($request->all());

        $file->file = $filename;

        $file->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'The file have been uploaded successfully',
        ]);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ad = Ad::findOrFail($id);

        $files = Adfile::where('ad', $id)->get();

        return view('adfiles.show', compact('ad', 'files'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Adfile::destroy($id);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'File deleted successfully',
        ]);

        return back();
    }
}
