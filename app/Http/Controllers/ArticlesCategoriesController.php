<?php

namespace App\Http\Controllers;

use App\ArticleCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ArticlesCategoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articlecategories = ArticleCategory::all();

        return view('article_categories.index'); //, compact('articlecategories'));
    }

    public function create()
    {
        return view('article_categories.create'); //, compact('articlecategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'      => 'required',
        ]);

        $category = new ArticleCategory($request->all());

        $category->save();

        return back()->with([
            'message' => [
                'type'    => 'success',
                'message' => 'Category has been created.',
            ],
        ]);
    }

    /**
     *  Show.
     */
    public function show($id)
    {
        $category = ArticleCategory::with('articles')->find($id);

        return response($category, 200);
    }

    /**
     *  Update.
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $category = ArticleCategory::find($id);
        $category->fill(request()->all());
        $category->save();

        return back()->with([
            'message' => [
                'type'    => 'success',
                'message' => 'Category has been updated.',
            ],
        ]);
    }

    /**
     *  Destroy.
     */
    public function destroy($id)
    {
        ArticleCategory::destroy($id);

        return back()->with([
            'message' => [
                'type'    => 'success',
                'message' => 'Category has been removed.',
            ],
        ]);
    }
}
