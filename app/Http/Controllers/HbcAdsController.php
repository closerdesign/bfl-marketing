<?php

namespace App\Http\Controllers;

use App\HbcAd;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class HbcAdsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'brand'    => 'required',
            'product'  => 'required',
            'size'     => 'required',
            'upc'      => 'required',
            'price'    => 'required',
        ]);

        $input = $request->all();

        $hbc = new HbcAd($input);

        $hbc->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Product added successfully',
        ]);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        HbcAd::destroy($id);

        Session::flash('message', [
            'type' => 'success',
            'message' => 'Product deleted successfully',
        ]);

        return back();
    }

    /**
     * Ordering up the resource in storage.
     */
    public function up($id)
    {
        $hbc = HbcAd::findOrFail($id);

        $current = $hbc->ordering;

        $hbc->ordering = $current + 1;

        $hbc->save();

        return back();
    }

    /**
     * Ordering up the resource in storage.
     */
    public function down($id)
    {
        $hbc = HbcAd::findOrFail($id);

        $current = $hbc->ordering;

        $hbc->ordering = $current - 1;

        $hbc->save();

        return back();
    }
}
