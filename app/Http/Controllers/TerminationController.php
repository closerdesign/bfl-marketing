<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Termination;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class TerminationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $terminations = Termination::orderBy('created_at', 'desc');

        if (Auth::user()->isStore()) {
            $terminations = $terminations->where('store', Auth::user()->store);
        }

        $terminations = $terminations->paginate(25);

        return view('termination.index', compact('terminations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('termination.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name'       => 'required',
            'last_name'        => 'required',
            'store'            => 'required',
            'department'       => 'required',
            'termination_date' => 'required',
            'last_day'         => 'required',
            'termination_type' => 'required',
            'reason'           => 'required',
            'rehire'           => 'required',
            'notice_given'     => 'required',
            'attachment'       => 'mimes:jpeg,png,jpg,doc,docx,csv,xls,xlsx,pdf|max:10000',
        ]);

        $filename = '';

        if ($request->attachment) {
            $extension = $request->file('attachment')->getClientOriginalExtension();

            $filename = uniqid().'.'.$extension;

            Storage::put('termination/'.$filename, file_get_contents($request->file('attachment')->getRealPath()), 'public');
        }

        $termination = new Termination($request->all());

        $termination->attachment = $filename;

        $termination->completed_by = Auth::user()->name;

        $termination->save();

        Mail::send('emails.termination.send', ['termination' => $termination], function ($m) use ($termination) {
            $m->from(Auth::user()->email, Auth::user()->name);

            $m->to('tmed@buyforlessok.com')
                ->subject('[Termination Form] '.$termination->first_name.' '.$termination->last_name);
        });

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Your request has been submitted.',
        ]);

        return redirect()->action('TerminationController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $termination = Termination::findOrFail($id);

        if (Auth::user()->isStore()) {
            $termination = Termination::where('store', Auth::user()->store)->findOrFail($id);
        }

        return view('termination.show', compact('termination'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
