<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Training;
use App\TrainingSlide;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class TrainingSlidesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $this->validate($request, [
            'name'  => 'required',
            'position' => 'required|integer',
            'description' => 'required',
            'image' => 'image|max:1000',
        ]);

        $training = Training::findOrFail($id);

        $slide = new TrainingSlide($request->all());

        $image = '';

        if ($request->hasFile('image')) {
            $image = uniqid().'.'.$request->file('image')->getClientOriginalExtension();

            Storage::put('training-slides/'.$image, file_get_contents($request->file('image')->getRealPath()), 'public');
        }

        $slide->image = $image;

        $slide->training_id = $id;

        $slide->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Slide added successfully.',
        ]);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slide = TrainingSlide::findOrFail($id);

        return view('training.slide', compact('slide'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'  => 'required',
            'position' => 'required|integer',
            'description' => 'required',
            'image' => 'image|max:1000',
        ]);

        $slide = TrainingSlide::findOrFail($id);

        $image = $slide->image;

        if ($request->hasFile('image')) {
            $image = uniqid().'.'.$request->file('image')->getClientOriginalExtension();

            Storage::put('training-slides/'.$image, file_get_contents($request->file('image')->getRealPath()), 'public');
        }

        $slide->fill($request->all());

        $slide->image = $image;

        $slide->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Slide updated successfully.',
        ]);

        return redirect()->action('TrainingController@edit', $slide->training_id);
    }

    /**
     * Preview Slide.
     */
    public function preview($id)
    {
        $slide = TrainingSlide::findOrFail($id);

        return view('training.preview', compact('slide'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slide = TrainingSlide::findOrFail($id);

        $slide->destroy($id);

        Session::flash('message', [
            'type' => 'success',
            'message' => 'Slide deleted successfully.',
        ]);

        return back();
    }
}
