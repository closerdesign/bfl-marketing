<?php

namespace App\Http\Controllers;

use App\Product;
use App\Search;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class SearchController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['store_search']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $searches = Search::orderBy('created_at', 'desc')->paginate(50);

        $counts = $this->search_frequency();

        return view('search.show', compact('searches', 'counts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $search = new Search($request->all());
        $search->search_string = urldecode($request->search_string);

        $search->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function store_search($store, $search_string, $id)
    {
        $search = new Search();
        $search->search_string = urldecode($search_string);
        $search->store_code = $store;
        $search->cart_product_id = $id;
        $search->save();
    }

    public function show_product($upc)
    {
        $product = Product::where('upc', '=', $upc)->first();

        return view('search.product', compact('product'));
    }

    public function review()
    {

       /* $searches = Search::where('keyword_processed', 1)
            ->where('reviewed', 0)
            ->where('cart_product_id' , '!=' ,  '0')
            ->join('products', 'cart_product_id', '=' , 'products.upc')
            ->select(DB::raw('*,products.brand_name as brand_name,products.description as description, searches.id as s_id, searches.created_at as s_created_at '))
            ->take(24)->get();
       */

        $searches = DB::select(DB::raw('SELECT *,p.brand_name as brand_name,p.description as description, searches.id as s_id, searches.created_at as s_created_at
                                       FROM searches JOIN products p ON p.upc = searches.cart_product_id 
                                       WHERE reviewed = 0 AND keyword_processed = 1 AND cart_product_id != 0 LIMIT 24'));

        return view('search.review', compact('searches'));
    }

    public function batch_review(Request $request)
    {
        if (! empty($request->reviewed)) {
            foreach ($request->reviewed as $k => $r) {
                if ($r == 'on') {
                    $this->mark_reviewed($k);
                }
            }
        }

        return back();
    }

    public function mark_reviewed($id)
    {
        Search::find($id)->update(['reviewed'=> 1]);
    }

    public function remove_keyword($id)
    {
        $search_row = Search::find($id);

        $product = Product::where('upc', '=', $search_row->cart_product_id)->first();
        $words = explode(',', $product->keywords);
        foreach ($words as $word) {
            if (trim(strtolower($search_row->search_string)) == trim($word)) {
                $string[] = '';
            } else {
                $string[] = $word;
            }
        }
        $keywords = implode(',', $string);

        $keywords = rtrim($keywords, ',');

        Product::where('id', $product->id)->update(['keywords' => $keywords]);

        $this->mark_reviewed($id);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Keyword Removed From product',
        ]);

        return back();
    }

    public function search_frequency()
    {
        $results = DB::select(DB::raw('SELECT count(search_string) as frequency, search_string FROM searches GROUP BY search_string ORDER BY frequency DESC LIMIT 50'));

        return $results;
    }

    public function run_keyword_process()
    {
        Artisan::call('process:keyword');

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Keyword Process Started',
        ]);

        return back();
    }
}
