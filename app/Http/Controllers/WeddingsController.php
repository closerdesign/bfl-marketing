<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Wedding;
use App\WeddingComment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class WeddingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $leads = file_get_contents('http://weddings.uptowngroceryco.com/leads');

        $leads = json_decode($leads);

        foreach ($leads as $lead) {
            $wedding = Wedding::where('lead_id', $lead->id)->first();

            if (count($wedding) != 1) {
                $lead->status = 'UNATTENDED';
            } else {
                $lead->status = $wedding->status;
            }
        }

        return view('weddings.index', compact('leads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lead = file_get_contents('http://weddings.uptowngroceryco.com/lead/'.$id);

        $lead = json_decode($lead);

        $wedding = Wedding::where('lead_id', $id)->first();

        return view('weddings.show', compact('lead', 'wedding'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'status' => 'required',
        ]);

        $wedding = Wedding::where('lead_id', $id)->first();

        $wedding->fill([
            'status' => $request->status,
        ]);

        $wedding->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Status updated successfully',
        ]);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     *  Change Resource Status.
     */
    public function status(Request $request, $id)
    {
        $this->validate($request, [
            'status' => 'required',
        ]);

        $wedding = Wedding::where('lead_id', $id)->get();

        if (count($wedding) != 1) {
            $wedding = new Wedding();
        }

        $wedding->lead_id = $id;

        $wedding->status = $request->status;

        $wedding->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Status updated successfully!',
        ]);

        return back();
    }

    /**
     *  Add comment to resource.
     */
    public function comment(Request $request, $id)
    {
        $this->validate($request, [
            'comment' => 'required',
            'file'    => 'max:5000',
        ]);

        $comment = new WeddingComment($request->all());

        $comment->wedding_id = $id;

        $file = '';

        if ($request->hasFile('file')) {
            $file = uniqid().'.'.$request->file('file')->getClientOriginalExtension();

            Storage::put('wedding-comment/'.$file, file_get_contents($request->file('file')->getRealPath()), 'public');
        }

        $comment->file = $file;

        $comment->user_id = Auth::user()->id;

        $comment->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Comment added successfully',
        ]);

        return back();
    }
}
