<?php

namespace App\Http\Controllers;

use App\Department;
use App\SalesData;
use App\Store;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class SalesController extends Controller
{
    private $connection;

    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     *  Sales Report.
     */
    public function index()
    {
        $sales = null;
        $start = date('Y-m-d');
        $end = date('Y-m-d');
        $stores = null;
        $departments = null;

        if (isset($_GET['start']) && isset($_GET['end'])) {
            $start = $_GET['start'];
            $end = $_GET['end'];
        }

        $sales = SalesData::whereBetween('sales_date', [$start, $end])->get();

        $stores = $sales->groupBy('store_num');

        $departments = $sales->groupBy('dept_name');

        $customer_count = DB::connection('sales')
            ->table('cust_data')
            ->whereBetween('sales_date', [$start, $end])
            ->select('store_num', 'cust_count')
            ->get();

        return view('sales.index', compact('sales', 'start', 'end', 'stores', 'departments', 'customer_count'));
    }

    /**
     * Total company sales.
     * @param int $num_previous_days
     * @return array and days and totals
     */
    public function sales_totals(Request $request)
    {
        if (! $request->num_previous_days) {
            $num_previous_days = 7;
        } else {
            $num_previous_days = $request->num_previous_days;
        }

        $report_days = $num_previous_days;

        $days_array = $this->the_days($num_previous_days);

        foreach ($days_array as $d) {
            $sales_total = "SELECT SUM(dept_sales) as sum_sales, DATE_FORMAT(sales_date,'%Y-%m-%d') as sales_date from sales_data WHERE sales_date = '".$d."'Group By sales_date";

            $sales_total_data = mysqli_query($this->connection, $sales_total);

            while ($st = mysqli_fetch_array($sales_total_data)) {
                $tot_sales[$st['sales_date']] = $st['sum_sales'];
            }
        }

        return view('sales.company-totals', compact('tot_sales', 'days_array', 'report_days'));
    }

    /**
     * Feed this a start date from.
     * @param $start
     * @return array
     */
    public function the_days($start)
    {
        if (! $start) {
            $start = 7;
        }
        $date = date('Y-m-d', strtotime(" -$start days"));
        $end_date = date('Y-m-d', strtotime('yesterday'));

        while (strtotime($date) <= strtotime($end_date)) {
            $days[] = $date;
            //   echo $date."<br>";
            $date = date('Y-m-d', strtotime('+1 day', strtotime($date)));
        }

        return $days;
    }

    /**
     * Get all stores.
     * @return array
     */
    public function stores()
    {
        $store_query = 'SELECT store_num from store_list WHERE store_num not in (5355,8839) ORDER BY store_num ASC';
        $stores = mysqli_query($this->connection, $store_query);

        //   die(print_r($stores));

        while ($st = mysqli_fetch_array($stores)) {
            $s[] = $st['store_num'];
        }

        return $s;
    }

    public function store_totals_by_day(Request $request)
    {
        $stores = $this->stores();
        $days = $this->the_days($request->num_previous_days);

        if (! $request->num_previous_days) {
            $report_days = 7;
        } else {
            $report_days = $request->num_previous_days;
        }

        foreach ($days as $d) {
            foreach ($stores as $s) {
                $sales_query = "SELECT SUM(dept_sales) as sum_sales, DATE_FORMAT(sales_date,'%Y-%m-%d') as sales_date , store_num from sales_data WHERE sales_date = '".$d."' and store_num = '".$s."' Group By sales_date, store_num";
                $sales_data = mysqli_query($this->connection, $sales_query);

                while ($r = mysqli_fetch_array($sales_data)) {
                    $ts[$r['sales_date']][$r['store_num']] = $r['sum_sales'];
                }
            }
        }

        return view('sales.store-totals', compact('ts', 'days', 'stores', 'report_days'));
    }

    /**
     * @return array
     */
    public function dept_index()
    {
        foreach ($this->the_days(9) as $d) {
            $dept_query = "SELECT SUM(`dept_sales`) as dept_sales, store_num, dept_name, `sales_date` FROM sales_data WHERE sales_date = '".$d."' GROUP BY store_num, dept_name, sales_date ORDER BY sales_date";
            $sales_data = mysqli_query($this->connection, $dept_query);

            while ($r = mysqli_fetch_array($sales_data)) {
                $s[$r['sales_date']][$r['store_num']][$r['dept_name']] = $r['dept_sales'];
            }
        }

        return view('sales.dept-sales', compact('s'));
    }

    /**
     * Dept summary for the last 7 days.
     */
    public function dept_summary()
    {
        $summary_query = 'SELECT SUM(`dept_sales`) as dept_sales, store_num, dept_name FROM sales_data WHERE sales_date Between DATE_SUB(CURRENT_DATE, interval 7 day) AND DATE_SUB(CURRENT_DATE, interval 1 day) GROUP BY store_num, dept_name ORDER BY `store_num`, `dept_name`';
        $sales_data = mysqli_query($this->connection, $summary_query);

        while ($r = mysqli_fetch_array($sales_data)) {
            $s[$r['store_num']][$r['dept_name']] = $r['dept_sales'];
        }

        // Was a bar graph moved it to a pie chart

        return $s; //view('sales.dept-sales', compact('s', 'dept'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * return data to seef to pie chart
     */
    public function dept_pie_chart()
    {
        $s = $this->dept_summary();

        $days = 'SELECT DATE_SUB(CURRENT_DATE, interval 7 day) as start_date, DATE_SUB(CURRENT_DATE, interval 1 day) as end_date FROM dual';
        $d = mysqli_query($this->connection, $days);

        while ($m = mysqli_fetch_array($d)) {
            $days = ['start_date' => $m['start_date'], 'end_date' => $m['end_date']];
        }

        return view('sales.dept-pie-chart', compact('s', 'days'));
    }
}
