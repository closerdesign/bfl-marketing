<?php

namespace App\Http\Controllers;

use App\RocImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Storage;

class RocImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $this->validate($request, [
            'file' => 'required|image|max:10000',
        ]);

        $filename = uniqid().'.'.request()->file('file')->getClientOriginalExtension();

        Storage::put('roc-image/'.$filename, file_get_contents(request()->file('file')->getRealPath()), 'public');

        $image = new RocImage();
        $image->image = $filename;
        $image->roc_entry_id = $id;
        $image->save();

        return back()->with([
            'message' => [
                'type' => 'success',
                'message' => Lang::get('roc-image.title').': '.Lang::get('general.saved'),
            ],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RocImage  $rocImage
     * @return \Illuminate\Http\Response
     */
    public function show(RocImage $rocImage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RocImage  $rocImage
     * @return \Illuminate\Http\Response
     */
    public function edit(RocImage $rocImage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RocImage  $rocImage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RocImage $rocImage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RocImage  $rocImage
     * @return \Illuminate\Http\Response
     */
    public function destroy(RocImage $rocImage)
    {
        $rocImage->destroy($rocImage->id);

        return back()
            ->with([
                'message' => [
                    'type'    => 'success',
                    'message' => Lang::get('roc-image.title').': '.Lang::get('general.deleted'),
                ],
            ]);
    }
}
