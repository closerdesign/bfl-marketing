<?php

namespace App\Http\Controllers;

use App\Employee;
use App\EmployeeDocument;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class EmployeeDocumentsController extends Controller
{
    public function store(Request $request, $id)
    {
        $this->validate($request, [
            'file_name'=> 'required',
            'file' => 'required',
        ]);

        $employee = Employee::findOrFail($id);

        $filename = uniqid().'.'.$request->file('file')->getClientOriginalExtension();

        Storage::put('emp_docs/'.$filename, file_get_contents($request->file('file')->getRealPath()), 'public');

        $docs = new EmployeeDocument($request->all());

        $docs->employee_id = $id;

        $docs->file = $filename;

        $docs->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Document has been added successfully.',
        ]);

        return back();

        //return redirect()->action('EmployeesController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $doc = EmployeeDocument::findOrFail($id);
        $doc->destroy($id);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Document has been successfully deleted.',
        ]);

        return back();
    }
}
