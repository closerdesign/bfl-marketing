<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Hocza\Sendy\Facades\Sendy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class MailingListsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('mailing-lists.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'brand' => 'required',
        ]);

        if ($request->brand == '1') {
            \Hocza\Sendy\Facades\Sendy::setListId('dwPhRy3UuuVLNgn1u1iRJA')->subscribe(['email' => $request->email, 'name' => $request->name]);
        } elseif ($request->brand == '2') {
            \Hocza\Sendy\Facades\Sendy::setListId('ViKqF1OGzY6BZ8IGc72A7w')->subscribe(['email' => $request->email, 'name' => $request->name]);
        } elseif ($request->brand == '3') {
            \Hocza\Sendy\Facades\Sendy::setListId('bARzZutqxyHGZ8U7Sr6DVQ')->subscribe(['email' => $request->email, 'name' => $request->name]);
        } else {
            \Hocza\Sendy\Facades\Sendy::setListId('jiW5vVFcECgB4yBfFY9Nhw')->subscribe(['email' => $request->email, 'name' => $request->name]);
        }

        Session::flash('message', [
            'title' => 'Thanks!',
            'message' => $request->email.' has been added to our mailing list.',
            'type' => 'success',
        ]);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
