<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\ImprovementRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class ImprovementRequestsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $requests = ImprovementRequest::where('user_id', Auth::user()->id)->orderBy('status')->orderBy('created_at', 'DESC')->get();

        if (Auth::user()->isAdmin()) {
            $requests = ImprovementRequest::orderBy('status')->orderBy('created_at', 'DESC')->get();
        }

        return view('improvement-requests.index', compact('requests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('improvement-requests.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'       => 'required',
            'description' => 'required',
            'file'        => 'mimes:pdf,doc,docx,xls,xlsx',
        ]);

        $improvement_request = new ImprovementRequest($request->all());

        $filename = '';

        if ($request->hasFile('file')) {
            $filename = uniqid().'.'.$request->file('file')->getClientOriginalExtension();

            Storage::put('improvement-requests/'.$filename, file_get_contents($request->file('file')->getRealPath()), 'public');
        }

        $improvement_request->file = $filename;

        $improvement_request->user_id = Auth::user()->id;

        $improvement_request->status = 'PENDING';

        $improvement_request->save();

        $admins = User::where('admin', 1)->get();

        foreach ($admins as $user) {
            Mail::send('emails.new-improvement-request', ['improvement_request' => $improvement_request, 'user' => $user], function ($m) use ($improvement_request, $user) {
                $m->from(Auth::user()->email, Auth::user()->name);

                $m->to($user->email, $user->name)
                    ->replyTo(Auth::user()->email)
                    ->subject('[New Improvement Request] '.$improvement_request->title);
            });
        }

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Request created successfully',
        ]);

        return redirect()->action('ImprovementRequestsController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $improvement_request = ImprovementRequest::findOrFail($id);

        return view('improvement-requests.show', compact('improvement_request'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'status' => 'required',
        ]);

        $improvement_request = ImprovementRequest::findOrFail($id);

        $improvement_request->status = $request->status;

        $improvement_request->save();

        Mail::send('emails.improvement-request-updated', ['improvement_request' => $improvement_request], function ($m) use ($improvement_request) {
            $m->from(Auth::user()->email, Auth::user()->name);

            $m->to($improvement_request->users->email, $improvement_request->users->name)
                ->replyTo(Auth::user()->email)
                ->subject('[Updated Request] - '.$improvement_request->title);
        });

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'The request has been updated successfully',
        ]);

        return redirect()->action('ImprovementRequestsController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
