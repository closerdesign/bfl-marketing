<?php

namespace App\Http\Controllers;

use App\DeliMenu;
use Illuminate\Http\Request;

class DeliMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DeliMenu  $deliMenu
     * @return \Illuminate\Http\Response
     */
    public function show(DeliMenu $deliMenu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DeliMenu  $deliMenu
     * @return \Illuminate\Http\Response
     */
    public function edit(DeliMenu $deliMenu)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DeliMenu  $deliMenu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DeliMenu $deliMenu)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DeliMenu  $deliMenu
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeliMenu $deliMenu)
    {
        //
    }
}
