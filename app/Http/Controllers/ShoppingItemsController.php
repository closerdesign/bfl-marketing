<?php

namespace App\Http\Controllers;

use App\Department;
use App\Http\Requests;
use App\ShoppingItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class ShoppingItemsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['show', 'departments', 'list']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = ShoppingItem::all();

        return view('shopping_items.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('shopping_items.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'upc' => 'integer',
            'product_name' => 'required',
            'size' => 'required',
            'uom' => 'required',
            'department_id' => 'required|integer',
            'category_id' => 'required|integer',
            'price' => 'required',
            'price_strategy' => 'required',
            'price_type' => 'required',
            'start_date' => 'date',
            'end_date' => 'date',
            'tax' => 'required',
            'image' => 'required|image|max:1000',
        ]);

        $shopping_item = new ShoppingItem($request->all());

        $filename = uniqid().'.'.$request->file('image')->getClientOriginalExtension();

        Storage::put('shopping-items/'.$filename, file_get_contents($request->file('image')->getRealPath()), 'public');

        $shopping_item->image = $filename;

        $shopping_item->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Shopping Item saved successfully',
        ]);

        return redirect()->action('ShoppingItemsController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $shopping_item = ShoppingItem::findOrFail($id);

        return json_encode($shopping_item);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = ShoppingItem::findOrFail($id);

        return view('shopping_items.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'upc' => 'integer',
            'product_name' => 'required',
            'size' => 'required',
            'uom' => 'required',
            'department_id' => 'required|integer',
            'category_id' => 'required|integer',
            'price' => 'required',
            'price_strategy' => 'required',
            'price_type' => 'required',
            'start_date' => 'date',
            'end_date' => 'date',
            'tax' => 'required',
            'image' => 'image|max:1000',
        ]);

        $shopping_item = ShoppingItem::findOrFail($id);

        $filename = $shopping_item->image;

        if ($request->hasFile('image')) {
            $filename = uniqid().'.'.$request->file('image')->getClientOriginalExtension();

            Storage::put('shopping-items/'.$filename, file_get_contents($request->file('image')->getRealPath()), 'public');
        }

        $shopping_item->fill($request->all());

        $shopping_item->image = $filename;

        $shopping_item->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Shopping Item updated successfully',
        ]);

        return redirect()->action('ShoppingItemsController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ShoppingItem::destroy($id);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Item removed successfully',
        ]);

        return redirect()->action('ShoppingItemsController@index');
    }

    /**
     * List Departments with Shopping List Items.
     *
     * @return API list
     */
    public function departments()
    {
        $departments = Department::has('shopping_items')->get();

        return response($departments, 200);
    }

    /**
     * List Products By Department.
     *
     * @return Product List
     */
    public function list($id)
    {
        $products = Department::with('shopping_items')->where('id', $id)->get();

        return response($products, 200);
    }
}
