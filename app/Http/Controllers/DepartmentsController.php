<?php

namespace App\Http\Controllers;

use App\Department;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class DepartmentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'list']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Department::all();

        return view('departments.index', compact('departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('departments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        if ($request->active != 1) {
            $request->active = 0;
        }

        $department = new Department($request->all());

        $department->active = ($request->active == 1) ? true : false;

        $department->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Department created successfully',
        ]);

        return redirect()->action('DepartmentsController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $department = Department::findOrFail($id);

        return view('departments.edit', compact('department'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $department = Department::findOrFail($id);

        $department->fill($request->all());

        $department->active = ($request->active == 1) ? true : false;

        $department->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Department updated successfully',
        ]);

        return redirect()->action('DepartmentsController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Department::destroy($id);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Department removed successfully',
        ]);

        return redirect()->action('DepartmentsController@index');
    }

    /**
     *  List.
     */
    public function list()
    {
        return response(Department::all(), 200);
    }
}
