<?php

namespace App\Http\Controllers;

use App\RocEntryCompletion;
use Illuminate\Http\Request;

class RocEntryCompletionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RocEntryCompletion  $rocEntryCompletion
     * @return \Illuminate\Http\Response
     */
    public function show(RocEntryCompletion $rocEntryCompletion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RocEntryCompletion  $rocEntryCompletion
     * @return \Illuminate\Http\Response
     */
    public function edit(RocEntryCompletion $rocEntryCompletion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RocEntryCompletion  $rocEntryCompletion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RocEntryCompletion $rocEntryCompletion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RocEntryCompletion  $rocEntryCompletion
     * @return \Illuminate\Http\Response
     */
    public function destroy(RocEntryCompletion $rocEntryCompletion)
    {
        //
    }
}
