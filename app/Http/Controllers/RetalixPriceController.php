<?php

namespace App\Http\Controllers;

use App\RetalixPrice;
use App\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RetalixPriceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'price_check']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RetalixPrice  $retalixPrice
     * @return \Illuminate\Http\Response
     */
    public function show(RetalixPrice $retalixPrice)
    {
        //
    }

    /**
     *  Price Check.
     */
    public function price_check($store, $upc)
    {
        try {
            $store = Store::where('store_code', $store)
                ->first();

            // Let's try to find the product in the ideal scenario
            $retalix_upc = format_retalix_upc($upc);

            $price = RetalixPrice::where('upc_ean', $retalix_upc)
                ->where('price_strategy', $store->price_strategy)
                ->orderBy('ip_start_date', 'desc');

            if ($price->count() > 0) {
                return response($price->first(), 200);
            }

            // If we can't find it, we'll have to check if it is a meat PLU
            if (strlen($upc) === 11) {
                // First we get the PLU portion
                $meat_plu = substr($upc, 0, 6);

                // Then we will add the 5 zeros at the end
                $meat_plu = (string) $meat_plu.'00000';

                // Then, we will add the verification digit
                $meat_plu = generate_upc_checkdigit($meat_plu);

                // And finally, before running the query, we will add a couple of leading zeros,
                // to complete the 14 digits in Retalix database.
                $meat_plu = str_pad($meat_plu, 14, 0, STR_PAD_LEFT);

                // Then, we will run the same query we ran initially.
                $price = RetalixPrice::where('upc_ean', $meat_plu)
                    ->where('price_strategy', $store->price_strategy)
                    ->orderBy('ip_start_date', 'desc');

                return response($price->first(), 200);
            }

            //  Finally. If we can't find a UPC, we can also check on the produce side of things
            if (strlen($upc) === 4) {
                // First we add zeros to complete the 11 digits initial number
                $produce_plu = '0000000'.(string) $upc;

                // Then we need to add a zero replacing any check digits
                $produce_plu = $produce_plu.'0';

                // Now, we will add leading zeros
                $produce_plu = str_pad($produce_plu, 14, 0, STR_PAD_LEFT);

                // Finally, we will run the same query we ran before
                $price = RetalixPrice::where('upc_ean', $produce_plu)
                    ->where('price_strategy', $store->price_strategy)
                    ->orderBy('ip_start_date', 'desc');

                return response($price->first(), 200);
            }

            return null;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
