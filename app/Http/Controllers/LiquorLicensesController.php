<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Http\Requests;
use App\LiquorLicenses;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class LiquorLicensesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::all();

        //$liquorlicenses = LiquorLicenses::orderBy('license_number');
        return view('liquor-licenses.index', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {

       // print_r($_POST);

        $employee = Employee::where('id', $id);

        return view('liquor-licenses.create', compact('employee'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $this->validate($request, [
            'license_number'  => 'required',
            'expiration_date' => 'required',
            'file'            => 'required|mimes:pdf,jpg,png,gif,jpeg|max:5000',
            'file_description'=> 'required',
        ]);

        $employee = Employee::findOrFail($id);

        $filename = uniqid().'.'.$request->file('file')->getClientOriginalExtension();

        Storage::put('liquor-licenses/'.$filename, file_get_contents($request->file('file')->getRealPath()), 'public');

        $liquorlicenses = new LiquorLicenses($request->all());

        $liquorlicenses->employee_id = $id;

        $liquorlicenses->file = $filename;

        $liquorlicenses->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Liquor license has been added successfully.',
        ]);

        return back();

        //return redirect()->action('EmployeesController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::where('id', $id);

        return view('liquor-licenses.create', compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $license = LiquorLicenses::findOrFail($id);
        $license->delete();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'License has been successfully deleted.',
        ]);

        return back();
    }

    public function report()
    {
        $today = date('Y-m-d');
        $licenses = LiquorLicenses::where('expiration_date', '>', $today)->orderBy('expiration_date')->get();

        return view('liquor-licenses.report', compact('licenses'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function expiring()
    {

        //die("HELLO");

        $data = DB::select(DB::raw('SELECT MAX(expiration_date) as expiration_date, ll.employee_id, e.`name`, s.store_code, ll.license_number, notes FROM liquor_licenses as ll
                                              LEFT JOIN employees as e ON e.id = ll.employee_id
                                              LEFT JOIN stores s ON s.id = e.`store_id`
                                              WHERE expiration_date BETWEEN CURDATE() AND CURDATE() + INTERVAL 30 DAY
                                              AND e.active = 1
                                              AND ll.employee_id NOT IN (SELECT employee_id FROM liquor_licenses WHERE expiration_date > CURDATE() + INTERVAL 30 DAY )
                                              GROUP BY employee_id, `name`, store_id, license_number, notes'));

        return view('liquor-licenses.expire', compact('data'));

        // return view('employees.liquor', compact('data'));
    }
}
