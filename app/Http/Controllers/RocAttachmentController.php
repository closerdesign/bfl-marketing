<?php

namespace App\Http\Controllers;

use App\RocAttachment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Storage;

class RocAttachmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $this->validate($request, [
            'file' => 'required|mimes:pdf,doc,docx,xls,xlsx,ppt,pptx|max:10000',
        ]);

        $filename = uniqid().'.'.request()->file('file')->getClientOriginalExtension();

        Storage::put('roc-attachment/'.$filename, file_get_contents(request()->file('file')->getRealPath()), 'public');

        $attachment = new RocAttachment([
            'file' => $filename,
            'roc_entry_id' => $id,
        ]);

        $attachment->save();

        return back()
            ->with('message', [
                'type' => 'success',
                'message' => Lang::get('roc-attachment.title').': '.Lang::get('general.saved'),
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RocAttachment  $rocAttachment
     * @return \Illuminate\Http\Response
     */
    public function show(RocAttachment $rocAttachment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RocAttachment  $rocAttachment
     * @return \Illuminate\Http\Response
     */
    public function edit(RocAttachment $rocAttachment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RocAttachment  $rocAttachment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RocAttachment $rocAttachment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RocAttachment  $rocAttachment
     * @return \Illuminate\Http\Response
     */
    public function destroy(RocAttachment $rocAttachment)
    {
        $rocAttachment->destroy($rocAttachment->id);

        return back()->with([
            'message' => [
                'type' => 'success',
                'message' => Lang::get('roc-attachment.title').': '.Lang::get('deleted'),
            ],
        ]);
    }
}
