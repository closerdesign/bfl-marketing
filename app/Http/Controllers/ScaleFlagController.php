<?php

namespace App\Http\Controllers;

use App\ScaleFlag;
use Illuminate\Http\Request;

class ScaleFlagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ScaleFlag  $scaleFlag
     * @return \Illuminate\Http\Response
     */
    public function show(ScaleFlag $scaleFlag)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ScaleFlag  $scaleFlag
     * @return \Illuminate\Http\Response
     */
    public function edit(ScaleFlag $scaleFlag)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ScaleFlag  $scaleFlag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ScaleFlag $scaleFlag)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ScaleFlag  $scaleFlag
     * @return \Illuminate\Http\Response
     */
    public function destroy(ScaleFlag $scaleFlag)
    {
        //
    }
}
