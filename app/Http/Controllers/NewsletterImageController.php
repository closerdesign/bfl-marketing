<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\NewsletterImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class NewsletterImageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['display']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $this->validate($request, [
            'image'      => 'required|mimes:jpg,jpeg,png|max:10000',
        ]);

        $filename = '';

        $extension = $request->file('image')->getClientOriginalExtension();

        $filename = uniqid().'.'.$extension;

        Storage::put('newsletter-images/'.$filename, file_get_contents($request->file('image')->getRealPath()), 'public');

        $image = new NewsletterImage($request->all());

        $image->image = $filename;

        $image->newsletter_id = $id;

        $image->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Your image has been added.',
        ]);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $image = NewsletterImage::findOrFail($id);

        $image->destroy($id);

        Session::flash('message', [
            'type' => 'success',
            'message' => 'Image has been successfully deleted.',
        ]);

        return back();
    }

    /**
     *  API call to display on websites.
     */
    public function display($id)
    {
        $images = NewsletterImage::where('newsletter_id', $id)->get();

        return response($images, 200);
    }
}
