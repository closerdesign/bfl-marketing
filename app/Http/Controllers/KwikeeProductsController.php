<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\KwikeeFile;
use App\KwikeeImage;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class KwikeeProductsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'image_display']);
    }

    /**
     *  Form.
     */
    public function form()
    {
        return view('kwikee-products.form');
    }

    /**
     *  Import.
     */
    public function import(Request $request)
    {
        $this->validate($request, [
            'file'    => 'required|mimes:xml',
        ]);

        $data = file_get_contents($request->file('file')->getRealPath());

        $filename = uniqid().'.'.$request->file('file')->getClientOriginalExtension();

        Storage::put('kwikee-xml/'.$filename, $data, 'public');

        $kwikee_file = new KwikeeFile();

        $kwikee_file->name = $request->file('file')->getClientOriginalName();

        $kwikee_file->filename = $filename;

        $kwikee_file->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'The File Was Uploaded Successfully!',
        ]);

        return redirect()->action('KwikeeProductsController@import');
    }

    /**
     *  Process.
     */
    public static function process($id)
    {
        $kwikee_file = KwikeeFile::findOrFail($id);

        $data = file_get_contents($kwikee_file->filename);

        $xml = new \SimpleXMLElement($data);

        $total = 0;

        $last_record = 0;

        $active = true;

        if ($kwikee_file->last_record > 0) {
            $active = false;

            $last_record = $kwikee_file->last_record;
        }

        foreach ($xml->products->product as $kwikee) {
            if ($kwikee->product_id == $last_record) {
                $active = true;
            }

            if ($active == true) {
                $product = Product::firstOrNew(['upc' => sprintf('%013d', $kwikee->upc_10)]);

                $product->kwikee_name = $kwikee->data->profile->custom_product_name;

                $product->kwikee_description = $kwikee->data->profile->description;

                $product->kwikee_size = $kwikee->data->profile->product_size.' '.$kwikee->data->profile->uom;

                $product->kwikee_brand_name = $kwikee->data->profile->brand->name;

                $product->save();

                if (! empty($kwikee->images->asset->files)) {
                    foreach ($kwikee->images->asset as $asset) {
                        if ($asset->view == 'CF') {
                            foreach ($asset->files->file as $file) {
                                $kwikee_image = KwikeeImage::updateOrCreate(
                                    ['product_id' => $product->id, 'type' => $file->type],
                                    ['url'        => $file->url]
                                );
                            }
                        }
                    }
                }

                $total += 1;
            }

            if ($total == 500) {
                $last_record = $kwikee->product_id;

                break;
            }
        }

        $kwikee_file->last_record = $last_record;

        $kwikee_file->status = 1;

        if ($total < 500) {
            $kwikee_file->status = 2;

            Mail::send('emails.kwikee', ['file' => $kwikee_file, 'total' => $total], function ($m) use ($kwikee_file, $total) {
                $m->from('jrodriguez@buyforlessok.com', 'SARA Platform');

                $m->to('jrodriguez@buyforlessok.com', 'SARA Platform')
                    ->replyTo('jrodriguez@buyforlessok.com')
                    ->subject('[Kwikee File] '.$kwikee_file->name.' - Items list has been successfully completed.');
            });
        }

        $kwikee_file->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Process finished. '.$total.' items processed successfully',
        ]);

        return redirect()->action('KwikeeProductsController@import');
    }

    /**
     *  Task.
     */
    public static function task()
    {
        $next = KwikeeFile::where('status', '<', 2)->orderBy('created_at')->first();

        if (! empty($next)) {
            self::process($next->id);
        }
    }

    /**
     *  Kwikee Image Display.
     */
    public function image_display()
    {
        $url = $_GET['url'];

        $c = curl_init($url);

        $authString = env('KWIKEE_USER').':'.env('KWIKEE_PASSWORD');

        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($c, CURLOPT_USERPWD, $authString);

        $content = curl_exec($c);

        $contentType = curl_getinfo($c, CURLINFO_CONTENT_TYPE);

        header('Content-Type:'.$contentType);

        echo $content;
    }
}
