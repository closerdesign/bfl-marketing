<?php

namespace App\Http\Controllers;

use App\HourlySale;
use Illuminate\Http\Request;

class HourlySaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\HourlySale  $hourlySale
     * @return \Illuminate\Http\Response
     */
    public function show(HourlySale $hourlySale)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HourlySale  $hourlySale
     * @return \Illuminate\Http\Response
     */
    public function edit(HourlySale $hourlySale)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HourlySale  $hourlySale
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HourlySale $hourlySale)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HourlySale  $hourlySale
     * @return \Illuminate\Http\Response
     */
    public function destroy(HourlySale $hourlySale)
    {
        //
    }
}
