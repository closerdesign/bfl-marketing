<?php

namespace App\Http\Controllers;

use App\Article;
use App\ArticleCategory;
use App\ArticlesCategoriesReference;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class ArticlesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['show', 'article']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::orderBy('created_at', 'desc')->paginate(20);

        return view('articles.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = ArticleCategory::all();

        return view('articles.create', compact($categories));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'       => 'required',
            'intro_text'  => 'required',
            'content'     => 'required',
            'cover_img'   => 'image|max:1000',
            'status'      => 'required|numeric',
        ]);

        $filename = '';

        if ($request->hasFile('cover_img')) {
            $filename = uniqid().'.'.$request->file('cover_img')->getClientOriginalExtension();

            Storage::put('articles/'.$filename, file_get_contents($request->file('cover_img')->getRealPath()), 'public');
        }

        $article = new Article($request->all());

        $article->cover_img = $filename;

        $article->save();

        $article->categories()->attach($request->categories);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Article saved successfully',
        ]);

        return redirect()->action('ArticlesController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = explode('-', $id)[0];

        $articles = Article::where('status', 1)
            ->orderBy('created_at', 'desc')
            ->get();

        return response($articles, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::findOrFail($id);

        return view('articles.edit', compact('article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'       => 'required',
            'intro_text'  => 'required',
            'content'     => 'required',
            'cover_img'   => 'mimes:jpg,jpeg,png,gif',
            'status'      => 'required|numeric',
        ]);

        $article = Article::findOrFail($id);

        $filename = basename($article->cover_img);

        $article->fill($request->all());

        if ($request->hasFile('cover_img')) {
            $filename = uniqid().'.'.$request->file('cover_img')->getClientOriginalExtension();

            Storage::put('articles/'.$filename, file_get_contents($request->file('cover_img')->getRealPath()), 'public');
        }

        $article->cover_img = $filename;

        $article->save();

        $article->categories()->sync($request->categories);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Article updated successfully',
        ]);

        return redirect()->action('ArticlesController@index');
    }

    /**
     * Confirm deletion for the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $article = Article::findOrFail($id);

        return view('articles.delete', compact('article'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Article::destroy($id);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Article deleted successfully',
        ]);

        return redirect('/articles');
    }

    /**
     *  Single Article API.
     */
    public function article($id)
    {
        $article = Article::findOrFail(explode('-', $id)[0]);

        return response($article, 200);
    }
}
