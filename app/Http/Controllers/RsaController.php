<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class RsaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->apiKeys = [
            'bfl' => [
                'URL' => 'https://buyforlessok.rsaamerica.com/PartnerApi/SSWebRestApi.svc',
                'EnterpriseId' => 'A07DC24C-B545-4FED-A840-3632CBD5F0F5',
                'SecurityKey' => 'ED2559CF-6927-4A3B-811A-E223808D98CD',
                'UserToken' => '2C4F6225-1374-487D-AACB-60BCD42E7BEB',
            ],
            'uptown' => [
                'URL' => 'https://uptowngroceryco.rsaamerica.com/partnerapi/SSWebRestApi.svc',
                'EnterpriseId' => 'C7C9E7CB-579C-4E22-A694-F3347F2C83A3',
                'SecurityKey' => 'D8CE9DA1-526E-4F8E-9604-4C29B8410717',
                'UserToken' => '2C4F6225-1374-487D-AACB-60BCD42E7BEB',
            ],
        ];
    }

    /**
     *  Redemption Report.
     */
    public function redemption_report()
    {
        try {
            $start_date = null;
            $end_date = null;
            $brand = null;
            $coupons = null;

            if (isset($_GET['start_date']) && isset($_GET['end_date']) && isset($_GET['brand'])) {
                $client = new Client();

                $brand = $_GET['brand'] ?? 'bfl';

                $url = $this->apiKeys[$brand]['URL'].'/GetCouponsListWithRedeems';

                $body = '
                {
                    "CouponTypeId":4,
                    "EnterpriseId":"'.$this->apiKeys[$brand]['EnterpriseId'].'",
                    "SecurityKey":"'.$this->apiKeys[$brand]['SecurityKey'].'",
                    "UserToken":"2C4F6225-1374-487D-AACB-60BCD42E7BEB",
                    "StartDate":"01-01-2020",
                    "EndDate":"05-22-2020"
                }';

                $response = $client->post($url, [
                    'headers' => [
                        'Content-type' => 'application/json; charset=utf-8',
                    ],
                    'body' => $body,
                ]);

                $coupons = json_decode($response->getBody());

                $coupons = $coupons->CouponsListWithRedeems;
            }

            return view('rsa.redemption-report', compact('coupons'));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     *  Coupon Product Analysis.
     */
    public function coupon_product_analysis($coupon, $brand)
    {
        try {
            $client = new Client();

            $url = $this->apiKeys[$brand]['URL'].'/GetCouponProductAnalysis';

            $body = '
                {
                    "EnterpriseId":"'.$this->apiKeys[$brand]['EnterpriseId'].'",
                    "SecurityKey":"'.$this->apiKeys[$brand]['SecurityKey'].'",
                    "UserToken":"2C4F6225-1374-487D-AACB-60BCD42E7BEB",
                    "RSAOfferId": '.$coupon.'
                }';

            $response = $client->post($url, [
                'headers' => [
                    'Content-type' => 'application/json; charset=utf-8',
                ],
                'body' => $body,
            ]);

            $coupon = json_decode($response->getBody());

            return view('rsa.coupon-product-analysis', compact('coupon'));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
