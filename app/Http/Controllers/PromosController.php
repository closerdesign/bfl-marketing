<?php

namespace App\Http\Controllers;

use App\Exports\PromoExport;
use App\Http\Requests;
use App\Promo;
use App\User;
use Aws\S3\S3Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class PromosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['show', 'daily_summary']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $promos = Promo::available()->paginate(15);

        $old = false;

        return view('promos.index', compact('promos', 'old'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function old()
    {
        $promos = Promo::orderBy('start_date', 'desc')->paginate(15);

        $old = true;

        return view('promos.index', compact('promos', 'old'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('promos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'        => 'required',
            'image'       => 'image|max:1000',
            'description' => 'required',
            'start_date'  => 'required|date',
            'end_date'    => 'required|date',
            'price'       => 'required',
            'upc'         => 'required',
            'publish_date' => 'required',
            'department_id' => 'required|integer',
            'limit_per_transaction' => 'required|numeric',
            'coupon' => 'mimes:pdf|max:1000',
        ]);

        $filename = '';

        if ($request->hasFile('image')) {
            $filename = uniqid().'.'.$request->file('image')->getClientOriginalExtension();

            Storage::put('promos/'.$filename, file_get_contents($request->file('image')->getRealPath()), 'public');
        }

        $promo = new Promo($request->all());

        $coupon = '';

        if ($request->hasFile('coupon')) {
            $coupon = uniqid().'.'.$request->file('coupon')->getClientOriginalExtension();

            Storage::put('promo-coupon/'.$coupon, file_get_contents($request->file('coupon')->getRealPath()), 'public');
        }

        $promo->image = $filename;

        $promo->coupon = $coupon;

        $promo->save();

        $promo->stores()->attach($request->stores);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Promo saved successfully',
        ]);

        /*
         * This is where the image from the special is copied to the promo
         */

        /* commenting out for now. S3 will not allow /. Turns it into %2F
        if ($request->img_ava != '') {
            $img = substr(strrchr($request->img_ava, "/"), 1);
            $s3 = new S3Client([
                'version' => 'latest',
                'region' => env('S3_REGION'),
                'credentials' => array(
                    'key' => 'AKIAIDKLRMDGRPAGQYYA',
                    'secret'  => '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'
                )
            ]);

            $sourceBucket = 'specials';
            $targetBucket = 'promos';
            $bucket = 'bfl-corp-sara/';

            $s3->copyObject([
                'Bucket' => urlencode($bucket.$targetBucket),
                'Key' => $img,
                'CopySource' => urlencode($bucket.$sourceBucket."/".$img),
            ]);
        }
        */

        return redirect()->action('PromosController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $promos = Promo::current($id)
            ->where('websites', true)
            ->get();

        foreach ($promos as $promo) {
            $promo->valid = $promo->valid_dates;
        }

        return response($promos, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $promo = Promo::findOrFail($id);

        return view('promos.edit', compact('promo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'        => 'required',
            'image'       => 'image|max:1000',
            'description' => 'required',
            'start_date'  => 'required|date',
            'end_date'    => 'required|date',
            'price'       => 'required',
            'upc'         => 'required',
            'publish_date' => 'required',
            'department_id' => 'required|integer',
            'limit_per_transaction' => 'required|numeric',
            'coupon' => 'mimes:pdf|max:1000',
        ]);

        $promo = Promo::findOrFail($id);

        $promo->mobile_app_deal = false;

        $coupon = basename($promo->coupon);

        if ($request->hasFile('coupon')) {
            $coupon = uniqid().'.'.$request->file('coupon')->getClientOriginalExtension();

            Storage::put('promo-coupon/'.$coupon, file_get_contents($request->file('coupon')->getRealPath()), 'public');
        }

        $current_img = basename($promo->image);

        $promo->fill($request->all());

        if ($request->hasFile('image')) {
            $filename = uniqid().'.'.$request->file('image')->getClientOriginalExtension();

            Storage::put('promos/'.$filename, file_get_contents($request->file('image')->getRealPath()), 'public');

            $promo->image = $filename;
        } elseif ($current_img == 'pending-image.jpg') {
            $promo->image = '';
        } else {
            $promo->image = $current_img;
        }

        $promo->while_supplies_last = 0;

        if ($request->while_supplies_last == 1) {
            $promo->while_supplies_last = 1;
        }

        $promo->coupon = $coupon;

        $promo->save();

        if (empty($request->stores)) {
            $promo->stores()->detach();
        } else {
            $promo->stores()->sync($request->stores);
        }

        Session::flash('message', [
            'type' => 'success',
            'message' => 'Promo updated successfully',
        ]);

        return redirect('/promos');
    }

    /**
     *  Confirm resource deletion.
     */
    public function delete($id)
    {
        $promo = Promo::findOrFail($id);

        return view('promos.delete', compact('promo'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Promo::destroy($id);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Promo deleted successfully',
        ]);

        return redirect('/promos');
    }

    /**
     *  Excel Exportable File.
     */
    public function excel($id)
    {
        return Excel::download(new PromoExport($id), 'promo.xlsx');
    }

    /**
     *  Promos Cloning Feature.
     */
    public function cloning($id)
    {
        $old = Promo::findOrFail($id);

        $promo = new Promo();

        $promo->fill($old->toArray());

        $promo->image = basename($old->image);

        $promo->save();

        Session::flash('message', [
            'type' => 'success',
            'message' => 'Promo Cloned Successfully',
        ]);

        return redirect()->action('PromosController@edit', $promo->id);
    }

    /**
     *  Daily Summary.
     */
    public static function daily_summary()
    {
        $promos = Promo::available()->get();

        if (count($promos) > 0) {
            Mail::send('emails.promos.daily-summary', ['promos' => $promos], function ($m) use ($promos) {
                $m->from('digital@buyforlessok.com', 'BFL Media Team');

                $m->to('supportcenter@buyforlessok.com')
                    ->cc('jrodriguez@buyforlessok.com')
                    ->replyTo('digital@buyforlessok.com')
                    ->subject('['.date('m-d-Y').'] Here are our Promos for Today!: ');
            });
        }

        return 'Ok';
    }

    /**
     *  IT Processed.
     */
    public function it_processed($id)
    {
        $promo = Promo::findOrFail($id);

        $checked = [
            'Yes' => false,
            'No'  => true,
        ];

        $promo->it_processed = $checked[$promo->it_processed];

        $promo->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Promo Updated Successfully',
        ]);

        return back();
    }
}
