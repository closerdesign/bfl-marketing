<?php

namespace App\Http\Controllers;

use App\Brand;
use App\ProductRequest;
use App\TextSubscriber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use Jelovac\Bitly4laravel\Facades\Bitly4laravel;

class TextSubscribersController extends Controller
{
    public $brand;

    public $phone_number;

    public $request;

    public function __construct(Request $request)
    {
        $this->brand = [
            '14058967933' => 1,
            '14058967965' => 2,
            '14058967983' => 3,
            '14058967969' => 4,
        ];

        $this->brand = Brand::findOrFail($this->brand[$request->to]);
        $this->phone_number = $request->msisdn;
        $this->text = $request->text;
    }

    /**
     *  Send.
     */
    public function send($to, $message)
    {
        /* prepare data for sending */
        $data = [
            'User'          => 'buyforlessok', /* change to your EZ Texting username */
            'Password'      => 'C@mbiar123ezte', /* change to your EZ Texting password */
            'PhoneNumbers'  => [$to],
            'Subject'       => '',
            'Message'       => $message,
            'MessageTypeID' => 1,
        ];

        /* send message */
        $curl = curl_init('https://app.eztexting.com/sending/messages?format=json');
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($curl);
        curl_close($curl);

        /* parse result of API call */
        $json = json_decode($response);

        switch ($json->Response->Code) {
            case 201:
                echo 'success';
               // exit("Message Sent *1*");
            case 401:
                exit('Invalid user or password');
            case 403:
                $errors = $json->Response->Errors;
                exit('The following errors occurred: '.implode('; ', $errors));
            case 500:
                exit('Service Temporarily Unavailable');
        }
    }

    /**
     *  Subscribe.
     */
    public function subscribe($request)
    {
        $check = 0;

        $check = TextSubscriber::where('phone_number', $this->phone_number)
            ->where('brand_id', $this->brand->id)
            ->where('optin', true)
            ->count();

        $groups = $this->get_ez_text_groups();

        if (! isset($groups)) {
            $check = 0;
        }

        $list = [
            '1' => 'Buy For Less',
            '2' => 'Uptown',
            '3' => 'SmartSaver',
            '4' => 'Supermercado',
        ];

        if (! empty($groups)) {
            if (in_array($list[$this->brand->id], $groups)) {
                $check = 1;
            }
        } else {
            $check = 0;
        }

        $subscriber = TextSubscriber::updateOrCreate(
            ['phone_number' => $this->phone_number],
            ['brand_id'     => $this->brand->id, 'text' => $this->text]
        );

        $subscriber->save();

        $this->subscribe_eztext($request);

        $message = 'Thank you for signing up! You will receive your next '.$this->brand->name.' Subscriber Special soon!';

        if ($this->brand->rewards->count() > 0) {
            $url = URL::action('RewardsController@show', $this->brand->rewards->first()->id);
            $url = Bitly4laravel::shorten($url);
            $url = $url->data->url;

            $message = 'You\'ve unlocked a reward! Click here to see details: '.$url;
        }

        if ($this->brand->subscriber_specials()->count() > 0) {
            $url = action('AdsController@email', $this->brand->subscriber_specials()->first()->id);

            $url = Bitly4laravel::shorten($url);
            $url = $url->data->url;

            $message = 'This weekend only! Head to your nearest '.$this->brand->name.' to redeem your subscriber specials: '.$url;

            $this->send($request->msisdn, $message);
        } else {
            $this->send($request->msisdn, $message);
        }
    }

    /**
     *  Stop.
     */
    public function stop($request)
    {
        $subscriber = TextSubscriber::updateOrCreate(
            ['phone_number' => $this->phone_number],
            ['brand_id'     => $this->brand->id, 'optin' => false]
        );

        $subscriber->save();

        $message = 'Stop: You have successfully unsubscribed and will receive no more messages.';

        $this->send($request->msisdn, $message);
    }

    /**
     *  Product Request.
     */
    public function product_request($request)
    {
        $product = new ProductRequest($request->all());
        $product->brand_id = $this->brand->id;
        $product->save();

        $message = 'Thank you for your request. We will do our best in order to supply your favorite '.$this->brand->name.' store with the requested product.';

        $this->send($request->msisdn, $message);

        /*

        $brand = $this->brand;

        Mail::send('emails.product-requests.request', ['product' => $product, 'brand' => $brand, 'request' => $request], function ($m) use ($product, $brand, $request) {

           $emails = $brand->stores->map(function ($e) {
                return $e->email;
            })->toArray();

            $m->from('media@buyforlessok.com', 'Media Team');

            $m  ->to($emails)
                ->cc(['levans@buyforlessok.com', 'hbinkowski@buyforlessok.com', 'jrodriguez@buyforlessok.com', 'rmolder@buyforlessok.com'])
                ->replyTo('media@buyforlessok.com')
                ->subject('[' . $brand->name . '] A New Product Request Has Been Received');
        });
        */
        $this->subscribe($request);
    }

    public function subscribe_eztext($request)
    {
        if (substr($this->phone_number, 0, 1) == 1) {
            $this->phone_number = substr($this->phone_number, 1);
        }

        $list = [
            '1' => 'Buy For Less',
            '2' => 'Uptown',
            '3' => 'SmartSaver',
            '4' => 'Supermercado',
        ];

        $group = $list[$this->brand->id];

        $data = [
            'User'          => 'buyforlessok',
            'Password'      => 'C@mbiar123ezte',
            'PhoneNumber' => $this->phone_number,
            'FirstName'   => '',
            'LastName'    => '',
            'Email'       => '',
            'Note'        => $list[$this->brand->id],
            'Groups'      => [$group],

        ];

        $curl = curl_init('https://app.eztexting.com/contacts?format=json');
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($curl);
        curl_close($curl);

        /* parse result of API call */
        $json = json_decode($response);

        if ($json->Response->Code == 403) {
            $this->addExistingToGroup($request);

            return 'Added';
        }
        /*
                switch ($json->Response->Code) {
                    case 201:
                        exit("Message Sent");
                    case 401:
                        exit("Invalid user or password");
                    case 403:
                        $errors = $json->Response->Errors;
                        exit("The following errors occurred: " . implode('; ', $errors));
                    case 500:
                        exit("Service Temporarily Unavailable");
                }
        */
    }

    public function addExistingToGroup($request)
    {

      //  echo $this->phone_number;

        //Get EZ TEXT ID
        $curl = curl_init('https://app.eztexting.com/contacts?format=json&User=buyforlessok&Password=C@mbiar123ezte&query='.$this->phone_number);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($curl);
        curl_close($curl);

        $json = json_decode($response);
        if ($json->Response->Code == 403) {
            exit('Number Not found');
        }

        //       print_r($json->Response->Entries);

        foreach ($json->Response->Entries as $e) {
            $id = $e->ID;
            $groups = $e->Groups;
        }

        if (! isset($id)) {
            exit();
        }

        $list = [
            '1' => 'Buy For Less',
            '2' => 'Uptown',
            '3' => 'SmartSaver',
            '4' => 'Supermercado',
        ];

        $groups[] = $list[$this->brand->id];

        // print_r($groups);

        $data = [
            'User'         => 'buyforlessok',
            'Password'      => 'C@mbiar123ezte',
            'PhoneNumber' => $this->phone_number,
            'Groups'      => $groups,
        ];

        $curl = curl_init('https://app.eztexting.com/contacts/'.$id.'?format=json');
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($curl);
        curl_close($curl);

        $json = json_decode($response);

        /*

       switch ($json->Response->Code) {
           case 201:
               exit("Message Sent");
           case 401:
               exit("Invalid user or password");
           case 403:
               $errors = $json->Response->Errors;
               exit("The following errors occurred: " . implode('; ', $errors));
           case 500:
               exit("Service Temporarily Unavailable");
       }
       */
    }

    public function get_ez_text_groups()
    {
        if (substr($this->phone_number, 0, 1) == 1) {
            $this->phone_number = substr($this->phone_number, 1);
        }

        //Get EZ TEXT INFO
        $curl = curl_init('https://app.eztexting.com/contacts?format=json&User=buyforlessok&Password=C@mbiar123ezte&query='.$this->phone_number);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($curl);
        curl_close($curl);

        $json = json_decode($response);
        if ($json->Response->Code == 403) {
            exit('Number Not found');
        }

        //       print_r($json->Response->Entries);

        foreach ($json->Response->Entries as $e) {
            $id = $e->ID;
            $groups = $e->Groups;
        }

        if (isset($groups)) {
            return $groups;
        } else {
            return '';
        }
    }

    /**
     * rewards program.
     */
    public function rewards($request)
    {
        $this->subscribe_eztext($request);

        $rewards_url[1] = 'https://www.buyforlessok.com/loyalty?utm_source=Texting%20Campaign';
        $rewards_url[2] = 'https://www.uptowngroceryco.com/loyalty?utm_source=Texting%20Campaign';
        $rewards_url[3] = 'https://smartsaverok.com/loyalty?utm_source=Texting%20Campaign';
        $rewards_url[4] = 'https://www.buyforlessok.com/loyalty?utm_source=Texting%20Campaign';

        $loyalty_url = $rewards_url[$this->brand->id];

        $url = Bitly4laravel::shorten($loyalty_url);
        $url = $url->data->url;

        $message = 'Welcome to '.$this->brand->name.'. Download our app to start earning rewards and redeeming coupons: '.$url;

        $this->send($request->msisdn, $message);
    }

    /**
     *  Inbound.
     */
    public function inbound(Request $request)
    {
        switch (strtolower($request->text)) {
            case 'bed':
                $this->subscribe($request);

                break;

            case 'bigsale':
                $this->subscribe($request);

                break;

            case 'cama':
                $this->subscribe($request);

                break;

            case 'fiestas':
                $this->subscribe($request);

                break;

            case 'health':
                $this->subscribe($request);

                break;

            case 'holaok':
                $this->subscribe($request);

                break;

            case 'hola ok':
                $this->subscribe($request);

                break;

            case 'holiday':
                $this->subscribe($request);

                break;

            case 'holidays':
                $this->subscribe($request);

                break;

            case 'exitos':
                $this->subscribe($request);

                break;

            case 'zeta':
                $this->subscribe($request);

                break;

            case 'secret':
                $this->subscribe($request);

                break;

            case 'secreto':
                $this->subscribe($request);

                break;

            case 'test2':
                $this->subscribe_eztext($request);

                break;

            case 'valentine':
                $this->subscribe($request);

                break;

            case 'grill':
                $this->subscribe($request);

                break;

            case 'rewards':
                $this->rewards($request);

                break;

            case 'stop':
                $this->stop($request);

                break;

            default: $this->product_request($request);
        }
    }
}
