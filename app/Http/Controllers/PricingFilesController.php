<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\PricingFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class PricingFilesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        //2019-06-27_02-30_ONLINESHOP_1006.dat.filepart
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $files = PricingFile::orderBy('created_at', 'DESC')->paginate(10);

        return view('pricing-files.index', compact('files'));
    }

    /**
     *  Pickup Retalix Files.
     */
    public static function pickup()
    {
        $files = Storage::disk('retalix')->files('uploads');

        foreach ($files as $file) {
            $filename = basename($file);

            $extension = explode('.', $filename);

            if ($extension[1] == 'dat') {
                $content = Storage::disk('retalix')->get('uploads/'.$filename);

                Storage::put('pricing-files/'.$filename, $content, 'public');

                $pricing = new PricingFile([
                    'name'   => $filename,
                    'file'   => $filename,
                    'store'  => 9515,
                    'status' => 0,
                ]);

                $pricing->save();

                Storage::disk('retalix')->delete('uploads/'.$filename);
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'file'    => 'required|max:5000',
            'store'   => 'required',
        ]);

        $file = new PricingFile();

        $file->name = $request->file('file')->getClientOriginalName();

        $filename = uniqid().'.'.$request->file('file')->getClientOriginalExtension();

        Storage::put('pricing-files/'.$filename, file_get_contents($request->file('file')->getRealPath()), 'public');

        $file->file = $filename;

        $file->store = $request->store;

        $file->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'The file has been successfully created and added to the processing queue.',
        ]);

        return back();
    }

    public function destroy($id)
    {
        $file = PricingFile::findOrFail($id);

        $file->destroy($id);

        Session::flash('message', [
            'type' => 'success',
            'message' => 'File successfully deleted.',
        ]);

        return back();
    }
}
