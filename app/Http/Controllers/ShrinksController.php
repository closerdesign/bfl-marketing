<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Shrink;
use App\ShrinkGroup;
use App\Store;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ShrinksController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $date_from = null;
        $date_to = null;

        if (isset($_GET['date_from']) && isset($_GET['date_to'])) {
            $date_from = $_GET['date_from'];
            $date_to = $_GET['date_to'];
        }

        $groups = DB::table('shrinks')
            ->select('shrink_groups.id', 'shrink_groups.name')
            ->join('shrink_items', 'shrink_items.id', '=', 'shrinks.shrink_item_id')
            ->join('shrink_groups', 'shrink_groups.id', '=', 'shrink_items.shrink_group_id')
            ->whereBetween('shrinks.date', [$date_from, $date_to])
            ->groupBy('shrink_groups.id')
            ->get();

        foreach ($groups as $group) {
            $group->products = DB::table('shrinks')
                ->select('shrink_items.id', 'shrink_items.name', DB::raw('sum(shrinks.value) as value'))
                ->join('shrink_items', 'shrink_items.id', '=', 'shrinks.shrink_item_id')
                ->where('shrink_items.shrink_group_id', '=', $group->id)
                ->whereBetween('shrinks.date', [$date_from, $date_to])
                ->groupBy('shrink_items.id')
                ->get();
        }

        return view('shrinks.index', compact('date_from', 'date_to', 'groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'shrink_item_id' => 'required|integer',
            'value'          => 'required|regex:/^\d*(\.\d{1,2})?$/',
            'date'           => 'required|date',
            'store_id'       => 'required|integer',
        ]);

        $shrink = new Shrink($request->all());

        $shrink->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Shrink amount reported successfully',
        ]);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
