<?php

namespace App\Http\Controllers;

use App\RocEntry;
use App\RocEntryCompletion;
use App\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class RocEntryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $entries = RocEntry::where('roc_id', null)->get();

        return view('roc-entry.index', compact('entries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('roc-entry.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'content' => 'required',
            'stores' => 'required',
        ]);

        $entry = new RocEntry(request()->all());

        $stores = implode(',', request()->stores);

        $entry->stores = $stores;

        $entry->user_id = auth()->user()->id;

        $entry->save();

        return redirect()
            ->action('RocEntryController@edit', $entry->id)
            ->with([
            'message' => [
                'type' => 'success',
                'message' => Lang::get('roc-entry.title').': '.Lang::get('general.saved'),
            ],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RocEntry  $rocEntry
     * @return \Illuminate\Http\Response
     */
    public function show(RocEntry $rocEntry)
    {
        return view('roc-entry.show', compact('rocEntry'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RocEntry  $rocEntry
     * @return \Illuminate\Http\Response
     */
    public function edit(RocEntry $rocEntry)
    {
        return view('roc-entry.edit', compact('rocEntry'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RocEntry  $rocEntry
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RocEntry $rocEntry)
    {
        $this->validate($request, [
            'name' => 'required',
            'content' => 'required',
            'stores' => 'required',
        ]);

        $rocEntry->fill(request()->all());

        $stores = implode(',', request()->stores);

        $rocEntry->stores = $stores;

        $rocEntry->user_id = auth()->user()->id;

        $rocEntry->save();

        return redirect()
            ->action('RocEntryController@edit', $rocEntry->id)
            ->with([
                'message' => [
                    'type' => 'success',
                    'message' => Lang::get('roc-entry.title').': '.Lang::get('general.updated'),
                ],
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RocEntry  $rocEntry
     * @return \Illuminate\Http\Response
     */
    public function destroy(RocEntry $rocEntry)
    {
        $rocEntry->delete($rocEntry->id);

        return back()->with([
            'message' => [
                'type'    => 'success',
                'message' => Lang::get('roc-entry.title').': '.Lang::get('general.deleted'),
            ],
        ]);
    }

    /**
     *  Mark As Completed.
     */
    public function mark_as_completed(Request $request, $id)
    {
        $completion = new RocEntryCompletion([
            'roc_entry_id' => $id,
            'store_id' => Store::where('store_code', auth()->user()->store)->first()->id,
            'completed_by' => auth()->user()->name,
        ]);

        $completion->save();

        return redirect()
            ->action('StoreLevelController@home')
            ->with([
                'message' => [
                    'type' => 'success',
                    'message' => Lang::get('roc-entry.title').': '.Lang::get('general.completed'),
                ],
            ]);
    }
}
