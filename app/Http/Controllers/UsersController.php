<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderBy('name');

        if (isset($_GET['keyword'])) {
            $users = $users->where(function ($q) {
                $q->where('name', 'like', '%'.$_GET['keyword'].'%')
                    ->orWhere('email', 'like', '%'.$_GET['keyword'].'%')
                    ->orWhere('phone', 'like', '%'.$_GET['keyword'].'%');
            });
        }

        $users = $users->get();

        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'   => 'required',
            'email'  => 'required|unique:users,email',
            'admin'  => 'required|boolean',
            'phone'  => 'required|numeric',
            'status' => 'required|boolean',
        ]);

        $user = new User($request->all());

        $password = uniqid();

        $user->password = Hash::make($password);

        $user->save();

        Mail::send('emails.new-user', ['user' => $user, 'password' => $password], function ($m) use ($user, $password) {
            $m->from(Auth::user()->email, Auth::user()->name);

            $m->to($user->email, $user->name)
                ->replyTo(Auth::user()->email)
                ->subject('Welcome to SARA Platform!');
        });

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'User created successfully!',
        ]);

        return redirect()->action('UsersController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        return view('users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'admin' => 'required|boolean',
            'status' => 'required|boolean',
        ]);

        $user = User::findOrFail($id);

        $user->fill($request->all());

        $user->save();

        Session::flash('message', [
            'type'   => 'success',
            'message' => 'User profile updated successfully!',
        ]);

        return redirect()->action('UsersController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);

        Session::flash('message', [
            'type' => 'success',
            'message' => 'User removed successfully',
        ]);

        return back();
    }

    /**
     *  Display Password Change Form.
     */
    public function password()
    {
        return view('users.password');
    }

    /**
     *  Processing Password Update.
     */
    public function password_update(Request $request)
    {
        $this->validate($request, [
            'current_password'      => 'required',
            'password'              => 'required|confirmed',
        ]);

        if (Hash::check($request->current_password, Auth::user()->password)) {
            $user = User::findOrFail(Auth::user()->id);

            $user->password = bcrypt($request->password);

            $user->save();

            Session::flash('message', [
                'type'    => 'success',
                'message' => 'Your password has been updated!',
            ]);
        } else {
            Session::flash('message', [
                'type'    => 'danger',
                'message' => 'Your current password is wrong. Please check.',
            ]);
        }

        return back();
    }
}
