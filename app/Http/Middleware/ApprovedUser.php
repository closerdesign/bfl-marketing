<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ApprovedUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        dd(Auth::user()->isApproved());

        if (Auth::check() && (Auth::user()->isApproved() == 'Pending Approval')) {
            Auth::logout();

            Session::flash('message', [
                'type'     => 'danger',
                'message'  => 'You Account Has Not Yet Been Approved',
            ]);

            return redirect('/home');
        }

        return $next($request);
    }
}
