<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'donation-requests',
        'dlrs',
        'event-signups',
        'huddles/store/{id}',
        'logout',
        'product-requests/web',
        'teacher-votes',
        'text-to-product',
        'volunteers',
        'sales',
    ];
}
