<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class Store
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && Auth::user()->isStore()) {
            return $next($request);
        }

        Session::flash('message', [
            'type'    => 'danger',
            'message' => 'Restricted Access.',
        ]);

        return redirect('/home');
    }
}
