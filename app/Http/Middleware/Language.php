<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $lang = substr(\Illuminate\Support\Facades\Request::server('HTTP_ACCEPT_LANGUAGE'), 0, 2);

        if (Session::has('language')) {
            $lang = Session::get('language');
        }

        App::setLocale($lang);

        return $next($request);
    }
}
