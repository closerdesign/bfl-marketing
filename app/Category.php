<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class Category extends Model
{
    protected $fillable = [
        'name',
        'parent_id',
        'name_es',
        'active',
    ];

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id')->where('active', 1);
    }

    public function products()
    {
        return $this->hasMany(\App\Product::class);
    }

    public function scopeShopping($query, $store)
    {
        return $query;
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    public static function cats_with_products(int $store)
    {
        $r = DB::select(DB::raw('SELECT * FROM categories c 
                              WHERE c.id IN (SELECT category_id FROM category_product cp 
                              JOIN products p ON p.id = cp.product_id 
                              JOIN shopping_products sp ON sp.sku = p.upc
                              WHERE sp.store_number = :store) OR parent_id = 0 ORDER BY id, parent_id'), [$store]);

        return $r;
    }
}
