<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroceryAd extends Model
{
    protected $fillable = [
        'ad_id',
        'brand',
        'product',
        'size',
        'upc',
        'price',
        'position',
        'notes',
    ];
}
