<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Ad extends Model
{
    protected $fillable = [
        'name',
        'date_from',
        'date_to',
        'brand_id',
        'status',
        'websites',
        'header_image',
        'dotw_image',
        'custom_template',
    ];

    protected $hidden = [
        'status',
        'brand_id',
        'created_at',
        'updated_at',
    ];

    public function getStatusAttribute($value)
    {
        $status = 'Pending';

        if ($value === 1) {
            $status = 'Revised';
        }

        return $status;
    }

    public function getWebsitesAttribute($value)
    {
        $websites = 'No';

        if ($value == 1) {
            $websites = 'Yes';
        }

        return $websites;
    }

    public function getHeaderImageAttribute($value)
    {
        $header_image = '';

        if ($value != '') {
            $header_image = Storage::url('ads/header-images/'.$value);
        }

        return $header_image;
    }

    public function stores()
    {
        return $this->belongsToMany(\App\Store::class);
    }

    public function brands()
    {
        return $this->belongsTo(\App\Brand::class, 'brand_id', 'id');
    }

    public function specials()
    {
        return $this->hasMany(\App\Special::class)->orderBy('position_number');
    }

    public function featured()
    {
        return $this->hasMany(\App\Special::class)->orderBy('position_number')
            ->where('status', 1)
            ->where('featured', 1);
    }

    public function plu_specials()
    {
        return $this->hasMany(\App\Special::class)->where('plu_required', true);
    }

    public function pages()
    {
        return $this->hasMany(\App\Adpage::class, 'ad', 'id');
    }

    public function files()
    {
        return $this->hasMany(\App\Adfile::class, 'ad', 'id');
    }

    /**
     *  Banners Relationship.
     */
    public function banners()
    {
        return $this->hasMany(\App\AdBanner::class);
    }

    /**
     *  Coming Scope.
     */
    public function scopeComing($query)
    {
        return $query
            ->where('date_to', '>', date('Y-m-d'));
    }

    /**
     *  Current Scope.
     */
    public function scopeCurrent($query)
    {
        return $query
            ->where('date_from', '<=', date('Y-m-d'))
            ->where('date_to', '>=', date('Y-m-d'))
            ->where('status', true);
    }
}
