<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Adpage extends Model
{
    protected $fillable = [
        'image',
        'ad',
    ];

    public function ads()
    {
        return $this->belongsTo(\App\Ad::class, 'ad', 'id');
    }

    public function getImageAttribute($value)
    {
        return Storage::url('adpages/'.$value);
    }
}
