<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DonationRequest extends Model
{
    protected $fillable = [

        'brand',
        'organization',
        'website',
        'org_phone',
        'address',
        'nonprofit',
        'nonprofit_number',
        'prev_donation',
        'prev_details',
        'bfl_employees',
        'bfl_employee_list',
        'org_mission',
        'event',
        'event_website',
        'event_date',
        'event_time',
        'pickup_date',
        'event_location',
        'beneficiaries',
        'request_type',
        'items',
        'additional_info',
        'attach_file',
        'advertising',
        'advertising_details',
        'logos',
        'logo_email',
        'first_name',
        'last_name',
        'email',
        'phone',
        'location',
        'referral_source',
        'status',

    ];
}
