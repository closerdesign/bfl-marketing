<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoreAislePanels extends Model
{
    //
    protected $fillable = [
        'panel_name',
        'aisle_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * Aisle relationship
     */
    public function aisle()
    {
        return $this->belongsTo(StoreAisle::class);
    }
}
