<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    protected $fillable = [
        'store_id',
        'controller_name',
        'status',
    ];

    /**
     *  Store Relationship.
     */
    public function store()
    {
        return $this->belongsTo(\App\Store::class);
    }

    /**
     *  Items Relationship.
     */
    public function items()
    {
        return $this->hasMany(InventoryItem::class);
    }
}
