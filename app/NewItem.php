<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewItem extends Model
{
    protected $fillable = [

        'brand_id',
        'image',
        'title',
        'description',
        'status',

    ];

    /**
     *  Brand Relationship.
     */
    public function brands()
    {
        return $this->belongsTo(\App\Brand::class, 'brand_id', 'id', 'brands');
    }
}
