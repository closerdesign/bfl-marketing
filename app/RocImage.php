<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class RocImage extends Model
{
    protected $fillable = [
        'image',
    ];

    /**
     *  MUTATORS.
     */

    /**
     *  Image Mutator.
     */
    public function getImageAttribute($value)
    {
        return Storage::url('roc-image/'.$value);
    }
}
