<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Termination extends Model
{
    protected $fillable = [

        'first_name',
        'preferred_name',
        'last_name',
        'store',
        'department',
        'termination_date',
        'last_day',
        'termination_type',
        'reason',
        'other_details',
        'rehire',
        'notice_given',
        'badge_collected',
        'replacement',
        'notes',
        'attachment',
        'completed_by',

    ];

    public function getFileAttribute($value)
    {
        return Storage::url('termination/'.$value);
    }
}
