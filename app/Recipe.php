<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Recipe extends Model
{
    protected $fillable = [
        'name',
        'image',
        'ingredients',
        'directions',
        'status',
    ];

    public function getStatusAttribute($value)
    {
        $status = 'Unpublished';

        if ($value == 1) {
            $status = 'Published';
        }

        return $status;
    }

    public function getImageAttribute($value)
    {
        return Storage::url('recipes/'.$value);
    }
}
