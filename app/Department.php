<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = [
        'code',
        'name',
        'active',
    ];

    public function shopping_items()
    {
        return $this->hasMany(\App\ShoppingItem::class, 'department_id', 'id');
    }

    public function specials()
    {
        return $this->hasMany(\App\Special::class);
    }

    public function sales()
    {
        return $this->hasMany(SalesData::class, 'dept_name', 'name');
    }

    /*  Code Attribute
     *
     * */

    public function getCodeAttribute($value)
    {
        return sprintf('%03d', $value);
    }
}
