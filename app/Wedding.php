<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wedding extends Model
{
    protected $fillable = [
        'status',
    ];

    public function comments()
    {
        return $this->hasMany(\App\WeddingComment::class)->orderBy('created_at', 'desc');
    }
}
