<?php

    /**
     *  Calculate UPC Check Digit.
     */
    function generate_upc_checkdigit($upc_code)
    {
        if (strlen($upc_code) < 11) {
            $upc_code = str_pad($upc_code, 11, 0, STR_PAD_LEFT);
        }

        $odd_total = 0;
        $even_total = 0;

        for ($i = 0; $i < 11; $i++) {
            if ((($i + 1) % 2) == 0) {
                /* Sum even digits */
                $even_total += $upc_code[$i];
            } else {
                /* Sum odd digits */
                $odd_total += $upc_code[$i];
            }
        }

        $sum = (3 * $odd_total) + $even_total;

        /* Get the remainder MOD 10*/
        $check_digit = $sum % 10;

        /* If the result is not zero, subtract the result from ten. */
        $digit = ($check_digit > 0) ? 10 - $check_digit : $check_digit;

        $upc = (int) $upc_code.(int) $digit;

        return $upc;
    }

    /**
     *  Format Retalix UPC.
     */
    function format_retalix_upc($upc)
    {
        $upc = generate_upc_checkdigit($upc);

        return str_pad($upc, 14, 0, STR_PAD_LEFT);
    }

    /**
     *  Extract PLU.
     */
    function extract_plu($upc)
    {
        $plu = substr($upc, 0, -6);

        $plu = ltrim($plu, '0');

        $plu = $plu.'00000';

        return $plu;
    }

    /**
     *  Produce PLU.
     */
    function produce_plu($plu)
    {
        $plu = ltrim($plu, '0');

        $plu = rtrim($plu, 1);

        $last = substr($plu, -1, 1);

        $first = substr($plu, 0, 3);

        $plu = $first.$last;

        return $plu;
    }
