<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

class Promo extends Model
{
    protected $fillable = [

        'name',
        'image',
        'description',
        'publish_date',
        'start_date',
        'end_date',
        'price',
        'while_supplies_last',
        'upc',
        'department_id',
        'limit_per_transaction',
        'additional_upcs',
        'comments',
        'coupon',
        'it_processed',
        'websites',
        'promo_number',
        'mobile_app_deal',

    ];

    protected $hidden = [

        'department_id',
        'comments',
        'created_at',
        'updated_at',

    ];

    public function brands()
    {
        return $this->belongsToMany(\App\Brand::class);
    }

    public function stores()
    {
        return $this->belongsToMany(\App\Store::class);
    }

    public function department()
    {
        return $this->belongsTo(\App\Department::class);
    }

    public function scopeCurrent($query, $brand)
    {
        return $query
            ->join('promo_store', 'promo_id', '=', 'promos.id')
            ->join('stores', 'promo_store.store_id', '=', 'stores.id')
            ->join('brands', 'stores.brand_id', '=', 'brands.id')
            ->where('brands.id', '=', $brand)
            ->where('publish_date', '<=', date('Y-m-d'))
//            ->where('start_date', '<=', date('Y-m-d', strtotime(date('Y-m-d') . ' + 10 days')))
            ->where('end_date', '>=', date('Y-m-d'))
            ->groupBy('promos.id')
            ->orderBy('end_date', 'asc')
            ->select('promos.*');
    }

    public function scopeAvailable($query)
    {
        return $query
            ->where('end_date', '>=', date('Y-m-d'))
            ->orderBy('end_date', 'desc');
    }

    public function getValidDatesAttribute()
    {
        $valid = 'While Supplies Last';

        if ($this->while_supplies_last != 1) {
            $valid = 'Valid '.date('F jS', strtotime($this->start_date)).' to '.date('F jS', strtotime($this->end_date));
        }

        return $valid;
    }

    public function getImageAttribute($value)
    {
        if ($value == '') {
            return URL::to('img/pending-image.jpg');
        } else {
            return Storage::url('promos/'.$value);
        }
    }

    public function getCouponAttribute($value)
    {
        if ($value != '') {
            $value = Storage::url('promo-coupon/'.$value);
        } else {
            $value = '';
        }

        return $value;
    }

    public function getItProcessedAttribute($value)
    {
        $it_processed = 'No';

        if ($value == true) {
            $it_processed = 'Yes';
        }

        return $it_processed;
    }
}
