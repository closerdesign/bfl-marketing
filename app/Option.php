<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $fillable = [
        'name',
        'choice_id',
        'is_default',
        'surcharge',
    ];

    public function getIsDefaultAttribute($value)
    {
        $default = 'No';

        if ($value == 1) {
            $default = 'Yes';
        }

        return $default;
    }
}
