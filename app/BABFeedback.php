<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BABFeedback extends Model
{
    protected $table = 'bab_feedback';

    protected $fillable = [
        'first_name',
        'last_name',
        'role',
        'store',
        'email',
        'executive_email',
        'location',
        'meeting_date',
        'time_start',
        'time_end',
        'leader',
        'present',
        'absent',
        'praise_report',
        'wins',
        'brand_demo',
        'how_poa',
        'guest_one',
        'guest_two',
        'support_needs',
        'learning_sc',
        'focus_old',
        'focus_new',
        'receipt_ideas',
        'addtl_info',
        'attachment',
    ];
}
