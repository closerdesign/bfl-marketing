<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roc extends Model
{
    protected $fillable = [
        'date',
    ];

    /**
     *  RELATIONSHIPS.
     */

    /**
     *  Entries.
     */
    public function entries()
    {
        return $this->hasMany(RocEntry::class);
    }
}
