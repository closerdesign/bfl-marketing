<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Huddle extends Model
{
    protected $fillable = [

        'store_id',
        'leader',
        'details',
        'ideas',
        'requests',
        'comments',

    ];

    /**
     *  Store Relationship.
     */
    public function store()
    {
        return $this->belongsTo(\App\Store::class);
    }
}
