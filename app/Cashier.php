<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cashier extends Model
{
    protected $fillable = [
        'store_number',
        'cashier_number',
        'name',
        'start',
        'birthday',
        'remarks',
        'type',
        'dont_sign_on_sale_fg',
        'dont_sign_on_trn_fg',
        'record_status_date',
    ];
}
