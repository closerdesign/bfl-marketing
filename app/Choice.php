<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Choice extends Model
{
    protected $fillable = [
        'name',
        'campaign_item_id',
    ];

    public function options()
    {
        return $this->hasMany(\App\Option::class);
    }
}
