<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampaignItemOccurrence extends Model
{
    protected $fillable = [
        'campaign_item_id',
        'event_date_time',
        'store',
    ];
}
