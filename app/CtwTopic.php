<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CtwTopic extends Model
{
    protected $fillable = ['topic'];

    /**
     *  Area Relationship.
     */
    public function area()
    {
        return $this->belongsTo(CtwArea::class);
    }
}
