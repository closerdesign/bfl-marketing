<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CashierNumbers extends Model
{
    protected $fillable = [
        'cashier_number',
        'store_id',
        'employee_id',
    ];

    /**
     *  Stores Relationship.
     */
    public function store()
    {
        return $this->belongsTo(\App\Store::class);
    }
}
