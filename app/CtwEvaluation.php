<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CtwEvaluation extends Model
{
    protected $fillable = ['store_id', 'comments'];

    /**
     *  User ID Default Value.
     */
    public function __construct(array $attributes = [])
    {
        $this->user_id = auth()->user()->id;

        parent::__construct($attributes);
    }

    /**
     *  Scores Relationship.
     */
    public function scores()
    {
        return $this->hasMany(CtwScore::class, 'evaluation_id');
    }

    /**
     *  Store Relationship.
     */
    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    /**
     *  User Relationship.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
