<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrainingSlide extends Model
{
    protected $fillable = [

        'training_id',
        'name',
        'description',
        'image',
        'video',
        'position',

    ];
}
