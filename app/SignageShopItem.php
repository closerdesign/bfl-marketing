<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SignageShopItem extends Model
{
    protected $fillable = [

        'signage_shop_id',
        'name',
        'description',
        'file',
        'cover_image',
        'request',
        'download',
        'status',

    ];

    /**
     *  Return Active Items.
     */
    public function scopeActive($query)
    {
        return $query
            ->where('status', 1)
            ->orderBy('updated_at', 'asc');
    }
}
