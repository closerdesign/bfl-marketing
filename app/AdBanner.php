<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

class AdBanner extends Model
{
    protected $fillable = [
        'file',
        'ad_id',
    ];

    /**
     *  Image Attribute.
     */
    public function getFileAttribute($value)
    {
        return Storage::url('ad-banners/'.$value);
    }
}
