<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Adfile extends Model
{
    protected $fillable = [
        'file',
        'ad',
    ];

    public function getFileAttribute($value)
    {
        return Storage::url('adfiles/'.$value);
    }
}
