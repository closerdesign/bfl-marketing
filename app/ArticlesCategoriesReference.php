<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticlesCategoriesReference extends Model
{
    protected $fillable = [
        'articles_category_id',
        'article_id',

    ];

    protected $table = 'articles_categories_reference';

    public function article_categories()
    {
        return $this->belongsTo(\App\ArticleCategory::class, 'id');
    }

    public function articles()
    {
        return $this->belongsTo(\App\Article::class, 'id');
    }
}
