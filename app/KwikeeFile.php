<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class KwikeeFile extends Model
{
    protected $fillable = [
        'name',
        'filename',
        'last_record',
        'status',
    ];

    public function getFilenameAttribute($value)
    {
        $filename = '';

        if ($value != '') {
            $filename = Storage::url('kwikee-xml/'.$value);
        }

        return $filename;
    }

    public function getStatusAttribute($value)
    {
        $status = [
            '0' => 'Pending',
            '1'  => 'In Progress',
            '2' => 'Completed',
        ];

        return $status[$value];
    }
}
