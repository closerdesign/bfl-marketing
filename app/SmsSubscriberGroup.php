<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmsSubscriberGroup extends Model
{
    protected $fillable = ['sms_group_id',
            'sms_subscriber_id',
        ];

    protected $table = 'sms_subscriber_group';
}
