<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GalleryCategories extends Model
{
    protected $fillable = [
        'category_name',
    ];
}
