<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmsSubscriber extends Model
{
    //
    protected $fillable = [
        'id',
        'phone_number',
        'subscribed',
    ];
}
