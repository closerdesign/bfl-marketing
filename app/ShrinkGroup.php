<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShrinkGroup extends Model
{
    protected $fillable = [
        'name',
    ];

    public function items()
    {
        return $this->hasMany(\App\ShrinkItem::class, 'shrink_group_id', 'id');
    }
}
