<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsletterImage extends Model
{
    protected $fillable = [

        'newsletter_id',
        'image',

    ];
}
