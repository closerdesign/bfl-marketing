<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShoppingProduct extends Model
{
    protected $fillable = [

        'store_id',
        'sku',
        'brand_name',
        'product_name',
        'description',
        'size',
        'uom',
        'dept_code',
        'scale_flag',
        'how_to_sell',
        'avg_weight',
        'wgt_selector',
        'store_number',
        'ad_flag',
        'cool',
        'regular_price',
        'sale_price',
        'sale_qty',
        'promo_start_date',
        'promo_end_date',
        'promo_buy_qty',
        'promo_get_qty',
        'promo_min_qty',
        'promo_limit_qty',
        'family_sale_flag',
        'family_sale_code',
        'family_sale_desc',
        'tax_pct',
        'bottle_deposit',
        'promo_tag',
        'movement',

    ];

    protected $hidden = [
        'store_id',
        'brand_name',
        'product_name',
        'dept_code',
        'scale_flag',
        'how_to_sell',
        'store_number',
        'family_sale_flag',
        'family_sale_code',
        'family_sale_desc',
        'bottle_deposit',
    ];

    public function product()
    {
        return $this->belongsTo(\App\Product::class, 'sku', 'upc');
    }
}
