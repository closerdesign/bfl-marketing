<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RetalixPrice extends Model
{
    protected $fillable = [
        'upc_ean',
        'product_description',
        'prm_store_number',
        'ip_unit_price',
        'ip_price_multiple',
        'descriptive_size',
        'ip_start_date',
        'start_date',
        'ip_end_date',
        'end_date',
        'price_strategy',
        'ad_group',
        'dept_code',
        'scale_flag',
    ];

    protected $appends = [
        'upc',
        'online_price',
    ];

    protected $hidden = [
        'id',
        'prm_store_number',
        'ip_start_date',
        'ip_end_date',
        'price_strategy',
        'created_at',
        'updated_at',
    ];

    /**
     *  Format Ip Unit Price.
     */
    public function getIpUnitPriceAttribute($price)
    {
        return number_format($price, 2);
    }

    /**
     *  UPC Attribute.
     */
    public function getUpcAttribute()
    {
        $upc = ltrim($this->attributes['upc_ean'], '0');

        $upc = substr($upc, 0, -1);

        return $upc;
    }

    /**
     *  PLU Attribute.
     */
    public function getPluAttribute()
    {
        return extract_plu($this->upc_ean);
    }

    /**
     *  Online Price Attribute.
     */
    public function getOnlinePriceAttribute()
    {
        $price = $this->attributes['ip_unit_price'];

        $price = $price + ($price * 0.07);

        $price = number_format($price, 2);

        return $price;
    }
}
