<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Campaign extends Model
{
    protected $fillable = [
        'name',
        'url',
        'end_date',
        'background_image',
        'primary_color',
        'secondary_color',
        'status',
        'brand_id',
        'description',
        'terms',
        'pickup_start_time',
        'pickup_end_time',
        'prior_notice',
        'pickup_start_date',
        'pickup_deadline',
        'pickup_deadline_time',
        'tag_manager_head',
        'tag_manager_body',
        'warning',
        'offline_mode_list',
        'template',
    ];

    protected $hidden = [
        'id',
        'status',
        'created_at',
        'updated_at',
    ];

    /**
     *  End Date Setter.
     */
    public function setEndDateAttribute($value)
    {
        if ($value == '') {
            $this->attributes['end_date'] = null;
        } else {
            $this->attributes['end_date'] = $value;
        }
    }

    public function getBackgroundImageAttribute($value)
    {
        return Storage::url('campaigns/'.$value);
    }

    public function getStatusAttribute($value)
    {
        $status = 'Inactive';

        if ($value == 1) {
            $status = 'Active';
        }

        return $status;
    }

    public function brands()
    {
        return $this->belongsTo(\App\Brand::class, 'brand_id', 'id', 'brands');
    }

    public function categories()
    {
        return $this->belongsToMany(\App\CampaignCategory::class, 'campaign_campaign_category', 'campaign_id', 'campaign_category_id')
            ->where('status', 1)
            ->orderBy('position');
    }

    public function stores()
    {
        return $this->belongsToMany(\App\Store::class);
    }

    /**
     *  FAQs.
     */
    public function faqs()
    {
        return $this->hasMany(\App\CampaignFaq::class);
    }
}
