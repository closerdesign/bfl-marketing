<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImprovementRequest extends Model
{
    protected $fillable = [
        'title',
        'description',
        'file',
    ];

    public function users()
    {
        return $this->belongsTo(\App\User::class, 'user_id', 'id', 'users');
    }

    public function getStatusAttribute($value)
    {
        return str_replace('_', ' ', $value);
    }
}
