<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Brand extends Model
{
    protected $fillable = [
        'name',
        'logo',
        'icon',
        'website',
        'online_shopping_website',
        'ios_mobile_app_url',
        'android_mobile_app_url',
        'shipt_url',
        'loyalty_website_url',
        'facebook_url',
        'instagram_url',
    ];

    public function getLogoAttribute($value)
    {
        return Storage::url('brands/'.$value);
    }

    /**
     *  Icon Attribute.
     */
    public function getIconAttribute($value)
    {
        $icon = '/img/pending-image.jpg';

        if ($value != null) {
            $icon = Storage::url('brand/icon/'.$value);
        }

        return $icon;
    }

    public function campaigns()
    {
        return $this->hasMany(\App\Campaign::class);
    }

    /**
     *  Events Relationship.
     */
    public function events()
    {
        return $this->hasMany(Event::class)->current();
    }

    public function promos()
    {
        return $this->hasManyThrough(\App\Promo::class, \App\Store::class);
    }

    /**
     *  Rewards Relationship.
     */
    public function rewards()
    {
        return $this->hasMany(\App\Reward::class)
            ->where('status', true)
            ->where('start_date', '<=', date('Y-m-d'))
            ->where('end_date', '>=', date('Y-m-d'));
    }

    public function stores()
    {
        return $this->hasMany(\App\Store::class);
    }

    /**
     *  Subscriber Specials.
     */
    public function subscriber_specials()
    {
        return $this->hasMany(\App\Ad::class)->where('websites', false)->current();
    }
}
