<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shrink extends Model
{
    protected $fillable = [
        'shrink_item_id',
        'value',
        'date',
        'store_id',
    ];

    public function item()
    {
        return $this->belongsTo(\App\ShrinkItem::class, 'shrink_item_id', 'id');
    }

    public function store()
    {
        return $this->belongsTo(\App\Store::class);
    }
}
