<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

class Article extends Model
{
    protected $fillable = [
        'title',
        'intro_text',
        'content',
        'cover_image',
        'status',
    ];

    public function getStatusAttribute($value)
    {
        $status = 'Unpublished';

        if ($value == 1) {
            $status = 'Published';
        }

        return $status;
    }

    public function getCoverImgAttribute($value)
    {
        if ($value == '') {
            return URL::to('/img/pending-image.jpg');
        } else {
            return Storage::url('articles/'.$value);
        }
    }

    public function scopeLatest($query)
    {
        return $query
            ->orderBy('created_at', 'desc')
            ->take(2)
            ->get();
    }

    /**
     *   Categories Relationship (Pivot).
     */
    public function categories()
    {
        return $this->belongsToMany(ArticleCategory::class, 'article_article_category');
    }
}
