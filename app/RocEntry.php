<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class RocEntry extends Model
{
    protected $fillable = [
        'name',
        'content',
        'status',
        'stores',
    ];

    /**
     *  Store Status.
     */
    public function store_status($id)
    {
        if ($this->completions()->where('store_id', $id)->count() > 0) {
            return Lang::get('roc-entry.completed');
        }

        return Lang::get('roc-entry.pending');
    }

    /**
     *  Store Status.
     */
    public function store_status_label($id)
    {
        if ($this->completions()->where('store_id', $id)->count() > 0) {
            return 'label-success';
        }

        return 'label-danger';
    }

    /**
     *  RELATIONSHIPS.
     */

    /**
     *  Attachments Relationship.
     */
    public function attachments()
    {
        return $this->hasMany(RocAttachment::class);
    }

    /**
     *  Completions Relationship.
     */
    public function completions()
    {
        return $this->hasMany(RocEntryCompletion::class);
    }

    /**
     *  Images Relationship.
     */
    public function images()
    {
        return $this->hasMany(RocImage::class);
    }

    /**
     *  GETTERS.
     */

    /**
     *  Stores List Mutator.
     */
    public function getStoresListAttribute()
    {
        return Store::whereIn('id', explode(',', $this->attributes['stores']))->get();
    }

    /**
     *  Status Mutator.
     */
    public function getStatusAttribute()
    {
        if ($this->completions()->where('store_id', auth()->user()->store_data->id)->count() > 0) {
            return Lang::get('roc-entry.completed');
        }

        return Lang::get('roc-entry.pending');
    }

    /**
     *  SCOPES.
     */

    /**
     *  Committed.
     */
    public function scopeCommitted($query)
    {
        return $query->where('roc_id', '!=', null);
    }
}
