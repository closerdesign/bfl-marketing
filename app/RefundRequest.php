<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RefundRequest extends Model
{
    protected $fillable = [
        'name',
        'email',
        'phone',
        'store',
        'area',
        'amount',
        'comments',
        'purchase_date',
    ];
}
