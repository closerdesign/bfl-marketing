<?php

namespace App\Exports;

use App\Promo;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class PromoExport implements FromView
{
    public $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function view(): View
    {
        return view('promos.excel', [
            'promo' => Promo::find($this->id),
        ]);
    }
}
