<?php

namespace App\Exports;

use App\InventoryItem;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class InventoryExport implements FromView
{
    public $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function view(): View
    {
        return view('inventory.xls-report', [
            'items' => InventoryItem::where('inventory_id', $this->id)->get(),
        ]);
    }
}
