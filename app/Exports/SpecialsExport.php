<?php

namespace App\Exports;

use App\Special;
use Maatwebsite\Excel\Concerns\FromCollection;

class SpecialsExport implements FromCollection
{
    public $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return Special::where('ad_id', $this->id)->get();
    }
}
