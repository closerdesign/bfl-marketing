<?php

namespace App\Exports;

use App\ShoppingProduct;
use Maatwebsite\Excel\Concerns\FromCollection;

class ShoppingProductsExport implements FromCollection
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return ShoppingProduct::doesntHave('product')->groupBy('sku')->get();
    }
}
