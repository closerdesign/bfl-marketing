<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProduceAd extends Model
{
    protected $fillable = [
        'ad_id',
        'brand',
        'product',
        'size',
        'upc',
        'price',
        'position',
        'notes',
    ];
}
