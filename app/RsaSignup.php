<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RsaSignup extends Model
{
    protected $fillable = [
        'first_name',
        'last_name',
        'sign_up_date',
        'store_code',
        'store_name',
        'user_name',
    ];
}
