<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dlr extends Model
{
    protected $fillable = [

        'err_code',
        'message_timestamp',
        'message_id',
        'msisdn',
        'network_code',
        'price',
        'scts',
        'status',
        'to',

    ];
}
