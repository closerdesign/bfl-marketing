<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItAsset extends Model
{
    protected $fillable = [
        'employee_id',
        'name',
        'type',
        'model',
        'serial',
        'accesories',
        'comments',
        'username',
        'password',
        'price',
        'status',
    ];
}
