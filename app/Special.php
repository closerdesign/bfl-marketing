<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

class Special extends Model
{
    protected $fillable = [

        'item_description',
        'alt_description',
        'size',
        'sku',
        'status',
        'cost',
        'price',
        'srp',
        'position',
        'department_id',
        'item_info_location',
        'image',
        'comments',
        'public_comment',
        'featured',
        'ad_id',
        'additional_upcs',
        'order_no',
        'limit',
        'yield',
        'adj_cost',
        'ad_gross',
        'supplier',
        'plu_required',
        'plu',
        'promo_number',
        'position_number',
        'ad_stroke',
        'alt_price_format',
        'mobile_app_deal',

    ];

    public function department()
    {
        return $this->belongsToMany(\App\Department::class);
    }

    public function ad()
    {
        return $this->belongsTo(\App\Ad::class);
    }

    public function upcs()
    {
        return $this->hasMany(\App\SpecialUpc::class, 'special_id', 'id');
    }

    public function getStatusAttribute($value)
    {
        $status = 'Pending';

        if ($value == 1) {
            $status = 'Approved';
        }

        return $status;
    }

    public function getImageAttribute($value)
    {
        if ($value != '') {
            return Storage::url('specials/'.$value);
        } else {
            return URL::to('/img/pending-image.jpg');
        }
    }

    public function scopeFeatured($query, $ad)
    {
        return $query
            ->where('status', 1)
            ->where('featured', 1)
            ->where('ad_id', $ad)
            ->get();
    }

    public function scopeProducts($query, $ad)
    {
        return $query
            ->where('ad_id', $ad)
            ->orderBy('item_description')
            ->get();
    }

    public function getPriceTagAttribute()
    {
        $value = (string) $this->price;

        $value = str_replace('¢', '<sup>¢</sup>', $value);

        $value = str_replace('$', '<sup>$</sup>', $value);

        if (count(explode('.', $value)) == 2) {
            $value = explode('.', $value)[0].'<sup>'.explode('.', $value)[1].'</sup>';
        }

        return $value;
    }
}
