<?php

namespace App\Imports;

use App\Special;
use Maatwebsite\Excel\Concerns\ToModel;

class SpecialsImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new Special([
            'item_description' => $row[0],
            'status' => 1,
            'price' => $row[1],
            'srp' => $row[2],
            'department_id' => 0,
            'ad_id' => $row[3],
            'size' => $row[4],
            'position' => null,
            'sku' => $row[5],
            'featured' => 0,
        ]);
    }
}

// item_description,price,srp,ad_id,size,sku
