<?php

namespace App\Imports;

use App\ItemMovement;
use Maatwebsite\Excel\Concerns\ToModel;

class ItemMovementImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        $week_ending_date = date('Y-m-d', strtotime($row[9]));

        if (intval($row[0] > 0)) {
            return new ItemMovement([
                'store' => $row[0],
                'upc' => $row[1],
                'description' => $row[2],
                'dept' => $row[3],
                'category' => $row[4],
                'size' => $row[5],
                'qty_sold' => $row[6],
                'wght_sold' => $row[7],
                'amt_sold' => $row[8],
                'week_ending_date' => $week_ending_date,
            ]);
        }
    }
}
