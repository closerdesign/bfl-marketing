<?php

namespace App\Imports;

use App\Ticket;
use Maatwebsite\Excel\Concerns\ToModel;

class TicketImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        $date = ($row[2] - 25569) * 86400;

        return new Ticket([
            'location' => $row[0],
            'event' => $row[1],
            'event_date' => date('F d, Y', $date),
            'sale_date' => $row[3],
        ]);
    }
}
