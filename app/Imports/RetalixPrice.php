<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;

class RetalixPrice implements ToModel
{
    /**
     * @param Model $model
     */
    public function model(array $price)
    {
        return new \App\RetalixPrice([
            'upc_ean' => $price[0],
            'product_description' => $price[1],
            'prm_store_number' => $price[2],
            'ip_unit_price' => $price[3],
            'descriptive_size' => $price[4],
            'ip_start_date' => $price[5],
            'ip_end_date' => $price[7],
            'price_strategy' => $price[8],
        ]);
    }
}
