<?php

namespace App\Imports;

use App\AwgPrice;
use Maatwebsite\Excel\Concerns\ToModel;

class AwgPriceImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        $unit_city = $row[9];

        if ($row[8] > 0) {
            $unit_city = $row[9] / $row[8];
        }

        $unit_rural = $row[12];

        if ($row[11] > 0) {
            $unit_rural = $row[12] / $row[11];
        }

        return new AwgPrice([
            'obi' => $row[0],
            'item' => $row[1],
            'description' => $row[2],
            'size' => $row[3],
            'pack' => $row[4],
            'excise_tax' => $row[5],
            'case_cost' => $row[6],
            'unit_cost' => $row[7],
            'unit_city' => $unit_city,
            'unit_rural' => $unit_rural,
            'bfl_vendor_id' => $row[16],
            'bfl_dept' => $row[17],
            'upc' => $row[18],
        ]);
    }
}
