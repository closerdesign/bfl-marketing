<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Training extends Model
{
    protected $fillable = [

        'name',
        'category',

    ];

    /**
     *   Slides.
     */
    public function slides()
    {
        return $this->hasMany(\App\TrainingSlide::class)->orderBy('position');
    }
}
