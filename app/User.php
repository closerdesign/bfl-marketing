<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'admin', 'phone', 'status', 'store',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin()
    {
        return $this->admin; // this looks for an admin column in your users table
    }

    /**
     *  Displays User Approval.
     */
    public function isApproved()
    {
        return $this->status;
    }

    public function isStore()
    {
        $store = false;

        if ($this->store != '') {
            $store = true;
        }

        return $store;
    }

    public function getAdminAttribute($value)
    {
        $role = 'Team';

        if ($value == 1) {
            $role = 'Admin';
        }

        return $role;
    }

    public function getStatusAttribute($value)
    {
        $status = 'Pending Approval';

        if ($value == 1) {
            $status = 'Approved';
        }

        return $status;
    }

    public function setStoreAttribute($value)
    {
        $this->attributes['store'] = (string) $value ?: null;
    }

    /**
     *  Store Relationship.
     */
    public function store_data()
    {
        return $this->belongsTo(Store::class, 'store', 'store_code');
    }
}
