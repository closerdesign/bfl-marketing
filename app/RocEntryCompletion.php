<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RocEntryCompletion extends Model
{
    protected $fillable = [
        'roc_entry_id',
        'store_id',
        'completed_by',
    ];
}
