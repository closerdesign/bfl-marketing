<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampaignFaq extends Model
{
    protected $fillable = [

        'question',
        'answer',
        'campaign_id',

    ];

    /**
     *  Campaign Relationship.
     */
    public function campaign()
    {
        return $this->belongsTo(\App\Campaign::class);
    }
}
