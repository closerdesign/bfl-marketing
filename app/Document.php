<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Document extends Model
{
    protected $fillable = [
        'name',
        'description',
        'file',
        'status',
    ];

    public function getFileAttribute($value)
    {
        return Storage::url('documents/'.$value);
    }

    public function getStatusAttribute($value)
    {
        $status = 'Unpublished';

        if ($value == 1) {
            $status = 'Published';
        }

        return $status;
    }
}
