<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryItem extends Model
{
    protected $fillable = ['upc', 'description', 'qty', 'retail_price', 'inventory_id'];
}
