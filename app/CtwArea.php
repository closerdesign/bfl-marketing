<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CtwArea extends Model
{
    protected $fillable = [
        'name',
    ];

    /**
     *  Areas Relationship.
     */
    public function areas()
    {
        return $this->hasMany(Brand::class);
    }

    /**
     *  Brands Relationship.
     */
    public function brands()
    {
        return $this->belongsToMany(Brand::class);
    }

    /**
     *  Topics Relationship.
     */
    public function topics()
    {
        return $this->hasMany(CtwTopic::class)
            ->orderBy('topic');
    }
}
