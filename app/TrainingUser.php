<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrainingUser extends Model
{
    protected $fillable = [

        'training_id',
        'name',
        'email',
        'store',
        'department',

    ];
}
