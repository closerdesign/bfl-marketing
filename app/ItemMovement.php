<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemMovement extends Model
{
    protected $fillable = [
        'store',
        'upc',
        'description',
        'dept',
        'category',
        'size',
        'qty_sold',
        'wght_sold',
        'amt_sold',
        'week_ending_date',
    ];
}
