<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HourlySale extends Model
{
    protected $fillable = [
        'store_id',
        'dt',
        'ttime',
        'str_hier_id',
        'net_sls_amt',
        'net_sls_qty',
        'cust_qty',
    ];
}
