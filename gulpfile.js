var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

    mix.sass('app.scss');

    mix.copy('resources/assets/bower/bootstrap/dist/css/bootstrap.min.css', 'resources/assets/css/bootstrap.min.css');
    mix.copy('resources/assets/bower/jquery-ui/themes/base/jquery-ui.css', 'resources/assets/css/jquery-ui.css');
    mix.copy('resources/assets/bower/jquery-ui/themes/base/datepicker.css', 'resources/assets/css/datepicker.css');
    mix.copy('resources/assets/bower/summernote/dist/summernote.css', 'resources/assets/css/summernote.css');
    mix.copy('resources/assets/bower/select2/dist/css/select2.min.css', 'resources/assets/css/select2.min.css');
    mix.copy('resources/assets/bower/datatables/media/css/dataTables.bootstrap.min.css', 'resources/assets/css/dataTables.bootstrap.min.css');
    mix.copy('resources/assets/bower/select2-bootstrap-css/select2-bootstrap.min.css', 'resources/assets/css/select2-bootstrap.min.css');
    mix.copy('resources/assets/bower/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css', 'resources/assets/css/bootstrap-colorpicker.min.css');
    mix.copy('resources/assets/bower/pickadate/lib/themes/default.css', 'resources/assets/css/default.css');
    mix.copy('resources/assets/bower/pickadate/lib/themes/default.date.css', 'resources/assets/css/default.date.css');
    mix.copy('resources/assets/bower/pickadate/lib/themes/default.time.css', 'resources/assets/css/default.time.css');

    mix.styles([
        'bootstrap.min.css',
        'jquery-ui.css',
        'datepicker.css',
        'summernote.css',
        'select2.min.css',
        'dataTables.bootstrap.min.css',
        'select2-bootstrap.min.css',
        'bootstrap-colorpicker.min.css',
        'default.css',
        'default.date.css',
        'default.time.css'
    ]);

    mix.copy('resources/assets/bower/bootstrap/fonts/glyphicons-halflings-regular.eot', 'public/fonts/glyphicons-halflings-regular.eot');
    mix.copy('resources/assets/bower/bootstrap/fonts/glyphicons-halflings-regular.svg', 'public/fonts/glyphicons-halflings-regular.svg');
    mix.copy('resources/assets/bower/bootstrap/fonts/glyphicons-halflings-regular.ttf', 'public/fonts/glyphicons-halflings-regular.ttf');
    mix.copy('resources/assets/bower/bootstrap/fonts/glyphicons-halflings-regular.woff', 'public/fonts/glyphicons-halflings-regular.woff');
    mix.copy('resources/assets/bower/bootstrap/fonts/glyphicons-halflings-regular.woff2', 'public/fonts/glyphicons-halflings-regular.woff2');
    mix.copy('resources/assets/bower/summernote/dist/font/summernote.eot', 'public/css/font/summernote.eot');
    mix.copy('resources/assets/bower/summernote/dist/font/summernote.ttf', 'public/css/font/summernote.ttf');
    mix.copy('resources/assets/bower/summernote/dist/font/summernote.woff', 'public/css/font/summernote.woff');

    mix.copy('resources/assets/bower/jquery/dist/jquery.min.js', 'resources/assets/js/jquery.min.js');
    mix.copy('resources/assets/bower/bootstrap/dist/js/bootstrap.min.js', 'resources/assets/js/bootstrap.min.js');
    mix.copy('resources/assets/bower/jquery-ui/jquery-ui.min.js', 'resources/assets/js/jquery-ui.min.js');
    mix.copy('resources/assets/bower/summernote/dist/summernote.min.js', 'resources/assets/js/summernote.min.js');
    mix.copy('resources/assets/bower/select2/dist/js/select2.min.js', 'resources/assets/js/select2.min.js');
    mix.copy('resources/assets/bower/datatables/media/js/jquery.dataTables.min.js', 'resources/assets/js/jquery.dataTables.min.js');
    mix.copy('resources/assets/bower/datatables/media/js/dataTables.bootstrap.min.js', 'resources/assets/js/dataTables.bootstrap.min.js');
    mix.copy('resources/assets/bower/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js', 'resources/assets/js/bootstrap-colorpicker.min.js');
    mix.copy('resources/assets/bower/pickadate/lib/picker.js', 'resources/assets/js/picker.js');
    mix.copy('resources/assets/bower/pickadate/lib/picker.date.js', 'resources/assets/js/picker.date.js');
    mix.copy('resources/assets/bower/pickadate/lib/picker.time.js', 'resources/assets/js/picker.time.js');
    mix.copy('resources/assets/bower/typeahead.js/dist/typeahead.bundle.min.js', 'resources/assets/js/typeahead.bundle.min.js');
    mix.copy('resources/assets/bower/typeahead.js/dist/bloodhound.min.js', 'resources/assets/js/bloodhound.min.js');
    mix.copy('resources/assets/bower/jquery-validation/dist/jquery.validate.min.js', 'resources/assets/js/jquery.validate.min.js');
    mix.copy('resources/assets/bower/numeral/min/numeral.min.js', 'resources/assets/js/numeral.min.js');
    mix.copy('resources/assets/bower/js-cookie/src/js.cookie.js', 'resources/assets/js/js.cookie.js');

    mix.scripts([
        'jquery.min.js',
        'bootstrap.min.js',
        'jquery-ui.min.js',
        'summernote.min.js',
        'select2.min.js',
        'jquery.dataTables.min.js',
        'dataTables.bootstrap.min.js',
        'bootstrap-colorpicker.min.js',
        'picker.js',
        'picker.date.js',
        'picker.time.js',
        'typeahead.bundle.min.js',
        'bloodhound.min.js',
        'jquery.validate.min.js',
        'numeral.min.js',
        'js.cookie.js',
        'scripts.js'
    ]);

});
