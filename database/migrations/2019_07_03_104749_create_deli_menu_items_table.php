<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliMenuItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deli_menu_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('deli_menu_category_id')->unsigned();
            $table->string('item_name');
            $table->string('item_description');
            $table->string('item_price');
            $table->integer('item_rank');
            $table->timestamps();
        });

        Schema::table('deli_menu_items', function ($table) {
            $table->foreign('deli_menu_category_id')->references('id')->on('deli_menus');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deli_menu_items');
    }
}
