<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CampaignCategoryCampaignItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_category_campaign_item', function (Blueprint $table) {
            $table->integer('campaign_category_id')->unsigned();
            $table->integer('campaign_item_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('campaign_category_campaign_item');
    }
}
