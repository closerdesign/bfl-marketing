<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTextSubscribersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('text_subscribers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phone_number');
            $table->integer('brand_id')->unsigned();
//            $table->foreign('brand_id')->references('id')->on('brands')->onDelete('cascade');
            $table->boolean('optin')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('text_subscribers');
    }
}
