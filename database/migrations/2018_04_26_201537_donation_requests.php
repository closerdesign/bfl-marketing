<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class DonationRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donation_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->text('brand');
            $table->text('organization');
            $table->text('website');
            $table->text('org_phone');
            $table->text('address');
            $table->boolean('nonprofit');
            $table->text('nonprofit_number');
            $table->boolean('prev_donation');
            $table->text('prev_details');
            $table->boolean('bfl_employees');
            $table->text('bfl_employee_list');
            $table->text('org_mission');
            $table->text('event');
            $table->text('event_website');
            $table->date('event_date');
            $table->text('event_time');
            $table->date('pickup_date');
            $table->text('event_location');
            $table->text('beneficiaries');
            $table->text('request_type');
            $table->text('items');
            $table->text('additional_info');
            $table->text('attach_file');
            $table->boolean('advertising');
            $table->text('advertising_details');
            $table->text('logos');
            $table->text('logo_email');
            $table->text('first_name');
            $table->text('last_name');
            $table->text('email');
            $table->text('phone');
            $table->text('location');
            $table->text('referral_source');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('donation_requests');
    }
}
