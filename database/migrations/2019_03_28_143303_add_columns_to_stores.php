<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToStores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stores', function ($table) {
            $table->string('pharmacy_hours')->nullable();
            $table->string('catering_events_hours')->nullable();
            $table->string('deli_hours')->nullable();
            $table->string('bakery_hours')->nullable();
            $table->string('produce_hours')->nullable();
            $table->string('floral_hours')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
