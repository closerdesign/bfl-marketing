<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCashiersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cashiers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('store_number');
            $table->integer('cashier_number');
            $table->string('name');
            $table->dateTime('start');
            $table->dateTime('birthday')->nullable();
            $table->string('remarks')->nullable();
            $table->string('type');
            $table->integer('dont_sign_on_sale_fg');
            $table->integer('dont_sign_on_trn_fg');
            $table->dateTime('record_status_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cashiers');
    }
}
