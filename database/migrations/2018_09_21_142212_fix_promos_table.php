<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixPromosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('promos', function ($table) {
            $table->boolean('while_supplies_last')->default(false)->change();
            $table->longText('additional_upcs')->nullable()->change();
            $table->longText('comments')->nullable()->change();
            $table->longText('comments')->nullable()->change();
            $table->boolean('it_processed')->default(false)->change();
            $table->boolean('websites')->default(false)->change();
            $table->longText('comments')->nullable()->change();
            $table->string('promo_number')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('promos', function ($table) {
            $table->integer('while_supplies_last')->change();
            $table->longText('additional_upcs')->change();
            $table->longText('comments')->change();
            $table->longText('comments')->change();
            $table->integer('it_processed')->change();
            $table->integer('websites')->change();
            $table->longText('comments')->change();
            $table->string('promo_number')->change();
        });
    }
}
