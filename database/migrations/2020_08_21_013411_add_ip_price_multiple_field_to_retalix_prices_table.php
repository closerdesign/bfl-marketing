<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIpPriceMultipleFieldToRetalixPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('retalix_prices', function (Blueprint $table) {
            $table->integer('ip_price_multiple')->default(1)->after('ip_unit_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('retalix_prices', function (Blueprint $table) {
            $table->dropColumn('ip_price_multiple');
        });
    }
}
