<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromoMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promo_movements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('mmbr_prom_id');
            $table->date('strt_date');
            $table->date('end_date');
            $table->string('prom_desc');
            $table->decimal('prom_amt', 8, 2);
            $table->integer('prom_qty');
            $table->date('dt');
            $table->integer('store');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promo_movements');
    }
}
