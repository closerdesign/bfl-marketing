<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddingDeptCodeToRetalixPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('retalix_prices', function (Blueprint $table) {
            $table->string('dept_code')->nullable()->after('price_strategy');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('retalix_prices', function (Blueprint $table) {
            $table->dropColumn('dept_code');
        });
    }
}
