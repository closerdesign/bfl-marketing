<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddIdeasRequestsCommentsToHuddles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('huddles', function ($table) {
            $table->longText('ideas');
            $table->longText('requests');
            $table->longText('comments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('huddles', function ($table) {
            $table->dropColumn('ideas');
            $table->dropColumn('requests');
            $table->dropColumn('comments');
        });
    }
}
