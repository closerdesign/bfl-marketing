<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSignageShopItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('signage_shop_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('signage_shop_id')->unsigned();
            $table->string('name');
            $table->string('description');
            $table->string('file');
            $table->string('cover_image');
            $table->boolean('request')->default(0);
            $table->boolean('download')->default(0);
            $table->boolean('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('signage_shop_items');
    }
}
