<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAwgPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('awg_prices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('obi');
            $table->string('item');
            $table->string('description');
            $table->string('size');
            $table->string('pack');
            $table->string('excise_tax')->nullable();
            $table->decimal('case_cost', 8, 2);
            $table->decimal('unit_cost', 8, 4);
            $table->decimal('unit_city', 8, 2);
            $table->decimal('unit_rural', 8, 2);
            $table->integer('bfl_vendor_id');
            $table->integer('bfl_dept');
            $table->string('upc');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('awg_prices');
    }
}
