<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPaymentProcessorFieldsToStores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stores', function (Blueprint $table) {
            $table->string('stripe_key');
            $table->string('stripe_secret');
            $table->string('paypal_username');
            $table->string('paypal_password');
            $table->string('paypal_secret');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stores', function (Blueprint $table) {
            $table->dropColumn('stripe_key');
            $table->dropColumn('stripe_secret');
            $table->dropColumn('paypal_username');
            $table->dropColumn('paypal_password');
            $table->dropColumn('paypal_secret');
        });
    }
}
