<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddArticleArticleCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('articles_categories_reference');

        Schema::create('article_article_category', function ($table) {
            $table->integer('article_id')->unsigned();
            $table->integer('article_category_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('article_article_category');

        Schema::create('articles_categories_reference', function ($table) {
            $table->integer('articles_category_id')->refrences('id')->on('articles_categories');
            $table->integer('article_id')->refrences('id')->on('articles');
        });
    }
}
