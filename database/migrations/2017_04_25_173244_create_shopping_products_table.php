<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateShoppingProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopping_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('store_id');
            $table->string('sku');
            $table->string('brand_name');
            $table->string('product_name');
            $table->string('description');
            $table->string('size');
            $table->string('uom');
            $table->string('dept_code');
            $table->string('scale_flag');
            $table->string('how_to_sell');
            $table->string('avg_weight');
            $table->string('wgt_selector');
            $table->string('store_number');
            $table->string('ad_flag');
            $table->string('cool');
            $table->string('regular_price');
            $table->string('sale_price');
            $table->string('sale_qty');
            $table->string('promo_start_date');
            $table->string('promo_end_date');
            $table->string('promo_buy_qty');
            $table->string('promo_get_qty');
            $table->string('promo_min_qty');
            $table->string('promo_limit_qty');
            $table->string('family_sale_flag');
            $table->string('family_sale_code');
            $table->string('family_sale_desc');
            $table->string('tax_pct');
            $table->string('bottle_deposit');
            $table->string('promo_tag');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shopping_products');
    }
}
