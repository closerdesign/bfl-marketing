<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFieldsToStores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stores', function ($table) {
            $table->string('address');
            $table->string('address2');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('phone');
            $table->string('store_manager');
            $table->string('store_manager_image');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stores', function ($table) {
            $table->dropColumn('address');
            $table->dropColumn('address2');
            $table->dropColumn('latitude');
            $table->dropColumn('longitude');
            $table->dropColumn('phone');
            $table->dropColumn('store_manager');
            $table->dropColumn('store_manager_image');
        });
    }
}
