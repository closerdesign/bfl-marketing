<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateShoppingItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopping_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('upc')->unique();
            $table->string('brand_name');
            $table->string('product_name');
            $table->longText('description');
            $table->string('size');
            $table->string('uom');
            $table->integer('department_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->string('price');
            $table->integer('price_strategy')->unsigned();
            $table->integer('price_type')->unsigned();
            $table->date('start_date');
            $table->date('end_date');
            $table->string('tax');
            $table->string('image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shopping_items');
    }
}
