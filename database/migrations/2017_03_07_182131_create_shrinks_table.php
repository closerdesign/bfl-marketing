<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateShrinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shrinks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shrink_item_id')->unsigned()->index();
            $table->foreign('shrink_item_id')->references('id')->on('shrink_items')->onDelete('cascade');
            $table->string('value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shrinks');
    }
}
