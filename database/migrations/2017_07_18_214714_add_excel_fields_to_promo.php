<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddExcelFieldsToPromo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('promos', function ($table) {
            $table->integer('department_id')->unsigned();
            $table->integer('limit_per_transaction');
            $table->longText('additional_upcs');
            $table->longText('comments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('promos', function ($table) {
            $table->dropColumn('department_id');
            $table->dropColumn('limit_per_transaction');
            $table->dropColumn('additional_upcs');
            $table->dropColumn('comments');
        });
    }
}
