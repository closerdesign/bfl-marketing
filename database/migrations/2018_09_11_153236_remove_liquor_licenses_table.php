<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveLiquorLicensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('liquor_licenses');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('liquor_licenses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cashier_number')->unsigned();
            $table->date('due_date');
            $table->string('image');
            $table->timestamps();
        });
    }
}
