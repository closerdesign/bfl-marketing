<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCtwTopicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ctw_topics', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('topic');
            $table->integer('ctw_area_id')->unsigned();
//            $table->foreign('ctw_area_id')
//                ->references('id')
//                ->on('ctw_areas')
//                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ctw_topics');
    }
}
