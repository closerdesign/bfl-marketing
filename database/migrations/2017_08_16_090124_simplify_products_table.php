<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class SimplifyProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function ($table) {
            $table->dropColumn('department');
            $table->dropColumn('vendor');
            $table->dropColumn('pack');
            $table->dropColumn('size_description');
            $table->string('image');
            $table->longText('description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function ($table) {
            $table->integer('department');
            $table->integer('vendor');
            $table->string('pack');
            $table->string('size_description');
            $table->dropColumn('image');
            $table->dropColumn('description');
        });
    }
}
