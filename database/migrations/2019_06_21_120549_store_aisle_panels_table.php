<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class StoreAislePanelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_aisle_panels', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('aisle_id')->unsigned();
            $table->string('panel_name');
            $table->timestamps();
        });

        Schema::table('store_aisle_panels', function ($table) {
            $table->foreign('aisle_id')->references('id')->on('store_aisles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
