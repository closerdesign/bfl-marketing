<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHourlySalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hourly_sales', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('store_id');
            $table->date('dt');
            $table->time('ttime');
            $table->string('str_hier_id');
            $table->decimal('net_sls_amt', 8, 2);
            $table->integer('net_sls_qty');
            $table->integer('cust_qty');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hourly_sales');
    }
}
