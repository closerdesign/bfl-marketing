<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_movements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('store')->nullable();
            $table->integer('upc')->nullable();
            $table->string('description')->nullable();
            $table->integer('dept')->nullable();
            $table->integer('category')->nullable();
            $table->string('size')->nullable();
            $table->decimal('qty_sold', 8, 2)->nullable();
            $table->decimal('wght_sold', 8, 2)->nullable();
            $table->decimal('amt_sold', 8, 2)->nullable();
            $table->date('week_ending_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_movements');
    }
}
