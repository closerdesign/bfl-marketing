<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTerminationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('terminations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('preferred_name');
            $table->string('last_name');
            $table->integer('store');
            $table->string('department');
            $table->date('termination_date');
            $table->date('last_day');
            $table->string('termination_type');
            $table->string('reason');
            $table->longText('other_details');
            $table->string('rehire');
            $table->string('notice_given');
            $table->string('badge_collected');
            $table->longText('replacement');
            $table->longText('notes');
            $table->string('attachment');
            $table->string('completed_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('terminations');
    }
}
