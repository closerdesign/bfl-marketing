<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBabFeedbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bab_feedback', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('role');
            $table->string('store');
            $table->string('email');
            $table->string('executive_email');
            $table->string('location');
            $table->date('meeting_date');
            $table->string('time_start');
            $table->string('time_end');
            $table->string('leader');
            $table->text('present');
            $table->text('absent');
            $table->text('praise_report');
            $table->text('wins');
            $table->text('brand_demo');
            $table->text('how_poa');
            $table->text('guest_one');
            $table->text('guest_two');
            $table->text('support_needs');
            $table->text('learning_sc');
            $table->text('focus_old');
            $table->text('focus_new');
            $table->text('receipt_ideas');
            $table->text('addtl_info');
            $table->string('attachment');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bab_feedback');
    }
}
