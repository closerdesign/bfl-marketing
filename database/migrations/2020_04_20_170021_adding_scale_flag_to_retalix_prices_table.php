<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddingScaleFlagToRetalixPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('retalix_prices', function (Blueprint $table) {
            $table->boolean('scale_flag')->default(1)->after('dept_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('retalix_prices', function (Blueprint $table) {
            $table->dropColumn('scale_flag');
        });
    }
}
