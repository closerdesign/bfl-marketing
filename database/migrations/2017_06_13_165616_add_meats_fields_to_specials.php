<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddMeatsFieldsToSpecials extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('specials', function ($table) {
            $table->string('order_no');
            $table->string('limit');
            $table->string('yield');
            $table->string('adj_cost');
            $table->string('ad_gross');
            $table->string('supplier');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('specials', function ($table) {
            $table->dropColumn('order_no');
            $table->dropColumn('limit');
            $table->dropColumn('yield');
            $table->dropColumn('adj_cost');
            $table->dropColumn('ad_gross');
            $table->dropColumn('supplier');
        });
    }
}
