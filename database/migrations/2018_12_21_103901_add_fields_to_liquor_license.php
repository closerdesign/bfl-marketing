<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToLiquorLicense extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('liquor_licenses', function (Blueprint $table) {
            $table->string('file_description');
            $table->mediumText('notes')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('liquor_licenses', function (Blueprint $table) {
            $table->dropColumn('file_description');
            $table->dropColumn('notes');
        });
    }
}
