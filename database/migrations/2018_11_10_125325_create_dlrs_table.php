<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDlrsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dlrs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('err_code')->nullable();
            $table->string('message_timestamp')->nullable();
            $table->string('message_id')->nullable();
            $table->string('msisdn')->nullable();
            $table->string('network_code')->nullable();
            $table->string('price')->nullable();
            $table->string('scts')->nullable();
            $table->string('status')->nullable();
            $table->string('to')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dlrs');
    }
}
