<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAdGroupFieldToRetalixPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('retalix_prices', function (Blueprint $table) {
            $table->integer('ad_group')->nullable()->after('price_strategy');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('retalix_prices', function (Blueprint $table) {
            $table->dropColumn('ad_group');
        });
    }
}
