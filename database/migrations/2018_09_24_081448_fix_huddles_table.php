<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixHuddlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('huddles', function ($table) {
            $table->longText('ideas')->nullable()->change();
            $table->longText('requests')->nullable()->change();
            $table->longText('comments')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('huddles', function ($table) {
            $table->longText('ideas')->nullable();
            $table->longText('requests')->nullable();
            $table->longText('comments')->nullable();
        });
    }
}
