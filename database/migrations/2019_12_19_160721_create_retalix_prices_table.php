<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRetalixPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retalix_prices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('upc_ean');
            $table->string('product_description');
            $table->integer('prm_store_number');
            $table->string('ip_unit_price');
            $table->string('descriptive_size');
            $table->string('ip_start_date');
            $table->string('ip_end_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('retalix_prices');
    }
}
