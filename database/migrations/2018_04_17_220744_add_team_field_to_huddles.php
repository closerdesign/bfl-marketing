<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddTeamFieldToHuddles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('huddles', function ($table) {
            $table->dropForeign('huddles_store_id_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('huddles', function ($table) {
            $table->foreign('store_id')->references('id')->on('stores')->onDelete('cascade');
        });
    }
}
