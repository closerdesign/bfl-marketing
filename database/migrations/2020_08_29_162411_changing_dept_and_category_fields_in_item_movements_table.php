<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangingDeptAndCategoryFieldsInItemMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('item_movements', function (Blueprint $table) {
            $table->string('dept')->nullable()->change();
            $table->string('category')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_movements', function (Blueprint $table) {
            $table->integer('dept')->nullable()->change();
            $table->integer('category')->nullable()->change();
        });
    }
}
