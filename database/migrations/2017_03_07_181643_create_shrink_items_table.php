<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateShrinkItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shrink_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('shrink_group_id')->unsigned()->index();
            $table->foreign('shrink_group_id')->references('id')->on('shrink_groups')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shrink_items');
    }
}
