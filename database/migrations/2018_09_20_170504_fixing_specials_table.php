<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixingSpecialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('specials', function ($table) {
            $table->string('image')->nullable()->change();
            $table->longText('comments')->nullable()->change();
            $table->string('position')->nullable()->change();
            $table->string('cost')->nullable()->change();
            $table->string('item_info_location')->nullable()->change();
            $table->string('additional_upcs', 1024)->nullable()->change();
            $table->string('order_no')->nullable()->change();
            $table->string('limit')->nullable()->change();
            $table->string('yield')->nullable()->change();
            $table->string('adj_cost')->nullable()->change();
            $table->string('ad_gross')->nullable()->change();
            $table->string('supplier')->nullable()->change();
            $table->string('srp')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('specials', function ($table) {
            $table->string('image')->change();
            $table->longText('comments')->change();
            $table->string('position')->change();
            $table->string('cost')->change();
            $table->string('item_info_location')->change();
            $table->string('additional_upcs', 1024)->change();
            $table->string('order_no')->change();
            $table->string('limit')->change();
            $table->string('yield')->change();
            $table->string('adj_cost')->change();
            $table->string('ad_gross')->change();
            $table->string('supplier')->change();
            $table->string('srp')->change();
        });
    }
}
