<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddPickupHoursAndPriorNoticeToCampaigns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaigns', function ($table) {
            $table->time('pickup_start_time');
            $table->time('pickup_end_time');
            $table->string('prior_notice');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campaigns', function ($table) {
            $table->dropColumn('pickup_start_time');
            $table->dropColumn('pickup_end_time');
            $table->dropColumn('prior_notice');
        });
    }
}
