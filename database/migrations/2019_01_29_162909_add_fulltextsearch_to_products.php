<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFulltextsearchToProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
           DB::statement('CREATE FULLTEXT INDEX keywords_index ON products(keywords)');
           DB::statement('CREATE FULLTEXT INDEX brand_name_index ON products(brand_name)');
           DB::statement('CREATE FULLTEXT INDEX kwikee_brand_name_index ON products(kwikee_brand_name)');
           DB::statement('CREATE FULLTEXT INDEX kwikee_description_index ON products(kwikee_description)');
           DB::statement('CREATE FULLTEXT INDEX name_index ON products(name)');
           DB::statement('CREATE FULLTEXT INDEX kwikee_name_index ON products(kwikee_name)');
        */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
