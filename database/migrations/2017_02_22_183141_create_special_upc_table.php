<?php

use Illuminate\Database\Migrations\Migration;

class CreateSpecialUpcTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('special_upc');
    }
}
