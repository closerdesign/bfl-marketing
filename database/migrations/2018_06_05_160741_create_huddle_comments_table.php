<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHuddleCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('huddle_comments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('huddle_id')->unsigned();
            $table->foreign('huddle_id')->references('id')->on('huddles')->onDelete('cascade');
            $table->text('name');
            $table->text('email');
            $table->longText('comment');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('huddle_comments');
    }
}
