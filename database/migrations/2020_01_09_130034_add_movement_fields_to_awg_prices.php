<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMovementFieldsToAwgPrices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('awg_prices', function (Blueprint $table) {
            $table->decimal('qty', 8, 2)->nullable();
            $table->decimal('amount', 8, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('awg_prices', function (Blueprint $table) {
            $table->dropColumn('qty');
            $table->dropColumn('amount');
        });
    }
}
