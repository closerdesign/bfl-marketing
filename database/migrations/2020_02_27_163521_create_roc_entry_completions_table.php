<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRocEntryCompletionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roc_entry_completions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('roc_entry_id')->unsigned();
            $table->integer('store_id')->unsigned();
            $table->string('completed_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roc_entry_completions');
    }
}
