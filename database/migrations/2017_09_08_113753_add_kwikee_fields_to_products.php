<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddKwikeeFieldsToProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function ($table) {
            $table->string('kwikee_name');
            $table->longText('kwikee_description');
            $table->string('kwikee_brand_name');
            $table->string('kwikee_size');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function ($table) {
            $table->dropColumn('kwikee_name');
            $table->dropColumn('kwikee_description');
            $table->dropColumn('kwikee_brand_name');
            $table->dropColumn('kwikee_size');
        });
    }
}
