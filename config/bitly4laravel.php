<?php
/**
 * Configuraton file for bitly4laravel
 * Populate only the needed fields and comment/remove the others.
 */
return [
    'access_token' => '0f6cd8fd724085508fb0307a40a0914ec4af1164',
    'cache_enabled' => false,
    'cache_duration' => 3600, // Duration in minutes
    'cache_key_prefix' => 'Bitly4Laravel.',
    'response_format' => 'json', // json, xml
    'request_type' => 'get', // get, post
    'request_options' => [],
    'client_config' => [],
];
