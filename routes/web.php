<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('en', function () {
    \Illuminate\Support\Facades\Session::put('language', 'en');

    return back();
});
Route::get('es', function () {
    \Illuminate\Support\Facades\Session::put('language', 'es');

    return back();
});

Route::auth();

Route::get('', 'HomeController@index');
Route::get('home', 'HomeController@index');

Route::resource('ad-banners', 'AdBannersController');

Route::resource('adfiles', 'AdfilesController');

Route::resource('adpages', 'AdpagesController');

Route::get('ads/all', 'AdsController@all');
Route::get('ads/banners/{ad}', 'AdsController@banners');
Route::get('ads/subscriber-specials-email/{id}', 'AdsController@email');
Route::get('ads/email/{id}', 'AdsController@ad_email');
Route::get('ads/emailing/{id}', 'AdsController@emailing');
Route::get('ads/text/{id}', 'AdsController@ad_text');
Route::get('ads/current/{brand}', 'AdsController@current');
Route::get('ads/current_api/{brand}', 'AdsController@current_api');
Route::get('ads/duplicate/{id}', 'AdsController@duplicate');
Route::get('ads/generate-pdf/{id}', 'AdsController@generate_pdf');
Route::get('ads/export/{id}', 'AdsController@export');
Route::get('ads/export_csv/{id}', 'AdsController@export_csv');
Route::get('ads/export_csv/{id}', 'AdsController@export_csv');
Route::get('ads/mailing/{id}', 'AdsController@mailing');
Route::get('ads/pages/{brand}', 'AdsController@pages');
Route::get('ads/pdf/{id}', 'AdsController@pdf');
Route::get('ads/pdf-template/{id}', 'AdsController@pdf_template');
Route::get('ads/pdfshift/{id}', 'AdsController@pdfshift');
Route::get('ads/ad/{id}', 'AdsController@ad');
Route::get('generate-pdf/ad/{id}', 'AdsController@ad')->name('generate-pdf');
Route::post('ads/custom-report/{id}', 'AdsController@custom_report');
Route::get('ads/poster/{id}/{page}', 'AdsController@poster');
Route::get('ads/poster-pdf/{id}/{page}', 'AdsController@poster_pdf');
Route::resource('ads', 'AdsController');

Route::get('articles/{id}/delete', 'ArticlesController@delete');
Route::resource('/articles', 'ArticlesController');

Route::resource('article-category', 'ArticlesCategoriesController');

Route::resource('awg-price', 'AwgPriceController');

Route::resource('bab-feedback', 'BABFeedbackController');

Route::get('brand/stores/{id}', 'BrandsController@brand_stores');
Route::get('brand/{id}', 'BrandsController@email');
Route::resource('brands', 'BrandsController');

Route::get('campaigns/orders', 'CampaignsController@orders');
Route::get('campaigns/order-details/{id}', 'CampaignsController@order_details');
Route::get('campaigns/replicate/{id}', 'CampaignsController@replicate');
Route::resource('campaigns', 'CampaignsController');

Route::post('campaign-categories/sort', 'CampaignCategoriesController@sort');
Route::resource('campaign-categories', 'CampaignCategoriesController');

Route::resource('campaign-faqs', 'CampaignFaqsController');

Route::get('campaign-items/duplicate/{id}', 'CampaignItemsController@duplicate');
Route::resource('campaign-items', 'CampaignItemsController');

Route::resource('campaign-item-sizes', 'CampaignItemSizesController');

Route::resource('cashiers', 'CashiersController');

Route::resource('cashiersnumbers', 'CashierNumbersController');
Route::post('cashiersnumbers/{id}', 'CashierNumbersController@store');

Route::resource('categories', 'CategoriesController');

Route::resource('choices', 'ChoicesController');

Route::resource('ctw-area', 'CtwAreaController');

Route::get('ctw-evaluation/heatmap', 'CtwEvaluationController@heatmap')->name('ctw-evaluation.heatmap');
Route::resource('ctw-evaluation', 'CtwEvaluationController');
Route::post('ctw-evaluation/mark-as-completed/{id}', 'CtwEvaluationController@mark_as_completed')->name('ctw-evaluation.mark-as-completed');

Route::resource('ctw-topic', 'CtwTopicController');
Route::post('ctw-topic/store/{id}', 'CtwTopicController@store')->name('ctw-topic.store');

Route::resource('ctw-score', 'CtwScoreController');
Route::get('ctw-score/check/{evaluation}/{topic}', 'CtwScoreController@check');

Route::get('deli-ads/delete/{id}', 'DeliAdsController@destroy');
Route::get('deli-ads/up/{id}', 'DeliAdsController@up');
Route::get('deli-ads/down/{id}', 'DeliAdsController@down');
Route::resource('deli-ads', 'DeliAdsController');

Route::resource('deliMenu', 'DeliMenuController');

Route::resource('deliMenuCategory', 'DeliMenuCategoryController');
Route::get('deliMenuCategory-menu-categories/{menu_id}', 'DeliMenuCategoryController@menu_categories');

Route::resource('deliMenuItem', 'DeliMenuItemController');

Route::resource('departments', 'DepartmentsController');

Route::resource('dlrs', 'DlrsController');

Route::get('docs-list', 'DocumentsController@docs_list');
Route::resource('documents', 'DocumentsController');

Route::get('donation-request-list', 'DonationRequestsController@index');
Route::get('donation-request/all', 'DonationRequestsController@all');
Route::resource('donation-requests', 'DonationRequestsController');

Route::get('email/{brand}/{template}', 'EmailsController@campaign');

Route::get('employees/discount-request', 'EmployeesController@discount_request');
Route::post('employees/discount-request-process', 'EmployeesController@discount_request_process');
Route::resource('employees', 'EmployeesController');

Route::resource('employee-docs', 'EmployeeDocumentsController');
Route::post('employee-docs/{id}', 'EmployeeDocumentsController@store');

Route::resource('event-signups', 'EventSignupsController');

Route::get('events/{id}/delete', 'EventsController@delete');
Route::get('events/clone/{id}', 'EventsController@clone');
Route::get('events/signups/{id}', 'EventsController@view_signups');
Route::get('events/show-all', 'EventsController@show_all');
Route::resource('events', 'EventsController');

Route::get('gallery/imagedelete/{id}', 'GalleryController@imagedelete');
Route::get('gallery/imageedit/{id}', 'GalleryController@imageedit');
Route::get('gallery/show/{id}', 'GalleryController@show');
Route::get('gallery/billboard/{id}', 'GalleryController@billboard')->middleware('cors');
Route::post('gallery/update/{id}', 'GalleryController@update');
Route::post('gallery/imageupdate/{id}', 'GalleryController@imageupdate');
Route::post('gallery/categorystore', 'GalleryController@gallerystore');
Route::post('gallery/imagepost', 'GalleryController@imagepost');
Route::post('gallery/pullgallery', 'GalleryController@pullgallery');
Route::resource('gallery', 'GalleryController');

Route::get('grocery-ads/delete/{id}', 'GroceryAdsController@destroy');
Route::get('grocery-ads/up/{id}', 'GroceryAdsController@up');
Route::get('grocery-ads/down/{id}', 'GroceryAdsController@down');
Route::resource('grocery-ads', 'GroceryAdsController');

Route::get('hbc-ads/delete/{id}', 'HbcAdsController@destroy');
Route::get('hbc-ads/up/{id}', 'HbcAdsController@up');
Route::get('hbc-ads/down/{id}', 'HbcAdsController@down');
Route::resource('hbc-ads', 'HbcAdsController');

Route::get('huddles/summary', 'HuddlesController@summary');
Route::get('huddles/view/{id}', 'HuddlesController@view');
Route::get('huddles/filter/{store}', 'HuddlesController@filter');
Route::post('huddles/store/{id}', 'HuddleCommentsController@store');
Route::resource('huddles', 'HuddlesController');

Route::resource('improvement-requests', 'ImprovementRequestsController');

Route::get('inventory/export/{id}', 'InventoryController@export');
Route::get('inventory-reports', 'InventoryController@list');
Route::get('inventory/pricing-import/{id}', 'InventoryController@pricing_import');
Route::get('inventory-reports/{id}', 'InventoryController@report');
Route::resource('inventory', 'InventoryController');

Route::resource('inventory-item', 'InventoryItemController');
Route::post('inventory-item/store/{id}', 'InventoryItemController@store');

Route::group(['prefix' => 'IT'], function () {
    Route::get('sigis/upload-form', 'IT\SigisController@upload_form');
    Route::post('sigis/upload', 'IT\SigisController@upload');
});

Route::resource('it-assets', 'ItAssetsController');

Route::resource('item', 'ItemController');

Route::resource('item-movement', 'ItemMovementController');

Route::resource('item-occurrence', 'CampaignItemOccurrenceController');

Route::get('kwikee-products/image-display', 'KwikeeProductsController@image_display');
Route::get('kwikee-products/import', 'KwikeeProductsController@form');
Route::post('kwikee-products/import', 'KwikeeProductsController@import');
Route::get('kwikee-products/process/{file}', 'KwikeeProductsController@process');
Route::get('kwikee-products/task', 'KwikeeProductsController@task');

//Route::post('liquor-licenses/store', 'LiquorLicensesController@store');
//Route::get('liquor-licenses/{cashier_number}', 'LiquorLicensesController@create');

Route::get('liquor-licenses/expiring', 'LiquorLicensesController@expiring');
Route::get('liquor-licenses/report', 'LiquorLicensesController@report');
Route::resource('liquor-licenses', 'LiquorLicensesController');
Route::post('liquor-licenses/{id}', 'LiquorLicensesController@store');
//Route::get('expiring', 'LiquorLicensesController@expiring');

//Route::get('liquor-licenses/create', 'LiquorLicensesController@create');

Route::resource('loyalty', 'LoyaltyController');
Route::post('loyalty-register', 'LoyaltyController@register_user');
Route::post('loyalty-login', 'LoyaltyController@validate_user');
Route::get('loyalty-coupons/{brand_id}', 'LoyaltyController@get_coupons');
Route::post('loyalty-clips', 'LoyaltyController@get_user_clips');

Route::get('mailing-lists', 'MailingListsController@index');
Route::post('mailing-lists/store', 'MailingListsController@store');

Route::get('meat-ads/delete/{id}', 'MeatAdsController@destroy');
Route::get('meat-ads/up/{id}', 'MeatAdsController@up');
Route::get('meat-ads/down/{id}', 'MeatAdsController@down');
Route::resource('meat-ads', 'MeatAdsController');

Route::get('new-items/show/{id}', 'NewItemsController@show');
Route::resource('new-items', 'NewItemsController');

Route::get('newsletter-images/delete/{id}', 'NewsletterImageController@destroy');
Route::get('newsletters/show/{id}', 'NewsletterController@show');
Route::resource('newsletters', 'NewsletterController');
Route::post('newsletter-images/store/{id}', 'NewsletterImageController@store');

Route::get('notifications/destroy/{id}', 'NotificationsController@destroy');
Route::get('notifications/kitchen-duty', 'NotificationsController@kitchen_days');
Route::post('notifications/store-kitchen', 'NotificationsController@store_kitchen');
Route::resource('notifications', 'NotificationsController');

Route::resource('options', 'OptionsController');

Route::get('pricing-files/pickup', 'PricingFilesController@pickup');
Route::resource('pricing-files', 'PricingFilesController');

Route::get('produce-ads/delete/{id}', 'ProduceAdsController@destroy');
Route::get('produce-ads/up/{id}', 'ProduceAdsController@up');
Route::get('produce-ads/down/{id}', 'ProduceAdsController@down');
Route::resource('produce-ads', 'ProduceAdsController');

Route::get('products/import', 'ProductsController@import');
Route::get('products/optimize', 'ProductsController@optimize');
Route::get('products/categories-upload-form', 'ProductsController@categories_upload_form');
Route::get('products/optimize-kwikee-image', 'ProductsController@optimize_kwikee_image');
Route::get('products/shipt-products-report', 'ProductsController@shipt_products');
Route::get('products/shipt-categories-report', 'ProductsController@shipt_categories');
Route::post('products/file-import', 'ProductsController@file_import');
Route::post('products/ajax-categories/{id}', 'ProductsController@ajax_categories');
Route::post('products/ajax-img/{id}', 'ProductsController@ajax_img');
Route::post('products/filter', 'ProductsController@filter');
Route::post('products/categories-upload', 'ProductsController@categories_upload');
Route::get('products/clear-session', 'ProductsController@clear_session');

Route::get('products/aws_test', 'ProductsController@aws_internal');
Route::post('products/aws_test_run', 'ProductsController@aws_internal_run');

Route::resource('products', 'ProductsController');

Route::post('product-requests/web', 'ProductRequestsController@web');
Route::resource('product-requests', 'ProductRequestsController');

Route::resource('promo-movement', 'PromoMovementController');

Route::get('promos/{id}/delete', 'PromosController@delete');
Route::get('promos/{id}/excel', 'PromosController@excel');
Route::get('promos/clone/{id}', 'PromosController@cloning');
Route::get('promos/old', 'PromosController@old');
Route::get('promos/daily-summary', 'PromosController@daily_summary');
Route::get('promos/it-processed/{id}', 'PromosController@it_processed');
Route::resource('promos', 'PromosController');

Route::get('recipes/{id}/delete', 'RecipesController@delete');
Route::resource('recipes', 'RecipesController');

Route::get('rsa/coupon-product-analysis/{coupon}/{brand}', 'RsaController@coupon_product_analysis');
Route::get('rsa/redemption-report', 'RsaController@redemption_report');

Route::resource('order-reports', 'ReportController');

Route::resource('refunds', 'RefundRequestsController');
Route::resource('refund-request', 'RefundRequestsController@create');

Route::resource('rewards', 'RewardsController');

Route::resource('roc', 'RocController');

Route::post('roc-entry/mark-as-completed/{id}', 'RocEntryController@mark_as_completed')->name('roc-entry.mark-as-completed');
Route::resource('roc-entry', 'RocEntryController');

Route::resource('roc-attachment', 'RocAttachmentController');
Route::post('roc-attachment/store/{id}', 'RocAttachmentController@store');

Route::resource('roc-image', 'RocImageController');
Route::post('roc-image/store/{id}', 'RocImageController@store');

Route::resource('search', 'SearchController');
Route::get('search/product/{upc}', 'SearchController@show_product');
Route::post('batch-review', 'SearchController@batch_review');
Route::get('remove_keyword/{id}', 'SearchController@remove_keyword');
Route::get('search-review', 'SearchController@review');
Route::get('run-keyword-process', 'SearchController@run_keyword_process');

Route::get('sales', 'SalesController@index');
Route::post('sales', 'SalesController@index');
Route::get('sales-company/{days?}', 'SalesController@sales_totals');
Route::post('sales-company/{days?}', 'SalesController@sales_totals');
Route::get('sales-stores/{days?}', 'SalesController@store_totals_by_day');
Route::post('sales-stores/{days?}', 'SalesController@store_totals_by_day');
Route::get('sales-dept', 'SalesController@dept_index');
Route::get('sales-dept-summary', 'SalesController@dept_summary');
Route::get('sales-dept-summary-pie', 'SalesController@dept_pie_chart');

Route::get('shopping-items/departments/{store}', 'ShoppingItemsController@departments');
Route::get('shopping-items/list/{id}', 'ShoppingItemsController@list');
Route::resource('shopping-items', 'ShoppingItemsController');

Route::get('shopping-products/excel', 'ShoppingProductsController@excel');
Route::get('shopping-products/import', 'ShoppingProductsController@import');
Route::resource('shopping-products', 'ShoppingProductsController');

Route::resource('shrink-groups', 'ShrinkGroupsController');
Route::get('shrink-items/list/{id}', 'ShrinkItemsController@items_list');
Route::resource('shrink-items', 'ShrinkItemsController');
Route::resource('shrinks', 'ShrinksController');

Route::get('signage/bins-form', 'SignageController@bins_form');
Route::get('signage/bins-template', 'SignageController@bins_template');
Route::get('signage/bins-pdf', 'SignageController@bins_pdf');
Route::get('signage/deli-sign', 'SignageController@deli_sign');
Route::get('signage/deli-template', 'SignageController@deli_template');
Route::get('signage/deli-pdf', 'SignageController@deli_pdf');
Route::resource('signage', 'SignageController');
Route::resource('signage-shop', 'SignageShopController');
Route::get('signage-shop/campaign/{id}', 'SignageShopController@show');
Route::post('signage-shop/print-request/{id}', 'SignageShopController@print_request');
Route::get('signage-shop/destroy/{id}', 'SignageShopController@destroy');
Route::get('signage-shop/add-item/{id}', 'SignageShopController@add_item');
Route::post('signage-shop/create-item', 'SignageShopController@create_item');
Route::get('signage-shop/edit-item/{id}', 'SignageShopController@edit_item');
Route::get('signage-shop/destroy-item/{id}', 'SignageShopController@destroy_item');
Route::patch('signage-shop/update-item/{id}', 'SignageShopController@update_item');

Route::post('send-message', 'SmsController@send_sms');
Route::get('reply-sms', 'SmsController@reply_sms');
Route::get('sms-subscribe', 'SmsController@subscribe');
Route::get('sms-group-create', 'SmsController@sms_group');
Route::post('sms-group-create', 'SmsController@create_group');
Route::get('sms-test-subscribe', 'SmsController@test_subscribe');
Route::get('sms-groups', 'SmsController@groups');
Route::get('sms-group-edit/{id}', 'SmsController@group_edit');
Route::post('sms-group-update/{id}', 'SmsController@group_update');
Route::resource('sms', 'SmsController');

Route::get('specials/clone/{id}', 'SpecialsController@clone');
Route::get('specials/digital-signage-image/{id}/{type}', 'SpecialsController@digital_signage_image')->name('digital-signage-image');
Route::get('specials/import', 'SpecialsController@import_form');
Route::get('specials/search', 'SpecialsController@search');
Route::get('specials/make-promo/{id}', 'SpecialsController@make_promo');
Route::get('specials/make-special-image/{id}/{img}', 'SpecialsController@make_special_img');
Route::post('specials/cloning/{id}', 'SpecialsController@cloning');
Route::post('specials/export-to-gallery', 'SpecialsController@export_to_gallery')->name('export-to-gallery');
Route::post('specials/import', 'SpecialsController@import');
Route::post('specials/multiple-cloning', 'SpecialsController@multiple_cloning');
Route::post('specials/upcs-search', 'SpecialsController@upcs_search');
Route::resource('specials', 'SpecialsController');

Route::delete('special-upcs/{id}', 'SpecialUpcsController@destroy');
Route::resource('special-upcs', 'SpecialUpcsController');
Route::post('special-upcs/{id}', 'SpecialUpcsController@store');

Route::post('store-level-form', 'StoreLevelController@form');

Route::get('stores/{store}/delete', 'StoresController@delete');
Route::resource('stores', 'StoresController');

Route::post('store-aisles-add', 'StoreAisleController@store');
Route::post('store-aisles-add', 'StoreAisleController@store');
Route::post('store-aisles-update/{id}', 'StoreAisleController@update_aisle');

Route::post('store-aisles-delete/{$aisle_id}', 'StoreAisleController@destroy');
Route::get('store-aisles-delete/{$aisle_id}', 'StoreAisleController@destroy');

Route::resource('store-aisles', 'StoreAisleController');
Route::get('store-aisles-all/{store_id}', 'StoreAisleController@index');

//Route::resource('store-aisle-panels/', 'StoreAislePanelsController');
Route::get('store-aisle-panels-index/{id}', 'StoreAislePanelsController@index');
Route::post('store-aisle-panels-store', 'StoreAislePanelsController@store');
Route::delete('store-aisle-panels-delete/{id}', 'StoreAislePanelsController@destroy_panel');
Route::get('store-aisle-panels-show/{id}', 'StoreAislePanelsController@show');
Route::post('store-aisle-panels-update/{id}', 'StoreAislePanelsController@update');

Route::get('surveys', 'SurveysController@list');

Route::resource('teacher-votes', 'TeacherVotesController@store');

Route::get('termination/show/{id}', 'TerminationController@show');
Route::resource('termination', 'TerminationController');
Route::resource('termination-request', 'TerminationController@create');

Route::post('text-to-product', 'TextSubscribersController@inbound');

Route::resource('ticket', 'TicketController');

Route::get('training/complete', 'TrainingController@complete');
Route::resource('training', 'TrainingController');
Route::get('training/{id}', 'TrainingController@show');
Route::get('training/{id}/users', 'TrainingController@users');
Route::post('training/start/{id}', 'TrainingController@start');

Route::resource('training-slides', 'TrainingSlidesController');
Route::resource('training-slides/{id}/edit', 'TrainingSlidesController@edit');
Route::get('training-slides/{id}/preview', 'TrainingSlidesController@preview');
Route::post('training-slides/store/{id}', 'TrainingSlidesController@store');

Route::resource('transaction', 'TransactionController');

Route::get('updates/list', 'UpdatesController@list');
Route::resource('updates', 'UpdatesController');

Route::get('password', 'UsersController@password');
Route::post('password', 'UsersController@password_update');
Route::resource('user', 'UsersController');

Route::resource('volunteers', 'VolunteersController');

Route::post('weddings/status/{id}', 'WeddingsController@status');
Route::post('weddings/status-update/{id}', 'WeddingsController@status_update');
Route::post('weddings/comment/{id}', 'WeddingsController@comment');
Route::resource('weddings', 'WeddingsController');

Route::group(['prefix' => 'store-level'], function () {
    Route::get('ads', 'StoreLevelController@ads')->middleware('store');
    Route::get('home', 'StoreLevelController@home');
    Route::get('bg-check-request', 'StoreLevelController@bg_check_request');
    Route::post('bg-check-request/store', 'StoreLevelController@bg_check_store');
    Route::get('orientation-form', 'StoreLevelController@orientation_attendee');
    Route::post('orientation-form/store', 'StoreLevelController@orientation_attendee_store');
    Route::get('signage-request', 'StoreLevelController@signage_request');
    Route::get('termination-request', 'StoreLevelController@termination_request');
    Route::get('vacation-request', 'StoreLevelController@vacation_request');
    Route::post('vacation-request/store', 'StoreLevelController@vacation_request_store');
    Route::get('bg-orientation', 'StoreLevelController@bg_orientation_request');
    Route::post('bg-orientation/store', 'StoreLevelController@bg_orientation_request_form');
    Route::get('cash-report', 'StoreLevelController@cash_report');
    Route::post('cash-report/store', 'StoreLevelController@cash_report_form');
    Route::get('info-signs', 'StoreLevelController@info_signs');
    Route::get('info-sign-process', 'StoreLevelController@info_sign_process');
    Route::post('info-signs/pdf', 'StoreLevelController@info_sign_pdf');
    Route::get('kraft-promos', 'StoreLevelController@kraft_promos');
    Route::get('kraft-promo-process', 'StoreLevelController@kraft_promo_process');
    Route::post('kraft-promos/pdf', 'StoreLevelController@kraft_promo_pdf');
    Route::get('deal-of-the-week', 'StoreLevelController@deal_of_the_week');
    Route::get('deal-of-the-week-process', 'StoreLevelController@deal_of_the_week_process');
    Route::get('four-ups-process', 'StoreLevelController@four_ups_process');
    Route::post('deal-of-the-week-pdf', 'StoreLevelController@deal_of_the_week_pdf');
    Route::get('liquor-licenses', 'StoreLevelController@liquor_licenses');
    Route::get('subscriberspecials', 'StoreLevelController@specials');
    Route::get('subscriberspecial/{id}', 'StoreLevelController@special');
});
