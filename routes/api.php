<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::get('ads/{id}', 'AdsController@current_api');

Route::get('articles', 'ArticlesController@index');

Route::get('articles/{id}', 'ArticlesController@article');

Route::get('article-category/{id}', 'ArticlesCategoriesController@show');

Route::get('brand/{id}', 'BrandsController@show');

Route::get('campaigns/by-name/{name}/{store}', 'CampaignsController@campaign_by_name');

Route::get('campaign-items/classes-events/{brand}', 'CampaignItemsController@classes_events');

Route::get('departments', 'DepartmentsController@list');

Route::get('events/{id}', 'EventsController@show');

Route::get('new-items/{brand}', 'NewItemsController@display');

Route::get('newsletters/{brand}', 'NewsletterController@display');
Route::get('newsletter-images/{id}', 'NewsletterImageController@display');

Route::get('gallery/{id}', 'GalleryController@getGalleryDisplay');

// This line is depricated with Loyalty App
Route::post('loyalty-program-login', 'LoyaltyController@validate_user');
Route::get('loyalty-coupons/{brand_id}', 'LoyaltyController@get_coupons');

Route::get('promos/{id}', 'PromosController@show');

Route::get('retalix-price/price-check/{store}/{upc}', 'RetalixPriceController@price_check');

Route::get('shopping-categories-list/{store}', 'ProductsController@shopping_categories');
Route::get('shopping-product-view/{store}/{upc}', 'ProductsController@shopping_product');
Route::get('shopping-category-view/{store}/{category}', 'ProductsController@shopping_products');
Route::get('shopping-product-search/{store}/{keyword}', 'ProductsController@shopping_search');
Route::get('shopping-product-search-add/{store}/{keyword}/{id}', 'SearchController@store_search');
Route::get('shopping-product-favorites/{store}/{upcs}', 'ProductsController@shopping_favorites');

Route::get('shopping-product-search-aws/{store}/{keyword}', 'ProductsController@aws_search');

Route::get('shopping-products/departments/{store}', 'ShoppingProductsController@departments');
Route::get('shopping-products/products/{department}/{store}', 'ShoppingProductsController@products');
Route::get('shopping-products/product/{store}/{id}', 'ShoppingProductsController@product');
Route::get('shopping-products/reorder-list/{store}/{upcs}', 'ShoppingProductsController@reorder_list');

Route::get('stores/{id}', 'StoresController@show');
Route::get('store-by-code/{id}', 'StoresController@store_by_code');
Route::get('stores-with-current-ads/{ids}', 'StoresController@stores_with_current_ads');

Route::get('ticket', 'TicketController@index');
