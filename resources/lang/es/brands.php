<?php

return [

    'android-mobile-app-url' => 'URL Aplicación Móvil Android',
    'facebook-url' => 'URL de la Facebook Fan Page',
    'instagram-url' => 'URL del Perfil de Instagram',
    'ios-mobile-app-url' => 'URL Aplicación Móvil iOS',
    'loyalty-website-url' => 'Sitio Web del Programa de Fidelización',
    'online-shopping-website' => 'Website para Compras en Línea',
    'shipt-url' => 'Dirección Web para SHIPT',
    'title' => 'Marcas',

];
