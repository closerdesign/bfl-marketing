<?php

return [

    'title' => 'Inventario',
    'controller-name' => 'Encargado',
    'start' => 'Comenzar',
    'export' => 'Exportar',

    'go-back' => 'Volver',
    'finish' => 'Finalizar',

];
