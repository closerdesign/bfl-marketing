<?php

return [

    'ads-manager' => 'Administrador de Boletines',
    'are-you-sure' => '¿Estás seguro? Esta operación no podrá deshacerse.',
    'brand' => 'Marca',
    'campaigns-manager' => 'Administrador de Campañas',
    'category-manager' => 'Administrador de Categorías',
    'completed' => 'El registro ha sido complettado.',
    'content' => 'Contenido',
    'deleted' => 'Objeto eliminado exitosamente.',
    'department-manager' => 'Administrador de Departamentos',
    'e-commerce' => 'Ventas En Línea',
    'edit' => 'Editar',
    'email' => 'Correo Electrónico',
    'general-requests' => 'Solicitudes',
    'general-settings' => 'Configuraciones Generales',
    'inventory' => 'Inventario',
    'inventory-manager' => 'Administrador de Inventarios',
    'name' => 'Nombre',
    'no-records-found' => 'No hay registros disponibles.',
    'online-shopping' => 'Tienda En Línea',
    'online-shopping-search-stats' => 'Estadísticas de Búsqueda',
    'pricing-files-manager' => 'Administrador de Archivos de Precios',
    'product-manager' => 'Administrador de Productos',
    'reporting' => 'Reportes',
    'resources' => 'Recursos',
    'search' => 'Buscar',
    'select' => 'Seleccione',
    'optional' => 'Opcional',
    'status' => 'Estado',
    'save' => 'Guardar Cambios',
    'saved' => 'Registro almacenado exitosamente.',
    'start' => 'Comenzar',
    'update' => 'Actualizar',
    'updated' => 'El registro ha sido actualizado exitosamente',
    'welcome' => 'Hola :name!',
    'your-store' => 'Tu Tienda:',

];
