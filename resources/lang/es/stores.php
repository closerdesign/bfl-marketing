<?php

return [

    'basic-information' => 'Información Básica',
    'city' => 'Ciudad',
    'closed' => 'Cerrada',
    'guests-services-hours' => 'Horario de Servicio Al Cliente',
    'hours-information' => 'Horarios',
    'id' => 'Tienda',
    'online-shopping' => 'Compras en Línea',
    'online-shopping-phone-number' => 'Número de Contacto de Compras en Línea',
    'open' => 'Abierta',
    'postal-code' => 'Código Postal',
    'price_strategy' => 'Estrategia De Precios',
    'settings' => 'Configuración',
    'state' => 'Estado',

];
