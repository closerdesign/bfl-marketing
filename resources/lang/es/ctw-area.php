<?php

return [
    'brands' => 'Marcas',
    'create' => 'Crear Área',
    'edit' => 'Editar',
    'manage-areas' => 'Administrar Áreas',
    'name' => 'Nombre',
    'none' => 'Ninguna',
    'title' => 'Áreas de CTW',
];
