<?php

return [

    'view-campaign' => 'Ver Campaña',
    'delete' => 'Eliminar',
    'replicate' => 'Duplicar',

    'template' => 'Plantilla',
    'default-template' => 'Plantilla Por Defecto',
    'classes-template' => 'Plantilla de Clases',

];
