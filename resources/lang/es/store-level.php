<?php

return [

    'add-email-subscriber' => 'Añadir Cliente a la Lista de Email',
    'add-subscriber' => 'Agregar Suscriptor',
    'assignments' => 'Tareas Asignadas',
    'current-ads' => 'Promociones Vigentes',
    'general-requests' => 'Solicitudes',
    'informational-signage' => 'Señalización Informativa',
    'liquor-licenses' => 'Licencias de Licores',
    'marketing' => 'Mercadeo',
    'please-note-that-future-ads-are-subject-to-change' => 'Por favor tenga en cuenta que los anuncios semanales futuros están sujetos a cambio sin previo aviso.',
    'product-name' => 'Nombre del Producto',
    'price' => 'Precio',
    'signage' => 'Señalización',
    'signage-creation' => 'Creador de Anuncios',
    'signage-request' => 'Solicitar Anuncios',
    'unit-of-measure' => 'Unidad de Medida',

    'brand' => 'Marca',
    'deal-of-the-week' => 'Coupon Signage',
    'kraft-promos' => 'Kraft Promos',
    'fineprint' => 'Condiciones',
    'populate-from-coupon' => 'Utilizar Cupón para completar el formulario',
    'coupon' => 'Cupón',
    'color' => 'Color de Texto',
    'type' => 'Tipo',
    'black' => 'Negro',
    'white' => 'Blanco',
    'background' => 'Background',
    'default' => 'Default',

];
