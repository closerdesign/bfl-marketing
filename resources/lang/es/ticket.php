<?php

return [

    'import-tickets-list' => 'Importar Listado de Boletos',
    'import' => 'Importar',
    'tickets-list' => 'Listado de Boletos',
    'location' => 'Locación',
    'event' => 'Evento',
    'event-date' => 'Fecha del Evento',
    'sale-date' => 'Inicio Ventas',
    'file' => 'Archivo',

];
