<?php

return [

    'comments' => 'Comentarios',
    'continue' => 'Continuar',
    'create' => 'Crear Evaluación',
    'created' => 'Fecha de Creación',
    'evaluation-has-been-marked-as-completed' => 'La evaluación ha sido marcada como completada.',
    'heatmap' => 'Heatmap',
    'index' => 'Lista de Evaluaciones',
    'mark-as-completed' => 'Marcar como Completada',
    'show-results' => 'Mostrar Resultados',
    'score' => 'Puntaje',
    'start' => 'Iniciar Evaluación',
    'started' => 'Iniciada',
    'store' => 'Tienda',
    'title' => 'Evaluaciones de CTW',
    'user' => 'Usuario',
    'status' => 'Estado',

];
