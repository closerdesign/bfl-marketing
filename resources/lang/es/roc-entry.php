<?php

return [

    'add-new-entry' => 'Crear Nueva Entrada',
    'completed' => 'Completada',
    'content' => 'Contenido',
    'files' => 'Archivos',
    'go-to-pending-entries' => 'Ir A Entradas Pendientes',
    'images' => 'Imágenes',
    'mark-as-completed' => 'Marcar como completado',
    'marked-as-completed' => 'Marcada como completada.',
    'name' => 'Nombre',
    'pending' => 'Pendiente',
    'stores' => 'Tiendas',
    'there-are-not-pending-entries-at-this-time' => 'No hay entradas pendientes en este momento.',
    'title' => 'Entradas',
    'you-have-no-assignments-at-this-time' => 'No tienes asignaciones en este momento.',

];
