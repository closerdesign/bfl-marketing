<?php

return [

    'view-campaign' => 'View Campaign',
    'delete' => 'Delete',
    'replicate' => 'Replicate',

    'template' => 'Template',
    'default-template' => 'Default Template',
    'classes-template' => 'Classes Template',

];
