<?php

return [

    'add-new-entry' => 'Add New Entry',
    'completed' => 'Completed',
    'content' => 'Content',
    'files' => 'Files',
    'go-to-pending-entries' => 'Go To Pending Entries',
    'images' => 'Images',
    'mark-as-completed' => 'Mark As Completed',
    'marked-as-completed' => 'Marked as completed.',
    'name' => 'Name',
    'pending' => 'Pending',
    'stores' => 'Stores',
    'there-are-not-pending-entries-at-this-time' => 'There are not pending entries at this time.',
    'title' => 'ROC Entries',
    'you-have-no-assignments-at-this-time' => 'You have no assignments at this time.',

];
