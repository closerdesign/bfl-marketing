<?php

return [

    'android-mobile-app-url' => 'Android Mobile App URL',
    'facebook-url' => 'Facebook Page URL',
    'instagram-url' => 'Instagram Profile URL',
    'ios-mobile-app-url' => 'iOS Mobile App URL',
    'loyalty-website-url' => 'Loyalty Website URL',
    'online-shopping-website' => 'Online Shopping Website',
    'shipt-url' => 'SHIPT URL',
    'title' => 'Brands',

];
