<?php

return [

    'title' => 'Inventory',
    'controller-name' => 'Controller Name',
    'start' => 'Start',
    'export' => 'Export',

    'go-back' => 'Go back',
    'finish' => 'Finish',

];
