<?php

return [

    'comments' => 'Comments',
    'continue' => 'Continue',
    'create' => 'Create Evaluation',
    'created' => 'Created',
    'evaluation-has-been-marked-as-completed' => 'The evaluation has been marked as completed.',
    'heatmap' => 'Heatmap',
    'index' => 'Evaluations List',
    'mark-as-completed' => 'Mark As Completed',
    'score' => 'Score',
    'show-results' => 'Show Results',
    'start' => 'Start A New Evaluation',
    'started' => 'Started',
    'status' => 'Status',
    'store' => 'Store',
    'title' => 'CTW Evaluations',
    'user' => 'User',

];
