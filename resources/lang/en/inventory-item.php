<?php

return [

    'title' => 'Inventory Item',
    'upc' => 'UPC',
    'qty' => 'Qty',
    'send' => 'Send',

];
