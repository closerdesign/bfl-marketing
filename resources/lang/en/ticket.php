<?php

return [

    'import-tickets-list' => 'Import Tickets List',
    'import' => 'Import',
    'tickets-list' => 'Tickets List',
    'location' => 'Location',
    'event' => 'Event',
    'event-date' => 'Event Date',
    'sale-date' => 'Sale Dale',
    'file' => 'File',

];
