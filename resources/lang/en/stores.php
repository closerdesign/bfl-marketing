<?php

return [

    'basic-information' => 'Basic Information',
    'city' => 'City',
    'closed' => 'Closed',
    'guest-services-hours' => 'Guest Services Hours',
    'hours-information' => 'Hours',
    'id' => 'Store',
    'online-shopping' => 'Online Shopping',
    'online-shopping-phone-number' => 'Online Shopping Phone Number',
    'open' => 'Open',
    'postal-code' => 'Postal Code',
    'price-strategy' => 'Price Strategy',
    'settings' => 'Settings',
    'state' => 'State',

];
