<?php

return [
    'brands' => 'Brands',
    'create' => 'Create A New Area',
    'edit' => 'Edit',
    'manage-areas' => 'Manage Areas',
    'name' => 'Name',
    'none' => 'None',
    'title' => 'CTW Areas',
];
