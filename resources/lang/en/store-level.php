<?php

return [

    'add-email-subscriber' => 'Add Email Subscriber',
    'add-subscriber' => 'Add Subscriber',
    'assignments' => 'Assignments',
    'current-ads' => 'Current Ads',
    'general-requests' => 'General Requests',
    'informational-signage' => 'Informational Signage',
    'liquor-licenses' => 'Liquor Licenses',
    'marketing' => 'Marketing',
    'please-note-that-future-ads-are-subject-to-change' => 'Please note that future ads are subject to change.',
    'product-name' => 'Product Name',
    'price' => 'Price',
    'signage' => 'Signage',
    'signage-creation' => 'Signage Creation',
    'signage-request' => 'Signage Request',
    'unit-of-measure' => 'Unit Of Measure',

    'brand' => 'Brand',
    'deal-of-the-week' => 'Coupon Signage',
    'kraft-promos' => 'Kraft Promos',
    'fineprint' => 'Fineprint',
    'populate-from-coupon' => 'To populate from a coupon, please select an item from this list',
    'coupon' => 'Coupon',
    'color' => 'Text Color',
    'type' => 'Type',
    'black' => 'Black',
    'white' => 'White',
    'background' => 'Background',
    'default' => 'Default',

];
