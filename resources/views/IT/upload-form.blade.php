@extends('layouts.app')

@section('content')
    
    <div class="container-fluid">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Upload SIGIS File
            </div>
            <div class="panel-body">
                <form action="{{ action('IT\SigisController@upload') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="file">File</label>
                        <input type="file" class="form-control" name="file" required >
                    </div>
                    <button class="btn btn-success">
                        Upload
                    </button>
                </form>
            </div>
        </div>
    </div>
    
    @endsection