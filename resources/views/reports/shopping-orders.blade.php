@extends('layouts.app')

@section('content')

    <div class="container reports">
        <div class="row">
            <div class="col-sm-2">
                <div class="chart">
                    <h1>Total Sales</h1>
                    <span>${{ number_format($orders->sum_total, 2) }}</span>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="chart">
                    <h1>Total Orders</h1>
                    <span>{{ $orders->count_total }}</span>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="chart">
                    <h1>Basket Size</h1>
                    <span>${{ number_format($orders->avg_order_val->avg_basket, 2) }}</span>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="chart">
                    <h1>Order Count By Store</h1>
                    <canvas id="orderCountByStore"></canvas>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="chart">
                    <h1>Sales</h1>
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#daily">Daily Total</a></li>
                        <li><a data-toggle="tab" href="#monthly">Monthly Total</a></li>
                        <li><a data-toggle="tab" href="#sales-store">Stores Overview</a></li>
                        <li><a data-toggle="tab" href="#sales-village">9515</a></li>
                        <li><a data-toggle="tab" href="#sales-edmond">1230</a></li>
                        <li><a data-toggle="tab" href="#sales-expwy">3501</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="daily" class="tab-pane fade in active">
                            <canvas id="dailyTotal"></canvas>
                        </div>
                        <div id="monthly" class="tab-pane fade">
                            <canvas id="monthlyTotal"></canvas>
                        </div>
                        <div id="sales-store" class="tab-pane fade">
                            <canvas id="salesByStore"></canvas>
                        </div>
                        <div id="sales-village" class="tab-pane fade">
                            <canvas id="monthlyVillage"></canvas>
                        </div>
                        <div id="sales-edmond" class="tab-pane fade">
                            <canvas id="monthlyEdmond"></canvas>
                        </div>
                        <div id="sales-expwy" class="tab-pane fade">
                            <canvas id="monthlyExpwy"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="chart">
                    <h1>Payment Method</h1>
                    <canvas id="paymentType"></canvas>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="chart">
                    <h1>Orders By Complex</h1>
                    <canvas id="complex"></canvas>
                </div>
            </div>
        </div>
    </div>

    {{--Payment Method Breakdown--}}

    <?php $stripe = 0; $paypal = 0; $instore = 0; $pp_instore = 0; ?>
    @foreach($orders->payment_type as $pt)
        @if($pt->payment_method == 'Stripe')
            <?php $stripe = ($stripe + $pt->num_method); ?>
        @elseif($pt->payment_method == 'Paypal')
            <?php $paypal = ($paypal + $pt->num_method); ?>
        @elseif($pt->payment_method == 'InStore')
            <?php $instore = ($instore + $pt->num_method); ?>
        @elseif($pt->payment_method == 'InStorePaypal')
            <?php $pp_instore = ($pp_instore + $pt->num_method); ?>
        @endif
    @endforeach

@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.js"></script>

    <script>

        // Order Count By Store

        var ocbsLine = document.getElementById('orderCountByStore');
        ocbsLine.height = 75;
        new Chart(ocbsLine, {
            type: 'line',
            data:{
                labels: [
                    @foreach($orders->monthly_by_store as $m)
                            @if($m->store == 9515)
                                '{{date('M',mktime(0, 0, 0, $m->mnth, 10))}}',
                            @endif
                    @endforeach
                ],
                datasets:[{
                    label: '9515',
                    borderColor: 'rgba(10, 75, 225, 1)',
                    backgroundColor: 'rgba(10, 75, 225, 1)',
                    data: [
                        @foreach($orders->monthly_by_store as $ms)
                            @if($ms->store == 9515)
                                {{$ms->fulfilled}},
                            @endif
                        @endforeach
                    ], fill: false
                },{
                    label: '1230',
                    borderColor: 'rgba(215, 0, 45, 1)',
                    backgroundColor: 'rgba(215, 0, 45, 1)',
                    data: [
                        @foreach($orders->monthly_by_store as $ms)
                        @if($ms->store == 1230)
                            {{$ms->fulfilled}},
                        @endif
                        @endforeach
                    ], fill: false
                },{
                    label: '3501',
                    borderColor: 'rgba(125, 80, 20, 1)',
                    backgroundColor: 'rgba(125, 80, 20, 1)',
                    data: [
                        @foreach($orders->monthly_by_store as $ms)
                        @if($ms->store == 3501)
                        {{$ms->fulfilled}},
                        @endif
                        @endforeach
                    ], fill: false
                }
                {{--,{--}}
                    {{--label: '2701',--}}
                    {{--borderColor: 'rgba(255, 150, 200, 1)',--}}
                    {{--backgroundColor: 'rgba(255, 150, 200, 1)',--}}
                    {{--data: [--}}
                        {{--@foreach($orders->monthly_by_store as $ms)--}}
                        {{--@if($ms->store == 2701)--}}
                        {{--{{$ms->fulfilled}},--}}
                        {{--@endif--}}
                        {{--@endforeach--}}
                    {{--], fill: false--}}
                {{--},{--}}
                    {{--label: '1006',--}}
                    {{--borderColor: 'rgba(110, 250, 200, 1)',--}}
                    {{--backgroundColor: 'rgba(110, 250, 200, 1)',--}}
                    {{--data: [--}}
                        {{--@foreach($orders->monthly_by_store as $ms)--}}
                        {{--@if($ms->store == 1006)--}}
                        {{--{{$ms->fulfilled}},--}}
                        {{--@endif--}}
                        {{--@endforeach--}}
                    {{--], fill: false--}}
                {{--}--}}
                ]
            },
            options: {responsive: true}
        });

        // Monthly Total

        var ctxLine = document.getElementById('monthlyTotal');
        ctxLine.height = 75;
        var monthlyTotal = new Chart(ctxLine, {
            type: 'line',
            data:{
                labels: [
                    @foreach($orders->total_by_month as $m)
                        '{{date('M',mktime(0, 0, 0, $m->mnth, 10))}}',
                    @endforeach
                ],
                datasets:[{
                    label: 'Monthly Sales',
                    backgroundColor: 'rgba(0, 255, 255, 0.2)',
                    data: [
                        @foreach($orders->total_by_month as $ms)
                        {{$ms->totals}},
                        @endforeach
                    ]
                }]
            },
            options: {
                responsive: true,
                animation: {
                    duration: 0
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });

        // Daily Total

        var dailyLine = document.getElementById('dailyTotal');
        dailyLine.height = 75;
        var dailyTotal = new Chart(dailyLine, {
            type: 'line',
            data:{
                labels: [
                    @foreach($orders->total_by_day as $d)
                        {{date('d', strtotime($d->day))}},
                    @endforeach
                ],

                datasets:[{
                    label: 'Daily Sales',
                    backgroundColor: 'rgba(255, 255, 0, 0.2)',
                    data: [
                        @foreach($orders->total_by_day as $d)
                            {{$d->totals}},
                        @endforeach
                    ]
                }]
            },
            options: {
                responsive: true,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });

        // Sales By Store

        var sbsLine = document.getElementById('salesByStore');
        sbsLine.height = 75;
        var salesByStore = new Chart(sbsLine, {
            type: 'line',
            data:{
                labels: [
                    @foreach($orders->monthly_by_store as $m)
                            @if($m->store == 9515)
                        '{{date('M',mktime(0, 0, 0, $m->mnth, 10))}}',
                    @endif
                    @endforeach
                ],
                datasets:[{
                    label: '9515',
                    borderColor: 'rgba(10, 75, 225, 1)',
                    backgroundColor: 'rgba(10, 75, 225, 1)',
                    data: [
                        @foreach($orders->monthly_by_store as $ms)
                        @if($ms->store == 9515)
                        {{$ms->totals}},
                        @endif
                        @endforeach
                    ], fill: false
                },{
                    label: '1230',
                    borderColor: 'rgba(215, 0, 45, 1)',
                    backgroundColor: 'rgba(215, 0, 45, 1)',
                    data: [
                        @foreach($orders->monthly_by_store as $ms)
                        @if($ms->store == 1230)
                        {{$ms->totals}},
                        @endif
                        @endforeach
                    ], fill: false
                },{
                    label: '3501',
                    borderColor: 'rgba(125, 80, 20, 1)',
                    backgroundColor: 'rgba(125, 80, 20, 1)',
                    data: [
                        @foreach($orders->monthly_by_store as $ms)
                        @if($ms->store == 3501)
                        {{$ms->totals}},
                        @endif
                        @endforeach
                    ], fill: false
                }
                {{--,{--}}
                    {{--label: '2701',--}}
                    {{--borderColor: 'rgba(255, 150, 200, 1)',--}}
                    {{--backgroundColor: 'rgba(255, 150, 200, 1)',--}}
                    {{--data: [--}}
                        {{--@foreach($orders->monthly_by_store as $ms)--}}
                        {{--@if($ms->store == 2701)--}}
                        {{--{{$ms->totals}},--}}
                        {{--@endif--}}
                        {{--@endforeach--}}
                    {{--], fill: false--}}
                {{--},{--}}
                    {{--label: '1006',--}}
                    {{--borderColor: 'rgba(110, 250, 200, 1)',--}}
                    {{--backgroundColor: 'rgba(110, 250, 200, 1)',--}}
                    {{--data: [--}}
                        {{--@foreach($orders->monthly_by_store as $ms)--}}
                        {{--@if($ms->store == 1006)--}}
                        {{--{{$ms->totals}},--}}
                        {{--@endif--}}
                        {{--@endforeach--}}
                    {{--], fill: false--}}
                {{--}--}}
                ]
            },
            options: {
                responsive: true,
                animation: {
                    duration: 0
                }
            }
        });

        // 9515 Monthly

        var vmsBar = document.getElementById('monthlyVillage');
        vmsBar.height = 75;
        var monthlyVillage = new Chart(vmsBar, {
            type: 'bar',
            data:{
                labels: [
                    @foreach($orders->monthly_by_store as $m)
                        @if($m->store == 9515)
                            '{{date('M',mktime(0, 0, 0, $m->mnth, 10))}}',
                        @endif
                    @endforeach
                ],
                datasets:[{
                    label: '9515',
                    borderColor: 'rgba(10, 75, 225, 1)',
                    backgroundColor: 'rgba(10, 75, 225, 1)',
                    data: [
                        @foreach($orders->monthly_by_store as $ms)
                            @if($ms->store == 9515)
                                {{$ms->totals}},
                            @endif
                        @endforeach
                    ], fill: false
                }]
            },
            options: {
                responsive: true,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
                animation: {
                    duration: 0
                }
            }
        });

        // 1230 Monthly

        var emsBar = document.getElementById('monthlyEdmond');
        emsBar.height = 75;
        var monthlyEdmond = new Chart(emsBar, {
            type: 'bar',
            data:{
                labels: [
                    @foreach($orders->monthly_by_store as $m)
                            @if($m->store == 1230)
                        '{{date('M',mktime(0, 0, 0, $m->mnth, 10))}}',
                    @endif
                    @endforeach
                ],
                datasets:[{
                    label: '1230',
                    borderColor: 'rgba(215, 0, 45, 1)',
                    backgroundColor: 'rgba(215, 0, 45, 1)',
                    data: [
                        @foreach($orders->monthly_by_store as $ms)
                            @if($ms->store == 1230)
                                {{$ms->totals}},
                            @endif
                        @endforeach
                    ], fill: false
                }]
            },
            options: {
                responsive: true,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
                animation: {
                    duration: 0
                }
            }
        });

        // 3501 Monthly

        var xmsBar = document.getElementById('monthlyExpwy');
        xmsBar.height = 75;
        var monthlyExpwy = new Chart(xmsBar, {
            type: 'bar',
            data:{
                labels: [
                    @foreach($orders->monthly_by_store as $m)
                            @if($m->store == 3501)
                        '{{date('M',mktime(0, 0, 0, $m->mnth, 10))}}',
                    @endif
                    @endforeach
                ],
                datasets:[{
                    label: '3501',
                    borderColor: 'rgba(125, 80, 20, 1)',
                    backgroundColor: 'rgba(125, 80, 20, 1)',
                    data: [
                        @foreach($orders->monthly_by_store as $ms)
                            @if($ms->store == 3501)
                                {{$ms->totals}},
                            @endif
                        @endforeach
                    ], fill: false
                }]
            },
            options: {
                responsive: true,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
                animation: {
                    duration: 0
                }
            }
        });

        // Payment Type

        new Chart(document.getElementById("paymentType"), {
            type: 'pie',
            data: {
                labels: ["Stripe", "Paypal", "InStore", "InStorePaypal"],
                datasets: [
                    {
                        backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9"],
                        data: [{{ $stripe }},{{ $paypal }},{{ $instore }},{{ $pp_instore }}]
                    }
                ]
            },
            options: {
                legend: {
                    position: 'bottom'
                }
            }
        });

        // Complex

        new Chart(document.getElementById("complex"), {
            type: 'bar',
            data: {
                labels: [
                    @foreach($orders->by_complex as $c)
                        '{{ substr($c->complex, 0, 10) . '...' }}',
                    @endforeach
                ],
                datasets: [
                    {
                        backgroundColor: ["#5a9a4f", "#cb5b54","#007dc6","#ffd602"],
                        data: [
                            @foreach($orders->by_complex as $cx)
                                '{{ $cx->num_complex }}',
                            @endforeach
                        ]
                    }
                ]
            },
            options: {
                legend: { display: false }
            }
        });

    </script>

@endsection