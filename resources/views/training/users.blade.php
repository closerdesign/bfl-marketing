@extends('layouts.app')

@section('content')

    <div class="container">
        <p>
            <a href="{{ action('TrainingController@edit', $training->id) }}" class="btn btn-primary">
                <i class="fa fa-arrow-circle-left"></i> Go Back
            </a>
        </p>
        <div class="panel panel-primary">
            <div class="panel-heading">Users for {{ $training->name }}</div>

            <div class="panel-body">

                <div class="table-responsive">
                    <table class="table table-striped table-condensed">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Store</th>
                            <th>Dept</th>
                            <th>Started</th>
                            <th>Completed</th>
                        </tr>
                        </thead>

                        <tbody>
                        @if( count($users) == 0 )
                            <tr><td colspan="6" class="text-center">No users have taken this training module.</td></tr>
                        @else
                            @foreach($users as $user)
                                <tr><td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->store }}</td>
                                <td>{{ $user->department }}</td>
                                <td>{{ $user->created_at }}</td>
                                    <td>
                                        @if($user->complete == 1)
                                            {{ $user->updated_at }}
                                        @else
                                            <span class="text-danger">Not yet complete</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </table>
                    {{ $users->render() }}
                </div>
            </div>
        </div>
    </div>

@endsection