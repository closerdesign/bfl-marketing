@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">

                    <div class="panel-heading">{{ $slide->name }}</div>

                    <div class="panel-body">

                        @if(!empty($slide->image))
                            <img class="img-responsive center-block" src="{{ Storage::url('training-slides/' . $slide->image) }}" alt="">
                        @endif

                        @if(!empty($slide->video))
                            <div class="sizer">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe src="https://player.vimeo.com/video/{!! $slide->video !!}"></iframe>
                                </div>
                            </div>
                        @endif

                        <p>{!! $slide->description !!}</p>

                    </div>

                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-xs-6">
                                <a class="btn btn-primary" href=""><i class="fa fa-arrow-circle-left"></i> Previous</a>
                            </div>
                            <div class="col-xs-6 text-right">
                                <a class="btn btn-primary" href="">Next <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection