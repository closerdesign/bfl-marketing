@extends('layouts.app')

@section('content')

    <div class="container">
        <p>
            <a href="{{ action('TrainingController@edit', $slide->training_id) }}" class="btn btn-primary">
                <i class="fa fa-arrow-circle-left"></i> Go Back
            </a>
            <a href="{{ action('TrainingSlidesController@preview', $slide->id) }}" target="_blank" class="btn btn-info">
                <i class="fa fa-desktop"></i> Preview Slide
            </a>
        </p>
        <div class="row">
            <div class="col-md-5">
                <div class="panel panel-info">
                    <div class="panel-heading">Media</div>
                    <div class="panel-body">
                        <h4>Image</h4>
                        @if(empty($slide->image))
                            <p>No image added.</p>
                        @else
                            <img class="img-responsive" src="{{ Storage::url('training-slides/' . $slide->image) }}" alt="">
                        @endif
                        <h4>Video</h4>
                        @if(empty($slide->video))
                            <p>No video added.</p>
                        @else
                            <div class="sizer">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe src="https://player.vimeo.com/video/{!! $slide->video !!}"></iframe>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                <p>
                    To add a video, you only need to add the unique id from the Vimeo link. <br />
                    <small>Example: <span class="text-muted">https://vimeo.com/<mark>264880943</mark></span></small>
                </p>
            </div>
            <div class="col-md-7">
                <div class="panel panel-primary">
                    <div class="panel-heading">Update Slide</div>
                    <div class="panel-body">
                        <form action="{{ action('TrainingSlidesController@update', $slide->id) }}" method="post" enctype="multipart/form-data" >
                            {{ csrf_field() }}

                            {{ method_field('PATCH') }}
                            <div class="col-md-6 form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" value="{{ $slide->name }}" required >
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="position">Position</label>
                                <input type="number" class="form-control text-center" name="position" value="{{ $slide->position }}" required >
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="image">Image</label>
                                <input type="file" class="form-control" name="image" >
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="video">Video</label>
                                <input type="text" class="form-control" name="video" value="{{ $slide->video }}" >
                            </div>
                            <div class="col-md-12 form-group">
                                <label for="description">Description</label>
                                <textarea name="description" id="description" cols="30" rows="5" class="form-control summernote" >{{ $slide->description }}</textarea>
                            </div>
                            <div class="col-md-12 form-group">
                                <button class="btn-success form-control" type="submit"><i class="fa fa-floppy-o"></i> Update Slide</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection