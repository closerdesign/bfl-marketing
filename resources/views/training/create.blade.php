@extends('layouts.app')

@section('content')

    <div class="container">
        <p><a href="{{ action('TrainingController@index') }}" class="btn btn-primary"><i class="fa fa-arrow-circle-left"></i> Go Back</a></p>
        <div class="row">
            <div class="col-md-6 col-md-offset-3">

                <div class="panel panel-primary">
                    <div class="panel-heading">Create Training</div>
                    <div class="panel-body">
                        <form action="{{ action('TrainingController@store') }}" method="post" >
                            {{ csrf_field() }}
                            <div class="col-md-12 form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" value="" required >
                            </div>
                            <div class="col-md-12 form-group">
                                <label for="category">Category</label>
                                <input type="text" class="form-control" name="category" value="" required >
                            </div>
                            <div class="col-md-12 form-group">
                                <button class="btn-success form-control" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection