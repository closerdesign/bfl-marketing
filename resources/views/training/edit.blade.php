@extends('layouts.app')

@section('content')

    <div class="container">
        <p>
            <a href="{{ action('TrainingController@index') }}" class="btn btn-primary">
                <i class="fa fa-arrow-circle-left"></i> Go Back
            </a>
            <a href="{{ action('TrainingController@users', $training->id) }}" class="btn btn-info">
                <i class="fa fa-list"></i> Users
            </a>
        </p>
        <div class="row">
            <div class="col-md-5">

                <div class="panel panel-primary">
                    <div class="panel-heading">Edit Training</div>
                    <div class="panel-body">
                        <form action="{{ action('TrainingController@update', $training->id) }}" method="post" >
                            {{ csrf_field() }}

                            {{ method_field('PATCH') }}

                            <div class="col-md-12 form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" value="{{ $training->name }}" required >
                            </div>
                            <div class="col-md-12 form-group">
                                <label for="category">Category</label>
                                <input type="text" class="form-control" name="category" value="{{ $training->category }}" required >
                            </div>
                            <div class="col-md-12 form-group">
                                <button class="btn-success form-control" type="submit"><i class="fa fa-floppy-o"></i> Update</button>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="panel panel-info">
                    <div class="panel-heading">Slides</div>
                    <div class="panel-body">
                        <div class="table-responsive">

                            <table class="table table-striped table-condensed">

                                <thead>
                                <tr>
                                    <th>Pos.</th>
                                    <th>Name</th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                                </thead>

                                <tbody>
                                @if( count($slides) > 0 )
                                    @foreach($slides as $slide)
                                        <tr>
                                            <td width="50">{{ $slide->position }}</td>
                                            <td><a href="{{ action('TrainingSlidesController@edit', $slide->id) }}">{{ $slide->name }}</a></td>
                                            <td class="text-center" width="25">
                                                <a href="{{ action('TrainingSlidesController@preview', $slide->id) }}" target="_blank">
                                                    <button class="btn-xs btn-info" type="submit" style="height: 25px; width: 25px;">
                                                        <i class="fa fa-desktop"></i>
                                                    </button>
                                                </a>
                                            </td>
                                            <td class="text-center" width="25">
                                                <form action="{{ action('TrainingSlidesController@destroy', $slide->id) }}" method="post" onsubmit="return confirm('Are you sure?')">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                    <button class="btn-xs btn-danger" type="submit" style="height: 25px; width: 25px;">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr><td colspan="3"><p class="text-center">No slides added yet.</p></td></tr>
                                @endif
                                </tbody>

                            </table>

                        </div>
                    </div>
                </div>

                <p>
                    To add a video, you only need to add the unique id from Vimeo. <br />
                    <small>Example: <span class="text-muted">https://vimeo.com/<mark>264829470</mark></span></small>
                </p>

            </div>
            <div class="col-md-7">

                <div class="panel panel-primary">
                    <div class="panel-heading">Add Slide</div>
                    <div class="panel-body">
                        <form action="{{ action('TrainingSlidesController@store', $training->id) }}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="col-md-6 form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" value="" required >
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="position">Position</label>
                                <input type="number" class="form-control text-center" name="position" value="" required >
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="image">Image</label>
                                <input type="file" class="form-control" name="image" >
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="video">Video</label>
                                <input type="text" class="form-control" name="video" value="" >
                                <label>Enter only Vimeo code.</label>
                            </div>
                            <div class="col-md-12 form-group">
                                <label for="description">Description</label>
                                <textarea name="description" id="description" cols="30" rows="5" class="form-control summernote" ></textarea>
                            </div>
                            <div class="col-md-12 form-group">
                                <button class="btn-success form-control" type="submit"><i class="fa fa-plus"></i> Add Slide</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection