@extends('layouts.app')

@section('content')

    <div class="container-fluid">

        @if(Auth::user())
            <p class="text-right">
                <a href="{{ action('TrainingController@create') }}" class="btn btn-success"><i class="fa fa-plus"></i> Create New Training</a>
            </p>
        @endif

        <div class="panel panel-default">
            <div class="panel-heading">Training</div>

            <div class="panel-body">

                <div class="table-responsive">

                    <table class="table table-striped table-hover table-condensed">

                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Category</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>

                        <tbody>
                        @if( count($trainings) == 0 )
                            <tr><td colspan="4">No training modules have been added.</td></tr>
                        @else
                            @foreach($trainings as $training)
                            <tr>
                                <td>
                                    @if(Auth::user())
                                        <a href="{{ action('TrainingController@edit', $training->id) }}">{{ $training->name }}</a>
                                    @else
                                        {{ $training->name }}
                                    @endif
                                </td>
                                <td>{{ $training->category }}</td>
                                <td width="150" class="text-right">
                                    <a href="{{ action('TrainingController@show', $training->id) }}">
                                        <button class="btn-xs btn-success">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Start Training
                                        </button>
                                    </a>
                                </td>
                                <td class="text-left">
                                    @if(Auth::user())
                                        <form action="{{ action('TrainingController@destroy', $training->id) }}" method="post" onsubmit="return confirm('Are you sure? This cannot be undone. All slides associated with this training module will be removed as well.')">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button class="btn-xs btn-danger" type="submit">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        @endif
                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </div>

@endsection