@extends('layouts.app')

@section('content')

    @if( Session::has('training') && (Session::get('training')['id'] == $training->id) )

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">

                        @foreach($slides as $slide)

                            <div class="panel-heading">{{ $slide->name }}</div>

                            <div class="panel-body">

                                @if(!empty($slide->image))
                                    <img class="img-responsive center-block" src="{{ Storage::url('training-slides/' . $slide->image) }}" alt="">
                                @endif

                                @if(!empty($slide->video))
                                    <div class="sizer">
                                        <div class="embed-responsive embed-responsive-16by9">
                                            <iframe src="https://player.vimeo.com/video/{!! $slide->video !!}"></iframe>
                                        </div>
                                    </div>
                                @endif

                                <p>{!! $slide->description !!}</p>

                            </div>

                            <div class="panel-footer">
                                <div class="row">
                                    <div class="col-xs-6">
                                        @if( $slides->previousPageURL() == '' ) &nbsp; @else
                                            <a class="btn btn-primary" href="{{ $slides->previousPageURL() }}"><i class="fa fa-arrow-circle-left"></i> Previous</a>
                                        @endif
                                    </div>
                                    @if( $slides->hasMorePages() )
                                        <div class="col-xs-6 text-right">
                                            <a class="btn btn-primary" href="{{ $slides->nextPageUrl() }}">Next <i class="fa fa-arrow-circle-right"></i></a>
                                        </div>
                                    @else
                                        <div class="col-xs-6 text-right">
                                            <a href="{{ action('TrainingController@complete') }}" class="btn btn-success">
                                                <i class="fa fa-check"></i> Complete Training
                                            </a>
                                        </div>
                                    @endif
                                </div>
                            </div>

                        @endforeach

                </div>
            </div>
        </div>
    </div>

    @else

    <div class="container">
        <div class="row">
            <p><a href="{{ action('TrainingController@index') }}" class="btn btn-primary"><i class="fa fa-arrow-circle-left"></i> Go Back</a></p>
            @if( count($slides) == 0 )
                <p class="text-danger text-center">There are no slides associated with this training module.</p>
            @else
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-primary">
                        <div class="panel-heading">{{ $training->name }}</div>
                        <div class="panel-body">
                            <form action="{{ action('TrainingController@start', $training->id) }}" method="post" >
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-sm-6 form-group">
                                        <label for="name">Name</label>
                                        <input type="text" class="form-control input-lg" name="name" value="{{ old('name') }}" required >
                                    </div>
                                    <div class="col-sm-6 form-group">
                                        <label for="email">Email</label>
                                        <input type="text" class="form-control input-lg" name="email" value="{{ old('email') }}" required >
                                    </div>
                                    <div class="col-sm-6 form-group form-group-lg">
                                        <label for="store">Store</label>
                                        <select class="form-control" id="store" name="store" required >
                                            <option>Select...</option>
                                            <option @if( old('store_id') == 'Support Center' ) selected @endif value="Support Center">Support Center</option>
                                            @foreach(\App\Store::orderBy('id')->get() as $store)
                                                <option @if( old('store') == $store->id ) selected @endif value="{{ $store->store_code }}">{{ $store->store_code }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6 form-group">
                                        <label for="department">Department</label>
                                        <input type="text" class="form-control input-lg" name="department" value="{{ old('department') }}" required >
                                    </div>
                                    <div class="col-md-6"><span class="label label-danger">Note: All Fields are Required</span></div>
                                    <div class="col-md-6 text-right">
                                        <button id="start" type="submit" class="btn btn-success btn-lg">Start Training</button>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            @endif
        </div>

    </div>

    @endif

@endsection