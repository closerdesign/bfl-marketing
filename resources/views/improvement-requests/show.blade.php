@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                Improvement Request: {{ $improvement_request->title }}
            </div>
            <div class="panel-body">

                {!! $improvement_request->description !!}
                @if( $improvement_request->file != "" )
                <hr>
                <p>
                    Attached File:
                    <a href="{{ \Illuminate\Support\Facades\Storage::url('improvement-requests/' . $improvement_request->file) }}" target="_blank">
                        {{ $improvement_request->file }}
                    </a>
                </p>
                @endif

                @if( Auth::user()->isAdmin() )
                <hr>
                <form action="{{ action('ImprovementRequestsController@update', $improvement_request->id) }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}
                    <div class="form-group">
                        <label for="status">Status Update</label>
                        <div class="input-group">
                            <select name="status" id="status" class="form-control">
                                <option value="">Select...</option>
                                <option @if($improvement_request->status == 'PENDING') selected @endif value="PENDING">Pending</option>
                                <option @if($improvement_request->status == 'IN_PROGRESS') selected @endif value="IN_PROGRESS">In Progress</option>
                                <option @if($improvement_request->status == 'COMPLETED') selected @endif value="COMPLETED">Completed</option>
                                <option @if($improvement_request->status == 'DISCARDED') selected @endif value="DISCARDED">Discarded</option>
                                <option @if($improvement_request->status == 'POSTPONED') selected @endif value="POSTPONED">Postponed</option>
                            </select>
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="submit">
                                    Go
                                </button>
                            </span>
                        </div>
                    </div>
                </form>
                @endif

            </div>
        </div>
    </div>

    @endsection