@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">Improvement Requests</div>
            <div class="panel-body">
                <p class="text-right">
                    <a href="{{ action('ImprovementRequestsController@create') }}" class="btn btn-success">
                        Create A New Request
                    </a>
                </p>

                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                    @foreach(['IN_PROGRESS', 'PENDING', 'COMPLETED', 'POSTPONED', 'DISCARDED'] as $status)
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#{{ $status }}" aria-expanded="true" aria-controls="collapseOne">
                                        {{ str_replace("_", " ", $status) }}
                                    </a>
                                </h4>
                            </div>
                            <div id="{{ $status }}" class="panel-collapse collapse @if($status == 'IN_PROGRESS') in @endif" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">

                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped datatable">
                                            <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th>Status</th>
                                                @if(Auth::user()->isAdmin())
                                                    <th>Action</th>
                                                @endif
                                                <th>Created</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($requests as $request)

                                                @if( $request->status == str_replace("_", " ", $status))
                                                <tr>
                                                    <th>
                                                        <a href="{{ action('ImprovementRequestsController@show', $request->id) }}">
                                                            {{ $request->title }}
                                                        </a>
                                                    </th>
                                                    <td class="text-center">{{ $request->status }}</td>
                                                    @if(Auth::user()->isAdmin())
                                                        <td>
                                                            <form action="{{ action('ImprovementRequestsController@update', $request->id) }}" method="post">
                                                                {{ csrf_field() }}
                                                                {{ method_field('PATCH') }}
                                                                <div class="input-group">
                                                                    <select name="status" id="status" class="form-control">
                                                                        <option value="">Select...</option>
                                                                        <option value="IN_PROGRESS">In Progress</option>
                                                                        <option value="COMPLETED">Completed</option>
                                                                        <option value="DISCARDED">Discarded</option>
                                                                        <option value="POSTPONED">Postponed</option>
                                                                    </select>
                                                                    <span class="input-group-btn">
                                        <button class="btn btn-primary" type="submit">
                                            Go
                                        </button>
                                    </span>
                                                                </div>
                                                            </form>
                                                        </td>
                                                    @endif
                                                    <td class="text-center">{{ date('Y-m-d', strtotime($request->created_at)) }}</td>
                                                </tr>
                                                @endif

                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                        @endforeach

                </div>

            </div>
        </div>
    </div>

    @endsection