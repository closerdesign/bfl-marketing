@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">Create A New Request</div>
            <div class="panel-body">
                <form action="{{ action('ImprovementRequestsController@store') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" name="title" value="{{ old('title') }}" required >
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea name="description" id="description" cols="30" rows="10"
                                  class="form-control summernote">{{ old('description') }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="file">File</label>
                        <input type="file" class="form-control" name="file" >
                    </div>
                    <button class="btn btn-primary">Save changes</button>
                </form>
            </div>
        </div>
    </div>

    @endsection