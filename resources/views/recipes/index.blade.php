@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="panel panel-default">
            <div class="panel-heading">Recipes Manager</div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-eye" aria-hidden="true"></i> View
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="https://buyforlessok.com/recipes" target="_blank">
                                        Buy For Less
                                    </a>
                                </li>
                                <li>
                                    <a href="https://smartsaverok.com/recipes" target="_blank">
                                        Smart Saver
                                    </a>
                                </li>
                                <li>
                                    <a href="https://uptowngroceryco.com/recipes" target="_blank">
                                        Uptown
                                    </a>
                                </li>

                            </ul>
                        </div>

                        <a class="btn btn-success" href="{{ action('RecipesController@create') }}"><i class="fa fa-plus-circle"></i> Add recipe</a>

                    </div>
                </div>
                <hr>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach(App\Recipe::orderBy('name')->get() as $recipe)
                            <tr>
                                <th>
                                    <a href="{{ action('RecipesController@edit', $recipe->id) }}">
                                        {{ $recipe->name }}
                                    </a>
                                </th>
                                <td class="text-center">{{ $recipe->status }}</td>
                                <td class="text-center">
                                    <form action="{{ action('RecipesController@destroy', $recipe->id) }}" method="post" onsubmit="return confirm('Are you sure?')">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button class="btn btn-danger" type="submit">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @endsection