@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Editing Recipe: {{ $recipe->name }}</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <form action="{{ action('RecipesController@update', $recipe->id) }}" method="post" enctype="multipart/form-data" >
                                    {{ csrf_field() }}
                                    {{ method_field('PATCH') }}
                                    <div class="row">
                                        <div class="col-md-12 form-group">
                                            <label for="name">Name</label>
                                            <input type="text" class="form-control" name="name" value="{{ $recipe->name }}" required >
                                        </div>
                                        <div class="col-md-12">
                                            <img src="{{ $recipe->image }}" alt="{{ $recipe->name }}" width="220" class="img-rounded pull-right">
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <label for="image">Image</label>
                                            <input type="file" class="form-control" name="image" >
                                            <label>Image size: 640px x 480px. 72ppp.</label>
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <label for="ingredients">Ingredients</label>
                                            <textarea name="ingredients" id="ingredients" cols="30" rows="10"
                                                      class="form-control summernote" required >{{ $recipe->ingredients }}</textarea>
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <label for="directions">Directions</label>
                                            <textarea name="directions" id="directions" cols="30" rows="10"
                                                      class="form-control summernote" required >{{ $recipe->directions }}</textarea>
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <label for="status">Status</label>
                                            <select name="status" id="status" class="form-control" required >
                                                <option value="">Select...</option>
                                                <option @if($recipe->status == 'Published') selected @endif value="1">Published</option>
                                                <option @if($recipe->status == 'Unpublished') selected @endif value="0">Unpublished</option>
                                            </select>
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <button class="btn btn-primary form-control" type="submit">Save changes</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection