@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">Create a new recipe</div>
            <div class="panel-body">
                <form action="{{ action('RecipesController@store') }}" method="post" enctype="multipart/form-data" >
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-12">
                            <p class="lead">Create a new recipe:</p>
                        </div>
                        <div class="col-md-12 form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}" required >
                        </div>
                        <div class="col-md-12 form-group">
                            <label for="image">Image</label>
                            <input type="file" class="form-control" name="image" required >
                            <label>Image size: 640px x 480px. 72ppp.</label>
                        </div>
                        <div class="col-md-12 form-group">
                            <label for="ingredients">Ingredients</label>
                            <textarea name="ingredients" id="ingredients" cols="30" rows="10"
                                      class="form-control summernote" required >{{ old('ingredients') }}</textarea>
                        </div>
                        <div class="col-md-12 form-group">
                            <label for="directions">Directions</label>
                            <textarea name="directions" id="directions" cols="30" rows="10"
                                      class="form-control summernote" required >{{ old('directions') }}</textarea>
                        </div>
                        <div class="col-md-12 form-group">
                            <label for="status">Status</label>
                            <select name="status" id="status" class="form-control" required >
                                <option value="">Select...</option>
                                <option @if(old('status') == 1) selected @endif value="1">Published</option>
                                <option value="0">Unpublished</option>
                            </select>
                        </div>
                        <div class="col-md-12 form-group">
                            <button class="btn btn-primary form-control" type="submit">Save changes</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @endsection