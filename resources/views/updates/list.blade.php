@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">Platform Updates</div>
                    <div class="panel-body">
                        <ul class="list-group">
                            @foreach(\App\Update::orderBy('created_at', 'desc')->get() as $update)
                                <li class="list-group-item">
                                    <a href="{{ action('UpdatesController@show', $update->id) }}">{{ $update->title }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection