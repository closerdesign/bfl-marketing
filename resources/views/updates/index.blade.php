@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">Updates</div>
            <div class="panel-body">
                <p class="text-right">
                    <a href="{{ action('UpdatesController@create') }}" class="btn btn-success">Add New Update</a>
                </p>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped datatable">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Created at</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($updates as $update)
                            <tr>
                                <td>
                                    <a href="{{ action('UpdatesController@edit', $update->id) }}">
                                        {{ $update->title }}
                                    </a>
                                </td>
                                <td class="text-center">
                                    {{ $update->created_at }}
                                </td>
                                <td class="text-center">
                                    <form
                                            action="{{ action('UpdatesController@destroy', $update->id) }}"
                                            method="post"
                                            onsubmit="return confirm('Are you sure?')">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button class="btn btn-danger">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @endsection