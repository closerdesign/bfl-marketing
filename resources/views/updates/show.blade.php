@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">{{ $update->title }}</div>
            <div class="panel-body">
                {!! $update->description !!}
                <hr>
                <p class="text-right">
                    Created: {{ $update->created_at }}
                </p>
            </div>
        </div>
    </div>

    @endsection