@extends('layouts.app')

@section('content')

    <div class="container">
        <p class="text-right">
            <img src="{{ $reward->image }}" alt="{{ $reward->name }}" class="img-responsive" width="90">
        </p>
        <form action="{{ action('RewardsController@update', $reward->id) }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Edit Reward: {{ $reward->name }}
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="name" value="{{ $reward->name }}" required >
                    </div>
                    <div class="form-group">
                        <label for="image">Image</label>
                        <input type="file" class="form-control" name="image">
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea name="description" id="description" cols="30" rows="10"
                                  class="form-control summernote">{!! $reward->description !!}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="plu">PLU</label>
                        <input type="number" class="form-control" name="plu" value="{{ $reward->plu }}" >
                    </div>
                    <div class="form-group checkbox">
                        <label><input type="checkbox" name="plu_required" value="1" @if( $reward->plu_required == true ) checked @endif > Is PLU Required?</label>
                    </div>
                    <div class="form-group">
                        <label for="promo_number">Promo Number</label>
                        <input type="text" class="form-control" name="promo_number" value="{{ $reward->promo_number }}" >
                    </div>
                    <div class="form-group">
                        <label for="brand_id">Brand</label>
                        <select name="brand_id" id="brand_id" class="form-control" required >
                            <option value="">Select...</option>
                            @foreach(App\Brand::all() as $brand)
                                <option @if( $reward->brand_id == $brand->id ) selected @endif value="{{ $brand->id }}">{{ $brand->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="start_date">Start Date</label>
                        <input type="date" class="form-control" name="start_date" value="{{ $reward->start_date }}" required >
                    </div>
                    <div class="form-group">
                        <label for="end_date">End Date</label>
                        <input type="date" class="form-control" name="end_date" value="{{ $reward->end_date }}" required >
                    </div>
                    <div class="form-group">
                        <label for="status">Status</label>
                        <select name="status" id="status" class="form-control">
                            <option value="">Select...</option>
                            <option @if( $reward->status == 'Active' ) selected @endif value="1">Active</option>
                            <option @if( $reward->status == 'Inactive' ) selected @endif value="0">Inactive</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="comments">Comments</label>
                        <textarea name="comments" id="comments" cols="30" rows="10" class="form-control summernote">{!! $reward->comments !!}</textarea>
                    </div>
                </div>
            </div>
            <button class="btn btn-success pull-right">
                <i class="fa fa-save"></i> Save Changes
            </button>
        </form>
    </div>

@endsection