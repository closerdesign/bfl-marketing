@extends('layouts.app')

@section('content')

    <div class="container">
        <p class="text-right">
            <a href="{{ action('RewardsController@create') }}" class="btn btn-success">
                Create A New Reward
            </a>
        </p>
        <div class="panel panel-primary">
            <div class="panel-heading">
                Rewards
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-condensed">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>&nbsp;</th>
                            <th>Name</th>
                            <th>PLU Req.</th>
                            <th>Promo #</th>
                            <th>Brand</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Status</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        @foreach($rewards as $reward)
                        <tr>
                            <td>{{ $reward->id }}</td>
                            <td class="text-center">
                                <a href="{{ action('RewardsController@show', $reward->id) }}" target="_blank">
                                    <i class="fa fa-image"></i>
                                </a>
                            </td>
                            <th>
                                <a style="text-transform: uppercase" href="{{ action('RewardsController@edit', $reward->id) }}">
                                    {{ $reward->name }}
                                </a>
                            </th>
                            <td>
                                @If( $reward->plu_required > 0 )
                                    Yes @if($reward->plu !== 0)<b>[{{ $reward->plu }}]</b> @else <b>[Not Set]</b> @endif
                                @else
                                    No
                                @endif
                            </td>
                            <td>{{ $reward->promo_number }}</td>
                            <td>{{ $reward->brand->name }}</td>
                            <td>{{ $reward->start_date }}</td>
                            <td>{{ $reward->end_date }}</td>
                            <td>
                                <?php $label_status = ['Inactive' => 'warning', 'Active' => 'success'] ?>
                                <span class="label label-{{ $label_status[$reward->status] }}">
                                    {{ $reward->status }}
                                </span>
                            </td>
                            <td class="text-center">
                                <form action="{{ action('RewardsController@destroy', $reward->id) }}" method="post" onsubmit="return confirm('Are you sure?')">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button class="btn-xs btn-danger">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>

    @endsection