@extends('layouts.app')

@section('content')

    <div class="container">
        <form action="{{ action('RewardsController@store') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Create A New Reward
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="name" value="{{ old('name') }}" required >
                    </div>
                    <div class="form-group">
                        <label for="image">Image</label>
                        <input type="file" class="form-control" name="image">
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea name="description" id="description" cols="30" rows="10"
                                  class="form-control summernote">{!! old('description') !!}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="plu">PLU</label>
                        <input type="number" class="form-control" name="plu" value="{{ old('plu') }}" >
                    </div>
                    <div class="form-group checkbox">
                        <label><input type="checkbox" name="plu_required" value="1" > Is PLU Required?</label>
                    </div>
                    <div class="form-group">
                        <label for="promo_number">Promo Number</label>
                        <input type="text" class="form-control" name="promo_number" value="{{ old('promo_number') }}" >
                    </div>
                    <div class="form-group">
                        <label for="brand_id">Brand</label>
                        <select name="brand_id" id="brand_id" class="form-control" required >
                            <option value="">Select...</option>
                            @foreach(App\Brand::all() as $brand)
                                <option @if( old('brand_id') == $brand->id ) selected @endif value="{{ $brand->id }}">{{ $brand->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="start_date">Start Date</label>
                        <input type="date" class="form-control" name="start_date" value="{{ old('start_date') }}" required >
                    </div>
                    <div class="form-group">
                        <label for="end_date">End Date</label>
                        <input type="date" class="form-control" name="end_date" value="{{ old('end_date') }}" required >
                    </div>
                    <div class="form-group">
                        <label for="status">Status</label>
                        <select name="status" id="status" class="form-control">
                            <option value="">Select...</option>
                            <option value="1">Active</option>
                            <option value="0">Inactive</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="comments">Comments</label>
                        <textarea name="comments" id="comments" cols="30" rows="10" class="form-control summernote">{!! old('comments') !!}</textarea>
                    </div>
                </div>
            </div>
            <button class="btn btn-success pull-right">
                <i class="fa fa-save"></i> Save Changes
            </button>
        </form>
    </div>

    @endsection