@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <p class="lead">ROC (Retail Operations & Communications)</p>

        @foreach($rocs as $roc)
        <div class="panel panel-default">
            <div class="panel-heading">
                {{ date('F d, Y', strtotime($roc->date)) }}
            </div>
            <div class="panel-body">
                <ul>
                    @foreach($roc->entries as $entry)
                    <li>
                        <a href="{{ route('roc-entry.show', $entry->id) }}">
                            {{ $entry->name }}
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
        @endforeach
    </div>

    @endsection