@extends('layouts.app')

@section('content')

    <div class="container">
        <form action="{{ action('SignageController@deli_pdf') }}">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Generate Deli Sign
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="text">Product</label>
                    <input type="text" class="form-control" name="text" required >
                </div>
                <div class="form-group">
                    <label for="price">Price</label>
                    <input type="text" class="form-control" name="price" placeholder="Optional" >
                </div>
                <button class="btn btn-success btn-block">Generate</button>
            </div>
        </div>
        </form>
    </div>

    @endsection