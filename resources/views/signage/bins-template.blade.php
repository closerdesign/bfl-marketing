<html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet">

    <style>
        body{
            margin: 0;
            padding: 0;
            font-family: Arial, Helvetica, sans-serif;
        }

        .page{
            width: 1056px;
            height: 816px;
            background-size: cover;
            position: relative;
        }

        .content{
            width: 974px;
            height: 613px;
            position: absolute;
            top: 163px;
            left: 43px;
            display: flex;
            justify-content: center;
            flex-direction: column;
            text-align: center;
        }

        .product-name{
            font-size: 63.72px;
        }

        .product-description{
            font-size: 38.94px;
        }

        .price{
            font-family: 'Oswald', sans-serif;
            font-size: 240px;
            font-weight: bold;
        }

        .size{
            font-size: 30px;
        }
    </style>
</head>
<body>
    <div class="page" style="background-image: url(/img/85by11/{{ $background }}.jpg);">
        <div class="content">
            <div class="container">
                <div class="product-name">
                    {{ $product_name ?? '' }}
                </div>
                <div class="product-description">
                    {{ $product_description ?? '' }}
                </div>
                <div class="price">
                    {{ $price ?? '' }}
                </div>
                <div class="size">
                    {{ $size ?? '' }}
                </div>
            </div>
        </div>
    </div>
</body>
</html>