<html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Permanent+Marker&display=swap" rel="stylesheet">
    <style>
        body{
            margin: 0;
            padding: 0;
        }
        .container{
            width: 1224px;
            height: 1584px;
            display: flex;
            justify-content: center;
            flex-direction: column;
            margin: 0;
            padding: 0;
        }
        .sign{
            width: 504px;
            margin: 0 auto;
            height: 288px;
            background: black;
            color: white;
            text-align: center;
            display: flex;
            justify-content: center;
            flex-direction: column;
            font-family: 'Permanent Marker', cursive;
            font-size: 48px;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="sign">
            {!! $_GET['text'] !!}
        </div>
    </div>
</body>
</html>