@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading"><i class="fa fa-sticky-note-o"></i> @lang('forms.signage')</div>
            <div class="panel-body">
                <form action="{{ action('SignageController@store') }}" method="post" >
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">@lang('forms.full-name')</label>
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}" required >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">@lang('forms.email')</label>
                                <input type="text" class="form-control" name="email" value="{{ old('email') }}" required >
                            </div>
                        </div>
                    </div><hr />
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="store">@lang('forms.store')</label>
                                <select class="form-control" id="store" name="store">
                                    <option>@lang('forms.select')</option>
                                    @foreach(\App\Store::orderBy('store_code')->get() as $store)
                                        <option @if( old('store') == $store->store_code ) selected @endif value="{{ $store->store_code }}">{{ $store->store_code }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="event">@lang('forms.event-name')</label>
                                <input type="text" class="form-control" name="event" value="{{ old('event') }}" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="date_needed">@lang('forms.date-needed')</label>
                            <input type="text" class="form-control datepicker" name="date_needed" value="{{ old('date_needed') }}" required >
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="amount">@lang('forms.amount-needed')</label>
                                <input type="text" class="form-control" name="amount" value="{{ old('amount') }}" required >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="size">@lang('forms.size')</label>
                                <select class="form-control" id="size" name="size">
                                    <option>Select...</option>
                                    <option @if( old('size') == '4 ups/tag - Quarter Sheet' ) selected @endif value="4 ups/tag - Quarter Sheet">@lang('forms.qtr-sheet')</option>
                                    <option @if( old('size') == '2 ups - Half Sheet' ) selected @endif value="2 ups - Half Sheet">@lang('forms.half-sheet')</option>
                                    <option @if( old('size') == '1 up - Full Sheet' ) selected @endif value="1 up - Full Sheet">@lang('forms.full-sheet')</option>
                                    <option @if( old('size') == 'Legal (8.5x11)' ) selected @endif value="Legal (8.5x11)">@lang('forms.legal')</option>
                                    <option @if( old('size') == 'Tabloid (11x17)' ) selected @endif value="Tabloid (11x17)">@lang('forms.tabloid')</option>
                                    <option @if( old('size') == 'Other' ) selected @endif value="Other">@lang('forms.other')</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="receive">@lang('forms.receive')</label>
                                <select class="form-control" id="receive" name="receive">
                                    <option>@lang('forms.select')</option>
                                    <option @if( old('receive') == 'Courier - Monday' ) selected @endif value="Courier - Monday">@lang('forms.courier-mon')</option>
                                    <option @if( old('receive') == 'Courier - Thursday' ) selected @endif value="Courier - Thursday">@lang('forms.courier-thu')</option>
                                    <option @if( old('receive') == 'Email (pdf)' ) selected @endif value="Email (pdf)">@lang('forms.email-pdf')</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="other-size" style="display: none;">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="size_other">@lang('forms.size-other')</label>
                                <input type="text" id="other-txt" class="form-control" name="size_other" value="{{ old('size_other') }}" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="contents">@lang('forms.sign-content')</label>
                                <textarea class="form-control" rows="3" id="contents" name="contents">{{ old('contents') }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="comments">@lang('forms.adtl-comments')</label>
                                <textarea class="form-control" rows="3" id="comments" name="comments">{{ old('comments') }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button class="btn btn-success btn-block">@lang('forms.submit')</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>
        $('#size').change( function(){
            var x = $(this).val();

            if (x === 'Other') {
                $('#other-size').slideDown();
                $('#other-txt').prop('required', true);
            } else {
                $('#other-size').slideUp();
                $('#other-txt').removeAttr('required');
            }
        });
    </script>
@endsection