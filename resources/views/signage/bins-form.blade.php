@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Bins Signage
            </div>
            <div class="panel-body">
                <form action="{{ action('SignageController@bins_pdf') }}">
                    @csrf
                    <div class="form-group">
                        <label for="product_name">Product Name</label>
                        <input type="text" class="form-control" name="product_name" required >
                    </div>
                    <div class="form-group">
                        <label for="product_description">Product Description (Optional)</label>
                        <input type="text" class="form-control" name="product_description">
                    </div>
                    <div class="form-group">
                        <label for="price">Price</label>
                        <input type="text" class="form-control" name="price" required>
                    </div>
                    <div class="form-group">
                        <label for="size">Size</label>
                        <input type="text" class="form-control" name="size" required >
                    </div>
                    <div class="form-group">
                        <label for="background">Background</label>
                        <select name="background" id="background" class="form-control">
                            <option value="">Select...</option>
                            <option value="bfl-yellow">BFL Yellow</option>
                            <option value="organic">Organic</option>
                            <option value="bfl-sale">BFL Sale</option>
                            <option value="supermercado">Supermercado</option>
                            <option value="supermercado-organic">Supermercado Organic</option>
                            <option value="supermercado-sale">Supermercado Sale</option>
                            <option value="smartsaver">Smart Saver</option>
                        </select>
                    </div>
                    <button class="btn btn-success btn-block">Generate</button>
                </form>
            </div>
        </div>
    </div>

    @endsection