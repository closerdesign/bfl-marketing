@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <p class="text-right">
            <a href="{{ action('UsersController@create') }}" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add A New User</a>
        </p>
        <div class="panel panel-primary">
            <div class="panel-heading">Users List</div>
            <div class="panel-body">
                <form action="">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" class="form-control" name="keyword" required >
                            <div class="input-group-btn">
                                <button class="btn btn-success">
                                    <i class="fa fa-search"></i> @lang('general.search')
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="table-responsive">
                    <table class="table table-striped table-condensed">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Phone</th>
                            <th>Store</th>
                            <th>Status</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                        @foreach($users as $user)
                            <tr>
                                <th>
                                    <a href="{{ action('UsersController@edit', $user->id) }}">
                                        {{ $user->name }}
                                    </a>
                                </th>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->admin }}</td>
                                <td>{{ $user->phone }}</td>
                                <td class="text-center">{{ $user->store }}</td>
                                <td>{{ $user->status }}</td>
                                <td class="text-center">
                                    <form onsubmit="return confirm('Are you sure?')" action="{{ action('UsersController@destroy', $user->id) }}" method="post">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button class="btn-xs btn-danger">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @endsection