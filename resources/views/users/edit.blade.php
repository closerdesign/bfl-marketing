@extends('layouts.app')

@section('content')
    
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-primary">
                    <div class="panel-heading"><i class="fa fa-user"></i> User Profile</div>
                    <div class="panel-body">
                        <form action="{{ action('UsersController@update', $user->id) }}" method="post">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" value="{{ $user->name }}" required >
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" name="email" value="{{ $user->email }}" required >
                            </div>
                            <div class="form-group">
                                <label for="admin">Role</label>
                                <select name="admin" id="ad,om" class="form-control" required >
                                    <option value="">Select...</option>
                                    <option @if($user->admin == 'Admin') selected @endif value="1">Admin</option>
                                    <option @if($user->admin == 'Team') selected @endif value="0">Team</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="status">Status</label>
                                <select name="status" id="status" class="form-control" required >
                                    <option value="">Selected</option>
                                    <option @if($user->status == 'Approved') selected @endif value="1">Approved</option>
                                    <option @if($user->status == 'Pending Approval') selected @endif value="0">Pending Approval</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="store">Store</label>
                                <select name="store" id="store" class="form-control">
                                    <option value="">Select...</option>
                                    @foreach(\App\Store::orderBy('store_code')->get() as $store)
                                        <option @if($user->store == $store->store_code) selected @endif value="{{ $store->store_code }}">{{ $store->store_code }} - {{$store->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button class="btn-lg btn-success" type="submit">Save changes</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    @endsection