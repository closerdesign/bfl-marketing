@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">Add a new user</div>
            <div class="panel-body">
                <form action="{{ action('UsersController@store') }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="name" value="{{ old('name') }}" required >
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" name="email" value="{{ old('email') }}" required >
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone</label>
                        <input type="text" class="form-control" name="phone" value="{{ old('phone') }}" required >
                    </div>
                    <div class="form-group">
                        <label for="admin">Role</label>
                        <select name="admin" id="admin" class="form-control" required >
                            <option value="">Select...</option>
                            <option value="0">Team</option>
                            <option value="1">Admin</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="status">Status</label>
                        <select name="status" id="status" class="form-control" required >
                            <option value="">Select...</option>
                            <option value="1">Approved</option>
                            <option value="0">Pending Approval</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="store">Store</label>
                        <select name="store" id="store" class="form-control">
                            <option value="">Select...</option>
                            @foreach(\App\Store::orderBy('store_code')->get() as $store)
                                <option value="{{ $store->store_code }}">{{ $store->store_code }} - {{$store->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <button class="btn btn-primary">Save changes</button>
                </form>
            </div>
        </div>
    </div>

    @endsection