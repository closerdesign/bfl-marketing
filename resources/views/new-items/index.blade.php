@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="panel panel-primary">
                    <div class="panel-heading"><i class="fa fa-certificate"></i> New Items</div>

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Brand</th>
                                    <th>Title</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>

                                @if(count($items) < 1)
                                    <tr><td colspan="6" class="text-center text-muted">No New Items Added.</td></tr>
                                @endif
                                @foreach($items as $item)

                                    <tr>
                                        <td>
                                            <a href="{{ Storage::url('new-items/' . $item->image) }}" target="_blank">
                                                <img src="{{ Storage::url('new-items/' . $item->image) }}" style="height: 50px; width: 50px;" />
                                            </a>
                                        </td>
                                        <td>
                                            {{ $item->brands->name }}
                                        </td>
                                        <td>
                                            <a href="{{ action('NewItemsController@show', $item->id) }}">{{ $item->title }}</a>
                                        </td>
                                        <td>
                                            @if( $item->status == '1')
                                                <label class="label label-success">Published</label>
                                            @else
                                                <label class="label label-default">Unpublished</label>
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            <form onsubmit="return confirm('Are you sure? This can\'t be undone.')" action="{{ action('NewItemsController@destroy', $item->id) }}" method="post">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button class="btn-xs btn-danger" type="submit"><i class="fa fa-trash"></i></button>
                                            </form>
                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                            {{ $items->render() }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="panel panel-info">
                    <div class="panel-heading"><i class="fa fa-plus"></i> Add New Item</div>

                    <div class="panel-body">
                        <form action="{{ action('NewItemsController@store') }}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label>Brand</label>
                                    <select class="form-control" name="brand_id" required >
                                        <option value="" >Select...</option>
                                        @foreach(\App\Brand::orderBy('id')->get() as $brand)
                                            <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Image</label>
                                    <input type="file" class="form-control" name="image" required />
                                </div>
                                <div class="col-md-12 form-group">
                                    <label>Title</label>
                                    <input type="text" class="form-control" name="title" required />
                                </div>
                                <div class="col-md-12 form-group">
                                    <label>Description</label>
                                    <textarea class="form-control summernote" name="description" rows="5"></textarea>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label>Status</label>
                                    <select class="form-control" name="status" required >
                                        <option value="0">Unpublished</option>
                                        <option value="1">Published</option>
                                    </select>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label>&nbsp;</label>
                                    <button class="btn btn-block btn-success" type="submit">Upload</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>
        $('.summernote').summernote({
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']]
            ]
        });
    </script>
@endsection