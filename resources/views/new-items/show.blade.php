@extends('layouts.app')

@section('content')

    <div class="container">
        <p><button class="btn btn-primary" onclick="window.history.back()"><i class="fa fa-arrow-circle-left"></i> Go Back</button></p>
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-info">
                    <div class="panel-heading"><i class="fa fa-plus"></i> Add New Item</div>

                    <div class="panel-body">
                        <form action="{{ action('NewItemsController@update', $item->id) }}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label>Brand</label>
                                    <select class="form-control" name="brand_id" required >
                                        <option value="" >Select...</option>
                                        @foreach(\App\Brand::orderBy('id')->get() as $brand)
                                            <option value="{{ $brand->id }}" @if( $item->brand_id == $brand->id ) selected @endif>{{ $brand->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Image</label>
                                    <input type="file" class="form-control" name="image" />
                                </div>
                                <div class="col-md-12 form-group">
                                    <label>Title</label>
                                    <input type="text" class="form-control" name="title" value="{{ $item->title }}" required />
                                </div>
                                <div class="col-md-12 form-group">
                                    <label>Description</label>
                                    <textarea class="form-control summernote" name="description" rows="5">{{ $item->description }}</textarea>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label>Status</label>
                                    <select class="form-control" name="status" required >
                                        <option value="0" @if( $item->status == 0 ) selected @endif >Unpublished</option>
                                        <option value="1" @if( $item->status == 1 ) selected @endif >Published</option>
                                    </select>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label>&nbsp;</label>
                                    <button class="btn btn-block btn-success" type="submit"><i class="fa fa-save"></i> Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>
        $('.summernote').summernote({
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']]
            ]
        });
    </script>
@endsection