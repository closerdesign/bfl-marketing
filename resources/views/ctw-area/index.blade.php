@extends('layouts.app')

@section('content')

    <div class="container-fluid">

        <p class="lead">
            @lang('ctw-area.title')
            <span class="pull-right">
                <a href="{{ route('ctw-area.create') }}" class="btn btn-success">@lang('ctw-area.create')</a>
            </span>
        </p>

        <div>

            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                @php $i=0; @endphp
                @foreach($areas as $area)
                <li role="presentation" @if($i=0)class="active"@endif><a href="#tab{{ $area->id }}" aria-controls="home" role="tab" data-toggle="tab">{{ $area->name }}</a></li>
                @php $i++ @endphp
                @endforeach
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                @php $i=0; @endphp
                @foreach($areas as $area)
                <div role="tabpanel" class="tab-pane @if($i=0) active @endif" id="tab{{ $area->id }}">

                    <form action="{{ route('ctw-area.update', $area->id) }}" method="post">
                        @csrf
                        @method('PATCH')
                        <div class="form-group">
                            <label for="brands">@lang('ctw-area.brands')</label>
                            <div class="input-group">
                                <select name="brands[]" class="form-control multiple-select" multiple="multiple">
                                    @foreach( \App\Brand::orderBy('name')->get() as $brand )
                                        <option value="{{ $brand->id }}" @if( in_array($brand->id, $area->brands()->select('brand_id')->pluck('brand_id')->toArray()) ) selected @endif >{{ $brand->name }}</option>
                                    @endforeach
                                </select>
                                <span class="input-group-btn">
                                    <button class="btn btn-success"><i class="fa fa-refresh"></i> @lang('general.update')</button>
                                </span>
                            </div>
                        </div>
                    </form>

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <tbody>
                            @foreach($area->topics as $topic)
                                <tr>
                                    <td>
                                        <form action="{{ route('ctw-topic.update', $topic->id) }}" method="post">
                                            @csrf
                                            @method('PATCH')
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="topic" value="{{ $topic->topic }}" required >
                                                <div class="input-group-btn">
                                                    <button class="btn btn-success">
                                                        <i class="fa fa-refresh"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    <form action="{{ route('ctw-topic.store', $area->id) }}" method="post">
                        @csrf
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" class="form-control" name="topic" placeholder="@lang('ctw-topic.topic')" required >
                                <div class="input-group-btn">
                                    <button class="btn btn-success">@lang('general.save')</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                @php $i++ @endphp
                @endforeach
            </div>

        </div>

    </div>

    @endsection