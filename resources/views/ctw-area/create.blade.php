@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <p class="lead">@lang('ctw-area.create')</p>
    </div>

    <hr>

    <div class="container-fluid">
        <form action="{{ route('ctw-area.store') }}" method="post">
            @csrf
            <div class="form-group">
                <label for="name">@lang('ctw-area.name')</label>
                <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="@lang('ctw-area.name')" required >
            </div>
            <div class="form-group">
                <label for="brands">@lang('brands.title')</label>
                <select name="brands[]" id="brands" class="form-control multiple-select" multiple="multiple">
                    @foreach( \App\Brand::orderBy('name')->get() as $brand )
                        <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                    @endforeach
                </select>
            </div>
            <button class="btn btn-success">
                <i class="fa fa-save"></i> @lang('general.save')
            </button>
        </form>
    </div>

    @endsection