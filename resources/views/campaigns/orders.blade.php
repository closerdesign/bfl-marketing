@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Campaigns Orders</div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped datatable">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Store</th>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>Total</th>
                                    <th>Method</th>
                                    <th>Pmt Status</th>
                                    <th>Campaign</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders as $order)
                                <tr>
                                    <td>
                                        <a href="{{ action('CampaignsController@order_details', $order->id) }}">
                                            <i class="fa fa-search"></i>
                                        </a> {{ $order->id }}
                                    </td>
                                    <td>{{ $order->name }}</td>
                                    <td class="text-center">{{ $order->phone }}</td>
                                    <td class="text-center">
                                        {{ $order->email }}
                                        <hr>
                                        <ul>
                                            @foreach($order->products as $product)
                                            <li>{!! $product->name !!} x {{ $product->qty }}</li>
                                            @endforeach
                                        </ul>
                                    </td>
                                    <td class="text-center">{{ $order->pickup }}</td>
                                    <td class="text-center">{{ $order->pickup_date }}</td>
                                    <td class="text-center">{{ $order->pickup_time }}</td>
                                    <td class="text-center">{{ $order->total }}</td>
                                    <td class="text-center">{{ $order->payment_method }}</td>
                                    <td class="text-center">@if( $order->payment_status == 1 ) Paid @else Unpaid @endif</td>
                                    <td class="text-center">{{ $order->campaign }}</td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection