@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <p class="text-right"><a href="{{ action('CampaignsController@create') }}" class="btn btn-success"><i class="fa fa-plus-circle"></i> Create Campaign</a></p>
        <div class="panel panel-default">
            <div class="panel-heading">Campaigns</div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-condensed table-striped">
                        <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th>Name</th>
                            <th>URL</th>
                            <th>Brand</th>
                            <th>End Date</th>
                            <th>Pickup Deadline</th>
                            <th>Status</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($campaigns as $campaign)
                            <tr>
                                <td>
                                    <div class="btn-group">
                                        <button class="btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-chevron-down"></i> Actions
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="https://{{ $campaign->url }}" target="_blank">
                                                    @lang('campaigns.view-campaign')
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{ action('CampaignsController@replicate', $campaign->id) }}">
                                                    @lang('campaigns.replicate')
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" onclick="$('#delete-{{ $campaign->id }}').submit();">
                                                    @lang('campaigns.delete')
                                                    <form id="delete-{{ $campaign->id }}" onsubmit="return confirm('Are you sure? This can\'t be undone.')" action="{{ action('CampaignsController@destroy', $campaign->id) }}" method="post">
                                                        {{ csrf_field() }}
                                                        {{ method_field('DELETE') }}
                                                    </form>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                                <th>
                                    <a href="{{ action('CampaignsController@edit', $campaign->id) }}">
                                        {{ $campaign->name }}
                                    </a>
                                </th>
                                <td>{{ $campaign->url }}</td>
                                <td>{{ $campaign->brands->name }}</td>
                                <td>{{ $campaign->end_date }}</td>
                                <td>{{ $campaign->pickup_deadline }}</td>
                                <td>
                                    <span class="label @if( $campaign->status == 'Active') label-success @else label-danger @endif text-uppercase">
                                        {{ $campaign->status }}
                                    </span>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @endsection