@extends('layouts.app')

@section('content')

    <div class="container">

        <p class="text-right">
            <img width="200" src="{{ $campaign->background_image }}" alt="{{ $campaign->name }}"
                 class="img-responsive img-rounded">
        </p>

        <form action="{{ action('CampaignsController@update', $campaign->id) }}" method="post" enctype="multipart/form-data">
            <div class="panel panel-primary">
                <div class="panel-heading">Edit Campaign: {{ $campaign->name }}</div>
                <div class="panel-body">

                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}

                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="name" value="{{ $campaign->name }}" required />
                    </div>
                    <div class="form-group">
                        <label for="url">URL</label>
                        <input type="text" class="form-control" name="url" value="{{ $campaign->url }}" required />
                    </div>
                    <div class="form-group">
                        <label for="end_date">End Date (Optional)</label>
                        <input type="text" class="form-control datepicker" name="end_date" value="{{ $campaign->end_date }}">
                    </div>
                    <div class="form-group">
                        <label for="background_image">Background Image (2560 x 1440)</label>
                        <input type="file" class="form-control" name="background_image" />
                    </div>
                    <div class="form-group">
                        <label for="primary_color">Primary Color</label>
                        <input type="text" class="form-control colorpicker" name="primary_color" value="{{ $campaign->primary_color }}" required />
                    </div>
                    <div class="form-group">
                        <label for="secondary_color">Secondary Color</label>
                        <input type="text" class="form-control colorpicker" name="secondary_color" value="{{ $campaign->secondary_color }}" required >
                    </div>
                    <div class="form-group">
                        <label for="status">Status</label>
                        <select name="status" id="status" class="form-control" required >
                            <option value="">Select...</option>
                            <option @if( $campaign->status == "Active" ) selected @endif value="1">Active</option>
                            <option @if( $campaign->status == "Inactive" ) selected @endif value="0">Inactive</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="brand_id">Brands</label>
                        <select name="brand_id" id="brand_id" class="form-control" required >
                            <option value="">Select...</option>
                            @foreach(App\Brand::all() as $brand)
                                <option @if( $campaign->brand_id == $brand->id ) selected @endif value="{{ $brand->id }}" >{{ $brand->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="stores">Stores</label>
                        <select name="stores[]" id="stores" class="form-control multiple-select stores_select" multiple required >
                            @foreach(\App\Store::orderBy('store_code')->get() as $store)
                                <option @if(in_array($store->id, $campaign->stores()->select('id')->pluck('id')->toArray())) selected @endif value="{{ $store->id }}">{{ $store->store_code }} {{ $store->name }} [{{ $store->brands->name }}]</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea name="description" id="description" cols="30" rows="5"
                                  class="form-control">{{ $campaign->description }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="pickup_start_time">Pickup Start Time</label>
                        <input type="text" class="form-control timepicker" name="pickup_start_time" value="{{ $campaign->pickup_start_time }}" required >
                    </div>
                    <div class="form-group">
                        <label for="pickup_end_time">Pickup End Time</label>
                        <input type="text" class="form-control timepicker" name="pickup_end_time" value="{{ $campaign->pickup_end_time }}" required >
                    </div>
                    <div class="form-group">
                        <label for="prior_notice">Prior Notice (Days)</label>
                        <input type="number" class="form-control text-center" name="prior_notice" value="{{ $campaign->prior_notice }}" required >
                    </div>
                    <div class="form-group">
                        <label for="pickup_start_date">Pickup Start Date</label>
                        <input type="text" class="form-control datepicker" name="pickup_start_date" value="{{ $campaign->pickup_start_date }}" >
                    </div>
                    <div class="form-group">
                        <label for="pickup_deadline">Pickup Deadline [Optional]</label>
                        <input type="text" class="form-control datepicker" name="pickup_deadline" value="{{ $campaign->pickup_deadline }}" >
                    </div>
                    <div class="form-group">
                        <label for="pickup_deadline_time">Pickup Deadline Time [Optional]</label>
                        <input type="text" class="form-control timepicker" name="pickup_deadline_time" value="{{ $campaign->pickup_deadline_time }}" >
                    </div>
                    <div class="form-group">
                        <label for="terms">Terms</label>
                        <textarea name="terms" id="terms" cols="30" rows="10"
                                  class="form-control summernote">{{ $campaign->terms }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="tag_manager_head">Tag Manager Head Snippet</label>
                        <textarea name="tag_manager_head" id="tag_manager_head" cols="30" rows="10"
                                  class="form-control">{{ $campaign->tag_manager_head }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="tag_manager_body">Tag Manager Body Snippet</label>
                        <textarea name="tag_manager_body" id="tag_manager_body" cols="30" rows="10"
                                  class="form-control">{{ $campaign->tag_manager_body }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="warning">Warning</label>
                        <textarea name="warning" id="warning" cols="30" rows="10" class="form-control">{!! $campaign->warning !!}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="offline_mode_list">Offline Mode List</label>
                        <input type="text" class="form-control" name="offline_mode_list" value="{{ $campaign->offline_mode_list }}">
                    </div>
                    <div class="form-group">
                        <label for="template">@lang('campaigns.template')</label>
                        <select name="template" id="template" class="form-control">
                            <option @if($campaign->template == null) selected @endif value="">@lang('campaigns.default-template')</option>
                            <option @if($campaign->template == 'classes') selected @endif value="classes">@lang('campaigns.classes-template')</option>
                        </select>
                    </div>
                </div>
            </div>
            <button class="btn btn-success" type="submit">
                <i class="fa fa-save"></i> Save changes
            </button>
        </form>

        <hr>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Categories</div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-condensed">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Campaigns</th>
                                    <th>Status</th>
                                    <th>&nbsp;</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($campaign->categories as $category)
                                <tr>
                                    <th>
                                        <a href="{{ action('CampaignCategoriesController@edit', $category->id) }}">
                                            {{ $category->name }}
                                        </a>
                                    </th>
                                    <td>
                                        @foreach($category->campaigns as $campaign)
                                            [{{ $campaign->name }}]
                                            @endforeach
                                    </td>
                                    <td class="text-center">{{ $category->status }}</td>
                                    <td>
                                        <form action="{{ action('CampaignCategoriesController@destroy', $category->id) }}" method="post" onsubmit="return confirm('Are you sure? This can\'t be undone!')">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button class="btn-xs btn-danger" type="submit"><i class="fa fa-trash"></i></button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    @endsection

@section('js')

    <script>
        $('#brand_id').change(function(){
            var brand = $(this).val();
            $.get('/brand/stores/' + brand, function(data){
                $('#stores').html('');
                $.each(data, function(i, item){
                    $('#stores').append('<option value=' + item['id'] + '>[' + item['store_code'] + '] ' + item['name'] + '</option>');
                })
            });
        });
    </script>

@endsection