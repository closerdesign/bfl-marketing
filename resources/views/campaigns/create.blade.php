@extends('layouts.app')

@section('content')

    <div class="container">
        <form action="{{ action('CampaignsController@store') }}" method="post" enctype="multipart/form-data">
            <div class="panel panel-primary">
                <div class="panel-heading">Create Campaign</div>
                <div class="panel-body">

                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}" required />
                            </div>
                            <div class="form-group">
                                <label for="url">URL</label>
                                <input type="text" class="form-control" name="url" value="{{ old('url') }}" required />
                            </div>
                            <div class="form-group">
                                <label for="end_date">End Date [Optional]</label>
                                <input type="text" class="form-control datepicker" name="end_date" required >
                            </div>
                            <div class="form-group">
                                <label for="background_image">Background Image (2560 x 1440)</label>
                                <input type="file" class="form-control" name="background_image" required />
                            </div>
                            <div class="form-group">
                                <label for="primary_color">Primary Color</label>
                                <input type="text" class="form-control colorpicker" name="primary_color" value="{{ old('primary_color') }}" required />
                            </div>
                            <div class="form-group">
                                <label for="secondary_color">Secondary Color</label>
                                <input type="text" class="form-control colorpicker" name="secondary_color" value="{{ old('secondary_color') }}" required >
                            </div>
                            <div class="form-group">
                                <label for="status">Status</label>
                                <select name="status" id="status" class="form-control" required >
                                    <option value="">Select...</option>
                                    <option @if( old('status') == 1 ) selected @endif value="1">Active</option>
                                    <option @if( old('status') == 0 ) selected @endif value="0">Inactive</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="brand_id">Brand</label>
                                <select name="brand_id" id="brand_id" class="form-control" required >
                                    <option value="">Select...</option>
                                    @foreach(\App\Brand::all() as $brand)
                                        <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="stores">Stores</label>
                                <select name="stores[]" id="stores" class="form-control multiple-select stores_select" multiple required >
                                    <option value="">Select...</option>
                                    @foreach(\App\Store::orderBy('store_code')->get() as $store)
                                        <option value="{{ $store->id }}">{{ $store->store_code }} {{ $store->name }} [{{ $store->brands->name }}]</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea name="description" id="description" cols="30" rows="5"
                                          class="form-control">{{ old('description') }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="pickup_start_time">Pickup Start Time</label>
                                <input type="text" class="form-control timepicker" name="pickup_start_time" value="{{ old('pickup_start_time') }}" required >
                            </div>
                            <div class="form-group">
                                <label for="pickup_end_time">Pickup End Time</label>
                                <input type="text" class="form-control timepicker" name="pickup_end_time" value="{{ old('pickup_end_time') }}" required >
                            </div>
                            <div class="form-group">
                                <label for="prior_notice">Prior Notice (Days)</label>
                                <input type="number" class="form-control" name="prior_notice" value="{{ old('prior_notice') }}" required >
                            </div>
                            <div class="form-group">
                                <label for="pickup_start_date">Pickup Start Date</label>
                                <input type="text" class="form-control datepicker" name="pickup_start_date" value="{{ old('pickup_start_date') }}" >
                            </div>
                            <div class="form-group">
                                <label for="pickup_deadline">Pickup Deadline [Optional]</label>
                                <input type="text" class="form-control datepicker" name="pickup_deadline" value="{{ old('pickup_deadline') }}" >
                            </div>
                            <div class="form-group">
                                <label for="pickup_deadline_time">Pickup Deadline Time [Optional]</label>
                                <input type="text" class="form-control timepicker" name="pickup_deadline_time" value="{{ old('pickup_deadline_time') }}" >
                            </div>
                            <div class="form-group">
                                <label for="terms">Terms</label>
                                <textarea name="terms" id="terms" cols="30" rows="10"
                                          class="form-control summernote">{{ old('terms') }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="tag_manager_head">Tag Manager Head Snippet</label>
                                <textarea name="tag_manager_head" id="tag_manager_head" cols="30" rows="10"
                                          class="form-control">{{ old('tag_manager_head') }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="tag_manager_body">Tag Manager Body Snippet</label>
                                <textarea name="tag_manager_body" id="tag_manager_body" cols="30" rows="10"
                                          class="form-control">{{ old('tag_manager_body') }}</textarea>
                            </div>

                            <div class="form-group">
                                <label for="warning">Warning</label>
                                <textarea name="warning" id="warning" cols="30" rows="10"
                                          class="form-control">{!! old('warning') !!}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="offline_mode_list">Offline Mode List</label>
                                <input type="text" class="form-control" name="offline_mode_list" value="{{ old('offline_mode_list') }}">
                            </div>
                            <div class="form-group">
                                <label for="template">@lang('campaigns.template')</label>
                                <select name="template" id="template" class="form-control">
                                    <option value="">@lang('campaigns.default-template')</option>
                                    <option value="classes">@lang('campaigns.classes-template')</option>
                                </select>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <button class="btn btn-success pull-right" type="submit"><i class="fa fa-save"></i> Save changes</button>
        </form>
    </div>

    @endsection

@section('js')

    <script>
        $('#brand_id').change(function(){
            var brand = $(this).val();
            $.get('/brand/stores/' + brand, function(data){
                $('#stores').html('');
                $.each(data, function(i, item){
                    $('#stores').append('<option value=' + item['id'] + '>[' + item['store_code'] + '] ' + item['name'] + '</option>');
                })
            });
        });
    </script>

    @endsection