@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Order Details - Order No. {{ $order->id }}</div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <tr>
                                    <th>Name</th>
                                    <td>{{ $order->name }}</td>
                                    <th>Phone</th>
                                    <td>{{ $order->phone }}</td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td>{{ $order->email }}</td>
                                    <th>Store</th>
                                    <td>{{ $order->pickup }}</td>
                                </tr>
                                <tr>
                                    <th>Date</th>
                                    <td>{{ $order->pickup_date }}</td>
                                    <th>Time</th>
                                    <td>{{ $order->pickup_time }}</td>
                                </tr>
                                <tr>
                                    <th>Total Order</th>
                                    <td>{{ $order->total }}</td>
                                    <th>Payment Method</th>
                                    <td>{{ $order->payment_method }}</td>
                                </tr>
                                <tr>
                                    <th>Payment Status</th>
                                    <td>@if($order->payment_status == 1) Paid @else Unpaid @endif</td>
                                    <th>Created</th>
                                    <td>{{ $order->created_at }}</td>
                                </tr>
                                <tr>
                                    <th colspan="4">Comments</th>
                                </tr>
                                <tr>
                                    <td colspan="4">{{ $order->comments }}</td>
                                </tr>
                            </table>
                        </div>
                        <p class="lead">Product Details</p>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Qty</th>
                                    <th>Price</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($order->products as $product)
                                <tr>
                                    <td>{!! $product->name !!}</td>
                                    <td>{{ $product->qty }}</td>
                                    <td>{{ $product->price }}</td>
                                    <td>{{ $product->total }}</td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <p>
                            <a href="{{ action('CampaignsController@orders') }}" class="btn btn-default">
                                <i class="fa fa-arrow-circle-left"></i> Go back to the list
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection