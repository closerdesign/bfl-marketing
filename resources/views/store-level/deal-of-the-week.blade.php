@extends('layouts.app')

@section('content')

    <div class="container">
        <form action="{{ action('StoreLevelController@deal_of_the_week_pdf') }}" method="post">
            {{ csrf_field() }}
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Create Coupon Poster / 4-Ups
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="brand_id">@lang('store-level.brand')</label>
                        <select name="brand_id" id="brand-id" class="form-control">
                            <option value="">@lang('general.select')</option>
                            @foreach(\App\Brand::all() as $brand)
                                <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="coupons">@lang('store-level.coupon') (@lang('general.optional'))</label>
                        <select name="coupons" id="coupons" class="form-control">
                            <option value="">@lang('general.select')</option>
                        </select>
                        <label>@lang('store-level.populate-from-coupon')</label>
                    </div>
                    <div class="form-group">
                        <label for="name">@lang('store-level.product-name')</label>
                        <input type="text" id="name" class="form-control" name="name" placeholder="i.e. Gala Apples" required >
                    </div>
                    <div class="form-group">
                        <label for="price">@lang('store-level.price')</label>
                        <input type="text" id="price" class="form-control" name="price" placeholder="i.e. $2.99" required>
                    </div>
                    <div class="form-group">
                        <label for="unit_of_measure">@lang('store-level.unit-of-measure')</label>
                        <input type="text" id="unit" class="form-control" name="unit_of_measure" placeholder="i.e. LB" >
                    </div>
                    <div class="form-group">
                        <label for="fineprint">@lang('store-level.fineprint')</label>
                        <input type="text" id="fineprint" class="form-control" name="fineprint">
                    </div>
                    <div class="form-group">
                        <label for="background">@lang('store-level.background')</label>
                        <select name="background" id="background" class="form-control">
                            <option value="deal.jpg">@lang('store-level.default')</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>@lang('store-level.color')</label>
                        <div class="checkbox">
                            <label class="radio-inline"><input type="radio" name="color" value="black" checked>@lang('store-level.black')</label>
                            <label class="radio-inline"><input type="radio" name="color" value="white">@lang('store-level.white')</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>@lang('store-level.type')</label>
                        <div class="checkbox">
                            <label class="radio-inline"><input type="radio" name="type" value="Poster" checked>Poster</label>
                            <label class="radio-inline"><input type="radio" name="type" value="4ups">4-Ups</label>
                        </div>
                    </div>
                    <button class="btn btn-success form-control">Generate</button>
                </div>
            </div>
        </form>
    </div>

@endsection

@section('js')

    <script>
        var coupons = [];

        $('#brand-id').change( function(){
            $('#coupons').find('option').remove().end().append('<option value="">Select...</option>').val('');
            var selected = $(this).val();
            $.get('https://marketing.buyforlessok.com/loyalty-coupons/' + selected, function(data){
                coupons = JSON.parse(data);
                coupons = coupons.Offers;
                coupons.forEach(function(offer, index) {
                    $('<option>').val(index).text(offer.ProductName + ' - ' + offer.Title).appendTo('#coupons');
                });
            });
        });

        $('#coupons').change( function(){
            var id = $(this).val();
            var selectedCoupon = coupons[id];
            var date = selectedCoupon.ExpiresOn;
            var ms = date.substring(date.lastIndexOf("(") + 1, date.lastIndexOf("+"));
            ms = parseInt(ms);
            var formatted = new Date(ms);
            var day = formatted.getDate();
            var mon = formatted.getMonth() + 1;
            var year = formatted.getFullYear();
            var exp =  mon + '/' + day + '/' + year;
            $('#name').val(selectedCoupon.ProductName);
            $('#price').val(selectedCoupon.Title);
            $('#fineprint').val(selectedCoupon.Details + '.  Expires ' + exp);
        });

    </script>

@endsection