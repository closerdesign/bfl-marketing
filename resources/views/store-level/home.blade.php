@extends('layouts.store-level')

@section('store-level-content')

    <div class="container-fluid">
        <p class="lead">@lang('store-level.assignments')</p>

        <ul class="list-group">
            @if( $rocEntries->count() === 0 )
            <li class="list-group-item">
                <i>@lang('roc-entry.you-have-no-assignments-at-this-time')</i>
            </li>
            @endif
            @foreach($rocEntries as $entry)
                <li class="list-group-item">
                    <span class="label {{ $entry->store_status_label(auth()->user()->store_data->id) }} text-uppercase">{{ $entry->store_status(auth()->user()->store_data->id) }}</span> <a href="{{ route('roc-entry.show', $entry->id) }}">{{ $entry->name }}</a>
                </li>
            @endforeach
        </ul>

        <p class="lead">@lang('store-level.current-ads')</p>

        <ul class="list-group">
            @foreach($ads as $ad)
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $ad->id }}" aria-expanded="true" aria-controls="collapse{{ $ad->id }}">
                                    {{ $ad->name }}
                                </a>
                                <span class="badge pull-right">{{ date('F d', strtotime($ad->date_from)) }} to {{ date('F d', strtotime($ad->date_to)) }}</span>
                            </h4>
                        </div>
                        <div id="collapse{{ $ad->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{ $ad->id }}">
                            <div class="panel-body">
                                @if( $ad->date_from > date('Y-m-d') )
                                <div class="alert alert-info">
                                    <i class="fa fa-info"></i> @lang('store-level.please-note-that-future-ads-are-subject-to-change')
                                </div>
                                @endif
                                <div class="row">
                                    @foreach($ad->specials as $special)
                                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 form-group">
                                            <div class="panel panel-default">
                                                <div class="panel-heading" style="height: 75px; overflow: auto;">
                                                    <b>{!! $special->item_description !!}</b>
                                                </div>
                                                <div class="panel-body" style="overflow: auto; height: 220px;">
                                                    <div class="row">
                                                        <div class="col-md-3 col-md-offset-0 col-sm-6 col-sm-offset-0 col-xs-6 col-xs-offset-3">
                                                            <img src="/img/dummy.png" alt="{{ $special->item_description }}" style="background: url({{ $special->image }}); background-size: contain; background-repeat: no-repeat;" class="form-group img-responsive img-rounded">
                                                        </div>
                                                        <div class="col-md-9 col-sm-4 col-xs-12 ">
                                                            <p class="lead text-right">
                                                                {!! $special->price_tag !!} {{ $special->size }}
                                                            </p>
                                                            <p>
                                                                <b>Image UPC</b><br />
                                                                {{ $special->sku }}
                                                            </p>
                                                            @if($special->additional_upcs != "")
                                                                <p>
                                                                    <b>Additional UPCs</b><br />
                                                                    {{ $special->additional_upcs }}
                                                                </p>
                                                            @endif
                                                            @if($special->comments != "")
                                                                <p>
                                                                    <b>Comments</b><br />
                                                                    {{ $special->comments }}
                                                                </p>
                                                            @endif

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </ul>

        <p class="lead">@lang('store-level.marketing')</p>

        <ul class="list-group">
            <li class="list-group-item"><a href="{{ action('MailingListsController@index') }}">@lang('store-level.add-email-subscriber')</a></li>
        </ul>

        <p class="lead">@lang('store-level.signage')</p>

        <ul class="list-group">
            <li class="list-group-item">
                <a href="{{ action('StoreLevelController@deal_of_the_week') }}">
                    @lang('store-level.signage-creation')
                </a>
            </li>
            <li class="list-group-item">
                <a href="{{ action('StoreLevelController@signage_request') }}">
                    @lang('store-level.signage-request')
                </a>
            </li>
            <li class="list-group-item">
                <a href="{{ action('StoreLevelController@info_signs') }}">
                    @lang('store-level.informational-signage')
                </a>
            </li>
        </ul>

        <p class="lead">@lang('store-level.general-requests')</p>

        <ul class="list-group">
            <li class="list-group-item"><a href="{{ action('StoreLevelController@bg_orientation_request') }}">@lang('nav.orientation')</a></li>
            <li class="list-group-item"><a href="{{ action('EmployeesController@discount_request') }}">@lang('nav.discount-request')</a></li>
        </ul>

        <p class="lead">@lang('nav.resources')</p>

        <ul class="list-group">
            <li class="list-group-item"><a href="{{ action('DocumentsController@index') }}">@lang('nav.documents')</a></li>
        </ul>
    </div>

    @endsection