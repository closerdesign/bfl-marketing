@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">@lang('forms.orientation')</div>
            <div class="panel-body">
                <form action="{{ action('StoreLevelController@form') }}" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="form_name" value="Orientation Attendee">
                    <h3>@lang('forms.new-hire-info')</h3>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.full-name')</label>
                            <input type="text" class="form-control" name="name" required />
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.nickname')</label>
                            <input type="text" class="form-control" name="nickname" />
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.orientation-date')</label>
                            <input type="date" class="form-control" name="orientation_date" required />
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.orientation-time')</label>
                            <input type="time" class="form-control" name="orientation_time" required />
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.contact-phone')</label>
                            <input type="text" class="form-control" name="phone" required />
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.job-title')</label>
                            <input type="text" class="form-control" name="title" required />
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.pay-rate')</label>
                            <input type="text" class="form-control" name="pay_rate" required />
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.orien-rehire')</label>
                            <select class="form-control" name="rehire" required >
                                <option value="">@lang('forms.select')</option>
                                <option value="Yes">@lang('forms.yes')</option>
                                <option value="No">@lang('forms.no')</option>
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.drug-date')</label>
                            <input type="date" class="form-control" name="drug_date" />
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.drug-time')</label>
                            <input type="time" class="form-control" name="drug_time" />
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.work-status')</label>
                            <select class="form-control" name="status" required >
                                <option value="">@lang('forms.select')</option>
                                <option value="Full Time Plus (38 or more hours weekly)">@lang('forms.ft-plus')</option>
                                <option value="Full Time (30 - 37 hours weekly)">@lang('forms.ft')</option>
                                <option value="Part Time (Less than 30 hours weekly)">@lang('forms.pt')</option>
                            </select>
                        </div>
                        <div class="col-md-12">&nbsp;</div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.identification')</label>
                            <select class="form-control" name="identification" required >
                                <option value="">@lang('forms.select')</option>
                                <option value="List A">@lang('forms.list-a')</option>
                                <option value="List B and C">@lang('forms.list-b')</option>
                                <option value="Other">@lang('forms.list-other')</option>
                            </select><br />
                            <label>@lang('forms.details')</label>
                            <input type="text" class="form-control" name="id_details" />
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.language')</label>
                            <select class="form-control" name="language" required >
                                <option value="">@lang('forms.select')</option>
                                <option value="English Primary Language">@lang('forms.eng-primary')</option>
                                <option value="Spanish Primary Language">@lang('forms.esp-primary')</option>
                                <option value="Bilingual; understands English clearly">@lang('forms.bilingual')</option>
                                <option value="Other">@lang('forms.lang-other')</option>
                            </select><br />
                            <label>@lang('forms.details')</label>
                            <input type="text" class="form-control" name="language_details" />
                        </div>
                    </div>
                    <h3>@lang('forms.reporting-inst')</h3>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.report-date')</label>
                            <input type="date" class="form-control" name="report_date" required />
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.report-time')</label>
                            <input type="time" class="form-control" name="report_time" required />
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.report-to')</label>
                            <input type="text" class="form-control" name="report_to" required />
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.store')</label>
                            <select class="form-control" name="store" required >
                                <option value="">@lang('forms.select')</option>
                                <option value="1006" >1006</option>
                                <option value="1201" >1201</option>
                                <option value="1205" >1205</option>
                                <option value="1230" >1230</option>
                                <option value="2001" >2001</option>
                                <option value="2701" >2701</option>
                                <option value="3501" >3501</option>
                                <option value="3701" >3701</option>
                                <option value="3713" >3713</option>
                                <option value="4150" >4150</option>
                                <option value="4424" >4424</option>
                                <option value="7957" >7957</option>
                                <option value="9515" >9515</option>
                                <option value="0" >Support Center</option>
                            </select>
                        </div>
                        <div class="col-md-12 form-group">
                            <button class="btn btn-success form-control">@lang('forms.submit')</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection