<html>
<head>
    <style>
        @import url('https://fonts.googleapis.com/css?family=Oswald&display=swap');
        body {
            margin: 0;
            padding: 0;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        }
        .container {
            position: relative;
            border: 8px solid #CCC;
            @if($_GET['landscape'] !== '1')
                width: 850px;
                height: 1104px;
            @else
                width: 1100px;
                height: 846px;
            @endif
        }
        .first-line {
            font-family: 'Oswald', sans-serif;
            font-weight: bold;
            line-height: 1em;
            font-size: 96px;
            text-align: center;
            padding: 10px;
            @if($_GET['type'] == 'Warning')
                color: #C30D2E;
            @elseif($_GET['type'] == 'Notice')
                color: #253f8f;
            @else
                color: #000;
            @endif
        }
        .second-line {
            font-family: 'Arial', sans-serif;
            text-align: center;
            font-size: 45px;
            padding: 10px;
        }
        .break {
            text-align: center;
            width: 35%;
            border: 3px solid #000;
        }
        .header {
            position: relative;
            top: 0;
            left: 0;
            width: 100%;
            text-align: center;
            font-size: 96px;
            font-weight: bold;
            color: #fff;
            @if($_GET['type'] == 'Warning')
                background-color: #C30D2E;
            @elseif($_GET['type'] == 'Notice')
                background-color: #253f8f;
            @else
                background-color: #fff;
            @endif
        }
        .footer {
            position: absolute;
            bottom: -8px;
            width: 100%;
            text-align: center;
        }

        .footer > img {
            background-color: #eee;
            height: 100px;
            margin: auto 0;
            padding: 15px;
        }
    </style>
</head>
<body>
<div class="container">
    @if($_GET['type'] !== 'Info')
        <div class="header">
            @if($_GET['type'] == 'Warning')
                ATTENTION
            @elseif($_GET['type'] == 'Notice')
                NOTICE
            @endif
        </div>
    @endif
    <div class="content">
        <p class="first-line">{!! $_GET['first_line'] !!}</p>
        @if($_GET['second_line'])<hr class="break" />@endif
        <p class="second-line">{!! $_GET['second_line'] !!}</p>
    </div>
    <div class="footer">
        <img src="/img/logos/{{ $_GET['brand'] }}-logo.png" />
    </div>
</div>
</body>
</html>