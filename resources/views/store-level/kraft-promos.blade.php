@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <i class="fa fa-money"></i> Kraft Promos
            </div>
            <div class="panel-body">
                <form action="{{ action('StoreLevelController@kraft_promo_pdf') }}" enctype="multipart/form-data" method="post">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label>Item Name</label> <small>(Use &lt;br&gt; for new line)</small>
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="i.e. Kraft Original Mayo <br> or Miracle Whip" required />
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Item Size</label>
                            <input type="text" class="form-control" name="size" value="{{ old('size') }}" placeholder="i.e. 30 oz" required />
                        </div>
                        <div class="col-md-4 form-group">
                            <label>Price</label>
                            <input type="text" class="form-control" name="price" value="{{ old('price') }}" placeholder="i.e. $3.16" required />
                        </div>
                        <div class="col-md-4 form-group">
                            <label>Minus Amount</label>
                            <input type="text" class="form-control" name="minus" value="{{ old('minus') }}" placeholder="i.e. $1" required />
                        </div>
                        <div class="col-md-4 form-group">
                            <label>Total</label>
                            <input type="text" class="form-control" name="total" value="{{ old('total') }}" placeholder="i.e. $2.16" required />
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Image URL</label>
                            <input type="text" class="form-control" name="img_url" value="{{ old('img_url') }}" placeholder="i.e. https://bfl-corp-sara.s3.us-west-2.amazonaws.com/specials/59a46c4149770.png" />
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="image">Image Upload (optional)</label>
                            <input type="file" class="form-control" name="image">
                        </div>
                        <div class="col-md-2 text-right"><br />
                            <button type="submit" class="btn btn-success">Generate Promo</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection