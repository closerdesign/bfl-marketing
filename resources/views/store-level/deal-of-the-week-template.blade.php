<html>
<head>
    <style>
        body{
            margin: 0;
            padding: 0;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            color: {{ $_GET['color'] }};
        }
        h1{
            text-transform: capitalize;
        }
        .container{
            width: 1280px;
            max-width: 1280px;
            overflow: hidden;
            height: 1978px;
            margin: 0 auto;
            background-size: cover !important;
            background-repeat: no-repeat !important;
        }
        .container > .inner{
            width: 1280px;
            height: 1978px;
            position: relative;
        }
        .container > .inner > .item-block {
            position: absolute;
            width: 1270px;
            height: 1130px;
            top: 730px;
            display: flex;
            justify-content: center;
            flex-direction: column;
        }
        .container > .inner > .item-block > .name{
            text-align: center;
            line-height: 1;
            font-size: 125px;
            min-width: 100%;
            display: flex;
            justify-content: center;
            flex-direction: column;
        }
        .container > .inner > .item-block > .price{
            text-align: center;
            font-weight: bold;
            line-height: 1;
            font-size: 235px;
            padding: 20px;
            min-width: 100%;
        }
        .container > .inner > .item-block > .unit-of-measure{
            text-align: center;
            font-size: 75px;
            font-weight: bold;
            min-width: 100%;
        }
        .container > .inner > .fineprint{
            width: 1280px;
            text-align: center;
            bottom: 35px;
            position: absolute;
            font-size: 24px;
        }


    </style>
</head>
<body>
<div class="container" style="background: url(/img/dotw/{{ $_GET['brand'] }}-{{ $_GET['background'] }})">
    <div class="inner">
        <div class="item-block">
            @if('group' != strtolower($_GET['name']))
                <div class="name">{{ $_GET['name'] }}</div><br />
            @endif
            <div class="price">{{ $_GET['price'] }}</div><br />
            <div class="unit-of-measure">{{ $_GET['unit_of_measure'] }}</div>
        </div>
        @if( isset($_GET['fineprint']) && ($_GET['fineprint'] != "") )
            <div class="fineprint">{{ $_GET['fineprint'] }}</div>
        @endif
    </div>
</div>
</body>
</html>