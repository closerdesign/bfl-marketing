@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="panel panel-default">
            <div class="panel-heading">@lang('forms.orientation')</div>
            <div class="panel-body">
                <form action="{{ action('StoreLevelController@form') }}" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="form_name" value="Background Check / Orientation Attendee">
                    <div class="col-md-6 form-group">
                        <label>@lang('forms.full-name-bg')</label>
                        <input type="text" class="form-control" name="name" required />
                    </div>
                    <div class="col-md-6 form-group">
                        <label>@lang('forms.nickname')</label>
                        <input type="text" class="form-control" name="middle_name" />
                    </div>
                    <div class="col-md-6 form-group">
                        <label>@lang('forms.dob')</label>
                        <input type="date" class="form-control" name="dob" required />
                    </div>
                    <div class="col-md-6 form-group">
                        <label>@lang('forms.contact-phone')</label>
                        <input type="text" class="form-control" name="phone" required />
                    </div>
                    <div class="col-md-6 form-group">
                        <label>@lang('forms.drug-date')</label>
                        {{--<input type="date" class="form-control" name="drug_date" />--}}
                        <select class="form-control" name="drug_date" id="drug-date" required >
                            <option value="">@lang('forms.select')</option>
                            <option value="Monday">@lang('forms.monday')</option>
                            <option value="Friday">@lang('forms.friday')</option>
                        </select><br />
                        <label>@lang('forms.drug-time')</label><br />
                        <input type="hidden" name="drug_time" id="drug-time" value="" />
                        <span id="drug-time-label">Please select a date...</span>
                        {{--<input type="time" class="form-control" name="drug_time" />--}}
                    </div>
                    <div class="col-md-6 form-group">
                        <label>@lang('forms.job-title')</label>
                        <input type="text" class="form-control" name="title" required />
                    </div>
                    <div class="col-md-6 form-group">
                        <label>@lang('forms.pay-rate')</label>
                        <input type="text" class="form-control" name="pay_rate" required />
                    </div>
                    <div class="col-md-6 form-group">
                        <label>@lang('forms.orien-rehire')</label>
                        <select class="form-control" name="rehire" required >
                            <option value="">@lang('forms.select')</option>
                            <option value="Yes">@lang('forms.yes')</option>
                            <option value="No">@lang('forms.no')</option>
                        </select>
                    </div>
                    {{--<div class="col-md-6 form-group">--}}
                        {{--<label>@lang('forms.orientation-date')</label>--}}
                        {{--<input type="date" class="form-control" name="orientation_date" required />--}}
                    {{--</div>--}}
                    <div class="col-md-6 form-group">
                        <label>@lang('forms.work-status')</label>
                        <select class="form-control" name="status" required >
                            <option value="">@lang('forms.select')</option>
                            <option value="Full Time Plus (38 or more hours weekly)">@lang('forms.ft-plus')</option>
                            <option value="Full Time (30 - 37 hours weekly)">@lang('forms.ft')</option>
                            <option value="Part Time (Less than 30 hours weekly)">@lang('forms.pt')</option>
                        </select>
                    </div>
                    <div class="col-md-6 form-group">
                        <label>@lang('forms.language')</label>
                        <select class="form-control" name="language" required >
                            <option value="">@lang('forms.select')</option>
                            <option value="English Primary Language">@lang('forms.eng-primary')</option>
                            <option value="Spanish Primary Language">@lang('forms.esp-primary')</option>
                            <option value="Bilingual; understands English clearly">@lang('forms.bilingual')</option>
                            <option value="Other">@lang('forms.lang-other')</option>
                        </select>
                        <label style="padding-top: 15px;">@lang('forms.details')</label>
                        <input type="text" class="form-control" name="language_details" />
                    </div>
                    <div class="col-md-6 form-group">
                        <label>@lang('forms.report-date')</label>
                        <input type="date" class="form-control" name="available_start_date" required />
                    </div>
                    {{--<div class="col-md-6 form-group">--}}
                        {{--<label>@lang('forms.report-time')</label>--}}
                        {{--<input type="time" class="form-control" name="report_time" required />--}}
                    {{--</div>--}}
                    <div class="col-md-6 form-group">
                        <label>@lang('forms.report-to')</label>
                        <input type="text" class="form-control" name="department_manager" required />
                    </div>
                    <div class="col-md-6 form-group">
                        <label>@lang('forms.store')</label>
                        <select class="form-control" name="store" required >
                            <option value="">@lang('forms.select')</option>
                            <option value="1006" >1006</option>
                            <option value="1201" >1201</option>
                            <option value="1205" >1205</option>
                            <option value="1230" >1230</option>
                            <option value="2001" >2001</option>
                            <option value="2701" >2701</option>
                            <option value="3501" >3501</option>
                            <option value="3701" >3701</option>
                            <option value="3713" >3713</option>
                            <option value="4150" >4150</option>
                            <option value="4424" >4424</option>
                            <option value="7957" >7957</option>
                            <option value="9515" >9515</option>
                            <option value="8953" >Support Center</option>
                        </select>
                    </div>
                    <div class="col-md-6 form-group">
                        <label>@lang('forms.app-version')</label>
                        <select class="form-control" name="application" required >
                            <option value="">@lang('forms.select')</option>
                            <option value="Online">@lang('forms.online')</option>
                            <option value="Paper: English 3/01/11">@lang('forms.paper-english')</option>
                            <option value="Paper: Spanish 6/21/11">@lang('forms.paper-spanish')</option>
                        </select>
                    </div>
                    <div class="col-md-12 form-group">
                        <label>@lang('forms.previous-emp')</label>
                        <select class="form-control" name="previous_emp" required >
                            <option value="">@lang('forms.select')</option>
                            <option value="Yes">@lang('forms.yes')</option>
                            <option value="No">@lang('forms.no')</option>
                        </select><br />
                        <label>@lang('forms.list-details')</label>
                        <input type="text" class="form-control" name="previous_emp_details" />
                    </div>
                    <div class="col-md-12 form-group">
                        <label>@lang('forms.criminal')</label>
                        <select class="form-control" name="criminal" required >
                            <option value="">@lang('forms.select')</option>
                            <option value="Yes">@lang('forms.yes')</option>
                            <option value="No">@lang('forms.no')</option>
                        </select><br />
                        <label>@lang('forms.list-details')</label>
                        <input type="text" class="form-control" name="criminal_details" />
                    </div>
                    <div class="col-md-6 form-group">
                        <label>@lang('forms.years-in-ok')</label>
                        <input type="text" class="form-control" name="years_in_ok" required />
                    </div>
                    <div class="col-md-6 form-group">
                        <label>@lang('forms.num-jobs')</label>
                        <input type="text" class="form-control" name="jobs" required />
                    </div>
                    <div class="col-md-6 form-group">
                        <label>Referred By</label>
                        <input type="text" class="form-control" name="referred_by" />
                    </div>
                    <div class="col-md-6 form-group">
                        <label>@lang('forms.attitude')</label>
                        <input type="text" class="form-control" name="attitude" />
                    </div>
                    <div class="col-md-6 form-group">
                        <label>@lang('forms.orien-liquor')</label>
                        <select class="form-control" name="liquor_license_required" required >
                            <option value="">@lang('forms.select')</option>
                            <option value="Yes">@lang('forms.yes')</option>
                            <option value="No">@lang('forms.no')</option>
                        </select>
                    </div>
                    <div class="col-md-6 form-group">
                        <label>@lang('forms.other')</label>
                        <textarea class="form-control" name="interview_details"></textarea>
                    </div>
                    <div class="col-md-12 form-group">
                        <button class="btn btn-success form-control">@lang('forms.submit')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <p><br /><br /></p>

@endsection

@section('js')
    <script>
        $('#drug-date').change(function() {
            var drugdate = $(this).val();
            if (drugdate === 'Monday') {
                $('#drug-time').val('2pm - 4pm');
                $('#drug-time-label').text('2pm - 4pm');

            } else if (drugdate === 'Friday') {
                $('#drug-time').val('9am - 11am');
                $('#drug-time-label').text('9am - 11am');
            } else {
                $('#drug-time').val('');
                $('#drug-time-label').text('Please select a date...');
            }
        });
    </script>
@endsection