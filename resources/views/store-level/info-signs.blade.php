@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <i class="fa fa-info-circle"></i> @lang('nav.info-signs')
            </div>
            <div class="panel-body">
                <form action="{{ action('StoreLevelController@info_sign_pdf') }}" method="post">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label>@lang('forms.first-line')</label>
                            <input type="text" class="form-control" name="first_line" required />
                        </div>
                        <div class="col-md-12 form-group">
                            <label>@lang('forms.second-line')</label>
                            <input type="text" class="form-control" name="second_line" />
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.type')</label>
                            <select class="form-control" name="type" required >
                                <option value="">@lang('forms.select')</option>
                                <option value="Info">Info</option>
                                <option value="Warning">Warning</option>
                                <option value="Notice">Notice</option>
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Brand</label>
                            <select class="form-control" name="brand" required >
                                <option value="">@lang('forms.select')</option>
                                <option value="buyforless">Buy For Less</option>
                                <option value="uptown">Uptown</option>
                                <option value="supermercado">SuperMercado</option>
                                <option value="smartsaver">Smart Saver</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label class="checkbox-inline"><input type="checkbox" name="landscape" value="1">Landscape Orientation</label>
                        </div>
                        <div class="col-md-6 text-right">
                            <button type="submit" class="btn btn-success">Generate Sign</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection