@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Upcoming Ads
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-condensed">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th class="text-center">Date From</th>
                            <th class="text-center">Date To</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($ads as $ad)
                        <tr>
                            <th>
                                <a href="#" data-toggle="modal" data-target="#myModalAd{{ $ad->id }}">
                                    {{ $ad->name }}
                                </a>
                            </th>
                            <td class="text-center">{{ $ad->date_from }}</td>
                            <td class="text-center">{{ $ad->date_to }}</td>
                            <td class="text-center">
                                <a href="#" data-toggle="modal" data-target="#myModalAd{{ $ad->id }}">
                                    <i class="fa fa-plus-circle"></i>
                                </a>
                                <!-- Modal -->
                                <div class="modal fade" id="myModalAd{{ $ad->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">{{ $ad->name }}</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-condensed">
                                                        @foreach($ad->specials as $special)
                                                        <tr>
                                                            <th>{{ $special->item_description }}</th>
                                                            <td class="text-right">{{ $special->price }}</td>
                                                        </tr>
                                                        @endforeach
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @endsection