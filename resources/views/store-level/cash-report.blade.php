@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">@lang('forms.cash-report')</div>
            <div class="panel-body">
                <form action="{{ action('StoreLevelController@form') }}" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="form_name" value="Cash Over / Short Report">
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.store')</label>
                            <select class="form-control" name="store" required >
                                <option value="">@lang('forms.select')</option>
                                <option value="1006" >1006</option>
                                <option value="1201" >1201</option>
                                <option value="1205" >1205</option>
                                <option value="1230" >1230</option>
                                <option value="2001" >2001</option>
                                <option value="2701" >2701</option>
                                <option value="3501" >3501</option>
                                <option value="3701" >3701</option>
                                <option value="3713" >3713</option>
                                <option value="4150" >4150</option>
                                <option value="4424" >4424</option>
                                <option value="7957" >7957</option>
                                <option value="9515" >9515</option>
                                <option value="0" >Support Center</option>
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.shortage-date')</label>
                            <input type="date" class="form-control" name="date_of_shortage" required />
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.employee-name')</label>
                            <input type="text" class="form-control" name="employee_name" required />
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.over-short')</label>
                            <select class="form-control" name="type" required >
                                <option value="">@lang('forms.select')</option>
                                <option value="Overage">@lang('forms.overage')</option>
                                <option value="Shortage">@lang('forms.shortage')</option>
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.amount')</label>
                            <input type="text" class="form-control" name="amount" required />
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.dept-shortage')</label>
                            <select class="form-control" name="department" required >
                                <option value="">@lang('forms.select')</option>
                                <option value="Cashier">@lang('forms.cashier')</option>
                                <option value="Front Office">@lang('forms.front-office')</option>
                                <option value="Back Office">@lang('forms.back-office')</option>
                                <option value="Deli">@lang('forms.deli')</option>
                                <option value="Other">@lang('forms.other')</option>
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.pockets')</label>
                            <select class="form-control" name="checked_pockets" required >
                                <option value="">@lang('forms.select')</option>
                                <option value="Yes">@lang('forms.yes')</option>
                                <option value="No">@lang('forms.no')</option>
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.repayment')</label>
                            <select class="form-control" name="repayment_form" required >
                                <option value="">@lang('forms.select')</option>
                                <option value="Yes">@lang('forms.yes')</option>
                                <option value="No">@lang('forms.no')</option>
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.mgmt-pers')</label>
                            <input type="text" class="form-control" name="management_personnel_reporting" required />
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.corp-notified')</label>
                            <input type="text" class="form-control" name="corporate_notified" required />
                        </div>
                        <div class="col-md-12 form-group">
                            <label>@lang('forms.comments-box')</label>
                            <textarea class="form-control" name="comments_box"></textarea>
                        </div>
                        <div class="col-md-12 form-group">
                            <button class="btn btn-success form-control">@lang('forms.submit')</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection