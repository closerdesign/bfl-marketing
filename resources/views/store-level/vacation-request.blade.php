@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">@lang('forms.vacation')</div>
            <div class="panel-body">
                <form action="{{ action('StoreLevelController@form') }}" method="post">
                    <input type="hidden" name="form_name" value="Vacation in Lieu">
                    {{ csrf_field() }}
                    <h3>@lang('forms.emp-info')</h3>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.full-name')</label>
                            <input type="text" class="form-control" name="name" required />
                        </div>
                        {{--<div class="col-md-6 form-group">--}}
                            {{--<label>@lang('forms.ssn')</label>--}}
                            {{--<input type="text" class="form-control" name="ssn" />--}}
                        {{--</div>--}}
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.hours-requested')</label>
                            <input type="text" class="form-control" name="hours_requested" required />
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.hire-date')</label>
                            <input type="date" class="form-control" name="hire_date" />
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.seniority-date')</label>
                            <input type="date" class="form-control" name="seniority_date" />
                        </div>
                    </div>
                    <h3>@lang('forms.vil-adtl-info')</h3>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label>@lang('forms.expedited-pymt')</label>
                            <textarea class="form-control" name="expedited_payment"></textarea>
                        </div>
                        <div class="col-md-12 form-group">
                            <label>@lang('forms.cashing-in')</label>
                            <input type="text" class="form-control" name="justification" required />
                            {{--<textarea class="form-control" name="justification"></textarea>--}}
                        </div>
                    </div>
                    <h3>@lang('forms.reviewer')</h3>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.reviewer-name')</label>
                            <input type="text" class="form-control" name="reviewer" />
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.recommendation')</label>
                            <input type="text" class="form-control" name="recommendation" />
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.date')</label>
                            <input type="date" class="form-control" name="review_date" />
                        </div>
                    </div>
                    <h3>@lang('forms.eligibility')</h3>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.hours-eligible')</label>
                            <input type="text" class="form-control" name="hours_eligible" required />
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.hours-used')</label>
                            <input type="text" class="form-control" name="hours_used" />
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.hours-vil')</label>
                            <input type="text" class="form-control" name="hours_vil" />
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.hours-currently')</label>
                            <input type="text" class="form-control" name="hours_currently" />
                        </div>
                    </div>
                    <h3>@lang('forms.vil-approver')</h3>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.approver-name')</label>
                            <input type="text" class="form-control" name="approver" />
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.decision')</label>
                            <select class="form-control" name="decision" >
                                <option value="">@lang('forms.select')</option>
                                <option value="Approved">@lang('forms.approved')</option>
                                <option value="Disapproved">@lang('forms.disapproved')</option>
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.date')</label>
                            <input type="date" class="form-control" name="approve_date" />
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.store')</label>
                            <select class="form-control" name="store" required >
                                <option value="">@lang('forms.select')</option>
                                <option value="1006" >1006</option>
                                <option value="1201" >1201</option>
                                <option value="1205" >1205</option>
                                <option value="1230" >1230</option>
                                <option value="2001" >2001</option>
                                <option value="2701" >2701</option>
                                <option value="3501" >3501</option>
                                <option value="3701" >3701</option>
                                <option value="3713" >3713</option>
                                <option value="4150" >4150</option>
                                <option value="4424" >4424</option>
                                <option value="7957" >7957</option>
                                <option value="9515" >9515</option>
                                <option value="0" >Support Center</option>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <p class="lead">@lang('forms.notes'):</p>
                            <ol>
                                <li>@lang('forms.vil-notes-ol1')</li>
                                <li>@lang('forms.vil-notes-ol2')</li>
                                <li>@lang('forms.vil-notes-ol3')</li>
                                <li>@lang('forms.vil-notes-ol4')</li>
                            </ol>
                            <p>@lang('forms.vil-notes-except')</p>
                            <ol type="A">
                                <li>@lang('forms.vil-notes-ola')</li>
                                <li>@lang('forms.vil-notes-olb')</li>
                            </ol><br />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <button class="btn btn-success form-control">@lang('forms.submit')</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection