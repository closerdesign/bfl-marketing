@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                @lang('store-level.liquor-licenses')
            </div>
            <div class="panel-body">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    @foreach($stores as $store)
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading{{ $store->id }}">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $store->id }}" aria-expanded="true" aria-controls="collapseOne">
                                    [{{ $store->store_code }}] {{ $store->name }}
                                </a>
                            </h4>
                        </div>
                        <div id="collapse{{ $store->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{ $store->id }}">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-condensed">
                                        <thead>
                                        <tr>
                                            <th>@lang('employees.name')</th>
                                            <th>@lang('employees.liquor-licenses')</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($store->employees as $employee)
                                        <tr>
                                            <th>{{ $employee->name }}</th>
                                            <td>
                                                <ul>
                                                    @foreach($employee->licenses as $license)
                                                    <li>
                                                        <a target="_blank" href="{{ $license->file }}">
                                                            [{{ $license->license_number }}] Expiration Date: {{ $license->expiration_date }}
                                                        </a>
                                                    </li>
                                                    @endforeach
                                                </ul>
                                            </td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    @endsection