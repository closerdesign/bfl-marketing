<html>
<head>
    <style>
        body{
            margin: 0;
            padding: 0;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            color: {{ $_GET['color'] }};
        }
        h1{
            text-transform: capitalize;
        }
        .container{
            width: 1375px;
            height: 1060px;
            margin: 0 auto;
            border: none;
            background-size: cover !important;
            background-repeat: no-repeat !important;
        }
        .tag{
            border: 1px solid transparent;
            position: absolute;
            width: 625px;
            height: 470px;
        }
        .box1{
            left: 30px;
            top: 30px;
        }
        .box2{
            left: 720px;
            top: 30px;
        }
        .box3{
            left: 30px;
            top: 562px;
        }
        .box4{
            left: 720px;
            top: 562px;
        }
        .name{
            width: 100%;
            position: absolute;
            top: 215px;
            text-align: center;
            @if(strlen($_GET['name']) > 20)
                font-size: 28px;
            @else
                font-size: 36px;
            @endif
            font-weight: bold;
        }
        .price{
            width: 100%;
            position: absolute;
            @if(strlen($_GET['name']) > 20)
                top: 275px;
            @else
                top: 255px;
            @endif
            text-align: center;
            @if(strlen($_GET['price']) > 10)
                font-size: 60px;
            @else
                font-size: 100px;
            @endif
            font-weight: bold;
        }
        .unit{
            width: 100%;
            position: absolute;
            top: 375px;
            text-align: center;
            font-size: 24px;
            font-weight: bold;
        }
        .fineprint{
            width: 100%;
            position: absolute;
            bottom: 15px;
            text-align: center;
        }

    </style>
</head>
<body>
    <div class="container" style="background: url(/img/dotw/{{ $_GET['brand'] }}-4ups-{{ $_GET['background'] }})">
        <div class="tag box1">
            @if('group' != strtolower($_GET['name']))
            <div class="name">{{ $_GET['name'] }}</div>
            @endif
            <div class="price">{{ $_GET['price'] }}</div>
            <div class="unit">{{ $_GET['unit_of_measure'] }}</div>
            <div class="fineprint">{{ $_GET['fineprint'] }}</div>
        </div>
        <div class="tag box2">
            @if('group' != strtolower($_GET['name']))
                <div class="name">{{ $_GET['name'] }}</div>
            @endif
            <div class="price">{{ $_GET['price'] }}</div>
            <div class="unit">{{ $_GET['unit_of_measure'] }}</div>
            <div class="fineprint">{{ $_GET['fineprint'] }}</div>
        </div>
        <div class="tag box3">
            @if('group' != strtolower($_GET['name']))
                <div class="name">{{ $_GET['name'] }}</div>
            @endif
            <div class="price">{{ $_GET['price'] }}</div>
            <div class="unit">{{ $_GET['unit_of_measure'] }}</div>
            <div class="fineprint">{{ $_GET['fineprint'] }}</div>
        </div>
        <div class="tag box4">
            @if('group' != strtolower($_GET['name']))
                <div class="name">{{ $_GET['name'] }}</div>
            @endif
            <div class="price">{{ $_GET['price'] }}</div>
            <div class="unit">{{ $_GET['unit_of_measure'] }}</div>
            <div class="fineprint">{{ $_GET['fineprint'] }}</div>
        </div>
    </div>
</body>
</html>