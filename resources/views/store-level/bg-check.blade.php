@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">@lang('forms.bg-check')</div>
            <div class="panel-body">
                <form action="{{ action('StoreLevelController@form') }}" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="form_name" value="Background Check">
                    <h3>@lang('forms.candidate-info')</h3>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.full-name')</label>
                            <input type="text" class="form-control" name="name" required />
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.ssn')</label>
                            <input type="text" class="form-control" name="ssn" required />
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.dob')</label>
                            <input type="date" class="form-control" name="dob" required />
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.dl-num')</label>
                            <input type="text" class="form-control" name="dl_num" />
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.dl-state')</label>
                            <input type="text" class="form-control" name="dl_state" />
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.app-version')</label>
                            <select class="form-control" name="application" required >
                                <option value="">@lang('forms.select')</option>
                                <option value="Online">@lang('forms.online')</option>
                                <option value="Paper: English 3/01/11">@lang('forms.paper-english')</option>
                                <option value="Paper: Spanish 6/21/11">@lang('forms.paper-spanish')</option>
                            </select>
                        </div>
                        <div class="col-md-12 form-group">
                            <label>@lang('forms.positions') <small>@lang('forms.multi-select')</small></label>
                            <select multiple class="form-control" name="position" id="position" style="height: 100px; required">
                                <option>@lang('forms.cashier')</option>
                                <option>@lang('forms.cash-office')</option>
                                <option>@lang('forms.dept-specify')</option>
                                <option>@lang('forms.driver')</option>
                                <option>@lang('forms.mgmt-specify')</option>
                                <option>@lang('forms.service-clerk')</option>
                                <option>@lang('forms.stocker')</option>
                                <option>@lang('forms.hr')</option>
                                <option>@lang('forms.other-specify')</option>
                            </select><br />
                            <label>@lang('forms.specify')</label>
                            <input type="text" class="form-control" name="position_details" />
                        </div>
                        <div class="col-md-12 form-group">
                            <label>@lang('forms.previous-emp')</label>
                            <select class="form-control" name="previous_emp" required >
                                <option value="">@lang('forms.select')</option>
                                <option value="Yes">@lang('forms.yes')</option>
                                <option value="No">@lang('forms.no')</option>
                            </select><br />
                            <label>@lang('forms.list-details')</label>
                            <input type="text" class="form-control" name="previous_emp_details" />
                        </div>
                        <div class="col-md-12 form-group">
                            <label>@lang('forms.criminal')</label>
                            <select class="form-control" name="criminal" required >
                                <option value="">@lang('forms.select')</option>
                                <option value="Yes">@lang('forms.yes')</option>
                                <option value="No">@lang('forms.no')</option>
                            </select><br />
                            <label>@lang('forms.list-details')</label>
                            <input type="text" class="form-control" name="criminal_details" />
                        </div>
                    </div>
                    <h3>@lang('forms.adtl-dtls-app')</h3>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.years-in-ok')</label>
                            <input type="text" class="form-control" name="years_in_ok" required />
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.num-jobs')</label>
                            <input type="text" class="form-control" name="jobs" required />
                        </div>
                        <div class="col-md-12 form-group">
                            <label>@lang('forms.other')</label>
                            <textarea class="form-control" name="app_details"></textarea>
                        </div>
                    </div>
                    <h3>@lang('forms.adtl-dtls-int')</h3>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label>Referred By</label>
                            <input type="text" class="form-control" name="referred_by" />
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.attitude')</label>
                            <input type="text" class="form-control" name="attitude" />
                        </div>
                        <div class="col-md-12 form-group">
                            <label>@lang('forms.other')</label>
                            <textarea class="form-control" name="interview_details"></textarea>
                        </div>
                        <div class="col-md-6 form-group">
                            <label>@lang('forms.store')</label>
                            <select class="form-control" name="store" required >
                                <option value="">@lang('forms.select')</option>
                                <option value="1006" >1006</option>
                                <option value="1201" >1201</option>
                                <option value="1205" >1205</option>
                                <option value="1230" >1230</option>
                                <option value="2001" >2001</option>
                                <option value="2701" >2701</option>
                                <option value="3501" >3501</option>
                                <option value="3701" >3701</option>
                                <option value="3713" >3713</option>
                                <option value="4150" >4150</option>
                                <option value="4424" >4424</option>
                                <option value="7957" >7957</option>
                                <option value="9515" >9515</option>
                                <option value="0" >Support Center</option>
                            </select>
                        </div>
                        <div class="col-md-12 form-group">
                            <button class="btn btn-success form-control">@lang('forms.submit')</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection