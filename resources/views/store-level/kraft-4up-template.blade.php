<html>
    <head>
        <style>
            body {
                margin: 0;
                padding: 0;
                font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            }
            h1 {
                text-transform: capitalize;
            }
            .container {
                width: 1375px;
                height: 1060px;
                margin: 0 auto;
                border: none;
                background-size: cover !important;
                background-repeat: no-repeat !important;
            }
            .tag {
                border: 1px solid transparent;
                position: absolute;
                width: 625px;
                height: 470px;
            }
            .box1 {
                left: 30px;
                top: 30px;
            }
            .box2 {
                left: 720px;
                top: 30px;
            }
            .box3 {
                left: 30px;
                top: 562px;
            }
            .box4 {
                left: 720px;
                top: 562px;
            }
            .item {
                position: absolute;
                font-weight: bold;
                font-size: 24px;
                text-align: right;
                text-shadow: white 3px 0px 0px, white 2.83487px 0.981584px 0px, white 2.35766px 1.85511px 0px, white 1.62091px 2.52441px 0px, white 0.705713px 2.91581px 0px, white -0.287171px 2.98622px 0px, white -1.24844px 2.72789px 0px, white -2.07227px 2.16926px 0px, white -2.66798px 1.37182px 0px, white -2.96998px 0.42336px 0px, white -2.94502px -0.571704px 0px, white -2.59586px -1.50383px 0px, white -1.96093px -2.27041px 0px, white -1.11013px -2.78704px 0px, white -0.137119px -2.99686px 0px, white 0.850987px -2.87677px 0px, white 1.74541px -2.43999px 0px, white 2.44769px -1.73459px 0px, white 2.88051px -0.838247px 0px;
                top: 140px;
                right: 20px;
            }
            .size {
                font-weight: normal;
                text-align: right;
            }
            .math {
                position: absolute;
                right: 20px;
                bottom: 50px;
                width: 40%;
                text-align: center;
                font-weight: bold;
                font-size: 36px;
                color: #C51F33;
            }
            .divider {
                width: 40%;
                text-align: center;
                border: 2px solid #C51F33;
                margin-top: -3px;
            }
            .fineprint {
                position: absolute;
                right: 20px;
                bottom: 30px;
                text-align: right;
                font-size: 12px;
                font-weight: bold;
            }
            .total {
                font-family: Arial Black, Arial, sans-serif;
                font-size: 64px;
                margin-top: -10px;
                background: rgb(255,246,0);
                background: radial-gradient(circle, rgba(255,246,0,1) 0%, rgba(255,255,255,0) 100%);
                text-shadow: white 3px 0px 0px, white 2.83487px 0.981584px 0px, white 2.35766px 1.85511px 0px, white 1.62091px 2.52441px 0px, white 0.705713px 2.91581px 0px, white -0.287171px 2.98622px 0px, white -1.24844px 2.72789px 0px, white -2.07227px 2.16926px 0px, white -2.66798px 1.37182px 0px, white -2.96998px 0.42336px 0px, white -2.94502px -0.571704px 0px, white -2.59586px -1.50383px 0px, white -1.96093px -2.27041px 0px, white -1.11013px -2.78704px 0px, white -0.137119px -2.99686px 0px, white 0.850987px -2.87677px 0px, white 1.74541px -2.43999px 0px, white 2.44769px -1.73459px 0px, white 2.88051px -0.838247px 0px;
            }
            .image {
                position: absolute;
                bottom: 25px;
                left: 15px;
            }
        </style>
    </head>
    <body>
        <div class="container" style="background: url(/img/kraft/kraft-4ups.jpg)">
            <div class="tag box1">
                <div class="image">
                    <img src="{{ $_GET['image'] }}" style="max-width: 365px; max-height: 315px;" />
                </div>
                <div class="item">
                    {!! $_GET['name'] !!}<br />
                    <div class="size">{!! $_GET['size'] !!}</div>
                </div>
                <div class="math">
                    {{ $_GET['price'] }}<br />
                    - {{ $_GET['minus'] }}
                    <hr class="divider" />
                    <div class="total">{{ $_GET['total'] }}</div>
                </div>
                <div class="fineprint">Each when you buy any 5. Mix or Match.</div>
            </div>
            <div class="tag box2">
                <div class="image">
                    <img src="{{ $_GET['image'] }}" style="max-width: 365px; max-height: 315px;" />
                </div>
                <div class="item">
                    {!! $_GET['name'] !!}<br />
                    <div class="size">{!! $_GET['size'] !!}</div>
                </div>
                <div class="math">
                    {{ $_GET['price'] }}<br />
                    - {{ $_GET['minus'] }}
                    <hr class="divider" />
                    <div class="total">{{ $_GET['total'] }}</div>
                </div>
                <div class="fineprint">Each when you buy any 5. Mix or Match.</div>
            </div>
            <div class="tag box3">
                <div class="image">
                    <img src="{{ $_GET['image'] }}" style="max-width: 365px; max-height: 315px;" />
                </div>
                <div class="item">
                    {!! $_GET['name'] !!}<br />
                    <div class="size">{!! $_GET['size'] !!}</div>
                </div>
                <div class="math">
                    {{ $_GET['price'] }}<br />
                    - {{ $_GET['minus'] }}
                    <hr class="divider" />
                    <div class="total">{{ $_GET['total'] }}</div>
                </div>
                <div class="fineprint">Each when you buy any 5. Mix or Match.</div>
            </div>
            <div class="tag box4">
                <div class="image">
                    <img src="{{ $_GET['image'] }}" style="max-width: 365px; max-height: 315px;" />
                </div>
                <div class="item">
                    {!! $_GET['name'] !!}<br />
                    <div class="size">{!! $_GET['size'] !!}</div>
                </div>
                <div class="math">
                    {{ $_GET['price'] }}<br />
                    - {{ $_GET['minus'] }}
                    <hr class="divider" />
                    <div class="total">{{ $_GET['total'] }}</div>
                </div>
                <div class="fineprint">Each when you buy any 5. Mix or Match.</div>
            </div>
        </div>
    </body>
</html>