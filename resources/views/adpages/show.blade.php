@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="/ads">Ad Pages</a> > {{ $ad->name }} - {{ $ad->brands->name }}
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <form action="{{ action('AdpagesController@store', $ad->id) }}" enctype="multipart/form-data" method="post">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="ad" value="{{ $ad->id }}">
                                    <div class="col-md-12 form-group">
                                        <label for="image">Image</label>
                                        <input type="file" class="form-control" name="image" required >
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <button class="btn btn-primary form-control" type="submit">Save</button>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-8">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Image</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($pages as $page)
                                            <tr>
                                                <td><img width="240" src="{{ $page->image }}" alt=""></td>
                                                <td class="text-center">
                                                    <form method="post" action="{{ action('AdpagesController@destroy', $page->id) }}">
                                                        {{ csrf_field() }}
                                                        {{ method_field('DELETE') }}
                                                        <button class="btn btn-default"><i class="fa fa-trash"></i></button>
                                                    </form>

                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <p class="pull-right">
                                    <a class="btn btn-default" href="{{ action('AdfilesController@show', $ad->id) }}">
                                        Ad Files <i class="fa fa-arrow-circle-right"></i>
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection