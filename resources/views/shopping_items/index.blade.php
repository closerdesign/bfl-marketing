@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p class="pull-right">
                    <a href="{{ action('ShoppingItemsController@create') }}" class="btn btn-primary">Add product</a>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Shopping Items</div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped datatable">
                                <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Name</th>
                                    <th>Brand</th>
                                    <th>Category</th>
                                    <th>Department</th>
                                    <th>&nbsp;</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($items as $item)
                                <tr>
                                    <td>
                                        <a href="{{ action('ShoppingItemsController@edit', $item->id) }}">
                                            <img src="{{ $item->image }}" width="100" alt="" class="img-responsive">
                                        </a>
                                    </td>
                                    <th>
                                        <a href="{{ action('ShoppingItemsController@edit', $item->id) }}">{{ $item->product_name }}</a>
                                    </th>
                                    <td>{{ $item->brand_name }}</td>
                                    <td>{{ $item->category_id }}</td>
                                    <td>{{ $item->departments->name }}</td>
                                    <td>
                                        <form method="post" action="{{ action('ShoppingItemsController@destroy', $item->id) }}" onsubmit="return confirm('Are you sure? This operation can\'t be undone')">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button class="btn btn-default form-control" type="submit"><i class="fa fa-trash"></i></button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection