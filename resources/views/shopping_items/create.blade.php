@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Create a New Shopping Item</div>
                    <div class="panel-body">
                        <form action="{{ action('ShoppingItemsController@store') }}" enctype="multipart/form-data" method="post">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-4 form-group">
                                    <label for="upc">UPC</label>
                                    <input type="text" class="form-control" name="upc" value="{{ old('upc') }}" required >
                                </div>
                                <div class="col-md-8 form-group">
                                    <label for="product_name">Product Name</label>
                                    <input type="text" class="form-control" name="product_name" value="{{ old('product_name') }}" required >
                                </div>
                                <div class="col-md-4 form-group">
                                    <label for="brand_name">Brand Name</label>
                                    <input type="text" class="form-control" name="brand_name" value="{{ old('brand_name') }}" required >
                                </div>
                                <div class="col-md-4 form-group">
                                    <label for="size">Size</label>
                                    <input type="text" class="form-control" name="size" value="{{ old('size') }}" required >
                                </div>
                                <div class="col-md-4 form-group">
                                    <label for="uom">UOM</label>
                                    <input type="text" class="form-control" name="uom" value="{{ old('uom') }}" required >
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="description">Description</label>
                                    <textarea name="description" id="" cols="30" rows="10"
                                              class="form-control summernote">{!! old('description') !!}</textarea>
                                </div>
                                <div class="col-md-4 form-group">
                                    <label for="department_id">Department</label>
                                    <select name="department_id" id="department_id" class="form-control">
                                        <option value="">Select...</option>
                                        @foreach(\App\Department::orderBy('name')->get() as $department)
                                        <option @if( old('department_id') == $department->id ) selected @endif value="{{ $department->id }}">{{ $department->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4 form-group">
                                    <label for="category_id">Category</label>
                                    <input type="text" class="form-control" name="category_id" value="{{ old('category_id') }}" required >
                                </div>
                                <div class="col-md-4 form-group">
                                    <label for="price">Price</label>
                                    <input type="text" class="form-control" name="price" value="{{ old('price') }}" required >
                                </div>
                                <div class="col-md-4 form-group">
                                    <label for="price_strategy">Price Strategy</label>
                                    <select name="price_strategy" id="price_strategy" class="form-control">
                                        <option value="">Select...</option>
                                        <option @if( old('price_strategy') == 1 ) selected @endif value="1">Regular</option>
                                        <option @if( old('price_strategy') == 77 ) selected @endif value="77">Cost + Format</option>
                                    </select>
                                </div>
                                <div class="col-md-4 form-group">
                                    <label for="price_type">Price Type</label>
                                    <select name="price_type" id="price_type" class="form-control">
                                        <option value="">Select..</option>
                                        <option @if( old('price_type') == 1 ) selected @endif value="1">Regular</option>
                                        <option @if( old('price_type') == 2 ) selected @endif value="2">TPR (Temporary Price Reduction)</option>
                                        <option @if( old('price_type') == 3 ) selected @endif value="3">Green Tag</option>
                                        <option @if( old('price_type') == 4 ) selected @endif value="4">Sale</option>
                                    </select>
                                </div>
                                <div class="col-md-4 form-group">
                                    <label for="start_date">Start Date</label>
                                    <input type="text" class="form-control datepicker" name="start_date" value="{{ old('start_date') }}" >
                                </div>
                                <div class="col-md-4 form-group">
                                    <label for="end_date">End Date</label>
                                    <input type="text" class="form-control datepicker" name="end_date" value="{{ old('end_date') }}" >
                                </div>
                                <div class="col-md-4 form-group">
                                    <label for="tax">Tax</label>
                                    <input type="text" class="form-control" name="tax" value="{{ old('tax') }}" >
                                </div>
                                <div class="col-md-4 form-group">
                                    <label for="image">Image</label>
                                    <input type="file" class="form-control" name="image" required >
                                </div>
                                <div class="col-md-12 form-group">
                                    <button class="btn-primary btn-lg pull-right" type="submit">Save changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection