@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="panel panel-default">
            <div class="panel-heading">Events</div>
            <div class="panel-body">

                <div class="row">
                    <div class="col-md-12 text-right">
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-eye" aria-hidden="true"></i> View
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="https://buyforlessok.com/events" target="_blank">
                                        Buy For Less
                                    </a>
                                </li>
                                <li>
                                    <a href="https://smartsaverok.com/events" target="_blank">
                                        Smart Saver
                                    </a>
                                </li>
                                <li>
                                    <a href="https://uptowngroceryco.com/events" target="_blank">
                                        Uptown
                                    </a>
                                </li>

                            </ul>
                        </div>

                        <a href="{{ action('EventsController@create') }}" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add Event</a>

                    </div>

                </div>

                <hr>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Brand</th>
                            <th width="75">Signups</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($events as $event)
                            <tr>
                                <th><a href="{{ action('EventsController@edit', $event->id) }}">{{ $event->name }}</a></th>
                                <td>{{ $event->date }}</td>
                                <td>{{ $event->time }}</td>
                                <td>{{ $event->brands->name }}</td>
                                <td>
                                    @if($event->signups == '1')
                                        <a href="{{ action('EventsController@view_signups', $event->id) }}">Yes</a>
                                    @else
                                        No
                                    @endif
                                </td>
                                <td>{{ $event->status }}</td>
                                <td>
                                    <p class="text-center">
                                        <a href="{{ action('EventsController@clone', $event->id) }}" class="btn btn-primary" style="width: 35px">
                                            <i class="fa fa-copy"></i>
                                        </a>
                                    </p>
                                    <p class="text-center">
                                        <a class="btn btn-danger" href="{{ action('EventsController@delete', $event->id) }}">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </p>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <p>
            <?php $url = url()->current() ?>
            @if( substr($url, strlen($url) - 3, strlen($url)) == 'all' )
                <a href="{{ action('EventsController@index') }}">View only upcoming events</a>
            @else
                <a href="{{ action('EventsController@show_all') }}">View all events</a>
            @endif
        </p>
    </div>

    @endsection