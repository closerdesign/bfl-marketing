@extends('layouts.app')

@section('content')

    <div class="container">
        <p>
            <a href="{{ action('EventsController@index') }}" class="btn btn-primary">
                <i class="fa fa-arrow-circle-left"></i> Go Back
            </a>
        </p>
        <div class="panel panel-primary">
            <div class="panel-heading">Create a new event</div>
            <div class="panel-body">
                <form action="{{ action('EventsController@store') }}" method="post" enctype="multipart/form-data" >
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-4 form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}" required >
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="location">Location (Optional)</label>
                            <input type="text" class="form-control" name="location" value="{{ old('location') }}" >
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="image">Image (Optional)</label>
                            <input type="file" class="form-control" name="image" >
                            <label>Image size: 500 x 500 px.</label>
                        </div>
                        <div class="col-md-12 form-group">
                            <label for="intro">Intro
                                <a href="#" data-toggle="tooltip" data-placement="right" title="This is a teaser for the event. Make it short, sweet, and enticing.">
                                        <i class="fa fa-info-circle"></i>
                                    </a>
                            </label>
                            <textarea name="intro" id="intro" cols="30" rows="10" class="form-control" required >{{ old('intro') }}</textarea>
                        </div>
                        <div class="col-md-12 form-group">
                            <label for="description">Description
                                <a href="#" data-toggle="tooltip" data-placement="right" title="This is for the lengthy description of the event. Be sure to indicate which location, if not all.">
                                        <i class="fa fa-info-circle"></i>
                                    </a>
                            </label>
                            <textarea name="description" id="description" cols="30" rows="5" class="form-control summernote" required >{{ old('description') }}</textarea>
                        </div>
                        <div class="col-md-12 form-group">
                            <label class="checkbox-inline">
                                <input type="checkbox" value="" id="allDay">This is an all day event
                            </label>
                        </div>
                        <div class="col-md-3 form-group">
                            <label for="date">Date</label>
                            <input type="text" class="form-control datepicker" name="date" value="{{ old('date') }}" required >
                        </div>
                        <div class="col-md-3 form-group">
                            <label for="end_date">End Date (Optional)</label>
                            <input type="text" class="form-control datepicker" name="end_date" value="{{ old('end_date') }}" >
                        </div>
                        <div class="col-md-3 form-group">
                            <label for="time">Time</label>
                            <input type="text" class="form-control timepicker" id="time" name="time" value="{{ old('time') }}" required >
                        </div>
                        <div class="col-md-3 form-group">
                            <label for="end_time">End Time (Optional)</label>
                            <input type="text" class="form-control timepicker" id="end_time" name="end_time" value="{{ old('end_time') }}">
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="brand_id">Brand</label>
                            <select name="brand_id" id="brand_id" class="form-control" required >
                                <option value="">Select...</option>
                                @foreach(App\Brand::all() as $brand)
                                    <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="signups">Signups (Optional)
                                <a href="#" data-toggle="tooltip" data-placement="right" title="This indicates whether or not this event will allow signups from the website.">
                                    <i class="fa fa-info-circle"></i>
                                </a>
                            </label>
                            <select name="signups" id="signups" class="form-control">
                                <option value="">Select...</option>
                                <option @if(old('signups') == '1') selected @endif value="1">Yes</option>
                                <option @if(old('signups') == '0') selected @endif value="0">No</option>
                            </select>
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="status">Status
                                <a href="#" data-toggle="tooltip" data-placement="right" title="This indicates whether the event is published or not published on the website. Please publish events as far in advance as possible.">
                                        <i class="fa fa-info-circle"></i>
                                    </a>
                            </label>
                            <select name="status" id="status" class="form-control">
                                <option value="">Select...</option>
                                <option @if(old('status') == 'Published') selected @endif value="1">Published</option>
                                <option @if(old('status') == 'Unpublished') selected @endif value="0">Unpublished</option>
                            </select>
                        </div>
                        <div class="col-md-12 form-group">
                            <button class="btn btn-primary form-control" type="submit">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <script>
        $('#allDay').click( function(){
           if ($(this).is(':checked')) {
               $('#time').val('00:00');
               $('#end_time').val('23:30');
           } else {
               $('#time').val('');
               $('#end_time').val('');
           }
        });
    </script>

@endsection