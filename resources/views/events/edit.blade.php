@extends('layouts.app')

@section('content')

    <div class="container">
        <p>
            <a href="{{ action('EventsController@index') }}" class="btn btn-primary">
                <i class="fa fa-arrow-circle-left"></i> Go Back
            </a>
        </p>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Editing: {{ $event->name }}</div>
                    <div class="panel-body">
                        <form action="{{ action('EventsController@update', $event->id) }}" method="post" enctype="multipart/form-data" >
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}
                            <div class="row">
                                @if($event->image != "")
                                    <div class="col-md-12 form-group">
                                        <img src="{{ $event->image }}" alt="{{ $event->name }}" class="img-responsive pull-right" width="200">
                                    </div>
                                @endif
                                <div class="col-md-6 form-group">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" name="name" value="{{ $event->name }}" required >
                                </div>
                                <div class="col-md-6 form-group">
                                    <label for="location">Location (Optional)</label>
                                    <input type="text" class="form-control" name="location" value="{{ $event->location }}" >
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="image">Image (Optional)</label>
                                    <input type="file" class="form-control" name="image" >
                                    <label>Image size: 500 x 500 px.</label>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="intro">Intro
                                        <a href="#" data-toggle="tooltip" data-placement="right" title="This is a teaser for the event. Make it short, sweet, and enticing.">
                                            <i class="fa fa-info-circle"></i>
                                        </a>
                                    </label>
                                    <textarea name="intro" id="intro" cols="30" rows="10" class="form-control" required >{{ $event->intro }}</textarea>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="description">Description
                                         <a href="#" data-toggle="tooltip" data-placement="right" title="This is for the lengthy description of the event. Be sure to indicate which location, if not all.">
                                            <i class="fa fa-info-circle"></i>
                                        </a>
                                    </label>
                                    <textarea name="description" id="description" cols="30" rows="5" class="form-control summernote" required >{{ $event->description }}</textarea>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" value="" id="allDay" @if($event->time == '00:00:00' && $event->end_time == '23:30:00') checked @endif>This is an all day event
                                    </label>
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="date">Date</label>
                                    <input type="text" class="form-control datepicker" name="date" value="{{ $event->date }}" required >
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="end_date">End Date (Optional)</label>
                                    <input type="text" class="form-control datepicker" name="end_date" value="{{ $event->end_date }}" >
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="time">Time</label>
                                    <input type="text" class="form-control timepicker" id="time" name="time" value="{{ $event->time }}" required >
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="end_time">End Time (Optional)</label>
                                    <input type="text" class="form-control timepicker" id="end_time" name="end_time" value="{{ $event->end_time }}">
                                </div>
                                <div class="col-md-4 form-group">
                                    <label for="brand_id">Brand</label>
                                    <select name="brand_id" id="brand_id" class="form-control" required >
                                        <option value="">Select...</option>
                                        @foreach(App\Brand::all() as $brand)
                                            <option @if($event->brand_id == $brand->id) selected @endif value="{{ $brand->id }}">{{ $brand->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4 form-group">
                                    <label for="signups">Signups (Optional)
                                        <a href="#" data-toggle="tooltip" data-placement="right" title="This indicates whether or not this event will allow signups from the website.">
                                            <i class="fa fa-info-circle"></i>
                                        </a>
                                    </label>
                                    <select name="signups" id="signups" class="form-control">
                                        <option value="">Select...</option>
                                        <option @if($event->signups == '1') selected @endif value="1">Yes</option>
                                        <option @if($event->signups == '0') selected @endif value="0">No</option>
                                    </select>
                                </div>
                                <div class="col-md-4 form-group">
                                    <label for="status">Status
                                        <a href="#" data-toggle="tooltip" data-placement="right" title="This indicates whether the event is published or not published on the website. Please publish events as far in advance as possible.">
                                            <i class="fa fa-info-circle"></i>
                                        </a>
                                    </label>
                                    <select name="status" id="status" class="form-control">
                                        <option value="">Select...</option>
                                        <option @if($event->status == 'Published') selected @endif value="1">Published</option>
                                        <option @if($event->status == 'Unpublished') selected @endif value="0">Unpublished</option>
                                    </select>
                                </div>
                                <div class="col-md-12 form-group">
                                    <button class="btn btn-primary form-control" type="submit">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <script>
        $('#allDay').click( function(){
            if ($(this).is(':checked')) {
                $('#time').val('00:00');
                $('#end_time').val('23:30');
            } else {
                $('#time').val('');
                $('#end_time').val('');
            }
        });
    </script>

@endsection