@extends('layouts.app')

@section('content')

    <div class="container">
        <p class="no-print">
            <a href="{{ action('EventsController@index') }}" class="btn btn-primary">
                <i class="fa fa-arrow-circle-left"></i> Go Back
            </a>
            <a id="copyEmails" class="btn btn-info">
                <i class="fa fa-copy"></i> Copy Emails</a>
            </a>
            <a href="javascript:window.print();" class="btn btn-default">
                <i class="fa fa-print"></i> Print</a>
            </a>
        </p>
        <h1>{{ $event->name }}</h1>
        <h3>{{ date('F jS, Y', strtotime($event->date)) }} @if($event->end_date != null) to {{ date('F jS, Y', strtotime($event->end_date)) }} @endif</h3>
        <h4>@if($event->location){{$event->location}} | @endif{{ date('h:i a', strtotime($event->time)) }} @if( $event->end_time != null ) to {{ date('h:i a', strtotime($event->end_time)) }} @endif</h4>
        <div class="row">
            @if(count($signups) < 1)
                <p class="lead text-center text-muted">There are currently no signups for this event.</p>
            @else
                <div class="col-md-6 col-md-offset-3">
                    <ul class="list-group">
                        @foreach($signups as $signup)
                            <li class="list-group-item">
                                <i class="fa fa-square-o"></i> &nbsp; &nbsp;{{ $signup->name }} | {{ $signup->email }} | {{ $signup->phone }}
                            </li>
                        @endforeach
                    </ul>
                </div>

            @endif
        </div>

    </div>

    <input type="text" value="{{ $email_list }}" id="emails" style="position: absolute; width: 10px; right: -10px;" />

@endsection

@section('js')
    <script>
        $('#copyEmails').click(function() {
            var copyText = document.getElementById('emails');

            copyText.select();

            document.execCommand('copy');
        });
    </script>
@endsection