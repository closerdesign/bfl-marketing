@extends('layouts.app')

@section('content')

    <div class="container-fluid">

        <p class="lead">
            @lang('roc.title'): @lang('roc.pending-requests')
        </p>

        <div class="text-right form-group">
            <a href="{{ action('RocEntryController@create') }}" class="btn btn-success">
                @lang('roc-entry.add-new-entry')
            </a>
        </div>

        @if( $entries->count() === 0 )
        <hr>
        <p class="lead text-center">
            @lang('roc-entry.there-are-not-pending-entries-at-this-time')
        </p>
        <p class="text-center">
            <a href="{{ action('RocEntryController@create') }}" class="btn btn-success">
                @lang('roc-entry.add-new-entry')
            </a>
        </p>
        @endif

        @foreach($entries as $entry)
        <div class="panel panel-default">
            <div class="panel-heading">
                <a href="{{ action('RocEntryController@edit', $entry->id) }}" class="badge">
                    @lang('general.edit')
                </a>
                {{ $entry->name }}
            </div>
            <div class="panel-body">
                <p>{{ $entry->content }}</p>
                <p>
                    <b>@lang('roc-entry.stores'):</b>
                </p>
                <ul class="list-group">
                    @foreach($entry->stores_list as $store)
                        <li class="list-group-item">
                            [{{ $store->store_code }}] {{ $store->brands->name }} - {{ $store->name }}
                        </li>
                    @endforeach
                </ul>
                <p class="text-right">
                    <i>{{ $entry->attachments->count() }} @lang('roc-entry.files'), {{ $entry->images->count() }} @lang('roc-entry.images')</i>.
                </p>
            </div>
            <div class="panel-footer">
                <div class="text-right">
                    <button class="btn-xs btn-danger" onclick="$('#roc-entry-delete-{{ $entry->id }}').submit();">
                        <i class="fa fa-trash"></i>
                    </button>
                    <form method="post" id="roc-entry-delete-{{ $entry->id }}" action="{{ route('roc-entry.destroy', $entry->id) }}" onsubmit="return confirm('@lang('general.are-you-sure')')">
                        @csrf
                        @method('DELETE')
                    </form>
                </div>
            </div>
        </div>
        @endforeach

    </div>

    @endsection