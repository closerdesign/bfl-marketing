@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="panel panel-default">
            <div class="panel-heading">
                {{ $rocEntry->name }}
            </div>
            <div class="panel-body">
                {{ $rocEntry->content }}
                <hr/>
                <p>
                    <b>@lang('roc-entry.stores')</b>
                </p>

                <ul class="list-group">
                    @foreach($rocEntry->stores_list as $store)
                    <li class="list-group-item">
                        <span class="text-uppercase label {{ $rocEntry->store_status_label($store->id) }}">{{ $rocEntry->store_status($store->id) }}</span> [{{ $store->store_code }}] {{ $store->brands->name }} - {{ $store->name }}
                    </li>
                    @endforeach
                </ul>

                <hr>

                <div class="row">
                    @foreach($rocEntry->attachments as $attachment)
                    <div class="col-md-2 col-sm-3 col-xs-4">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <p class="text-center">
                                    <a href="{{ $attachment->file }}" target="_blank">
                                        <img src="/img/report.svg" alt="" class="img-responsive">
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @foreach($rocEntry->images as $image)
                    <div class="col-md-2 col-sm-3 col-xs-4">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <p class="text-center">
                                    <a href="{{ $image->image }}" target="_blank">
                                        <img src="/img/jpg.svg" alt="" class="img-responsive">
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            @if( (auth()->user()->store != null) && ($rocEntry->status != \Illuminate\Support\Facades\Lang::get('roc-entry.completed')) )
            <div class="panel-footer">
                <div class="text-right">
                    <button class="btn btn-success" onclick="$('#mark-as-completed-{{ $rocEntry->id }}').submit()">
                        <i class="fa fa-check"></i> @lang('roc-entry.mark-as-completed')
                    </button>
                    <form id="mark-as-completed-{{ $rocEntry->id }}" action="{{ route('roc-entry.mark-as-completed', $rocEntry->id) }}" method="post" onsubmit="return confirm('@lang('general.are-you-sure')')">
                        @csrf
                    </form>
                </div>
            </div>
            @endif
        </div>
    </div>

    @endsection