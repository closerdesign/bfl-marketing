@extends('layouts.app')

@section('content')
    
    <div class="container-fluid">
        <div class="panel panel-default">
            <div class="panel-heading">
                @lang('roc-entry.add-new-entry')
            </div>
            <div class="panel-body">
                <form action="{{ action('RocEntryController@store') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="name">@lang('roc-entry.name')</label>
                        <input type="text" class="form-control" name="name" value="{{ old('name') }}" required >
                    </div>
                    <div class="form-group">
                        <label for="content">@lang('roc-entry.content')</label>
                        <textarea name="content" id="content" cols="30" rows="10" class="form-control">{{ old('content') }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="stores">@lang('roc-entry.stores')</label>
                        <select name="stores[]" id="stores" class="form-control multiple-select" multiple >
                            @foreach(\App\Store::active()->get() as $store)
                                <option value="{{ $store->id }}">{{ $store->store_code }}</option>
                            @endforeach
                        </select>
                    </div>
                    <button class="btn btn-success btn-block">
                        @lang('general.save')
                    </button>
                </form>
            </div>
        </div>
    </div>
    
    @endsection