@extends('layouts.app')

@section('content')
    
    <div class="container-fluid">
        <p class="lead">@lang('general.edit'): {{ $rocEntry->name }}</p>
        <div class="panel panel-default">
            <div class="panel-heading">
                @lang('roc-entry.add-new-entry')
            </div>
            <div class="panel-body">
                <form action="{{ action('RocEntryController@update', $rocEntry->id) }}" method="post">
                    @csrf
                    @method('PATCH')
                    <div class="form-group">
                        <label for="name">@lang('roc-entry.name')</label>
                        <input type="text" class="form-control" name="name" value="{{ $rocEntry->name }}" required >
                    </div>
                    <div class="form-group">
                        <label for="content">@lang('roc-entry.content')</label>
                        <textarea name="content" id="content" cols="30" rows="10" class="form-control">{{ $rocEntry->content }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="stores">@lang('roc-entry.stores')</label>
                        <select name="stores[]" id="stores" class="form-control multiple-select" multiple >
                            @foreach(\App\Store::active()->get() as $store)
                                <option @if( in_array($store->id, explode(',', $rocEntry->stores)) ) selected @endif value="{{ $store->id }}">{{ $store->store_code }}</option>
                            @endforeach
                        </select>
                    </div>
                    <button class="btn btn-success btn-block">
                        @lang('general.save')
                    </button>
                </form>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">@lang('roc-attachment.title')</div>
            <div class="panel-body">
                <form action="{{ action('RocAttachmentController@store', $rocEntry->id) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="file">@lang('roc-attachment.file')</label>
                        <input type="file" class="form-control" name="file" required >
                    </div>
                    <button class="btn btn-success">
                        @lang('general.save')
                    </button>
                </form>
                <hr>
                <div class="row">
                    @foreach($rocEntry->attachments as $attachment)
                    <div class="col-md-2 col-xs-6">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <p class="text-center">
                                    <a target="_blank" href="{{ $attachment->file }}">
                                        <i class="fa fa-file fa-4x"></i>
                                    </a>
                                </p>
                            </div>
                            <div class="panel-footer">
                                <div class="text-right">
                                    <button class="btn-danger btn-xs" onclick="$('#attachment-delete-{{ $attachment->id }}').submit();">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </div>
                                <form onsubmit="return confirm('@lang('general.are-you-sure')')" id="attachment-delete-{{ $attachment->id }}" action="{{ route('roc-attachment.destroy', $attachment->id) }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                </form>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>

            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">@lang('roc-image.title')</div>
            <div class="panel-body">
                <form action="{{ action('RocImageController@store', $rocEntry->id) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="file">@lang('roc-image.file')</label>
                        <input type="file" class="form-control" name="file" required >
                    </div>
                    <button class="btn btn-success">
                        @lang('general.save')
                    </button>
                </form>
                <hr>
                <div class="row">
                    @foreach($rocEntry->images as $image)
                        <div class="col-md-2 col-xs-6">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <p class="text-center">
                                        <a target="_blank" href="{{ $image->image }}">
                                            <img src="/img/dummy.png" alt="" class="img-responsive" style="background: url({{ $image->image }}); background-size: contain; background-repeat: no-repeat;">
                                        </a>
                                    </p>
                                </div>
                                <div class="panel-footer">
                                    <div class="text-right">
                                        <button class="btn-danger btn-xs" onclick="$('#image-delete-{{ $image->id }}').submit();">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </div>
                                    <form id="image-delete-{{ $image->id }}" action="{{ route('roc-image.destroy', $image->id) }}" method="post" onsubmit="return confirm('@lang('general.are-you-sure')')">
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="text-center">
            <a href="{{ route('roc-entry.index') }}" class="btn btn-default"><i class="fa fa-arrow-circle-left"></i> @lang('roc-entry.go-to-pending-entries')</a>
        </div>

    </div>
    
    @endsection