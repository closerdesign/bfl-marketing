@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-primary">
                    <div class="panel-heading">Your Wellness Newsletters</div>

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Brand</th>
                                    <th>File</th>
                                    <th>Date From</th>
                                    <th>Date To</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>

                                @if(count($newsletters) < 1)
                                    <tr><td colspan="7" class="text-center">No Newsletters added.</td></tr>
                                @endif
                                @foreach($newsletters as $newsletter)

                                    <tr>
                                        <td>
                                            <a href="{{ action('NewsletterController@show', $newsletter->id) }}" class="label label-info" title="click to edit"><i class="fa fa-plus-circle"></i> {{ $newsletter->id }}</a>
                                        </td>
                                        <td>
                                            {{ $newsletter->brands->name }}
                                        </td>
                                        <td>
                                            <a href="{{ Storage::url('newsletters/' . $newsletter->file) }}" target="_blank">
                                                {{ $newsletter->file }}
                                            </a>
                                        </td>
                                        <td>
                                            {{ $newsletter->date_from }}
                                        </td>
                                        <td>
                                            {{ $newsletter->date_to }}
                                        </td>
                                        <td class="text-center">
                                            <form onsubmit="return confirm('Are you sure? This can\'t be undone.')" action="{{ action('NewsletterController@destroy', $newsletter->id) }}" method="post">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button class="btn-xs btn-danger" type="submit"><i class="fa fa-trash"></i></button>
                                            </form>
                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                            {{ $newsletters->render() }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-info">
                    <div class="panel-heading"><i class="fa fa-plus"></i> Add Newsletter</div>

                    <div class="panel-body">
                        <form action="{{ action('NewsletterController@store') }}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label>Brand</label>
                                <select class="form-control" name="brand_id" required >
                                    <option value="" >Select...</option>
                                    @foreach(\App\Brand::orderBy('id')->get() as $brand)
                                        <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>From</label>
                                        <input type="date" class="form-control" name="date_from" required />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>To</label>
                                        <input type="date" class="form-control" name="date_to" required />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>File</label>
                                <input type="file" class="form-control" name="file" required />
                            </div>
                            <div class="form-group">
                                <button class="btn btn-block btn-success">Upload</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection