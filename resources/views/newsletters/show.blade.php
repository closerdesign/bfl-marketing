@extends('layouts.app')

@section('content')

    <div class="container">
        <p>
            <a href="{{ action('NewsletterController@index') }}" class="btn btn-primary">
                <i class="fa fa-arrow-circle-left"></i> Go Back
            </a>
        </p>
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">Edit Newsletter</div>
                    <div class="panel-body">
                        <form action="{{ action('NewsletterController@update', $newsletter->id) }}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}
                            <div class="form-group">
                                <label>Brand</label>
                                <select class="form-control" name="brand_id" required >
                                    <option value="" >Select...</option>
                                    @foreach(\App\Brand::orderBy('id')->get() as $brand)
                                        <option value="{{ $brand->id }}" @if( $newsletter->brand_id == $brand->id ) selected @endif>{{ $brand->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>From</label>
                                        <input type="date" class="form-control" name="date_from" value="{{ $newsletter->date_from }}" required />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>To</label>
                                        <input type="date" class="form-control" name="date_to" value="{{ $newsletter->date_to }}" required />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>File</label>
                                <input type="file" class="form-control" name="file" />
                            </div>
                            <div class="form-group">
                                <button class="btn btn-block btn-success"><i class="fa fa-save"></i> Update Newsletter</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="panel panel-info">
                    <div class="panel-heading">Images</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">

                                <form action="{{ action('NewsletterImageController@store', $newsletter->id) }}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label>Image</label>
                                        <input type="file" class="form-control" name="image" required />
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-block btn-success"><i class="fa fa-plus"></i> Add Image</button>
                                    </div>
                                </form>

                            </div>
                            <div class="col-md-8" style="display: table; border-left: 1px solid #f5f5f5">
                                @if(count($images) < 1)
                                    <div style="height: 150px; width: 100%; display: table-cell; vertical-align: middle;" class="text-center">No images uploaded.</div>
                                @endif
                                @foreach($images as $image)
                                    <div style="padding: 15px; float: left;" class="text-right">
                                        <form onsubmit="return confirm('Are you sure? This can\'t be undone.')" action="{{ action('NewsletterImageController@destroy', $image->id) }}">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button class="btn-xs btn-danger"><i class="fa fa-trash"></i></button><br />
                                        </form>
                                        <a href="{{ Storage::url('newsletter-images/' . $image->image) }}" target="_blank">
                                            <img src="{{ Storage::url('newsletter-images/' . $image->image) }}" style="height: 250px" />
                                        </a>
                                    </div>

                                @endforeach
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

