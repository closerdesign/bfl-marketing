@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <p class="text-right">
            <a href="{{ action('AwgPriceController@create') }}" class="btn btn-success">
                Import AWG Pricing File
            </a>
        </p>
        <div class="panel panel-primary">
            <div class="panel-heading">
                AWG Pricing Analysis
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-sm datatable-awg">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>OBI</th>
                            <th>Item</th>
                            <th>Description</th>
                            <th>Size</th>
                            <th>Pack</th>
                            <th>Excise Tax</th>
                            <th>Case Cost</th>
                            <th>Unit Cost</th>
                            <th>Unit City</th>
                            <th>City GP%</th>
                            <th>Unit Rural</th>
                            <th>Rural GP%</th>
                            <th>BFL Vendor Id</th>
                            <th>BFL Dept</th>
                            <th>UPC</th>
                            <th>Qty Sold</th>
                            {{--<th>Wght Sold</th>--}}
                            <th>Amt Sold</th>
                            <th>BFL Unit Sold</th>
                            {{--<th>BFL Unit Sold / City</th>--}}
                            {{--<th>BFL Unit Sold / Rural</th>--}}
                            <th>BFL GP%</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $i = 0 @endphp
                        @foreach($awg_prices as $price)
                        @php $i++ @endphp
                        <tr>
                            <td>{{ $i }}</td>
                            <td>{{ $price->obi }}</td>
                            <td>{{ $price->item }}</td>
                            <td>{{ $price->description }}</td>
                            <td>{{ $price->size }}</td>
                            <td>{{ $price->pack }}</td>
                            <td>{{ $price->excise_tax }}</td>
                            <td>{{ $price->case_cost }}</td>
                            <td>{{ $price->unit_cost }}</td>
                            <td>{{ $price->unit_city }}</td>
                            <td>{{ $price->city_gp }}%</td>
                            <td>{{ $price->unit_rural }}</td>
                            <td>{{ $price->rural_gp }}%</td>
                            <td>{{ $price->bfl_vendor_id }}</td>
                            <td>{{ $price->bfl_dept }}</td>
                            <td>{{ $price->upc }}</td>
                            <td class="text-right">{{ $price->qty }}</td>
                            {{--<td class="text-right"></td>--}}
                            <td class="text-right">{{ $price->amount }}</td>
                            <td class="text-center">{{ number_format($price->bfl_unit_sold, 2) }}</td>
                            <td class="text-center">
                                <span style="color: {{ $price->bfl_gp_color }}">
                                    {{ number_format($price->bfl_gp, 2) }}%
                                </span>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="10">TOTAL</th>
                            <th class="text-center">{{ number_format($awg_prices->avg('city_gp'), 2) }}%</th>
                            <th class="text-center">&nbsp;</th>
                            <th class="text-center">{{ number_format($awg_prices->avg('rural_gp'), 2) }}%</th>
                            <th class="text-center" colspan="3"></th>
                            <th class="text-right">{{ number_format($awg_prices->sum('qty'), 2) }}</th>
                            <th class="text-right">${{ number_format($awg_prices->sum('amount'), 2) }}</th>
                            <th class="text-center">AVG ${{ number_format($awg_prices->avg('bfl_unit_sold'), 2) }}</th>
                            <th class="text-center">AVG {{ number_format($awg_prices->avg('bfl_gp'),2) }}%</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @endsection

@section('js')

    <script>
        $(document).ready(function(){

            var table = $('.datatable-awg').DataTable({
                dom: 'fBprtpi',
                pageLength: 50,
                buttons: [
                    {
                        extend: 'print',
                        exportOptions: {
                            columns: ':visible'

                        }
                    },
                    {
                        extend: 'copy',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'csv',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'excel',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'pdf',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },

                    "colvis"
                ]

            });

        });
    </script>

    @endsection