@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">Import AWG Prices File</div>
            <div class="panel-body">
                <form action="{{ action('AwgPriceController@store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="file">File</label>
                        <input type="file" class="form-control" name="file" required >
                        <label>Columns: OBI, Item, Description, Size, Pack, Excise Tax, CaseCost, UnitCost, Unit, City, City GP%, Unit, Rural, Rural GP%, BFL Unit, BFL Retail, BFL Vendor ID, BFL Dept, UPC</label>
                    </div>
                    <button class="btn btn-success btn-block">
                        Import
                    </button>
                </form>
            </div>
        </div>
    </div>

    @endsection