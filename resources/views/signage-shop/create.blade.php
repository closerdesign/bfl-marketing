@extends('layouts.app')

@section('style')

@endsection

@section('content')
    <div class="container">
        <p><a href="{{ action('SignageShopController@index') }}" class="btn btn-primary"><i class="fa fa-arrow-circle-left"></i> Go Back</a></p>
        <div class="panel panel-primary">
            <div class="panel-heading">Create New Signage Shop Campaign</div>
            <div class="panel-body">
                <form action="{{ action('SignageShopController@store') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Brand</label>
                                <select class="form-control" name="brand_id" required >
                                    <option value="" >Select...</option>
                                    @foreach(\App\Brand::orderBy('id')->get() as $brand)
                                        <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" required >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea name="description" id="description" cols="30" rows="2" class="form-control" maxlength="255" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Start Date</label>
                                <input type="date" class="form-control" name="start_date" required />
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>End Date</label>
                                <input type="date" class="form-control" name="end_date" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Cover Image</label>
                                <input type="file" class="form-control" name="cover_image" />
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Status</label>
                                <select class="form-control" name="status" required >
                                    {{--<option value="0" >Inactive</option>--}}
                                    <option value="1" selected>Active</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group text-right"><br />
                                <button class="btn btn-success">Create Campaign</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')

@endsection