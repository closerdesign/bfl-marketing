@extends('layouts.app')

@section('style')
    <style>
        .panel-heading {
            cursor: pointer;
        }
        .panel-title {
            font-size: 16px;
            line-height: 2em;
        }
        @media(max-width:400px) {
            .btn {
                margin-top: 10px;
            }
        }
    </style>
@endsection

@section('content')

    @php
        $brands = [
            '1' => 'buyforless',
            '2' => 'uptown',
            '3' => 'smartsaver',
            '4' => 'supermercado',
            '5' => 'uptown',
            '6' => 'buyforless'
        ]
    @endphp

<div class="container">
    <div class="row">
        @if(Auth::user()->isAdmin() == 'Admin')
            <div class="col-md-12 text-right">
                <a href="{{ action('SignageShopController@create') }}" class="btn btn-success"><i class="fa fa-plus"></i> Create New Campaign</a>
            </div>
        @endif
        <div class="col-md-12">
            <h3>Current Campaigns</h3>
        </div>
    </div>
    <div class="panel-group" id="accordion">
        @foreach($shops as $shop)
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $shop->id }}">
                    <h4 class="panel-title">
                        <img src="/img/logos/{{ $brands[$shop->brand_id] }}-logo.png" height="25" /> &nbsp;
                        {{ $shop->name }}
                    </h4>
                </div>
                <div id="collapse{{ $shop->id }}" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">
                                <img src="https://bfl-corp-sara.s3.us-west-2.amazonaws.com/signage-shop/{{ $shop->cover_image }}"
                                     class="img-responsive img-rounded" style="max-width: 200px" />
                            </div>
                            <div class="col-md-9">
                                @if(Auth::user()->isAdmin() == 'Admin')
                                    <a href="{{ action('SignageShopController@show', $shop->id) }}" class="btn btn-default pull-right">
                                        <i class="fa fa-edit"></i> Edit Campaign
                                    </a>
                                @endif
                                <h3>{{ $shop->name }}</h3>
                                <h6>{{ date('m/d/Y', strtotime($shop->start_date)) }} to {{ date('m/d/Y', strtotime($shop->end_date)) }}</h6>
                                <hr />
                                {{ $shop->description }}
                            </div>
                        </div>
                        <hr />
                        @foreach($items->whereIn('signage_shop_id', $shop->id) as $item)
                            <div class="row">
                                <div class="col-md-2">
                                    <img src="https://bfl-corp-sara.s3.us-west-2.amazonaws.com/signage-shop/{{ $item->cover_image }}"
                                         class="img-responsive img-rounded" style="max-height: 150px" />
                                </div>
                                <div class="col-md-10">
                                    <h4>{{ $item->name }}</h4>
                                    <p>
                                        {{ $item->description }}
                                    </p>
                                </div>
                                <div class="col-md-2">&nbsp;</div>
                                <div class="col-md-10">
                                    @if($item->download == 1)
                                        <a href="https://bfl-corp-sara.s3.us-west-2.amazonaws.com/signage-shop/{{ $item->file }}"
                                           class="btn btn-primary" target="_blank"><i class="fa fa-download"></i> Download</a> &nbsp;
                                    @endif
                                    @if($item->request == 1)
                                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#printRequest{{ $item->id }}">
                                            Request Printed Version
                                        </a> &nbsp;
                                    @endif
                                        @if(Auth::user()->isAdmin() == 'Admin')
                                        <a href="{{ action('SignageShopController@edit_item', $item->id) }}" class="btn btn-default">
                                            <i class="fa fa-edit"></i> Edit Item
                                        </a>
                                    @endif
                                </div>
                            </div>
                            <hr />
                            @if($item->request == 1)
                                <div id="printRequest{{ $item->id }}" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Printed Version Request</h4>
                                            </div>
                                            <form action="{{ action('SignageShopController@print_request', $item->id) }}" method="post" >
                                                {{ csrf_field() }}
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label for="qty">Quantity</label>
                                                            <input type="text" name="qty" class="form-control" required value="1">
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label for="store_id">Store</label>
                                                            <select class="form-control" id="store_id" name="store_id" required>
                                                                <option value="">Select...</option>
                                                                @if(Auth::user() &&  Auth::user()->isStore())
                                                                    <option value="{{ App\Store::where('store_code', Auth::user()->store)->first()->id }}" selected>{{ Auth::user()->store }}</option>
                                                                @else
                                                                    <option value="0">Support Center</option>
                                                                    @foreach(\App\Store::orderBy('id')->get() as $store)
                                                                        <option @if( old('store_id') == $store->id || Auth::user()->store == $store->id ) selected @endif value="{{ $store->id }}">{{ $store->store_code }}</option>
                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <label for="comments">Comments</label>
                                                            <textarea name="comments" id="comments" cols="30" rows="3" class="form-control">{{ old('comments') }}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" class="btn btn-primary">Send Request</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                        @if(Auth::user()->isAdmin() == 'Admin')
                            <a href="{{ action('SignageShopController@add_item', $shop->id) }}" class="btn btn-success pull-right">
                                <i class="fa fa-plus"></i> Add Item to Campaign
                            </a>
                        @endif
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>

@endsection

@section('js')

@endsection