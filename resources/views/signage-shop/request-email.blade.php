@extends('emails.layouts.general')

@section('content')

    @php
        $brands = [
            '1' => 'Buy For Less',
            '2' => 'Uptown Grocery Co',
            '3' => 'Smart Saver',
            '4' => 'SuperMercado',
            '5' => 'Uptown Grocery Co',
            '6' => 'Corporate'
        ]
    @endphp

    <h3><b>Signage Shop Request for Store {{ $user->store }}</h3>
    <p>Made By: </b>{{ $user->name }} ({{ $user->email }})</p>
    <p><b>Campaign:</b> [{{ $brands[$shop->brand_id] }}] {{ $shop->name }}</p>
    <p><b>Campaign Description:</b> {{ $shop->description }}</p>
    <p style="text-align: center">
        <img src="https://bfl-corp-sara.s3.us-west-2.amazonaws.com/signage-shop/{{ $item->cover_image }}"
             class="img-responsive img-rounded" style="max-height: 250px" />
    </p>
    <p><b>Item:</b> {{ $item->name }}</p>
    <p><b>Description:</b> {{ $item->description }}</p>
    @if($item->file !== '')
    <p>
        <b>File:</b> <a href="https://bfl-corp-sara.s3.us-west-2.amazonaws.com/signage-shop/{{ $item->file }}" target="_blank">{{ $item->file }}</a>
    </p>
    @endif
    <p><b>Quantity:</b> {{ $request->qty }}</p>
    <p><b>Comments:</b> {{ $request->comments }}</p>

@endsection