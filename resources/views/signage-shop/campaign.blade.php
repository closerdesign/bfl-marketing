@extends('layouts.app')

@section('style')

@endsection

@section('content')

    <div class="container">
        <p>
            <a href="{{ action('SignageShopController@index') }}" class="btn btn-primary">
                <i class="fa fa-arrow-circle-left"></i> Go Back
            </a>
            <a href="{{ action('SignageShopController@destroy', $shop->id) }}" class="btn btn-danger pull-right"><i class="fa fa-trash"></i> Delete Campaign</a>
        </p>
        <div class="panel panel-primary">
            <div class="panel-heading">{{ $shop->name }} - Edit Campaign</div>
            <div class="panel-body">
                <form action="{{ action('SignageShopController@update', $shop->id) }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Brand</label>
                                <select class="form-control" name="brand_id" required >
                                    <option value="" >Select...</option>
                                    @foreach(\App\Brand::orderBy('id')->get() as $brand)
                                        <option value="{{ $brand->id }}" @if($shop->brand_id == $brand->id) selected @endif>{{ $brand->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" value="{{ $shop->name }}" required >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea name="description" id="description" cols="30" rows="2" class="form-control" maxlength="255" required>{{ $shop->description }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Start Date</label>
                                <input type="date" class="form-control" name="start_date" value="{{ $shop->start_date }}" required />
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>End Date</label>
                                <input type="date" class="form-control" name="end_date" value="{{ $shop->end_date }}" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Cover Image</label>
                                <input type="file" class="form-control" name="cover_image" />
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Status</label>
                                <select class="form-control" name="status" required >
                                    <option value="0" @if($shop->status == 0) selected @endif>Inactive</option>
                                    <option value="1" @if($shop->status == 1) selected @endif>Active</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <img src="https://bfl-corp-sara.s3.us-west-2.amazonaws.com/signage-shop/{{ $shop->cover_image }}"
                                 class="img-responsive img-rounded" style="max-width: 200px" />
                        </div>
                        <div class="col-md-6">
                            <div class="form-group text-right"><br />
                                <button class="btn btn-success">Submit Changes</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>



@endsection

@section('js')

@endsection