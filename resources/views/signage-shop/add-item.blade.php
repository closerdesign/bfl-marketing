@extends('layouts.app')

@section('style')
    <style>
        .checkbox > label {
            text-transform: capitalize;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <p><a href="{{ action('SignageShopController@index') }}" class="btn btn-primary"><i class="fa fa-arrow-circle-left"></i> Go Back</a></p>
        <div class="panel panel-primary">
            <div class="panel-heading">Add Item to {{ $shop->name }}</div>
            <div class="panel-body">
                <form action="{{ action('SignageShopController@create_item') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" required >
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Cover Image</label>
                                <input type="file" class="form-control" name="cover_image" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea name="description" id="description" cols="30" rows="2" class="form-control" maxlength="255" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>File</label>
                                <input type="file" class="form-control" name="file" />
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Status</label>
                                <select class="form-control" name="status" required >
                                    {{--<option value="0" >Inactive</option>--}}
                                    <option value="1" selected>Active</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="checkbox">
                                <label><input type="checkbox" name="download" value="1" checked>Download</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="checkbox">
                                <label><input type="checkbox" name="request" value="1">Request</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group text-right">
                                <button class="btn btn-success">Add Item</button>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="signage_shop_id" value="{{ $shop->id }}" />
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')

@endsection