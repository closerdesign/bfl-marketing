@extends('layouts.app')

@section('style')
    <style>
        .checkbox > label {
            text-transform: capitalize;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <p>
            <a href="{{ action('SignageShopController@index') }}" class="btn btn-primary">
                <i class="fa fa-arrow-circle-left"></i> Go Back
            </a>
            <a href="{{ action('SignageShopController@destroy_item', $item->id) }}" class="btn btn-danger pull-right">
                <i class="fa fa-trash"></i> Delete Item
            </a>
        </p>
        <div class="panel panel-primary">
            <div class="panel-heading">Edit Item</div>
            <div class="panel-body">
                <form action="{{ action('SignageShopController@update_item', $item->id) }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" value="{{ $item->name }}" required >
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="form-group">
                                <label>Cover Image</label>
                                <input type="file" class="form-control" name="cover_image" />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <img src="https://bfl-corp-sara.s3.us-west-2.amazonaws.com/signage-shop/{{ $item->cover_image }}"
                                 class="img-responsive img-rounded" style="max-width: 100px" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea name="description" id="description" cols="30" rows="2" class="form-control" maxlength="255" required>{{ $item->description }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>File</label>
                                <small>
                                    @if($item->file !== '')
                                    (<a href="https://bfl-corp-sara.s3.us-west-2.amazonaws.com/signage-shop/{{ $item->file }}" target="_blank">{{ $item->file }}</a>)
                                    @else
                                        <span class="text-muted">(No File Uploaded)</span>
                                    @endif
                                </small>
                                <input type="file" class="form-control" name="file" />
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Status</label>
                                <select class="form-control" name="status" required >
                                    <option value="0" @if($item->status == 0) selected @endif>Inactive</option>
                                    <option value="1" @if($item->status == 1) selected @endif>Active</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Download Option</label>
                                <select class="form-control" name="download" required >
                                    <option value="0" @if($item->download == 0) selected @endif>No</option>
                                    <option value="1" @if($item->download == 1) selected @endif>Yes</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Request Option</label>
                                <select class="form-control" name="request" required >
                                    <option value="0" @if($item->request == 0) selected @endif>No</option>
                                    <option value="1" @if($item->request == 1) selected @endif>Yes</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group text-right">
                                <button class="btn btn-success">Update Item</button>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="signage_shop_id" value="{{ $item->signage_shop_id }}" />
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')

@endsection