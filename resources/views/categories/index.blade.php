@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <p class="text-right">
            <a class="btn btn-success" href="{{ action('CategoriesController@create') }}">
                <i class="fa fa-plus"></i> Add A New Category
            </a>
        </p>
        <div class="panel panel-default">
            <div class="panel-heading">Categories List</div>
            <div class="panel-body">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Name ES</th>
                            <th>Parent Category</th>
                            <th> </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach(\App\Category::orderBy('name')->get() as $category)
                        <tr>
                            <td>
                                <a href="{{ action('CategoriesController@edit', $category->id) }}">
                                    {{ $category->name }}
                                </a>
                            </td>
                            <td>@if(isset($category->name_es))
                                    {{$category->name_es}}
                                @endif
                            </td>
                            <td>
                                @if(!empty($category->parent))
                                {{ $category->parent->name }}
                                @endif
                            </td>
                            <td> </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @endsection