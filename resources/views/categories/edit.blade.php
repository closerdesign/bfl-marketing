@extends('layouts.app')

<style>
    /* The switch - the box around the slider */
    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    /* Hide default HTML checkbox */
    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    /* The slider */
    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #2196F3;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }


</style>

@section('content')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <div class="container">
        <form action="{{ action('CategoriesController@update', $category->id) }}" method="post">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="panel panel-primary">
                <div class="panel-heading">Edit Category: {{ $category->name }}</div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="name" value="{{ $category->name }}" required >
                    </div>
                    <div class="form-group">
                        <label for="name_es">Name ES</label>
                        <input type="text" class="form-control" name="name_es" value="{{ $category->name_es }}" required >
                    </div>
                    <div class="form-group">
                        <label for="parent_id">Parent</label>
                        <select name="parent_id" id="parent_id" class="form-control">
                            <option value="">Select...</option>
                            @foreach(\App\Category::where('parent_id', '<', 1)->where('id', '!=', $category->id)->get() as $parent)
                                <option @if( $category->parent_id == $parent->id ) selected @endif value="{{ $parent->id }}">{{ $parent->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="switch">
                            <input type="checkbox" id="activeCat" class="form-control" name="active" @if($category->active == 1) checked @endif>
                            <span class="slider round"></span>
                        </label> @if($category->active == 1) Active @endif
                    </div>

                   <!-- <div class="form-group col-lg-2">
                        <label for="expire_date">Expire Date</label>
                        <input type="date" id="expire_date" class="form-control" name="expire_date" value="{{@$category->expire_date}}">
                    </div>
                    -->

                </div>
            </div>
            <button class="btn btn-success pull-right">
                <i class="fa fa-save"></i> Save Changes
            </button>
        </form>

    </div>

@endsection