@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <p class="lead text-center">@lang('store-level.add-email-subscriber')</p>
                <form action="{{ action('MailingListsController@store') }}" method="post" >
                    {{ csrf_field() }}
                    <div class="form-group">
                        <input type="text" class="form-control input-lg" name="name" value="{{ old('name') }}" placeholder="@lang('general.name')" >
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control input-lg" name="email" value="{{ old('email') }}" placeholder="@lang('general.email')" required >
                    </div>
                    <div class="form-group">
                        <select class="form-control input-lg" id="brand" name="brand">
                            <option>@lang('general.select') @lang('general.brand')...</option>
                            @foreach(\App\Brand::orderBy('id')->get() as $brand)
                                <option @if( old('brand') == $brand->id ) selected @endif value="{{ $brand->id }}">{{ $brand->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="text-center">
                        <button class="btn btn-success btn-block">@lang('store-level.add-subscriber')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection