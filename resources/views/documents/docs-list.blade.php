@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Documents List
            </div>
            <div class="panel-body">
                <ul class="list-group">
                    @foreach($docs as $doc)
                    <li class="list-group-item">
                        <a target="_blank" href="{{ $doc->file }}"><span class="label label-default"><i class="fa fa-download"></i> Download</span></a> {{ $doc->name }}
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

    @endsection