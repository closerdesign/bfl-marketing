@extends('layouts.app')

@section('content')

    <div class="container-fluid">

        <p class="text-right">
            <a href="{{ action('DocumentsController@create') }}" class="btn btn-success">
                <i class="fa fa-plus-circle"></i> Add A New Document
            </a>
        </p>

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-file"></i> Documents
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-condensed">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>File</th>
                            <th>Status</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($documents as $document)
                            <tr>
                                <th>
                                    <a href="{{ action('DocumentsController@edit', $document->id) }}">
                                        {{ $document->name }}
                                    </a>
                                </th>
                                <td>
                                    <a href="{{ $document->file }}" target="_blank">
                                        <i class="fa fa-download"></i> Download
                                    </a>
                                </td>
                                <td>
                                    <span class="label label-default">
                                        {{ $document->status }}
                                    </span>
                                </td>
                                <td class="text-right">
                                    <div class="input group">
                                        <form onsubmit="return confirm('Are You Sure?')" action="{{ action('DocumentsController@destroy', $document->id) }}" method="post">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button class="btn btn-danger">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                            <a href="{{ action('DocumentsController@show', $document->id) }}" class="btn btn-xs btn-success" target="_blank">
                                                <i class="fa fa-link"></i>
                                            </a>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $documents->render() }}
                </div>
            </div>
        </div>
    </div>

@endsection