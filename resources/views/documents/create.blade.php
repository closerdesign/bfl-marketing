@extends('layouts.app')

@section('content')

    <div class="container">
        <form action="{{ action('DocumentsController@store') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <i class="fa fa-file"></i> Create A New Document
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="name" value="{{ old('name') }}" required >
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea name="description" id="description" cols="30" rows="5"
                                  class="form-control summernote">{{ old('description') }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="file">File</label>
                        <input type="file" class="form-control" name="file" required >
                    </div>
                    <div class="form-group">
                        <label for="status">Status</label>
                        <select name="status" id="status" class="form-control" required >
                            <option value="">Select...</option>
                            <option @if( old('status') == '1' ) selected @endif  value="1">Published</option>
                            <option @if( old('status') == '0' ) selected @endif value="0">Unpublished</option>
                        </select>
                    </div>
                </div>
            </div>
            <button class="btn btn-success">
                <i class="fa fa-save"></i> Save Changes
            </button>
        </form>
    </div>

@endsection