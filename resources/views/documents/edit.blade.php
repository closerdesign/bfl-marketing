@extends('layouts.app')

@section('content')

    <div class="container">
        <form action="{{ action('DocumentsController@update', $document->id) }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <i class="fa fa-file"></i> Editing Document: {{ $document->name }}
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="name" value="{{ $document->name }}" required >
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea name="description" id="description" cols="30" rows="5"
                                  class="form-control summernote">{!! $document->description !!}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="file">File <a href="{{ $document->file }}" target="_blank" ><i class="fa fa-download"></i></a></label>
                        <input type="file" class="form-control" name="file" >
                    </div>
                    <div class="form-group">
                        <label for="status">Status</label>
                        <select name="status" id="status" class="form-control" required >
                            <option value="">Select...</option>
                            <option @if( $document->status == 'Published' ) selected @endif  value="1">Published</option>
                            <option @if( $document->status == 'Unpublished' ) selected @endif value="0">Unpublished</option>
                        </select>
                    </div>
                </div>
            </div>
            <button class="btn btn-success">
                <i class="fa fa-save"></i> Save Changes
            </button>
        </form>
    </div>

@endsection