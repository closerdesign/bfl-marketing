@extends('layouts.app-wo-menu')

@section('content')

    <div class="container">
        <hr>
        <div class="panel panel-default">
            <div class="panel-heading">
                {{ $document->name }}
            </div>
            <div class="panel-body">
                {!! $document->description !!}
            </div>
            <div class="panel-footer">
                <a href="{{ $document->file }}" class="btn btn-success form-control">
                    Click Here To Download <i class="fa fa-download"></i>
                </a>
            </div>
        </div>
    </div>

    @endsection