@extends('layouts.app')

@section('content')

    <div class="container-fluid">

        <div class="form-group">
            <div class="dropdown">
                <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Export <span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a target="_blank" href="{{ route('digital-signage-image', [$special->id, 'digital-signage']) }}" > Digital Signage Image</a></li>
                    <li><a target="_blank" href="{{ route('digital-signage-image', [$special->id, 'facebook']) }}" > Facebook Image</a></li>
                    <li><a target="_blank" href="{{ route('digital-signage-image', [$special->id, 'billboard']) }}" > Billboard Image</a></li>
                </ul>
            </div>
        </div>

        <form action="{{ action('SpecialsController@update', $special->id) }}" method="post"
              enctype="multipart/form-data">
            <div class="panel panel-primary">
                <div class="panel-heading">Editing:<br />{!! $special->item_description !!}</div>
                <div class="panel-body">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}
                    <input type="hidden" name="ad_id" value="{{ $special->ad_id }}">
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <p class="text-right">
                                @if($special->image == "")
                                    <img id="itemImg" width="150" src="/img/pending-image.jpg" alt="{{ $special->item_description }}">
                                @else
                                    <img id="itemImg" width="150" src="{{ $special->image }}" alt=""><br />
                                    <span id="imgDimensions"></span>
                                @endif
                            </p>
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="item_description">Item Description</label>
                            <input type="text" class="form-control" name="item_description"
                                   value="{{ $special->item_description }}" required>
                        </div>

                        <div class="col-md-3 form-group">
                            <label for="size">Size</label>
                            <input type="text" class="form-control" name="size" value="{{ $special->size }}">
                        </div>
                        <div class="col-md-3 form-group">
                            <label for="sku">UPC (For Image)</label>
                            <input type="text" class="form-control" name="sku" value="{{ $special->sku }}" autocomplete="off">
                        </div>
                        <div class="col-md-3 form-group">
                            <label for="status">Status</label>
                            <select name="status" id="status" class="form-control">
                                <option value="">Select...</option>
                                <option @if( $special->status == "Pending" ) selected @endif value="0">Pending</option>
                                <option @if( $special->status == "Approved" ) selected @endif value="1">Approved
                                </option>
                            </select>
                        </div>
                        <div class="col-md-3 form-group">
                            <label for="cost">Net Cost</label>
                            <input class="form-control meats-math" name="cost" id="cost" value="{{ $special->cost }}">
                        </div>
                        <div class="col-md-3 form-group">
                            <label for="price">Ad Retail</label>
                            <input type="text" class="form-control meats-math" name="price" id="price"
                                   value="{{ $special->price }}">
                        </div>
                        <div class="col-md-3 form-group">
                            <label for="alt_price_format">Use Alternative Price Format</label>
                            <select name="alt_price_format" id="alt_price_format" class="form-control">
                                <option value="">Select...</option>
                                <option @if( $special->alt_price_format == true ) selected @endif value="1">Yes</option>
                                <option @if( $special->alt_price_format == false ) selected @endif value="0">No</option>
                            </select>
                        </div>
                        <div class="col-md-3 form-group">
                            <label for="srp">Regular Retail</label>
                            <input type="text" class="form-control" name="srp" value="{{ $special->srp }}">
                        </div>
                        <div class="col-md-3 form-group">
                            <label for="position">Position</label>
                            <input type="text" class="form-control text-center" name="position" value="{{ $special->position }}">
                        </div>
                        <div class="col-md-3 form-group">
                            <label for="departament_id">Dept.</label>
                            <select name="department_id[]" id="department_id" class="form-control multiple-select"
                                    multiple="multiple" required>
                                <option value="">Select...</option>
                                @foreach($departments as $department)
                                    <option
                                            value="{{ $department->id }}"
                                            @if( in_array($department->id, $special->department()->select('department_id')->pluck('department_id')->toArray()) )
                                            selected
                                            @endif >
                                        {{ $department->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3 form-group">
                            <label for="item_info_location">Item Info Location</label>
                            <input type="text" class="form-control" name="item_info_location"
                                   value="{{ $special->item_info_location }}">
                        </div>
                        <div class="col-md-3 form-group">
                            <label for="image">Image (500x500)</label>
                            <input type="file" class="form-control" name="image">
                        </div>
                        <div class="col-md-6">
                            <label for="alt_description">@lang('specials.alt-description')</label>
                            <input type="text" class="form-control" name="alt_description" value="{{ $special->alt_description }}" max="48">
                        </div>
                        <div class="col-md-12 checkbox">
                            <label><input type="checkbox" name="plu_required" value="1" @if( $special->plu_required == true ) checked @endif > Is PLU required?</label>
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="position_number">Position Number</label>
                            <input type="number" class="form-control text-center" name="position_number" value="{{ $special->position_number }}">
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="plu">PLU</label>
                            <input type="number" class="form-control" name="plu" value="{{ $special->plu }}" >
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="promo_number">Promo Number</label>
                            <input type="text" class="form-control" name="promo_number" value="{{ $special->promo_number }}" >
                        </div>
                        <div class="col-md-12 form-group">
                            <label for="comments">Comments</label>
                            <textarea name="comments" id="comments" cols="30" rows="4"
                                      class="form-control">{{ $special->comments }}</textarea>
                        </div>
                        <div class="col-md-12 form-group">
                            <label for="public_comment">Public Comment</label>
                            <input type="text" class="form-control" name="public_comment" value="{{ $special->public_comment }}">
                        </div>
                        <div class="col-md-12 form-group">
                            <label for="additional_upcs">Additional UPCs</label>
                            <textarea name="additional_upcs" id="additional_upcs" cols="30" rows="2"
                                      class="form-control">{{ $special->additional_upcs }}</textarea>
                            <label>Separated by comma.</label>
                        </div>
                        <div class="col-md-2 form-group">
                            <label for="order_no">Order #</label>
                            <input type="text" class="form-control" name="order_no" value="{{ $special->order_no }}">
                        </div>
                        <div class="col-md-2 form-group">
                            <label for="limit">Limit</label>
                            <input type="text" class="form-control" name="limit" value="{{ $special->limit }}">
                        </div>
                        <div class="col-md-2 form-group">
                            <label for="yield">Yield</label>
                            <div class="input-group">
                                <input type="number" class="form-control meats-math" name="yield" id="yield"
                                       value="{{ $special->yield }}">
                                <div class="input-group-addon">%</div>
                            </div>
                        </div>
                        <div class="col-md-2 form-group">
                            <label for="adj_cost">Adj Cost</label>
                            <input type="text" class="form-control meats-math" name="adj_cost" id="adj_cost"
                                   value="{{ $special->adj_cost }}" readonly>
                        </div>
                        <div class="col-md-2 form-group">
                            <label for="ad_gross">Ad Gross %</label>
                            <div class="input-group">
                                <input type="text" class="form-control meats-math" name="ad_gross" id="ad_gross"
                                       value="{{ $special->ad_gross }}" readonly>
                                <div class="input-group-addon">%</div>
                            </div>
                        </div>
                        <div class="col-md-2 form-group">
                            <label for="supplier">Supplier</label>
                            <input type="text" class="form-control" name="supplier" value="{{ $special->supplier }}">
                        </div>
                        <div class="col-md-12 form-group">
                            <label>
                                <input type="checkbox" name="featured" value="1"
                                       @if( $special->featured == 1 ) checked @endif > This is a featured product
                            </label>
                        </div>
                        <div class="col-md-12 form-group">
                            <label>
                                <input type="checkbox" @if( $special->mobile_app_deal == 1 ) checked @endif name="mobile_app_deal" value="1"> @lang('specials.is-mobile-app-deal')
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <button class="btn btn-success btn-block" type="submit"><i class="fa fa-save"></i> Save changes</button>
        </form>
    </div>

@endsection

@section('js')
<script>

    $('.meats-math').change(function () {

        var ad_retail_val = $('#price').val();
        var yield_val = $('#yield').val();
        var cost_val = $('#cost').val();

        var adj_cost_val = (parseFloat(cost_val) / parseFloat(yield_val)) * 100;

        var ad_gross = ((parseFloat(ad_retail_val) - parseFloat(adj_cost_val)) / parseFloat(ad_retail_val) * 100);

        $('#adj_cost').attr("value", numeral(adj_cost_val).format('0.00'));
        $('#ad_gross').attr("value", numeral(ad_gross).format('0.00'));

    });

    $(document).ready(function(){
        var img = document.querySelector("#itemImg");
        if (img.naturalWidth > 0) {
            $('#imgDimensions').text(img.naturalWidth + 'x' + img.naturalHeight);
        }

        upcs_reload();
    });

</script>
@endsection