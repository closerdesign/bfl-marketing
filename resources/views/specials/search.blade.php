@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="panel panel-primary">
            <div class="panel-heading"><i class="fa fa-search"></i> Specials Search</div>
            <div class="panel-body">
                <form action="">
                    <div class="form-group">
                        <label for="keyword">Keyword</label>
                        <input type="text" class="form-control" name="keyword" required >
                    </div>
                    <button class="btn btn-success" type="submit">
                        <i class="fa fa-search"></i> Search Now!
                    </button>
                </form>
            </div>
        </div>

        @if( isset($_GET['keyword']) )

        <div class="panel panel-primary">
            <div class="panel-heading">Results for your search: <b style="text-transform: uppercase">{{ $_GET['keyword'] }}</b></div>
            <div class="panel-body">
                @foreach($specials as $special)
                <div class="panel panel-default">
                    <div class="panel-heading"><b>{{ $special->item_description }}</b> @if( $special->sku != "" ) - UPC: {{ $special->sku }} @endif</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">

                                @if( $special->image != "" )
                                <p>
                                    <img src="{{ $special->image }}" alt="{{ $special->item_description }}" width="90">
                                </p>
                                @endif

                                {{--@if( $special->ad->name && $special->ad->brands->name && $special->ad->date_from && $special->ad->date_to )--}}
                                {{--<p>--}}
                                    {{--<b>Used in the Ad:</b><br />--}}
                                    {{--{{ $special->ad->name }} - {{ $special->ad->brands->name }}<br />--}}
                                    {{--From {{ $special->ad->date_from }} to {{ $special->ad->date_to }}--}}
                                {{--</p>--}}
                                {{--@endif--}}

                            </div>
                            <div class="col-md-6">
                                <form onsubmit="return confirm('Are you sure?')" action="{{ action('SpecialsController@cloning', $special->id) }}" method="post">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="ad">Select the destination Ad</label>
                                        <select name="ad" id="ad" class="form-control">
                                            <option value="">Select...</option>
                                            @foreach(\App\Ad::where('date_to', ">=", date('Y-m-d'))->orderBy('brand_id')->orderBy('date_from')->get() as $ad)
                                                <option value="{{ $ad->id }}">{{ $ad->brands->name }} - {{ $ad->name }} - {{ $ad->date_from }} - {{ $ad->date_to }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <button class="btn btn-success pull-right" type="submit"><i class="fa fa-copy"></i> Clone This Item</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <b>Created at:</b> {{ $special->created_at }}
                    </div>
                </div>
                @endforeach
            </div>
        </div>

        @endif

    </div>

    @endsection