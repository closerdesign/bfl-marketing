@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">Cloning Item: {{ $special->item_description }}</div>
            <div class="panel-body">
                <form action="{{ action('SpecialsController@cloning', $special->id) }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="ad">Select the destination Ad</label>
                        <select name="ad" id="ad" class="form-control">
                            <option value="">Select...</option>
                            @foreach(\App\Ad::where('date_to', ">=", date('Y-m-d'))->orderBy('brand_id')->orderBy('date_from')->get() as $ad)
                                <option value="{{ $ad->id }}">{{ $ad->brands->name }} - {{ $ad->name }} - {{ $ad->date_from }} - {{ $ad->date_to }}</option>
                            @endforeach
                        </select>
                    </div>
                    <button class="btn btn-primary" type="submit">Clone</button>
                </form>
            </div>
        </div>
    </div>

    @endsection