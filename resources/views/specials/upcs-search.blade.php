<table class="table table-striped table-condensed">
    <thead>
    <tr>
        <th>&nbsp;</th>
        <th>UPC</th>
        <th>Brand</th>
        <th>Product Name</th>
        <th>&nbsp;</th>
    </tr>
    </thead>
    <tbody>
    @foreach($products as $product)
    <tr>
        <td><img src="{{ $product->image }}" alt="{{ $product->name }}" width="20" height="20"></td>
        <td>{{ $product->upc }}</td>
        <td>{{ $product->brand_name }}</td>
        <td>{{ $product->name }}</td>
        <td>
            <button onclick="upc_add('{{ $product->upc }}')" class="btn-xs btn-success">
                Add
            </button>
        </td>
    </tr>
    @endforeach
    </tbody>
</table>