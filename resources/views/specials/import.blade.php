@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Import Specials
                    </div>
                    <div class="panel-body">
                        <form action="{{ action('SpecialsController@import') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <input type="file" class="form-control" name="file" required>
                                <label>Please include these Columns in your file in the following order: Item Description, Ad Retail (TPR), Regular Retail (SRP), Ad ID, Size, UPC.</label>
                            </div>
                            <button class="btn btn-success btn-block">
                                Import
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection