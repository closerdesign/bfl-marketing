@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="/ads">Ad Files</a> > {{ $ad->name }} - {{ $ad->brands->name }}
                    </div>
                    <div class="panel-body">
                        <div class="col-md-4">
                            <form action="{{ action('AdfilesController@store') }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="ad" value="{{ $ad->id }}">
                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <label for="file">File</label>
                                        <input type="file" class="form-control" name="file" required >
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <button class="btn btn-primary form-control" type="submit">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-8">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>File</th>
                                        <th width="20%">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($files as $file)
                                        <tr>
                                            <th><a href="{{ $file->file }}" target="_blank">{{ $file->file }}</a></th>
                                            <td class="text-center">
                                                <form action="{{ action('AdfilesController@destroy', $file->id) }}" method="post">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                    <button class="btn btn-default" type="submit"><i class="fa fa-trash"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <p class="pull-right">
                                <a class="btn btn-default" href="{{ action('AdpagesController@show', $ad->id) }}">
                                    Ad Pages <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection