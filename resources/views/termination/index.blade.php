@extends('layouts.app')

@section('content')

    <div class="container">
        <p class="text-right">
            <a class="btn btn-success" href="{{ action('TerminationController@create') }}"><i class="fa fa-plus"></i> New Termination Request</a>
        </p>
        <div class="panel panel-primary">
            <div class="panel-heading">Termination Requests</div>

            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Store</th>
                            <th>Completed By</th>
                            <th>Created</th>
                            {{--<th width="20%">Actions</th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($terminations) < 1)
                            <tr><td colspan="4" class="text-center">No Termination Requests added.</td></tr>
                        @endif
                        @foreach($terminations as $termination)

                            <tr>
                                <td>
                                    <a href="{{ action('TerminationController@show', $termination->id) }}">
                                        {{ $termination->first_name }} {{ $termination->last_name }}
                                    </a>
                                </td>
                                <td>
                                   @if($termination->store == '0') Support Center @else {{ $termination->store }} @endif
                                </td>
                                <td>{{ $termination->completed_by }}</td>
                                <td>{{ $termination->created_at->format('m/d/Y &\nb\sp; g:i A') }}</td>
                                {{--<td class="text-center">--}}
                                    {{--<form onsubmit="return confirm('Are you sure? This can\'t be undone.')" action="{{ action('TerminationRequest@destroy', $brand->id) }}">--}}
                                        {{--{{ csrf_field() }}--}}
                                        {{--{{ method_field('DELETE') }}--}}
                                        {{--<button class="btn btn-xs btn-danger" type="submit"><i class="fa fa-trash"></i></button>--}}
                                    {{--</form>--}}
                                {{--</td>--}}
                            </tr>

                        @endforeach
                        </tbody>
                    </table>
                    {{ $terminations->render() }}
                </div>
            </div>
        </div>
    </div>

@endsection