@extends('layouts.app')

@section('content')

    <div class="container">
        <p>
            <a href="{{ action('TerminationController@index') }}" class="btn btn-primary">
                <i class="fa fa-arrow-circle-left"></i> Go Back
            </a>
        </p>
        <div class="panel panel-primary">
            <div class="panel-heading">Termination Request For {{ $termination->first_name }} {{ $termination->last_name }}</div>
            <div class="panel-body">

                <p><b>Name:</b> {{ $termination->first_name }} {{ $termination->last_name }}</p>
                <p><b>Store:</b> {{ $termination->store }}</p>
                <p><b>Department:</b> {{ $termination->department }}</p>
                <p><b>Date of Termination:</b> {{ $termination->termination_date }}</p>
                <p><b>Last Day Worked:</b> {{ $termination->last_day }}</p>
                <p><b>Termination Type:</b> {{ $termination->termination_type }}</p>
                <p><b>Reason for Termination:</b> {{ $termination->reason }}</p>
                <p><b>If Voluntary, Other Job Details:</b> {{ $termination->other_details }}</p>
                <p><b>Eligible for Rehire:</b> {{ $termination->rehire }}</p>
                <p><b>2-week Notice Given:</b> {{ $termination->notice_given }}</p>
                <p><b>Replacement Hire Needed:</b> {{ $termination->replacement }}</p>
                <p><b>Notes:</b> {{ $termination->notes }}</p>
                <p><b>Attachment:</b> <a href="{{ Storage::url('termination/' . $termination->attachment) }}" target="_blank">{{ $termination->attachment }}</a></p>
                <p><b>Completed By:</b> {{ $termination->completed_by }}</p>

            </div>
        </div>
    </div>

@endsection