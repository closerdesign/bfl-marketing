@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">@lang('forms.termination')</div>
            <div class="panel-body">
                <form action="{{ action('TerminationController@store') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <span style="color:red;">*</span>
                                    <label>@lang('forms.first-name')</label>
                                    <input type="text" class="form-control" name="first_name" required />
                                </div>
                                <div class="col-md-6 form-group">
                                    <span style="color:red;">*</span>
                                    <label>@lang('forms.last-name')</label>
                                    <input type="text" class="form-control" name="last_name" required />
                                </div>
                                <div class="col-md-6 form-group">
                                    <span style="color:red;">*</span>
                                    <label>@lang('forms.store')</label>
                                    <select class="form-control" name="store" required >
                                        <option value="">@lang('forms.select')</option>
                                        @if(Auth::user()->isStore())
                                            <option value="{{ Auth::user()->store }}">{{ Auth::user()->store }}</option>
                                        @else
                                        <option value="1006" >1006</option>
                                        <option value="1201" >1201</option>
                                        <option value="1205" >1205</option>
                                        <option value="1230" >1230</option>
                                        <option value="2001" >2001</option>
                                        <option value="2701" >2701</option>
                                        <option value="3501" >3501</option>
                                        <option value="3701" >3701</option>
                                        <option value="3713" >3713</option>
                                        <option value="4150" >4150</option>
                                        <option value="4424" >4424</option>
                                        <option value="7957" >7957</option>
                                        <option value="9515" >9515</option>
                                        <option value="0" >Support Center</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="col-md-6 form-group">
                                    <span style="color:red;">*</span>
                                    <label>@lang('forms.department')</label>
                                    <input type="text" class="form-control" name="department" required />
                                </div>
                                <div class="col-md-6 form-group">
                                    <span style="color:red;">*</span>
                                    <label>@lang('forms.term-date')</label>
                                    <input type="date" class="form-control" name="termination_date" required />
                                </div>
                                <div class="col-md-6 form-group">
                                    <span style="color:red;">*</span>
                                    <label>@lang('forms.last-day')</label>
                                    <input type="date" class="form-control" name="last_day" required />
                                </div>
                                <div class="col-md-6 form-group">
                                    <span style="color:red;">*</span>
                                    <label>@lang('forms.term-type')</label>
                                    <select class="form-control" name="termination_type" required >
                                        <option value="">@lang('forms.select')</option>
                                        <option value="Voluntary">Voluntary</option>
                                        <option value="Involuntary">Involuntary</option>
                                    </select>
                                </div>
                                <div class="col-md-6 form-group">
                                    <span style="color:red;">*</span>
                                    <label>Reason For Termination</label>
                                    <select class="form-control" name="reason" required >
                                        <option value="">Select...</option>
                                        <option value="Abandoned shift">@lang('forms.abandoned')</option>
                                        <option value="Compensation">@lang('forms.compensation')</option>
                                        <option value="Dishonest">@lang('forms.dishonest')</option>
                                        <option value="Found Another Job">@lang('forms.other-job')</option>
                                        <option value="Misconduct">@lang('forms.misconduct')</option>
                                        <option value="Move Out of State">@lang('forms.move-out')</option>
                                        <option value="No Call/No Show">@lang('forms.no-call')</option>
                                        <option value="Performance">@lang('forms.performance')</option>
                                        <option value="Project Ended">@lang('forms.project-end')</option>
                                        <option value="Quit - No Notice">@lang('forms.no-notice')</option>
                                        <option value="Reduction in Hours">@lang('forms.hours-reduction')</option>
                                        <option value="Retirement">@lang('forms.retirement')</option>
                                        <option value="Returned to School">@lang('forms.returned-school')</option>
                                        <option value="See Notes">@lang('forms.see-notes')</option>
                                        <option value="Terminated with Cause">@lang('forms.with-cause')</option>
                                        <option value="Theft">@lang('forms.theft')</option>
                                        <option value="Transfer">@lang('forms.transfer')</option>
                                        <option value="Voluntary Resignation">@lang('forms.resignation')</option>
                                    </select>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label>@lang('forms.other-details')</label>
                                    <textarea class="form-control" name="other_details"></textarea>
                                </div>

                                <div class="col-md-6 form-group">
                                    <span style="color:red;">*</span>
                                    <label>@lang('forms.rehire')</label>
                                    <select class="form-control" name="rehire" required >
                                        <option value="">@lang('forms.select')</option>
                                        <option value="Yes">@lang('forms.yes')</option>
                                        <option value="No">@lang('forms.no')</option>
                                    </select>
                                </div>
                                <div class="col-md-6 form-group">
                                    <span style="color:red;">*</span>
                                    <label>@lang('forms.notice-given')</label>
                                    <select class="form-control" name="notice_given" required >
                                        <option value="">@lang('forms.select')</option>
                                        <option value="Yes">@lang('forms.yes')</option>
                                        <option value="No">@lang('forms.no')</option>
                                    </select>
                                </div>
                                <div class="col-md-12 form-group">
                                    <span style="color:red;">*</span>
                                    <label>@lang('forms.replacement')</label>
                                    <textarea class="form-control" name="replacement" required ></textarea>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label>@lang('forms.notes')</label>
                                    <textarea class="form-control" name="notes"></textarea>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label>@lang('forms.attachment')</label>
                                    <input type="file" class="form-control" name="attachment" />
                                </div>
                                <div class="col-md-12 form-group">
                                    <button class="btn btn-success form-control">@lang('forms.submit')</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection