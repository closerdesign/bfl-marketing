@if( Auth::user() && Auth::user()->isStore() )

    <li><a href="{{ action('InventoryController@index') }}">Inventory</a></li>
{{--    <li><a href="{{ action('StoreLevelController@ads') }}">@lang('nav.ads')</a></li>--}}
{{--    <li><a href="{{ action('HuddlesController@index') }}">@lang('nav.huddles')</a></li>--}}
    <li><a href="{{ action('MailingListsController@index') }}">@lang('nav.mailing-lists')</a></li>
    <li><a href="{{ action('StoreLevelController@liquor_licenses') }}">@lang('nav.liquor-licenses')</a></li>
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            Signage <span class="caret"></span>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ action('StoreLevelController@signage_request') }}">@lang('nav.signage')</a></li>
            <li><a href="{{ action('SignageController@bins_form') }}">Bins Signage</a></li>
            <li><a href="{{ action('SignageShopController@index') }}">@lang('nav.signage-shop')</a></li>
            <li><a href="{{ action('SignageController@deli_sign') }}">@lang('nav.deli-signs')</a></li>
            <li><a href="{{ action('StoreLevelController@signage_request') }}">@lang('nav.signage')</a></li>
            <li><a href="{{ action('StoreLevelController@deal_of_the_week') }}">@lang('store-level.deal-of-the-week')</a></li>
            <li><a href="{{ action('StoreLevelController@kraft_promos') }}">@lang('store-level.kraft-promos')</a></li>
            <li><a href="{{ action('StoreLevelController@info_signs') }}">@lang('nav.info-signs')</a></li>
            <li><a href="{{ action('StoreLevelController@deal_of_the_week') }}">@lang('store-level.deal-of-the-week')</a></li>
            <li><a href="{{ action('StoreLevelController@kraft_promos') }}">@lang('store-level.kraft-promos')</a></li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">@lang('nav.reporting')</a>
        <ul class="dropdown-menu">
            <li><a href="{{ action('InventoryController@list') }}">Inventory</a></li>
        </ul>
    </li>

@elseif(Auth::user())

    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            @lang('roc.title')
        </a>

        <ul class="dropdown-menu">
            <li>
                <a href="{{ action('RocEntryController@index') }}">
                    @lang('roc.pending-requests')
                </a>
            </li>
            <li>
                <a href="{{ action('RocEntryController@create') }}">
                    @lang('roc-entry.add-new-entry')
                </a>
            </li>
            <li>
                <a href="{{ route('roc.index') }}">
                    @lang('roc.previous-weeks')
                </a>
            </li>
        </ul>
    </li>

    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            CTW
        </a>

        <ul class="dropdown-menu">
            <li>
                <a href="{{ route('ctw-evaluation.create') }}">@lang('ctw-evaluation.create')</a>
            </li>
            <li>
                <a href="{{ route('ctw-evaluation.index') }}">@lang('ctw-evaluation.index')</a>
            </li>
            <li>
                <a href="{{ route('ctw-area.index') }}">
                    @lang('ctw-area.manage-areas')
                </a>
            </li>
            <li>
                <a href="{{ route('ctw-evaluation.heatmap') }}">
                    @lang('ctw-evaluation.heatmap')
                </a>
            </li>
        </ul>
    </li>

    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            @lang('nav.products') <span class="caret"></span>
        </a>

        <ul class="dropdown-menu">
            <li><a href="{{ action('ProductsController@index') }}">@lang('nav.products')</a></li>
            <li><a href="{{ action('ShoppingProductsController@index') }}">@lang('nav.pricing-data')</a></li>
            <li><a href="{{ action('CategoriesController@index') }}">@lang('nav.categories')</a></li>
            <li><a href="{{ action('DepartmentsController@index') }}">@lang('nav.departments')</a></li>
            <li><a href="{{ action('ProductsController@import') }}">@lang('nav.import-products')</a></li>
            <li><a href="{{ action('PricingFilesController@index') }}">@lang('nav.import-shopping')</a></li>
            <li><a href="{{ action('KwikeeProductsController@form') }}">@lang('nav.import-kwikee')</a></li>
            <li><a href="{{ action('ProductsController@categories_upload_form') }}">@lang('nav.cat-upload')</a></li>
            <li><a href="{{ action('SearchController@index') }}">@lang('nav.os-search')</a></li>
        </ul>
    </li>
    <li><a href="{{ action('InventoryController@index') }}">Inventory</a></li>
    <li><a href="{{ action('AdsController@index') }}">@lang('nav.ads')</a></li>
    <li><a href="{{ action('PromosController@index') }}">@lang('nav.promos')</a></li>
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            Signage <span class="caret"></span>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ action('SignageController@bins_form') }}">Bins Signage</a></li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            @lang('nav.campaigns') <span class="caret"></span>
        </a>

        <ul class="dropdown-menu" role="menu">
            <li><a href="{{ action('CampaignsController@index') }}">@lang('nav.list')</a></li>
            <li><a href="{{ action('CampaignCategoriesController@index') }}">@lang('nav.categories')</a></li>
            <li><a href="{{ action('CampaignItemsController@index') }}">@lang('nav.items')</a></li>
            <li><a href="{{ action('CampaignFaqsController@index') }}">@lang('campaign-faqs.title')</a></li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            @lang('nav.content') <span class="caret"></span>
        </a>

        <ul class="dropdown-menu">
            <li><a href="{{ action('ArticlesController@index') }}">@lang('nav.articles')</a></li>
            <li><a href="{{ action('RecipesController@index') }}">@lang('nav.recipes')</a></li>
            <li><a href="{{ action('EventsController@index') }}">@lang('nav.events')</a></li>
            <li><a href="{{ action('NewsletterController@index') }}">@lang('nav.newsletters')</a></li>
            <li><a href="{{ action('NewItemsController@index') }}">@lang('nav.new-items')</a></li>
            <li><a href="{{ action('GalleryController@index') }}">@lang('nav.image-gallery' )</a></li>
            <li><a href="{{ action('TicketController@create') }}">@lang('nav.tickets')</a></li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            @lang('nav.reporting') <span class="caret"></span>
        </a>

        <ul class="dropdown-menu">
            <li><a href="{{ action('SalesController@index') }}">@lang('nav.sales-report')</a></li>
            <li><a href="{{ action('AwgPriceController@index') }}">AWG Pricing Analysis</a></li>
            <li><a href="{{ action('ItemMovementController@index') }}">Movement</a></li>
            <li><a href="{{ action('SalesController@sales_totals') }}">@lang('nav.sales-graphs')</a></li>
            <li><a href="{{ action('SalesController@dept_pie_chart') }}">@lang('nav.dept-breakdown')</a></li>
            <li><a href="{{ action('SurveysController@list') }}">@lang('nav.surveys')</a></li>
            <li><a href="{{ action('DonationRequestsController@index') }}">@lang('nav.donation-req')</a></li>
            <li><a href="{{ action('ProductRequestsController@index') }}">@lang('nav.product-req')</a></li>
            <li><a href="{{ action('HuddlesController@index') }}">@lang('nav.huddles')</a></li>
            <li><a href="{{ action('VolunteersController@index') }}">@lang('volunteers.title')</a></li>
            <li><a href="{{ action('ReportController@index') }}">@lang('nav.online-shopping')</a></li>
            <li><a href="{{ action('InventoryController@list') }}">Inventory</a></li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            @lang('nav.general') <span class="caret"></span>
        </a>

        <ul class="dropdown-menu">
            <li><a href="{{ action('BrandsController@index') }}">@lang('nav.brands')</a></li>
            <li><a href="{{ action('StoresController@index') }}">@lang('nav.stores')</a></li>
            <li><a href="{{ action('TrainingController@index') }}">@lang('nav.training')</a></li>
            <li><a href="{{ action('EmployeesController@index') }}">@lang('nav.employees')</a></li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            HR <span class="caret"></span>
        </a>

        <ul class="dropdown-menu">
            <li>
                <a href="{{ action('ItemController@index') }}">
                    Inventory
                </a>
            </li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            IT <span class="caret"></span>
        </a>

        <ul class="dropdown-menu">
            <li><a href="{{ action('IT\SigisController@upload_form') }}">SIGIS File Upload</a></li>
            <li><a href="{{ action('ItAssetsController@index') }}">IT Assets</a></li>
        </ul>
    </li>

@endif

<!-- Authentication Links -->
@if (Auth::guest())

    <li><a href="{{ url('/login') }}"><i class="fa fa-user"></i> @lang('nav.login')</a></li>
    {{--<li><a href="{{ url('/register') }}">Register</a></li>--}}

@else

    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            @lang('nav.resources') <span class="caret"></span>
        </a>
        <ul class="dropdown-menu">
{{--            <li><a href="{{ action('SignageShopController@index') }}">@lang('nav.signage-shop')</a></li>--}}
            {{--<li><a href="{{ action('StoreLevelController@termination_request') }}">@lang('nav.term-request')</a></li>--}}
            <li><a href="{{ action('StoreLevelController@bg_orientation_request') }}">@lang('nav.orientation')</a></li>
            {{--<li><a href="{{ action('StoreLevelController@vacation_request') }}">@lang('nav.vacation')</a></li>--}}
            <li><a href="{{ action('StoreLevelController@cash_report') }}">@lang('forms.cash-report')</a></li>
            <li><a href="{{ action('DocumentsController@index') }}">@lang('nav.documents')</a></li>
            <li><a href="{{ action('StoreLevelController@info_signs') }}">@lang('nav.info-signs')</a></li>
            <li><a href="{{ action('EmployeesController@discount_request') }}">@lang('nav.discount-request')</a></li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            <i class="fa fa-user"></i> {{ Auth::user()->name }} <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
            @if(Auth::user()->isAdmin())
                <li><a href="{{ action('ImprovementRequestsController@index') }}"><i class="fa fa-tasks"></i> @lang('nav.improvement')</a></li>
                <li><a href="{{ action('UpdatesController@index') }}"><i class="fa fa-wrench"></i> @lang('nav.updates')</a></li>
                <li><a href="{{ action('UsersController@index') }}"><i class="fa fa-users"></i> @lang('nav.user-mgmt')</a></li>
            @endif
            <li><a href="{{ action('UsersController@password') }}"><i class="fa fa-key"></i> @lang('nav.change-pw')</a></li>
            <li><a href="/@lang('nav.language-code')"><i class="fa fa-globe"></i> @lang('nav.change-language')</a></li>
            <li>
                <a href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">
                    <i class="fa fa-sign-out"></i> @lang('nav.logout')
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
        </ul>
    </li>

@endif