<?php
/**
 * Created by PhpStorm.
 * User: davidmeinke
 * Date: 2019-03-06
 * Time: 08:32
 */
?>

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">Assign Kitchen <i class="fa fa-keyboard-o"></i> </div>
                    <div class="panel-body">
                        <form action="{{ action('NotificationsController@store_kitchen') }}" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="single_date">First Monday of Kitchen Detail</label>
                            <input type="date" name="send_date" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="email">Email address</label>
                                <input class="form-control" name="notification_to" id="email">
                            </div>
                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">Kitchen Duty <i class="fa fa-trash"></i> </div>
                    <div class="panel-body">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>
                                        Week Of
                                    </th>
                                    <th>
                                        Email
                                    </th>
                                    <th class="text-right">
                                        Remove
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($kitchen_notifications as $n)
                                    <tr>
                                        <td>
                                            {{ date('m/d/Y', strtotime($n->send_date))}}
                                        </td>
                                        <td>
                                            {{ $n->notification_to }}
                                        </td>
                                        <td class="text-right">
                                            <a href="/notifications/destroy/{{$n->id}}" class="btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection