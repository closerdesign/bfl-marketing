@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">Notifications <i class="fa fa-keyboard-o"></i> </div>
                    <div class="panel-body">
                        <form action="{{ action("NotificationsController@store") }}" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="name">Notification Name</label>
                                <input type="text" name="name" id="name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="name">Notification Text</label>
                                <textarea class="form-control" rows="5" name="notification_text" id="notification_text"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="name">Notification To</label>
                                <input type="text" name="notification_to" id="notification_to" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="name">Send Frequency</label>
                                <select name="send_date_type" id="send_date_type" class="form-control">
                                    <option value="">Select...</option>
                                    <option value="dailyAt">Daily->At</option>
                                    <option value="weeklyOn">Weekly->At</option>
                                    <option value="monthlyOn">Monthly->At</option>
                                </select>
                            </div>
                            <div class="form-group" id="options">
                                <div class="form-group options" id="hour">
                                    <label for="hour">Hour</label>
                                    <select id="send_time" name="send_time" class="form-control">
                                        @for($h = 1; $h <= 24; $h++)
                                            <option value="{{$h}}:00">{{$h}}:00</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="form-group options" id="send_day" >
                                    <label for="send_day">Send Day (Day Of Week)</label>
                                    <select id="send_day" name="send_day" class="form-control">
                                        @foreach($days as $k => $day)
                                            <option value="{{$k}}">{{$day}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group options" id="send_day_month">
                                    <label for="send_day">Send Day (Day Of Month)</label>
                                    <select id="send_day_month" name="send_day_month" class="form-control">
                                        @for($d = 1; $d <= 31; $d++)
                                            <option value="{{$d}}">{{$d}}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="pull-right">
                                <button class="btn btn-primary form-control" type="submit">Save</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
            <!-- second column --->
            <div class="col-lg-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">Notifications <i class="fa fa-book"></i> </div>
                    <div class="panel-body">
                        <table class="table">
                            <tr>
                                <th>Name</th>
                                <th>On</th>
                                <th>Remove</th>
                            </tr>
                            @if (isset($notifications))
                                @foreach($notifications as $n)
                                 <tr>
                                    <td>{{$n->name}}</td>
                                    <td>{{$n->send_date_type}}</td>
                                     <td><a href="/notifications/destroy/{{$n->id}}"><i class="fa fa-trash"></i></a></td>
                                 </tr>
                                @endforeach
                            @endif
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('js')

    <script>
        $(document).ready(function() {
            $('#hour').hide();
            $('#send_day').hide();
            $(".options").hide();
            $("#send_date_type").change(function () {
                if ($("#send_date_type").val() === 'dailyAt') {
                    $(".options").hide();
                    $('#hour').show();
                }
                if ($("#send_date_type").val() === 'weeklyOn') {
                    $(".options").hide();
                    $('#hour').show();
                    $('#send_day').show();
                }
                if ($("#send_date_type").val() === 'monthlyOn') {
                    $(".options").hide();
                    $('#hour').show();
                    $('#send_day_month').show();
                }
            });
        });

    </script>



@endsection