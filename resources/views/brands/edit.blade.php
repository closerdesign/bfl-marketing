@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <p class="lead text-center">Edit brand: {{ $brand->name }}</p>
    </div>

    <hr>

    <div class="container-fluid">

        <form action="{{ action('BrandsController@update', $brand->id) }}" method="post" enctype="multipart/form-data">

            {{ csrf_field() }}

            {{ method_field('PATCH') }}

            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" name="name" value="{{ $brand->name }}" required >
            </div>

            <div class="form-group">
                <img src="{{ $brand->logo }}" alt="{{ $brand->name }}" width="180">
            </div>

            <div class="form-group">
                <label for="logo">Logo (640x480)</label>
                <input type="file" class="form-control" name="logo">
            </div>

            <div class="form-group">
                <img src="{{ $brand->icon }}" alt="{{ $brand->name }}" width="180">
            </div>

            <div class="form-group">
                <label for="icon">@lang('brands.icon')</label>
                <input type="file" class="form-control" name="icon">
            </div>

            <div class="form-group">
                <label for="website">Website</label>
                <input type="text" class="form-control" name="website" value="{{ $brand->website }}" required >
            </div>

            <div class="form-group">
                <label for="online_shopping_website">@lang('brands.online-shopping-website')</label>
                <input type="text" class="form-control" name="online_shopping_website" value="{{ $brand->online_shopping_website }}" >
            </div>

            <div class="form-group">
                <label for="ios_mobile_app_url">@lang('brands.ios-mobile-app-url')</label>
                <input type="text" class="form-control" name="ios_mobile_app_url" value="{{ $brand->ios_mobile_app_url }}" >
            </div>

            <div class="form-group">
                <label for="android_mobile_app_url">@lang('brands.android-mobile-app-url')</label>
                <input type="text" class="form-control" name="android_mobile_app_url" value="{{ $brand->android_mobile_app_url }}" >
            </div>

            <div class="form-group">
                <label for="shipt_url">@lang('brands.shipt-url')</label>
                <input type="text" class="form-control" name="shipt_url" value="{{ $brand->shipt_url }}">
            </div>

            <div class="form-group">
                <label for="loyalty_website_url">@lang('brands.loyalty-website-url')</label>
                <input type="text" class="form-control" name="loyalty_website_url" value="{{ $brand->loyalty_website_url }}" >
            </div>

            <div class="form-group">
                <label for="facebook_url">@lang('brands.facebook-url')</label>
                <input type="text" class="form-control" name="facebook_url" value="{{ $brand->facebook_url }}">
            </div>

            <div class="form-group">
                <label for="instagram_url">@lang('brands.instagram-url')</label>
                <input type="text" class="form-control" name="instagram_url" value="{{ $brand->instagram_url }}">
            </div>

            <button class="btn btn-success">Save changes</button>

        </form>

    </div>

    @endsection