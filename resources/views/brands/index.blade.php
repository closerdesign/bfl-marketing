@extends('layouts.app')

@section('content')

    <div class="container-fluid">

        <p class="lead text-center">@lang('brands.title')</p>

        <p class="text-right">
            <a class="btn btn-success" href="{{ action('BrandsController@create') }}"><i class="fa fa-plus"></i> Create A New Brand</a>
        </p>

        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Website</th>
                </tr>
                </thead>
                <tbody>
                @foreach($brands as $brand)

                    <tr>
                        <th>
                            <a href="{{ action('BrandsController@edit', $brand->id) }}">{{ $brand->name }}</a>
                        </th>
                        <td>{{ $brand->website }}</td>
                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection