@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <p class="lead text-center">Create a new brand</p>
    </div>

    <hr>

    <div class="container-fluid">
        <form action="{{ action('BrandsController@store') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" name="name" value="{{ old('name') }}" required >
            </div>
            <div class="form-group">
                <label for="logo">Logo (640 x 480)</label>
                <input type="file" class="form-control" name="logo" value="{{ old('logo') }}" required >
            </div>
            <div class="form-group">
                <label for="icon">@lang('brands.icon')</label>
                <input type="file" class="form-control" name="icon" value="{{ old('icon') }}" required >
            </div>
            <div class="form-group">
                <label for="website">Website</label>
                <input type="text" class="form-control" name="website" value="{{ old('website') }}" required >
            </div>
            <div class="form-group">
                <label for="ios_mobile_app_url">@lang('brands.ios-mobile-app-url')</label>
                <input type="text" class="form-control" name="ios_mobile_app_url" value="{{ old('ios_mobile_app_url') }}" >
            </div>

            <div class="form-group">
                <label for="android_mobile_app_url">@lang('brands.android-mobile-app-url')</label>
                <input type="text" class="form-control" name="android_mobile_app_url" value="{{ old('android_mobile_app_url') }}" >
            </div>

            <div class="form-group">
                <label for="shipt_url">@lang('brands.shipt-url')</label>
                <input type="text" class="form-control" name="shipt_url" value="{{ old('shipt_url') }}">
            </div>

            <div class="form-group">
                <label for="loyalty_website_url">@lang('brands.loyalty-website-url')</label>
                <input type="text" class="form-control" name="loyalty_website_url" value="{{ old('loyalty_website_url') }}" >
            </div>

            <div class="form-group">
                <label for="facebook_url">@lang('brands.facebook-url')</label>
                <input type="text" class="form-control" name="facebook_url" value="{{ old('facebook_url') }}">
            </div>

            <div class="form-group">
                <label for="instagram_url">@lang('brands.instagram-url')</label>
                <input type="text" class="form-control" name="instagram_url" value="{{ old('instagram_url') }}">
            </div>

            <button class="btn btn-success">Save changes</button>
        </form>
    </div>

    @endsection