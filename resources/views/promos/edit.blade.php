@extends('layouts.app')

@section('content')

    <style>
        form input[type="submit"]{
            background: none;
            border: none;
            cursor: pointer;
            padding: 0;
        }
    </style>

    <div class="container">
        <div class="form-group">
            <div class="btn-group" role="group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Promo Actions
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li>
                        <a href="{{ action('PromosController@excel', $promo->id) }}">
                            <i class="fa fa-file-pdf-o"></i> Export
                        </a>
                    </li>
                    <li>
                        <a href="{{ action('PromosController@cloning', $promo->id) }}" onclick="return confirm('Are You Sure?')">
                            <i class="fa fa-clone"></i> Clone
                        </a>
                    </li>
                    <li style="padding: 3px 20px;">
                        <form id="form-remove-{{ $promo->id }}" onsubmit="return confirm('Are you sure?')" method="post" action="{{ action('PromosController@destroy', $promo->id) }}">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <i class="fa fa-trash"></i> <input type="submit" value="DELETE">
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <form action="{{ action('PromosController@update', $promo->id) }}" method="post" enctype="multipart/form-data" >
        {{ csrf_field() }}
        {{ method_field('PATCH') }}
        <div class="container">

            <div class="panel panel-primary">
                <div class="panel-heading">Edit Promo: {{ $promo->name }}</div>
                <div class="panel-body">

                    <p class="text-right">
                        <img width="140" src="{{ $promo->image }}" alt="{{ $promo->name }}">
                    </p>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" name="name" value="{{ $promo->name }}" placeholder="e.g. Powerade 12 Pack" required >
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="image">Image</label>
                                    <input type="file" class="form-control" name="image" >
                                    <label>Image size: 350 x 350 px. 72 ppi.</label>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="publish_date">
                                        Publish Date
                                        <a href="#" data-toggle="tooltip" data-placement="right" title="This is the date when promo will start being displayed in our websites.">
                                            <i class="fa fa-info-circle"></i>
                                        </a>
                                    </label>
                                    <input type="text" class="form-control datepicker" name="publish_date" value="{{ $promo->publish_date }}" required >
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="start_date">
                                        Start Date
                                        <a href="#" data-toggle="tooltip" data-placement="right" title="This is the date when the promotion will be available to our guests for purchase">
                                            <i class="fa fa-info-circle"></i>
                                        </a>
                                    </label>
                                    <input type="text" class="form-control datepicker" name="start_date" value="{{ $promo->start_date }}" required >
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="end_date">End Date
                                        <a href="#" data-toggle="tooltip" data-placement="right" title="This is the date when promo will be taken off the website.">
                                            <i class="fa fa-info-circle"></i>
                                        </a>
                                    </label>
                                    <input type="text" class="form-control datepicker" name="end_date" value="{{ $promo->end_date }}" required >
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="price">Price </label>
                                    <input type="text" class="form-control" name="price" value="{{ $promo->price }}" placeholder="e.g. $1.99 LB, 99¢ Each, Buy One Get One Free" >
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="upc">UPC (For Image)</label>
                                    <input type="text" class="form-control" name="upc" value="{{ $promo->upc }}" >
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="limit">Limit (0=No Limit)</label>
                                    <input type="number" class="form-control text-center" name="limit_per_transaction" value="{{ $promo->limit_per_transaction }}">
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="stores">Stores</label>
                                    @foreach(\App\Store::orderBy('store_code')->get() as $store)
                                    <div class="checkbox">
                                        <label>
                                            <input @if( in_array($store->id, $promo->stores()->select('store_id')->pluck('store_id')->toArray()) ) checked @endif type="checkbox" name="stores[]" value="{{ $store->id }}"> [{{ $store->store_code }}] {{ $store->name }}
                                        </label>
                                    </div>
                                    @endforeach
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="department_id">Department</label>
                                    <select name="department_id" id="department_id" class="form-control">
                                        <option value="">Select...</option>
                                        @foreach(\App\Department::orderBy('name')->get() as $department)
                                            <option @if($promo->department_id == $department->id) selected @endif value="{{ $department->id }}">{{ $department->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="checkbox col-md-12">
                                    <label>
                                        <input type="checkbox" name="while_supplies_last" value="1" @if( $promo->while_supplies_last == 1 ) checked @endif > ¿While supplies last?
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <textarea name="description" id="description" class="form-control" rows="8" placeholder="e.g. Buy One Get One Free!" required >{{ $promo->description }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="additional_upcs">Additional UPCs</label>
                                        <textarea name="additional_upcs" id="additional_upcs" class="form-control" rows="3" >{{ $promo->additional_upcs }}</textarea>
                                        <label>Separated by comma.</label>
                                    </div>
                                    <div class="form-group">
                                        <label for="comments">Comments or Additional Instructions</label>
                                        <textarea name="comments" id="comments" cols="30" rows="10"
                                                  class="form-control">{{ $promo->comments }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="coupon">Coupon @if( $promo->coupon != "" ) <a href="{{ $promo->coupon }}" target="_blank"><i class="fa fa-download"></i></a> @endif</label>
                                        <input type="file" class="form-control" name="coupon" >
                                    </div>
                                    <div class="form-group">
                                        <label for="promo_number">Promo Number</label>
                                        <input type="text" class="form-control" name="promo_number" value="{{ $promo->promo_number }}" >
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="websites" value="1" @if( $promo->websites == true ) checked @endif > Post on websites
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label for="mobile_app_deal">
                                            <input type="checkbox" name="mobile_app_deal" value="1" @if( $promo->mobile_app_deal == true ) checked @endif > Mobile App Deal
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <button class="btn btn-success pull-right" type="submit"><i class="fa fa-save"></i> Save changes</button>
        </div>
    </form>

    @endsection