@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Deleting: {{ $promo->name }}</div>
                    <div class="panel-body">
                        <form action="{{ action('PromosController@destroy', $promo->id) }}" method="post">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <div class="row">
                                <div class="col-md-12">
                                    <p>Are you sure that you wan to delete: {{ $promo->name }}? This operation can't be undone.</p>
                                    <p><button type="submit" class="btn btn-primary">Yes, I'm sure.</button></p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection