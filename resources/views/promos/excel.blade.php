<html>
<head>
   <style>
       body{
           font-family: Helvetica;
       }
       table {
           border-collapse: collapse;
       }

       table, th, td {
           border: 1px solid black;
       }
   </style>
</head>
<body>
<p style="text-align: center; font-weight: bold; text-transform: uppercase">Promo Datasheet<br />{{ $promo->name }}</p>
<table cellpadding="5">
    <tr>
        <th>Promo Name</th>
        <td>{{ $promo->name }}</td>
    </tr>
    <tr>
        <th>Description</th>
        <td>{{ strip_tags($promo->description) }}</td>
    </tr>
    <tr>
        <th>Price</th>
        <td>{{ $promo->price }}</td>
    </tr>
    <tr>
        <th>While Supplies Last</th>
        <td>@if($promo->while_supplies_last == 1) Yes @else No @endif</td>
    </tr>
    <tr>
        <th>Image UPC</th>
        <td>{{ $promo->upc }}</td>
    </tr>
    <tr>
        <th>Discount Dept.</th>
        <td>{{ $promo->department->name }}</td>
    </tr>
    <tr>
        <th>Limit Per Transaction</th>
        <td>
            @if($promo->limit_per_transaction == 0)
            No limit
            @else
            {{ $promo->limit_per_transaction }}
            @endif
        </td>
    </tr>
    <tr>
        <th colspan="2">STORES</th>
    </tr>
    @foreach($promo->stores as $store)
        <tr>
            <th>[{{ $store->store_code }}] {{ $store->name }}</th>
            <td>{{ $promo->start_date }} - {{ $promo->end_date }}</td>
        </tr>
    @endforeach
    <tr>
        <th colspan="2">Comments</th>
    </tr>
    <tr>
        <td colspan="2">@if($promo->comments == "")There is no additional comments.@else{{ $promo->comments }}@endif</td>
    </tr>
    <tr>
        <th>Additional UPCs</th>
        <td>
            @foreach( explode(",", $promo->additional_upcs) as $upc )
            {{ $upc }}<br />
            @endforeach
        </td>
    </tr>
</table>
</body>
</html>