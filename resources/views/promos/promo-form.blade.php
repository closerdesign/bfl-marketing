<table>
    <tr>
        <td colspan="4">Buy for Less / Uptown Grocery Co.  Group Promo Form</td>
    </tr>
    <tr>
        <td colspan="4">&nbsp;</td>
    </tr>
    <tr>
        <td>Promo Name</td>
        <td colspan="3">{{ $promo->name }}</td>
    </tr>
    <tr>
        <td>Discount Dept</td>
        <td colspan="3">{{ $promo->department->name }}</td>
    </tr>
    <tr>
        <td>Limit Per Transaction</td>
        <td colspan="3">{{ $promo->limit_per_transaction }}</td>
    </tr>
    <tr>
        <td colspan="4">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="4">STORES AND DATES</td>
    </tr>
    <tr>
        <td colspan="2">Store</td>
        <td>Start</td>
        <td>End</td>
    </tr>
    @foreach($promo->stores as $store)
        <tr>
            <td colspan="2">[{{ $store->store_code }}] {{ $store->name }}</td>
            <td>{{ $promo->start_date }}</td>
            <td>{{ $promo->end_date }}</td>
        </tr>
    @endforeach
    <tr>
        <td colspan="4">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="4">DESCRIPTION</td>
    </tr>
    <tr>
        <td colspan="4">
            {{ $promo->description }}
        </td>
    </tr>
    <tr>
        <td colspan="4">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="4">ADDITIONAL COMMENTS / INSTRUCTIONS</td>
    </tr>
    <tr>
        <td colspan="4">
            {{ $promo->comments }}
        </td>
    </tr>

</table>