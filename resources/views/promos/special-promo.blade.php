@extends('layouts.app')

@section('content')

    <form action="{{ action('PromosController@store') }}" method="post" enctype="multipart/form-data" >
        {{ csrf_field() }}
        <div class="container">
            <div class="panel panel-primary">
                <div class="panel-heading">Create a new promo</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" name="name"  placeholder="e.g. Powerade 12 Pack" required  value="{{$promo->item_description}}">
                                </div>
                                <div class="form-group col-md-12">
                                   <!-- <img width="140" src="{{ $promo->image }}" alt="{{ $promo->item_description }}"> -->
                                    <label for="image">Image</label>
                                    <input type="file" class="form-control" name="image" >
                                    <input name="img_ava" type="hidden" value="{{$promo->image}}" disabled>
                                    <label>Image size: 350 x 350 px. 72 ppi.</label>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="publish_date">
                                        Publish Date
                                        <a href="#" data-toggle="tooltip" data-placement="right" title="This is the date when promo will start being displayed in our websites.">
                                            <i class="fa fa-info-circle"></i>
                                        </a>
                                    </label>
                                    <input type="text" class="form-control datepicker" name="publish_date" value="{{ old('publish_date') }}" required >
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="start_date">
                                        Start Date
                                        <a href="#" data-toggle="tooltip" data-placement="right" title="This is the date when the promotion will be available to our guests for purchase">
                                            <i class="fa fa-info-circle"></i>
                                        </a>
                                    </label>
                                    <input type="text" class="form-control datepicker" name="start_date" value="{{ old('start_date') }}" required >
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="end_date">End Date
                                        <a href="#" data-toggle="tooltip" data-placement="right" title="This is the date when promo will be taken off the website.">
                                            <i class="fa fa-info-circle"></i>
                                        </a>
                                    </label>
                                    <input type="text" class="form-control datepicker" name="end_date" value="{{ old('end_date') }}" required >
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="price">Price</label>
                                    <input type="text" class="form-control" name="price" value="{{ $promo->price }}" placeholder="e.g. $1.49 LB, 99¢ Each" >
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="upc">UPC (For Image)</label>
                                    <input type="text" class="form-control" name="upc" value="{{ $promo->upc }}" >
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="limit">Limit (0=No Limit)</label>
                                    <input type="number" class="form-control text-center" name="limit_per_transaction" value="{{$promo->limit}}">
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="stores">Stores</label>
                                    @foreach(\App\Store::orderBy('store_code')->get() as $store)
                                        <div class="checkbox">
                                            <label>
                                                <input @if( old('stores') && (in_array($store->id, old('stores'))) ) checked @endif type="checkbox" name="stores[]" value="{{ $store->id }}"> [{{ $store->store_code }}] {{ $store->name }}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="department_id">Department</label>
                                    <select name="department_id" id="department_id" class="form-control">
                                        <option value="">Select...</option>
                                        @foreach(\App\Department::orderBy('name')->get() as $department)
                                            <option value="{{ $department->id }}" @if($promo->department_id == $department->id) SELECTED @endif>{{ $department->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="checkbox col-md-12">
                                    <label>
                                        <input type="checkbox" name="while_supplies_last" value="1" @if( old('while_supplies_last') == 1 ) checked @endif > ¿While supplies last?
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="row" style="margin-right: 0">
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea name="description" id="description" class="form-control" rows="8" placeholder="e.g. Buy One Get One Free!" required >{{ old('description') }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="additional_upcs">Additional UPCs</label>
                                    <textarea name="additional_upcs" id="additional_upcs" class="form-control" rows="3" >{{ $promo->additional_upcs }}</textarea>
                                    <label>Separated by comma.</label>
                                </div>
                                <div class="form-group">
                                    <label for="comments">Comments or Additional Instructions</label>
                                    <textarea name="comments" id="comments" cols="30" rows="10"
                                              class="form-control" placeholder="This text is not published on the website. Please use this area for further comments, instructions and notes" >{{ old('comments') }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="coupon">Coupon</label>
                                    <input type="file" class="form-control" name="coupon" >
                                </div>
                                <div class="form-group">
                                    <label for="promo_number">Promo Number</label>
                                    <input type="text" class="form-control" name="promo_number" value="{{$promo->promo_number}}">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="websites" value="1" checked > Post on websites
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <button class="btn btn-success" type="submit"><i class="fa fa-save"></i> Save changes</button>
        </div>
    </form>


@endsection