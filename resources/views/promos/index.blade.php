@extends('layouts.app')

@section('content')

    <style>
        form input[type="submit"]{
            background: none;
            border: none;
            cursor: pointer;
            padding: 0;
        }
    </style>

    <div class="container">
        <div class="row">
            <div class="col-md-12 text-right">
                <div class="btn-group" role="group">
                    <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-eye" aria-hidden="true"></i> View
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="https://buyforlessok.com/deals" target="_blank">
                                Buy For Less
                            </a>
                        </li>
                        <li>
                            <a href="https://smartsaverok.com/promos" target="_blank">
                                Smart Saver
                            </a>
                        </li>
                        <li>
                            <a href="https://uptowngroceryco.com/deals" target="_blank">
                                Uptown
                            </a>
                        </li>

                    </ul>
                </div>

                <a href="{{ action('PromosController@create') }}" class="btn btn-success"><i class="fa fa-plus"></i> Create promo</a><br /><br />
            </div>
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading">@if($old) All @else Available @endif Promos</div>
            <div class="panel-body">

                <div class="table responsive">
                    <table class="table table-striped table-condensed">
                        <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>Name</th>
                            <th>UPC</th>
                            <th>Publish Date</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Stores</th>
                            <th>IT Checked</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($promos as $promo)
                            <tr>
                               <td>
                                   @if( $promo->mobile_app_deal == true ) <i class="fa fa-mobile-phone fa-2x"></i> @endif
                               </td>
                                <td class="text-center">
                                    <div class="btn-group" role="group">
                                        <button type="button" class="btn-xs btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Actions
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="{{ action('PromosController@excel', $promo->id) }}">
                                                    <i class="fa fa-file-pdf-o"></i> Export
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{ action('PromosController@cloning', $promo->id) }}" onclick="return confirm('Are You Sure?')">
                                                    <i class="fa fa-clone"></i> Clone
                                                </a>
                                            </li>
                                            <li style="padding: 3px 20px;">
                                                <form id="form-remove-{{ $promo->id }}" onsubmit="return confirm('Are you sure?')" method="post" action="{{ action('PromosController@destroy', $promo->id) }}">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                    <i class="fa fa-trash"></i> <input type="submit" value="DELETE">
                                                </form>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                                <td><a href="{{ action('PromosController@edit', $promo->id) }}"><img width="25" src="{{ $promo->image }}"
                                                                                                     alt=""></a></td>
                                <th>
                                    <a href="{{ action('PromosController@edit', $promo->id) }}">
                                        {!! $promo->name !!}
                                    </a>
                                </th>
                                <td>{{ $promo->upc }}</td>
                                <td class="text-center">{{ $promo->publish_date }}</td>
                                <td class="text-center">{{ $promo->start_date }}</td>
                                <td class="text-center">{{ $promo->end_date }}</td>
                                <td>
                                    @foreach($promo->stores as $store)
                                        {{ '[' . $store->store_code . ']' }}<br />
                                    @endforeach
                                </td>
                                <td class="text-center">
                                    <a href="{{ action('PromosController@it_processed', $promo->id) }}">
                                        <span class="label @if( $promo->it_processed == 'Yes' ) label-success @else label-danger @endif">
                                            {{ $promo->it_processed }}
                                        </span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $promos->render() }}
                </div>

            </div>
        </div>
        <p>
            @if($old)
                <a href="{{ action('PromosController@index') }}">Return to Available Promos</a>
                @else
                <a href="{{ action('PromosController@old') }}">Looking for an old promo?</a>
            @endif
        </p>
    </div>

    @endsection