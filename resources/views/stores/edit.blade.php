@extends('layouts.app')

@section('content')

    <form action="{{ action('StoresController@update', $store->id) }}" method="post" enctype="multipart/form-data" >

        {{ csrf_field() }}

        {{ method_field('PATCH') }}

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <p class="text-right">
                        <img class="img-rounded" width="100" src="{{ $store->store_manager_image }}" alt="{{ $store->name }}">
                    </p>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Edit store: {{ $store->name }}
                        </div>
                        <div class="panel-body">

                            <div>

                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#basic-information" aria-controls="basic-information" role="tab" data-toggle="tab">@lang('stores.basic-information')</a></li>
                                    <li role="presentation"><a href="#hours-information" aria-controls="hours-information" role="tab" data-toggle="tab">@lang('stores.hours-information')</a></li>
                                    <li role="presentation"><a href="#online-shopping" aria-controls="online-shopping" role="tab" data-toggle="tab">@lang('stores.online-shopping')</a></li>
                                    <li role="presentation"><a href="#loyalty" aria-controls="loyalty" role="tab" data-toggle="tab">Loyalty</a></li>
                                    <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">@lang('stores.settings')</a></li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content form-group">
                                    <div role="tabpanel" class="tab-pane active" id="basic-information">
                                        <div class="form-group">
                                            <label for="name">Name</label>
                                            <input type="text" class="form-control" name="name" value="{{ $store->name }}" required >
                                        </div>
                                        <div class="form-group">
                                            <label for="address">Address</label>
                                            <input type="text" class="form-control" name="address" value="{{ $store->address }}" required >
                                        </div>
                                        <div class="form-group">
                                            <label for="address2">Address 2</label>
                                            <input type="text" class="form-control" name="address2" value="{{ $store->address2 }}" required >
                                        </div>
                                        <div class="form-group">
                                            <label for="city">@lang('stores.city')</label>
                                            <input type="text" class="form-control" name="city" value="{{ $store->city }}" required >
                                        </div>
                                        <div class="form-group">
                                            <label for="state">@lang('stores.state')</label>
                                            <input type="text" class="form-control" name="state" value="{{ $store->state }}" required >
                                        </div>
                                        <div class="form-group">
                                            <label for="postal_code">@lang('stores.postal-code')</label>
                                            <input type="text" class="form-control" name="postal_code" value="{{ $store->postal_code }}" required >
                                        </div>
                                        <div class="form-group">
                                            <label for="latitude">Latitude</label>
                                            <input type="text" class="form-control" name="latitude" value="{{ $store->latitude }}" required >
                                        </div>
                                        <div class="form-group">
                                            <label for="longitude">Longitude</label>
                                            <input type="text" class="form-control" name="longitude" value="{{ $store->longitude }}" required >
                                        </div>
                                        <div class="form-group">
                                            <label for="phone">Phone</label>
                                            <input type="text" class="form-control" name="phone" value="{{ $store->phone }}" required >
                                        </div>
                                        <div class="form-group">
                                            <label for="store_manager">Store Manager</label>
                                            <input type="text" class="form-control" name="store_manager" value="{{ $store->store_manager }}" required >
                                        </div>
                                        <div class="form-group">
                                            <label for="email">Store Manager Email</label>
                                            <input type="email" class="form-control" name="email" value="{{ $store->email }}" required >
                                        </div>
                                        <div class="form-group">
                                            <label for="store_code">Store Code</label>
                                            <input type="text" class="form-control" name="store_code" value="{{ $store->store_code }}" required >
                                        </div>
                                        <div class="form-group">
                                            <label for="brand">Brand</label>
                                            <select name="brand_id" id="brand_id" class="form-control">
                                                <option value="">Select...</option>
                                                @foreach($brands as $brand)
                                                    <option @if($store->brand_id == $brand->id) selected @endif value="{{ $brand->id }}">{{ $brand->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="hours-information">
                                        <div class="form-group">
                                            <label for="store_hours">Store Hours</label>
                                            <input type="text" class="form-control" name="store_hours" value="{{ $store->store_hours }}" required >
                                        </div>
                                        <div class="form-group">
                                            <label for="pharmacy_hours">Pharmacy Hours</label>
                                            <input type="text" class="form-control" name="pharmacy_hours" value="{{ $store->pharmacy_hours }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="catering_events_hours">Catering & Events Hours</label>
                                            <input type="text" class="form-control" name="catering_events_hours" value="{{$store->catering_events_hours }}" >
                                        </div>
                                        <div class="form-group">
                                            <label for="deli_hours">Deli Hours</label>
                                            <input type="text" class="form-control" name="deli_hours" value="{{ $store->deli_hours }}" >
                                        </div>
                                        <div class="form-group">
                                            <label for="bakery_hours">Bakery Hours</label>
                                            <input type="text" class="form-control" name="bakery_hours" value="{{ $store->bakery_hours }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="produce_hours">Produce Hours</label>
                                            <input type="text" class="form-control" name="produce_hours" value="{{ $store->deli_hours}}" >
                                        </div>
                                        <div class="form-group">
                                            <label for="floral_hours">Floral Hours</label>
                                            <input type="text" class="form-control" name="floral_hours" value="{{ $store->floral_hours }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="guest_services_hours">@lang('stores.guest-services-hours')</label>
                                            <input type="text" class="form-control" name="guest_services_hours" value="{{ $store->guest_services_hours }}">
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="online-shopping">
                                        <div class="form-group">
                                            <label for="online_shopping_phone_number">@lang('stores.online-shopping-phone-number')</label>
                                            <input type="text" class="form-control" name="online_shopping_phone_number" value="{{ $store->online_shopping_phone_number }}">
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="loyalty">
                                        <div class="form-group">
                                            <label for="rsa_sendy_list">RSA Sendy List</label>
                                            <input type="text" class="form-control" name="rsa_sendy_list" value="{{ $store->rsa_sendy_list }}">
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="settings">
                                        <div class="form-group">
                                            <label for="tax_rate">TAX Rate</label>
                                            <div class="input-group">
                                                <input type="number" class="form-control" name="tax_rate" value="{{ $store->tax_rate }}" step="0.001" required >
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="price_strategy">@lang('stores.price-strategy')</label>
                                            <input type="number" class="form-control text-center" name="price_strategy" value="{{ $store->price_strategy }}" required >
                                        </div>

                                        <div class="form-group">
                                            <label for="zenreach_code">Zenreach Code</label>
                                            <input type="text" class="form-control" name="zenreach_code" value="{{ $store->zenreach_code }}" >
                                        </div>
                                        <div class="form-group">
                                            <label for="sendy_list_id">Sendy List ID</label>
                                            <input type="text" class="form-control" name="sendy_list_id" value="{{ $store->sendy_list_id }}" >
                                        </div>
                                        <div class="form-group">
                                            <label for="store_manager_image">Store Manager Image</label>
                                            <input type="file" class="form-control" name="store_manager_image" >
                                            <label>Image size: 350px x 350px. 72ppi.</label>
                                        </div>

                                        <div class="form-group">
                                            <label for="stripe_key">Stripe Key</label>
                                            <input type="text" class="form-control" name="stripe_key" value="{{ $store->stripe_key }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="stripe_secret">Stripe Secret</label>
                                            <input type="text" class="form-control" name="stripe_secret" value="{{ $store->stripe_secret }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="paypal_username">Paypal Username</label>
                                            <input type="text" class="form-control" name="paypal_username" value="{{ $store->paypal_username }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="paypal_password">Paypal Password</label>
                                            <input type="text" class="form-control" name="paypal_password" value="{{ $store->paypal_password }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="paypal_secret">Paypal Key</label>
                                            <input type="text" class="form-control" name="paypal_secret" value="{{ $store->paypal_secret }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="status">@lang('general.status')</label>
                                            <select name="status" id="status" class="form-control">
                                                <option value="">Select</option>
                                                <option @if( $store->status == Lang::get('stores.open') ) selected @endif value="1">Open</option>
                                                <option @if( $store->status == Lang::get('stores.closed') ) selected @endif value="0">Closed</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="shipt_files"><input type="checkbox" name="shipt_files" value="1" @if( $store->shipt_files == 1 ) checked @endif > Include on Shipt files generation routine</label>
                                        </div>
                                        <div class="form-group">
                                            <label for="is_cost_plus">
                                                <input type="checkbox" name="is_cost_plus" value="1" @if( $store->is_cost_plus ) checked @endif> Is Cost Plus
                                            </label>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <button class="btn btn-success btn-block mt-5"><i class="fa fa-save"></i> Update</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>

    @endsection