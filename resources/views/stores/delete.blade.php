@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Delete: {{ $store->name }}</div>

                    <div class="panel-body">

                        <div class="row">

                            <div class="col-md-12">

                                <p>Are you sure that you want to remove {{ $store->name }} store? This operation can't be undone.</p>

                                <form action="{{ action('StoresController@destroy', $store->id) }}" method="post">

                                    {{ csrf_field() }}

                                    {{ method_field('DELETE') }}

                                    <button type="submit" class="btn-primary">Yes, delete the store</button>

                                </form>

                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection