@extends('layouts.app')

@section('content')

    <div class="container-fluid">

        <p class="text-right">
            <a class="btn btn-success" href="{{ action('StoresController@create') }}">Create a new store</a>
        </p>

        <div class="panel panel-default">
            <div class="panel-heading">Stores</div>

            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-condensed">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Code</th>
                            <th>Brand</th>
                            <th>Address</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Store Manager</th>
                            <th>Tax Rate</th>
                            <th>@lang('stores.price-strategy')</th>
                            <th>@lang('general.status')</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($stores as $store)
                        <tr>
                            <th>
                                <a href="{{ action('StoresController@edit', $store->id) }}">
                                    {{ $store->name }}
                                </a>
                            </th>
                            <td>{{ $store->store_code }}</td>
                            <td>{{ $store->brands->name }}</td>
                            <td>{{ $store->address }}</td>
                            <td>{{ $store->phone }}</td>
                            <td>{{ $store->email }}</td>
                            <td>{{ $store->store_manager }}</td>
                            <td>{{ $store->tax_rate }}</td>
                            <td class="text-center">{{ $store->price_strategy }}</td>
                            <td>{{ $store->status }}</td>
                            <td class="text-center">
                                <form action="{{ action('StoresController@destroy', $store->id) }}" method="post" onsubmit="return confirm('Are you sure?')">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button class="btn-xs btn-danger"><i class="fa fa-trash"></i></button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @endsection