@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Create a new store</div>
                    <div class="panel-body">
                        <form action="{{ action('StoresController@store') }}" method="post" enctype="multipart/form-data" >
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}" required >
                            </div>
                            <div class="form-group">
                                <label for="address">Address</label>
                                <input type="text" class="form-control" name="address" value="{{ old('address') }}" required >
                            </div>
                            <div class="form-group">
                                <label for="address2">Address 2</label>
                                <input type="text" class="form-control" name="address2" value="{{ old('address2') }}" required >
                            </div>
                            <div class="form-group">
                                <label for="city">@lang('stores.city')</label>
                                <input type="text" class="form-control" name="city" value="{{ old('city') }}" required >
                            </div>
                            <div class="form-group">
                                <label for="state">@lang('stores.state')</label>
                                <input type="text" class="form-control" name="state" value="{{ old('state') }}" required >
                            </div>
                            <div class="form-group">
                                <label for="postal_code">@lang('stores.postal-code')</label>
                                <input type="text" class="form-control" name="postal_code" value="{{ old('postal_code') }}" required >
                            </div>
                            <div class="form-group">
                                <label for="latitude">Latitude</label>
                                <input type="text" class="form-control" name="latitude" value="{{ old('latitude') }}" required >
                            </div>
                            <div class="form-group">
                                <label for="longitude">Longitude</label>
                                <input type="text" class="form-control" name="longitude" value="{{ old('longitude') }}" required >
                            </div>
                            <div class="form-group">
                                <label for="phone">Phone</label>
                                <input type="text" class="form-control" name="phone" value="{{ old('phone') }}" required >
                            </div>
                            <div class="form-group">
                                <label for="store_manager">Store Manager</label>
                                <input type="text" class="form-control" name="store_manager" value="{{ old('store_manager') }}" required >
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="text" class="form-control" name="email" value="{{ old('email') }}" required >
                            </div>
                            <div class="form-group">
                                <label for="store_code">Store Code</label>
                                <input type="text" class="form-control" name="store_code" value="{{ old('store_code') }}" required >
                            </div>
                            <div class="form-group">
                                <label for="store_manager_image">Store Manager Image</label>
                                <input type="file" class="form-control" name="store_manager_image" required >
                                <label>Image size: 350px x 350px. 72ppi.</label>
                            </div>
                            <div class="form-group">
                                <label for="store_hours">Store Hours</label>
                                <input type="text" class="form-control" name="store_hours" value="{{ old('store_hours') }}" required >
                            </div>
                            <div class="form-group">
                                <label for="tax_rate">TAX Rate</label>
                                <div class="input-group">
                                    <input type="number" class="form-control" name="tax_rate" value="{{ old('tax_rate') }}"  step="0.001" required >
                                    <div class="input-group-addon">%</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="price_strategy">@lang('stores.price-strategy')</label>
                                <input type="number" class="form-control text-center" name="price_strategy" value="{{ old('price_strategy') }}" required >
                            </div>
                            <div class="form-group">
                                <label for="brand">Brand</label>
                                <select name="brand_id" id="brand_id" class="form-control">
                                    <option value="">Select...</option>
                                    @foreach($brands as $brand)
                                        <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="pharmacy_hours">Pharmacy Hours</label>
                                <input type="text" class="form-control" name="pharmacy_hours" value="{{ old('pharmacy_hours') }}">
                            </div>
                            <div class="form-group">
                                <label for="catering_events_hours">Catering & Events Hours</label>
                                <input type="text" class="form-control" name="catering_events_hours" value="{{ old('catering_events_hours') }}" >
                            </div>
                            <div class="form-group">
                                <label for="deli_hours">Deli Hours</label>
                                <input type="text" class="form-control" name="deli_hours" value="{{ old('deli_hours') }}" >
                            </div>
                            <div class="form-group">
                                <label for="bakery_hours">Bakery Hours</label>
                                <input type="text" class="form-control" name="bakery_hours" value="{{ old('bakery_hours') }}">
                            </div>
                            <div class="form-group">
                                <label for="produce_hours">Produce Hours</label>
                                <input type="text" class="form-control" name="produce_hours" value="{{ old('deli_hours') }}" >
                            </div>
                            <div class="form-group">
                                <label for="floral_hours">Floral Hours</label>
                                <input type="text" class="form-control" name="floral_hours" value="{{ old('floral_hours') }}">
                            </div>
                            <div class="form-group">
                                <label for="guest_services_hours">@lang('stores.guest-services-hours')</label>
                                <input type="text" class="form-control" name="guest_services_hours" value="{{ old('guest_services_hours') }}">
                            </div>
                            <div class="form-group">
                                <label for="stripe_key">Stripe Key</label>
                                <input type="text" class="form-control" name="stripe_key" value="{{ old('stripe_key') }}">
                            </div>
                            <div class="form-group">
                                <label for="stripe_secret">Stripe Secret</label>
                                <input type="text" class="form-control" name="stripe_secret" value="{{ old('stripe_secret') }}">
                            </div>
                            <div class="form-group">
                                <label for="paypal_username">Paypal Username</label>
                                <input type="text" class="form-control" name="paypal_username" value="{{ old('paypal_username') }}">
                            </div>
                            <div class="form-group">
                                <label for="paypal_password">Paypal Password</label>
                                <input type="text" class="form-control" name="paypal_password" value="{{ old('paypal_password') }}">
                            </div>
                            <div class="form-group">
                                <label for="paypal_secret">Paypal Key</label>
                                <input type="text" class="form-control" name="paypal_secret" value="{{ old('paypal_secret') }}">
                            </div>
                            <div class="form-group">
                                <label for="status">@lang('general.status')</label>
                                <select name="status" id="status" class="form-control">
                                    <option value="">Select</option>
                                    <option value="1">Open</option>
                                    <option value="0">Closed</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="is_cost_plus"><input type="checkbox" name="is_cost_plus" value="1"> Is Cost Plus Store</label>
                            </div>
                            <button class="btn btn-success btn-block" type="submit">Save changes</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection