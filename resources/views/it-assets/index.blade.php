@extends('layouts.app')

@section('content')

    <div class="container">
        <p class="text-right">
            <a href="{{ action('ItAssetsController@create') }}" class="btn btn-success">
                Add A New Asset
            </a>
        </p>
        <div class="panel panel-primary">
            <div class="panel-heading">
                IT Assets
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <tr>
                            <th>Type</th>
                            <th>Brand or Platform</th>
                            <th>Model</th>
                            <th>Serial Number</th>
                            <th>Username</th>
                            <th>Password</th>
                            <th>Price</th>
                        </tr>
                        @foreach($assets as $asset)
                        <tr>
                            <th class="text-uppercase">
                                {{ $asset->type }}
                            </th>
                            <th>
                                <a href="{{ action('ItAssetsController@edit', $asset->id) }}">
                                    {{ $asset->name }}
                                </a>
                            </th>
                            <th>{{ $asset->model }}</th>
                            <th>{{ $asset->serial }}</th>
                            <th>{{ $asset->username }}</th>
                            <th>{{ $asset->password }}</th>
                            <th>{{ $asset->price }}</th>
                        </tr>
                        @endforeach
                    </table>
                    {{ $assets->render() }}
                </div>
            </div>
        </div>
    </div>

    @endsection