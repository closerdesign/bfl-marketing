@extends('layouts.app')

@section('content')

    <div class="container">
        <form action="{{ action('ItAssetsController@store') }}" method="post">
            {{ csrf_field() }}
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Create IT Asset
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="employee_id">Employee</label>
                        <select name="employee_id" id="employee-id" class="form-control">
                            <option value="">Select...</option>
                            @foreach(\App\Employee::orderBy('name')->get() as $employee)
                                <option value="{{ $employee->id }}">{{ $employee->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="type">Type</label>
                        <select name="type" id="type" class="form-control" required >
                            <option value="">Select...</option>
                            <option value="software">Software</option>
                            <option value="hardware">Hardware</option>
                            <option value="other">Other</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Device or Platform Name</label>
                        <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Apple iPad Pro or Dropbox" required >
                    </div>
                    <div class="form-group">
                        <label for="model">Model</label>
                        <input type="text" class="form-control" name="model" value="{{ old('model') }}" placeholder="Any additional information to recognize the device">
                    </div>
                    <div class="form-group">
                        <label for="serial">Serial Number</label>
                        <input type="text" class="form-control" name="serial" value="{{ old('serial') }}" placeholder="i.e. ABC12345">
                    </div>
                    <div class="form-group">
                        <label for="accesories">Accesories</label>
                        <textarea name="accessories" id="accessories" cols="30" rows="10"
                                  class="form-control" placeholder="Includes charger, earphones and Apple Pencil">{{ old('accessories') }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="comments">Comments</label>
                        <textarea name="comments" id="comments" cols="30" rows="10"
                                  class="form-control" placeholder="Minor scratches in the screen">{{ old('comments') }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" class="form-control" name="username" value="{{ old('username') }}" autocomplete="off" placeholder="Optional" >
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" autocomplete="off" placeholder="Optional">
                    </div>
                    <div class="form-group">
                        <label for="price">Price</label>
                        <input type="number" class="form-control" name="price" value="{{ old('price') }}" placeholder="852" >
                    </div>
                    <div class="form-group">
                        <label for="status">Status</label>
                        <select name="status" id="status" class="form-control">
                            <option value="">Select...</option>
                            <option value="pending">Pending</option>
                            <option value="assigned">Assigned</option>
                            <option value="stolen">Stolen</option>
                            <option value="lost">Lost</option>
                            <option value="sold">Sold</option>
                        </select>
                    </div>
                </div>
            </div>
            <button class="btn btn-success form-control">
                Assign Device or Platform Access
            </button>
        </form>
    </div>

    @endsection