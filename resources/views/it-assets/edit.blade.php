@extends('layouts.app')

@section('content')

    <div class="container">
        <form action="{{ action('ItAssetsController@update', $asset->id) }}" method="post">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Create IT Asset
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="employee_id">Employee</label>
                        <select name="employee_id" id="employee-id" class="form-control">
                            <option value="">Select...</option>
                            @foreach(\App\Employee::orderBy('name')->get() as $employee)
                                <option @if($asset->id == $employee->id) selected @endif value="{{ $employee->id }}">{{ $employee->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="type">Type {{ $asset->type }}</label>
                        <select name="type" id="type" class="form-control" disabled="disabled" required >
                            <option value="">Select...</option>
                            <option @if( $asset->type == 'software' ) selected @endif value="software">Software</option>
                            <option @if( $asset->type =='hardware' ) selected @endif value="hardware">Hardware</option>
                            <option @if( $asset->type == 'other' ) selected @endif value="other">Other</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Device or Platform Name</label>
                        <input type="text" class="form-control" name="name" value="{{ $asset->name }}" placeholder="Apple iPad Pro or Dropbox" disabled="disabled" required >
                    </div>
                    <div class="form-group">
                        <label for="model">Model</label>
                        <input type="text" class="form-control" name="model" value="{{ $asset->model }}" placeholder="Any additional information to recognize the device" disabled="disabled">
                    </div>
                    <div class="form-group">
                        <label for="serial">Serial Number</label>
                        <input type="text" class="form-control" name="serial" value="{{ $asset->serial }}" placeholder="i.e. ABC12345" disabled="disabled">
                    </div>
                    <div class="form-group">
                        <label for="accesories">Accesories</label>
                        <textarea name="accessories" id="accessories" cols="30" rows="10"
                                  class="form-control" placeholder="Includes charger, earphones and Apple Pencil">{{ $asset->accessories }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="comments">Comments</label>
                        <textarea name="comments" id="comments" cols="30" rows="10"
                                  class="form-control" placeholder="Minor scratches in the screen">{{ $asset->comments }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" class="form-control" name="username" value="{{ $asset->username }}" autocomplete="off" placeholder="Optional" >
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" autocomplete="off" value="{{ $asset->password }}" placeholder="Optional">
                    </div>
                    <div class="form-group">
                        <label for="price">Price</label>
                        <input type="number" class="form-control" name="price" value="{{ $asset->price }}" placeholder="852" disabled="disabled" >
                    </div>
                    <div class="form-group">
                        <label for="status">Status</label>
                        <select name="status" id="status" class="form-control">
                            <option value="">Select...</option>
                            <option @if( $asset->status == 'pending' ) selected @endif value="pending">Pending</option>
                            <option @if( $asset->status == 'assigned' ) selected @endif value="assigned">Assigned</option>
                            <option @if( $asset->status == 'stolen' ) selected @endif value="stolen">Stolen</option>
                            <option @if( $asset->status == 'lost' ) selected @endif value="lost">Lost</option>
                            <option @if( $asset->status == 'sold' ) selected @endif value="sold">Sold</option>
                            <option @if( $asset->status == 'broken' ) selected @endif value="broken">Broken</option>
                        </select>
                    </div>
                </div>
            </div>
            <button class="btn btn-success form-control">
                Assign Device or Platform Access
            </button>
        </form>
    </div>

@endsection