@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <p class="lead text-center">Inventory System</p>
                @foreach( $inventories as $inventory )
                <a href="{{ action('InventoryController@show', $inventory->id) }}">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <p class="text-center">
                                Store: {{ $inventory->store->store_code }}
                                <br />{{ $inventory->controller_name }}
                            </p>
                            <p class="text-center">
                                <small style="color: green;">
                                    <b>{{ $inventory->status }}</b>
                                </small>
                            </p>
                        </div>
                    </div>
                </a>
                @endforeach
                <div class="form-group">
                    <a href="{{ action('InventoryController@create') }}" class="btn btn-success btn-block">
                        Start New Inventory
                    </a>
                </div>
            </div>
        </div>
    </div>

    @endsection