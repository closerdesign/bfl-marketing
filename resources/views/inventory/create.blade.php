@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Start Inventory
                    </div>
                    <div class="panel-body">
                        <form action="{{ action('InventoryController@store') }}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="store_id">@lang('stores.id')</label>
                                <select name="store_id" id="store_id" class="form-control">
                                    <option value="">@lang('general.select')</option>
                                    @foreach(\App\Store::where('status', 1)->orderBy('store_code')->get() as $store)
                                        <option @if( auth()->user()->store == $store->store_code  ) selected @endif  value="{{ $store->id }}">{{ $store->store_code }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="controller">@lang('inventory.controller-name')</label>
                                <input type="text" class="form-control" name="controller_name" value="{{ auth()->user()->name }}" required >
                            </div>
                            <button class="btn btn-success btn-block">
                                @lang('inventory.start')
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection