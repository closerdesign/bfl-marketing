@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form action="{{ action('InventoryItemController@store', $id) }}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="upc">@lang('inventory-item.upc')</label>
                                <input type="text" class="form-control text-center" name="upc" id="upc" required >
                            </div>
                            <div class="form-group">
                                <label for="qty">@lang('inventory-item.qty')</label>
                                <input type="text" class="form-control text-center" name="qty" value="1" required >
                            </div>
                            <button class="btn btn-success btn-block">
                                @lang('inventory-item.send')
                            </button>
                        </form>
                    </div>
                </div>
                <p class="text-center">
                    Store: {{ $inventory->store->store_code }}<br />
                    {{ $inventory->controller_name }}
                </p>
                <div class="form-group">
                    <a href="{{ action('InventoryController@index') }}" class="btn btn-default btn-block">
                        @lang('inventory.go-back')
                    </a>
                </div>
                <div class="form-group">
                    <form id="complete-inventory" action="{{ action('InventoryController@update', $inventory->id) }}" method="post">
                        @csrf
                        @method('PATCH')
                        <input type="hidden" name="status" value="completed">
                    </form>
                    <a href="#" onclick="completeInventory()" class="btn btn-danger btn-block">
                        @lang('inventory.finish')
                    </a>
                </div>
            </div>
        </div>
    </div>

    @endsection

@section('js')

    <script>
        function completeInventory()
        {
            var confirmation = confirm('@lang('general.are-you-sure')');

            if( confirmation === true )
            {
                document.getElementById('complete-inventory').submit();
            }
        }

        window.onload = function() {
            var input = document.getElementById("upc").focus();
        }
    </script>

    @endsection