@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <p class="text-right">
            <a href="{{ action('InventoryController@pricing_import', $inventory->id) }}" class="btn btn-success">
                Import Pricing
            </a>
            <a href="{{ action('InventoryController@export', $inventory->id) }}" class="btn btn-success">
                @lang('inventory.export')
            </a>
        </p>
        <div class="panel panel-primary">
            <div class="panel-heading">
                [{{ date('m-d-Y', strtotime($inventory->created_at)) }}] {{ $inventory->store->store_code }} by {{ $inventory->controller_name }} ({{ $inventory->status }})
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <p>
                        <i>{{ $inventory->items->count() }} items found.</i>
                    </p>
                    <table class="table table-striped table-condensed">
                        <thead>
                        <tr>
                            <th>UPC</th>
                            <th>Description</th>
                            <th>Qty</th>
                            <th>Retail Price</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $grand_total = 0; @endphp
                        @foreach( $inventory->items as $item )
                        <tr>
                            <td>{{ $item->upc }}</td>
                            <td>{{ $item->description }}</td>
                            <td>{{ $item->qty }}</td>
                            <td>{{ $item->retail_price }}</td>
                            <td>{{ $item->qty * $item->retail_price }}</td>
                        </tr>
                        @php $grand_total += ($item->qty * $item->retail_price); @endphp
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="4">TOTAL</th>
                            <th>{{ number_format($grand_total, 2) }}</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @endsection