@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Inventories
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-condensed">
                        <thead>
                        <tr>
                            <th>Store</th>
                            <th>Controller Name</th>
                            <th>Created At</th>
                            <th>Status</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach( $inventories as $inventory )
                        <tr>
                            <th>{{ $inventory->store->store_code }}</th>
                            <td>{{ $inventory->controller_name }}</td>
                            <td>{{ $inventory->created_at }}</td>
                            <td>{{ $inventory->status }}</td>
                            <td>
                                <a href="{{ action('InventoryController@report', $inventory->id) }}"
                                   class="btn btn-default btn-block">View Report</a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $inventories->render() }}
                </div>
            </div>
        </div>
    </div>

    @endsection