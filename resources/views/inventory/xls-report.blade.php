<table class="table table-striped table-condensed">
    <thead>
    <tr>
        <th>UPC</th>
        <th>Description</th>
        <th>Qty</th>
        <th>Retail Price</th>
        <th>Total</th>
    </tr>
    </thead>
    <tbody>
    @php $grand_total = 0; @endphp
    @foreach( $items as $item )
        <tr>
            <td>{{ $item->upc }}</td>
            <td>{{ $item->description }}</td>
            <td>{{ $item->qty }}</td>
            <td>{{ $item->retail_price }}</td>
            <td>{{ $item->qty * $item->retail_price }}</td>
        </tr>
        @php $grand_total += ($item->qty * $item->retail_price); @endphp
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <th colspan="4">TOTAL</th>
        <th>{{ number_format($grand_total, 2) }}</th>
    </tr>
    </tfoot>
</table>