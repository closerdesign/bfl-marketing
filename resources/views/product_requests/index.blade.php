@extends('layouts.app')

@section('content')

    <?php $brand = [1 => 'Buy For Less', 2 => 'Uptown Grocery Co', 3 => 'Smart Saver', 4 => 'SuperMercado']; ?>

    <div class="container">

        <div class="panel panel-primary">
            <div class="panel-heading">Product Requests</div>

            <div class="panel-body">

                <div class="table-responsive">

                    <table class="table table-striped table-hover table-condensed">

                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Brand</th>
                            <th>Keyword</th>
                            <th>Message</th>
                            <th>Number</th>
                            <th>Date / Time</th>
                        </tr>
                        </thead>

                        <tbody>
                        @if( count($product_requests) == 0 )
                            <tr><td colspan="6">No product requests have been submitted.</td></tr>
                        @else
                            @foreach($product_requests as $request)
                                <tr>
                                    <td>
                                        {{ $request->id }}
                                    </td>
                                    <td>
                                        {{ $brand[$request->brand_id] }}
                                    </td>
                                    <td>
                                        {{ $request->keyword }}
                                    </td>
                                    <td width="50%">
                                        {{ $request->text }}
                                    </td>
                                    <td>
                                        {{ $request->msisdn }}
                                    </td>
                                    <td>
                                        {{ $request->created_at->format('m/d/Y  g:i A') }}
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>

                    </table>

                    {{ $product_requests->render() }}

                </div>

            </div>

        </div>

    </div>

@endsection