@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <h3 class="text-primary text-center">Brand Advisory Board (BAB) Meeting</h3>
                <h4>Meeting occurance</h4>
                3rd Thursday of every month
                <h4>Who's Leading this Meeting?</h4>
                Brand Executives (Blanca, Dino, Josh & Ron)
                <h4>Who's Invited?</h4>
                <ul>
                    <li>Each Store Director within the Brand</li>
                    <li>1-2 additional team member(s) from each store within the brand</li>
                    <li>1 member from every department in the Support Center (Brand Execs can extend personal
                        invitations/requests to SC members & they can change each month)</li>
                    <li class="text-danger">8-15 people present total depending on # of stores within brand</li>
                </ul>
                <h4>Location</h4>
                Each of the 4 Brand Executives meet at the location of their choosing.
                <h4>How are these members chosen?</h4>
                These team members should be chosen to compliment what you lack in providing perspective for your specific
                brand as a whole. This group needs to be diverse. These members are made up from employees from your
                specific store that you lead. Each BAB member is <b>REQUIRED</b> to have a strengths finder test on file.
                If they do not please add their name &amp; email in the last section of this form or email kkelly@buyforlessok.com.
                <hr />
                <h3 class="text-primary text-center">Discussion Questions/Ideas</h3>
                <h4>PRAISE REPORTS</h4>
                Which team member(s) in this brand are doing exceptionally well in their role? How are they winning? How
                are they winning? Why are they worthy of praise/recognition? What will you do to recognize them?
                <h4>WINS</h4>
                How is this brand winning in serving its particular demographics &amp; communities?
                <h4>AREAS OF GROWTH</h4>
                How does this brand need to improve in serving its particular demographics & communities?
                <h4>AREAS OF GROWTH - HOW &amp; P.O.A.</h4>
                Now that you've identified how you need to grow - how will you do it? What's the plan for this brand?
                <h4>GUEST #1 - WAYS TO LOVE &amp; ENGAGE</h4>
                How can we love the #1 guest better? Ideas for special events, incentives, recognition, contests, prizes, etc.
                <h4>GUEST #2 - WAYS TO LOVE &amp; ENGAGE</h4>
                How can we love our #2 guest better? Ideas for special events, promos, contests, prizes, offers, etc.
                <h4>SUPPORT NEEDS</h4>
                What do you need from the Support Center - marketing materials, scanning Materials, hiring/recruiting,
                maintenance requests, financial reports, metrics, training, tool creation, etc.
                <h4>LEARNING FROM SUPPORT CENTER</h4>
                What did you learn from the support center team members who were present? Any new processes, systems,
                methods, or information?
                <h4>BAB FOCUS - TOP 1 - OLD</h4>
                What top area did you choose to focus on last month as a BAB team? Did you successfully accomplish this
                goal? Does it need to carry over to next month's meeting?
                <h4>BAB FOCUS - TOP 1 - NEW</h4>
                What top area will you be focusing on together as a BAB team? How will you successfully accomplish the goal?
                <h4>ADDITIONAL INFORMATION</h4>
                Is there anything else that was discussed that doesn't fit into the boxes above and needs to be
                shared/documented? Add it here.
                <h4>ATTACHMENT UPLOAD</h4>
                If you took photos of anything that help support this document or need to share any documents that support
                this document upload them here.
            </div>
            <div class="col-md-7">
                <h3 class="text-primary text-center">The Organizational Why</h3>
                <p>
                    We exist to love people well by focusing our time, energy and resources on loving and investing in our
                    #1 Guest - The employee who will impact the #2 Guest - Our customers.
                </p>
                <p>
                    "Positively impact & serve the community better and more completely than anyone else in our space.
                    Through that we inspire our guests to contribute and trust in us and our mission." - Ryan Potter
                </p>
                <p class="text-right"><span class="text-danger">*</span> = Required Field</p>
                <div class="panel panel-primary">
                    <div class="panel-heading">Feedback Form</div>
                    <div class="panel-body">
                        <form action="{{ action('BABFeedbackController@store') }}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="first_name">
                                        Who is completing this form? <span class="text-danger">*</span>
                                    </label>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <input type="text" class="form-control" placeholder="First Name" name="first_name" value="{{ old('first_name') }}" required>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <input type="text" class="form-control" placeholder="Last Name" name="last_name" value="{{ old('last_name') }}" required>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="role">What is your role?</label> <span class="text-danger">*</span>
                                    <input type="text" class="form-control" name="role" value="{{ old('role') }}" required>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="store">What location are you from?</label> <span class="text-danger">*</span>
                                    <select class="form-control" name="store" required >
                                        <option value="" >Select...</option>
                                        @foreach(\App\Store::orderBy('store_code')->get() as $store)
                                            <option value="{{ $store->store_code }}">[{{ $store->store_code }}] {{ $store->name }}</option>
                                        @endforeach
                                        <option value="8953">[8953] Support Center</option>
                                    </select>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="email">What is your email address?</label> <span class="text-danger">*</span>
                                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="executive_email">What is the Brand Executive's email address?</label> <span class="text-danger">*</span>
                                    <input type="email" class="form-control" name="executive_email" value="{{ old('executive_email') }}" required>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="location">Location of this BAB meeting</label> <span class="text-danger">*</span>
                                    <select class="form-control" name="location" required >
                                        <option value="" >Select...</option>
                                        @foreach(\App\Store::orderBy('store_code')->get() as $store)
                                            <option value="{{ $store->store_code }}">[{{ $store->store_code }}] {{ $store->name }}</option>
                                        @endforeach
                                        <option value="8953">[8953] Support Center</option>
                                    </select>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="meeting_date">Date the BAB Meeting Occurred</label> <span class="text-danger">*</span>
                                    <input type="date" class="form-control" name="meeting_date" value="{{ old('meeting_date') }}" required />
                                </div>
                                <div class="col-md-6 form-group">
                                    <label for="time_start">What time did the meeting BEGIN?</label> <span class="text-danger">*</span>
                                    <input type="time" class="form-control" name="time_start" value="{{ old('time_start') }}" required>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label for="time_end">What time did the meeting END?</label> <span class="text-danger">*</span>
                                    <input type="time" class="form-control" name="time_end" value="{{ old('time_end') }}" required>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="leader">Who led the meeting today?</label> <span class="text-danger">*</span>
                                    <input type="text" class="form-control" name="leader" value="{{ old('leader') }}" required>
                                    <small>Who prepared the agenda and facilitated the direction of the meeting?</small>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="present">Who was present for the meeting?</label> <span class="text-danger">*</span>
                                    <textarea class="form-control" name="present" rows="3">{{ old('present') }}</textarea>
                                    <small>LIST NAME & DEPARTMENT (e.g.: Jasmine A'Laddin - Bakery Mgr)</small>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="absent">Who was invited but absent from the meeting?</label> <span class="text-danger">*</span>
                                    <textarea class="form-control" name="absent" rows="3">{{ old('absent') }}</textarea>
                                    <small>LIST NAME & DEPARTMENT (e.g.: Mickey Mousinger - FEM)</small>
                                </div>
                                <div class="col-md-12">
                                    <h3 class="text-primary text-center">Agenda Reporting</h3>
                                    <p>
                                        These are the same guides each month but your answers should be different and reflect
                                        forward motion of what was shared &amp; discussed last month.
                                    </p>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="praise_report">Praise Report</label> <span class="text-danger">*</span>
                                    <textarea class="form-control" name="praise_report" rows="3">{{ old('praise_report') }}</textarea>
                                    <small>
                                        Which team member(s) in this brand are doing exceptionally well in their role?
                                        How are they winning? Why are they worthy of praise/recognition? What will you
                                        do to recognize them?
                                    </small>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="wins">WINS - Serving Brand's Demographic</label> <span class="text-danger">*</span>
                                    <textarea class="form-control" name="wins" rows="3">{{ old('wins') }}</textarea>
                                    <small>What are we doing well to serve this particular brand's demographic/community?</small>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="brand_demo">AREAS OF GROWTH - Serving Brand's Demographic</label> <span class="text-danger">*</span>
                                    <textarea class="form-control" name="brand_demo" rows="3">{{ old('brand_demo') }}</textarea>
                                    <small>Where do we need to do better in serving this particular brand's demographic/community?</small>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="how_poa">AREAS OF GROWTH - How &amp; POA</label> <span class="text-danger">*</span>
                                    <textarea class="form-control" name="how_poa" rows="3">{{ old('how_poa') }}</textarea>
                                    <small>How will we grow? What is the plan to approve the areas mentioned in the previous question?</small>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="guest_one">Guest #1 - Ways to Love &amp; Engage</label> <span class="text-danger">*</span>
                                    <textarea class="form-control" name="guest_one" rows="3">{{ old('guest_one') }}</textarea>
                                    <small>Ideas for growth opportunities, leadership development, training needs, etc.</small>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="guest_two">Guest #2 - Ways to Love &amp; Engage</label> <span class="text-danger">*</span>
                                    <textarea class="form-control" name="guest_two" rows="3">{{ old('guest_two') }}</textarea>
                                    <small>What ideas do we have to improve guest satisfaction? Specials, promos, events, displays, etc.</small>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="support_needs">Support Needs</label> <span class="text-danger">*</span>
                                    <textarea class="form-control" name="support_needs" rows="3">{{ old('support_needs') }}</textarea>
                                    <small>
                                        What do you need from the Support Center - marketing materials, scanning Materials,
                                        hiring/recruiting, maintenance requests, financial reports, metrics, training, tool creation, etc.
                                    </small>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="learning_sc">Learning From Support Center</label> <span class="text-danger">*</span>
                                    <textarea class="form-control" name="learning_sc" rows="3">{{ old('learning_sc') }}</textarea>
                                    <small>
                                        What did you learn from the support center team members who were present? Any new
                                        processes, systems, methods, or information?
                                    </small>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="focus_old">BAB FOCUS - Top 1 - Old</label> <span class="text-danger">*</span>
                                    <textarea class="form-control" name="focus_old" rows="3">{{ old('focus_old') }}</textarea>
                                    <small>
                                        Did you achieve the focus of last month? How did it go? Does it need to roll over
                                        to the focus for this month? Add it below if yes.
                                    </small>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="focus_new">BAB FOCUS - Top 1 - New</label> <span class="text-danger">*</span>
                                    <textarea class="form-control" name="focus_new" rows="3">{{ old('focus_new') }}</textarea>
                                    <small>
                                        What top area will you be focusing on together for this brand as a team? How will
                                        you successfully accomplish the goal?
                                    </small>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="receipt_ideas">
                                        Please provide AT LEAST 3 ideas for the bottom of your brand's receipts for next
                                        month. <span class="text-danger">*</span>
                                    </label>
                                    <textarea class="form-control" name="receipt_ideas" rows="3">{{ old('receipt_ideas') }}</textarea>
                                    <small>Media will make use of this information.</small>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="addtl_info">Additional Information</label> <span class="text-danger">*</span>
                                    <textarea class="form-control" name="addtl_info" rows="3">{{ old('addtl_info') }}</textarea>
                                    <small>
                                        Is there anything else that was discussed that doesn't fit into the boxes above
                                        and needs to be shared/documented? Add it here.
                                    </small>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="attachment">Attachment</label>
                                    <input type="file" class="form-control" name="attachment" />
                                    <small>
                                        If you took photos of anything that help support this document or need to share any
                                        documents that support this document upload them here.
                                    </small>
                                </div>
                                <div class="col-md-12 form-group">
                                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection