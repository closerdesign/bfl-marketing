@extends('layouts.app')

@section('content')
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>
    <div class="container">
        <div class="row">
            This is the menu
            <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#itemcollapse" aria-expanded="false" aria-controls="itemcollapse">Add Item</button>
            <div class="collapse" id="itemcollapse">
                <div class="panel panel-primary">
                    <div class="panel-heading" id="add_item">@{{message}}</div>
                    <div class="panel-body">
                        <form action="">
                            <div class="form-group">
                                <label for="item_name">Item Name</label>
                                <input class="form-control" name="item_name" id="item_name">
                            </div>
                            <div class="form-group">
                                <label for="category">Category</label>
                                <select id="category">
                                    @if(!empty($item_categories))
                                        @foreach(@$item_categories as $item_category)
                                            <option value="{{@$item_category->id}}">{{@$item_category->category_name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <button class="btn btn-sm btn-primary d-inline" data-toggle="modal" data-target="#categoryModal" type="button">Add Category</button>
                            </div>
                            <div class="form-group">
                                <label for="item_description">Item Description</label>
                                <input id="item_description" name="item_description" value="" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="label" for="price">Item Price</label>
                                <input name="item_price" id="price" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="rank">Rank</label>
                                <input name="rank" id="rank" class="form-control">
                            </div>
                            <div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>

                        </form>

                    </div>
                </div>
            </div>

        </div>
    </div>

<!-- Modal -->

    <div class="modal fade" id="categoryModal" tabindex="-1" role="dialog" aria-labelledby="categoryModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="categoryModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

<!-- End Modal -->

@endsection

@section('js')

<script>
    var add_item = new Vue({
        el: '#add_item',
        data: {
        message: 'Add Item'
    },
        computed:{},
        methods:{},
    });

   var cats = new Vue({
       el: '#item_categories',
       data: {
           categories:[]
       },
       computed: {},
       methods: {},
       mounted() {
           axios.get('/deliMenuCategory-menu-categories/2').then((response) => {
               this.categories = response.data;
               console.log(this.categories);
           });
       },
   });

</script>
@endsection