@extends('layouts.app')

@section('content')

    <div class="container">
        <form action="{{action('LiquorLicensesController@create')}}" method="post">
            {{ csrf_field() }}
            <select name="id">
                @foreach($employees as $employee)
                    <option value="{{$employee->id}}">{{$employee->name}}</option>
                @endforeach
            </select>
            <input type="submit">
        </form>

    </div>


@endsection