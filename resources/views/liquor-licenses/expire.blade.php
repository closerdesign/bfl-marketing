<?php
/**
 * Created by PhpStorm.
 * User: davidmeinke
 * Date: 2018-12-26
 * Time: 08:32
 */?>
@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <p class="text-right">
                    <a class="btn btn-success" href="/employees">Employees</a>

                    <a href="{{ action('LiquorLicensesController@report') }}" class="btn btn-success">
                        All Licenses
                    </a>

                    <a href="{{ action('LiquorLicensesController@expiring') }}" class="btn btn-warning">
                        Expiring Licenses
                    </a>

                </p>


                <div class="panel panel-primary">
                    <div class="panel-heading">Employee's with Liquor Licenses expiring within 30 days</div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-condensed datatable" id="licenses">
                                <thead>
                                <tr>

                                    <th>Name</th>
                                    <th>License Number</th>
                                    <th>Expiration Date</th>
                                </tr>
                                </thead>
                                @foreach($data as $d)
                                    <tr>
                                        <td>

                                            <a href="/employees/{{$d->employee_id}}/edit#liquor-licenses">
                                                {{ $d->name }}
                                            </a>

                                        </td>
                                        <td>
                                            {{$d->license_number}}
                                        </td>
                                        <td>
                                            {{date('m/d/Y', strtotime($d->expiration_date))}}
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

    </script>

@endsection
