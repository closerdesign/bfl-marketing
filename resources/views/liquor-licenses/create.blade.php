=@extends('layouts.app')

@section('content')

    <div class="container">
        <form action="{{ action('liquorlicenseController@store', $employee->id) }}" method="post">
            {{ csrf_field() }}
            <div class="panel panel-primary">
                <div class="panel-heading">Assign Employee To License</div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="name" value="{{ old('name') }}" required >
                    </div>
                    <div class="form-group">
                        <label for="license_number">License Number</label>
                        <input type="text" class="form-control" name="license_number" value="{{old('license_number')}}">
                    </div>
                    <div class="form-group">
                        <label for="expiration_date">Expiration Date</label>
                        <input type="date" class="form-control" name="expiration_date" value="{{old('expiration_date')}}">
                    </div>
                    <div class="form_group">
                        <label for="file" >File</label>
                        <input type="file" class="form-control" name="file">

                    </div>

                </div>
            </div>
        </form>
    </div>
    
    
<!--
    <div class="container">
        <p>
            <a href="{{ action('CashiersController@index') }}" class="btn btn-primary">
                <i class="fa fa-arrow-circle-left"></i> Go Back
            </a>
        </p>
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-primary">
                    <div class="panel-heading">Add Liquor License for {{ $cashier[0]->name }}</div>
                    <div class="panel-body">
                        <div class="row">

                            <form action="{{ action('LiquorLicensesController@store') }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="col-md-6 form-group">
                                    <label>Due Date</label>
                                    <input type="date" class="form-control" name="due_date" required />
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>PDF</label>
                                    <input type="file" class="form-control" name="image" required />
                                </div>
                                <div class="col-md-12 form-group">
                                    <button class="btn btn-block btn-success">Submit</button>
                                </div>
                                <input type="hidden" name="cashier_number" value="{{ $cashier[0]->cashier_number }}" />
                            </form>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-info">
                    <div class="panel-heading">Uploaded Licenses</div>
                    <div class="panel-body">

                        @if(count($licenses) < 1)
                            <p class="text-center text-muted">There are no licenses added for this cashier.</p>
                        @else
                            <ul class="list-group">
                                @foreach($licenses as $license)
                                    <li class="list-group-item">
                                        <a href="{{ Storage::url('liquor-licenses/' . $license->image) }}" target="_blank">
                                            {{ $license->image }}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
    
    -->

@endsection