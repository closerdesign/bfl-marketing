<?php
/**
 * Created by PhpStorm.
 * User: TheLegend
 * Date: 9/13/18
 * Time: 9:28 AM
 */
?>
@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <p class="text-right">
                    <a class="btn btn-success" href="/employees">Employees</a>
                </p>
                <div class="panel panel-primary">
                    <div class="panel-heading">Employee's with Liquor License</div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-condensed" id="licenses">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>License Number</th>
                                    <th>Expiration Date</th>
                                </tr>
                                </thead>
                                @foreach($licenses as $license)
                                    <tr>
                                        <td>
                                            {{$license->id}}
                                        </td>
                                        <td>

                                            <a href="/employees/{{$license->employee->id}}/edit#liquor-licenses">
                                                {{ $license->employee->name }}
                                            </a>

                                        </td>
                                        <td>
                                            {{$license->license_number}}
                                        </td>
                                        <td>
                                            {{date('m/d/Y', strtotime($license->expiration_date))}}
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

    </script>

@endsection

@section('js')

    <script>
        $(document).ready( function () {
            $('#licenses').DataTable();
        });

    </script>

@endsection