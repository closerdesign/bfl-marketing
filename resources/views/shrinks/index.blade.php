@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">Shrinks Report</div>
            <div class="panel-body">
                <div class="col-md-3">
                    <form action="{{ action('ShrinksController@store') }}" method="post">
                        <div class="panel panel-default">
                            <div class="panel-heading">Report Shrink</div>
                            <div class="panel-body">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="shrink_group_id">Group</label>
                                    <select name="shrink_group_id" id="shrink_group_id" class="form-control" required >
                                        <option value="">Select...</option>
                                        @foreach(\App\ShrinkGroup::all() as $group)
                                            <option value="{{ $group->id }}">{{ $group->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="shrink_item_id">Item</label>
                                    <select name="shrink_item_id" id="shrink_item_id" class="form-control" required >
                                        <option value="">Select...</option>

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="store_id">Store</label>
                                    <select name="store_id" id="store_id" class="form-control" required >
                                        <option value="">Select...</option>
                                        @foreach(\App\Store::orderBy('store_code')->get() as $store)
                                            <option value="{{ $store->id }}">{{ $store->store_code }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="date">Date</label>
                                    <input type="text" class="form-control datepicker" name="date" value="{{ old('date') }}" required >
                                </div>
                                <div class="form-group">
                                    <label for="value">Value</label>
                                    <input type="text" class="form-control" name="value" value="{{ old('value') }}" required >
                                </div>
                                <button class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-9">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form class="form-inline">
                                <div class="form-group">
                                    <label class="sr-only" for="date_from">Date From</label>
                                    <input type="text" class="form-control datepicker" name="date_from" placeholder="From">
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="date_to">Date To</label>
                                    <input type="text" class="form-control datepicker" name="date_to" placeholder="To">
                                </div>
                                <button type="submit" class="btn btn-success">Filter</button>
                            </form>
                        </div>
                    </div>
                    @if( isset($date_from) && isset($date_to) )
                        <div class="panel panel-primary">
                            <div class="panel-heading">From {{ $date_from }} to {{ $date_to }}</div>
                            @if(empty($groups))
                            <div class="panel-body">
                                <p>No records found.</p>
                            </div>
                            @endif
                        </div>
                    @endif
                    @foreach($groups as $group)
                    <div class="panel panel-default">
                        <div class="panel-heading">{{ $group->name }}</div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th width="40%">Item</th>
                                        <th width="20%">Amount</th>
                                        <th width="20%">Sales</th>
                                        <th width="20%">Shrink</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($group->products as $product)
                                    <tr>
                                        <th>{{ $product->name }}</th>
                                        <td class="text-right">{{ number_format($product->value, 2) }}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="panel panel-primary">
            <div class="panel-heading">Shrink Groups & Items</div>
            <div class="panel-body">

                <div class="col-md-6">
                    <p class="lead">Groups</p>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped datatable">
                            <thead>
                            <th>Name</th>
                            <th>&nbsp;</th>
                            </thead>
                            <tbody>
                            @foreach(\App\ShrinkGroup::all() as $group)
                            <tr>
                                <th>
                                    <a href="{{ action('ShrinkGroupsController@edit', $group->id) }}">
                                        {{ $group->name }}
                                    </a>
                                </th>
                                <td class="text-center">
                                    <form
                                            action="{{ action('ShrinkGroupsController@destroy', $group->id) }}"
                                            onsubmit="return confirm('Are you sure?')"
                                            method="post">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button class="btn btn-danger">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <hr>
                    <form action="{{ action('ShrinkGroupsController@store') }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="name" required >
                    </div>
                    <button class="btn btn-primary" type="submit">Save</button>
                    </form>
                </div>

                <div class="col-md-6">
                    <p class="lead">Items</p>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped datatable">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Group</th>
                                <th>&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach(\App\ShrinkItem::all() as $item)
                            <tr>
                                <th>
                                    <a href="{{ action('ShrinkItemsController@edit', $item->id) }}">
                                        {{ $item->name }}
                                    </a>
                                </th>
                                <td>
                                    {{ $item->group->name }}
                                </td>
                                <td>
                                    <form
                                        action="{{ action('ShrinkItemsController@destroy', $item->id) }}"
                                        method="post"
                                        onsubmit="return confirm('Are you sure?')"
                                    >
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button class="btn btn-danger">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <hr>
                    <form action="{{ action('ShrinkItemsController@store') }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}" required >
                        </div>
                        <div class="form-group">
                            <label for="shrink_group_id">Group</label>
                            <select name="shrink_group_id" id="shrink_group_id" class="form-control">
                                <option value="">Select...</option>
                                @foreach(\App\ShrinkGroup::all() as $group)
                                    <option value="{{ $group->id }}">{{ $group->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <button class="btn btn-primary">Save</button>
                    </form>
                </div>

            </div>
        </div>
    </div>

    @endsection

@section('js')

    <script>
        $('#shrink_group_id').change(function(){

            var id = $(this).val();

            $.get('/shrink-items/list/' + id)
                .done(function(data){
                    $('#shrink_item_id').html(data);
                });

        });
        $('.shrink-datatable').dataTable({
            "bPaginate": false
        });
    </script>

    @endsection