@extends('layouts.app')

@section('content')

    <div class="container">
        <form action="{{ action('EmployeesController@discount_request_process') }}" method="post">
            {{ csrf_field() }}
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Employees Discount Request
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="name" value="{{ old('name') }}" required >
                    </div>
                    <div class="form-group">
                        <label for="clock_in_code">Clock In Code</label>
                        <input type="number" class="form-control" name="clock_in_code" value="{{ old('clock_in_code') }}" required >
                    </div>
                    <div class="form-group">
                        <label for="loyalty_member_id">Loyalty Member ID</label>
                        <input type="number" class="form-control" name="loyalty_member_id" value="{{ old('loyalty_member_id') }}" required >
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone Number</label>
                        <input type="text" class="form-control" name="phone" value="{{ old('phone') }}" required >
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" name="email" value="{{ old('email') }}" required >
                    </div>
                    <div class="form-group">
                        <label for="stores">What stores do you use to shop at?</label>
                        <textarea name="stores" id="stores" cols="30" rows="5" class="form-control">{{ old('stores') }}</textarea>
                    </div>
                </div>
            </div>
            <button class="btn btn-success">
                Send Request
            </button>
        </form>
    </div>

    @endsection