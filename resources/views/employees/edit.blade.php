@extends('layouts.app')

@section('content')

    <div class="container">
        <p class="text-right">
            <a class="btn btn-success" href="/employees">Employees</a>
        </p>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#basic-information" aria-controls="home" role="tab" data-toggle="tab">Basic Information</a></li>
            <li role="presentation"><a href="#liquor-licenses" aria-controls="liquor-licenses" role="tab" data-toggle="tab">Liquor Licenses</a></li>
            <li role="presentation"><a href="#cashier-numbers" aria-controls="cashier-numbers" role="tab" data-toggle="tab">Cashier Numbers</a></li>
            <li role="presentation"><a href="#employee-docs" aria-controls="employee-docs" role="tab" data-toggle="tab">Documents</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="basic-information">
                <form action="{{ action('EmployeesController@update', $employee->id) }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}
                    <div class="panel panel-primary">
                        <div class="panel-heading">Edit Employee: {{ $employee->name }}</div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" value="{{ $employee->name }}" required >
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" name="email" value="{{ $employee->email }}" >
                            </div>
                            <div class="form-group">
                                <label for="phone">Phone</label>
                                <input type="text" class="form-control" name="phone" value="{{ $employee->phone }}" required >
                            </div>
                            <div class="form-group">
                                <label for="employee_id">Employee ID</label>
                                <input type="text" class="form-control" name="employee_id" value="{{ $employee->employee_id }}">
                            </div>
                            <div class="form-group">
                                <label for="birthdate">Birth Date</label>
                                <input type="date" class="form-control" name="birthdate" value="{{ $employee->birthdate }}">
                            </div>
                            <div class="form-group">
                                <label for="Store">Store Number</label>
                                <select name="store_id" class="form-control">
                                    @foreach($stores as $store)
                                        <option value="{{$store->id}}" @if($employee->store_id == $store->id) selected @endif>{{$store->store_code}} - {{$store->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="checkbox" class="form-check-input" name="active" value="1" @if($employee->active ==1 ) checked @endif>
                                <label class="form-check-label" for="active">Active</label>
                            </div>
                            <div class="form-group">
                                <label for="comments">Comments</label>
                                <textarea name="comments" id="comments" cols="30" rows="10" class="form-control">{{ $employee->comments }}</textarea>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-success">
                                    <i class="fa fa-save"></i> Save Changes
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div role="tabpanel" class="tab-pane" id="liquor-licenses">
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">Liquor Licenses List</div>
                            <div class="panel-body">
                                @if( count($employee->licenses) == 0 )
                                    <p>No liquor licenses available for this employee.</p>
                                @endif
                                <div class="table-responsive">
                                    <table class="table table-striped table-condensed">
                                        <thead>
                                        <tr>
                                            <th>License Number</th>
                                            <th>Expire Date</th>
                                            <th>File Description</th>
                                            <th>Notes</th>
                                            <th>File</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($employee->licenses as $license)
                                                <tr>
                                                    <td>{{$license->license_number}}</td>
                                                    <td>{{ date('m/d/Y', strtotime($license->expiration_date)) }}</td>
                                                    <td>{{$license->file_description}}</td>
                                                    <td>@if($license->notes)
                                                            <a data-toggle="tooltip" data-placement="top" title="{{$license->notes}}">
                                                                <i class='fa fa-lg fa-info-circle' aria-hidden="true"></i>
                                                            </a>
                                                        @else &nbsp; @endif
                                                    </td>
                                                    <td><a href="{{  $license->file  }}" target="_blank" title="View Record"><i class="fa fa-lg fa-file-pdf-o"></i></a></td>
                                                    <td>
                                                        <form action="{{ action('LiquorLicensesController@destroy', $license->id) }}" method="POST" onsubmit="return confirm('Are you sure?')">
                                                            {{ csrf_field() }}
                                                            {{ method_field('DELETE') }}
                                                            <button class="btn-xs btn-danger" type="submit" title="Delete Record">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">Add A Liquor License</div>
                            <div class="panel-body">
                                <form action="{{ action('LiquorLicensesController@store', $employee->id) }}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="license_number">License Number</label>
                                        <input type="text" name="license_number" id="license_number" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="expiration_date">Expiration Date</label>
                                        <input type="date" name="expiration_date" id="expiration_date" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="file">File</label>
                                        <input type="file" name="file" id="file" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="file_description">File Description</label>
                                        <select name="file_description" id="file_description" class="form-control">
                                            <option value="">Select...</option>
                                            <option value="liquor_license">Liquor License</option>
                                            <option value="Training_Certificate">Training Certificate</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="notes" >Notes</label>
                                        <textarea id="notes" name="notes" cols="30" rows="10" class="form-control"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-success">
                                            <i class="fa fa-save"></i> Save Changes
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="cashier-numbers">
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">Cashier Numbers</div>
                            <div class="panel-body">
                                @if($employee->cashiernumbers->isEmpty())
                                    <p>No Numbers available for this employee.</p>
                                @endif
                                <div class="table-responsive">
                                    <table class="table table-striped table-condensed">
                                        <thead>
                                            <tr>
                                                <th>Cashier Number</th>
                                                <th>Store</th>
                                                <th>&nbsp;</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($employee->cashiernumbers as $cashiernumber)
                                                <tr>
                                                    <td>
                                                        {{$cashiernumber->cashier_number}}
                                                    </td>
                                                    <td>
                                                        {{$cashiernumber->store->store_code}} - {{ $cashiernumber->store->name }}
                                                    </td>
                                                    <td>
                                                        <form action="{{ action('CashierNumbersController@destroy', $cashiernumber->id) }}" method="POST" onsubmit="return confirm('Are you sure you want to delete this record?')">
                                                            {{ csrf_field() }}
                                                            {{ method_field('DELETE') }}
                                                            <button class="btn-xs btn-danger" type="submit" title="Delete Record">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">Add Cashier Number</div>
                            <div class="panel-body">
                                <form action="{{ action('CashierNumbersController@store', $employee->id) }}" method="post">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="Store">Store Number</label>
                                        <select name="store_id" class="form-control">
                                            @foreach($stores as $store)
                                                <option value="{{$store->id}}">{{$store->store_code}} - {{$store->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="Store">Cashier Number</label>
                                        <input type="text" name="cashier_number" id="cashier_number" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-success">
                                            <i class="fa fa-save"></i> Save Changes
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="employee-docs">
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">Employee Documents</div>
                            <div class="panel-body">
                                @if($employee->employeedocuments->isEmpty())
                                    <p>No documents for this Employee</p>
                                @endif
                                <div class="table-responsive">
                                    <table class="table table-striped table-condensed">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Notes</th>
                                            <th>File</th>
                                            <th>&nbsp;</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($employee->employeedocuments as $docs)
                                                <tr>
                                                    <td>{{$docs->file_name}}</td>
                                                    <td>@if($docs->notes)
                                                            <a data-toggle="tooltip" data-placement="top" title="{{$docs->notes}}">
                                                                <i class='fa fa-lg fa-info-circle' aria-hidden="true"></i>
                                                            </a>
                                                        @else &nbsp;
                                                        @endif
                                                    </td>
                                                    <td><a href="{{  $docs->file  }}" target="_blank" title="View Record"><i class="fa fa-lg fa-file-pdf-o"></i></a></td>

                                                    <td>
                                                        <form action="{{ action('EmployeeDocumentsController@destroy', $docs->id) }}" method="post" onsubmit="return confirm('Are you sure you want to delete this record?')">
                                                            {{ csrf_field() }}
                                                            {{ method_field('DELETE') }}
                                                            <button class="btn-xs btn-danger" type="submit" title="Delete Record">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">Add Document</div>
                            <div class="panel-body">
                                <form action="{{ action('EmployeeDocumentsController@store', $employee->id) }}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="file_name">Document Name</label>
                                        <input type="text" name="file_name" id="file_name" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="file">File</label>
                                        <input type="file" name="file" id="file" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="notes">Notes</label>
                                        <textarea id="notes" name="notes" cols="30" rows="10" class="form-control"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-success">
                                            <i class="fa fa-save"></i> Save Changes
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection