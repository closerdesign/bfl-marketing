@extends('layouts.app')

@section('content')

    <div class="container">

        <p class="text-right">
            <a href="{{ action('EmployeesController@create') }}" class="btn btn-success">
                Add A New Employee
            </a>

            <a href="{{ action('LiquorLicensesController@report') }}" class="btn btn-success">
                All Licenses
            </a>

            <a href="{{ action('LiquorLicensesController@expiring') }}" class="btn btn-warning">
                Expiring Licenses
            </a>
        </p>

        <div class="panel panel-primary">
            <div class="panel-heading">Employees List</div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-condensed" id="emp_table">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Employee ID</th>
                            <th>Store</th>
                            <th>Birthdate</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($employees as $employee)
                                <tr>
                                    <td>{{ $employee->id }}</td>
                                    <td>
                                        <a href="{{ action('EmployeesController@edit', $employee->id) }}">
                                            {{ $employee->name }}
                                        </a>
                                    </td>
                                    <td>{{ $employee->email }}</td>
                                    <td>{{ $employee->phone }}</td>
                                    <td>{{ $employee->employee_id }}</td>
                                    <td>@if( $employee->store ) [{{ $employee->store->store_code }}] {{ $employee->store->name }} @endif</td>
                                    <td>{{ date('m/d/Y', strtotime($employee->birthdate)) }}</td>
                                    <td class="text-center">
                                        <form action="{{ action('EmployeesController@destroy', $employee->id) }}" method="post" onsubmit="return confirm('Are you sure?')">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button class="btn-xs btn-danger" type="submit">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

@endsection

@section('js')

    <script>
        $(document).ready( function () {
            $('#emp_table').DataTable(
                {
                    "paging": true,
                "lengthChange":true
                }
            );

        });

    </script>

@endsection