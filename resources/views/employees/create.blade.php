@extends('layouts.app')

@section('content')

    <div class="container">
        <form action="{{ action('EmployeesController@store') }}" method="post">
            {{ csrf_field() }}
            <div class="panel panel-primary">
                <div class="panel-heading">Create a New Employee</div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="name" value="{{ old('name') }}" required >
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" name="email" value="{{ old('email') }}" >
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone</label>
                        <input type="text" class="form-control" name="phone" value="{{ old('phone') }}" required >
                    </div>
                    <div class="form-group">
                        <label for="employee_id">Employee ID</label>
                        <input type="text" class="form-control" name="employee_id" value="{{ old('employee_id') }}">
                    </div>
                    <div class="form-group">
                        <label for="birthdate">Birth Date</label>
                        <input type="date" class="form-control" name="birthdate" value="{{ old('birthdate') }}">
                    </div>
                    <div class="form-group">
                        <label for="Store">Store Number</label>
                        <select name="store_id" class="form-control">
                            @foreach($stores as $store)
                                <option value="{{$store->id}}">{{$store->store_code}} - {{$store->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="comments">Comments</label>
                        <textarea name="comments" id="comments" cols="30" rows="10" class="form-control">{{ old('comments') }}</textarea>
                    </div>
                </div>
            </div>
            <button class="btn btn-success pull-right">
                <i class="fa fa-save"></i> Save Changes
            </button>
        </form>
    </div>

    @endsection