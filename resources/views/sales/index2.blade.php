<?php
$server = "sales.cqut6atwzuvc.us-west-2.rds.amazonaws.com";

$dbName = "store_sales";

$user = "root";

$pass = "greentag4le$$";

$range_start='';

$range_end='';

//$sales_query = "SELECT store_num, dept_name, dept_sales from sales_data WHERE sales_date = '".$display."' and store_num ='".$storeNum."' ORDER BY store_num ASC";

//$cust_query = "SELECT cust_count from cust_data WHERE sales_date='".$display."' and store_num =".$storeNum;


//$lnk = mysqli_connect($server, $user, $pass, $dbName);

//$sales_data=mysqli_query($lnk, $sales_query);

function set_days($week_start){

    global $week_end, $week_total, $Sunday, $Monday, $Tuesday, $Wednesday, $Thursday, $Friday, $Saturday, $last_week, $next_week, $yesterday, $today;

    $week_end = date("Y-m-d",strtotime($week_start." + 6 day"));

    $week_total = $week_start."' AND sales_date <= '".$week_end;

    $Sunday = $week_start;

    $Monday = date("Y-m-d",strtotime($week_start." + 1 day"));

    $Tuesday = date("Y-m-d",strtotime($week_start." + 2 day"));

    $Wednesday = date("Y-m-d",strtotime($week_start." + 3 day"));

    $Thursday = date("Y-m-d",strtotime($week_start." + 4 day"));

    $Friday = date("Y-m-d",strtotime($week_start." + 5 day"));

    $Saturday = date("Y-m-d",strtotime($week_start." + 6 day"));

    $last_week = date("Y-m-d",strtotime($week_start." - 7 day"));

    $next_week = date("Y-m-d",strtotime($week_start." + 7 day"));

    $yesterday = date("Y-m-d", strtotime ("-1 day"));

    $today = date("Y-m-d");

}

function the_days(){

    $date = date("Y-m-d",strtotime(" -7 day"));
    $end_date = date("Y-m-d");

    while(strtotime($date) <= strtotime($end_date)){

     //   echo "****".$date."****".$end_date."<br>";
        $day[] = $date;
     //   echo $date."<br>";

        $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));

    }
    return $day;
}


function this_week($week_start){

    $day['Sunday']      = $week_start;
    $day['Monday']      = date("Y-m-d",strtotime($week_start." + 1 day"));
    $day['Tuesday']     = date("Y-m-d",strtotime($week_start." + 2 day"));
    $day['Wednesday']   = date("Y-m-d",strtotime($week_start." + 3 day"));
    $day['Thursday']    = date("Y-m-d",strtotime($week_start." + 4 day"));
    $day['Friday']      = date("Y-m-d",strtotime($week_start." + 5 day"));
    $day['Saturday']    = date("Y-m-d",strtotime($week_start." + 6 day"));

    return $day;
}

$stores = ['9515', '1230', '3501'];

$lnk = mysqli_connect($server, $user, $pass, $dbName);


foreach(the_days() as $d){

    foreach($stores as $s){

        $sales_query = "SELECT SUM(dept_sales) as sum_sales, DATE_FORMAT(sales_date,'%Y-%m-%d') as sales_date , store_num from sales_data WHERE sales_date = '".$d."' and store_num = '".$s."' Group By sales_date, store_num";
        $sales_data=mysqli_query($lnk, $sales_query);

        while($r = mysqli_fetch_array($sales_data)){

        //    echo $r['sales_date'];

            $ts[$r['sales_date']][$r['store_num']] =  $r['sum_sales'];
        }

        $cust_query = "SELECT cust_count from cust_data WHERE sales_date='".$d."' and store_num =".$s;

        $cust_data=mysqli_query($lnk, $cust_query);

        while($row = mysqli_fetch_array($cust_data)){

           // echo $d. " - ".$s ."-".$row['cust_count'] . "--".$ts[$d][$s]."<br>";
        }
    }

    $sales_total = "SELECT SUM(dept_sales) as sum_sales, DATE_FORMAT(sales_date,'%Y-%m-%d') as sales_date from sales_data WHERE sales_date = '".$d."'Group By sales_date";

    $sales_total_data=mysqli_query($lnk, $sales_total);

   // echo "----";
   // print_r($sales_total_data);

    while($st = mysqli_fetch_array($sales_total_data)){

        //    echo $r['sales_date'];
        $tot_sales[$st['sales_date']] =  $st['sum_sales'];

    }

}


/*
foreach(the_days() as $d){

    echo $d." ---- ";

    foreach($stores as $s){

    $sales_query = "SELECT store_num, dept_name, dept_sales, DATE_FORMAT(sales_date, '%Y-%m-%d') as sales_date from sales_data WHERE sales_date = '".$d."' and store_num ='".$s."' ORDER BY store_num ASC";

    $sales_data=mysqli_query($lnk, $sales_query);

    while($row = mysqli_fetch_array($sales_data)){

      echo $d. " - ".$s ."-".$row['dept_name'] ." => ".$row['dept_sales']."<br>";
        }
    }
}


echo "<br><br>";
*/

//    foreach (this_week('2019-06-09') as $day){
//
//        foreach($sales as $k => $v){
//
//            echo $k. "--->"; print_r($v['9515']);
//        }


//    }

        ?>

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row" >
            <canvas id="storeTotal"></canvas>
        </div>
    </div>
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.js"></script>

    <script>

        // Monthly Total

       var ctxLine = document.getElementById('storeTotal');
       ctxLine.height = 75;
        var monthlyTotal = new Chart(ctxLine, {
            type: 'line',
            data:{
                labels:[
                    @foreach(the_days() as $da)
                    '{{date('M jS', strtotime($da))}}',
                    @endforeach
                ],
                datasets:[{
                    label: 'Weekly Sales 9515',
                    backgroundColor: 'rgba(0, 255, 255, 1)',
                    fill:false,
                    data: [
                        @foreach(the_days() as $dd)
                            {{@$ts[date('Y-m-d', strtotime($dd))][9515]}},
                        @endforeach
                    ],
                },
                    {
                        label: 'Daily Total Sales',
                        backgroundColor: 'rgba(0, 0, 255, 1)',
                        fill: false,
                        data:[
                            @foreach(the_days() as $dd)
                                {{@$tot_sales[date('Y-m-d', strtotime($dd))]}},
                            @endforeach
                        ]

                    }
                ]
            },
            options: {
                responsive: true,
                animation: {
                    duration: 0
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });


    </script>
@endsection
