@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row" >
            <canvas id="companyTotal"></canvas>
        </div>
        <div class="col-md-8">
        <form action="{{ action('SalesController@sales_totals') }}" method="post"  class="form-inline">
            {{csrf_field()}}
            <div class="form-group">
                <label for="days">Days To Report</label>
                <select id="days" name="num_previous_days">
                    @for($n =1; $n<=60; $n++)
                        <option value="{{$n}}" @if($report_days == $n)selected @endif>{{$n}}</option>
                   @endfor
                </select>
            </div>
            <button class="btn btn-primary" type="submit">Submit</button>
        </form>

        </div>
        <div class="col-md-4">
            <p class="pull-right">
             <a href="{{ action('SalesController@store_totals_by_day') }}" class="btn btn-success">See Store Totals</a>
            </p>
        </div>
    </div>
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.js"></script>

    <script>
        var ctxLine = document.getElementById('companyTotal');
        ctxLine.height = 150;
        var monthlyTotal = new Chart(ctxLine, {
            type: 'line',
            data:{
                labels:[
                    @foreach($days_array as $da)
                        '{{date('M jS', strtotime($da))}}',
                    @endforeach
                ],
                datasets:[{
                    label: 'Total Company Daily Sales',
                    backgroundColor: 'rgba(0, 0, 255, 1)',
                    fill:false,
                    data: [
                        @foreach($days_array as $dd)
                            {{@$tot_sales[date('Y-m-d', strtotime($dd))]}},
                        @endforeach
                    ],
                }]
            },
            options: {
                responsive: true,
                animation: {
                    duration: 0
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: false
                        }
                    }]
                }
            }
        });


    </script>
@endsection

