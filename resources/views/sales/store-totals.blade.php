@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row" >
            <canvas id="storesTotal"></canvas>
            <div class="col-md-8">
            <form action="{{ action('SalesController@store_totals_by_day') }}" method="post" class="form-inline">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="days">Days To Report</label>
                    <select id="days" name="num_previous_days">
                        @for($n =1; $n<=60; $n++)
                            <option value="{{$n}}" @if($report_days == $n) selected @endif>{{$n}}</option>
                        @endfor
                    </select>
                </div>
                <button class="btn btn-primary" type="submit">Submit</button>
            </form>
            </div>
            <div class="col-md-4">
                <p class="pull-right">
                    <a href="{{ action('SalesController@sales_totals') }}" class="btn btn-success">See Company Totals</a>
                </p>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.js"></script>

    <script>

        var ctxLine = document.getElementById('storesTotal');
        ctxLine.height = 150;
        var monthlyTotal = new Chart(ctxLine, {
            type: 'line',
            data:{
                labels:[
                    @foreach($days as $da)
                        '{{date('M jS', strtotime($da))}}',
                    @endforeach
                ],
                datasets:[
                    @foreach($stores as $store)
                    {
                    label: 'Daily  Sales {{$store}}',
                    backgroundColor: 'rgba({{rand(0, 255)}}, {{ rand(0, 255) }}, {{ rand(0, 255) }}, 1)',
                    fill:false,
                    data: [
                        @foreach($days as $dd)
                         {{ @$ts[date('Y-m-d', strtotime($dd))][$store] }},
                        @endforeach
                    ],
                    },
                   @endforeach

                ]
            },
            options: {
                responsive: true,
                legend: {position:'left', labels:{fontSize: 16 }},
                animation: {
                    duration: 0
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: false
                        }
                    }]
                }
            }
        });


    </script>
@endsection


{{--@php print_r($ts); @endphp--}}
