@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row" >
            <ul class="nav nav-tabs" role="tablist" id="myTab">
                @foreach($s as $k => $sales)
                    <li class="nav-item" >
                        <a class="nav-link" role="tab" data-toggle="tab" href="#store_{{$k}}"> {{$k}} </a>
                    </li>
                @endforeach
            </ul>

            <div class="tab-content" id="myTabContent">
                @foreach($s as $k => $sales)
                <div class="tab-pane fade @if($k == 1006) active @endif" id="store_{{$k}}" role="tabpanel" aria-labelledby="home-tab">
                    <canvas id="deptsummary_{{$k}}" width="600" height="400"></canvas>
                </div>
                @endforeach

            </div>

        </div>
    </div>

@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.js"></script>

    <script>

    @foreach($s as $k => $sales)
        var ctx_{{$k}} = document.getElementById('deptsummary_{{$k}}').getContext('2d');
        //ctxLine.height = 150;
        var deptsummary_{{$k}} = new Chart(ctx_{{$k}}, {
            type: 'bar',
            data:{
                labels:[
                    @foreach($sales as $d => $amount)
                        '{{$d}}',
                    @endforeach
                ],
                datasets:[{
                    label: 'Dept Sales Sum of last 7 days',
                    backgroundColor: 'rgba(0, 0, 255, 1)',
                    data: [
                        @foreach($sales as $d => $amount)
                        {{$amount}},
                        @endforeach
                    ],

                }]
            },
            options: {
                responsive: true,
                animation: {
                    duration: 0
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            autoSkip: false,
                            beginAtZero: false
                        }
                    }],
                    xAxes:[{ ticks: {autoSkip: false}}]
                }
            }
        });
    @endforeach

    </script>
@endsection




{{--    @foreach($s as $k => $sales)--}}
{{--        @if($k == 1006)--}}
{{--            @foreach($sales as $d => $amount)--}}
{{--                {{$d}} -- {{$amount}}--}}
{{--            @endforeach--}}
{{--        @endif--}}
{{--    @endforeach--}}



