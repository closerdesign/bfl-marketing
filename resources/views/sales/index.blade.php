@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <p class="lead text-center">
            Sales Report<br />[{{ date('m-d-Y', strtotime($start)) }} - {{ date('m-d-Y', strtotime($end)) }}]
        </p>
        <form action="" class="form-inline">
            <div class="form-group">
                <label for="start">Start Date</label>
                <input type="date" class="form-control datepicker" name="start" required >
            </div>
            <div class="form-group">
                <label for="end">End Date</label>
                <input type="date" class="form-control datepicker" name="end" required >
            </div>
            <button class="btn btn-success">Search</button>
        </form>
        @if( $sales != null )
            <hr />
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-condensed table-hover">
                    <tr>
                        <th>&nbsp;</th>
                        @foreach($stores as $store => $store_sales)
                            <th class="text-center" colspan="2">{{ $store }}</th>
                        @endforeach
                        <th class="text-center" colspan="2">TOTAL</th>
                    </tr>
                    @foreach( $departments as $department => $department_sales )
                        <tr>
                            <th>{{ $department }}</th>
                            @foreach($stores as $store => $store_sales)
                                <td class="text-right">{{ number_format($store_sales->where('dept_name', $department)->sum('dept_sales')) }}</td>
                                <td class="text-right">@if( $store_sales->where('dept_name', $department)->sum('dept_sales') > 0 ) {{ number_format(($store_sales->where('dept_name', $department)->sum('dept_sales') / $sales->where('store_num', $store)->sum('dept_sales') * 100), 2) }}% @else 0% @endif</td>
                            @endforeach
                            <th class="text-right">{{ number_format($sales->where('dept_name', $department)->sum('dept_sales')) }}</th>
                            <th class="text-right">{{ number_format(($sales->where('dept_name', $department)->sum('dept_sales') / $sales->sum('dept_sales')) * 100, 2) }}%</th>
                        </tr>
                    @endforeach
                    <tr class="success">
                        <th>TOTAL</th>
                        @foreach($stores as $store => $store_sales)
                            <th class="text-right">{{ number_format($sales->where('store_num', $store)->sum('dept_sales')) }}</th>
                            <th class="text-right">{{ number_format(($sales->where('store_num', $store)->sum('dept_sales') / $sales->sum('dept_sales')) * 100, 2) }}%</th>
                        @endforeach
                        <th class="text-right">{{ number_format($sales->sum('dept_sales')) }}</th>
                        <th class="text-right">100%</th>
                    </tr>
                    <tr class="info">
                        <th class="text-uppercase">Customer Count</th>
                        @foreach($stores as $store => $store_sales)
                            <th class="text-right">{{ $customer_count->where('store_num', $store)->sum('cust_count') }}</th>
                            <th class="text-right">
                                {{ number_format(($customer_count->where('store_num', $store)->sum('cust_count') / $customer_count->sum('cust_count')) * 100, 2) }}%
                            </th>
                        @endforeach
                        <th colspan="2" class="text-right">
                            {{ $customer_count->sum('cust_count') }}
                        </th>
                    </tr>
                    <tr class="warning">
                        <th class="text-uppercase">Sales Per Customer</th>
                        @foreach($stores as $store => $store_sales)
                            <th colspan="2" class="text-right">
                                @if( $customer_count->where('store_num', $store)->sum('cust_count') > 0 )
                                    ${{ number_format($sales->where('store_num', $store)->sum('dept_sales') / $customer_count->where('store_num', $store)->sum('cust_count'), 2) }}
                                @endif
                            </th>
                        @endforeach
                        <th class="text-right" colspan="2">
                            @if( $customer_count->sum('cust_count') )
                                ${{ number_format($sales->sum('dept_sales') / $customer_count->sum('cust_count'), 2) }}
                            @endif
                        </th>
                    </tr>
                    <tr class="info">
                        <th class="text-uppercase">Units Sold</th>
                        @foreach($stores as $store => $store_sales)
                        <th class="text-right">
                            {{ number_format($sales->where('store_num', $store)->sum('net_sls_qty')) }}
                        </th>
                        <th class="text-right">
                            AVG {{ number_format($sales->where('store_num', $store)->sum('dept_sales') / $sales->where('store_num', $store)->sum('net_sls_qty'), 2) }}
                        </th>
                        @endforeach
                        <th class="text-right" colspan="2">
                            {{ number_format($sales->sum('net_sls_qty')) }}
                        </th>
                    </tr>
                </table>
            </div>
        @endif
    </div>

    @endsection