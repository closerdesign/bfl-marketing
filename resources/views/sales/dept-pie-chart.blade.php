@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row" >
        <div class="col-md-12 text-center"><h3>{{ date('m/d/Y', strtotime($days['start_date']))}} - {{date('m/d/Y', strtotime($days['end_date']))}}</h3></div>
        <ul class="nav nav-tabs" role="tablist" id="myTab">
            @foreach($s as $k => $sales)
                <li class="nav-item" >
                    <a class="nav-link" role="tab" data-toggle="tab" href="#store_{{$k}}"> {{$k}} </a>
                </li>
            @endforeach
        </ul>

        <div class="tab-content" id="myTabContent">
            @foreach($s as $k => $sales)
                <div class="tab-pane fade @if($k == 1006) active @endif" id="store_{{$k}}" role="tabpanel" aria-labelledby="home-tab">
                    <canvas id="deptsummary_{{$k}}" width="" height=""></canvas>
                </div>
            @endforeach

        </div>

    </div>
</div>

@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.js"></script>
    <script>
        @foreach($s as $k => $sales)
        var ctx_{{$k}} = document.getElementById('deptsummary_{{$k}}').getContext('2d');
        ctx_{{$k}}.width = 600;
        ctx_{{$k}}.height = 600;
        var deptsummary_{{$k}} = new Chart(ctx_{{$k}}, {
            type: 'pie',
            data: {
                datasets:[{
                    data:[
                        @foreach($sales as $d => $amount)
                            '{{$amount}}',
                        @endforeach
                    ],
                    backgroundColor: [
                        @foreach($sales as $d)
                            'rgb(<?php echo str_pad( mt_rand( 0, 255 ) , 2, '0', STR_PAD_LEFT); echo " ".str_pad( mt_rand( 0, 255 ) , 2, '0', STR_PAD_LEFT); echo" ". str_pad( mt_rand( 0, 255 ) , 2, '0', STR_PAD_LEFT); ?>)',
                        @endforeach
                    ],
                    label: 'Store {{$k}}'
                }],
                labels: [
                    @foreach($sales as $d => $amount)
                        '{{$d}} - {{number_format($amount, 2, '.',',')}}',
                    @endforeach
                ]
            },
            options: {
                responsive: true,
                legend: {position:'left',  }

            }
        });
    @endforeach

    </script>

@endsection