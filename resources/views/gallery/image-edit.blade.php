@extends('layouts.app')

@section('content')

    <div class="container">
        <p><button class="btn btn-primary" onclick="window.history.back()"><i class="fa fa-arrow-circle-left"></i> Go Back</button></p>
        <div class="row">
            <div class="col-md-9">
                <div class="panel panel-primary">
                    <div class="panel-heading"><i class="fa fa-edit"></i> Image Edit</div>
                    <div class="panel-body">
                        <form action="{{ action('GalleryController@imageupdate', $image->id) }}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="gallery_id">
                                    Gallery
                                </label>
                                <select name="gallery_id" class="form-control">
                                    @foreach($galleries as $gallery)
                                        <option value="{{$gallery->id}}"  @if(@$image->gallery_id == $gallery->id) selected @endif>{{$gallery->gallery_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="link_url">URL</label>
                                <input type="text" class="form-control" name="link_url" required value="{{$image->link_url}}" >
                            </div>
                            <div class="row">
                                <div class="form-group col-lg-4">
                                    <label for="start_date">Start Date</label>
                                    <input type="date" class="form-control" name="start_date" required value="{{$image->start_date}}">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label for="end_date">End Date</label>
                                    <input type="date" class="form-control" name="end_date" id="end_date" required value="{{$image->end_date}}">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label for="display_order">Display Order</label>
                                    <select class="form-control" id="display_order" name="display_order">
                                        <option value="0">Select...</option>
                                        @for($x = 1; $x<= 12; $x++)
                                            <option value="{{$x}}" @if($x == $image->display_order) selected @endif >{{$x}}</option>
                                        @endfor

                                    </select>
                                </div>
                            </div>
                            <button class="btn btn-success pull-right">
                                <i class="fa fa-save"></i> Save Changes
                            </button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <img src="{{ $image->file }}" height="250px" class="img-responsive"><br />
                @if( $image->mobile_file )
                    <img src="https://bfl-corp-sara.s3.us-west-2.amazonaws.com/gallery_images/{{ $image->mobile_file }}" height="250px" class="img-responsive">
                @endif
            </div>
        </div>

    </div>

@endsection