@extends('layouts.app')

@section('content')

    <?php $locked = [1, 2, 3, 4, 5, 6, 7, 8]; ?>
    {{--Locking the site and online shopping site carousels--}}

    <div class="container-fluid">
        <div class="panel panel-default">

            <div class="panel-heading">
                <i class="fa fa-picture-o"></i> Image Galleries
            </div>

            <div class="panel-body">

                <form action="{{ action('GalleryController@gallerystore') }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="gallery_name" required>
                    </div>
                    <button class="btn btn-success">
                        <i class="fa fa-save"></i> Save
                    </button>
                </form>

                <hr>

                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Created</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($galleries) < 1)
                            <tr>
                                <td colspan="4" class="text-center text-muted">No galleries added yet.</td>
                            </tr>
                        @endif
                        @foreach($galleries as $gallery)
                            <tr>
                                <td>{{ $gallery->id }}</td>
                                <td>
                                    <a href="{{ action('GalleryController@show', $gallery->id) }}">{{ $gallery->gallery_name }}</a>
                                </td>
                                <td>{{ $gallery->created_at->format('m/d/Y  g:i A') }}</td>
                                <td class="text-center">
                                    @if( !in_array($gallery->id, $locked) )
                                        <form onsubmit="return confirm('Are you sure? This can\'t be undone.')"
                                              action="{{ action('GalleryController@destroy', $gallery->id) }}"
                                              method="post">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button class="btn-xs btn-danger" type="submit"><i class="fa fa-trash"></i>
                                            </button>
                                        </form>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $galleries->render() }}
                </div>
            </div>
        </div>

@endsection