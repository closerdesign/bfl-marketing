@extends('layouts.app')

@section('content')

    <div class="container-fluid">

        <p class="lead">
            Edit {{ $gallery->gallery_name }}
            <a class="btn btn-primary pull-right" href="{{ action('GalleryController@index') }}"><i class="fa fa-arrow-circle-left"></i> Go Back</a>
        </p>

        <div>

            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>
                <li role="presentation"><a href="#images" aria-controls="images" role="tab" data-toggle="tab">Images</a></li>
                <li role="presentation"><a href="#add-image" aria-controls="add-image" role="tab" data-toggle="tab">Add Image</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="home">
                    <form action="{{ action('GalleryController@update', $gallery->id) }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" name="name" value="{{ $gallery->gallery_name }}" required >
                        </div>
                        <button class="btn btn-success pull-right">
                            <i class="fa fa-save"></i> Update
                        </button>
                    </form>
                    <p><b>Note:</b> If you would like an image to link to the sign up modal to subscribe, put <mark>#SignUpModal</mark> in the URL field.</p>
                </div>

                <div role="tabpanel" class="tab-pane" id="images">
                    <div class="row">
                        @foreach($images->sortBy('display_order') as $image)
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="gallery-image-container">
                                            @if( date('Y-m-d') > date('Y-m-d', strtotime($image->end_date)) )
                                                <span class="label label-danger">
                                                        <i class="fa fa-exclamation-triangle fa-2x"></i>
                                                    </span>
                                            @endif
                                            <img src="{{ $image->file }}" class="img-responsive">
                                        </div>
                                        <p class="text-center">
                                            {{ date('m/d/Y', strtotime($image->start_date)) }} - {{ date('m/d/Y', strtotime($image->end_date)) }}<br />
                                            <a href="#" data-toggle="tooltip" title="{{ $image->link_url }}">{{ Str::limit($image->link_url, 20) }}</a>
                                        </p>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="pull-left">
                                            <span class="label label-info">{{ $image->display_order }}</span>
                                        </div>
                                        <div class="text-right">
                                            <a href="/gallery/imagedelete/{{ $image->id }}" class="btn-xs btn-danger" onclick="return confirm('Are You Sure?')">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                            <a href="/gallery/imageedit/{{ $image->id }}" class="btn-xs btn-primary">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div role="tabpanel" class="tab-pane" id="add-image">
                    <form action="{{ action('GalleryController@imagepost') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class=" form-group">
                            <label for="file">Image</label>
                            <input type="file" class="form-control" name="file" id="file" required>
                        </div>
                        <div class=" form-group">
                            <label for="mobile_file">Mobile Image</label>
                            <input type="file" class="form-control" name="mobile_file" id="mobile_file">
                        </div>
                        <div class="form-group">
                            <label for="link_url">URL</label>
                            <input type="text" class="form-control" name="link_url" required >
                        </div>
                        <div class="form-group ">
                            <label for="start_date">Start Date</label>
                            <input type="date" class="form-control" name="start_date" required >
                        </div>
                        <div class="form-group ">
                            <label for="end_date">End Date</label>
                            <input type="date" class="form-control" name="end_date" id="end_date" required >
                        </div>
                        <div class="form-group ">
                            <label for="display_order">Display Order</label>
                            <input type="number" class="form-control text-center" name="display_order">
                        </div>
                        <input type="hidden" name="gallery_id" value="{{ $gallery->id }}" />
                        <button class="btn btn-success btn-block">
                            <i class="fa fa-save"></i> Save
                        </button>
                    </form>
                </div>

            </div>

        </div>
    </div>

@endsection