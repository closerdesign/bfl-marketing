<table>
    <tr>
        <th>UPC</th>
        <th>Brand Name</th>
        <th>Product Name</th>
        <th>Size</th>
        <th>Description</th>
        <th>Store Number</th>
        <th>Regular Price</th>
        <th>Sale Price</th>
    </tr>
    @foreach($products as $product)
        <tr>
            <td>{{ $product->sku }}</td>
            <td>{{ $product->brand_name }}</td>
            <td>{{ $product->product_name }}</td>
            <td>{{ $product->size }}</td>
            <td>{{ $product->description }}</td>
            <td>{{ $product->store_number }}</td>
            <td>{{ $product->regular_price }}</td>
            <td>{{ $product->sale_price }}</td>
        </tr>
    @endforeach
</table>