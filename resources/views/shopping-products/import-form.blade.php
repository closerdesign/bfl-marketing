@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">Import Shopping Products File</div>
            <div class="panel-body">
                <form action="{{ action('ShoppingProductsController@import') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="file">File</label>
                        <input type="file" class="form-control" name="file" required >
                    </div>
                    <div class="form-group">
                        <label for="store_id">Store</label>
                        <select name="store_id" id="store_id" class="form-control">
                            <option value="">Select...</option>
                            @foreach(\App\Store::orderBy('store_code')->get() as $store)
                                <option value="{{ $store->id }}">{{ $store->store_code }} - {{ $store->brands->name }} - {{ $store->name }}</option>
                                @endforeach
                        </select>
                    </div>
                    <button class="btn btn-primary" type="submit">Import</button>
                </form>
            </div>
        </div>
    </div>

    @endsection