@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Pricing
            </div>
            <div class="panel-body">
                <p class="text-right">
                    <a href="{{ action('ShoppingProductsController@excel') }}" class="btn btn-success">
                        <i class="fa fa-file-excel-o"></i> Export to Excel
                    </a>
                </p>
                <div class="alert alert-info">
                    <p>{{ $total_wo }} of {{ $total }} products found. ({{ ($total_wo / $total) * 100 }}% products missing).</p>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>UPC</th>
                            <th>Brand Name</th>
                            <th>Product Name</th>
                            <th>Size</th>
                            <th>Description</th>
                            <th>Store Number</th>
                            <th>Regular Price</th>
                            <th>Sale Price</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $product)
                        <tr>
                            <td>{{ $product->sku }}</td>
                            <td>{{ $product->brand_name }}</td>
                            <td>{{ $product->product_name }}</td>
                            <td>{{ $product->size }}</td>
                            <td>{{ $product->description }}</td>
                            <td>{{ $product->store_number }}</td>
                            <td>{{ $product->regular_price }}</td>
                            <td>{{ $product->sale_price }}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $products->render() }}
                </div>
            </div>
        </div>
    </div>

    @endsection