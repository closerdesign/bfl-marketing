@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <h2 class="text-center">
            @lang('general.welcome', ['name' => auth()->user()->name])
        </h2>

        <p class="text-center">
            <a class="badge" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">
                <i class="fa fa-sign-out"></i> @lang('nav.logout')
            </a>
        </p>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>

        <p class="text-center">
            <a href="/@lang('nav.language-code')"><i class="fa fa-globe"></i> @lang('nav.change-language')</a>
        </p>

        <hr>

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <p class="lead">The ROC - Retail Operations Communication</p>
                <ul class="list-group">
                    <li class="list-group-item">
                        <a href="{{ action('RocEntryController@index') }}">
                            @lang('roc.pending-requests')
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="{{ action('RocEntryController@create') }}">
                            @lang('roc-entry.add-new-entry')
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="{{ route('roc.index') }}">
                            @lang('roc.previous-weeks')
                        </a>
                    </li>
                </ul>

                <p class="lead">CTW (Commitment to Win)</p>
                <ul class="list-group">
                    <li class="list-group-item">
                        <a href="{{ route('ctw-evaluation.create') }}">@lang('ctw-evaluation.create')</a>
                    </li>
                    <li class="list-group-item">
                        <a href="{{ route('ctw-evaluation.index') }}">@lang('ctw-evaluation.index')</a>
                    </li>
                    <li class="list-group-item">
                        <a href="{{ route('ctw-area.index') }}">
                            @lang('ctw-area.manage-areas')
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="{{ route('ctw-evaluation.heatmap') }}">
                            @lang('ctw-evaluation.heatmap')
                        </a>
                    </li>
                </ul>

                <p class="lead">@lang('ads.title')</p>
                <ul class="list-group">
                    <li class="list-group-item">
                        <a href="{{ action('AdsController@index') }}">@lang('general.ads-manager')</a>
                    </li>
                </ul>

                <p class="lead">@lang('products.title')</p>
                <ul class="list-group">
                    <li class="list-group-item">
                        <a href="{{ route('products.index') }}">
                            @lang('general.product-manager')
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="{{ action('CategoriesController@index') }}">
                            @lang('general.category-manager')
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="{{ action('DepartmentsController@index') }}">
                            @lang('general.department-manager')
                        </a>
                    </li>
                </ul>

                <p class="lead">@lang('general.online-shopping')</p>
                <ul class="list-group">
                    <li class="list-group-item">
                        <a href="{{ action('PricingFilesController@index') }}">
                            @lang('general.pricing-files-manager')
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="{{ action('SearchController@index') }}">
                            @lang('general.online-shopping-search-stats')
                        </a>
                    </li>
                </ul>

                <p class="lead">@lang('general.e-commerce')</p>
                <ul class="list-group">
                    <li class="list-group-item">
                        <a href="{{ action('CampaignsController@index') }}">
                            @lang('general.campaigns-manager')
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="{{ action('CampaignCategoriesController@index') }}">
                            @lang('nav.categories')
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="{{ action('CampaignItemsController@index') }}">
                            @lang('nav.items')
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="{{ action('CampaignFaqsController@index') }}">
                            @lang('campaign-faqs.title')
                        </a>
                    </li>
                </ul>

                <p class="lead">@lang('general.inventory')</p>
                <ul class="list-group">
                    <li class="list-group-item">
                        <a href="{{ action('InventoryController@index') }}">@lang('general.inventory-manager')</a>
                    </li>
                </ul>

                <p class="lead">@lang('general.content')</p>
                <ul class="list-group">
                    <li class="list-group-item"><a href="{{ action('ArticlesController@index') }}">@lang('nav.articles')</a></li>
                    <li class="list-group-item"><a href="{{ action('RecipesController@index') }}">@lang('nav.recipes')</a></li>
                    <li class="list-group-item"><a href="{{ action('EventsController@index') }}">@lang('nav.events')</a></li>
                    <li class="list-group-item"><a href="{{ action('GalleryController@index') }}">@lang('nav.image-gallery' )</a></li>
                    <li class="list-group-item"><a href="{{ action('TicketController@create') }}">@lang('nav.tickets')</a></li>
                </ul>

                <p class="lead">@lang('general.reporting')</p>
                <ul class="list-group">
                    <li class="list-group-item"><a href="{{ action('SalesController@index') }}">@lang('nav.sales-report')</a></li>
                    <li class="list-group-item"><a href="{{ action('SurveysController@list') }}">@lang('nav.surveys')</a></li>
                    <li class="list-group-item"><a href="{{ action('DonationRequestsController@index') }}">@lang('nav.donation-req')</a></li>
                    <li class="list-group-item"><a href="{{ action('ProductRequestsController@index') }}">@lang('nav.product-req')</a></li>
                    <li class="list-group-item"><a href="{{ action('HuddlesController@index') }}">@lang('nav.huddles')</a></li>
                    <li class="list-group-item"><a href="https://datastudio.google.com/s/kgGpJAxnp8Q" target="_blank">@lang('nav.online-shopping')</a></li>
                    <li class="list-group-item"><a href="{{ action('InventoryController@list') }}">@lang('general.inventory')</a></li>
                    <li class="list-group-item"><a href="{{ action('PromoMovementController@index') }}">Promo Movement</a></li>
                    <li class="list-group-item"><a href="{{ action('RsaController@redemption_report') }}">Loyalty Redemption Report</a></li>
                </ul>

                <p class="lead">@lang('general.resources')</p>
                <ul class="list-group">
                    <li class="list-group-item"><a href="{{ action('DocumentsController@index') }}">@lang('nav.documents')</a></li>
                    <li class="list-group-item"><a href="{{ action('TrainingController@index') }}">@lang('nav.training')</a></li>
                </ul>

                <p class="lead">@lang('general.general-requests')</p>
                <ul class="list-group">
                    <li class="list-group-item"><a href="{{ action('StoreLevelController@bg_orientation_request') }}">@lang('nav.orientation')</a></li>
                </ul>

                <p class="lead">@lang('general.general-settings')</p>
                <ul class="list-group">
                    <li class="list-group-item"><a href="{{ action('BrandsController@index') }}">@lang('nav.brands')</a></li>
                    <li class="list-group-item"><a href="{{ action('StoresController@index') }}">@lang('nav.stores')</a></li>
{{--                    <li class="list-group-item"><a href="{{ action('EmployeesController@index') }}">@lang('nav.employees')</a></li>--}}
                </ul>

            </div>
        </div>

    </div>

@endsection
