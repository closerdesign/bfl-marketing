@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">Wedding Lead Details: {{ $lead->name }}</div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">Name</div>
                            <div class="panel-body">
                                {{ $lead->name }}
                            </div>
                            <div class="panel-heading">Phone</div>
                            <div class="panel-body">
                                {{ $lead->phone }}
                            </div>
                            <div class="panel-heading">Email</div>
                            <div class="panel-body">
                                {{ $lead->email }}
                            </div>
                            <div class="panel-heading">Interest</div>
                            <div class="panel-body">
                                {{ $lead->interest }}
                            </div>
                            <div class="panel-heading">Location</div>
                            <div class="panel-body">
                                {{ $lead->location }}
                            </div>
                            <div class="panel-heading">Wedding Date</div>
                            <div class="panel-body">
                                {{ $lead->wedding_date }}
                            </div>
                            <div class="panel-heading">Venue</div>
                            <div class="panel-body">
                                {{ $lead->venue }}
                            </div>
                            <div class="panel-heading">Budget</div>
                            <div class="panel-body">
                                {{ $lead->budget }}
                            </div>
                            <div class="panel-heading">Created at</div>
                            <div class="panel-body">
                                {{ $lead->created_at }}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-12">
                                @if( count(\App\Wedding::where('lead_id', $lead->id)->get()) == 0 )
                                <div class="alert alert-info">
                                    <p>Current Status: UNATTENDED</p>
                                </div>
                                <form action="{{ action('WeddingsController@status', $lead->id) }}" method="post">
                                @else
                                <div class="alert alert-info">
                                    <p>Current Status: {{ $wedding->status }}</p>
                                </div>
                                <form action="{{ action('WeddingsController@update', $lead->id) }}" method="post">
                                    {{ method_field('PATCH') }}
                                @endif
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="status">Update Status</label>
                                        <select name="status" id="status" class="form-control">
                                            <option value="">Select...</option>
                                            <option value="ATTENDED">ATTENDED</option>
                                            <option value="CLOSED">CLOSED</option>
                                            <option value="DISCARDED">DISCARDED</option>
                                        </select>
                                    </div>
                                    <button class="btn btn-success pull-right">Update</button>
                                </form>
                            </div>
                        </div>
                        @if( count(\App\Wedding::where('lead_id', $lead->id)->get()) == 1 )
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <form action="{{ action('WeddingsController@comment', $wedding->id) }}" method="post" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <label for="comment">Add comment</label>
                                            <textarea name="comment" id="comment"
                                                      class="form-control summernote"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="file">File</label>
                                            <input type="file" class="form-control" name="file">
                                        </div>
                                        <button class="btn btn-success">Add comment</button>
                                    </form>
                                </div>
                            </div>

                            <hr>

                            <div class="row">
                                <div class="col-md-12">
                                    @foreach($wedding->comments as $comment)
                                    <div class="panel panel-default">
                                        <div class="panel-heading">By {{ $comment->user->name }} - {{ $comment->created_at }}</div>
                                        <div class="panel-body">
                                            {!! $comment->comment !!}
                                            @if( $comment->file != "" )
                                            <hr>
                                            <p><a href="{{ Storage::url('wedding-comment/' . $comment->file) }}" target="_blank"><i class="fa fa-download"></i> Download File</a></p>
                                            @endif
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>

                            @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection