@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">Weddings - Leads Report</div>
            <div class="panel-body">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <?php $statuses = ['UNATTENDED', 'ATTENDED', 'CLOSED', 'DISCARDED']; ?>
                    <?php $classes = ['UNATTENDED' => 'danger', 'ATTENDED' => 'warning', 'CLOSED' => 'success', 'DISCARDED' => 'info'] ?>
                    @foreach($statuses as $status)
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $status }}" aria-expanded="true" aria-controls="collapse{{ $status }}">
                                    {{ $status }}
                                </a>
                            </h4>
                        </div>
                        <div id="collapse{{ $status }}" class="panel-collapse collapse @if( $status == 'UNATTENDED' ) in @endif" role="tabpanel" aria-labelledby="heading{{ $status }}">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped datatable">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Email</th>
                                            <th>Interest</th>
                                            <th>Location</th>
                                            <th>Wedding Date</th>
                                            <th>Venue</th>
                                            <th>Budget</th>
                                            <th>Request Date</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($leads as $lead)
                                            @if($lead->status == $status)
                                            <tr>
                                                <th>
                                                    <a href="{{ action('WeddingsController@show', $lead->id) }}">
                                                        {{ $lead->name }}
                                                    </a>
                                                </th>
                                                <td>{{ $lead->phone }}</td>
                                                <td>{{ $lead->email }}</td>
                                                <td>{{ $lead->interest }}</td>
                                                <td>{{ $lead->location }}</td>
                                                <td>{{ $lead->wedding_date }}</td>
                                                <td>{{ $lead->venue }}</td>
                                                <td>{{ $lead->budget }}</td>
                                                <td>{{ $lead->created_at }}</td>
                                            </tr>
                                            @endif
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    @endsection