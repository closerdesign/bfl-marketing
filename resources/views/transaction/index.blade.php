@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">Transactions History</div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-condensed">
                        <thead>
                        <tr>
                            <th>Item</th>
                            <th>Store</th>
                            <th>Qty</th>
                            <th>Type</th>
                            <th>Approved By</th>
                            <td>Created At</td>
                        </tr>
                        @foreach($transactions as $transaction)
                        <tr>
                            <td><b>{{ $transaction->item->name }}</b></td>
                            <td class="text-center">{{ $transaction->store->store_code }}</td>
                            <td class="text-center">{{ $transaction->qty }}</td>
                            <td class="text-center">{{ $transaction->type }}</td>
                            <td>{{ $transaction->approved_by }}</td>
                            <td class="text-center">{{ $transaction->created_at }}</td>
                        </tr>
                        @endforeach
                        </thead>
                    </table>
                    {{ $transactions->render() }}
                </div>
            </div>
        </div>
    </div>

    @endsection