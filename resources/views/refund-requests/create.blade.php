@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading"><i class="fa fa-money"></i> Refund Request</div>
            <div class="panel-body">
                <form action="{{ action('RefundRequestsController@store') }}" method="post" >
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}" required >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">Email Address</label>
                                <input type="text" class="form-control" name="email" value="{{ old('email') }}" required >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="phone">Phone</label>
                                <input type="text" class="form-control" name="phone" value="{{ old('phone') }}" required >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="store">Store</label>
                                <select class="form-control" id="store" name="store">
                                    <option>Select...</option>
                                    @foreach(\App\Store::orderBy('store_code')->get() as $store)
                                        <option @if( old('store') == $store->store_code ) selected @endif value="{{ $store->store_code }}">{{ $store->store_code }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="area">Area / Department</label>
                                <input type="text" class="form-control" name="area" value="{{ old('area') }}" required >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="amount">Amount</label>
                                <input type="text" class="form-control" name="amount" value="{{ old('amount') }}" required >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="purchase_date">Date of Purchase</label>
                            <input type="date" class="form-control" name="purchase_date" required />
                        </div>
                    </div><br />
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="comments">Additional Comments</label>
                                <textarea class="form-control" rows="3" id="comments" name="comments">{{ old('comments') }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button class="btn btn-success btn-block">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection