@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="panel panel-default">
            <div class="panel-heading">Editing: {{ $article->title }}</div>
            <div class="panel-body">
                <form action="{{ action('ArticlesController@update', $article->id) }}" method="post" enctype="multipart/form-data" >
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}
                    <div class="row">
                        @if($article->cover_img != "")
                            <div class="col-md-12">
                                <img width="200" src="{{ $article->cover_img }}" alt="{{ $article->title }}" class="img-responsive pull-right">
                            </div>
                        @endif
                        <div class="col-md-12 form-group">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" name="title" value="{{ $article->title }}" required >
                        </div>
                        <div class="col-md-12 form-group">
                            <label for="intro_text">Intro Text</label>
                            <textarea name="intro_text" id="intro_text" cols="30" rows="10" class="form-control">{{ $article->intro_text }}</textarea>
                        </div>
                        <div class="col-md-12 form-group">
                            <label for="content">Content</label>
                            <textarea name="content" id="content" cols="30" rows="5" class="form-control summernote">{{ $article->content }}</textarea>
                        </div>
                        <div class="col-md-12 form-group">
                            <label for="categories">Categories</label>
                            <select name="categories[]" id="categories" class="form-control multiple-select" multiple="multiple" >
                                @foreach( \App\ArticleCategory::all() as $category )
                                    <option @if( in_array($category->id, $article->categories()->select('article_category_id')->pluck('article_category_id')->toArray()) ) ) selected @endif value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="cover_img">Cover Image</label>
                            <input type="file" class="form-control" name="cover_img" >
                            <label>Image size: 500 x 500 px.</label>
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="status">Status</label>
                            <select name="status" id="status" class="form-control">
                                <option value="">Select...</option>
                                <option @if($article->status == 'Published') selected @endif value="1">Published</option>
                                <option @if($article->status == 'Unpublished') selected @endif value="0">Unpublished</option>
                            </select>
                        </div>
                        <div class="col-md-12 form-group">
                            <button class="btn btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save changes</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @endsection