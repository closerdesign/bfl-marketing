@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Create a new article</div>
                    <div class="panel-body">
                        <form action="{{ action('ArticlesController@store') }}" method="post" enctype="multipart/form-data" >
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <label for="title">Title</label>
                                    <input type="text" class="form-control" name="title" value="{{ old('title') }}" required >
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="intro_text">Intro Text
                                        <a href="#" data-toggle="tooltip" data-placement="right" title="This is a short overview of the topics you will be covering in the article.">
                                            <i class="fa fa-info-circle"></i>
                                        </a>
                                    </label>
                                    <textarea name="intro_text" id="intro_text" cols="30" rows="10" class="form-control">{{ old('intro_text') }}</textarea>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="content">Content
                                        <a href="#" data-toggle="tooltip" data-placement="right" title="This is the body text of the article">
                                            <i class="fa fa-info-circle"></i>
                                        </a>
                                    </label>
                                    <textarea name="content" id="content" cols="30" rows="5" class="form-control summernote">{{ old('content') }}</textarea>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="categories">Categories</label>
                                    <select name="categories[]" id="categories" class="form-control multiple-select" multiple="multiple" >
                                        @foreach( \App\ArticleCategory::all() as $category )
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-6 form-group">
                                    <label for="cover_img">Cover Image</label>
                                    <input type="file" class="form-control" name="cover_img" >
                                    <label>Image size: 500 x 500 px.</label>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label for="status">Status
                                        <a href="#" data-toggle="tooltip" data-placement="right" title="This will determine if the article is published on the website or not.">
                                            <i class="fa fa-info-circle"></i>
                                        </a>
                                    </label>
                                    <select name="status" id="status" class="form-control">
                                        <option value="">Select...</option>
                                        <option @if(old('status') == 1) selected @endif value="1">Published</option>
                                        <option @if(old('status') == 0) selected @endif value="0">Unpublished</option>
                                    </select>
                                </div>
                                <div class="col-md-12 form-group">
                                    <button class="btn btn-success" type="submit"><i class="fa fa-floppy-o"></i> Save Article</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection