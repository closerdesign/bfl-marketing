@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-9">

                <div class="text-right form-group">
                    <a href="{{ action('ArticlesController@create') }}" class="btn btn-success"><i class="fa fa-plus-circle"></i> Create a new article</a>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">Articles List</div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>Title</th>
                                        <th>Status</th>
                                        <th>Created</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($articles as $article)
                                        <tr>
                                            <td>
                                                <img src="{{ $article->cover_img }}" width="50" alt="{{ $article->title }}">
                                            </td>
                                            <th>
                                                <a href="{{ action('ArticlesController@edit', $article->id) }}">
                                                    {{ $article->title }}
                                                </a>
                                            </th>
                                            <td>{{ $article->status }}</td>
                                            <td>{{ $article->created_at }}</td>
                                            <td class="text-center">
                                                <form action="{{ action('ArticlesController@destroy', $article->id) }}" method="post" onsubmit="return confirm('Are you sure? This can\'t be undone.')">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                    <button class="btn-xs btn-danger">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $articles->render() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-heading">Categories</div>
                    <div class="panel-body">
                        <form action="{{ action('ArticlesCategoriesController@store') }}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" required >
                            </div>
                            <button class="btn btn-success btn-block">
                                Save
                            </button>
                        </form>
                        <hr>
                        @foreach( \App\ArticleCategory::orderBy('name')->get() as $category )
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <form action="{{ action('ArticlesCategoriesController@update', $category->id) }}" method="post">
                                        @csrf
                                        {{ method_field('PATCH') }}
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="name" value="{{ $category->name }}" required >
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-success btn-block">
                                                @lang('general.update')
                                            </button>
                                        </div>
                                    </form>
                                    <div class="text-right">
                                        <form action="{{ route('article-category.destroy', $category->id) }}" method="post" onsubmit="return confirm('@lang('general.are-you-sure')')">
                                            @csrf
                                            {{ method_field('DELETE') }}
                                            <button class="btn btn-danger">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection