<html>
<head>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<style>
    .pg2sm {
        height:170px;
    }
    .med{
        height:185px;
        width: 471px;
        font-family: Helvetica, Arial, sans-serif;
    }

    .sm{
        width:314px;
        height: 180px;
    }

    .price_left{
        margin-top: 320px;
        font-size: 66px;
        font-weight: bolder;
        color: #000000;
        z-index: 20;
        width: 60px;
        position: absolute;
    }

    .price_left_upper_box{
        margin-top: 70px;
        font-size: 30px;
        font-weight: bolder;
        color: #000000;
        z-index: 20;
        width: 160px;
        position: absolute;
    }

    .description_large{
        margin-top: 110px;
        font-size: 20px;
        font-weight: bolder;
        width: 250px;
        text-align: left;
        float:left;
        z-index: 2;
        line-height: 80%;
        position: absolute;

    }

    .description_3_by{
        margin-top: 90px;
        font-size: 24px;
        font-weight: bold;
        text-align: left;
        position: absolute;
        line-height: 80%;

    }

    .price_3_by{
        margin-top: 70px;
        font-size: 34px;
        font-weight: bolder;
        color: #000000;
        z-index: 20;
        width: 160px;
        position: absolute;

    }

    .price_four{
        z-index: 190;
        margin-top: 20px;
        font-size: 30px;
        font-weight: bolder;
        position: absolute;

    }
    .picture_four {
        z-index: 10;
        float:right;
    }
    .desc_four{
        margin-top: 60px;
        z-index: 200;
        position: absolute;
        font-size: 18px;
        font-weight: bold;
        color: #000;
        float:left;
        line-height: 80%;
        width:100px;

    }

    .product-img {
        background-size: contain;
        background-repeat: no-repeat;
        background-position: center 80%;
    }

    .img-sm{
        height:190px;
        width:190px;
        float: right;
        z-index: 100;
    }

    .img-med{
        height:170px;
        width:170px;
        float: right;
        z-index: 100;
    }

    .img-xl{
        height: 340px;
        width: 340px;
        margin-left: 180px;

    }

    .img-lrg{
        height:190px;
        width: 190px;
    }

    .img-long{
        height: 300px;
        width :190px;
    }

    .lg-blk{
        height: 400px;
    }
    .med-blk{
        height: 200px;
    }
    .med-2-blk{
        height: 190px;
    }
    .med-sup{
        font-size: 34px;
    }
    .small-sup{
        font-size: 24px;
    }

    @media print {
        body{
            max-width: 2200px;
        }
        .footer {
            page-break-after: always;
        }
    }
    sup{
        font-size: 40px;

    }

    .size-lrg{
        font-size:14px;
        color:#000000;
        font-weight: bold;
    }

    .size-small{
        font-size:14px;

    }

    div{margin:0; padding:0;}

    .stroke{
        background: rgba(250, 250, 250 , 0.7);
        padding: 8px;
        border-radius: 10px;


    }


</style>

</head>
<body>
<div class="container" style="margin-right: auto; margin-left: auto; max-width: 2000px; padding-left: 75px; padding-right: 75px;">
    <div class="row" id="header_row">

        <div class="col-12" align="center">
            <img src="https://bfl-corp-sara.s3.us-west-2.amazonaws.com/liquor-licenses/5bbe5fa44ef5f.png" height="120">
        </div>
    </div>
    <div class="row">
        <div class="col-6 lg-blk" style="border-right: 1px #000000 solid;">
            <div style="margin-top: 250px;
            font-size: 80px;
            font-weight: bolder;
            color: #000000;
            z-index: 20;
            width: 160px;
            position: absolute;" class="stroke">
                {{!empty($ad->specials->where('position_number', '1')->first()->price) ? $ad->specials->where('position_number', '1')->first()->price : ""}}
            </div>
            <div style="float: left;" class="description_large @if(@$ad->specials->where('position_number', '1')->first()->ad_stroke ==1)  stroke   @endif">
                {{ !empty($ad->specials->where('position_number', '1')->first()->item_description) ?
                    $ad->specials->where('position_number', '1')->first()->item_description :
                    "NO DESCRIPTION FOR THIS ITEM" }}
                <div class="size-lrg">
                    {{!empty($ad->specials->where('position_number', '1')->first()->size) ?
                    $ad->specials->where('position_number', '1')->first()->size :
                    "" }}
                </div>
            </div>
            <div class="product-img img-xl" style="float:right; background-image:
            <?php echo  !empty($ad->specials->where('position_number', '1')->first()->image) ?
                "url('".$ad->specials->where('position_number', '1')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;">
            </div>


        </div>

        <div class="col-6 lg-blk" >
            <div style="height:220px;" >
                <div style="float: left;" class="description_large @if(@$ad->specials->where('position_number', '2')->first()->ad_stroke ==1)  stroke   @endif">
                    {{ !empty($ad->specials->where('position_number', '2')->first()->item_description) ?
                  $ad->specials->where('position_number', '2')->first()->item_description :
                  "NO DESCRIPTION FOR THIS ITEM" }}
                    <div class="size-lrg">
                        {{!empty($ad->specials->where('position_number', '2')->first()->size) ?
                        $ad->specials->where('position_number', '2')->first()->size :
                        "" }}
                    </div>
                </div>

                <div class="price_left_upper_box">
                    {{!empty($ad->specials->where('position_number', '2')->first()->price) ? $ad->specials->where('position_number', '2')->first()->price : ""}}
                </div>

                 <div class="product-img img-lrg" style="float: right; z-index: 100; background-image:
                 <?php echo  !empty($ad->specials->where('position_number', '2')->first()->image) ?
                     "url('".$ad->specials->where('position_number', '2')->first()->image."')" :
                     "url('/img/pending-image.jpg');" ?> ;
                     ">
                </div>
            </div>
            <div class="med-blk" >
                <div class="description_large" @if(@$ad->specials->where('position_number', '3')->first()->ad_stroke ==1)  stroke   @endif style="float: left;">
                    {{ !empty($ad->specials->where('position_number', '3')->first()->item_description) ?
                  $ad->specials->where('position_number', '3')->first()->item_description :
                  "NO DESCRIPTION FOR THIS ITEM" }}
                    <div class="size-lrg">
                        {{!empty($ad->specials->where('position_number', '3')->first()->size) ?
                  trim($ad->specials->where('position_number', '3 ')->first()->size) :
                  "" }}
                    </div>
                </div>
                <div class="price_left_upper_box">
                    {{!empty($ad->specials->where('position_number', '3')->first()->price) ? $ad->specials->where('position_number', '3')->first()->price : ""}}
                </div>


                <div class="product-img img-lrg" style="float: right; z-index: 100; background-image:
                <?php echo  !empty($ad->specials->where('position_number', '3')->first()->image) ?
                    "url('".$ad->specials->where('position_number', '3')->first()->image."')" :
                    "url('/img/pending-image.jpg');" ?> ;
                        ">
                </div>
            </div>
        </div>
    </div>

<!-- ROW 2-->
    <div class="row">
        <div class="col-6 med-blk" style="padding: 4px; border-right: 1px #000000 solid;">
            <div style="float: left;" class="description_large @if(@$ad->specials->where('position_number', '4')->first()->ad_stroke ==1)  stroke   @endif">
                {{ !empty($ad->specials->where('position_number', '4')->first()->item_description) ?
                  $ad->specials->where('position_number', '4')->first()->item_description :
                  "NO DESCRIPTION FOR THIS ITEM" }}
                <div class="size-lrg">
                    {{!empty($ad->specials->where('position_number', '4')->first()->size) ?
              $ad->specials->where('position_number', '4')->first()->size :
              "" }}
                </div>
            </div>
            <div class="price_left_upper_box">
                {{!empty($ad->specials->where('position_number', '4')->first()->price) ? $ad->specials->where('position_number', '4')->first()->price : ""}}
            </div>

            <div class="product-img img-lrg" style="float: right; z-index: 100; background-image:
            <?php echo  !empty($ad->specials->where('position_number', '4')->first()->image) ?
                "url('".$ad->specials->where('position_number', '4')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;
                    ">
            </div>
        </div>
        <div class="col-6" style="padding: 4px;">
            <div style="float: left;" class="description_large @if(@$ad->specials->where('position_number', '5')->first()->ad_stroke ==1)  stroke   @endif" >
                {{ !empty($ad->specials->where('position_number', '5')->first()->item_description) ?
                   $ad->specials->where('position_number', '5')->first()->item_description :
                   "NO DESCRIPTION FOR THIS ITEM 5" }}
                <div class="size-lrg">
                    {{!empty($ad->specials->where('position_number', '5')->first()->size) ?
                    $ad->specials->where('position_number', '5')->first()->size :
                    "" }}
                </div>
            </div>
            <div class="price_left_upper_box">
                {{!empty($ad->specials->where('position_number', '5')->first()->price) ? $ad->specials->where('position_number', '5')->first()->price : ""}}
            </div>

            <div class="product-img img-lrg" style="float: right; z-index: 100; background-image:
            <?php echo  !empty($ad->specials->where('position_number', '5')->first()->image) ?
                "url('".$ad->specials->where('position_number', '5')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;
                    ">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-6 med-blk" style="padding: 4px; border-right: 1px #000000 solid; ">
            <div style="float: left;" class="description_large @if(@$ad->specials->where('position_number', '6')->first()->ad_stroke ==1)  stroke   @endif" >
                {{ !empty($ad->specials->where('position_number', '6')->first()->item_description) ?
                  $ad->specials->where('position_number', '6')->first()->item_description :
                  "NO DESCRIPTION FOR THIS ITEM 6" }}
                <div class="size-lrg">
                    {{!empty($ad->specials->where('position_number', '6')->first()->size) ?
                    $ad->specials->where('position_number', '6')->first()->size :
                    "" }}
                </div>
            </div>
            <div class="price_left_upper_box">
                {{!empty($ad->specials->where('position_number', '6')->first()->price) ? $ad->specials->where('position_number', '6')->first()->price : ""}}
            </div>


            <div class="product-img img-lrg" style="float: right; z-index: 100; background-image:
            <?php echo  !empty($ad->specials->where('position_number', '6')->first()->image) ?
                "url('".$ad->specials->where('position_number', '6')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;
                    ">
            </div>
        </div>
        <div class="col-6 med-blk" style="padding: 4px;">
            <div style="float: left;" class="description_large @if(@$ad->specials->where('position_number', '7')->first()->ad_stroke ==1)  stroke   @endif">
                {{ !empty($ad->specials->where('position_number', '7')->first()->item_description) ?
                  $ad->specials->where('position_number', '7')->first()->item_description :
                  "NO DESCRIPTION FOR THIS ITEM 7" }}
                <div class="size-lrg">
                    {{!empty($ad->specials->where('position_number', '7')->first()->size) ?
                    $ad->specials->where('position_number', '7')->first()->size :
                    "" }}
                </div>
            </div>
            <div class="price_left_upper_box">
                {{!empty($ad->specials->where('position_number', '7')->first()->price) ? $ad->specials->where('position_number', '7')->first()->price : ""}}
            </div>

            <div class="product-img img-lrg" style="float: right; z-index: 100; background-image:
            <?php echo  !empty($ad->specials->where('position_number', '7')->first()->image) ?
                "url('".$ad->specials->where('position_number', '7')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;
                    ">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-6 med-blk" style="padding: 4px; border-right: 1px #000000 solid; border-bottom: 1px #000000 solid;">
            <div style="float: left;" class="description_large @if(@$ad->specials->where('position_number', '8')->first()->ad_stroke ==1)  stroke   @endif">
                {{ !empty($ad->specials->where('position_number', '8')->first()->item_description) ?
                  $ad->specials->where('position_number', '8')->first()->item_description :
                  "NO DESCRIPTION FOR THIS ITEM 8" }}
                <div class="size-lrg">
                    {{!empty($ad->specials->where('position_number', '8')->first()->size) ?
                    $ad->specials->where('position_number', '8')->first()->size :
                    "" }}
                </div>
            </div>
            <div class="price_left_upper_box">
                {{!empty($ad->specials->where('position_number', '8')->first()->price) ? $ad->specials->where('position_number', '8')->first()->price : ""}}
            </div>


            <div class="product-img img-lrg" style="float: right; z-index: 100; background-image:
            <?php echo  !empty($ad->specials->where('position_number', '8')->first()->image) ?
                "url('".$ad->specials->where('position_number', '8')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;
                    ">
            </div>
        </div>
        <div class="col-6 med-blk" style="padding: 4px;  border-bottom: 1px #000000 solid;">
            <div style="float: left;" class="description_large @if(@$ad->specials->where('position_number', '9')->first()->ad_stroke ==1)  stroke   @endif">
                {{ !empty($ad->specials->where('position_number', '9')->first()->item_description) ?
                  $ad->specials->where('position_number', '9')->first()->item_description :
                  "NO DESCRIPTION FOR THIS ITEM 9" }}
                <div class="size-lrg">
                    {{!empty($ad->specials->where('position_number', '9')->first()->size) ?
                    $ad->specials->where('position_number', '9')->first()->size :
                    "" }}
                </div>
            </div>
            <div class="price_left_upper_box">
                {{!empty($ad->specials->where('position_number', '9')->first()->price) ? $ad->specials->where('position_number', '9')->first()->price : ""}}
            </div>

            <div class="product-img img-lrg" style="float: right; z-index: 100; background-image:
            <?php echo  !empty($ad->specials->where('position_number', '9')->first()->image) ?
                "url('".$ad->specials->where('position_number', '9')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;
                    ">
            </div>
        </div>
    </div>
    <!-- 3 cols 2 rows -->
    <div class="row">
        <div class="col-4 med-blk" style="padding: 4px;">
            <div style="float: left;" class="description_large @if(@$ad->specials->where('position_number', '10')->first()->ad_stroke ==1)  stroke   @endif">
                {{ !empty($ad->specials->where('position_number', '10')->first()->item_description) ?
                  $ad->specials->where('position_number', '10')->first()->item_description :
                  "NO DESCRIPTION FOR THIS ITEM 10" }}
                <div class="size-lrg">
                    {{!empty($ad->specials->where('position_number', '10')->first()->size) ?
                    $ad->specials->where('position_number', '10')->first()->size :
                    "" }}
                </div>
            </div>
            <div class="price_3_by">
                {{!empty($ad->specials->where('position_number', '10')->first()->price) ? $ad->specials->where('position_number', '10')->first()->price : ""}}
            </div>

            <div class="product-img img-med" style="background-image:
            <?php echo  !empty($ad->specials->where('position_number', '10')->first()->image) ?
                "url('".$ad->specials->where('position_number', '10')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;">
            </div>
        </div>
        <div class="col-4 med-blk" style="padding: 4px; ">
            <div style="float: left" class="description_large @if(@$ad->specials->where('position_number', '11')->first()->ad_stroke ==1)  stroke   @endif">

                {{ !empty($ad->specials->where('position_number', '11')->first()->item_description) ?
                  $ad->specials->where('position_number', '11')->first()->item_description :
                  "NO DESCRIPTION FOR THIS ITEM 11" }}
                <div class="size-lrg">
                    {{!empty($ad->specials->where('position_number', '11')->first()->size) ?
                    $ad->specials->where('position_number', '11')->first()->size :
                    "" }}
                </div>
            </div>
            <div class="price_3_by">
                {{!empty($ad->specials->where('position_number', '11')->first()->price) ? $ad->specials->where('position_number', '11')->first()->price : ""}}
            </div>

            <div class="product-img img-med" style="background-image:
            <?php echo  !empty($ad->specials->where('position_number', '11')->first()->image) ?
                "url('".$ad->specials->where('position_number', '11')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;">
            </div>
        </div>
        <div class="col-4 med-blk" style="padding: 4px; ">
            <div style="float: left;" class="description_large @if(@$ad->specials->where('position_number', '12')->first()->ad_stroke ==1)  stroke   @endif">
                {{ !empty($ad->specials->where('position_number', '12')->first()->item_description) ?
                 $ad->specials->where('position_number', '12')->first()->item_description :
                 "NO DESCRIPTION FOR THIS ITEM 12" }}
                <div class="size-lrg">
                    {{!empty($ad->specials->where('position_number', '12')->first()->size) ?
                    $ad->specials->where('position_number', '12')->first()->size :
                    "" }}
                </div>
            </div>
            <div class="price_3_by">
                {{!empty($ad->specials->where('position_number', '12')->first()->price) ? $ad->specials->where('position_number', '12')->first()->price : ""}}
            </div>


            <div class="product-img img-med" style="background-image:
            <?php echo  !empty($ad->specials->where('position_number', '12')->first()->image) ?
                "url('".$ad->specials->where('position_number', '12')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-4 med-2-blk" style="padding: 4px;">
            <div style="float: left;" class="description_large @if(@$ad->specials->where('position_number', '13')->first()->ad_stroke ==1)  stroke   @endif">
                {{ !empty($ad->specials->where('position_number', '13')->first()->item_description) ?
                 $ad->specials->where('position_number', '13')->first()->item_description :
                 "NO DESCRIPTION FOR THIS ITEM 13" }}
                <div class="size-lrg">
                    {{!empty($ad->specials->where('position_number', '13')->first()->size) ?
                    $ad->specials->where('position_number', '13')->first()->size :
                    "" }}
                </div>
            </div>
            <div class="price_3_by">
                {{!empty($ad->specials->where('position_number', '13')->first()->price) ? $ad->specials->where('position_number', '13')->first()->price : ""}}
            </div>

            <div class="product-img img-med" style="background-image:
            <?php echo  !empty($ad->specials->where('position_number', '13')->first()->image) ?
                "url('".$ad->specials->where('position_number', '13')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;">
            </div>
        </div>
        <div class="col-4" style="padding: 4px; height:200px;">
            <div style="float: left" class="description_large @if(@$ad->specials->where('position_number', '14')->first()->ad_stroke ==1)  stroke   @endif">
                {{ !empty($ad->specials->where('position_number', '14')->first()->item_description) ?
                 $ad->specials->where('position_number', '14')->first()->item_description :
                 "NO DESCRIPTION FOR THIS ITEM 14" }}
                <div class="size-lrg">
                    {{!empty($ad->specials->where('position_number', '14')->first()->size) ?
                    $ad->specials->where('position_number', '14')->first()->size :
                    "" }}
                </div>
            </div>
            <div class="price_3_by">
                {{!empty($ad->specials->where('position_number', '14')->first()->price) ? $ad->specials->where('position_number', '14')->first()->price : ""}}
            </div>
            <div class="product-img img-med" style="background-image:
            <?php echo  !empty($ad->specials->where('position_number', '14')->first()->image) ?
                "url('".$ad->specials->where('position_number', '14')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;">
            </div>
        </div>
        <div class="col-4" style="padding: 4px; height:200px;">
            <div style="float: left" class="description_large @if(@$ad->specials->where('position_number', '15')->first()->ad_stroke ==1)  stroke   @endif">
                {{ !empty($ad->specials->where('position_number', '15')->first()->item_description) ?
                 $ad->specials->where('position_number', '15')->first()->item_description :
                 "NO DESCRIPTION FOR THIS ITEM 15" }}
                <div class="size-lrg">
                    {{!empty($ad->specials->where('position_number', '15')->first()->size) ?
                    $ad->specials->where('position_number', '15')->first()->size :
                    "" }}
                </div>
            </div>
            <div class="price_3_by">
                {{!empty($ad->specials->where('position_number', '15')->first()->price) ? $ad->specials->where('position_number', '15')->first()->price : ""}}
            </div>

            <div class="product-img img-med" style="background-image:
            <?php echo  !empty($ad->specials->where('position_number', '15')->first()->image) ?
                "url('".$ad->specials->where('position_number', '15')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;">
            </div>
        </div>
    </div>
    <div class="row" style="height: 40px">
        <div class="col-12" style="background-color: #000; color:#ffffff; font-weight: bold; vertical-align: middle; text-align: center;padding-top: 6px; margin:2px;">
            Can't find what you want in our stores? SEND A TEXT MESSAGE TO 405-896-7933 TO REQUEST THE PRODUCT YOU NEED
        </div>
    </div>
    <div class="row" style="height: 40px">
        <div class="col-4" style="background-color: #C10230; color:#ffffff; font-weight: bold; padding-top:6px;text-align: center;">
            Prices good through {{date('m-d-Y', strtotime($ad->date_to))}}
        </div>
        <div class="col-4" style="padding-top: 6px; text-align: center; color: #C10230; background-color: #F3C300; font-weight: bold;">
            MONEY ORDERS: 49 cents
        </div>
        <div class="col-4" style="background-color: #C10230; color:#ffffff; font-weight: bold; padding-top: 6px; text-align: center;">3501 NW Expressway, OKC . 7957 NW 23rd St., Bethany</div>
    </div>
    <div class="row">
        <div class="col-12" style="font-size: 8px; text-align: center;">
            We Gladly Accept WIC Checks, Oklahoma Access, Manufacturer Coupons. We Reserve The Right To Limit Quantities. All Limits Are Per Family. Not Liable for Typographical or Pictorial Errors. Some Items Featured Only Available at This Location.
        </div>
    </div>

    <!--  PAGE BREAK  -->
    <div style="page-break-after: always;"></div>

<div class="row"> <br> </div>

    <div class="row">
        <div class="col-3 pg2sm" >
            <div class="desc_four @if(@$ad->specials->where('position_number', '16')->first()->ad_stroke ==1)  stroke   @endif" style="float: left">
                {{ !empty($ad->specials->where('position_number', '16')->first()->item_description) ?
                $ad->specials->where('position_number', '16')->first()->item_description :
                "NO DESCRIPTION FOR THIS ITEM 16" }}
                <div class="size-small">
                    {{!empty($ad->specials->where('position_number', '16')->first()->size) ?
                      $ad->specials->where('position_number', '16')->first()->size :
                    "" }}
                </div>
            </div>
            <div class="product-img img-sm" style="background-image:
            <?php echo  !empty($ad->specials->where('position_number', '16')->first()->image) ?
                "url('".$ad->specials->where('position_number', '16')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;">
            </div>
            <div class="price_four">
                {{!empty($ad->specials->where('position_number', '16')->first()->price) ? $ad->specials->where('position_number', '16')->first()->price : ""}}
            </div>
        </div>
        <div class="col-3 pg2sm" style="padding: 4px; ">
            <div class="desc_four @if(@$ad->specials->where('position_number', '17')->first()->ad_stroke ==1)  stroke   @endif" >
                {{ !empty($ad->specials->where('position_number', '17')->first()->item_description) ?
                 $ad->specials->where('position_number', '17')->first()->item_description :
                 "NO DESCRIPTION FOR THIS ITEM 17" }}
                <div class="size-small">
                    {{!empty($ad->specials->where('position_number', '17')->first()->size) ?
                      $ad->specials->where('position_number', '17')->first()->size :
                    "" }}
                </div>
            </div>
            <div class="product-img img-sm" style="background-image:
            <?php echo  !empty($ad->specials->where('position_number', '17')->first()->image) ?
                "url('".$ad->specials->where('position_number', '17')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;">
            </div>
            <div class="price_four">
                {{!empty($ad->specials->where('position_number', '17')->first()->price) ? $ad->specials->where('position_number', '17')->first()->price : ""}}

            </div>
        </div>
        <div class="col-3 pg2sm" >
            <div class="desc_four @if(@$ad->specials->where('position_number', '18')->first()->ad_stroke ==1)  stroke   @endif">
                {{ !empty($ad->specials->where('position_number', '18')->first()->item_description) ?
                $ad->specials->where('position_number', '18')->first()->item_description :
                "NO DESCRIPTION FOR THIS ITEM 18" }}
                <div class="size-small">
                    {{!empty($ad->specials->where('position_number', '18')->first()->size) ?
                      $ad->specials->where('position_number', '18')->first()->size :
                    "" }}
                </div>
            </div>
            <div class="product-img img-sm" style="background-image:
            <?php echo  !empty($ad->specials->where('position_number', '18')->first()->image) ?
                "url('".$ad->specials->where('position_number', '18')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;">
            </div>
            <div class="price_four">
                {{!empty($ad->specials->where('position_number', '18')->first()->price) ? $ad->specials->where('position_number', '18')->first()->price : ""}}

            </div>

        </div>
        <div class="col-3 pg2sm">
            <div class="desc_four @if(@$ad->specials->where('position_number', '19')->first()->ad_stroke ==1)  stroke   @endif">
                {{ !empty($ad->specials->where('position_number', '19')->first()->item_description) ?
                $ad->specials->where('position_number', '19')->first()->item_description :
                "NO DESCRIPTION FOR THIS ITEM 19" }}
                <div class="size-small">
                    {{!empty($ad->specials->where('position_number', '19')->first()->size) ?
                      $ad->specials->where('position_number', '19')->first()->size :
                    "" }}
                </div>
            </div>
            <div class="product-img img-sm" style="background-image:
            <?php echo  !empty($ad->specials->where('position_number', '19')->first()->image) ?
                "url('".$ad->specials->where('position_number', '19')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;">
            </div>
            <div class="price_four">
                {{!empty($ad->specials->where('position_number', '19')->first()->price) ? $ad->specials->where('position_number', '19')->first()->price : ""}}

            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-3 pg2sm" >
            <div style="float: left;" class="desc_four @if(@$ad->specials->where('position_number', '20')->first()->ad_stroke ==1)  stroke   @endif">
                {{ !empty($ad->specials->where('position_number', '20')->first()->item_description) ?
                $ad->specials->where('position_number', '20')->first()->item_description :
                "NO DESCRIPTION FOR THIS ITEM 20" }}
                <div class="size-small">
                    {{!empty($ad->specials->where('position_number', '20')->first()->size) ?
                      $ad->specials->where('position_number', '20')->first()->size :
                    "" }}
                </div>
            </div>
            <div class="product-img img-sm" style="background-image:
            <?php echo  !empty($ad->specials->where('position_number', '20')->first()->image) ?
                "url('".$ad->specials->where('position_number', '20')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;">
            </div>
            <div class="price_four">
                {{!empty($ad->specials->where('position_number', '20')->first()->price) ? $ad->specials->where('position_number', '20')->first()->price : ""}}

            </div>

        </div>
        <div class="col-3 pg2sm" style="padding: 4px; ">
            <div style="float:left;" class="desc_four @if(@$ad->specials->where('position_number', '21')->first()->ad_stroke ==1)  stroke   @endif">
                {{ !empty($ad->specials->where('position_number', '21')->first()->item_description) ?
                $ad->specials->where('position_number', '21')->first()->item_description :
                "NO DESCRIPTION FOR THIS ITEM 21" }}
                <div class="size-small">
                    {{!empty($ad->specials->where('position_number', '21')->first()->size) ?
                      $ad->specials->where('position_number', '21')->first()->size :
                    "" }}
                </div>
            </div>
            <div class="product-img img-sm" style="background-image:
            <?php echo  !empty($ad->specials->where('position_number', '21')->first()->image) ?
                "url('".$ad->specials->where('position_number', '21')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;">
            </div>

                <div class="price_four">
                    {{!empty($ad->specials->where('position_number', '21')->first()->price) ? $ad->specials->where('position_number', '21')->first()->price : ""}}
                </div>

        </div>
        <div class="col-3 pg2sm" >
            <div style="float:left;" class="desc_four @if(@$ad->specials->where('position_number', '22')->first()->ad_stroke ==1)  stroke   @endif">
                {{ !empty($ad->specials->where('position_number', '22')->first()->item_description) ?
                 $ad->specials->where('position_number', '22')->first()->item_description :
                 "NO DESCRIPTION FOR THIS ITEM 22" }}
                <div class="size-small">
                    {{!empty($ad->specials->where('position_number', '22')->first()->size) ?
                      $ad->specials->where('position_number', '22')->first()->size :
                    "" }}
                </div>
            </div>
            <div class="product-img img-sm" style="background-image:
            <?php echo  !empty($ad->specials->where('position_number', '22')->first()->image) ?
                "url('".$ad->specials->where('position_number', '22')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;">
            </div>
            <div class="price_four">
                {{!empty($ad->specials->where('position_number', '22')->first()->price) ? $ad->specials->where('position_number', '22')->first()->price : ""}}

            </div>
        </div>
        <div class="col-3 pg2sm">
            <div style="float:left;" class="desc_four @if(@$ad->specials->where('position_number', '23')->first()->ad_stroke ==1)  stroke   @endif">
                {{ !empty($ad->specials->where('position_number', '23')->first()->item_description) ?
                $ad->specials->where('position_number', '23')->first()->item_description :
                "NO DESCRIPTION FOR THIS ITEM 23" }}
                <div class="size-small">
                    {{!empty($ad->specials->where('position_number', '23')->first()->size) ?
                      $ad->specials->where('position_number', '23')->first()->size :
                    "" }}
                </div>
            </div>
            <div class="product-img img-sm" style="background-image:
            <?php echo  !empty($ad->specials->where('position_number', '23')->first()->image) ?
                "url('".$ad->specials->where('position_number', '23')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;">
            </div>
            <div class="price_four">
                {{!empty($ad->specials->where('position_number', '23')->first()->price) ? $ad->specials->where('position_number', '23')->first()->price : ""}}

            </div>


        </div>
    </div>
    <div class="row">
        <div class="col-3 pg2sm">
            <div style="float: left;" class="desc_four @if(@$ad->specials->where('position_number', '24')->first()->ad_stroke ==1)  stroke   @endif">
                {{ !empty($ad->specials->where('position_number', '24')->first()->item_description) ?
                $ad->specials->where('position_number', '24')->first()->item_description :
                "NO DESCRIPTION FOR THIS ITEM 24" }}
                <div class="size-small">
                    {{!empty($ad->specials->where('position_number', '24')->first()->size) ?
                      $ad->specials->where('position_number', '24')->first()->size :
                    "" }}
                </div>
            </div>
            <div class="product-img img-sm" style="background-image:
            <?php echo  !empty($ad->specials->where('position_number', '24')->first()->image) ?
                "url('".$ad->specials->where('position_number', '24')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;">
            </div>
            <div class="price_four">
                {{!empty($ad->specials->where('position_number', '24')->first()->price) ? $ad->specials->where('position_number', '24')->first()->price : ""}}

            </div>

        </div>
        <div class="col-3 pg2sm" style="padding: 4px; ">
            <div style="float:left;" class="desc_four @if(@$ad->specials->where('position_number', '25')->first()->ad_stroke ==1)  stroke   @endif">
                {{ !empty($ad->specials->where('position_number', '25')->first()->item_description) ?
                $ad->specials->where('position_number', '25')->first()->item_description :
                "NO DESCRIPTION FOR THIS ITEM 25" }}
                <div class="size-small">
                    {{!empty($ad->specials->where('position_number', '25')->first()->size) ?
                      $ad->specials->where('position_number', '25')->first()->size :
                    "" }}
                </div>
            </div>
            <div class="product-img img-sm" style="background-image:
            <?php echo  !empty($ad->specials->where('position_number', '25')->first()->image) ?
                "url('".$ad->specials->where('position_number', '25')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;">
            </div>
            <div class="price_four">
                {{!empty($ad->specials->where('position_number', '25')->first()->price) ? $ad->specials->where('position_number', '25')->first()->price : ""}}

            </div>
        </div>
        <div class="col-3 pg2sm" >
            <div style="float:right;" class="desc_four @if(@$ad->specials->where('position_number', '26')->first()->ad_stroke ==1)  stroke   @endif">
                {{ !empty($ad->specials->where('position_number', '26')->first()->item_description) ?
                $ad->specials->where('position_number', '26')->first()->item_description :
                "NO DESCRIPTION FOR THIS ITEM 26" }}
                <div class="size-small">
                    {{!empty($ad->specials->where('position_number', '26')->first()->size) ?
                      $ad->specials->where('position_number', '26')->first()->size :
                    "" }}

                </div>
            </div>
            <div class="product-img img-sm" style="background-image:
            <?php echo  !empty($ad->specials->where('position_number', '26')->first()->image) ?
                "url('".$ad->specials->where('position_number', '26')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;">
            </div>
            <div class="price_four">
                {{!empty($ad->specials->where('position_number', '26')->first()->price) ? $ad->specials->where('position_number', '26')->first()->price : ""}}

            </div>

        </div>
        <div class="col-3 pg2sm">
            <div style="float:left;" class="desc_four @if(@$ad->specials->where('position_number', '27')->first()->ad_stroke ==1)  stroke   @endif">
                {{ !empty($ad->specials->where('position_number', '27')->first()->item_description) ?
                $ad->specials->where('position_number', '27')->first()->item_description :
                "NO DESCRIPTION FOR THIS ITEM 27" }}
                <div class="size-small">
                    {{!empty($ad->specials->where('position_number', '27')->first()->size) ?
                      $ad->specials->where('position_number', '27')->first()->size :
                    "" }}
                </div>
            </div>
            <div class="product-img img-sm" style="background-image:
            <?php echo  !empty($ad->specials->where('position_number', '27')->first()->image) ?
                "url('".$ad->specials->where('position_number', '27')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;">
            </div>
            <div class="price_four">
                {{!empty($ad->specials->where('position_number', '27')->first()->price) ? $ad->specials->where('position_number', '27')->first()->price : ""}}

            </div>


        </div>
    </div>
    <div class="row">
        <div class="col-3 pg2sm" >
            <div style="float: left;" class="desc_four @if(@$ad->specials->where('position_number', '28')->first()->ad_stroke ==1)  stroke   @endif">
                {{ !empty($ad->specials->where('position_number', '28')->first()->item_description) ?
                $ad->specials->where('position_number', '28')->first()->item_description :
                "NO DESCRIPTION FOR THIS ITEM 28" }}
                <div class="size-small">
                    {{!empty($ad->specials->where('position_number', '28')->first()->size) ?
                      $ad->specials->where('position_number', '28')->first()->size :
                    "" }}
                </div>
            </div>
            <div class="product-img img-sm" style="background-image:
            <?php echo  !empty($ad->specials->where('position_number', '28')->first()->image) ?
                "url('".$ad->specials->where('position_number', '28')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;">
            </div>
            <div class="price_four">
                {{!empty($ad->specials->where('position_number', '28')->first()->price) ? $ad->specials->where('position_number', '28')->first()->price : ""}}
            </div>
        </div>
        <div class="col-3 pg2sm" style="padding: 4px; ">
            <div style="float:left;" class="desc_four @if(@$ad->specials->where('position_number', '29')->first()->ad_stroke ==1)  stroke   @endif">
                {{ !empty($ad->specials->where('position_number', '29')->first()->item_description) ?
                $ad->specials->where('position_number', '29')->first()->item_description :
                "NO DESCRIPTION FOR THIS ITEM 29" }}
                <div class="size-small">
                    {{!empty($ad->specials->where('position_number', '29')->first()->size) ?
                      $ad->specials->where('position_number', '29')->first()->size :
                    "" }}
                </div>
            </div>
            <div class="product-img img-sm" style="background-image:
            <?php echo  !empty($ad->specials->where('position_number', '29')->first()->image) ?
                "url('".$ad->specials->where('position_number', '29')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;">
            </div>
            <div class="price_four">
                {{!empty($ad->specials->where('position_number', '29')->first()->price) ? $ad->specials->where('position_number', '29')->first()->price : ""}}
            </div>
        </div>
        <div class="col-3 pg2sm" >
            <div style="float:left;" class="desc_four @if(@$ad->specials->where('position_number', '30')->first()->ad_stroke ==1)  stroke   @endif">
                {{ !empty($ad->specials->where('position_number', '30')->first()->item_description) ?
                $ad->specials->where('position_number', '30')->first()->item_description :
                "NO DESCRIPTION FOR THIS ITEM 30" }}
                <div class="size-small">
                    {{!empty($ad->specials->where('position_number', '30')->first()->size) ?
                      $ad->specials->where('position_number', '30')->first()->size :
                    "" }}
                </div>
            </div>
            <div class="product-img img-sm" style="background-image:
            <?php echo  !empty($ad->specials->where('position_number', '30')->first()->image) ?
                "url('".$ad->specials->where('position_number', '30')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;">
            </div>
            <div class="price_four">
                {{!empty($ad->specials->where('position_number', '30')->first()->price) ? $ad->specials->where('position_number', '30')->first()->price : ""}}
            </div>
        </div>
        <div class="col-3 pg2sm">
            <div style="float:left;" class="desc_four @if(@$ad->specials->where('position_number', '31')->first()->ad_stroke ==1)  stroke   @endif">
                {{ !empty($ad->specials->where('position_number', '31')->first()->item_description) ?
                $ad->specials->where('position_number', '31')->first()->item_description :
                "NO DESCRIPTION FOR THIS ITEM 31" }}
                <div class="size-small">
                    {{!empty($ad->specials->where('position_number', '31')->first()->size) ?
                      $ad->specials->where('position_number', '31')->first()->size :
                    "" }}
                </div>
            </div>
            <div class="product-img img-sm" style="background-image:
            <?php echo  !empty($ad->specials->where('position_number', '31')->first()->image) ?
                "url('".$ad->specials->where('position_number', '31')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;">
            </div>
            <div class="price_four">
                {{!empty($ad->specials->where('position_number', '31')->first()->price) ? $ad->specials->where('position_number', '31')->first()->price : ""}}

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-3 pg2sm" >
            <div style="float: left;" class="desc_four @if(@$ad->specials->where('position_number', '32')->first()->ad_stroke ==1)  stroke   @endif">
                {{ !empty($ad->specials->where('position_number', '32')->first()->item_description) ?
                  $ad->specials->where('position_number', '32')->first()->item_description :
                  "NO DESCRIPTION FOR THIS ITEM 32" }}
                <div class="size-small">
                    {{!empty($ad->specials->where('position_number', '32')->first()->size) ?
                      $ad->specials->where('position_number', '32')->first()->size :
                    "" }}
                </div>
            </div>
            <div style="float: right;">
                <?php echo  !empty($ad->specials->where('position_number', '32')->first()->image) ?
                    '<img src="'.$ad->specials->where('position_number', '32')->first()->image.'" height="170">' :
                    "<img width='170' src='/img/pending-image.jpg' >"; ?>
            </div>
            <div class="price_four">
                {{!empty($ad->specials->where('position_number', '32')->first()->price) ? $ad->specials->where('position_number', '32')->first()->price : ""}}

            </div>
        </div>
        <div class="col-3 pg2sm" style="padding: 4px; ">
            <div style="float:left;" class="desc_four @if(@$ad->specials->where('position_number', '33')->first()->ad_stroke ==1)  stroke   @endif">
                {{ !empty($ad->specials->where('position_number', '33')->first()->item_description) ?
                $ad->specials->where('position_number', '33')->first()->item_description :
                "NO DESCRIPTION FOR THIS ITEM 33" }}
                <div class="size-small">
                    {{!empty($ad->specials->where('position_number', '33')->first()->size) ?
                      $ad->specials->where('position_number', '33')->first()->size :
                    "" }}
                </div>
            </div>
            <div class="product-img img-sm" style="background-image:
            <?php echo  !empty($ad->specials->where('position_number', '33')->first()->image) ?
                "url('".$ad->specials->where('position_number', '33')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;">
            </div>
            <div class="price_four">
                {{!empty($ad->specials->where('position_number', '33')->first()->price) ? $ad->specials->where('position_number', '33')->first()->price : ""}}

            </div>
        </div>
        <div class="col-3 pg2sm" >
            <div style="float:left;" class="desc_four @if(@$ad->specials->where('position_number', '34')->first()->ad_stroke ==1)  stroke   @endif">
                {{ !empty($ad->specials->where('position_number', '34')->first()->item_description) ?
                $ad->specials->where('position_number', '34')->first()->item_description :
                "NO DESCRIPTION FOR THIS ITEM 34" }}
                <div class="size-small">
                    {{!empty($ad->specials->where('position_number', '34')->first()->size) ?
                      $ad->specials->where('position_number', '34')->first()->size :
                    "" }}
                </div>
            </div>
            <div class="product-img img-sm" style="background-image:
            <?php echo  !empty($ad->specials->where('position_number', '34')->first()->image) ?
                "url('".$ad->specials->where('position_number', '34')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;">
            </div>
            <div class="price_four">
                {{!empty($ad->specials->where('position_number', '34')->first()->price) ? $ad->specials->where('position_number', '34')->first()->price : ""}}

            </div>
        </div>
        <div class="col-3 pg2sm">
            <div style="float:left;" class="desc_four @if(@$ad->specials->where('position_number', '35')->first()->ad_stroke ==1)  stroke   @endif">
                {{ !empty($ad->specials->where('position_number', '35')->first()->item_description) ?
                $ad->specials->where('position_number', '35')->first()->item_description :
                "NO DESCRIPTION FOR THIS ITEM 35" }}
                <div class="size-small">
                    {{!empty($ad->specials->where('position_number', '35')->first()->size) ?
                      $ad->specials->where('position_number', '35')->first()->size :
                    "" }}
                </div>
            </div>
            <div class="product-img img-sm" style="background-image:
            <?php echo  !empty($ad->specials->where('position_number', '35')->first()->image) ?
                "url('".$ad->specials->where('position_number', '35')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;">
            </div>
            <div class="price_four">
                {{!empty($ad->specials->where('position_number', '35')->first()->price) ? $ad->specials->where('position_number', '35')->first()->price : ""}}

            </div>
        </div>
    </div>

    <div class="row" >
    <div class="col-9 pg2sm" style="height: 340px; ">

            <div style=" width: 220px; height:170px; float: left; margin-left: -16px;">
                <div class="desc_four @if(@$ad->specials->where('position_number', '36')->first()->ad_stroke ==1)  stroke   @endif">
                    {{ !empty($ad->specials->where('position_number', '36')->first()->item_description) ?
                $ad->specials->where('position_number', '36')->first()->item_description :
                "NO DESCRIPTION FOR THIS ITEM 36" }}
                    <div class="size-small">
                        {{!empty($ad->specials->where('position_number', '36')->first()->size) ?
                          $ad->specials->where('position_number', '36')->first()->size :
                        "" }}
                    </div>
                </div>
                <div class="price_four">
                    {{!empty($ad->specials->where('position_number', '36')->first()->price) ? $ad->specials->where('position_number', '36')->first()->price : ""}}

                </div>
                <div class="product-img img-sm" style="background-image:
                <?php echo  !empty($ad->specials->where('position_number', '36')->first()->image) ?
                    "url('".$ad->specials->where('position_number', '36')->first()->image."')" :
                    "url('/img/pending-image.jpg');" ?> ;">
                </div>

            </div>
            <div style=" width: 220px; height:170px; float: left;">
                <div class="desc_four @if(@$ad->specials->where('position_number', '37')->first()->ad_stroke ==1)  stroke   @endif">
                    {{ !empty($ad->specials->where('position_number', '37')->first()->item_description) ?
                 $ad->specials->where('position_number', '37')->first()->item_description :
                 "NO DESCRIPTION FOR THIS ITEM 37" }}
                    <div class="size-small">
                        {{!empty($ad->specials->where('position_number', '37')->first()->size) ?
                          $ad->specials->where('position_number', '37')->first()->size :
                        "" }}
                    </div>
                </div>
                <div class="price_four">
                    {{!empty($ad->specials->where('position_number', '37')->first()->price) ? $ad->specials->where('position_number', '37')->first()->price : ""}}

                </div>
                <div class="product-img img-sm" style="background-image:
                <?php echo  !empty($ad->specials->where('position_number', '37')->first()->image) ?
                    "url('".$ad->specials->where('position_number', '37')->first()->image."')" :
                    "url('/img/pending-image.jpg');" ?> ;">
                </div>

            </div>
            <div style=" width: 220px; height:170px; float: left;">
                <div class="desc_four @if(@$ad->specials->where('position_number', '38')->first()->ad_stroke ==1)  stroke   @endif">
                    {{ !empty($ad->specials->where('position_number', '38')->first()->item_description) ?
                $ad->specials->where('position_number', '38')->first()->item_description :
                "NO DESCRIPTION FOR THIS ITEM 38" }}
                    <div class="size-small">
                        {{!empty($ad->specials->where('position_number', '38')->first()->size) ?
                          $ad->specials->where('position_number', '38')->first()->size :
                        "" }}
                    </div>
                </div>
                <div class="price_four">
                    {{!empty($ad->specials->where('position_number', '38')->first()->price) ? $ad->specials->where('position_number', '38')->first()->price : ""}}

                </div>
                <div class="product-img img-sm" style="background-image:
                <?php echo  !empty($ad->specials->where('position_number', '38')->first()->image) ?
                    "url('".$ad->specials->where('position_number', '38')->first()->image."')" :
                    "url('/img/pending-image.jpg');" ?> ;">
                </div>
            </div>

        <div class="pg2sm">
            <div style="width: 220px; height: 170px; float: left; margin-left: -16px;">
                <div class="desc_four @if(@$ad->specials->where('position_number', '39')->first()->ad_stroke ==1)  stroke   @endif">
                    {{ !empty($ad->specials->where('position_number', '39')->first()->item_description) ?
                $ad->specials->where('position_number', '39')->first()->item_description :
                "NO DESCRIPTION FOR THIS ITEM 39" }}

                    <div class="size-small">
                        {{!empty($ad->specials->where('position_number', '39')->first()->size) ?
                          $ad->specials->where('position_number', '39')->first()->size :
                        "" }}
                    </div>
                </div>
                <div class="price_four">
                    {{!empty($ad->specials->where('position_number', '39')->first()->price) ? $ad->specials->where('position_number', '39')->first()->price : ""}}

                </div>
                <div class="picture_four" style="float:right;">
                    <?php echo  !empty($ad->specials->where('position_number', '39')->first()->image) ?
                        '<img src="'.$ad->specials->where('position_number', '39')->first()->image.'" height="170">' :
                        "<img width='170' src='/img/pending-image.jpg' >"; ?>
                </div>
            </div>
            <div style="width: 220px; height: 170px; float: left;">
                <div class="desc_four @if(@$ad->specials->where('position_number', '40')->first()->ad_stroke ==1)  stroke   @endif">
                    {{ !empty($ad->specials->where('position_number', '40')->first()->item_description) ?
                $ad->specials->where('position_number', '40')->first()->item_description :
                "NO DESCRIPTION FOR THIS ITEM 40" }}
                    <div class="size-small">
                        {{!empty($ad->specials->where('position_number', '40')->first()->size) ?
                          $ad->specials->where('position_number', '40')->first()->size :
                        "" }}
                    </div>
                </div>
                <div class="price_four">
                    {{!empty($ad->specials->where('position_number', '40')->first()->price) ? $ad->specials->where('position_number', '40')->first()->price : ""}}

                </div>
                <div class="product-img img-sm" style="background-image:
                <?php echo  !empty($ad->specials->where('position_number', '40')->first()->image) ?
                    "url('".$ad->specials->where('position_number', '40')->first()->image."')" :
                    "url('/img/pending-image.jpg');" ?> ;">
                </div>
            </div>
            <div style="width: 220px; height: 170px; float: left;">
                <div class="desc_four @if(@$ad->specials->where('position_number', '41')->first()->ad_stroke ==1)  stroke   @endif">
                    {{ !empty($ad->specials->where('position_number', '41')->first()->item_description) ?
                $ad->specials->where('position_number', '41')->first()->item_description :
                "NO DESCRIPTION FOR THIS ITEM 41" }}
                    <div class="size-small">
                        {{!empty($ad->specials->where('position_number', '41')->first()->size) ?
                          $ad->specials->where('position_number', '41')->first()->size :
                        "" }}
                    </div>
                </div>
                <div class="price_four">
                    {{!empty($ad->specials->where('position_number', '41')->first()->price) ? $ad->specials->where('position_number', '41')->first()->price : ""}}

                </div>
                <div class="product-img img-sm" style="background-image:
                <?php echo  !empty($ad->specials->where('position_number', '41')->first()->image) ?
                    "url('".$ad->specials->where('position_number', '41')->first()->image."')" :
                    "url('/img/pending-image.jpg');" ?> ;">
                </div>
            </div>

        </div>

    </div>
    <div class="col-3">

        <div class="desc_four @if(@$ad->specials->where('position_number', '42')->first()->ad_stroke ==1)  stroke   @endif">{{ !empty($ad->specials->where('position_number', '42')->first()->item_description) ?
                $ad->specials->where('position_number', '42')->first()->item_description :
                "NO DESCRIPTION FOR THIS ITEM 42" }}</div>
        <div class="size-small">
            {{!empty($ad->specials->where('position_number', '42')->first()->size) ?
              $ad->specials->where('position_number', '42')->first()->size :
            "" }}
        </div>
        <div class="product-img img-long" style="background-image:
        <?php echo  !empty($ad->specials->where('position_number', '42')->first()->image) ?
            "url('".$ad->specials->where('position_number', '42')->first()->image."')" :
            "url('/img/pending-image.jpg');" ?> ;">
        </div>
        <div>
            {{!empty($ad->specials->where('position_number', '42')->first()->price) ? $ad->specials->where('position_number', '42')->first()->price : ""}}

        </div>
    </div>


    </div>
    <div class="row">
        <div class="col-3 pg2sm" >
            <div class="desc_four @if(@$ad->specials->where('position_number', '43')->first()->ad_stroke ==1)  stroke   @endif">
                {{ !empty($ad->specials->where('position_number', '43')->first()->item_description) ?
                $ad->specials->where('position_number', '43')->first()->item_description :
                "NO DESCRIPTION FOR THIS ITEM 43" }}
                <div class="size-small">
                    {{!empty($ad->specials->where('position_number', '43')->first()->size) ?
                      $ad->specials->where('position_number', '43')->first()->size :
                    "" }}
                </div>
            </div>
            <div class="price_four">
                {{!empty($ad->specials->where('position_number', '43')->first()->price) ? $ad->specials->where('position_number', '43')->first()->price : ""}}

            </div>
            <div class="product-img img-sm" style="background-image:
            <?php echo  !empty($ad->specials->where('position_number', '43')->first()->image) ?
                "url('".$ad->specials->where('position_number', '43')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;">
            </div>
        </div>
        <div class="col-3 pg2sm" >
            <div class="desc_four @if(@$ad->specials->where('position_number', '44')->first()->ad_stroke ==1)  stroke   @endif">
                {{ !empty($ad->specials->where('position_number', '44')->first()->item_description) ?
                $ad->specials->where('position_number', '44')->first()->item_description :
                "NO DESCRIPTION FOR THIS ITEM 44" }}
                <div class="size-small">
                    {{!empty($ad->specials->where('position_number', '44')->first()->size) ?
                      $ad->specials->where('position_number', '44')->first()->size :
                    "" }}
                </div>
            </div>
            <div class="price_four">
                {{!empty($ad->specials->where('position_number', '44')->first()->price) ? $ad->specials->where('position_number', '44')->first()->price : ""}}

            </div>
            <div class="product-img img-sm" style="background-image:
            <?php echo  !empty($ad->specials->where('position_number', '44')->first()->image) ?
                "url('".$ad->specials->where('position_number', '44')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;">
            </div>
        </div>
        <div class="col-3 pg2sm" >
            <div class="desc_four @if(@$ad->specials->where('position_number', '45')->first()->ad_stroke ==1)  stroke   @endif">
                {{ !empty($ad->specials->where('position_number', '45')->first()->item_description) ?
                $ad->specials->where('position_number', '45')->first()->item_description :
                "NO DESCRIPTION FOR THIS ITEM 45" }}
                <div class="size-small">
                    {{!empty($ad->specials->where('position_number', '45')->first()->size) ?
                      $ad->specials->where('position_number', '45')->first()->size :
                    "" }}
                </div>
            </div>
            <div class="price_four">
                {{!empty($ad->specials->where('position_number', '45')->first()->price) ? $ad->specials->where('position_number', '45')->first()->price : ""}}

            </div>
            <div class="product-img img-sm" style="background-image:
            <?php echo  !empty($ad->specials->where('position_number', '45')->first()->image) ?
                "url('".$ad->specials->where('position_number', '45')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;">
            </div>
        </div>
        <div class="col-3 pg2sm" >
            <div class="desc_four @if(@$ad->specials->where('position_number', '46')->first()->ad_stroke ==1)  stroke   @endif">
                {{ !empty($ad->specials->where('position_number', '46')->first()->item_description) ?
                 $ad->specials->where('position_number', '46')->first()->item_description :
                 "NO DESCRIPTION FOR THIS ITEM 46" }}
                <div class="size-small">
                    {{!empty($ad->specials->where('position_number', '46')->first()->size) ?
                      $ad->specials->where('position_number', '46')->first()->size :
                    "" }}
                </div>
            </div>
            <div class="price_four">
                {{!empty($ad->specials->where('position_number', '46')->first()->price) ? $ad->specials->where('position_number', '46')->first()->price : ""}}

            </div>
            <div class="product-img img-sm" style="background-image:
            <?php echo  !empty($ad->specials->where('position_number', '46')->first()->image) ?
                "url('".$ad->specials->where('position_number', '46')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;">
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-3 pg2sm" >
            <div class="desc_four @if(@$ad->specials->where('position_number', '47')->first()->ad_stroke ==1)  stroke   @endif">
                {{ !empty($ad->specials->where('position_number', '47')->first()->item_description) ?
                $ad->specials->where('position_number', '47')->first()->item_description :
                "NO DESCRIPTION FOR THIS ITEM 47" }}
                <div class="size-small">
                    {{!empty($ad->specials->where('position_number', '47')->first()->size) ?
                      $ad->specials->where('position_number', '47')->first()->size :
                    "" }}
                </div>
            </div>
            <div class="price_four" style="float: right">
                {{!empty($ad->specials->where('position_number', '47')->first()->price) ? $ad->specials->where('position_number', '47')->first()->price : ""}}

            </div>
            <div class="product-img img-sm" style="background-image:
            <?php echo  !empty($ad->specials->where('position_number', '47')->first()->image) ?
                "url('".$ad->specials->where('position_number', '47')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;">
            </div>
        </div>
        <div class="col-3 pg2sm" >
            <div class="desc_four @if(@$ad->specials->where('position_number', '48')->first()->ad_stroke ==1)  stroke   @endif">
                {{ !empty($ad->specials->where('position_number', '48')->first()->item_description) ?
                $ad->specials->where('position_number', '48')->first()->item_description :
                "NO DESCRIPTION FOR THIS ITEM 48" }}
                <div class="size-small">
                    {{!empty($ad->specials->where('position_number', '48')->first()->size) ?
                      $ad->specials->where('position_number', '48')->first()->size :
                    "" }}
                </div>
            </div>
            <div class="price_four">
                {{!empty($ad->specials->where('position_number', '48')->first()->price) ? $ad->specials->where('position_number', '48')->first()->price : ""}}

            </div>
            <div class="product-img img-sm" style="background-image:
            <?php echo  !empty($ad->specials->where('position_number', '48')->first()->image) ?
                "url('".$ad->specials->where('position_number', '48')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;">
            </div>
        </div>
        <div class="col-3 pg2sm" >
            <div class="desc_four @if(@$ad->specials->where('position_number', '49')->first()->ad_stroke ==1)  stroke   @endif">
                {{ !empty($ad->specials->where('position_number', '49')->first()->item_description) ?
                $ad->specials->where('position_number', '49')->first()->item_description :
                "NO DESCRIPTION FOR THIS ITEM 49" }}
                <div class="size-small">
                    {{!empty($ad->specials->where('position_number', '49')->first()->size) ?
                      $ad->specials->where('position_number', '49')->first()->size :
                    "" }}
                </div>
            </div>
            <div class="price_four">
                {{!empty($ad->specials->where('position_number', '49')->first()->price) ? $ad->specials->where('position_number', '49')->first()->price : ""}}

            </div>
            <div class="product-img img-sm" style="background-image:
            <?php echo  !empty($ad->specials->where('position_number', '49')->first()->image) ?
                "url('".$ad->specials->where('position_number', '49')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;">
            </div>
        </div>
        <div class="col-3 pg2sm" style="float:left;">
            <div class="desc_four @if(@$ad->specials->where('position_number', '50')->first()->ad_stroke ==1)  stroke   @endif">
                {{ !empty($ad->specials->where('position_number', '50')->first()->item_description) ?
                 $ad->specials->where('position_number', '50')->first()->item_description :
                 "NO DESCRIPTION FOR THIS ITEM 50" }}
                <div class="size-small">
                    {{!empty($ad->specials->where('position_number', '50')->first()->size) ?
                      $ad->specials->where('position_number', '50')->first()->size :
                    "" }}
                </div>
            </div>
            <div class="price_four">
                {{!empty($ad->specials->where('position_number', '50')->first()->price) ? $ad->specials->where('position_number', '50')->first()->price : ""}}

            </div>
            <div class="product-img img-sm" style="background-image:
            <?php echo  !empty($ad->specials->where('position_number', '50')->first()->image) ?
                "url('".$ad->specials->where('position_number', '50')->first()->image."')" :
                "url('/img/pending-image.jpg');" ?> ;">
            </div>
        </div>

    </div>
</div>
</body>
</html>

<?php

function split_price($price){

    if (preg_match('/\./', $price)){
        $array = explode('.', $price);
    }
    else {
        $array[0] = $price;
        $array[1] = '';
    }

    if ($array[0] ==''){
        $array[0] = '.'.$array[1];
        $array[1] ='';
    }
    return $array;
}

?>