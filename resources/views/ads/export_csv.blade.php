<table>
    <thead>
    <tr>
        <th>UPC</th>
        <th>Ad Retail</th>
        <th>Order Number</th>
        <th>Limit</th>
        <th>Item Description</th>
        <th>Size</th>
        <th>Cost</th>
        <th>Yield</th>
        <th>Adj Cost</th>
        <th>Ad Gross %</th>
        <th>Supplier</th>
        <th>Image</th>
    </tr>
    </thead>
    <tbody>
    @foreach($specials as $special)
        <tr>
            <td>{{ $special->sku }}</td>
            <td>{{ $special->srp }}</td>
            <td>{{ $special->order_no }}</td>
            <td>{{ $special->limit }}</td>
            <td>{{ $special->item_description }}</td>
            <td>{{ $special->size }}</td>
            <td>{{ $special->cost }}</td>
            <td>{{ $special->yield }}</td>
            <td>{{ $special->adj_cost }}</td>
            <td>{{ $special->ad_gross }}</td>
            <td>{{ $special->supplier }}</td>
            <td>{{ $special->image }}</td>
        </tr>
    @endforeach
    </tbody>
</table>