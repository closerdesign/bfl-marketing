@extends('layouts.app-wo-menu')

@section('content')

    <p>&nbsp;</p>

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading" style="text-transform: uppercase">{{ $info->name }}<a class="pull-right" href="/ads/current/{{ $brand }}"><i class="fa fa-arrow-circle-left"></i> Go back</a></div>

                    <div class="panel-body">

                        <div>

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                @foreach($pages as $page)
                                <li role="presentation" @if( $page->number == 1 ) class="active" @endif><a href="#page{{ $page->number }}" aria-controls="home" role="tab" data-toggle="tab">Page {{ $page->number }}</a></li>
                                @endforeach
                                <li role="presentation"><a href="#download" aria-controls="home" role="tab" data-toggle="tab">Download</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                @foreach($pages as $page)
                                <div role="tabpanel" class="tab-pane @if( $page->number == 1 ) active @endif" id="page{{ $page->number }}">
                                    <img src="{{ $page->image }}" alt="" class="img-responsive">
                                </div>
                                @endforeach
                                <div role="tabpanel" class="tab-pane" id="download">
                                    <p>&nbsp;</p>
                                    @foreach($files as $file)
                                    <p><a target="_blank" href="{{ $file->file }}" class="btn btn-primary form-control">Download</a></p>
                                    @endforeach
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection