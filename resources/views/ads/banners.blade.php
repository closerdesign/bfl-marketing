@extends('layouts.app')

@section('content')

    <div class="container">
        @foreach($formats as $format)
            <div class="panel panel-primary">
                <div class="panel-heading">
                    {{ $format }}
                </div>
                <div class="panel-body">
                    @foreach($specials as $special)
                    <div class="{{ $format }}" id="banner{{ $special->id }}">
                        <div class="item-description">
                            {!! $special->item_description !!}
                        </div>
                        <div class="price">
                            {{ $special->price }}
                            <p class="size">{!! $special->size !!}</p>
                        </div>
                        <div class="image">
                            @if($special->image == "")
                                <img src="/img/pending-image.jpg" alt="{{ $special->item_description }}">
                            @else
                                <img src="{{ $special->image }}" alt="">
                            @endif
                        </div>
                        <div class="logo">
                            <img src="{{ $special->ad->brands->logo }}" >
                        </div>
                        <div class="valid-dates">
                            VALID {{ date('F jS', strtotime($special->ad->date_from)) }} TO {{ date('F jS', strtotime($special->ad->date_to)) }}
                        </div>
                        <div class="bottom">
                            VISIT US AT <span class="url">{{ $special->ad->brands->website }}</span>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            @endforeach
    </div>

    @endsection