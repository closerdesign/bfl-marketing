<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>{{ $ad->name }}</title>
    <style>
        @page{
            margin: 0pt;
        }
        .page-break {
            page-break-after: always;
        }
        body{
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        }
        h1 {
            color: white;
            font-size: 22pt;
        }
        /* PAGE 1 */
        .pg1-header {
            position: absolute;
            background-color: lightcoral;
            left: 10pt;
            top: 10pt;
            width: 590pt;
            height: 100pt;
        }
        .pg1-header-valid {
            position: absolute;
            font-size: 9pt;
            width: 590pt;
            text-align: center;
        }
        .pg1-feature1 {
            position: absolute;
            background-color: lightblue;
            left: 10pt;
            top: 110pt;
            height: 350pt;
            width: 230pt;
            margin-top: 10pt;
        }
        .pg1-feature1-name {
            position: absolute;
            width: 100%;
            padding: 5pt;
            font-size: 16pt;
            text-align: center;
            top: 10pt;
            left: 60pt;
        }
        .pg1-feature1-img {
            width: 230pt;
            height: 350pt;
        }
        .pg1-feature2 {
            position: absolute;
            background-color: lightgreen;
            left: 370pt;
            top: 630pt;
            height: 350pt;
            width: 230pt;
            margin-top: 10pt;
        }
        .pg1-feature2-name {
            position: absolute;
            width: 50%;
            padding: 5pt;
            font-size: 16pt;
            text-align: center;
            top: 10pt;
            left: 0pt;
        }
        .pg1-feature2-img {
            width: 230pt;
            height: 350pt;
        }
        .pg1-feature-price {
            font-weight: bold;
            font-size: 32pt;
        }
        .pg1-box {
            position: absolute;
            background-color: lightgrey;
            height: 170pt;
            width: 170pt;
            margin-top: 10pt;
        }
        .pg1-box-price {
            position: absolute;
            left: 5pt;
            top: 113pt;
            z-index: 1;
            font-weight: bold;
            font-size: 24pt;
        }
        .pg1-box-name {
            position: absolute;
            width: 170pt;
            top: 140pt;
            left: 5pt;
            font-size: 10pt;
        }
        .pg1-box-img {
            position: absolute;
            z-index: 0;
            float: right;
            height: 140pt;
            width: 140pt;
        }
        .pg1-box-mid {
            position: absolute;
            background-color: lightgrey;
            height: 150pt;
            width: 190pt;
            margin-top: 10pt;
        }
        .pg1-box-mid-price {
            position: absolute;
            left: 5pt;
            top: 95pt;
            z-index: 1;
            font-weight: bold;
            font-size: 24pt
        }
        .pg1-box-mid-name {
            position: absolute;
            width: 170pt;
            top: 122pt;
            z-index: 1;
            left: 5pt;
            font-size: 10pt;
        }
        .pg1-box-mid-img {
            position: absolute;
            z-index: 0;
            float: right;
            height: 120pt;
            width: 190pt;
        }
        /* PAGE 2 */
        .pg2-header {
            position: absolute;
            background: lightcoral;
            left: 10pt;
            top: 10pt;
            height: 50pt;
            width: 590pt;
        }
        .pg2-box {
            position: absolute;
            background: lightgrey;
            height: 190pt;
            width: 190pt;
            margin-top: 10pt;
        }
        .pg2-box-price {
            position: absolute;
            left: 5pt;
            top: 133pt;
            z-index: 1;
            font-weight: bold;
            font-size: 24pt;
        }
        .pg2-box-name {
            position: absolute;
            width: 170pt;
            top: 160pt;
            left: 5pt;
            font-size: 10pt;
        }
        .pg2-box-img {
            position: absolute;
            z-index: 0;
            float: right;
            height: 160pt;
            width: 160pt;
        }
        .pg2-sec2-box {
            position: absolute;
            background: lightblue;
            height: 80pt;
            width: 135pt;
            margin-top: 10pt;
        }
        .sec-header {
            position: absolute;
            background: steelblue;
            height: 40pt;
            width: 590pt;
            margin-top: 10pt;
            margin-bottom: 10pt;
        }
        .pg2-sec3-box {
            position: absolute;
            background: lightsteelblue;
            border: 1px solid black;
            height: 150pt;
            width: 196pt;
        }
        .pg2-sec3-box-price {
            position: absolute;
            top: 118pt;
            left: 0pt;
            width: 100%;
            text-align: right;
            padding-right: 10pt;
            z-index: 1;
            font-weight: bold;
            font-size: 24pt;
        }
        .pg2-sec3-name {
            position: absolute;
            border: 1px solid white;
            height: 38pt;
            width: 196pt;
        }
        /* PAGE 3 */
        .pg3-sec1-box {
            position: absolute;
            background: lightsteelblue;
            border: 1px solid black;
            height: 150pt;
            width: 147.5pt;
        }
        .pg3-sec1-name {
            position: absolute;
            border: 1px solid white;
            height: 50pt;
            width: 147.5pt;
        }

    </style>
</head>
<body>

<div class="page-one page-break">

    <!-- HEADER -->

    <div id="pg1-header" class="pg1-header">

        <div id="pg1-header-valid" class="pg1-header-valid">
            Prices Effective {{ date('F jS', strtotime($ad->date_from)) }} to {{ date('F jS', strtotime($ad->date_to)) }}
        </div>

    </div>

    <!-- FEATURE 1 -->

    <div id="pg1-feature1" class="pg1-feature1">

        <img id="pg1-feature1-img" src="" class="pg1-feature1-img" />

        <div id="pg1-feature1-name" class="pg1-feature1-name">

            pg1-feature1-name image dimensions 230pt x 350pt<br />
            <div id="pg1-feature1-price" class="pg1-feature-price">$1.88 lb</div>

        </div>

    </div>

    <div id="pg1-box1" class="pg1-box" style="top: 110pt; left: 250pt;">

        <div id="pg1-box1-price" class="pg1-box-price">

            $8.99 lb

        </div>

        <img id="pg1-box1-img" src="" class="pg1-box-img" />

        <div id="pg1-box1-name" class="pg1-box-name">

            pg1-box1-name image dimensions 140pt x 140pt

        </div>

    </div>
    <div id="pg1-box2" class="pg1-box" style="top: 110pt; left: 430pt;">

        <div id="pg1-box2-price" class="pg1-box-price">

            $10.99 lb

        </div>

        <img id="pg1-box2-img" src="" class="pg1-box-img" />

        <div id="pg1-box2-name" class="pg1-box-name">

            pg1-box2-name image dimensions 140pt x 140pt

        </div>

    </div>

    <div id="pg1-box3" class="pg1-box" style="left: 250pt; top: 290pt;">

        <div id="pg1-box3-price" class="pg1-box-price">

            99¢ lb

        </div>

        <img id="pg1-box3-img" src="" class="pg1-box-img" />

        <div id="pg1-box3-name" class="pg1-box-name">

            pg1-box3-name image dimensions 140pt x 140pt

        </div>

    </div>

    <div id="pg1-box4" class="pg1-box" style="left: 430pt; top: 290pt;">

        <div id="pg1-box4-price" class="pg1-box-price">

            $2.95 ea

        </div>

        <img id="pg1-box4-img" src="" class="pg1-box-img" />

        <div id="pg1-box4-name" class="pg1-box-name">

            pg1-box4-name image dimensions 140pt x 140pt

        </div>

    </div>

    <!-- MIDDLE ROW -->

    <div id="pg1-box5" class="pg1-box-mid" style="left: 10pt; top: 470pt;">

        <div id="pg1-box5-price" class="pg1-box-mid-price">

            $2.47 ea

        </div>

        <img id="pg1-box5-img" src="" class="pg1-box-mid-img" />

        <div id="pg1-box5-name" class="pg1-box-mid-name">

            pg1-box5-name image dimensions 190pt x 120pt

        </div>

    </div>

    <div id="pg1-box6" class="pg1-box-mid" style="left: 210pt; top: 470pt;">

        <div id="pg1-box6-price" class="pg1-box-mid-price">

            $2.98 ea

        </div>

        <img id="pg1-box6-img" src="" class="pg1-box-mid-img" />

        <div id="pg1-box6-name" class="pg1-box-mid-name">

            pg1-box6-name image dimensions 190pt x 120pt

        </div>

    </div>

    <div id="pg1-box7" class="pg1-box-mid" style="left: 410pt; top: 470pt;">

        <div id="pg1-box7-price" class="pg1-box-mid-price">

            75¢ ea

        </div>

        <img id="pg1-box7-img" src="" class="pg1-box-mid-img" />

        <div id="pg1-box7-name" class="pg1-box-mid-name">

            pg1-box7-name image dimensions 190pt x 120pt

        </div>

    </div>

    <!-- END MIDDLE ROW -->

    <div id="pg1-box8" class="pg1-box" style="left: 10pt; top: 630pt; ">

        <div id="pg1-box8-price" class="pg1-box-price">

            98¢ ea

        </div>

        <img id="pg1-box8-img" src="" class="pg1-box-img" />

        <div id="pg1-box8-name" class="pg1-box-name">

            pg1-box8-name image dimensions 140pt x 140pt

        </div>

    </div>

    <div id="pg1-box9" class="pg1-box" style="left: 190pt; top: 630pt; ">

        <div id="pg1-box9-price" class="pg1-box-price">

            97¢ ea

        </div>

        <img id="pg1-box9-img" src="" class="pg1-box-img" />

        <div id="pg1-box9-name" class="pg1-box-name">

            pg1-box9-name image dimensions 140pt x 140pt

        </div>

    </div>

    <!-- FEATURE 2 -->

    <div id="pg1-feature2" class="pg1-feature2">

        <img id="pg1-feature2-img" src="" class="pg1-feature2-img" />

        <div id="pg1-feature2-name" class="pg1-feature2-name">

            pg1-feature2-name image dimensions 230pt x 350pt<br />
            <div id="pg1-feature2-price" class="pg1-feature-price">$.97 lb</div>

        </div>

    </div>

    <div id="pg1-box10" class="pg1-box" style="left: 10pt; top: 810pt;">

        <div id="pg1-box10-price" class="pg1-box-price">

            98¢ ea

        </div>

        <img id="pg1-box10-img" src="" class="pg1-box-img" />

        <div id="pg1-box10-name" class="pg1-box-name">

            pg1-box10-name image dimensions 140pt x 140pt

        </div>

    </div>

    <div id="pg1-box11" class="pg1-box" style="left: 190pt; top: 810pt;">

        <div id="pg1-box11-price" class="pg1-box-price">

            $1.95 ea

        </div>

        <img id="pg1-box11-img" src="" class="pg1-box-img" />

        <div id="pg1-box11-name" class="pg1-box-name">

            pg1-box11-name image dimensions 140pt x 140pt

        </div>

    </div>

</div>

<div class="page-two page-break">

    <!-- HEADER -->

    <div id="pg2-header" class="pg2-header">

        <h1 style="padding-left: 10pt; margin-top: 11pt;"></h1>

    </div>

    <!-- FIRST SECTION -->

    <div id="pg2-box1" class="pg2-box" style="left: 10pt; top: 60pt;">

        <div id="pg2-box1-price" class="pg2-box-price">

            $1.95 ea

        </div>

        <img id="pg2-box1-img" src="" class="pg2-box-img" />

        <div id="pg2-box1-name" class="pg2-box-name">

            pg2-box1-name image dimensions 160pt x 160pt

        </div>

    </div>

    <div id="pg2-box2" class="pg2-box" style="left: 210pt; top: 60pt;">

        <div id="pg2-box1-price" class="pg2-box-price">

            $1.95 ea

        </div>

        <img id="pg2-box2-img" src="" class="pg2-box-img" />

        <div id="pg2-box2-name" class="pg2-box-name">

            pg2-box2-name image dimensions 160pt x 160pt

        </div>

    </div>

    <div id="pg2-box3" class="pg2-box" style="left: 410pt; top: 60pt;">

        <div id="pg2-box3-price" class="pg2-box-price">

            $1.95 ea

        </div>

        <img id="pg2-box3-img" src="" class="pg2-box-img" />

        <div id="pg2-box3-name" class="pg2-box-name">

            pg2-box3-name image dimensions 160pt x 160pt

        </div>

    </div>

    <div id="pg2-box4" class="pg2-box" style="left: 10pt; top: 260pt;">

        <div id="pg2-box4-price" class="pg2-box-price">

            $.95 ea

        </div>

        <img id="pg2-box4-img" src="" class="pg2-box-img" />

        <div id="pg2-box4-name" class="pg2-box-name">

            pg2-box4-name image dimensions 160pt x 160pt

        </div>

    </div>

    <div id="pg2-box5" class="pg2-box" style="left: 210pt; top: 260pt;">

        <div id="pg2-box5-price" class="pg2-box-price">

            $8.95 lb

        </div>

        <img id="pg2-box5-img" src="" class="pg2-box-img" />

        <div id="pg2-box5-name" class="pg2-box-name">

            pg2-box5-name image dimensions 160pt x 160pt

        </div>

    </div>

    <div id="pg2-box6" class="pg2-box" style="left: 410pt; top: 260pt;">

        <div id="pg2-box6-price" class="pg2-box-price">

            $11.95 ea

        </div>

        <img id="pg2-box6-img" src="" class="pg2-box-img" />

        <div id="pg2-box6-name" class="pg2-box-name">

            pg2-box6-name image dimensions 160pt x 160pt

        </div>

    </div>

    <!-- SECOND SECTION -->

    <div id="pg2-sec2-container" style="position: absolute;
        background: steelblue;
        left: 10pt;
        top: 460pt;
        height: 90pt;
        width: 590pt;
        margin-top: 10pt;">

    </div>

    <div id="pg2-sec2-box1" class="pg2-sec2-box" style="left: 40pt; top: 465pt;">

        <img id="pg2-sec2-box1-img" src="" style="height: 80pt; width: 135pt" />

    </div>

    <div id="pg2-sec2-box2" class="pg2-sec2-box" style="left: 180pt; top: 465pt;">

        <img id="pg2-sec2-box2-img" src="" style="height: 80pt; width: 135pt" />

    </div>
    <div id="pg2-sec2-box3" class="pg2-sec2-box" style="left: 320pt; top: 465pt;">

        <img id="pg2-sec2-box3-img" src="" style="height: 80pt; width: 135pt" />

    </div>
    <div id="pg2-sec2-box4" class="pg2-sec2-box" style="left: 460pt; top: 465pt;">

        <img id="pg2-sec2-box4-img" src="" style="height: 80pt; width: 135pt" />

    </div>

    <!-- THIRD SECTION -->

    <div id="pg2-sec3-header" class="sec-header" style="left: 10pt; top: 560pt;">

        <h1 style="padding-left: 10pt; margin-top: 6pt;"></h1>

    </div>

    <div id="pg2-sec3-box1" class="pg2-sec3-box" style="left: 10pt; top: 610pt;">

        <img id="pg2-sec3-box1-img" src="" style="height: 150pt; width: 196pt;" />

        <div id="pg2-sec3-box1-price" class="pg2-sec3-box-price">

            $.95 ea

        </div>

    </div>

    <div id="pg2-sec3-box2" class="pg2-sec3-box" style="left: 206pt; top: 610pt;">

        <img id="pg2-sec3-box2-img" src="" style="height: 150pt; width: 196pt;" />

        <div id="pg2-sec3-box2-price" class="pg2-sec3-box-price">

            $8.98 lb

        </div>

    </div>

    <div id="pg2-sec3-box3" class="pg2-sec3-box" style="left: 402pt; top: 610pt; width: 196.5pt;">

        <img id="pg2-sec3-box3-img" src="" style="height: 150pt; width: 196pt;" />

        <div id="pg2-sec3-box3-price" class="pg2-sec3-box-price">

            $6.95 ea

        </div>

    </div>

    <div id="pg2-sec3-box1-name" class="pg2-sec3-name" style="left: 10pt; top: 763pt;">

        pg2-sec3-box1-name image dimensions 196pt x 150pt

    </div>

    <div id="pg2-sec3-box2-name" class="pg2-sec3-name" style="left: 206pt; top: 763pt;">

        pg2-sec3-box2-name image dimensions 196pt x 150pt

    </div>

    <div id="pg2-sec3-box3-name" class="pg2-sec3-name" style="left: 402pt; top: 763pt; width: 196.5pt;">

        pg2-sec3-box3-name image dimensions 196pt x 150pt

    </div>

    <!-- FOURTH SECTION -->

    <div id="pg2-sec4-box1" style="position: absolute;
        background: #f5f5f5;
        left: 10pt;
        top: 805pt;
        height: 180pt;
        width: 200pt;
        margin-top: 10pt;">

        <img id="pg2-sec4-box1-img" src="" style="height: 180pt; width: 200pt;" />

    </div>

    <div id="pg2-sec4-box2" style="position: absolute;
        background: #f5f5f5;
        left: 220pt;
        top: 805pt;
        height: 180pt;
        width: 380pt;
        margin-top: 10pt;">

        <img id="pg2-sec4-box2-img" src="" style="height: 180pt; width: 380pt;" />

    </div>

</div>

<div class="page-three page-break">

    <!-- FIRST SECTION -->

    <div id="pg3-sec1-header" class="sec-header" style="left: 10pt; top: 10pt;">

        <h1 style="padding-left: 10pt; margin-top: 7pt;"></h1>

    </div>

    <div id="pg3-sec1-box1" class="pg3-sec1-box" style="left: 10pt; top: 50pt;">

        <img id="pg3-sec1-box1-img" src="" style="height: 150pt; width: 147.5pt" />

        <div id="pg3-sec1-box1-price" class="pg2-sec3-box-price">$5 off</div>

    </div>

    <div id="pg3-sec1-box2" class="pg3-sec1-box" style="left: 158pt; top: 50pt;">

        <img id="pg3-sec1-box2-img" src="" style="height: 150pt; width: 147.5pt" />

        <div id="pg3-sec1-box2-price" class="pg2-sec3-box-price">$6.99 lb</div>

    </div>

    <div id="pg3-sec1-box3" class="pg3-sec1-box" style="left: 305pt; top: 50pt;">

        <img id="pg3-sec1-box3-img" src="" style="height: 150pt; width: 147.5pt" />

        <div id="pg3-sec1-box3-price" class="pg2-sec3-box-price">$10.99 lb</div>

    </div>

    <div id="pg3-sec1-box4" class="pg3-sec1-box" style="left: 452pt; top: 50pt; width: 146.5pt;">

        <img id="pg3-sec1-box4-img" src="" style="height: 150pt; width: 146.5pt" />

        <div id="pg3-sec1-box4-price" class="pg2-sec3-box-price">$5.95 ea</div>

    </div>

    <div id="pg3-sec1-box1-name" class="pg3-sec1-name" style="left: 10pt; top: 203pt;">

        pg3-sec1-box1-name image dimensions 147pt x 150pt

    </div>

    <div id="pg3-sec1-box2-name" class="pg3-sec1-name" style="left: 158pt; top: 203pt;">

        pg3-sec1-box2-name image dimensions 147pt x 150pt

    </div>

    <div id="pg3-sec1-box3-name" class="pg3-sec1-name" style="left: 305pt; top: 203pt;">

        pg3-sec1-box3-name image dimensions 147pt x 150pt

    </div>

    <div id="pg3-sec1-box4-name" class="pg3-sec1-name" style="left: 452pt; top: 203pt;">

        pg3-sec1-box4-name image dimensions 147pt x 150pt

    </div>

    <!-- SECOND SECTION -->

    <div id="pg3-sec2-header" class="sec-header" style="left: 10pt; top: 253pt;">

        <h1 style="padding-left: 10pt; margin-top: 7pt;"></h1>

    </div>

    <div id="pg3-sec2-box1" class="pg2-sec3-box" style="left: 10pt; top: 303pt;">

        <img id="pg3-sec2-box1-img" src="" style="width: 100%; height: 100%;" />

        <div id="pg3-sec2-box1-price" class="pg2-sec3-box-price">$5.95 ea</div>

    </div>

    <div id="pg3-sec2-box2" class="pg2-sec3-box" style="left: 206pt; top: 303pt;">

        <img id="pg3-sec2-box2-img" src="" style="width: 100%; height: 100%;" />

        <div id="pg3-sec2-box2-price" class="pg2-sec3-box-price">$5.95 ea</div>

    </div>

    <div id="pg3-sec2-box3" class="pg2-sec3-box" style="left: 402pt; top: 303pt; width: 196.5pt;">

        <img id="pg3-sec2-box3-img" src="" style="width: 100%; height: 100%;" />

        <div id="pg3-sec2-box3-price" class="pg2-sec3-box-price">$5.95 ea</div>

    </div>

    <div id="pg3-sec2-box1-name" class="pg2-sec3-name" style="left: 10pt; top: 456pt;">

        pg3-sec2-box1-name image dimensions 196pt x 150pt

    </div>

    <div id="pg3-sec2-box2-name" class="pg2-sec3-name" style="left: 206pt; top: 456pt;">

        pg3-sec2-box2-name image dimensions 196pt x 150pt

    </div>

    <div id="pg3-sec2-box3-name" class="pg2-sec3-name" style="left: 402pt; top: 456pt; width: 196.5pt;">

        pg3-sec2-box3-name image dimensions 196pt x 150pt

    </div>

    <!-- THIRD SECTION -->

    <div id="pg3-sec3-header" class="sec-header" style="left: 10pt; top: 508pt;">

        <h1 style="padding-left: 10pt; margin-top: 7pt;"></h1>

    </div>

    <div id="pg3-sec3-box1" class="pg2-sec3-box" style="left: 10pt; top: 558pt;">

        <img id="pg3-sec3-box1-img" src="" style="width: 100%; height: 100%;" />

        <div id="pg3-sec3-box1-price" class="pg2-sec3-box-price">$5.95 ea</div>

    </div>

    <div id="pg3-sec3-box2" class="pg2-sec3-box" style="left: 206pt; top: 558pt;">

        <img id="pg3-sec3-box2-img" src="" style="width: 100%; height: 100%;" />

        <div id="pg3-sec3-box2-price" class="pg2-sec3-box-price">$5.95 ea</div>

    </div>

    <div id="pg3-sec3-box3" class="pg2-sec3-box" style="left: 402pt; top: 558pt; width: 196.5pt;">

        <img id="pg3-sec3-box3-img" src="" style="width: 100%; height: 100%;" />

        <div id="pg3-sec3-box3-price" class="pg2-sec3-box-price">$5.95 ea</div>

    </div>

    <div id="pg3-sec3-box1-name" class="pg2-sec3-name" style="left: 10pt; top: 711pt;">

        pg3-sec3-box1-name image dimensions 196pt x 150pt

    </div>

    <div id="pg3-sec3-box2-name" class="pg2-sec3-name" style="left: 206pt; top: 711pt;">

        pg3-sec3-box2-name image dimensions 196pt x 150pt

    </div>

    <div id="pg3-sec3-box3-name" class="pg2-sec3-name" style="left: 402pt; top: 711pt; width: 196.5pt;">

        pg3-sec3-box3-name image dimensions 196pt x 150pt

    </div>

    <!-- FOURTH SECTION -->

    <div id="pg3-sec4-box1" style="position: absolute;
        background: #f5f5f5;
        left: 10pt;
        top: 763pt;
        height: 220pt;
        width: 380pt;
        margin-top: 10pt;">

        <img id="pg3-sec4-box1-img" src="" style="width: 100%; height: 100%;" />

    </div>

    <div id="pg3-sec4-box2" style="position: absolute;
        background: #f5f5f5;
        left: 400pt;
        top: 763pt;
        height: 220pt;
        width: 200pt;
        margin-top: 10pt;">

        <img id="pg3-sec4-box2-img" src="" style="width: 100%; height: 100%;" />

    </div>

</div>

<div class="page-four">

    <!-- HEADER -->

    <div id="pg4-header" class="pg2-header">

        <h1 style="padding-left: 10pt; margin-top: 11pt;"></h1>

    </div>

    <!-- FIRST ROW -->

    <div id="pg4-box1" class="pg2-box" style="left: 10pt; top: 60pt;">

        <div id="pg4-box1-price" class="pg2-box-price">

            $11.95 ea

        </div>

        <img id="pg4-box1-img" src="" class="pg2-box-img" />

        <div id="pg4-box1-name" class="pg2-box-name">

            pg4-box1-name image dimensions 160pt x 160pt

        </div>

    </div>

    <div id="pg4-box2" class="pg2-box" style="left: 210pt; top: 60pt;">

        <div id="pg4-box2-price" class="pg2-box-price">

            $11.95 ea

        </div>

        <img id="pg4-box2-img" src="" class="pg2-box-img" />

        <div id="pg4-box2-name" class="pg2-box-name">

            pg4-box2-name image dimensions 160pt x 160pt

        </div>

    </div>

    <div id="pg4-box3" class="pg2-box" style="left: 410pt; top: 60pt;">

        <div id="pg4-box3-price" class="pg2-box-price">

            $11.95 ea

        </div>

        <img id="pg4-box3-img" src="" class="pg2-box-img" />

        <div id="pg4-box3-name" class="pg2-box-name">

            pg4-box3-name image dimensions 160pt x 160pt

        </div>

    </div>

    <!-- SECOND ROW -->

    <div id="pg4-box4" class="pg2-box" style="left: 10pt; top: 260pt;">

        <div id="pg4-box4-price" class="pg2-box-price">

            $11.95 ea

        </div>

        <img id="pg4-box4-img" src="" class="pg2-box-img" />

        <div id="pg4-box4-name" class="pg2-box-name">

            pg4-box4-name image dimensions 160pt x 160pt

        </div>

    </div>

    <div id="pg4-box5" class="pg2-box" style="left: 210pt; top: 260pt;">

        <div id="pg4-box5-price" class="pg2-box-price">

            $11.95 ea

        </div>

        <img id="pg4-box5-img" src="" class="pg2-box-img" />

        <div id="pg4-box5-name" class="pg2-box-name">

            pg4-box5-name image dimensions 160pt x 160pt

        </div>

    </div>

    <div id="pg4-box6" class="pg2-box" style="left: 410pt; top: 260pt;">

        <div id="pg4-box6-price" class="pg2-box-price">

            $11.95 ea

        </div>

        <img id="pg4-box6-img" src="" class="pg2-box-img" />

        <div id="pg4-box6-name" class="pg2-box-name">

            pg4-box6-name image dimensions 160pt x 160pt

        </div>

    </div>

    <!-- THIRD ROW -->

    <div id="pg4-box7" class="pg2-box" style="left: 10pt; top: 460pt;">

        <div id="pg4-box7-price" class="pg2-box-price">

            $11.95 ea

        </div>

        <img id="pg4-box7-img" src="" class="pg2-box-img" />

        <div id="pg4-box7-name" class="pg2-box-name">

            pg4-box7-name image dimensions 160pt x 160pt

        </div>

    </div>

    <div id="pg4-box8" class="pg2-box" style="left: 210pt; top: 460pt;">

        <div id="pg4-box8-price" class="pg2-box-price">

            $11.95 ea

        </div>

        <img id="pg4-box8-img" src="" class="pg2-box-img" />

        <div id="pg4-box8-name" class="pg2-box-name">

            pg4-box8-name image dimensions 160pt x 160pt

        </div>

    </div>

    <div id="pg4-box9" class="pg2-box" style="left: 410pt; top: 460pt;">

        <div id="pg4-box9-price" class="pg2-box-price">

            $11.95 ea

        </div>

        <img id="pg4-box9-img" src="" class="pg2-box-img" />

        <div id="pg4-box9-name" class="pg2-box-name">

            pg4-box9-name image dimensions 160pt x 160pt

        </div>

    </div>

    <!-- THIRD ROW -->

    <div id="pg4-box10" class="pg2-box" style="left: 10pt; top: 660pt;">

        <div id="pg4-box10-price" class="pg2-box-price">

            $11.95 ea

        </div>

        <img id="pg4-box10-img" src="" class="pg2-box-img" />

        <div id="pg4-box10-name" class="pg2-box-name">

            pg4-box10-name image dimensions 160pt x 160pt

        </div>

    </div>

    <div id="pg4-box11" class="pg2-box" style="left: 210pt; top: 660pt;">

        <div id="pg4-box11-price" class="pg2-box-price">

            $11.95 ea

        </div>

        <img id="pg4-box11-img" src="" class="pg2-box-img" />

        <div id="pg4-box11-name" class="pg2-box-name">

            pg4-box11-name image dimensions 160pt x 160pt

        </div>

    </div>

    <div id="pg4-box12" class="pg2-box" style="left: 410pt; top: 660pt;">

        <div id="pg4-box12-price" class="pg2-box-price">

            $11.95 ea

        </div>

        <img id="pg4-box12-img" src="" class="pg2-box-img" />

        <div id="pg4-box12-name" class="pg2-box-name">

            pg4-box12-name image dimensions 160pt x 160pt

        </div>

    </div>

    <!-- FOURTH ROW -->

    <div id="pg4-footer" style="position: absolute;
        background: #eee;
        left: 10pt;
        top: 860pt;
        height: 125pt;
        width: 590pt;
        margin-top: 10pt;">

        <img id="pg4-footer-img" src="" style="height: 100%; width: 100%;" />

    </div>

</div>

</body>
</html>