<html>
<head>
    <style>
        @font-face {
            font-family: 'Sigmar One';
            src: url("/fonts/SigmarOne-Regular.ttf") format("truetype");
        }
        body{
            margin: 0;
            padding: 0;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        }
        .poster {
            width: 1536px;
            height: 1954px;
            background-image: url('/img/posters/sm-orange.jpeg');
        }

        .p2 {
            position: absolute;
            width: 1000px;
            height: 1545px;
            left: 270px;
            top: 250px;
        }

        .p2 .inner-container {
            position: relative;
            min-height: 1545px;
            min-width: 1000px;
        }

        .p2 .inner-container .grocery {
            position: absolute;
            left: 90px;
            top: 90px;
        }

        .p2 .inner-container .grocery .item {
            background-color: white;
            border-radius: 10px;
            width: 155px;
            height: 134px;
            float: left;
            margin-right: 10px;
            margin-bottom: 20px;
        }

        .p2 .inner-container .grocery .item .item-container {
            min-width: 203px;
            min-height: 180px;
            position: relative;
        }

        .p2 .inner-container .grocery .item .item-container .item-description {
            top: 10px;
            right: 55px;
            position: absolute;
            text-align: right;
            font-size: 9px;
        }

        .p2 .inner-container .grocery .item .item-container .item-description span {
            text-transform: uppercase;
        }

        .p2 .inner-container .grocery .item .item-container .price {
            position: absolute;
            right: 55px;
            bottom: 40px;
            text-align: right;
            font-size: 24px;
            line-height: 1;
        }

        .p2 .inner-container .grocery .item .item-container .price span {
            font-size: 9px;
            text-transform: uppercase;
        }

        .p2 .inner-container .meat {
            position: absolute;
            left: 90px;
            top: 470px;
        }

        .p2 .inner-container .meat .item {
            background-color: white;
            border-radius: 10px;
            width: 192px;
            height: 120px;
            float: left;
            margin-right: 10px;
            margin-bottom: 10px;
        }

        .p2 .inner-container .meat .item .item-container {
            min-width: 255px;
            min-height: 180px;
            position: relative;
        }

        .p2 .inner-container .meat .item .item-container .item-description {
            top: 10px;
            right: 70px;
            position: absolute;
            text-align: right;
            font-size: 9px;
        }

        .p2 .inner-container .meat .item .item-container .item-description span {
            text-transform: uppercase;
        }

        .p2 .inner-container .meat .item .item-container .price {
            position: absolute;
            right: 65px;
            bottom: 60px;
            text-align: right;
            font-size: 24px;
            line-height: 1;
        }

        .p2 .inner-container .meat .item .item-container .price span {
            font-size: 10px;
            text-transform: uppercase;
        }

        .p2 .inner-container .meat .featured {
            width: 260px;
        }

        .p2 .inner-container .meat .featured .item-description {
            right: 10px !important;
        }

        .p2 .inner-container .meat .featured .price {
            right: 0 !important;
        }

        .p2 .inner-container .produce {
            position: absolute;
            left: 100px;
            top: 765px;
        }

        .p2 .inner-container .produce .item {
            background-color: white;
            border-radius: 10px;
            width: 190px;
            height: 120px;
            float: left;
            margin-right: 10px;
            margin-bottom: 10px;
        }

        .p2 .inner-container .produce .item .item-container {
            min-width: 255px;
            min-height: 180px;
            position: relative;
        }

        .p2 .inner-container .produce .item .item-container .item-description {
            top: 10px;
            right: 70px;
            position: absolute;
            text-align: right;
            font-size: 10px;
        }

        .p2 .inner-container .produce .item .item-container .item-description span {
            text-transform: uppercase;
        }

        .p2 .inner-container .produce .item .item-container .price {
            position: absolute;
            right: 70px;
            bottom: 60px;
            text-align: right;
            font-size: 24px;
            line-height: 1;
        }

        .p2 .inner-container .produce .item .item-container .price span {
            font-size: 12px;
            text-transform: uppercase;
        }

        .p2 .inner-container .produce .featured {
            width: 343px;
        }

        .p2 .inner-container .bakery {
            position: absolute;
            left: 80px;
            top: 940px;
        }

        .p2 .inner-container .bakery .item {
            background-color: white;
            border-radius: 10px;
            width: 200px;
            height: 125px;
            float: left;
            margin-right: 10px;
            margin-bottom: 10px;
        }

        .p2 .inner-container .bakery .item .item-container {
            min-width: 255px;
            min-height: 180px;
            position: relative;
        }

        .p2 .inner-container .bakery .item .item-container .item-description {
            top: 10px;
            right: 65px;
            position: absolute;
            text-align: right;
            font-size: 10px;
        }

        .p2 .inner-container .bakery .item .item-container .item-description span {
            text-transform: uppercase;
        }

        .p2 .inner-container .bakery .item .item-container .price {
            position: absolute;
            right: 65px;
            bottom: 60px;
            text-align: right;
            font-size: 24px;
            line-height: 1;
        }

        .p2 .inner-container .bakery .item .item-container .price span {
            font-size: 12px;
            text-transform: uppercase;
        }

        .p2 .inner-container .bakery .featured {
            width: 343px;
        }

        .p2 .inner-container .deli {
            position: absolute;
            left: 80px;
            top: 1100px;
        }

        .p2 .inner-container .deli .item {
            background-color: white;
            border-radius: 10px;
            width: 200px;
            height: 125px;
            float: left;
            margin-right: 10px;
            margin-bottom: 10px;
        }

        .p2 .inner-container .deli .item .item-container {
            min-width: 255px;
            min-height: 180px;
            position: relative;
        }

        .p2 .inner-container .deli .item .item-container .item-description {
            top: 10px;
            right: 65px;
            position: absolute;
            text-align: right;
            font-size: 10px;
        }

        .p2 .inner-container .deli .item .item-container .item-description span {
            text-transform: uppercase;
        }

        .p2 .inner-container .deli .item .item-container .price {
            position: absolute;
            right: 65px;
            bottom: 60px;
            text-align: right;
            font-size: 24px;
            line-height: 1;
        }

        .p2 .inner-container .deli .item .item-container .price span {
            font-size: 12px;
            text-transform: uppercase;
        }

        .p2 .inner-container .deli .featured {
            width: 343px;
        }

        .p2 .inner-container .hbc {
            position: absolute;
            left: 75px;
            top: 1260px;
        }

        .p2 .inner-container .hbc .item {
            background-color: white;
            background-position: left;
            border-radius: 10px;
            width: 273px;
            height: 105px;
            float: left;
            margin-right: 10px;
            margin-bottom: 10px;
        }

        .p2 .inner-container .hbc .item .item-container {
            min-width: 255px;
            min-height: 155px;
            position: relative;
        }

        .p2 .inner-container .hbc .item .item-container .item-description {
            top: 10px;
            right: 10px;
            position: absolute;
            text-align: right;
            font-size: 12px;
            max-width: 55%;
        }

        .p2 .inner-container .hbc .item .item-container .item-description span {
            text-transform: uppercase;
        }

        .p2 .inner-container .hbc .item .item-container .price {
            position: absolute;
            right: 10px;
            bottom: 45px;
            text-align: right;
            font-size: 24px;
            line-height: 1;
        }

        .p2 .inner-container .hbc .item .item-container .price span {
            font-size: 12px;
            text-transform: uppercase;
        }

        .p2 .inner-container .hbc .featured {
            width: 343px;
        }

        .item {
            background-repeat: no-repeat;
            background-size: contain;
            background-position: center bottom;
            background-color: white;
        }

        .item-description {
            line-height: 1;
            font-weight: bold;
            text-shadow: white 3px 0px 0px, white 2.83487px 0.981584px 0px, white 2.35766px 1.85511px 0px, white 1.62091px 2.52441px 0px, white 0.705713px 2.91581px 0px, white -0.287171px 2.98622px 0px, white -1.24844px 2.72789px 0px, white -2.07227px 2.16926px 0px, white -2.66798px 1.37182px 0px, white -2.96998px 0.42336px 0px, white -2.94502px -0.571704px 0px, white -2.59586px -1.50383px 0px, white -1.96093px -2.27041px 0px, white -1.11013px -2.78704px 0px, white -0.137119px -2.99686px 0px, white 0.850987px -2.87677px 0px, white 1.74541px -2.43999px 0px, white 2.44769px -1.73459px 0px, white 2.88051px -0.838247px 0px;
            text-transform: capitalize;
        }

        .item-description span {
            text-transform: uppercase;
        }

        .size {
            font-weight: normal;
            text-transform: uppercase;
        }

        .item-container {
            position: relative;
        }

        .inner-container {
            position: relative;
        }

        .price {
            font-weight: bold;
            padding: 5px;
            text-shadow: white 3px 0px 0px, white 2.83487px 0.981584px 0px, white 2.35766px 1.85511px 0px, white 1.62091px 2.52441px 0px, white 0.705713px 2.91581px 0px, white -0.287171px 2.98622px 0px, white -1.24844px 2.72789px 0px, white -2.07227px 2.16926px 0px, white -2.66798px 1.37182px 0px, white -2.96998px 0.42336px 0px, white -2.94502px -0.571704px 0px, white -2.59586px -1.50383px 0px, white -1.96093px -2.27041px 0px, white -1.11013px -2.78704px 0px, white -0.137119px -2.99686px 0px, white 0.850987px -2.87677px 0px, white 1.74541px -2.43999px 0px, white 2.44769px -1.73459px 0px, white 2.88051px -0.838247px 0px;
            color: #C52033;
            letter-spacing: -3px;
        }

        .price span {
            letter-spacing: normal;
        }

        .dates {
            width: 100%;
            text-align: center;
            position: absolute;
            bottom: 30px;
        }

        .bfl-tab {
            z-index: 1000;
            height: 1954px;
            width: 1536px;
            position: absolute;
            top: 0;
            left: 0;
        }
    </style>
</head>
<body>
<div class="poster">
    <img src="/img/posters/sm-tab2.png" class="bfl-tab" />
    <div class="p2" style="background: url(/img/bg-back-supermercado.png); background-size: contain">
        <div class="inner-container">
            <div class="grocery">
                @foreach($ad->specials->whereIn('position_number', [7,8,9,10,11,12,13,14,15,16]) as $special)
                    <div class="item" style="background-image: url({{ $special->image }})">
                        <div class="item-container">
                            <div class="item-description">
                                {!! strtolower($special->item_description) !!}
                            </div>
                            <div class="price">
                                {{ $special->price }}<br /><span>{{ strtolower($special->size) }}</span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="meat">
                @php $i = 0 @endphp
                @foreach($ad->specials->whereIn('position_number', [17,18,19,20,21,22,23]) as $special)
                    <div class="item @if($i < 3) featured @endif" style="background-image: url({{ $special->image }})">
                        <div class="item-container">
                            <div class="item-description">
                                {!! strtolower($special->item_description) !!}
                            </div>
                            <div class="price">
                                {{ $special->price }}<br /><span>{{ strtolower($special->size) }}</span>
                            </div>
                        </div>
                    </div>
                    @php $i++ @endphp
                @endforeach
            </div>
            <div class="produce">
                @foreach($ad->specials->whereIn('position_number', [24,25,26,27]) as $special)
                    <div class="item" style="background-image: url({{ $special->image }})">
                        <div class="item-container">
                            <div class="item-description">
                                {!! strtolower($special->item_description) !!}
                            </div>
                            <div class="price">
                                {{ $special->price }}<br /><span>{{ strtolower($special->size) }}</span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="bakery">
                @foreach($ad->specials->whereIn('position_number', [28,29,30,31]) as $special)
                    <div class="item" style="background-image: url({{ $special->image }})">
                        <div class="item-container">
                            <div class="item-description">
                                {!! strtolower($special->item_description) !!}
                            </div>
                            <div class="price">
                                {{ $special->price }}<br /><span>{{ strtolower($special->size) }}</span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="deli">
                @foreach($ad->specials->whereIn('position_number', [32,33,34,35]) as $special)
                    <div class="item" style="background-image: url({{ $special->image }})">
                        <div class="item-container">
                            <div class="item-description">
                                {!! strtolower($special->item_description) !!}
                            </div>
                            <div class="price">
                                {{ $special->price }}<br /><span>{{ strtolower($special->size) }}</span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="hbc">
                @foreach($ad->specials->whereIn('position_number', [36,37,38,39,40,41]) as $special)
                    <div class="item" style="background-image: url({{ $special->image }})">
                        <div class="item-container">
                            <div class="item-description">
                                {!! strtolower($special->item_description) !!}
                            </div>
                            <div class="price">
                                {{ $special->price }}<br /><span>{{ strtolower($special->size) }}</span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="dates">
                Prices effective {{ date('F d, Y', strtotime($ad->date_from)) }} to {{ date('F d, Y', strtotime($ad->date_to)) }}
            </div>
        </div>
    </div>
</div>
</body>
</html>