<html>
<head>
    <style>
        body {
            margin: 0;
            padding: 0;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            text-transform: capitalize;
            letter-spacing: -1px;
        }

        h1 {
            text-transform: capitalize;
        }

        span {
            text-transform: uppercase;
        }

        .poster {
            background-image: url('/img/posters/up-blue.jpg');
            width: 1536px;
            height: 1954px;
            overflow: hidden;
        }

        .p4 {
            width: 989px;
            height: 1280px;
            position: absolute;
            left: 280px;
            top: 387px;
            background: white;
        }

        .p4 .container {
            position: relative;
        }

        .p4 .container .inner-container {
            position: absolute;
            top: 30px;
            left: 30px;
            width: 930px;
            height: 1240px;
        }

        .p4 .container .inner-container .item {
            width: 228px;
            height: 175px;
            float: left;
        }

        .p4 .container .inner-container .item .item-container {
            position: relative;
            min-height: 175px;
        }

        .p4 .container .inner-container .item .item-container .item-description {
            position: absolute;
            left: 20px;
            top: 20px;
            font-size: 21px;
        }

        .p4 .container .inner-container .item .item-container .item-description .size {
            font-size: 11px;
        }

        .p4 .container .inner-container .item .item-container .price {
            position: absolute;
            bottom: 20px;
            right: 20px;
            font-size: 24px;
            text-align: right;
            line-height: 1;
        }

        .p4 .container .inner-container .item .item-container .price span {
            font-size: 12px;
            text-transform: uppercase;
        }

        .p4 .container .inner-container .featured {
            width: 246px;
            height: 350px;
            float: left;
        }

        .p4 .container .inner-container .featured .item-container {
            height: 350px;
        }

        .p4 .container .inner-container .featured .item-container .item-description {
            font-size: 24px;
        }

        .p4 .container .inner-container .featured .item-container .item-description .size {
            font-size: 16px;
        }

        .p4 .container .inner-container .featured .item-container .price {
            font-size: 36px;
        }

        .p4 .container .blue {
            background-color: #017299;
            width: 900px;
            height: 140px;
            position: absolute;
            top: 390px;
            left: 30px;
            padding: 15px;
        }

        .p4 .container .blue .item {
            width: 448px;
            height: 140px;
            float: left;
            background-position: top left;
        }

        .p4 .container .blue .item .item-container {
            min-height: 140px;
        }

        .p4 .container .blue .item .item-container .item-description {
            position: absolute;
            font-size: 21px;
            right: 10px;
            top: 10px;
            text-align: right;
        }

        .p4 .container .blue .item .item-container .item-description .size {
            font-size: 12px;
        }

        .p4 .container .blue .item .item-container .price {
            position: absolute;
            bottom: 15px;
            right: 15px;
            font-size: 36px;
            line-height: 1;
            text-align: right;
        }

        .p4 .container .blue .item .item-container .price span {
            text-transform: uppercase;
            font-size: 12px;
        }

        .p4 .container .blue .side {
            border-right: solid 2px #017299;
        }

        .p4 .container .bakery {
            position: absolute;
            left: 30px;
            top: 570px;
            width: 930px;
        }

        .p4 .container .bakery .title {
            background: #017299;
            padding: 15px;
            color: white;
            font-weight: bold;
            text-transform: uppercase;
        }

        .p4 .container .bakery .item {
            width: 310px;
            height: 240px;
            float: left;
            background-size: cover;
            background-position: center top;
        }

        .p4 .container .bakery .item .item-container {
            min-height: 240px;
        }

        .p4 .container .bakery .item .item-container .item-description {
            position: absolute;
            bottom: 10px;
            left: 0px;
            padding: 10px;
            width: 290px;
            text-align: center;
        }

        .p4 .container .bakery .item .item-container .price {
            position: absolute;
            top: 15px;
            right: 15px;
            font-size: 32px;
            text-align: right;
            line-height: 1;
        }

        .p4 .container .bakery .item .item-container .price span {
            text-transform: uppercase;
            font-size: 12px;
        }

        .p4 .container .cheese {
            position: absolute;
            left: 30px;
            top: 860px;
            width: 930px;
        }

        .p4 .container .cheese .title {
            background: #017299;
            padding: 15px;
            color: white;
            font-weight: bold;
            text-transform: uppercase;
        }

        .p4 .container .cheese .item {
            width: 310px;
            height: 240px;
            float: left;
            background-size: cover;
            background-position: center top;
        }

        .p4 .container .cheese .item .item-container {
            min-height: 240px;
        }

        .p4 .container .cheese .item .item-container .item-description {
            position: absolute;
            bottom: 10px;
            left: 0px;
            padding: 10px;
            width: 290px;
            text-align: center;
        }

        .p4 .container .cheese .item .item-container .price {
            position: absolute;
            top: 15px;
            right: 15px;
            font-size: 32px;
            text-align: right;
            line-height: 1;
        }

        .p4 .container .cheese .item .item-container .price span {
            text-transform: uppercase;
            font-size: 12px;
        }

        .p4 .container .footer {
            width: 930px;
            height: 80px;
            position: absolute;
            left: 30px;
            top: 1145px;
            text-transform: uppercase;
            font-size: 11px;
            letter-spacing: 0;
            text-align: center;
        }

        .p4 .container .footer .disclaimer {
            padding-bottom: 10px;
            border-bottom: solid 1px black;
            margin-bottom: 10px;
            margin-top: 10px;
        }

        .p4 .container .footer .apple-pay {
            width: 60px;
            float: left;
            margin-right: 10px;
        }

        .p4 .container .footer .apple-pay img {
            max-width: 100%;
        }

        .p4 .container .footer .android-pay {
            float: left;
            margin-right: 10px;
        }

        .p4 .container .footer .android-pay img {
            max-width: 100%;
            max-height: 38px;
        }

        .p4 .container .footer .facebook-logo {
            float: left;
            min-width: 240px;
            border: solid 1px #cccccc;
            border-radius: 5px;
            height: 38px;
        }

        .p4 .container .footer .facebook-logo img {
            max-height: 28px;
            padding: 5px;
        }

        .p4 .container .footer .store {
            float: left;
            margin-left: 100px;
        }

        .item {
            background-repeat: no-repeat;
            background-size: contain;
            background-position: center bottom;
            background-color: white;
        }

        .item-description {
            line-height: 1;
            font-weight: bold;
            text-shadow: white 3px 0px 0px, white 2.83487px 0.981584px 0px, white 2.35766px 1.85511px 0px, white 1.62091px 2.52441px 0px, white 0.705713px 2.91581px 0px, white -0.287171px 2.98622px 0px, white -1.24844px 2.72789px 0px, white -2.07227px 2.16926px 0px, white -2.66798px 1.37182px 0px, white -2.96998px 0.42336px 0px, white -2.94502px -0.571704px 0px, white -2.59586px -1.50383px 0px, white -1.96093px -2.27041px 0px, white -1.11013px -2.78704px 0px, white -0.137119px -2.99686px 0px, white 0.850987px -2.87677px 0px, white 1.74541px -2.43999px 0px, white 2.44769px -1.73459px 0px, white 2.88051px -0.838247px 0px;
        }

        .size {
            font-weight: normal;
            text-transform: uppercase;
        }

        .item-container {
            position: relative;
        }

        .inner-container {
            position: relative;
        }

        .price {
            font-weight: bold;
            background-color: #C1D82F;
            border-radius: 0 20px 0 20px;
            border: solid 2px #000000;
            padding: 5px;
        }

        .tab-overlay {
            z-index: 1000;
        }

    </style>
</head>
<body>
<div class="poster">
    <img src="/img/posters/up-tab2.png" class="tab-overlay" />
    <div class="p4">
        <div class="container">
            <div class="inner-container">
                @php $i = 0; @endphp
                @foreach($ad->specials->whereIn('position_number', [41,42,43,44,45,46,47]) as $special)
                    <div class="@if( $i == 0 ) featured @endif item" style="background-image: url({{ $special->image }})">
                        <div class="item-container">
                            <div class="item-description">
                                {!! strtolower($special->item_description) !!}
                            </div>
                            <div class="price">
                                {{ $special->price }}<br /><span>{{ strtolower($special->size) }}</span>
                            </div>
                        </div>
                    </div>
                    @php $i++ @endphp
                @endforeach

            </div>

            <div class="blue" @if( $ad->banners->first() ) style="background: url({{ $ad->banners->first()->file }})" @endif>
                @php $i=0; @endphp
                @foreach($ad->specials->whereIn('position_number', [48,49]) as $special)
                    <div class="item @if($i == 0) side @endif" style="background-image: url({{ $special->image }})">
                        <div class="item-container">
                            <div class="item-description">
                                {!! strtolower($special->item_description) !!}
                            </div>
                            <div class="price">
                                {{ $special->price }}<br /><span>{{ strtolower($special->size) }}</span>
                            </div>
                        </div>
                    </div>
                    @php $i++ @endphp
                @endforeach
            </div>

            <div class="bakery">
                <div class="title">
                    Bakery
                </div>
                @foreach($ad->specials->whereIn('position_number', [50,51,52]) as $special)
                    <div class="item" style="background-image: url({{ $special->image }})">
                        <div class="item-container">
                            <div class="item-description">
                                {!! strtolower($special->item_description) !!}
                            </div>
                            <div class="price">
                                {{ $special->price }}<br /><span>{{ strtolower($special->size) }}</span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="cheese">
                <div class="title">
                    Cheese
                </div>
                @foreach($ad->specials->whereIn('position_number', [53,54,55]) as $special)
                    <div class="item" style="background-image: url({{ $special->image }})">
                        <div class="item-container">
                            <div class="item-description">
                                {!! strtolower($special->item_description) !!}
                            </div>
                            <div class="price">
                                {{ $special->price }}<br/><span>{{ strtolower($special->size) }}</span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="footer">
                <div class="disclaimer">
                    We gladly accept WIC Checks, Oklahoma Access, Manufacturer Coupons. We reserve the right to limit quantities. All limits are per family. Not liable for typographical or pictorial errors.
                </div>
                <div class="apple-pay">
                    <img src="/img/Apple_Pay_Mark_RGB_052318.svg" alt="Apple Pay Logo">
                </div>
                <div class="android-pay">
                    <img src="/img/android-pay.png" alt="Android Pay Logo">
                </div>
                <div class="facebook-logo">
                    <img src="/img/FindUs-FB-RGB-BRC-Site-500.svg" alt="Facebook Badge"><br /><br />
                    Effective {{ date('F d, Y', strtotime($ad->date_from)) }} to {{ date('F d, Y', strtotime($ad->date_to)) }}
                </div>
                @foreach($ad->brands->stores as $store)
                    <div class="store">
                        <b>{{ $store->name }}</b><br />
                        {{ $store->address }}<br />
                        {{ $store->phone }}<br />
                        Open Daily {{ $store->store_hours }}
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
</body>
</html>