<html>
<head>
    <style>
        body {
            margin: 0;
            padding: 0;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            text-transform: capitalize;
            letter-spacing: -1px;
        }

        h1 {
            text-transform: capitalize;
        }

        span {
            text-transform: uppercase;
        }

        .poster {
            background-image: url('/img/posters/up-aqua.jpg');
            width: 1536px;
            height: 1954px;
            overflow: hidden;
        }

        .p2 {
            width: 989px;
            height: 1280px;
            position: absolute;
            left: 280px;
            top: 387px;
            background: white;
        }

        .p2 .inner-container {
            width: 930px;
            margin: 0 auto;
            padding-top: 30px;
        }

        .p2 .inner-container .meat {
            margin-bottom: 10px;
            float: left;
        }

        .p2 .inner-container .meat .title {
            background: #c5e1e4;
            font-weight: bold;
            text-transform: uppercase;
            padding: 15px;
        }

        .p2 .inner-container .meat .title span {
            color: #C41230;
            text-transform: capitalize;
            margin-left: 40px;
            font-family: 'CooperBTforWFM', 'Times New Roman', Times, serif;
            font-style: italic;
            font-size: 1.5em;
        }

        .p2 .inner-container .meat .item {
            width: 310px;
            float: left;
            height: 180px;
        }

        .p2 .inner-container .meat .item .item-container {
            min-height: 180px;
        }

        .p2 .inner-container .meat .item .item-container .item-description {
            position: absolute;
            top: 10px;
            left: 10px;
        }

        .p2 .inner-container .meat .item .item-container .item-description .size {
            font-size: .7em;
        }

        .p2 .inner-container .meat .item .item-container .price {
            position: absolute;
            bottom: 10px;
            right: 10px;
            font-size: 32px;
            text-align: right;
            line-height: 1;
        }

        .p2 .inner-container .meat .item .item-container .price span {
            text-transform: uppercase;
            font-size: 12px;
        }

        .p2 .inner-container .deli {
            margin-top: 10px;
            float: left;
        }

        .p2 .inner-container .deli .title {
            background: #017299;
            font-weight: bold;
            text-transform: uppercase;
            padding: 15px;
            color: white;
        }

        .p2 .inner-container .deli .title span {
            color: #C41230;
            background: white;
            border: solid 1px black;
            border-radius: 4px;
            padding: 5px;
            text-transform: capitalize;
            margin-left: 40px;
            font-family: 'CooperBTforWFM', 'Times New Roman', Times, serif;
            font-size: 1em;
            -webkit-transform: skewX(25deg);
            transform: skewX(25deg);
        }

        .p2 .inner-container .deli .item {
            width: 232.5px;
            float: left;
            height: 200px;
            margin-bottom: 50px;
        }

        .p2 .inner-container .deli .item .item-container {
            min-height: 190px;
        }

        .p2 .inner-container .deli .item .item-container .item-description {
            position: absolute;
            bottom: -60px;
            left: 10px;
            vertical-align: top;
        }

        .p2 .inner-container .deli .item .item-container .item-description .size {
            font-size: .7em;
        }

        .p2 .inner-container .deli .item .item-container .price {
            position: absolute;
            right: 10px;
            top: 10px;
            font-size: 32px;
            text-align: right;
            line-height: 1;
        }

        .p2 .inner-container .deli .item .item-container .price span {
            font-size: 12px;
            text-transform: uppercase;
        }

        .p2 .inner-container .sushi {
            margin-top: 10px;
            float: left;
            width: 930px;
        }

        .p2 .inner-container .sushi .title {
            background: #017299;
            font-weight: bold;
            text-transform: uppercase;
            padding: 15px;
            color: white;
        }

        .p2 .inner-container .sushi .title span {
            color: #C41230;
            background: white;
            border: solid 1px black;
            border-radius: 4px;
            padding: 5px;
            text-transform: capitalize;
            margin-left: 40px;
            font-family: 'CooperBTforWFM', 'Times New Roman', Times, serif;
            font-size: 1em;
            -webkit-transform: skewX(25deg);
            transform: skewX(25deg);
        }

        .p2 .inner-container .sushi .item {
            width: 310px;
            float: left;
            height: 220px;
            margin-bottom: 15px;
            background-size: cover;
            background-position: top center;
        }

        .p2 .inner-container .sushi .item .item-container {
            min-height: 220px;
        }

        .p2 .inner-container .sushi .item .item-container .item-description {
            position: absolute;
            bottom: 10px;
            left: 10px;
            vertical-align: top;
        }

        .p2 .inner-container .sushi .item .item-container .item-description .size {
            font-size: .7em;
        }

        .p2 .inner-container .sushi .item .item-container .price {
            position: absolute;
            right: 10px;
            top: 10px;
            font-size: 32px;
            line-height: 1;
            text-align: right;
        }

        .p2 .inner-container .sushi .item .item-container .price span {
            text-transform: uppercase;
            font-size: 12px;
        }

        .p2 .inner-container .banners {
            width: 930px;
            float: left;
            height: 190px;
            background: #f5f5f5;
            border: solid 1px #cccccc;
        }

        .item {
            background-repeat: no-repeat;
            background-size: contain;
            background-position: center bottom;
            background-color: white;
        }

        .item-description {
            line-height: 1;
            font-weight: bold;
            text-shadow: white 3px 0px 0px, white 2.83487px 0.981584px 0px, white 2.35766px 1.85511px 0px, white 1.62091px 2.52441px 0px, white 0.705713px 2.91581px 0px, white -0.287171px 2.98622px 0px, white -1.24844px 2.72789px 0px, white -2.07227px 2.16926px 0px, white -2.66798px 1.37182px 0px, white -2.96998px 0.42336px 0px, white -2.94502px -0.571704px 0px, white -2.59586px -1.50383px 0px, white -1.96093px -2.27041px 0px, white -1.11013px -2.78704px 0px, white -0.137119px -2.99686px 0px, white 0.850987px -2.87677px 0px, white 1.74541px -2.43999px 0px, white 2.44769px -1.73459px 0px, white 2.88051px -0.838247px 0px;
        }

        .size {
            font-weight: normal;
            text-transform: uppercase;
        }

        .item-container {
            position: relative;
        }

        .inner-container {
            position: relative;
        }

        .price {
            font-weight: bold;
            background-color: #C1D82F;
            border-radius: 0 20px 0 20px;
            border: solid 2px #000000;
            padding: 5px;
        }

        .tab-overlay {
            z-index: 1000;
        }

    </style>
</head>
<body>
<div class="poster">
    <img src="/img/posters/up-tab2.png" class="tab-overlay" />
    <div class="p2">
        <div class="inner-container">
            <div class="meat">
                <div class="title">Meat & Seafood <span>Save Time & Try Our Meat Grilling Station!</span></div>
                @foreach($ad->specials->whereIn('position_number', [10,11,12,13,14,15]) as $special)
                    <div class="item" style="background-image: url({{ $special->image }})">
                        <div class="item-container">
                            <div class="item-description">
                                {!! strtolower($special->item_description) !!}
                            </div>
                            <div class="price">
                                {{ $special->price }}<br /><span>{{ $special->size }}</span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="deli">
                <div class="title">Deli <span>Chef Prepared</span></div>
                @foreach($ad->specials->whereIn('position_number', [16,17,18,19]) as $special)
                    <div class="item" style="background-image: url({{ $special->image }}); background-size: cover;">
                        <div class="item-container">
                            <div class="item-description">
                                {!! strtolower($special->item_description) !!}
                            </div>
                            <div class="price">
                                {{ $special->price }}<br /><span>{{ $special->size }}</span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="sushi">
                <div class="title">Sushi <span>Visit Our Sushi Chefs or Order Online at sushi.uptowngroceryco.com</span></div>
                @foreach($ad->specials->whereIn('position_number', [20,21,22]) as $special)
                    <div class="item" style="background-image: url({{ $special->image }})">
                        <div class="item-container">
                            <div class="item-description">
                                {!! strtolower($special->item_description) !!}
                            </div>
                            <div class="price">
                                {{ $special->price }}<br /><span>{{ strtolower($special->size) }}</span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="banners" @if( count($ad->banners) > 1 ) style="background: url({{ $ad->banners->last()->file }})" @endif></div>
        </div>
    </div>
</div>
</body>
</html>