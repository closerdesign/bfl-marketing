<html>
<head>
    <style>
        body{
            margin: 0;
            padding: 0;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        }
        h1{
            text-transform: capitalize;
        }
        span{
            text-transform: uppercase;
        }
        .poster {
            width: 1536px;
            height: 1954px;
            background-image: url('/img/posters/bfl-bg.jpeg');
            overflow: hidden;
        }
        .container{
            position: absolute;
            width: 1112px;
            height: 723px;
            top: 117px;
            left: 317px;
        }
        .header{
            height: 235px;
            width: 1111px;
            margin-left: 2px;
        }
        .header > img{
            width: 300px;
            margin-left: 490px;
        }
        .background{
            background-repeat: no-repeat;
            background-size: contain;
            background-position: center bottom;
            background-color: white;
        }
        .block{
            background-color: #c52033;
            width: 1000px;
            height: 445px;
            margin: 0 auto;
            border-radius: 10px;
            margin-bottom: 20px;
        }
        .block > .items{
            width: 1000px;
            height: 405px;
            margin-left: 12px;
            padding-top: 20px !important;
        }
        .items > .item{
            width: 318px;
            height: 375px;
            border-radius: 10px;
            float: left;
            margin-right: 10px;
            position: relative;
            background-color: white;
            background-size: contain;
        }
        .item > h1{
            background: rgba(255,255,255,0.8);
            padding: 10px;
            font-size: 1.3em;
            text-align: center;
            line-height: 1;
        }
        .item > h2{
            position: absolute;
            bottom: -35px;
            right: 5px;
            font-size: 6em;
            line-height: 1;
            text-align: right;
            margin: 0;
            -webkit-text-stroke-width: 3px;
            -webkit-text-stroke-color: #c52033;
            color: #fff200;
        }

        .item > h3{
            position: absolute;
            bottom: -45px;
            right: 5px;
            font-size: 3em;
            line-height: 1;
            text-align: right;
            margin: 0;
            -webkit-text-stroke-width: 1px;
            -webkit-text-stroke-color: #c52033;
            color: #fff200;
        }

        .item > h2 > span{
            text-transform: uppercase;
            font-size: .2em;
            -webkit-text-stroke-width: 0px;
            color: white;
        }
        .footer{
            width: 1114px;
            margin: 0 auto;

        }
        .footer > .black{
            background: black;
            color: white;
            text-align: center;
            text-transform: uppercase;
            padding: 5px;
            margin-bottom: 3px;
        }
        .black > span{
            color: #fff200;
            font-weight: bold;
        }
        .footer > .red{
            background: #c52033;
            padding: 5px;
            margin-bottom: 3px;
            color: white;
            text-align: center;
            text-transform: uppercase;
        }
        .red > span{
            background: #fff200;
            color: #c52033;
            padding: 3px;
            margin: 0 15px 0 15px;
            font-weight: bold;
            padding-left: 15px;
            padding-right: 15px;
            border-radius: 3px;
        }
        .footer > .small{
            text-align: center;
            font-size: .5em;
        }
        .p2{
            position: relative;
        }
        .p2 > .red{
            width: 1140px;
            height: 770px;
            background: #c52033;
            border-radius: 10px;
            position: absolute;
            left: 70px;
            top: 70px;
        }
        .inner-red{
            position: relative;
            width: 1140px;
            height: 200px;
        }
        .inner-red > .items{
            width: 1070px;
            height: 220px;
            background: white;
            border-radius: 10px;
            position: absolute;
            left: 35px;
            margin: 0 auto;
        }
        .items1{
            top: 35px;
        }
        .items2{
            top: 275px;
        }
        .items3{
            top: 515px;
        }
        .inner-red > .items > .title{
            position: absolute;
            left: 15px;
            top: -18px;
            color: #fff200;
            text-transform: uppercase;
            font-weight: bold;
        }
        .inner-red > .items > .item{
            width: 160px;
            height: 200px;
            margin: 0px 5px 0 10px;
        }
        .inner-red > .items > .item > h1{
            font-size: .7em;
            margin: 0;
        }
        .inner-red > .items > .item > h2{
            font-size: 2.4em;
            -webkit-text-stroke-width: 1px;
            bottom: -15px;
            right: 0;
        }
        .inner-red > .items > .item > h2 > span{
            -webkit-text-stroke-width: 0px;
            color: black;
        }
        .middle-block{
            position: absolute;
            top: 860px;
            left: 70px;
            width: 1140px;
            height: 680px;
        }
        .inner-mb{
            position: relative;
            width: 1140px;
            margin: 0 auto;
        }
        .inner-mb > .items {
        }
        .inner-mb > .items > .item{
            width: 262px;
            height: 160px;
            margin: 0px 0 5px 0;
            border-top: solid 5px #c52033;
            border-radius: 0;
        }
        .inner-mb > .items > .background{
            background-position: left bottom;
        }
        .inner-mb > .items > .item > h1{
            font-size: .9em;
            width: 100% !important;
            padding: 5px;
            margin: 0;
        }
        .inner-mb > .items > .item > h2{
            font-size: 2.4em;
            -webkit-text-stroke-width: 1px;
            bottom: 0px;
            right: 0;
        }
        .inner-mb > .items > .item > h2 > span{
            -webkit-text-stroke-width: 0px;
            color: black;
        }
        .ft{
            width: 1140px;
            height: 380px;
            border: solid 1px #f5f5f5;
            position: absolute;
            left: 70px;
            top: 1560px;
        }
        .ft-inner{
            position: relative;
            width: 1140px;
            height: 380px;
        }
        .ft-inner > .items > .item{
            width: 285px;
            height: 190px;
            margin: 0;
        }
        .ft-inner > .items > .item > h1{
            font-size: .9em;
            width: 100% !important;
            padding: 5px;
            margin: 0;
        }
        .ft-inner > .items > .item > h2{
            font-size: 3em;
            -webkit-text-stroke-width: 2px;
            bottom: 5px;
            right: 5px;
        }
        .ft-inner > .items > .item > h2 > span{
            -webkit-text-stroke-width: 0px;
            color: black;
            background: white;
            border-radius: 2px;
            padding: 1px 5px 1px 5px;
            margin-right: 5px;
            font-size: .1em;
        }
        .ft-inner > .items > .background{
            background-size: cover;
            background-position: center center;
        }
        .ft-inner > .items > .item > .title{
            width: 100%;
            background: #c52033;
            color: #ffffff;
            text-align: center;
            text-transform: uppercase;
            font-size: .7em;
            font-weight: bold;
            padding-top: 5px;
            padding-bottom: 5px;
        }
        .produce{
            width: 250px !important;
        }
        .bfl-tab {
            z-index: 1000;
            height: 1955px;
            width: 1536px;
        }
    </style>
</head>
<body>
<div class="poster">
    <img src="/img/posters/bfl-tab.png" class="bfl-tab" />
    <div class="container">
        @if($ad->dotw_image != '')
            <div class="dotw" style="position: absolute; z-index: 1000; top: 5px; left: 568px;">
                <img src="https://bfl-corp-sara.s3.us-west-2.amazonaws.com/ads/dotw-images/{{$ad->dotw_image}}" height="177" width="493" />
            </div>
            <div class="header" style="background: url('/img/dotw/bfl-dotw-header.jpg'); background-size: cover;">
                {{--<img src="{{ $ad->brands->logo }}" alt="">--}}
            </div>
        @else
            <div class="header" @if( $ad->header_image != "" ) style="background: url({{ $ad->header_image }}); background-size: cover;" @endif>
                {{--<img src="{{ $ad->brands->logo }}" alt="">--}}
            </div>
        @endif
        <div class="block">
            <div class="items">
                @foreach($ad->specials->whereIn('position_number', [1,2,3]) as $special)
                    <div class="item background" style="background-image: url({{ $special->image }});">
                        <h1>{!! strtolower($special->item_description) !!}</h1>
                        @if( $special->alt_price_format != true )
                            <h2>{{ $special->price }}<br /><span>{{ $special->size }}</span></h2>
                        @else
                            <h3>{{ $special->price }}</h3>
                        @endif
                    </div>
                @endforeach
            </div>
        </div>
        <div class="block">
            <div class="items">
                @foreach($ad->specials->whereIn('position_number', [4,5,6]) as $special)
                    <div class="item background" style="background-image: url({{ $special->image }});">
                        <h1>{!! strtolower($special->item_description) !!}</h1>
                        @if( $special->alt_price_format != true )
                            <h2>{{ $special->price }}<br /><span>{{ $special->size }}</span></h2>
                        @else
                            <h3>{{ $special->price }}</h3>
                        @endif
                    </div>
                @endforeach
            </div>
        </div>
        <div class="block">
            <div class="items">
                @foreach($ad->specials->whereIn('position_number', [7,8,9]) as $special)
                    <div class="item background" style="background-image: url({{ $special->image }});">
                        <h1>{!! strtolower($special->item_description) !!}</h1>
                        @if( $special->alt_price_format != true )
                            <h2>{{ $special->price }}<br /><span>{{ $special->size }}</span></h2>
                        @else
                            <h3>{{ $special->price }}</h3>
                        @endif
                    </div>
                @endforeach
            </div>
        </div>
        <div class="footer">
            <div class="black">
                Can't find what you want in our store? Send a text message to <span>405-896-7933</span> to request the product you need!
            </div>
            <div class="red">
                Prices effective: {{ $ad->date_from }} - {{ $ad->date_to }} <span>Money Orders: 69¢</span> 3501 NW Expressway, OKC - 7957 NW 23rd St., Bethany
            </div>
            <div class="small">
                We gladly accept WIC Checks, Oklahoma Access, Manufacturer Coupons. We reserve the right to limit quantities. All limits are per family. Not liable for typographical or pictorial errors. Some items featured only available at this location.
            </div>
        </div>
    </div>
</div>
</body>
</html>