<html>
<head>
    <style>
        body {
            margin: 0;
            padding: 0;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            text-transform: capitalize;
            letter-spacing: -1px;
        }

        h1 {
            text-transform: capitalize;
        }

        span {
            text-transform: uppercase;
        }

        .poster {
            background-image: url('/img/posters/up-green.jpg');
            width: 1536px;
            height: 1954px;
            overflow: hidden;
        }

        .p1 {
            width: 989px;
            height: 1280px;
            position: absolute;
            left: 280px;
            top: 387px;
            background: white;
        }

        .p1 .header {
            width: 989px;
            height: 150px;
            background: url(/img/uptown-ad-header.jpg) no-repeat;
        }

        .p1 .featured-block {
            width: 930px;
            height: 570px;
            position: absolute;
            left: 20px;
            top: 165px;
            border-bottom: solid 1px black;
        }

        .p1 .featured-block .featured-content {
            position: relative;
            width: 989px;
            height: 570px;
        }

        .p1 .featured-block .featured-content .featured-item {
            position: absolute;
            width: 620px;
            height: 570px;
        }

        .p1 .featured-block .featured-content .featured-item .item-container {
            min-height: 570px;
        }

        .p1 .featured-block .featured-content .featured-item .item-container .item-description {
            position: absolute;
            left: 10px;
            top: 10px;
            max-width: 100%;
            font-size: 48px;
        }

        .p1 .featured-block .featured-content .featured-item .item-container .item-description .size {
            font-weight: normal;
        }

        .p1 .featured-block .featured-content .featured-item .item-container .price {
            position: absolute;
            right: 15px;
            bottom: 15px;
            font-size: 96px;
            text-align: right;
            line-height: 1;
        }

        .p1 .featured-block .featured-content .featured-item .item-container .price span {
            font-size: 24px;
            text-transform: uppercase;
        }

        .p1 .featured-block .featured-content .right-col {
            position: absolute;
            left: 640px;
            top: 0;
            width: 290px;
            min-height: 570px;
            background: #f5f5f5;
        }

        .p1 .featured-block .featured-content .right-col .right-col-container {
            position: relative;
        }

        .p1 .featured-block .featured-content .right-col .right-col-container .right-col-item .item-container .item-description {
            height: 285px;
            font-size: 24px;
            text-align: right;
        }

        .p1 .featured-block .featured-content .right-col .right-col-container .right-col-item .item-container .item-description .size {
            font-size: 18px;
        }

        .p1 .featured-block .featured-content .right-col .right-col-container .right-col-item .item-container .price {
            position: absolute;
            right: 10px;
            bottom: 10px;
            font-size: 36px;
            text-align: right;
            line-height: 1;
        }

        .p1 .featured-block .featured-content .right-col .right-col-container .right-col-item .item-container .price span {
            text-transform: uppercase;
            font-size: 12px;
        }

        .p1 .front-footer {
            position: absolute;
            left: 20px;
            top: 780px;
            width: 930px;
            height: 480px;
        }

        .p1 .front-footer .front-footer-content {
            position: relative;
        }

        .p1 .front-footer .front-footer-content .front-footer-item {
            width: 300px;
            height: 240px;
            float: left;
            padding-right: 10px;
        }

        .p1 .front-footer .front-footer-content .front-footer-item .item-container {
            position: relative;
            min-height: 240px;
        }

        .p1 .front-footer .front-footer-content .front-footer-item .item-container .item-description {
            position: absolute;
            left: 10px;
            top: 10px;
        }

        .p1 .front-footer .front-footer-content .front-footer-item .item-container .price {
            position: absolute;
            bottom: 10px;
            right: 10px;
            font-size: 36px;
            text-align: right;
            line-height: 1;
        }

        .p1 .front-footer .front-footer-content .front-footer-item .item-container .price span {
            text-transform: uppercase;
            font-size: 11px;
        }

        .item {
            background-repeat: no-repeat;
            background-size: contain;
            background-position: center bottom;
            background-color: white;
        }

        .item-description {
            line-height: 1;
            font-weight: bold;
            text-shadow: white 3px 0px 0px, white 2.83487px 0.981584px 0px, white 2.35766px 1.85511px 0px, white 1.62091px 2.52441px 0px, white 0.705713px 2.91581px 0px, white -0.287171px 2.98622px 0px, white -1.24844px 2.72789px 0px, white -2.07227px 2.16926px 0px, white -2.66798px 1.37182px 0px, white -2.96998px 0.42336px 0px, white -2.94502px -0.571704px 0px, white -2.59586px -1.50383px 0px, white -1.96093px -2.27041px 0px, white -1.11013px -2.78704px 0px, white -0.137119px -2.99686px 0px, white 0.850987px -2.87677px 0px, white 1.74541px -2.43999px 0px, white 2.44769px -1.73459px 0px, white 2.88051px -0.838247px 0px;
        }

        .size {
            font-weight: normal;
            text-transform: uppercase;
        }

        .item-container {
            position: relative;
        }

        .inner-container {
            position: relative;
        }

        .price {
            font-weight: bold;
            background-color: #C1D82F;
            border-radius: 0 20px 0 20px;
            border: solid 2px #000000;
            padding: 5px;
        }

        .tab-overlay {
            z-index: 1000;
        }

        .dotw {
            position: absolute;
            z-index: 1000;
            top: -23px;
            left: 20px;
        }

    </style>
</head>
<body>
<div class="poster">
    <img src="/img/posters/up-tab.png" class="tab-overlay" />
    <div class="p1">
        <div style="text-align: @if($ad->dotw_image != '') right @else center @endif; position: relative; top: 0; font-size: 14px;">
            Prices effective {{ date('F d, Y', strtotime($ad->date_from)) }} to {{ date('F d, Y', strtotime($ad->date_to)) }} &nbsp;
        </div>
        @if($ad->dotw_image != '')
            <div class="dotw">
                <img src="https://bfl-corp-sara.s3.us-west-2.amazonaws.com/ads/dotw-images/{{$ad->dotw_image}}" width="500" height="180"/>
            </div>
        @endif
        <div class="header">

        </div>
        <div class="featured-block">
            <div class="featured-content">

                @foreach($ad->specials->whereIn('position_number', [1]) as $special)
                    <div class="featured-item item" style="background-image: url({{ $special->image }})">
                        <div class="item-container">
                            <div class="item-description">
                                {!! strtolower($special->item_description) !!}
                            </div>
                            <div class="price">
                                {{ $special->price }}<span><br />{{ $special->size }}</span>
                            </div>
                        </div>
                    </div>
                @endforeach

                <div class="right-col">

                    <div class="right-col-container">
                        @foreach($ad->specials->whereIn('position_number', [2,3]) as $special)
                            <div class="right-col-item item" style="background-image: url({{ $special->image }})">
                                <div class="item-container">
                                    <div class="item-description">
                                        {!! strtolower($special->item_description) !!}
                                    </div>
                                    <div class="price">
                                        {{ $special->price }}<span><br />{{ strtolower($special->size) }}</span>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                </div>
            </div>
        </div>
        <div class="front-footer">
            <div class="front-footer-content">
                @foreach($ad->specials->whereIn('position_number', [4,5,6,7,8,9]) as $special)
                    <div class="front-footer-item item" style="background-image: url({{ $special->image }})">
                        <div class="item-container">
                            <div class="item-description">
                                {!! strtolower($special->item_description) !!}
                            </div>
                            <div class="price">
                                {{ $special->price }}<br /><span>{{ $special->size }}</span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
</body>
</html>