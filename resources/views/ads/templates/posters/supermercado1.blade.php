<html>
<head>
    <style>
        @font-face {
            font-family: 'Sigmar One';
            src: url("/fonts/SigmarOne-Regular.ttf") format("truetype");
        }
        body{
            margin: 0;
            padding: 0;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        }
        .poster {
            width: 1536px;
            height: 1954px;
            background-image: url('/img/posters/sm-green.jpeg');
        }

        .p1 {
            position: absolute;
            width: 1000px;
            height: 1545px;
            left: 270px;
            top: 250px;
        }

        .p1 .inner-container {
            position: relative;
            min-height: 1545px;
            min-width: 1000px;
        }

        .p1 .inner-container .produce {
            position: absolute;
            top: 285px;
            left: 110px;
        }

        .p1 .inner-container .produce .item {
            background-color: white;
            border-radius: 10px;
            width: 385px;
            height: 272px;
            float: left;
            margin-right: 20px;
        }

        .p1 .inner-container .produce .item .item-container {
            position: relative;
            min-height: 375px;
        }

        .p1 .inner-container .produce .item .item-container .item-description {
            position: absolute;
            top: 15px;
            right: 15px;
            font-size: 20px;
            text-align: right;
        }

        .p1 .inner-container .produce .item .item-container .item-description span {
            text-transform: uppercase;
            text-align: right;
        }

        .p1 .inner-container .produce .item .item-container .price {
            font-family: 'Sigmar One', 'Arial', 'sans-serif';
            position: absolute;
            bottom: 100px;
            right: 15px;
            text-align: right;
            font-size: 65px;
            line-height: 1;
        }

        .p1 .inner-container .produce .item .item-container .price span {
            text-transform: uppercase;
            font-size: 21px;
        }

        .p1 .inner-container .meat {
            position: absolute;
            top: 675px;
            left: 95px;
        }

        .p1 .inner-container .meat .item {
            background-color: white;
            border-radius: 10px;
            width: 385px;
            height: 272px;
            float: left;
            margin-right: 20px;
        }

        .p1 .inner-container .meat .item .item-container {
            position: relative;
            min-height: 375px;
        }

        .p1 .inner-container .meat .item .item-container .item-description {
            position: absolute;
            top: 15px;
            right: 15px;
            font-size: 20px;
            text-align: right;
        }

        .p1 .inner-container .meat .item .item-container .item-description span {
            text-transform: uppercase;
        }

        .p1 .inner-container .meat .item .item-container .price {
            font-family: 'Sigmar One', 'Arial', 'sans-serif';
            position: absolute;
            bottom: 100px;
            right: 15px;
            text-align: right;
            font-size: 65px;
            line-height: 1;
        }

        .p1 .inner-container .meat .item .item-container .price span {
            text-transform: uppercase;
            font-size: 21px;
        }

        .p1 .inner-container .grocery {
            position: absolute;
            top: 1060px;
            left: 95px;
        }

        .p1 .inner-container .grocery .item {
            background-color: white;
            border-radius: 10px;
            width: 385px;
            height: 272px;
            float: left;
            margin-right: 20px;
        }

        .p1 .inner-container .grocery .item .item-container {
            position: relative;
            min-height: 375px;
        }

        .p1 .inner-container .grocery .item .item-container .item-description {
            position: absolute;
            top: 15px;
            right: 15px;
            font-size: 20px;
            text-align: right;
        }

        .p1 .inner-container .grocery .item .item-container .item-description span {
            text-transform: uppercase;
        }

        .p1 .inner-container .grocery .item .item-container .price {
            font-family: 'Sigmar One', 'Arial', 'sans-serif';
            position: absolute;
            bottom: 100px;
            right: 15px;
            text-align: right;
            font-size: 65px;
            line-height: 1;
        }

        .p1 .inner-container .grocery .item .item-container .price span {
            text-transform: uppercase;
            font-size: 21px;
        }

        .item {
            background-repeat: no-repeat;
            background-size: contain;
            background-position: center bottom;
            background-color: white;
        }

        .item-description {
            line-height: 1;
            font-weight: bold;
            text-shadow: white 3px 0px 0px, white 2.83487px 0.981584px 0px, white 2.35766px 1.85511px 0px, white 1.62091px 2.52441px 0px, white 0.705713px 2.91581px 0px, white -0.287171px 2.98622px 0px, white -1.24844px 2.72789px 0px, white -2.07227px 2.16926px 0px, white -2.66798px 1.37182px 0px, white -2.96998px 0.42336px 0px, white -2.94502px -0.571704px 0px, white -2.59586px -1.50383px 0px, white -1.96093px -2.27041px 0px, white -1.11013px -2.78704px 0px, white -0.137119px -2.99686px 0px, white 0.850987px -2.87677px 0px, white 1.74541px -2.43999px 0px, white 2.44769px -1.73459px 0px, white 2.88051px -0.838247px 0px;
            text-transform: capitalize;
        }

        .item-description span {
            text-transform: uppercase;
        }

        .size {
            font-weight: normal;
            text-transform: uppercase;
        }

        .item-container {
            position: relative;
        }

        .inner-container {
            position: relative;
        }

        .price {
            font-weight: bold;
            padding: 5px;
            text-shadow: white 3px 0px 0px, white 2.83487px 0.981584px 0px, white 2.35766px 1.85511px 0px, white 1.62091px 2.52441px 0px, white 0.705713px 2.91581px 0px, white -0.287171px 2.98622px 0px, white -1.24844px 2.72789px 0px, white -2.07227px 2.16926px 0px, white -2.66798px 1.37182px 0px, white -2.96998px 0.42336px 0px, white -2.94502px -0.571704px 0px, white -2.59586px -1.50383px 0px, white -1.96093px -2.27041px 0px, white -1.11013px -2.78704px 0px, white -0.137119px -2.99686px 0px, white 0.850987px -2.87677px 0px, white 1.74541px -2.43999px 0px, white 2.44769px -1.73459px 0px, white 2.88051px -0.838247px 0px;
            color: #C52033;
            letter-spacing: -3px;
        }

        .price span {
            letter-spacing: normal;
        }

        .dates {
            width: 100%;
            text-align: center;
            position: absolute;
            bottom: 50px;
            font-size: 12px;
        }
        .bfl-tab {
            z-index: 1000;
            height: 1954px;
            width: 1536px;
            position: absolute;
            top: 0;
            left: 0;
        }
        .dotw {
            position: absolute;
            top: 48px;
            left: 450px;
        }
    </style>
</head>
<body>
<div class="poster">
    <img src="/img/posters/sm-tab1.png" class="bfl-tab" />
    @if( $ad->header_image != "" )
        <div class="container p1" style="background: url({{ $ad->header_image }}); background-size: contain">
    @else
        @if( $ad->dotw_image != "" )
            <div class="container p1" style="background: url(/img/dotw/sm-dotw-bg.jpg); background-size: contain">
        @else
            <div class="container p1" style="background: url(/img/bg-front-supermercado.png); background-size: contain">
        @endif
    @endif
        <div class="inner-container">
            @if( $ad->dotw_image != "" )
                <div class="dotw">
                    <img src="https://bfl-corp-sara.s3.us-west-2.amazonaws.com/ads/dotw-images/{{$ad->dotw_image}}" height="171" width="495" />
                </div>
            @endif
            <div class="produce">
                @foreach($ad->specials->whereIn('position_number', [1,2]) as $special)
                    <div class="item" style="background-image: url({{ $special->image }})">
                        <div class="item-container">
                            <div class="item-description">
                                {!! strtolower($special->item_description) !!}
                            </div>
                            <div class="price">
                                {{ $special->price }}<br /><span>{{ strtolower($special->size) }}</span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="meat">
                @foreach($ad->specials->whereIn('position_number', [3,4]) as $special)
                    <div class="item" style="background-image: url({{ $special->image }})">
                        <div class="item-container">
                            <div class="item-description">
                                {!! strtolower($special->item_description) !!}
                            </div>
                            <div class="price">
                                {{ $special->price }}<br /><span>{{ strtolower($special->size) }}</span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="grocery">
                @foreach($ad->specials->whereIn('position_number', [5,6]) as $special)
                    <div class="item" style="background-image: url({{ $special->image }})">
                        <div class="item-container">
                            <div class="item-description">
                                {!! strtolower($special->item_description) !!}
                            </div>
                            <div class="price">
                                {{ $special->price }}<br /><span>{{ strtolower($special->size) }}</span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="dates">
                Prices effective {{ date('F d, Y', strtotime($ad->date_from)) }} to {{ date('F d, Y', strtotime($ad->date_to)) }}
            </div>
        </div>
    </div>
</div>
</body>
</html>