<html>
<head>
    <style>
        body {
            margin: 0;
            padding: 0;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            text-transform: capitalize;
            letter-spacing: -1px;
        }

        h1 {
            text-transform: capitalize;
        }

        span {
            text-transform: uppercase;
        }

        .poster {
            background-image: url('/img/posters/up-purple.jpg');
            width: 1536px;
            height: 1954px;
            overflow: hidden;
        }

        .p3 {
            width: 989px;
            height: 1280px;
            position: absolute;
            left: 280px;
            top: 387px;
            background: white;
        }

        .p3 .inner-container {
            width: 930px;
            margin: 0 auto;
            padding-top: 30px;
        }

        .p3 .inner-container .grocery {
            margin-bottom: 10px;
            float: left;
        }

        .p3 .inner-container .grocery .title {
            background: #C41230;
            font-weight: bold;
            text-transform: uppercase;
            padding: 15px;
            color: #ffffff;
        }

        .p3 .inner-container .grocery .title span {
            color: white;
            text-transform: capitalize;
            margin-left: 40px;
            font-family: 'CooperBTforWFM', 'Times New Roman', Times, serif;
            font-style: italic;
            font-size: 1.5em;
        }

        .p3 .inner-container .grocery .item {
            width: 232.5px;
            float: left;
            height: 210px;
            margin-bottom: 10px;
        }

        .p3 .inner-container .grocery .item .item-container {
            min-height: 210px;
        }

        .p3 .inner-container .grocery .item .item-container .item-description {
            position: absolute;
            top: 10px;
            left: 10px;
        }

        .p3 .inner-container .grocery .item .item-container .item-description .size {
            font-size: .7em;
        }

        .p3 .inner-container .grocery .item .item-container .price {
            position: absolute;
            right: 10px;
            bottom: 10px;
            font-size: 24px;
            text-align: right;
            line-height: 1;
        }

        .p3 .inner-container .grocery .item .item-container .price span {
            text-transform: uppercase;
            font-size: 12px;
        }

        .p3 .inner-container .hbc {
            margin-top: 10px;
            float: left;
        }

        .p3 .inner-container .hbc .title {
            background: #9d3293;
            font-weight: bold;
            text-transform: uppercase;
            padding: 15px;
            color: white;
        }

        .p3 .inner-container .hbc .title span {
            color: #C41230;
            background: white;
            border: solid 1px black;
            border-radius: 4px;
            padding: 5px;
            text-transform: capitalize;
            margin-left: 40px;
            font-family: 'CooperBTforWFM', 'Times New Roman', Times, serif;
            font-size: 1em;
            -webkit-transform: skewX(25deg);
            transform: skewX(25deg);
        }

        .p3 .inner-container .hbc .item {
            width: 310px;
            float: left;
            height: 180px;
            margin-bottom: 50px;
            background-position: top left;
        }

        .p3 .inner-container .hbc .item .item-container {
            min-height: 180px;
        }

        .p3 .inner-container .hbc .item .item-container .item-description {
            position: absolute;
            top: 10px;
            right: 10px;
            vertical-align: top;
            text-align: right;
        }

        .p3 .inner-container .hbc .item .item-container .item-description .size {
            font-size: .7em;
        }

        .p3 .inner-container .hbc .item .item-container .price {
            position: absolute;
            right: 10px;
            bottom: 10px;
            font-size: 32px;
            text-align: right;
            line-height: 1;
        }

        .p3 .inner-container .hbc .item .item-container .price span {
            text-transform: uppercase;
            font-size: 12px;
        }

        .item {
            background-repeat: no-repeat;
            background-size: contain;
            background-position: center bottom;
            background-color: white;
        }

        .item-description {
            line-height: 1;
            font-weight: bold;
            text-shadow: white 3px 0px 0px, white 2.83487px 0.981584px 0px, white 2.35766px 1.85511px 0px, white 1.62091px 2.52441px 0px, white 0.705713px 2.91581px 0px, white -0.287171px 2.98622px 0px, white -1.24844px 2.72789px 0px, white -2.07227px 2.16926px 0px, white -2.66798px 1.37182px 0px, white -2.96998px 0.42336px 0px, white -2.94502px -0.571704px 0px, white -2.59586px -1.50383px 0px, white -1.96093px -2.27041px 0px, white -1.11013px -2.78704px 0px, white -0.137119px -2.99686px 0px, white 0.850987px -2.87677px 0px, white 1.74541px -2.43999px 0px, white 2.44769px -1.73459px 0px, white 2.88051px -0.838247px 0px;
        }

        .size {
            font-weight: normal;
            text-transform: uppercase;
        }

        .item-container {
            position: relative;
        }

        .inner-container {
            position: relative;
        }

        .price {
            font-weight: bold;
            background-color: #C1D82F;
            border-radius: 0 20px 0 20px;
            border: solid 2px #000000;
            padding: 5px;
        }

        .tab-overlay {
            z-index: 1000;
        }

    </style>
</head>
<body>
<div class="poster">
    <img src="/img/posters/up-tab2.png" class="tab-overlay" />
    <div class="p3">
        <div class="inner-container">
            <div class="grocery">
                <div class="title">Grocery <span>Oklahoma Local with Low Low Prices!</span></div>
                @foreach($ad->specials->whereIn('position_number', [23,24,25,26,27,28,29,30,31,32,33,34]) as $special)
                    <div class="item" style="background-image: url({{ $special->image }})">
                        <div class="item-container">
                            <div class="item-description">
                                {!! strtolower($special->item_description) !!}
                            </div>
                            <div class="price">
                                {{ $special->price }}<br /><span>{{ strtolower($special->size) }}</span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="hbc">
                <div class="title">Health, Beauty & Care</div>
                @foreach($ad->specials->whereIn('position_number', [35,36,37,38,39,40]) as $special)
                    <div class="item" style="background-image: url({{ $special->image }})">
                        <div class="item-container">
                            <div class="item-description">
                                {!! strtolower($special->item_description) !!}
                            </div>
                            <div class="price">
                                {{ $special->price }}<br /><span>{{ strtolower($special->size) }}</span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>
    </div>
</div>
</body>
</html>