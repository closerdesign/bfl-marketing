<html>
<head>
    <style>
        @font-face {
            font-family: 'Bowlby One SC';
            src: url("/fonts/bowlby-one-v10-latin-regular.woff2") format("woff2"), url("/fonts/bowlby-one-v10-latin-regular.woff") format("woff"), url("/fonts/bowlby-one-v10-latin-regular.ttf") format("truetype");
        }

        body {
            margin: 0;
            padding: 0;
            font-family: Arial, sans-serif;
            font-weight: bold;
        }

        h1 {
            text-transform: capitalize;
        }

        .poster {
            width: 1536px;
            height: 1954px;
            background-image: url('/img/posters/ss-bg.jpg');
            overflow: hidden;
        }

        .container {
            position: absolute;
            width: 1125px;
            height: 1740px;
            left: 310px;
            top: 105px;
            overflow: hidden;
        }

        .header {
            height: 240px;
            width: 1280px;
            margin: 0 auto;
        }

        .header img {
            width: 300px;
            margin-left: 490px;
        }

        .autoheader {
            height: 240px;
            width: 1280px;
            margin: 0 0;
        }

        .autoheader .ah-left {
            width: 635px;
            height: 100%;
            float: left;
        }

        .autoheader .ah-left img {
            width: 550px;
            margin-left: 75px;
            margin-top: -95px;
        }

        .autoheader .ah-right {
            width: 635px;
            height: 100%;
            float: right;
        }

        .ah-locations {
            margin-top: 80px;
            font-weight: bold;
            font-size: 18px;
        }

        .ah-locations > img {
            height: 125px;
            float: left;
            margin-right: 25px;
        }

        .background {
            background-repeat: no-repeat;
            background-size: contain;
            background-position: center bottom;
            background-color: white;
        }

        .block {
            background-image: url("/img/torn-bg.png");
            background-size: cover;
            background-repeat: no-repeat;
            width: 1000px;
            height: 395px;
            margin: 0 auto;
            margin-bottom: 20px;
        }

        .wow {
            position: relative;
        }

        .wow > img {
            position: absolute;
            top: 235;
            z-index: 1;
            width: 200px;
        }

        .block > .items {
            width: 1000px;
            height: 395px;
            margin-left: 55px;
            padding-top: 20px !important;
        }

        .items > .item {
            width: 400px;
            height: 330px;
            float: left;
            margin-right: 25px;
            position: relative;
            background-color: white;
            background-size: contain;
            margin-left: 25px;
        }

        .item > h1 {
            padding: 10px;
            font-size: 1.5em;
            text-align: center;
            line-height: 1;
        }

        .item > h2 {
            position: absolute;
            bottom: -35px;
            right: 5px;
            font-size: 8em;
            line-height: .9;
            text-align: right;
            margin: 0;
            color: #D0072B;
        }

        .item > h2 > span {
            text-transform: uppercase;
            font-size: .3em;
            color: white;
        }

        .footer {
            width: 1125px;
            margin: 0 auto;
            padding-top: 20px;
        }

        .footer > .black {
            background: black;
            color: white;
            text-align: center;
            text-transform: uppercase;
            padding: 5px;
            margin-bottom: 3px;
        }

        .black > span {
            color: #FFD602;
            font-weight: bold;
        }

        .footer > .red {
            background: #000000;
            padding: 5px;
            margin-bottom: 3px;
            color: white;
            text-align: center;
            text-transform: uppercase;
        }

        .red > span {
            background: #FFD602;
            color: #000000;
            padding: 3px;
            margin: 0 15px 0 15px;
            font-weight: bold;
            padding-left: 15px;
            padding-right: 15px;
            border-radius: 3px;
        }

        .footer > .small {
            text-align: center;
            font-size: .5em;
        }

        .p2 {
            position: relative;
        }

        .p2 > .red {
            width: 1140px;
            height: 770px;
            background: #000000;
            position: absolute;
            left: 70px;
            top: 70px;
        }

        .inner-red > .items {
            width: 1070px;
            height: 220px;
            background: white;
            position: absolute;
            left: 35px;
            margin: 0 auto;
        }

        .inner-red > .items > .title {
            position: absolute;
            left: 15px;
            top: -18px;
            color: #FFD602;
            text-transform: uppercase;
            font-weight: bold;
        }

        .inner-red > .items > .item {
            width: 160px;
            height: 200px;
            margin: 0px 5px 0 10px;
        }

        .inner-red > .items > .item > h1 {
            font-size: .7em;
            margin: 0;
        }

        .inner-red > .items > .item > h2 {
            font-size: 2.4em;
            -webkit-text-stroke-width: 1px;
            bottom: -15px;
            right: 0;
        }

        .inner-red > .items > .item > h2 > span {
            -webkit-text-stroke-width: 0px;
        }

        .middle-block {
            position: absolute;
            top: 40px;
            left: 70px;
            width: 1140px;
            height: 680px;
        }

        .inner-mb {
            position: relative;
            width: 1140px;
            margin: 0 auto;
        }

        .inner-mb > .items > .item {
            width: 262px;
            height: 220px;
            margin: 0px 0 5px 0;
            border-top: solid 5px #000000;
            border-radius: 0;
        }

        .inner-mb > .items > .background {
            background-position: left bottom;
        }

        .inner-mb > .items > .item > h1 {
            width: 252px;
            font-size: 1.2em;
            /*width: 100% !important;*/
            padding: 5px;
            margin: 0;
        }

        .inner-mb > .items > .item > h2 {
            font-size: 2.4em;
            -webkit-text-stroke-width: 2px;
            bottom: 0;
            right: 0;
            line-height: 1;
        }

        .inner-mb > .items > .item > h2 > span {
            color: #000;
            background: #fff;
            font-size: .4em;
            border-radius: 2px;
            -webkit-text-stroke-width: 0;
            margin-right: 5px;
            padding: 1px 5px 1px 5px;
            line-height: 25px;
        }

        .ft-inner > .items > .item {
            width: 285px;
            height: 290px;
            margin: 0;
        }

        .ft-inner > .items > .item > h1 {
            font-size: .9em;
            width: 100% !important;
            padding: 5px;
            margin: 0;
        }

        .ft-inner > .items > .item > h2 {
            font-size: 3em;
            -webkit-text-stroke-width: 2px;
            bottom: 5px;
            right: 5px;
        }

        .ft-inner > .items > .item > h2 > span {
            -webkit-text-stroke-width: 1px;
            -webkit-text-stroke-color: #fff;
            color: #000;
        }

        .ft-inner > .items > .background {
            background-size: cover;
            background-position: center center;
        }

        .ft-inner > .items > .item > .title {
            width: 100%;
            background: #000000;
            color: #ffffff;
            text-align: center;
            text-transform: uppercase;
            font-size: .7em;
            font-weight: bold;
            padding-top: 5px;
            padding-bottom: 5px;
        }

        .cost-plus {
            padding: 5px;
            font-size: .939em;
            text-align: justify;
        }

        .item-description {
            text-shadow: white 3px 0px 0px, white 2.83487px 0.981584px 0px, white 2.35766px 1.85511px 0px, white 1.62091px 2.52441px 0px, white 0.705713px 2.91581px 0px, white -0.287171px 2.98622px 0px, white -1.24844px 2.72789px 0px, white -2.07227px 2.16926px 0px, white -2.66798px 1.37182px 0px, white -2.96998px 0.42336px 0px, white -2.94502px -0.571704px 0px, white -2.59586px -1.50383px 0px, white -1.96093px -2.27041px 0px, white -1.11013px -2.78704px 0px, white -0.137119px -2.99686px 0px, white 0.850987px -2.87677px 0px, white 1.74541px -2.43999px 0px, white 2.44769px -1.73459px 0px, white 2.88051px -0.838247px 0px;
        }

        .item-description span {
            text-transform: uppercase;
        }

        .price {
            text-shadow: white 3px 0px 0px, white 2.83487px 0.981584px 0px, white 2.35766px 1.85511px 0px, white 1.62091px 2.52441px 0px, white 0.705713px 2.91581px 0px, white -0.287171px 2.98622px 0px, white -1.24844px 2.72789px 0px, white -2.07227px 2.16926px 0px, white -2.66798px 1.37182px 0px, white -2.96998px 0.42336px 0px, white -2.94502px -0.571704px 0px, white -2.59586px -1.50383px 0px, white -1.96093px -2.27041px 0px, white -1.11013px -2.78704px 0px, white -0.137119px -2.99686px 0px, white 0.850987px -2.87677px 0px, white 1.74541px -2.43999px 0px, white 2.44769px -1.73459px 0px, white 2.88051px -0.838247px 0px;
        }

        .price span {
            text-shadow: none;
        }

        .bowlby {
            font-family: 'Bowlby One SC', 'Arial', 'sans-serif';
            font-size: 5.0em !important;
        }

        .bfl-tab {
            z-index: 1000;
            width: 1536px;
            height: 1954px;
            overflow: hidden;
        }

    </style>
</head>
<body>
<div class="poster">
    <img src="/img/posters/ss-tab.png" class="bfl-tab" />
    <div class="container">
        @if( $ad->header_image != "" )
            <div class="header"  style="background: url({{ $ad->header_image }}); background-size: cover;"></div>
        @else
            <div class="autoheader">
                <div class="ah-left">
                    <img src="{{ $ad->brands->logo }}" alt="">
                </div>
                <div class="ah-right">
                    <div class="ah-locations">
                        <img src="/img/local.png" alt="">
                        1201 S. Cornwell Dr., Yukon<br />
                        1104 SE 44th St., OKC<br />
                        10011 SE 15th St., MWC<br />
                        1205 E. Lindsey St., Norman
                    </div>
                </div>
            </div>
        @endif
        <div class="block">
            <div class="wow"><img src="/img/ss-wow.png" /></div>
            <div class="items">
                @foreach($ad->specials->whereIn('position_number', [1,2]) as $special)
                    <div class="item background" style="background-image: url({{ $special->image }});">
                        <h1 class="price">{!! strtolower($special->item_description) !!}</h1>
                        <h2 class="price bowlby">{{ $special->price }}<br /><span>{{ $special->size }}</span></h2>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="block">
            <div class="wow"><img src="/img/ss-wow.png" /></div>
            <div class="items">
                @foreach($ad->specials->whereIn('position_number', [3,4]) as $special)
                    <div class="item background" style="background-image: url({{ $special->image }});">
                        <h1 class="price">{!! strtolower($special->item_description) !!}</h1>
                        <h2 class="price bowlby">{{ $special->price }}<br /><span>{{ $special->size }}</span></h2>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="block">
            <div class="wow"><img src="/img/ss-wow.png" /></div>
            <div class="items">
                @foreach($ad->specials->whereIn('position_number', [5,6]) as $special)
                    <div class="item background" style="background-image: url({{ $special->image }});">
                        <h1 class="price">{!! strtolower($special->item_description) !!}</h1>
                        <h2 class="price bowlby">{{ $special->price }}<br /><span>{{ $special->size }}</span></h2>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="footer">
            <div class="cost-plus">
                Buying at Cost Plus 10% means LOWER PRICES on EVERY item, in EVERY department, EVERYDAY! You don't have to
                buy in bulk, don't pay for membership, don't need a shopper's card or sacrifice on selection, variety,
                quality or freshness. All with 100% Money Back Guarantee on everything we sell.<br />
            </div>
            <div class="black">
                Can't find what you want in our store? Send a text message to <span>405-896-7983</span> to request the product you need!
            </div>
            <div class="red" style="background-color: #D0072B">
                Prices effective: <b>{{ $ad->date_from }}</b> to <b>{{ $ad->date_to }}</b> <span>Money Orders: 69¢</span> WE ARE LOCAL!
            </div>
            <div class="small">
                We gladly accept WIC Checks, or Oklahoma Access. We reserve the right to limit quantities.
                All limits are per family. Not liable for typographical or pictorial errors.
            </div>
        </div>
    </div>
</div>
</body>
</html>