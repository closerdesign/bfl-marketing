<html>
<head>
    <link rel="stylesheet" href="/css/supermercado-soft-revamp.css">
    <link href="https://fonts.googleapis.com/css?family=Bowlby+One+SC&display=swap" rel="stylesheet">
</head>
<body>
<div class="container p1">
    <div class="header">
        <img src="{{ $ad->brands->logo }}" alt="">
        <div class="dotw">

        </div>
    </div>
    <div class="inner-container">
        @if( $ad->dotw_image != "" )
            <div class="dotw">
                <img src="https://bfl-corp-sara.s3.us-west-2.amazonaws.com/ads/dotw-images/{{$ad->dotw_image}}" />
            </div>
        @endif
        <div class="block produce">
            @foreach($ad->specials->whereIn('position_number', [1,2]) as $special)
                <div class="item half">
                    <div class="item-container" style="background-image: url({{ $special->image }})">
                        <div class="item-description">
                            {!! strtolower($special->item_description) !!}
                        </div>
                        <div class="price">
                            {!! $special->price_tag !!}<br /><span>{{ strtolower($special->size) }}</span>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="block meat">
            @foreach($ad->specials->whereIn('position_number', [3,4]) as $special)
                <div class="item half">
                    <div class="item-container" style="background-image: url({{ $special->image }})">
                        <div class="item-description">
                            {!! strtolower($special->item_description) !!}
                        </div>
                        <div class="price">
                            {!! $special->price_tag !!}<br /><span>{{ strtolower($special->size) }}</span>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="block produce">
            @foreach($ad->specials->whereIn('position_number', [5,6,7,8]) as $special)
                <div class="item fourth">
                    <div class="item-container featured" style="background-image: url({{ $special->image }})">
                        <div class="item-description">
                            {!! strtolower($special->item_description) !!}
                        </div>
                        <div class="price">
                            {!! $special->price_tag !!}<br /><span>{{ strtolower($special->size) }}</span>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="block grocery">
            @foreach($ad->specials->whereIn('position_number', [9,10,11,12]) as $special)
                <div class="item fourth">
                    <div class="item-container featured" style="background-image: url({{ $special->image }})">
                        <div class="item-description">
                            {!! strtolower($special->item_description) !!}
                        </div>
                        <div class="price">
                            {!! $special->price_tag !!}<br /><span>{{ strtolower($special->size) }}</span>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="dates">
        <img src="/img/SM-ad-block-5-dollar-chicken.png" alt="">
        Prices effective {{ date('F d, Y', strtotime($ad->date_from)) }} to {{ date('F d, Y', strtotime($ad->date_to)) }}
    </div>
</div>
<div class="container p2">
    <div class="inner-container">
        <div class="block grocery">
            @foreach($ad->specials->whereIn('position_number', [13,14,15,16,17,18,19,20,21,22,23,24,25,26,27]) as $special)
                <div class="item">
                    <div class="item-container" style="background-image: url({{ $special->image }})">
                        <div class="item-description">
                            {!! strtolower($special->item_description) !!}
                        </div>
                        <div class="price">
                            {!! $special->price_tag !!}<br /><span>{{ strtolower($special->size) }}</span>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="block meat">
            @php $i = 0 @endphp
            @foreach($ad->specials->whereIn('position_number', [28,29,30,31,32,33,34]) as $special)
            <div class="item @if( $i < 3 ) third @else fourth @endif">
                <div class="item-container" style="background-image: url({{ $special->image }})">
                    <div class="item-description">
                        {!! strtolower($special->item_description) !!}
                    </div>
                    <div class="price">
                        {!! $special->price_tag !!}<br /><span>{{ strtolower($special->size) }}</span>
                    </div>
                </div>
            </div>
            @php $i++ @endphp
            @endforeach
        </div>
        <div class="block produce">
            @foreach($ad->specials->whereIn('position_number', [35,36,37,38]) as $special)
                <div class="item fourth">
                    <div class="item-container" style="background-image: url({{ $special->image }})">
                        <div class="item-description">
                            {!! strtolower($special->item_description) !!}
                        </div>
                        <div class="price">
                            {!! $special->price_tag !!}<br /><span>{{ strtolower($special->size) }}</span>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="block bakery">
            @foreach($ad->specials->whereIn('position_number', [39,40,41,42]) as $special)
                <div class="item fourth">
                    <div class="item-container" style="background-image: url({{ $special->image }})">
                        <div class="item-description">
                            {!! strtolower($special->item_description) !!}
                        </div>
                        <div class="price">
                            {!! $special->price_tag !!}<br /><span>{{ strtolower($special->size) }}</span>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="block deli">
            @foreach($ad->specials->whereIn('position_number', [43,44,45,46]) as $special)
                <div class="item fourth">
                    <div class="item-container" style="background-image: url({{ $special->image }})">
                        <div class="item-description">
                            {!! strtolower($special->item_description) !!}
                        </div>
                        <div class="price">
                            {!! $special->price_tag !!}<br /><span>{{ strtolower($special->size) }}</span>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="block hbc">
            @foreach($ad->specials->whereIn('position_number', [47,48,49,50,51,52]) as $special)
                <div class="item third">
                    <div class="item-container" style="background-image: url({{ $special->image }})">
                        <div class="item-description">
                            {!! strtolower($special->item_description) !!}
                        </div>
                        <div class="price">
                            {!! $special->price_tag !!}<br /><span>{{ strtolower($special->size) }}</span>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="dates">
        Prices effective {{ date('F d, Y', strtotime($ad->date_from)) }} to {{ date('F d, Y', strtotime($ad->date_to)) }}
    </div>
</div>
</body>
</html>