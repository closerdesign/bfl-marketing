<html>
<head>
    <link rel="stylesheet" href="/css/bfl-grocery-4x5.css">
</head>
<body>
<div class="container">
    @if($ad->dotw_image != '')
        <div class="dotw" style="position: absolute; z-index: 1000; top: 5px; left: 568px;">
            <img src="https://bfl-corp-sara.s3.us-west-2.amazonaws.com/ads/dotw-images/{{$ad->dotw_image}}" />
        </div>
        <div class="header" style="background: url('/img/dotw/bfl-dotw-header.jpg'); background-size: cover;">

        </div>
    @else
        <div class="header" @if( $ad->header_image != "" ) style="background: url({{ $ad->header_image }}); background-size: cover;" @endif>

        </div>
    @endif
    <div class="block">
        <div class="items">
            @foreach($ad->specials->whereIn('position_number', [1,2,3]) as $special)
            <div class="item background" style="background-image: url({{ $special->image }});">
                <h1>{!! strtolower($special->item_description) !!}</h1>
                @if( $special->alt_price_format != true )
                    <h2>{{ $special->price }}<br /><span>{{ $special->size }}</span></h2>
                @else
                    <h3>{{ $special->price }}</h3>
                @endif
            </div>
            @endforeach
        </div>
    </div>
    <div class="block">
        <div class="items">
            @foreach($ad->specials->whereIn('position_number', [4,5,6]) as $special)
                <div class="item background" style="background-image: url({{ $special->image }});">
                    <h1>{!! strtolower($special->item_description) !!}</h1>
                    @if( $special->alt_price_format != true )
                        <h2>{{ $special->price }}<br /><span>{{ $special->size }}</span></h2>
                    @else
                        <h3>{{ $special->price }}</h3>
                    @endif
                </div>
            @endforeach
        </div>
    </div>
    <div class="block">
        <div class="items">
            @foreach($ad->specials->whereIn('position_number', [7,8,9]) as $special)
                <div class="item background" style="background-image: url({{ $special->image }});">
                    <h1>{!! strtolower($special->item_description) !!}</h1>
                    @if( $special->alt_price_format != true )
                        <h2>{{ $special->price }}<br /><span>{{ $special->size }}</span></h2>
                    @else
                        <h3>{{ $special->price }}</h3>
                    @endif
                </div>
            @endforeach
        </div>
    </div>
    <div class="footer">
        <div class="black">
            Can't find what you want in our store? Send a text message to <span>405-896-7933</span> to request the product you need!
        </div>
        <div class="red">
            Prices effective: {{ $ad->date_from }} - {{ $ad->date_to }} <span>Money Orders: 69¢</span> 3501 NW Expressway, OKC - 7957 NW 23rd St., Bethany
        </div>
        <div class="small">
            We gladly accept WIC Checks, Oklahoma Access, Manufacturer Coupons. We reserve the right to limit quantities. All limits are per family. Not liable for typographical or pictorial errors. Some items featured only available at this location.
        </div>
    </div>
</div>
<div class="container p2">
    <div class="red">
        <div class="inner-red">
            <div class="items items1">
                <div class="title">Meat</div>
                @foreach($ad->specials->whereIn('position_number', [10,11,12,13,14,15]) as $special)
                <div class="item background" style="background-image: url({{ $special->image }});">
                    <h1>{!! strtolower($special->item_description) !!}</h1>
                    <h2>{{ $special->price }}<br /><span>{{ $special->size }}</span></h2>
                </div>
                @endforeach
            </div>
            <div class="items items2">
                <div class="title">Produce</div>
                @foreach($ad->specials->whereIn('position_number', [16,17,18,19,20,21]) as $special)
                    <div class="item background produce" style="background-image: url({{ $special->image }})">
                        <h1>{!! strtolower($special->item_description) !!}</h1>
                        <h2>{{ $special->price }}<br /><span>{{ $special->size }}</span></h2>
                    </div>
                @endforeach
            </div>
            <div class="items items3">
                <div class="title">
                    Health / Beauty / Cosmetics
                </div>
                @foreach($ad->specials->whereIn('position_number', [22,23,24,25,26,27]) as $special)
                <div class="item background" style="background-image: url({{ $special->image }});">
                    <h1>{!! strtolower($special->item_description) !!}</h1>
                    <h2>{{ $special->price }}<br /><span>{{ $special->size }}</span></h2>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="middle-block">
        <div class="inner-mb">
            <div class="items">
                <?php $i = 0; ?>
                @foreach($ad->specials->whereIn('position_number', [28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47]) as $special)
                    <div class="item background" style="background-image: url({{ $special->image }});">
                        <h1>{!! strtolower($special->item_description) !!}</h1>
                        <h2>{{ $special->price }}<br /><span>{{ $special->size }}</span></h2>
                    </div>
                    <?php $i++ ?>
                    @if( $i == 4 ) <?php $i = 0; ?> @endif
                @endforeach
            </div>
        </div>
    </div>
    <div class="ft">
        <div class="ft-inner">
            <div class="items">
                @foreach($ad->specials->whereIn('position_number', [48,49,50,51,52,53,54,55]) as $special)
                    <div class="item background" style="background-image: url({{ $special->image }});">
                        <div class="title">
                            &nbsp;
                        </div>
                        <h1>{!! strtolower($special->item_description) !!}</h1>
                        <h2>{{ $special->price }}<br /><span>{{ $special->size }}</span></h2>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
</body>
</html>