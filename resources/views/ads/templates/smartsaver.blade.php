<html>
<head>
    <link rel="stylesheet" href="/css/smartsaver-css.css">
</head>
<body>
<div class="container">
    @if( $ad->header_image != "" )
        <div class="header"  style="background: url({{ $ad->header_image }}); background-size: cover;"></div>
    @else
        @if( $ad->dotw_image != "" )
            <div class="dotw">
                <img src="https://bfl-corp-sara.s3.us-west-2.amazonaws.com/ads/dotw-images/{{$ad->dotw_image}}" width="500" height="180"/>
            </div>
        @endif
        <div class="autoheader">
            <div class="ah-left">
                <img src="{{ $ad->brands->logo }}" alt="">
            </div>
            <div class="ah-right">
                <div class="ah-locations">
                    @if( $ad->dotw_image == "" )
                        <img src="/img/local.png" alt="">
                        1201 S. Cornwell Dr., Yukon<br />
                        1104 SE 44th St., OKC<br />
                        10011 SE 15th St., MWC<br />
                        1205 E. Lindsey St., Norman
                    @endif
                </div>
            </div>
        </div>
    @endif
    <div class="block">
        <div class="wow"><img src="/img/ss-wow.png" /></div>
        <div class="items">
            @foreach($ad->specials->whereIn('position_number', [1,2]) as $special)
                <div class="item background" style="background-image: url({{ $special->image }});">
                    <h1 class="price">{!! strtolower($special->item_description) !!}</h1>
                    <h2 class="price bowlby">{{ $special->price }}<br /><span>{{ $special->size }}</span></h2>
                </div>
            @endforeach
        </div>
    </div>
    <div class="block">
        <div class="wow"><img src="/img/ss-wow.png" /></div>
        <div class="items">
            @foreach($ad->specials->whereIn('position_number', [3,4]) as $special)
                <div class="item background" style="background-image: url({{ $special->image }});">
                    <h1 class="price">{!! strtolower($special->item_description) !!}</h1>
                    <h2 class="price bowlby">{{ $special->price }}<br /><span>{{ $special->size }}</span></h2>
                </div>
            @endforeach
        </div>
    </div>
    <div class="block">
        <div class="wow"><img src="/img/ss-wow.png" /></div>
        <div class="items">
            @foreach($ad->specials->whereIn('position_number', [5,6]) as $special)
                <div class="item background" style="background-image: url({{ $special->image }});">
                    <h1 class="price">{!! strtolower($special->item_description) !!}</h1>
                    <h2 class="price bowlby">{{ $special->price }}<br /><span>{{ $special->size }}</span></h2>
                </div>
            @endforeach
        </div>
    </div>
    <div class="footer">
        <div class="cost-plus">
            Buying at Cost Plus 10% means LOWER PRICES on EVERY item, in EVERY department, EVERYDAY! You don't have to
            buy in bulk, don't pay for membership, don't need a shopper's card or sacrifice on selection, variety,
            quality or freshness. All with 100% Money Back Guarantee on everything we sell.<br />
        </div>
        <div class="black">
            Can't find what you want in our store? Send a text message to <span>405-896-7983</span> to request the product you need!
        </div>
        <div class="red" style="background-color: #D0072B">
            Prices effective: <b>{{ $ad->date_from }}</b> to <b>{{ $ad->date_to }}</b> <span>Money Orders: 69¢</span> WE ARE LOCAL!
        </div>
        <div class="small">
            We gladly accept WIC Checks, or Oklahoma Access. We reserve the right to limit quantities.
            All limits are per family. Not liable for typographical or pictorial errors.
        </div>
    </div>
</div>
<div class="container p2">
    <div class="locs">
        <img src="/img/ss-local.png" alt="" width="263"><br /><br />
        1201 S. Cornwell Dr., Yukon<br />
        1104 SE 44th St., OKC<br />
        10011 SE 15th St., MWC<br />
        1205 E. Lindsey St., Norman
    </div>
    <div class="middle-block">
        <div class="inner-mb">
            <div class="items">
                <?php $i = 0; ?>
                @foreach($ad->specials->whereIn('position_number', [7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38]) as $special)
                    <div class="item background" style="background-image: url({{ $special->image }}); @if($i != 3) margin-right: 30px; @endif">
                        <h1 class="item-description">{!! strtolower($special->item_description) !!}</h1>
                        <h2 class="price">{{ $special->price }}<br /><span>{{ $special->size }}</span></h2>
                    </div>
                    <?php $i++ ?>
                    @if( $i == 4 ) <?php $i = 0; ?> @endif
                @endforeach
            </div>
        </div>
    </div>
</div>
</body>
</html>