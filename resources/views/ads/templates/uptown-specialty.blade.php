<html>
    <head>
        <link rel="stylesheet" href="/css/uptown-specialty-css.css">
    </head>
    <body>
        <div class="container">
            <div class="p4">
                <div class="item-row-1">
                    @foreach($ad->specials->whereIn('position_number', [53,54,55,56]) as $special)
                        <div class="item" style="background-image: url({{ $special->image }})">
                            <div class="item-description">
                                {!! $special->item_description !!}<br />
                                <span style="font-weight: normal">{{ strtolower($special->size) }}</span>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="item-row-2">
                    @foreach($ad->specials->whereIn('position_number', [57,58,59,60]) as $special)
                        <div class="item" style="background-image: url({{ $special->image }})">
                            <div class="item-description">
                                {!! $special->item_description !!}<br />
                                <span style="font-weight: normal">{{ strtolower($special->size) }}</span>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="item-row-3">
                    @foreach($ad->specials->whereIn('position_number', [61,62]) as $special)
                        <div class="item" style="background-image: url({{ $special->image }})">
                            <div class="item-description">
                                {!! $special->item_description !!}<br />
                                <span style="font-weight: normal">{{ strtolower($special->size) }}</span>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="p1">
                <div class="dates">{{ date('F d, Y', strtotime($ad->date_from)) }} to {{ date('F d, Y', strtotime($ad->date_to)) }}</div>
                <div class="item-row-1">
                    @foreach($ad->specials->whereIn('position_number', [1,2]) as $special)
                        <div class="item" style="background-image: url({{ $special->image }})">
                            <div class="p1-price">
                                <div class="item-description">
                                    {!! $special->item_description !!}<br />
                                    <span style="font-weight: normal">{{ strtolower($special->size) }}</span>
                                </div>
                                <div class="price">
                                    {{ $special->price }}
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="item-row-2">
                    @foreach($ad->specials->whereIn('position_number', [3,4]) as $special)
                        <div class="item" style="background-image: url({{ $special->image }})">
                            <div class="p1-price">
                                <div class="item-description">
                                    {!! $special->item_description !!}<br />
                                    <span style="font-weight: normal">{{ strtolower($special->size) }}</span>
                                </div>
                                <div class="price">
                                    {{ $special->price }}
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="standard-row">
                    @foreach($ad->specials->whereIn('position_number', [5,6,7,8,9,10,11,12]) as $special)
                        <div class="item" style="background-image: url({{ $special->image }})">
                            <div class="p1-price">
                                <div class="item-description">
                                    {!! $special->item_description !!}<br />
                                    <span style="font-weight: normal">{{ strtolower($special->size) }}</span>
                                </div>
                                <div class="price">{{ $special->price }}</div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="container">
            <div class="p2">
                &nbsp;<div class="standard-row">
                    @foreach($ad->specials->whereIn('position_number', [13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32]) as $special)
                        <div class="item" style="background-image: url({{ $special->image }})">
                            <div class="p1-price">
                                <div class="item-description">
                                    {!! $special->item_description !!}<br />
                                    <span style="font-weight: normal">{{ strtolower($special->size) }}</span>
                                </div>
                                <div class="price">{{ $special->price }}</div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="p3">
                &nbsp;<div class="standard-row">
                    @foreach($ad->specials->whereIn('position_number', [33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52]) as $special)
                        <div class="item" style="background-image: url({{ $special->image }})">
                            <div class="p1-price">
                                <div class="item-description">
                                    {!! $special->item_description !!}<br />
                                    <span style="font-weight: normal">{{ strtolower($special->size) }}</span>
                                </div>
                                <div class="price">{{ $special->price }}</div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </body>
</html>