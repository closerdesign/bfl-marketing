<html>
<head>
    <link rel="stylesheet" href="/css/uptown-css.css">
</head>
<body>
<div class="container">
    <div class="p4">
        <div class="container">
            <div class="inner-container">
                @php $i = 0; @endphp
                @foreach($ad->specials->whereIn('position_number', [41,42,43,44,45,46,47]) as $special)
                    <div class="@if( $i == 0 ) featured @endif item" style="background-image: url({{ $special->image }})">
                        <div class="item-container">
                            <div class="item-description">
                                {!! strtolower($special->item_description) !!}
                            </div>
                            <div class="price">
                                {{ $special->price }}<br /><span>{{ strtolower($special->size) }}</span>
                            </div>
                        </div>
                    </div>
                    @php $i++ @endphp
                @endforeach

            </div>

            <div class="blue" @if( $ad->banners->first() ) style="background: url({{ $ad->banners->first()->file }})" @endif>
                @php $i=0; @endphp
                @foreach($ad->specials->whereIn('position_number', [48,49]) as $special)
                <div class="item @if($i == 0) side @endif" style="background-image: url({{ $special->image }})">
                    <div class="item-container">
                        <div class="item-description">
                            {!! strtolower($special->item_description) !!}
                        </div>
                        <div class="price">
                            {{ $special->price }}<br /><span>{{ strtolower($special->size) }}</span>
                        </div>
                    </div>
                </div>
                @php $i++ @endphp
                @endforeach
            </div>

            <div class="bakery">
                <div class="title">
                    Bakery
                </div>
                @foreach($ad->specials->whereIn('position_number', [50,51,52]) as $special)
                    <div class="item" style="background-image: url({{ $special->image }})">
                        <div class="item-container">
                            <div class="item-description">
                                {!! strtolower($special->item_description) !!}
                            </div>
                            <div class="price">
                                {{ $special->price }}<br /><span>{{ strtolower($special->size) }}</span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="cheese">
                <div class="title">
                    Cheese
                </div>
                @foreach($ad->specials->whereIn('position_number', [53,54,55]) as $special)
                    <div class="item" style="background-image: url({{ $special->image }})">
                        <div class="item-container">
                            <div class="item-description">
                                {!! strtolower($special->item_description) !!}
                            </div>
                            <div class="price">
                                {{ $special->price }}<br/><span>{{ strtolower($special->size) }}</span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="footer">
                <div class="disclaimer">
                    We gladly accept WIC Checks, Oklahoma Access, Manufacturer Coupons. We reserve the right to limit quantities. All limits are per family. Not liable for typographical or pictorial errors.
                </div>
                <div class="apple-pay">
                    <img src="/img/Apple_Pay_Mark_RGB_052318.svg" alt="Apple Pay Logo">
                </div>
                <div class="android-pay">
                    <img src="/img/android-pay.png" alt="Android Pay Logo">
                </div>
                <div class="facebook-logo">
                    <img src="/img/FindUs-FB-RGB-BRC-Site-500.svg" alt="Facebook Badge"><br /><br />
                    Effective {{ date('F d, Y', strtotime($ad->date_from)) }} to {{ date('F d, Y', strtotime($ad->date_to)) }}
                </div>
                <div class="stores">
                    @foreach($ad->brands->stores as $store)
                        <div class="store">
                            <div class="store-container">
                                <b>{{ $store->name }}</b><br />
                                {{ $store->address }}<br />
                                {{ $store->phone }}<br />
                                Open Daily {{ $store->store_hours }}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>


        </div>
    </div>
    <div class="p1">
        <div style="text-align: @if($ad->dotw_image != '') right @else center @endif; position: relative; top: 8px; right: 10px; font-size: 13px;">
            Prices effective {{ date('F d, Y', strtotime($ad->date_from)) }} to {{ date('F d, Y', strtotime($ad->date_to)) }}
        </div>
        @if($ad->dotw_image != '')
            <div class="dotw">
                <img src="https://bfl-corp-sara.s3.us-west-2.amazonaws.com/ads/dotw-images/{{$ad->dotw_image}}" width="500" height="180"/>
            </div>
        @endif
        <div class="header">

        </div>
        <div class="featured-block">
            <div class="featured-content">

                @foreach($ad->specials->whereIn('position_number', [1]) as $special)
                <div class="featured-item item" style="background-image: url({{ $special->image }})">
                    <div class="item-container">
                        <div class="item-description">
                            {!! strtolower($special->item_description) !!}
                        </div>
                        <div class="price">
                            {{ $special->price }}<span><br />{{ $special->size }}</span>
                        </div>
                    </div>
                </div>
                @endforeach

                <div class="right-col">

                    <div class="right-col-container">
                        @foreach($ad->specials->whereIn('position_number', [2,3]) as $special)
                            <div class="right-col-item item" style="background-image: url({{ $special->image }})">
                                <div class="item-container">
                                    <div class="item-description">
                                        {!! strtolower($special->item_description) !!}
                                    </div>
                                    <div class="price">
                                        {{ $special->price }}<span><br />{{ strtolower($special->size) }}</span>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                </div>
            </div>
        </div>
        <div class="front-footer">
            <div class="front-footer-content">
                @foreach($ad->specials->whereIn('position_number', [4,5,6,7,8,9]) as $special)
                    <div class="front-footer-item item" style="background-image: url({{ $special->image }})">
                        <div class="item-container">
                            <div class="item-description">
                                {!! strtolower($special->item_description) !!}
                            </div>
                            <div class="price">
                                {{ $special->price }}<br /><span>{{ $special->size }}</span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="p2">
        <div class="inner-container">
            <div class="meat">
                <div class="title">Meat & Seafood <span>Save Time & Try Our Meat Grilling Station!</span></div>
                @foreach($ad->specials->whereIn('position_number', [10,11,12,13,14,15]) as $special)
                    <div class="item" style="background-image: url({{ $special->image }})">
                        <div class="item-container">
                            <div class="item-description">
                                {!! strtolower($special->item_description) !!}
                            </div>
                            <div class="price">
                                {{ $special->price }}<br /><span>{{ $special->size }}</span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="deli">
                <div class="title">Deli <span>Chef Prepared</span></div>
                @foreach($ad->specials->whereIn('position_number', [16,17,18,19]) as $special)
                    <div class="item" style="background-image: url({{ $special->image }}); background-size: cover;">
                        <div class="item-container">
                            <div class="item-description">
                                {!! strtolower($special->item_description) !!}
                            </div>
                            <div class="price">
                                {{ $special->price }}<br /><span>{{ $special->size }}</span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="sushi">
                <div class="title">Sushi <span>Visit Our Sushi Chefs or Order Online at sushi.uptowngroceryco.com</span></div>
                @foreach($ad->specials->whereIn('position_number', [20,21,22]) as $special)
                    <div class="item" style="background-image: url({{ $special->image }})">
                        <div class="item-container">
                            <div class="item-description">
                                {!! strtolower($special->item_description) !!}
                            </div>
                            <div class="price">
                                {{ $special->price }}<br /><span>{{ strtolower($special->size) }}</span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="banners" @if( count($ad->banners) > 1 ) style="background: url({{ $ad->banners->last()->file }})" @endif ></div>
        </div>
    </div>
    <div class="p3">
        <div class="inner-container">
            <div class="grocery">
                <div class="title">Grocery <span>Oklahoma Local with Low Low Prices!</span></div>
                @foreach($ad->specials->whereIn('position_number', [23,24,25,26,27,28,29,30,31,32,33,34]) as $special)
                    <div class="item" style="background-image: url({{ $special->image }})">
                        <div class="item-container">
                            <div class="item-description">
                                {!! strtolower($special->item_description) !!}
                            </div>
                            <div class="price">
                                {{ $special->price }}<br /><span>{{ strtolower($special->size) }}</span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="hbc">
                <div class="title">Health, Beauty & Care</div>
                @foreach($ad->specials->whereIn('position_number', [35,36,37,38,39,40]) as $special)
                    <div class="item" style="background-image: url({{ $special->image }})">
                        <div class="item-container">
                            <div class="item-description">
                                {!! strtolower($special->item_description) !!}
                            </div>
                            <div class="price">
                                {{ $special->price }}<br /><span>{{ strtolower($special->size) }}</span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>
    </div>
</div>
</body>
</html>