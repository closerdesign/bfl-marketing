<html>
<head>
    <style>
        .page{
            width: 1800px;
            height: 3150px;
            background: #f5f5f5;
        }

        .container{
            width: 1700px;
            margin: 0 auto;
        }

        .box{
            width: 800px;
            height: 800px;
            float: left;
            margin: 25px;
            position: relative;
            border-radius: 25px;
        }

        .description{
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-size: 36px;
            position: absolute;
            top: -15px;
            left: -15px;
            font-weight: bold;
            background: black;
            border-radius: 25px;
            padding: 25px;
            color: white;
            max-width: 70%;
        }

        .price{
            background: red;
            color: white;
            font-weight: bold;
            font-size: 96px;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            border-radius: 25px;
            padding: 20px;
            text-align: center;
            position: absolute;
            bottom: 25px;
            right: 25px;
        }
    </style>
</head>
<body>
<div class="page">
    <div class="container">
        <div class="header">

        </div>
        @foreach($ad->specials as $item)
            <div class="box" style="background: url({{ $item->image }}) #ffffff; background-repeat: no-repeat; background-position: bottom; background-size: cover;">
                <div class="description">
                    {{ $item->item_description }}
                </div>
                <div class="price">
                    {{ $item->price }}
                </div>
            </div>
        @endforeach
        <div class="footer">

        </div>
    </div>
</div>
</body>
</html>