<html>
<head>
    <link rel="stylesheet" href="/css/supermercado-css.css">
</head>
<body>
    @if( $ad->header_image != "" )
        <div class="container p1" style="background: url({{ $ad->header_image }})">
    @else
        @if( $ad->dotw_image != "" )
            <div class="container p1" style="background: url(/img/dotw/sm-dotw-bg.jpg)">
        @else
            <div class="container p1" style="background: url(/img/bg-front-supermercado.png)">
        @endif
    @endif
    <div class="inner-container">
        @if( $ad->dotw_image != "" )
            <div class="dotw">
                <img src="https://bfl-corp-sara.s3.us-west-2.amazonaws.com/ads/dotw-images/{{$ad->dotw_image}}" />
            </div>
        @endif
        <div class="produce">
            @foreach($ad->specials->whereIn('position_number', [1,2]) as $special)
                <div class="item" style="background-image: url({{ $special->image }})">
                    <div class="item-container">
                        <div class="item-description">
                            {!! strtolower($special->item_description) !!}
                        </div>
                        <div class="price">
                            {{ $special->price }}<br /><span>{{ strtolower($special->size) }}</span>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="meat">
            @foreach($ad->specials->whereIn('position_number', [3,4]) as $special)
                <div class="item" style="background-image: url({{ $special->image }})">
                    <div class="item-container">
                        <div class="item-description">
                            {!! strtolower($special->item_description) !!}
                        </div>
                        <div class="price">
                            {{ $special->price }}<br /><span>{{ strtolower($special->size) }}</span>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="grocery">
            @foreach($ad->specials->whereIn('position_number', [5,6]) as $special)
                <div class="item" style="background-image: url({{ $special->image }})">
                    <div class="item-container">
                        <div class="item-description">
                            {!! strtolower($special->item_description) !!}
                        </div>
                        <div class="price">
                            {{ $special->price }}<br /><span>{{ strtolower($special->size) }}</span>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="dates">
            Prices effective {{ date('F d, Y', strtotime($ad->date_from)) }} to {{ date('F d, Y', strtotime($ad->date_to)) }}
        </div>
    </div>
</div>
<div class="container p2" style="background: url(/img/bg-back-supermercado.png)">
    <div class="inner-container">
        <div class="grocery">
            @foreach($ad->specials->whereIn('position_number', [7,8,9,10,11,12,13,14,15,16]) as $special)
                <div class="item" style="background-image: url({{ $special->image }})">
                    <div class="item-container">
                        <div class="item-description">
                            {!! strtolower($special->item_description) !!}
                        </div>
                        <div class="price">
                            {{ $special->price }}<br /><span>{{ strtolower($special->size) }}</span>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="meat">
            @php $i = 0 @endphp
            @foreach($ad->specials->whereIn('position_number', [17,18,19,20,21,22,23]) as $special)
                <div class="item @if($i < 3) featured @endif" style="background-image: url({{ $special->image }})">
                    <div class="item-container">
                        <div class="item-description">
                            {!! strtolower($special->item_description) !!}
                        </div>
                        <div class="price">
                            {{ $special->price }}<br /><span>{{ strtolower($special->size) }}</span>
                        </div>
                    </div>
                </div>
                @php $i++ @endphp
            @endforeach
        </div>
        <div class="produce">
            @foreach($ad->specials->whereIn('position_number', [24,25,26,27]) as $special)
                <div class="item" style="background-image: url({{ $special->image }})">
                    <div class="item-container">
                        <div class="item-description">
                            {!! strtolower($special->item_description) !!}
                        </div>
                        <div class="price">
                            {{ $special->price }}<br /><span>{{ strtolower($special->size) }}</span>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="bakery">
            @foreach($ad->specials->whereIn('position_number', [28,29,30,31]) as $special)
                <div class="item" style="background-image: url({{ $special->image }})">
                    <div class="item-container">
                        <div class="item-description">
                            {!! strtolower($special->item_description) !!}
                        </div>
                        <div class="price">
                            {{ $special->price }}<br /><span>{{ strtolower($special->size) }}</span>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="deli">
            @foreach($ad->specials->whereIn('position_number', [32,33,34,35]) as $special)
                <div class="item" style="background-image: url({{ $special->image }})">
                    <div class="item-container">
                        <div class="item-description">
                            {!! strtolower($special->item_description) !!}
                        </div>
                        <div class="price">
                            {{ $special->price }}<br /><span>{{ strtolower($special->size) }}</span>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="hbc">
            @foreach($ad->specials->whereIn('position_number', [36,37,38,39,40,41]) as $special)
                <div class="item" style="background-image: url({{ $special->image }})">
                    <div class="item-container">
                        <div class="item-description">
                            {!! strtolower($special->item_description) !!}
                        </div>
                        <div class="price">
                            {{ $special->price }}<br /><span>{{ strtolower($special->size) }}</span>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="dates">
            Prices effective {{ date('F d, Y', strtotime($ad->date_from)) }} to {{ date('F d, Y', strtotime($ad->date_to)) }}
        </div>
    </div>
</div>
</body>
</html>