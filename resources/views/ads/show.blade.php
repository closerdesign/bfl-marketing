@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Ad: {{ $ad->name }} - {{ $ad->stores->name }} [ From {{ $ad->date_from }} to {{ $ad->date_to }} ]</div>

                    <div class="panel-body">

                        <div class="row">

                            <div class="col-md-12">

                                <div>

                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#grocery" aria-controls="grocery" role="tab" data-toggle="tab">Grocery</a></li>
                                        <li role="presentation"><a href="#produce" aria-controls="produce" role="tab" data-toggle="tab">Produce</a></li>
                                        <li role="presentation"><a href="#meat" aria-controls="meat" role="tab" data-toggle="tab">Meat</a></li>
                                        <li role="presentation"><a href="#hbc" aria-controls="hbc" role="tab" data-toggle="tab">HBC</a></li>
                                        <li role="presentation"><a href="#deli" aria-controls="deli" role="tab" data-toggle="tab">Deli</a></li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="grocery">
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <form action="{{ action('GroceryAdsController@store') }}" method="post">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="ad_id" value="{{ $ad->id }}">
                                                        <div class="col-md-12">
                                                            <p class="lead">Add grocery product:</p>
                                                        </div>
                                                        <div class="col-md-12 form-group">
                                                            <label for="brand">Brand</label>
                                                            <input type="text" class="form-control" name="brand" required >
                                                        </div>
                                                        <div class="col-md-12 form-group">
                                                            <label for="product">Product</label>
                                                            <input type="text" class="form-control" name="product" required >
                                                        </div>
                                                        <div class="col-md-12 form-group">
                                                            <label for="size">Size</label>
                                                            <input type="text" class="form-control" name="size" required >
                                                        </div>
                                                        <div class="col-md-12 form-group">
                                                            <label for="upc">UPC</label>
                                                            <input type="text" class="form-control" name="upc" required >
                                                        </div>
                                                        <div class="col-md-6 form-group">
                                                            <label for="price">Price</label>
                                                            <input type="text" class="form-control" name="price" required >
                                                        </div>
                                                        <div class="col-md-6 form-group">
                                                            <label for="position">Position</label>
                                                            <input type="text" class="form-control" name="position" required >
                                                        </div>
                                                        <div class="col-md-12 form-group">
                                                            <label for="notes">Notes</label>
                                                            <textarea name="notes" id="notes" cols="30" rows="10" class="form-control"></textarea>
                                                        </div>
                                                        <div class="col-md-12 form-group">
                                                            <button class="btn btn-primary form-control" type="submit">Save product</button>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered table-striped">
                                                            <thead>
                                                            <tr>
                                                                <th>Brand</th>
                                                                <th>Product</th>
                                                                <th>Size</th>
                                                                <th>UPC</th>
                                                                <th>Price</th>
                                                                <th>Position</th>
                                                                <th>Notes</th>
                                                                <th>Actions</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach($groceries as $grocery)
                                                                <tr>
                                                                    <td>{{ $grocery->brand }}</td>
                                                                    <td>{{ $grocery->product }}</td>
                                                                    <td>{{ $grocery->size }}</td>
                                                                    <td>{{ $grocery->upc }}</td>
                                                                    <td>{{ $grocery->price }}</td>
                                                                    <td>{{ $grocery->position }}</td>
                                                                    <td>{{ $grocery->notes }}</td>
                                                                    <td width="15%" class="text-center">
                                                                        <a href="/grocery-ads/delete/{{ $grocery->id }}"><span class="glyphicon glyphicon-trash"></span></a>&nbsp;
                                                                        <a href="/grocery-ads/down/{{ $grocery->id }}"><span class="glyphicon glyphicon-menu-down"></span></a>&nbsp;
                                                                        <a href="/grocery-ads/up/{{ $grocery->id }}"><span class="glyphicon glyphicon-menu-up"></span></a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="produce">

                                            <hr>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <form action="{{ action('ProduceAdsController@store', $ad->id) }}" method="post">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="ad_id" value="{{ $ad->id }}">
                                                        <div class="col-md-12">
                                                            <p class="lead">Add produce product:</p>
                                                        </div>
                                                        <div class="col-md-12 form-group">
                                                            <label for="brand">Brand</label>
                                                            <input type="text" class="form-control" name="brand" required >
                                                        </div>
                                                        <div class="col-md-12 form-group">
                                                            <label for="product">Product</label>
                                                            <input type="text" class="form-control" name="product" required >
                                                        </div>
                                                        <div class="col-md-12 form-group">
                                                            <label for="size">Size</label>
                                                            <input type="text" class="form-control" name="size" required >
                                                        </div>
                                                        <div class="col-md-12 form-group">
                                                            <label for="upc">UPC</label>
                                                            <input type="text" class="form-control" name="upc" required >
                                                        </div>
                                                        <div class="col-md-6 form-group">
                                                            <label for="price">Price</label>
                                                            <input type="text" class="form-control" name="price" required >
                                                        </div>
                                                        <div class="col-md-6 form-group">
                                                            <label for="position">Position</label>
                                                            <input type="text" class="form-control" name="position" required >
                                                        </div>
                                                        <div class="col-md-12 form-group">
                                                            <label for="notes">Notes</label>
                                                            <textarea name="notes" id="notes" cols="30" rows="10" class="form-control"></textarea>
                                                        </div>
                                                        <div class="col-md-12 form-group">
                                                            <button class="btn btn-primary form-control" type="submit">Save product</button>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered table-striped">
                                                            <thead>
                                                            <tr>
                                                                <th>Brand</th>
                                                                <th>Product</th>
                                                                <th>Size</th>
                                                                <th>UPC</th>
                                                                <th>Price</th>
                                                                <th>Position</th>
                                                                <th>Notes</th>
                                                                <th>Actions</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach($produces as $produce)
                                                                <tr>
                                                                    <td>{{ $produce->brand }}</td>
                                                                    <td>{{ $produce->product }}</td>
                                                                    <td>{{ $produce->size }}</td>
                                                                    <td>{{ $produce->upc }}</td>
                                                                    <td>{{ $produce->price }}</td>
                                                                    <td>{{ $produce->position }}</td>
                                                                    <td>{{ $produce->notes }}</td>
                                                                    <td width="15%" class="text-center">
                                                                        <a href="/produce-ads/delete/{{ $produce->id }}"><span class="glyphicon glyphicon-trash"></span></a>&nbsp;
                                                                        <a href="/produce-ads/down/{{ $produce->id }}"><span class="glyphicon glyphicon-menu-down"></span></a>&nbsp;
                                                                        <a href="/produce-ads/up/{{ $produce->id }}"><span class="glyphicon glyphicon-menu-up"></span></a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="meat">

                                            <hr>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <form action="{{ action('MeatAdsController@store', $ad->id) }}" method="post">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="ad_id" value="{{ $ad->id }}">
                                                        <div class="col-md-12">
                                                            <p class="lead">Add meat product:</p>
                                                        </div>
                                                        <div class="col-md-12 form-group">
                                                            <label for="brand">Brand</label>
                                                            <input type="text" class="form-control" name="brand" required >
                                                        </div>
                                                        <div class="col-md-12 form-group">
                                                            <label for="product">Product</label>
                                                            <input type="text" class="form-control" name="product" required >
                                                        </div>
                                                        <div class="col-md-12 form-group">
                                                            <label for="size">Size</label>
                                                            <input type="text" class="form-control" name="size" required >
                                                        </div>
                                                        <div class="col-md-12 form-group">
                                                            <label for="upc">UPC</label>
                                                            <input type="text" class="form-control" name="upc" required >
                                                        </div>
                                                        <div class="col-md-6 form-group">
                                                            <label for="price">Price</label>
                                                            <input type="text" class="form-control" name="price" required >
                                                        </div>
                                                        <div class="col-md-6 form-group">
                                                            <label for="position">Position</label>
                                                            <input type="text" class="form-control" name="position" required >
                                                        </div>
                                                        <div class="col-md-12 form-group">
                                                            <label for="notes">Notes</label>
                                                            <textarea name="notes" id="notes" cols="30" rows="10" class="form-control"></textarea>
                                                        </div>
                                                        <div class="col-md-12 form-group">
                                                            <button class="btn btn-primary form-control" type="submit">Save product</button>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered table-striped">
                                                            <thead>
                                                            <tr>
                                                                <th>Brand</th>
                                                                <th>Product</th>
                                                                <th>Size</th>
                                                                <th>UPC</th>
                                                                <th>Price</th>
                                                                <th>Position</th>
                                                                <th>Notes</th>
                                                                <th>Actions</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach($meats as $meat)
                                                                <tr>
                                                                    <td>{{ $meat->brand }}</td>
                                                                    <td>{{ $meat->product }}</td>
                                                                    <td>{{ $meat->size }}</td>
                                                                    <td>{{ $meat->upc }}</td>
                                                                    <td>{{ $meat->price }}</td>
                                                                    <td>{{ $meat->position }}</td>
                                                                    <td>{{ $meat->notes }}</td>
                                                                    <td width="15%" class="text-center">
                                                                        <a href="/meat-ads/delete/{{ $meat->id }}"><span class="glyphicon glyphicon-trash"></span></a>&nbsp;
                                                                        <a href="/meat-ads/down/{{ $meat->id }}"><span class="glyphicon glyphicon-menu-down"></span></a>&nbsp;
                                                                        <a href="/meat-ads/up/{{ $meat->id }}"><span class="glyphicon glyphicon-menu-up"></span></a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="hbc">

                                            <hr>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <form action="{{ action('HbcAdsController@store', $ad->id) }}" method="post">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="ad_id" value="{{ $ad->id }}">
                                                        <div class="col-md-12">
                                                            <p class="lead">Add HBC product:</p>
                                                        </div>
                                                        <div class="col-md-12 form-group">
                                                            <label for="brand">Brand</label>
                                                            <input type="text" class="form-control" name="brand" required >
                                                        </div>
                                                        <div class="col-md-12 form-group">
                                                            <label for="product">Product</label>
                                                            <input type="text" class="form-control" name="product" required >
                                                        </div>
                                                        <div class="col-md-12 form-group">
                                                            <label for="size">Size</label>
                                                            <input type="text" class="form-control" name="size" required >
                                                        </div>
                                                        <div class="col-md-12 form-group">
                                                            <label for="upc">UPC</label>
                                                            <input type="text" class="form-control" name="upc" required >
                                                        </div>
                                                        <div class="col-md-6 form-group">
                                                            <label for="price">Price</label>
                                                            <input type="text" class="form-control" name="price" required >
                                                        </div>
                                                        <div class="col-md-6 form-group">
                                                            <label for="position">Position</label>
                                                            <input type="text" class="form-control" name="position" required >
                                                        </div>
                                                        <div class="col-md-12 form-group">
                                                            <label for="notes">Notes</label>
                                                            <textarea name="notes" id="notes" cols="30" rows="10" class="form-control"></textarea>
                                                        </div>
                                                        <div class="col-md-12 form-group">
                                                            <button class="btn btn-primary form-control" type="submit">Save product</button>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered table-striped">
                                                            <thead>
                                                            <tr>
                                                                <th>Brand</th>
                                                                <th>Product</th>
                                                                <th>Size</th>
                                                                <th>UPC</th>
                                                                <th>Price</th>
                                                                <th>Position</th>
                                                                <th>Notes</th>
                                                                <th>Actions</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach($hbcs as $hbc)
                                                                <tr>
                                                                    <td>{{ $hbc->brand }}</td>
                                                                    <td>{{ $hbc->product }}</td>
                                                                    <td>{{ $hbc->size }}</td>
                                                                    <td>{{ $hbc->upc }}</td>
                                                                    <td>{{ $hbc->price }}</td>
                                                                    <td>{{ $hbc->position }}</td>
                                                                    <td>{{ $hbc->notes }}</td>
                                                                    <td width="15%" class="text-center">
                                                                        <a href="/hbc-ads/delete/{{ $hbc->id }}"><span class="glyphicon glyphicon-trash"></span></a>&nbsp;
                                                                        <a href="/hbc-ads/down/{{ $hbc->id }}"><span class="glyphicon glyphicon-menu-down"></span></a>&nbsp;
                                                                        <a href="/hbc-ads/up/{{ $hbc->id }}"><span class="glyphicon glyphicon-menu-up"></span></a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="deli">

                                            <hr>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <form action="{{ action('DeliAdsController@store', $ad->id) }}" method="post">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="ad_id" value="{{ $ad->id }}">
                                                        <div class="col-md-12">
                                                            <p class="lead">Add Deli product:</p>
                                                        </div>
                                                        <div class="col-md-12 form-group">
                                                            <label for="brand">Brand</label>
                                                            <input type="text" class="form-control" name="brand" required >
                                                        </div>
                                                        <div class="col-md-12 form-group">
                                                            <label for="product">Product</label>
                                                            <input type="text" class="form-control" name="product" required >
                                                        </div>
                                                        <div class="col-md-12 form-group">
                                                            <label for="size">Size</label>
                                                            <input type="text" class="form-control" name="size" required >
                                                        </div>
                                                        <div class="col-md-12 form-group">
                                                            <label for="upc">UPC</label>
                                                            <input type="text" class="form-control" name="upc" required >
                                                        </div>
                                                        <div class="col-md-6 form-group">
                                                            <label for="price">Price</label>
                                                            <input type="text" class="form-control" name="price" required >
                                                        </div>
                                                        <div class="col-md-6 form-group">
                                                            <label for="position">Position</label>
                                                            <input type="text" class="form-control" name="position" required >
                                                        </div>
                                                        <div class="col-md-12 form-group">
                                                            <label for="notes">Notes</label>
                                                            <textarea name="notes" id="notes" cols="30" rows="10" class="form-control"></textarea>
                                                        </div>
                                                        <div class="col-md-12 form-group">
                                                            <button class="btn btn-primary form-control" type="submit">Save product</button>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered table-striped">
                                                            <thead>
                                                            <tr>
                                                                <th>Brand</th>
                                                                <th>Product</th>
                                                                <th>Size</th>
                                                                <th>UPC</th>
                                                                <th>Price</th>
                                                                <th>Position</th>
                                                                <th>Notes</th>
                                                                <th>Actions</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach($delis as $deli)
                                                                <tr>
                                                                    <td>{{ $deli->brand }}</td>
                                                                    <td>{{ $deli->product }}</td>
                                                                    <td>{{ $deli->size }}</td>
                                                                    <td>{{ $deli->upc }}</td>
                                                                    <td>{{ $deli->price }}</td>
                                                                    <td>{{ $deli->position }}</td>
                                                                    <td>{{ $deli->notes }}</td>
                                                                    <td width="15%" class="text-center">
                                                                        <a href="/deli-ads/delete/{{ $deli->id }}"><span class="glyphicon glyphicon-trash"></span></a>&nbsp;
                                                                        <a href="/deli-ads/down/{{ $deli->id }}"><span class="glyphicon glyphicon-menu-down"></span></a>&nbsp;
                                                                        <a href="/deli-ads/up/{{ $deli->id }}"><span class="glyphicon glyphicon-menu-up"></span></a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection