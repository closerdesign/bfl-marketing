<?php
/**
 * Created by PhpStorm.
 * User: TheLegend
 * Date: 9/10/18
 * Time: 2:07 PM
 */
?>
<style>
    .standard_box{
        position:relative;
        z-index: 1000; margin-top:-30px; margin-left:5px;
        font-family: Helvetica, Arial, sans-serif;
        font-size:48px;  font-weight:bolder;
        height: 80px; width: 80px;
        padding: 18px -16px -18px 15px;
        background-image: url(https://bfl-corp-sara.s3.us-west-2.amazonaws.com/liquor-licenses/5ba143f1d8a16.png);
        background-repeat: no-repeat;

    }
    .standard_box_cent{
        position:relative;
        z-index: 1000; margin-top:-30px; margin-left:5px;
        font-family: Helvetica, Arial, sans-serif;
        font-size:48px;  font-weight:bolder;
        height: 80px; width: 80px;
        padding: 18px -3px -18px 3px;
        background-image: url(https://bfl-corp-sara.s3.us-west-2.amazonaws.com/liquor-licenses/5ba143f1d8a16.png);
        background-repeat: no-repeat;

    }
    .small_box{
        width: 180px;
        height: 180px;
    }

</style>

<table>
    <tr>
        <td colspan="3" title="header">
            LOGO HERE
            <a href="{{ route('generate-pdf',['download'=>'pdf', 'id' => $ad->id]) }}">Download PDF</a>

        </td>
    </tr>
    <tr>
        <td rowspan="2" width="180" height="360" style="background-color: #d4ccb0">1</td>
        <td class="small_box" style="background-color: #ffffff">
            {{$items[3]}}

            <?php $split = split_price($ad->specials->where('position_number', '3')->first()->price); //echo $ad->specials->where('position_number', '3')->first()->price; ?>
            <div align="center"><img src="https://bfl-corp-sara.s3.us-west-2.amazonaws.com/liquor-licenses/5ba01349d5ce1.jpg" style="z-index: -1" width="220" height="100"></div>
            <div id="price1" style="padding-top: 18px;" <?php if(cent($split[0])){ echo 'class="standard_box_cent"';}else{echo 'class="standard_box"'; }?> >{{$split[0]}}<sup style="font-size: 28px; font-weight: bolder; margin-top:0px;">{{$split[1]}}</sup></div>
            <div style="font-family: 'Lucida Sans', Arial, sans-serif; font-weight: lighter; z-index:2202;">{{ $ad->specials->where('position_number', '2')->first()->item_description }}</div>
        </td>
        <td class="small_box" style="background-color: #FFFFFF">
            <?php $split = split_price($ad->specials->where('position_number', '5')->first()->price); //echo $ad->specials->where('position_number', '3')->first()->price; ?>
            <div align="center"><img src="https://bfl-corp-sara.s3.us-west-2.amazonaws.com/liquor-licenses/5ba1667abc116.jpg" style="z-index: -1"  width="220" height="100"></div>
            <div id="price1" style="padding-top: 18px;" <?php if(cent($split[0])){ echo 'class="standard_box_cent"';}else{echo 'class="standard_box"'; }?> >{{$split[0]}}<sup style="font-size: 28px; font-weight: bolder; margin-top:0px;">{{$split[1]}}</sup></div>
            <div style="font-family: 'Lucida Sans', Arial, sans-serif; font-weight: lighter;">{{ $ad->specials->where('position_number', '3')->first()->item_description }}</div>
        </td>
    </tr>
    <tr>
        <td class="small_box" style="background-color: #ffffff">
            <?php $split = split_price($ad->specials->where('position_number', '4')->first()->price); //echo $ad->specials->where('position_number', '3')->first()->price; ?>
            <div align="center"><img src="https://bfl-corp-sara.s3.us-west-2.amazonaws.com/liquor-licenses/5ba1667abc116.jpg" style="z-index: -1" width="220" height="100"></div>
            <div id="price1" style="padding-top: 18px;" <?php if(cent($split[0])){ echo 'class="standard_box_cent"';}else{echo 'class="standard_box"'; }?> >{{$split[0]}}<sup style="font-size: 28px; font-weight: bolder; margin-top:0px;">{{$split[1]}}</sup></div>
            <div style="font-family: 'Lucida Sans', Arial, sans-serif; font-weight: lighter;">{{ $ad->specials->where('position_number', '4')->first()->item_description }}</div>
        </td>
        <td class="small_box" style="background-color: #ffffff">
            <?php $split = split_price($ad->specials->where('position_number', '5')->first()->price); //echo $ad->specials->where('position_number', '3')->first()->price; ?>
            <div align="center"><img src="https://bfl-corp-sara.s3.us-west-2.amazonaws.com/liquor-licenses/5ba1667abc116.jpg" style="z-index: -1" width="220" height="100"></div>
            <div id="price1" style="padding-top: 18px;" <?php if(cent($split[0])){ echo 'class="standard_box_cent"';}else{echo 'class="standard_box"'; }?> >{{$split[0]}}<sup style="font-size: 28px; font-weight: bolder; margin-top:0px;">{{$split[1]}}</sup></div>
            <div style="font-family: 'Lucida Sans', Arial, sans-serif; font-weight: lighter;">{{ $ad->specials->where('position_number', '5')->first()->item_description }}</div>
        </td>

    </tr>
    <tr>
        <td class="small_box" style="background-color: #ffffff">
            <?php $split = split_price($ad->specials->where('position_number', '7')->first()->price); //echo $ad->specials->where('position_number', '3')->first()->price; ?>
            <div align="center"><img src="https://bfl-corp-sara.s3.us-west-2.amazonaws.com/liquor-licenses/5ba16d2a74abd.jpg" style="z-index: -1" width="220" height="100"></div>
            <div id="price1" style="padding-top: 18px;" <?php if(cent($split[0])){ echo 'class="standard_box_cent"';}else{echo 'class="standard_box"'; }?> >{{$split[0]}}<sup style="font-size: 28px; font-weight: bolder; margin-top:0px;">{{$split[1]}}</sup></div>
            <div style="font-family: 'Lucida Sans', Arial, sans-serif; font-weight: lighter;">{{ $ad->specials->where('position_number', '7')->first()->item_description }}</div>
        </td>
        <td class="small_box" style="background-color: #ffffff">
            <?php $split = split_price($ad->specials->where('position_number', '7')->first()->price); //echo $ad->specials->where('position_number', '3')->first()->price; ?>
            <div align="center"><img src="https://bfl-corp-sara.s3.us-west-2.amazonaws.com/liquor-licenses/5ba16d2a74abd.jpg" style="z-index: -1" width="220" height="100"></div>
            <div id="price1" style="padding-top: 18px;" <?php if(cent($split[0])){ echo 'class="standard_box_cent"';}else{echo 'class="standard_box"'; }?> >{{$split[0]}}<sup style="font-size: 28px; font-weight: bolder; margin-top:0px;">{{$split[1]}}</sup></div>
            <div style="font-family: 'Lucida Sans', Arial, sans-serif; font-weight: lighter;">{{ $ad->specials->where('position_number', '7')->first()->item_description }}</div>
        </td>
        <td class="small_box" style="background-color: #ffffff">
            <?php $split = split_price($ad->specials->where('position_number', '8')->first()->price); //echo $ad->specials->where('position_number', '3')->first()->price; ?>
            <div align="center"><img src="https://bfl-corp-sara.s3.us-west-2.amazonaws.com/liquor-licenses/5ba16d2a74abd.jpg" style="z-index: -1" width="220" height="100"></div>
            <div id="price1" style="padding-top: 18px;" <?php if(cent($split[0])){ echo 'class="standard_box_cent"';}else{echo 'class="standard_box"'; }?> >{{$split[0]}}<sup style="font-size: 28px; font-weight: bolder; margin-top:0px;">{{$split[1]}}</sup></div>
            <div style="font-family: 'Lucida Sans', Arial, sans-serif; font-weight: lighter;">{{ $ad->specials->where('position_number', '8')->first()->item_description }}</div>
        </td>
    </tr>
    <tr>
        <td class="small_box" style="background-color: #ffffff">
            <?php $split = split_price($ad->specials->where('position_number', '9')->first()->price); //echo $ad->specials->where('position_number', '3')->first()->price; ?>
            <div align="center"><img src="https://bfl-corp-sara.s3.us-west-2.amazonaws.com/liquor-licenses/5ba16d2a74abd.jpg" style="z-index: -1" width="220" height="100"></div>
            <div id="price1" style="padding-top: 18px;"<?php if(cent($split[0])){ echo 'class="standard_box_cent"';}else{echo 'class="standard_box"'; }?> >{{$split[0]}}<sup style="font-size: 28px; font-weight: bolder; margin-top:0px;">{{$split[1]}}</sup></div>
            <div style="font-family: 'Lucida Sans', Arial, sans-serif; font-weight: lighter;">{{ $ad->specials->where('position_number', '9')->first()->item_description }}</div>
        </td>
        <td class="small_box" style="background-color: #ffffff">
            {{$items[10]}}
            <?php $split = split_price($ad->specials->where('position_number', '10')->first()->price); //echo $ad->specials->where('position_number', '3')->first()->price; ?>
            <div align="center"><img src="https://bfl-corp-sara.s3.us-west-2.amazonaws.com/liquor-licenses/5ba16d2a74abd.jpg" style="z-index: -1" width="220" height="100"></div>
            <div id="price1" style="padding-top: 18px;" <?php if(cent($split[0])){ echo 'class="standard_box_cent"';}else{echo 'class="standard_box"'; }?> >{{$split[0]}}<sup style="font-size: 28px; font-weight: bolder; margin-top:0px;">{{$split[1]}}</sup></div>
            <div style="font-family: 'Lucida Sans', Arial, sans-serif; font-weight: lighter;">{{ $ad->specials->where('position_number', '10')->first()->item_description }}</div>
        </td>
        <td rowspan="2" width="180" height="360" style="background-color: #000000">13</td>
    </tr>
    <tr>
        <td class="small_box" style="background-color: #ffffff">
            <?php $split = split_price($ad->specials->where('position_number', '11')->first()->price); //echo $ad->specials->where('position_number', '3')->first()->price; ?>
            <div align="center"><img src="https://bfl-corp-sara.s3.us-west-2.amazonaws.com/liquor-licenses/5ba16d2a74abd.jpg" style="z-index: -1" width="220" height="100"></div>
            <div id="price1" style="padding-top: 18px;" <?php if(cent($split[0])){ echo 'class="standard_box_cent"';}else{echo 'class="standard_box"'; }?> >{{$split[0]}}<sup style="font-size: 28px; font-weight: bolder; margin-top:0px;">{{$split[1]}}</sup></div>
            <div style="font-family: 'Lucida Sans', Arial, sans-serif; font-weight: lighter;">{{ $ad->specials->where('position_number', '11')->first()->item_description }}</div>

        </td>
        <td class="small_box" style="background-color: #FFFFFF">
            <?php $split = split_price($ad->specials->where('position_number', '12')->first()->price); //echo $ad->specials->where('position_number', '3')->first()->price; ?>
            <div align="center"><img src="https://bfl-corp-sara.s3.us-west-2.amazonaws.com/liquor-licenses/5ba16d2a74abd.jpg" style="z-index: -1" width="220" height="100"></div>
            <div id="price1" style="padding-top: 18px;" <?php if(cent($split[0])){ echo 'class="standard_box_cent"';}else{echo 'class="standard_box"'; }?> >{{$split[0]}}<sup style="font-size: 28px; font-weight: bolder; margin-top:0px;">{{$split[1]}}</sup></div>
            <div style="font-family: 'Lucida Sans', Arial, sans-serif; font-weight: lighter;">{{ $ad->specials->where('position_number', '12')->first()->item_description }}</div>
        </td>

    </tr>
</table>


<?php

function split_price($price){

    if (preg_match('/\./', $price)){
        $array = explode('.', $price);
    }
    else {
        $array[0] = $price;
        $array[1] = '';
    }

    if ($array[0] ==''){
        $array[0] = '.'.$array[1];
        $array[1] ='';
    }
    return $array;
}

function cent($split1){
    if (preg_match('/\./', $split1)){
        return true;
    }
    else {
        return false;
    }
}


?>