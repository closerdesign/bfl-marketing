@extends('layouts.app')

@section('content')

    <div class="container-fluid">

        <p class="text-right">
            <a href="{{ action('AdsController@create') }}" class="btn btn-success"><i class="fa fa-plus"></i> Create A New Ad</a>
        </p>

        <div class="panel panel-primary">
            <div class="panel-heading">Ads</div>

            <div class="panel-body">

                <div class="table-responsive">

                    <table class="table table-striped table-condensed">

                        <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>Brand</th>
                            <th>Name</th>
                            <th>Date From</th>
                            <th>Date To</th>
                            <th>Status</th>
                            <th>Web Delivery</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach( $ads as $ad )

                            <tr>
                                <td>
                                    <div class="btn-group">
                                        <button class="btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-chevron-down"></i> Actions
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="#" onclick="$('#delete-{{ $ad->id }}').submit();">
                                                    Delete
                                                    <form id="delete-{{ $ad->id }}" action="{{ action('AdsController@destroy', $ad->id) }}" method="post" onsubmit="return confirm('Are you sure?')">
                                                        {{ csrf_field() }}
                                                        {{ method_field('DELETE') }}
                                                    </form>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{ action('AdsController@duplicate', $ad->id) }}">Duplicate</a>
                                            </li>
                                            <li><a href="{{ action('AdsController@generate_pdf', $ad->id) }}" target="_blank"> Generate PDF</a></li>
                                        </ul>
                                    </div>
                                </td>
                                <td><img src="{{ $ad->brands->logo }}" alt="{{ $ad->name }}" width="25"></td>
                                <td>{{ $ad->brands->name }}</td>
                                <th>
                                    <a href="{{ action('AdsController@edit', $ad->id) }}">
                                        {{ $ad->name }}
                                    </a>
                                </th>
                                <td class="text-center">{{ $ad->date_from }}</td>
                                <td class="text-center">{{ $ad->date_to }}</td>
                                <td class="text-center">{{ $ad->status }}</td>
                                <td class="text-center">{{ $ad->websites }}</td>
                            </tr>

                        @endforeach
                        </tbody>

                    </table>

                    {{ $ads->render() }}

                </div>

            </div>
        </div>

        <p class="text-center">
            @if($previous == 0)
                <a href="{{ action('AdsController@all') }}">¿Are you looking for our previous ads?</a>
            @else
                <a href="{{ action('AdsController@index') }}">¿Are you looking for our current ads?</a>
            @endif
        </p>
    </div>

    @endsection

    @section('js')

    <script>
        $('.datatable-ads').DataTable({
            "order": [
                [ 3, "asc" ]
            ],
            dom: 'fBprtpi',
            buttons: [
                {
                    extend: 'print',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'copy',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'csv',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdf',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                'colvis'
            ],
            columnDefs: [ {
                targets: -1,
                visible: true
            } ]
        });
    </script>

    @endsection