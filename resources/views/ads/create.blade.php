@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Create a new Ad
            </div>
            <div class="panel-body">
                <form action="{{ action('AdsController@store') }}" method="post">
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="brand_id">Brand</label>
                            <select name="brand_id" id="brand_id" class="form-control">
                                <option value="">Select...</option>
                                @foreach(App\Brand::all() as $brand)
                                    <option @if(old('brand') == $brand->id) selected @endif value="{{ $brand->id }}">{{ $brand->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-12">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" name="name" required placeholder="e.g. Weekly Ad, Bi-Weekly Ad, Subscriber Specials" >
                        </div>

                        <div class="col-md-6 form-group">
                            <label for="date_from">Date From</label>
                            <input type="text" class="form-control datepicker" name="date_from" value="{{ old('date_from') }}" required >
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="date_to">Date To</label>
                            <input type="text" class="form-control datepicker" name="date_to" value="{{ old('date_to') }}" required >
                        </div>
                        <div class="col-md-12 form-group">
                            <p><input type="checkbox" name="status" value="1" @if(old('status') == 1) checked @endif > This ad has been revised and is ready to be published.</p>
                            <p><input type="checkbox" name="websites" value="1" @if(old('websites') == 1) checked @endif > This ad will be delivered to websites.</p>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-primary" type="submit">Create Ad</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

    @endsection