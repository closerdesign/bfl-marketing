
@extends('layouts.app')

@section('style')
    <style>
        table tbody tr.selected {
            background-color: #dddddd !important;
        }
    </style>
@endsection

@section('content')

    <?php $class = ['Approved' => 'success', 'Pending' => '']; ?>

    <div class="container-fluid">

        <p class="lead text-right">{{ $ad->brands->name }} - {{ $ad->name }} - {{ $ad->date_from }}
            to {{ $ad->date_to }}</p>

        <div class="dropdown pull-right">
            <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"><i
                        class="fa fa-align-justify"></i> <span class="caret"></span></button>
            <ul class="dropdown-menu">
                <li><a href="{{ action('AdsController@export_csv', $ad->id) }}"> Ad Export</a></li>
                <li><a href="{{ action('SpecialsController@import') }}"> XLS Import</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="{{ action('AdsController@pdf_template', $ad->id) }}" target="_blank"> PDF Template</a></li>
                <li><a href="{{ action('AdsController@generate_pdf', $ad->id) }}" target="_blank"> Generate PDF</a></li>
                <li><a href="{{ action('AdsController@ad_text', $ad->id) }}" onclick="return confirm_text()"> Send
                        Text</a></li>
                <li><a href="{{ action('AdsController@banners', $ad->id) }}" target="_blank"> Generate Banners</a></li>
                <li role="separator" class="divider"></li>
                @if($ad->brand_id == 1 || $ad->brand_id == 3)
                    {{--BFL & Smart Saver--}}
                    <li><a href="{{ action('AdsController@poster', [$ad->id, 0]) }}" target="_blank"> Poster</a></li>
                    <li><a href="{{ action('AdsController@poster_pdf', [$ad->id, 0]) }}" target="_blank"> Poster [PDF]</a></li>
                @elseif($ad->brand_id == 2)
                    {{--Uptown--}}
                    <li><a href="{{ action('AdsController@poster', [$ad->id, 1]) }}" target="_blank"> Poster
                            Page 1</a></li>
                    <li><a href="{{ action('AdsController@poster_pdf', [$ad->id, 1]) }}" target="_blank"> Poster Page 1 [PDF]</a></li>
                    <li><a href="{{ action('AdsController@poster', [$ad->id, 2]) }}" target="_blank"> Poster
                            Page 2</a></li>
                    <li><a href="{{ action('AdsController@poster_pdf', [$ad->id, 2]) }}" target="_blank"> Poster Page 2 [PDF]</a></li>
                    <li><a href="{{ action('AdsController@poster', [$ad->id, 3]) }}" target="_blank"> Poster
                            Page 3</a></li>
                    <li><a href="{{ action('AdsController@poster_pdf', [$ad->id, 3]) }}" target="_blank">
                            Poster Page 3 [PDF]</a></li>
                    <li><a href="{{ action('AdsController@poster', [$ad->id, 4]) }}" target="_blank"> Poster
                            Page 4</a></li>
                    <li><a href="{{ action('AdsController@poster_pdf', [$ad->id, 4]) }}" target="_blank"> Poster Page 4 [PDF]</a></li>
                @elseif($ad->brand_id == 4)
                    {{--SuperMercado--}}
                    <li><a href="{{ action('AdsController@poster', [$ad->id, 1]) }}" target="_blank"> Poster
                            Page 1</a></li>
                    <li><a href="{{ action('AdsController@poster_pdf', [$ad->id, 1]) }}" target="_blank"> Poster Page 1 [PDF]</a></li>
                    <li><a href="{{ action('AdsController@poster', [$ad->id, 2]) }}" target="_blank"> Poster
                            Page 2</a></li>
                    <li><a href="{{ action('AdsController@poster_pdf', [$ad->id, 2]) }}" target="_blank"> Poster Page 2 [PDF]</a></li>
                @endif
                <li role="separator" class="divider"></li>
                <li><a href="{{ action('AdsController@ad_email', $ad->id) }}" target="_blank"> Email
                        - Weekly Ad</a></li>
                <li><a href="{{ action('AdsController@email', $ad->id) }}" target="_blank"> Subscriber
                        Specials</a></li>
                <li><a href="{{ action('AdsController@mailing', $ad->id) }}" target="_blank"> Mailing</a></li>
            </ul>
        </div>

        <div>

            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#basic-information" aria-controls="home" role="tab"
                                                          data-toggle="tab">Basic Information</a></li>
                <li role="presentation"><a href="#products" aria-controls="products" role="tab" data-toggle="tab">Products</a>
                </li>
                <li role="presentation"><a href="#images" aria-controls="images" role="tab" data-toggle="tab">Images</a>
                </li>
                <li role="presentation"><a href="#pdf" aria-controls="pdf" role="tab" data-toggle="tab">PDF</a></li>
                <li role="presentation"><a href="#banners" aria-controls="banners" role="tab"
                                           data-toggle="tab">Banners</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="basic-information">
                    <form action="{{ action('AdsController@update', $ad->id) }}" method="post"
                          enctype="multipart/form-data">

                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                        {{ method_field('PATCH') }}

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="brand_id">Brand</label>
                                <select name="brand_id" id="brand_id" class="form-control">
                                    <option value="">Select...</option>
                                    @foreach(App\Brand::all() as $brand)
                                        <option @if($ad->brand_id == $brand->id) selected
                                                @endif value="{{ $brand->id }}">{{ $brand->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" value="{{ $ad->name }}" required
                                       placeholder="e.g. Weekly Ad, Bi-Weekly Ad, Subscriber Specials">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="stores">Stores</label>
                            <select name="stores[]" id="stores" class="form-control multiple-select" multiple="multiple">
                                @foreach(\App\Store::where('brand_id', $ad->brand_id)->get() as $store)
                                <option @if( in_array($store->id, $ad->stores()->select('id')->pluck('id')->toArray())) selected @endif value="{{ $store->id }}">{{ $store->store_code }} {{ $store->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="date_from">From</label>
                            <input type="text" class="form-control datepicker" name="date_from"
                                   value="{{ $ad->date_from }}" required>
                        </div>

                        <div class="form-group">
                            <label for="date_to">To</label>
                            <input type="text" class="form-control datepicker" name="date_to" value="{{ $ad->date_to }}"
                                   required>
                        </div>

                        <div class="form-group">
                            <div class="checkbox">
                                <label><input type="checkbox" name="status" value="1"
                                              @if($ad->status == 'Revised') checked @endif > This ad has been revised
                                    and is ready to be published.</label><br/>
                                <label><input type="checkbox" name="websites" value="1"
                                              @if($ad->websites == 'Yes') checked @endif > This ad will be delivered to
                                    websites.</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="header_image">Header Image (1280x240)</label>
                            <input type="file" class="form-control" name="header_image">
                        </div>

                        <div class="form-group">
                            <label for="dotw_image">Deal of the Week Image (643x231)</label>
                            <input type="file" class="form-control" name="dotw_image">
                        </div>

                        <div class="form-group">
                            <label for="custom_template">Custom Template</label>
                            <select name="custom_template" id="custom-template" class="form-control">
                                <option value="">Select...</option>
                                <option @if($ad->custom_template == 'UG-G-4x4') selected @endif value="UG-G-4x4">Uptown
                                    - Grocery - 4x4
                                </option>
                                <option @if($ad->custom_template == 'bfl-grocery-4x5') selected
                                        @endif value="bfl-grocery-4x5">BFL - Grocery - 4x5
                                </option>
                                <option @if($ad->custom_template == 'holidays-gm') selected @endif value="holidays-gm">
                                    Holidays GM
                                </option>
                                <option @if($ad->custom_template == 'uptown-specialty') selected
                                        @endif value="uptown-specialty">Uptown Specialty Kehe
                                </option>
                                <option @if($ad->custom_template == 'uptown-grocery-4x4-produce-4') selected
                                        @endif value="uptown-grocery-4x4-produce-4">Uptown Grocery 4x4 Produce 4
                                </option>
                                <option @if($ad->custom_template == 'supermercado-with-strip') selected
                                        @endif value="supermercado-with-strip">Supermercado With Strip
                                </option>
                                <option @if($ad->custom_template == 'supermercado-soft-revamp') selected
                                        @endif value="supermercado-soft-revamp">Supermercado Soft Revamp
                                </option>
                                <option @if($ad->custom_template == 'buyforless-grocery-32') selected
                                        @endif value="buyforless-grocery-32">Buy For Less Grocery 32
                                </option>
                                <option @if($ad->custom_template == 'buyforless-grocery-6-16') selected
                                        @endif value="buyforless-grocery-6-16">Buy For Less Grocery 6 - 16
                                </option>
                            </select>
                        </div>

                        <button class="btn btn-success" type="submit"><i class="fa fa-floppy-o"></i> Update</button>

                    </form>
                </div>
                <div role="tabpanel" class="tab-pane" id="products">

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3 col-md-offset-6">
                                <a href="{{ action('SpecialsController@search') }}"
                                   class="btn btn-success form-control">
                                    <i class="fa fa-search"></i> Items Search
                                </a>
                            </div>
                            <div class="col-md-3">
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-success form-control" data-toggle="modal"
                                        data-target="#myModal">
                                    Clone Selected Items
                                </button>

                                <!-- Modal -->
                                <div class="modal fade" id="myModal" tabindex="-1" role="dialog"
                                     aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <form action="{{ action('SpecialsController@multiple_cloning') }}"
                                                  method="post" onsubmit="return validateMultipleCloning()">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="clone_items" id="clone_items">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close"><span aria-hidden="true">&times;</span>
                                                    </button>
                                                    <h4 class="modal-title" id="myModalLabel">Cloning Multiple
                                                        Items</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <select name="ads[]" id="ads" class="form-control"
                                                                multiple="multiple" required>
                                                            @foreach(\App\Ad::where('date_to', ">=", date('Y-m-d'))->orderBy('brand_id')->orderBy('date_from')->get() as $destination_ad)
                                                                <option value="{{ $destination_ad->id }}">{{ $destination_ad->brands->name }}
                                                                    - {{ $destination_ad->name }}
                                                                    - {{ $destination_ad->date_from }}
                                                                    - {{ $destination_ad->date_to }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">
                                                        Close
                                                    </button>
                                                    <button class="btn btn-success"><i class="fa fa-copy"></i> Clone
                                                        Selected Items
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div>

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs nav-ads" role="tablist">
                            <li role="presentation" class="active"><a href="#list" aria-controls="home" role="tab"
                                                                      data-toggle="tab"> <i class="fa fa-list"></i> ALL
                                    ITEMS</a></li>
                            <li role="presentation"><a href="#add-item" aria-controls="profile" role="tab"
                                                       data-toggle="tab"><i class="fa fa-plus-circle"></i> ADD ITEM</a>
                            </li>
                            @foreach($departments as $department)
                                <li role="presentation"><a href="#list-{{ $department->id }}" aria-controls="home"
                                                           role="tab" data-toggle="tab"> {{ $department->name }} </a>
                                </li>
                            @endforeach
                            <li><a href="#custom-report" aria-controls="custom-report" role="tab" data-toggle="tab">CUSTOM
                                    REPORT</a></li>
                            <li><a href="#all-items" aria-controls="all-items" role="tab" data-toggle="tab">ALL ITEMS <span class="badge">BETA</span></a></li>

                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="list">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered datatable-ads">
                                                <thead>
                                                <tr>
                                                    <th>&nbsp;</th>
                                                    <th>&nbsp;</th>
                                                    <th>Img</th>
                                                    <th>Position</th>
                                                    <th>Dept.</th>
                                                    <th>Description</th>
                                                    <th>Size</th>
                                                    <th>Net Cost</th>
                                                    <th>Regular Retail</th>
                                                    <th>Ad Retail</th>
                                                    <th>UPC</th>
                                                    <th>PLU Required</th>
                                                    <th>Comments</th>
                                                    <th>Item Info Location</th>
                                                    <th>Order #</th>
                                                    <th>Lmt</th>
                                                    <th>Yield</th>
                                                    <th>Adj Cost</th>
                                                    <th>Ad Gross</th>
                                                    <th>Supplier</th>
                                                    <th>Status</th>
                                                    <th>Featured</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($ad->specials as $special)
                                                    <tr class="{{ $class[$special->status] }}">
                                                        <td class="text-center">
                                                            {{ $special->id }}
                                                        </td>
                                                        <td class="text-center">
                                                            <p>
                                                            <form action="{{ action('SpecialsController@destroy', $special->id) }}"
                                                                  method="post"
                                                                  onsubmit="return confirm('Are you sure?')">
                                                                {{ csrf_field() }}
                                                                {{ method_field('DELETE') }}
                                                                <button class="btn-xs btn-danger" type="submit">
                                                                    <i class="fa fa-trash"></i>
                                                                </button>
                                                            </form>
                                                            </p>
                                                            <p>
                                                                <a href="{{ action('SpecialsController@clone', $special->id) }}">
                                                                    <button class="btn-xs btn-primary"><i
                                                                                class="fa fa-copy"></i></button>
                                                                </a>
                                                            </p>
                                                        </td>
                                                        <td class="text-center">
                                                            @if($special->image == "")
                                                                <a href="{{ action('SpecialsController@edit', $special->id) }}">
                                                                    <img width="50" src="/img/pending-image.jpg"
                                                                         alt="{{ $special->item_description }}">
                                                                </a>
                                                            @else
                                                                <a href="{{ action('SpecialsController@edit', $special->id) }}">
                                                                    <img width="50" src="{{ $special->image }}" alt="">
                                                                </a>
                                                            @endif
                                                        </td>
                                                        <td class="text-center">{{ $special->position_number }}</td>
                                                        <td>
                                                            @foreach($special->department as $department)
                                                                [{{ $department->name }}]
                                                            @endforeach
                                                        </td>
                                                        <th>
                                                            <a href="{{ action('SpecialsController@edit', $special->id) }}">
                                                                {{ $special->item_description }}
                                                            </a>
                                                        </th>
                                                        <td class="text-center">{{ $special->size }}</td>
                                                        <td class="text-center">{{ $special->cost }}</td>
                                                        <td class="text-center">{{ $special->srp }}</td>
                                                        <td class="text-center">{{ $special->price }}</td>
                                                        <td class="text-center">
                                                            <p>{{ $special->sku }}</p>
                                                            @if($special->additional_upcs != "")
                                                                <b>Additional UPCs</b><br/>
                                                                {{ $special->additional_upcs }}
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if($special->plu_required == 1)
                                                                Yes <br/><br/>
                                                                <b>{{ $special->plu }}</b>
                                                            @else
                                                                No
                                                            @endif
                                                        </td>
                                                        <td>
                                                            {{ $special->comments }}
                                                        </td>
                                                        <td>{{ $special->item_info_location }}</td>
                                                        <td>{{ $special->order_no }}</td>
                                                        <td>{{ $special->limit }}</td>
                                                        <td>{{ $special->yield }}</td>
                                                        <td>{{ $special->adj_cost }}</td>
                                                        <td>{{ $special->ad_gross }}</td>
                                                        <th>{{ $special->supplier }}</th>
                                                        <td>{{ $special->status }}</td>
                                                        <td>
                                                            @if($special->featured == 1)
                                                                <span class="badge">Yes</span>
                                                            @else
                                                                No
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            @foreach($departments as $department)

                                <div role="tabpanel" class="tab-pane" id="list-{{ $department->id }}">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <p>&nbsp;</p>
                                            <div class="table-responsive">
                                                <table class="table table-striped datatable-ads">
                                                    <thead>
                                                    <tr>
                                                        <th>&nbsp;</th>
                                                        <th>&nbsp;</th>
                                                        <th>Img</th>
                                                        <th>Position</th>
                                                        <th>Dept.</th>
                                                        <th>Description</th>
                                                        <th>Size</th>
                                                        <th>Net Cost</th>
                                                        <th>Regular Retail</th>
                                                        <th>Ad Retail</th>
                                                        <th>UPC</th>
                                                        <th>Comments</th>
                                                        <th>Item Info Location</th>
                                                        <th>Order #</th>
                                                        <th>Lmt</th>
                                                        <th>Yield</th>
                                                        <th>Adj Cost</th>
                                                        <th>Ad Gross</th>
                                                        <th>Supplier</th>
                                                        <th>Status</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach(\App\Special::where('ad_id', $ad->id)->join('department_special', 'department_special.special_id', '=', 'specials.id')->where('department_special.department_id', $department->id)->get() as $special)
                                                        <tr class="{{ $class[$special->status] }}">
                                                            <td class="text-center">
                                                                {{ $special->id }}
                                                            </td>
                                                            <td class="text-center">
                                                                <p>
                                                                <form action="{{ action('SpecialsController@destroy', $special->id) }}"
                                                                      method="post"
                                                                      onsubmit="return confirm('Are you sure?')">
                                                                    {{ csrf_field() }}
                                                                    {{ method_field('DELETE') }}
                                                                    <button class="btn-xs btn-danger" type="submit">
                                                                        <i class="fa fa-trash"></i>
                                                                    </button>
                                                                </form>
                                                                </p>
                                                                <p>
                                                                    <a href="{{ action('SpecialsController@clone', $special->id) }}">
                                                                        <button class="btn-xs btn-primary"><i
                                                                                    class="fa fa-copy"></i></button>
                                                                    </a>
                                                                </p>
                                                            </td>
                                                            <td class="text-center">
                                                                @if($special->image == "")
                                                                    <a href="{{ action('SpecialsController@edit', $special->id) }}">
                                                                        <img width="50" src="/img/pending-image.jpg"
                                                                             alt="{{ $special->item_description }}">
                                                                    </a>
                                                                @else
                                                                    <a href="{{ action('SpecialsController@edit', $special->id) }}">
                                                                        <img width="50" src="{{ $special->image }}"
                                                                             alt="">
                                                                    </a>
                                                                @endif
                                                            </td>
                                                            <td>{{ $special->position }}</td>
                                                            <td>
                                                                @foreach($special->department as $department)
                                                                    [{{ $department->name }}]
                                                                @endforeach
                                                            </td>
                                                            <th>
                                                                <a href="{{ action('SpecialsController@edit', $special->id) }}">
                                                                    {{ $special->item_description }}
                                                                </a>
                                                            </th>
                                                            <td class="text-center">{{ $special->size }}</td>
                                                            <td class="text-center">{{ $special->cost }}</td>
                                                            <td class="text-center">{{ $special->srp }}</td>
                                                            <td class="text-center">{{ $special->price }}</td>
                                                            <td class="text-center">
                                                                <p>{{ $special->sku }}</p>
                                                                @if($special->additional_upcs != "")
                                                                    <b>Additional UPCs</b><br/>
                                                                    {{ $special->additional_upcs }}
                                                                @endif
                                                            </td>
                                                            <td>
                                                                {{ $special->comments }}
                                                            </td>
                                                            <td>{{ $special->item_info_location }}</td>
                                                            <td>{{ $special->order_no }}</td>
                                                            <td>{{ $special->limit }}</td>
                                                            <td>{{ $special->yield }}</td>
                                                            <td>{{ $special->adj_cost }}</td>
                                                            <td>{{ $special->ad_gross }}</td>
                                                            <th>{{ $special->supplier }}</th>
                                                            <td>{{ $special->status }}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            @endforeach

                            <div role="tabpanel" class="tab-pane" id="add-item">

                                <div class="row">
                                    <div class="col-md-12">
                                        <p>&nbsp;</p>
                                        <form class="form-validate" action="{{ action('SpecialsController@store') }}"
                                              method="post" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="row">
                                                <div class="col-md-6 form-group">
                                                    <label for="item_description">Item Description</label>
                                                    <input type="text" class="form-control" name="item_description"
                                                           value="{{ old('item_description') }}" required>
                                                </div>
                                                <div class="col-md-3 form-group">
                                                    <label for="size">Size</label>
                                                    <input type="text" class="form-control" name="size"
                                                           value="{{ old('size') }}">
                                                </div>
                                                <div class="col-md-3 form-group">
                                                    <label for="sku">UPC (For Image)</label>
                                                    <input type="text" class="form-control" name="sku"
                                                           value="{{ old('sku') }}" autocomplete="off">
                                                </div>
                                                <div class="col-md-3 form-group">
                                                    <label for="status">Status</label>
                                                    <select name="status" id="status" class="form-control">
                                                        <option value="">Select...</option>
                                                        <option @if( old('status') == 0 ) selected @endif value="0">
                                                            Pending
                                                        </option>
                                                        <option @if( old('status') == 1 ) selected @endif value="1">
                                                            Approved
                                                        </option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3 form-group">
                                                    <label for="cost">Net Cost</label>
                                                    <input type="text" class="form-control" name="cost" id="cost"
                                                           value="{{ old('cost') }}">
                                                </div>
                                                <div class="col-md-3 form-group">
                                                    <label for="price">Ad Retail</label>
                                                    <input type="text" class="form-control meats-math" name="price"
                                                           id="price" value="{{ old('price') }}" required>
                                                </div>
                                                <div class="col-md-3 form-group">
                                                    <label for="srp">Regular Retail</label>
                                                    <input type="text" class="form-control" name="srp"
                                                           value="{{ old('srp') }}">
                                                </div>
                                                <div class="col-md-3 form-group">
                                                    <label for="position">Position</label>
                                                    <input type="text" class="form-control" name="position"
                                                           value="{{ old('position') }}">
                                                </div>
                                                <div class="col-md-3 form-group">
                                                    <label for="departament_id">Dept.</label>
                                                    <select name="department_id[]" id="department_id"
                                                            class="form-control multiple-select" multiple="multiple"
                                                            required>
                                                        <option value="">Select...</option>
                                                        @foreach(\App\Department::orderBy('name')->get() as $department)
                                                            <option value="{{ $department->id }}">{{ $department->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-3 form-group">
                                                    <label for="item_info_location">Item Info Location</label>
                                                    <input type="text" class="form-control" name="item_info_location"
                                                           value="{{ old('item_info_location') }}">
                                                </div>
                                                <div class="col-md-3 form-group">
                                                    <label for="image">Image (500x500)</label>
                                                    <input type="file" class="form-control" name="image">
                                                </div>
                                                <div class="col-md-12 checkbox">
                                                    <label><input type="checkbox" name="plu_required" value="1"> Is PLU
                                                        required?</label>
                                                </div>
                                                <div class="col-md-4 form-group">
                                                    <label for="position_number">Position Number</label>
                                                    <select name="position_number" id="position_number"
                                                            class="form-control">
                                                        <option value="">Select...</option>
                                                        @for($i = 1; $i <= 13; $i++)
                                                            <option value="{{$i}}">Position {{$i}}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                                <div class="col-md-4 form-group">
                                                    <label for="plu">PLU</label>
                                                    <input type="number" class="form-control" name="plu"
                                                           value="{{ old('plu') }}">
                                                </div>
                                                <div class="col-md-4 form-group">
                                                    <label for="promo_number">Promo Number</label>
                                                    <input type="text" class="form-control" name="promo_number"
                                                           value="{{ old('promo_number') }}">
                                                </div>
                                                <div class="col-md-12 form-group">
                                                    <label for="comments">Comments</label>
                                                    <textarea name="comments" id="comments" cols="30" rows="4"
                                                              class="form-control">{{ old('comments') }}</textarea>
                                                </div>
                                                <div class="col-md-12 form-group">
                                                    <label for="public_comment">Public Comment</label>
                                                    <input type="text" class="form-control" name="public_comment"
                                                           value="{{ old('public_comment') }}">
                                                </div>
                                                <div class="col-md-12 form-group">
                                                    <label for="related_upcs">Additional UPC</label>
                                                    <textarea name="additional_upcs" id="additional_upcs" cols="30"
                                                              rows="2"
                                                              class="form-control">{{ old('related_upcs') }}</textarea>
                                                    <label>Separated by comma.</label>
                                                </div>
                                                <div class="col-md-2 form-group">
                                                    <label for="order_no">Order #</label>
                                                    <input type="text" class="form-control" name="order_no"
                                                           value="{{ old('order_no') }}">
                                                </div>
                                                <div class="col-md-2 form-group">
                                                    <label for="limit">Limit</label>
                                                    <input type="text" class="form-control" name="limit"
                                                           value="{{ old('limit') }}">
                                                </div>
                                                <div class="col-md-2 form-group">
                                                    <label for="yield">Yield</label>
                                                    <div class="input-group">
                                                        <input type="number" class="form-control meats-math"
                                                               name="yield" id="yield" value="{{ old('yield') }}">
                                                        <div class="input-group-addon">%</div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 form-group">
                                                    <label for="adj_cost">Adj Cost</label>
                                                    <input type="text" class="form-control meats-math" name="adj_cost"
                                                           id="adj_cost" value="{{ old('adj_cost') }}" readonly>
                                                </div>
                                                <div class="col-md-2 form-group">
                                                    <label for="ad_gross">Ad Gross %</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control meats-math"
                                                               name="ad_gross" id="ad_gross"
                                                               value="{{ old('ad_gross') }}" readonly>
                                                        <div class="input-group-addon">%</div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 form-group">
                                                    <label for="supplier">Supplier</label>
                                                    <input type="text" class="form-control" name="supplier"
                                                           value="{{ old('supplier') }}">
                                                </div>
                                                <div class="col-md-12 form-group">
                                                    <label><input type="checkbox" name="featured" value="1"
                                                                  @if( old('featured') == 1 ) checked @endif > This is a
                                                        featured product</label>
                                                </div>
                                                <div class="col-md-12 form-group">
                                                    <label><input type="checkbox" name="mobile_app_deal"
                                                                  value="1"> @lang('specials.is-mobile-app-deal')
                                                    </label>
                                                </div>
                                                <div class="col-md-12 form-group">
                                                    <input type="hidden" name="ad_id" value="{{ $ad->id }}">
                                                    <button class="btn btn-success" type="submit"><i
                                                                class="fa fa-floppy-o"></i> Save changes
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                            <div role="tabpanel" class="tab-pane" id="custom-report">

                                <p>&nbsp;</p>

                                @if(Session::has('custom'))
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p>&nbsp;</p>
                                            <div class="table-responsive">
                                                <table class="table table-striped datatable-ads">
                                                    <thead>
                                                    <tr>
                                                        <th>&nbsp;</th>
                                                        <th>&nbsp;</th>
                                                        <th>Img</th>
                                                        <th>Position</th>
                                                        <th>Dept.</th>
                                                        <th>Description</th>
                                                        <th>Size</th>
                                                        <th>Net Cost</th>
                                                        <th>Regular Retail</th>
                                                        <th>Ad Retail</th>
                                                        <th>UPC</th>
                                                        <th>Comments</th>
                                                        <th>Item Info Location</th>
                                                        <th>Order #</th>
                                                        <th>Lmt</th>
                                                        <th>Yield</th>
                                                        <th>Adj Cost</th>
                                                        <th>Ad Gross</th>
                                                        <th>Supplier</th>
                                                        <th>Status</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach(Session::get('custom') as $special)
                                                        <tr>
                                                            <td class="text-center">
                                                                {{ $special->id }}
                                                            </td>
                                                            <td class="text-center">
                                                                <p>
                                                                <form action="{{ action('SpecialsController@destroy', $special->id) }}"
                                                                      method="post"
                                                                      onsubmit="return confirm('Are you sure?')">
                                                                    {{ csrf_field() }}
                                                                    {{ method_field('DELETE') }}
                                                                    <button class="btn-xs btn-danger" type="submit">
                                                                        <i class="fa fa-trash"></i>
                                                                    </button>
                                                                </form>
                                                                </p>
                                                                <p>
                                                                    <a href="{{ action('SpecialsController@clone', $special->id) }}">
                                                                        <button class="btn-xs btn-primary"><i
                                                                                    class="fa fa-copy"></i></button>
                                                                    </a>
                                                                </p>
                                                            </td>
                                                            <td class="text-center">
                                                                @if($special->image == "")
                                                                    <a href="{{ action('SpecialsController@edit', $special->id) }}">
                                                                        <img width="50" src="/img/pending-image.jpg"
                                                                             alt="{{ $special->item_description }}">
                                                                    </a>
                                                                @else
                                                                    <a href="{{ action('SpecialsController@edit', $special->id) }}">
                                                                        <img width="50" src="{{ $special->image }}"
                                                                             alt="">
                                                                    </a>
                                                                @endif
                                                            </td>
                                                            <td>{{ $special->position }}</td>
                                                            <td>
                                                                @foreach($special->department as $dept)
                                                                    [{{ $dept->name }}]
                                                                @endforeach
                                                            </td>
                                                            <th>
                                                                <a href="{{ action('SpecialsController@edit', $special->id) }}">
                                                                    {{ $special->item_description }}
                                                                </a>
                                                            </th>
                                                            <td class="text-center">{{ $special->size }}</td>
                                                            <td class="text-center">{{ $special->cost }}</td>
                                                            <td class="text-center">{{ $special->srp }}</td>
                                                            <td class="text-center">{{ $special->price }}</td>
                                                            <td class="text-center">
                                                                <p>{{ $special->sku }}</p>
                                                                @if($special->additional_upcs != "")
                                                                    <b>Additional UPCs</b><br/>
                                                                    {{ $special->additional_upcs }}
                                                                @endif
                                                            </td>
                                                            <td>
                                                                {{ $special->comments }}
                                                            </td>
                                                            <td>{{ $special->item_info_location }}</td>
                                                            <td>{{ $special->order_no }}</td>
                                                            <td>{{ $special->limit }}</td>
                                                            <td>{{ $special->yield }}</td>
                                                            <td>{{ $special->adj_cost }}</td>
                                                            <td>{{ $special->ad_gross }}</td>
                                                            <th>{{ $special->supplier }}</th>
                                                            <td>{{ $special->status }}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                @endif

                                <div class="row">
                                    <div class="col-md-6 col-md-offset-3">
                                        <ul class="list-group">
                                            @foreach($departments as $department)
                                                <li class="list-group-item"><input onchange="custom_categories()"
                                                                                   class="custom-category"
                                                                                   type="checkbox"
                                                                                   value="{{ $department->id }}"> {{ $department->name }}
                                                </li>
                                            @endforeach
                                        </ul>
                                        <form action="{{ action('AdsController@custom_report', $ad->id) }}"
                                              method="post">
                                            {{ csrf_field() }}
                                            <div class="checkbox">
                                                <label><input type="checkbox" name="custom_excel" value="1"> Export to
                                                    Excel</label><br/>
                                            </div>
                                            <input type="hidden" name="categories" id="custom" value="">
                                            <button class="btn btn-success form-control">Generate Report</button>
                                        </form>
                                    </div>
                                </div>

                            </div>

                            <div role="tabpanel" class="tab-pane active" id="all-items">

                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="table-responsive">
                                            <table class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th>&nbsp;</th>
                                                    <th>Img</th>
                                                    <th>Position</th>
                                                    <th>Featured</th>
                                                    <th>Dept.</th>
                                                    <th>Description</th>
                                                    <th>Size</th>
                                                    <th>Net Cost</th>
                                                    <th>Regular Retail</th>
                                                    <th>Ad Retail</th>
                                                    <th>UPC</th>
                                                    <th>PLU Required</th>
                                                    <th>Comments</th>
                                                    <th>Item Info Location</th>
                                                    <th>Order #</th>
                                                    <th>Lmt</th>
                                                    <th>Yield</th>
                                                    <th>Adj Cost</th>
                                                    <th>Ad Gross</th>
                                                    <th>Supplier</th>
                                                    <th>Status</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($ad->specials as $special)
                                                    <tr class="{{ $class[$special->status] }}">
                                                        <td>
                                                            <div class="btn-group">
                                                                <button class="btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                                                                    <i class="fa fa-chevron-down"></i> Actions
                                                                </button>
                                                                <ul class="dropdown-menu">
                                                                    <li><a target="_blank" href="{{ route('digital-signage-image', [$special->id, 'digital-signage']) }}" > Digital Signage Image</a></li>
                                                                    <li><a target="_blank" href="{{ route('digital-signage-image', [$special->id, 'facebook']) }}" > Facebook Image</a></li>
                                                                    <li><a target="_blank" href="{{ route('digital-signage-image', [$special->id, 'billboard']) }}" > Billboard Image</a></li>
                                                                </ul>
                                                            </div>
                                                        </td>
                                                        <td class="text-center">
                                                            @if($special->image == "")
                                                                <a href="{{ action('SpecialsController@edit', $special->id) }}">
                                                                    <img width="50" src="/img/pending-image.jpg" alt="{{ $special->item_description }}">
                                                                </a>
                                                            @else
                                                                <a href="{{ action('SpecialsController@edit', $special->id) }}">
                                                                    <img width="50" src="{{ $special->image }}" alt="">
                                                                </a>
                                                        @endif
                                                        </td>
                                                        <td class="text-center">{{ $special->position_number }}</td>
                                                        <td>
                                                            @if($special->featured == 1)
                                                                <span class="badge">Yes</span>
                                                            @else
                                                                No
                                                            @endif
                                                        </td>
                                                        <td>
                                                    @foreach($special->department as $department)
                                                            [{{ $department->name }}]
                                                        @endforeach
                                                        </td>
                                                        <th>
                                                            <a href="{{ action('SpecialsController@edit', $special->id) }}">
                                                                {{ $special->item_description }}
                                                            </a>
                                                        </th>
                                                        <td class="text-center">{{ $special->size }}</td>
                                                        <td class="text-center">{{ $special->cost }}</td>
                                                        <td class="text-center">{{ $special->srp }}</td>
                                                        <td class="text-center">{{ $special->price }}</td>
                                                        <td class="text-center">
                                                        <p>{{ $special->sku }}</p>
                                                    @if($special->additional_upcs != "")
                                                            <b>Additional UPCs</b><br />
                                                        {{ $special->additional_upcs }}
                                                        @endif
                                                        </td>
                                                        <td>
                                                    @if($special->plu_required == 1)
                                                            Yes <br /><br />
                                                        <b>{{ $special->plu }}</b>
                                                    @else
                                                            No
                                                        @endif
                                                        </td>
                                                        <td>
                                                        {{ $special->comments }}
                                                    @if( $special->public_comment != "" )
                                                            <i>Public Comment: {{ $special->public_comment }}</i>
                                                        @endif
                                                        </td>
                                                        <td>{{ $special->item_info_location }}</td>
                                                        <td>{{ $special->order_no }}</td>
                                                        <td>{{ $special->limit }}</td>
                                                        <td>{{ $special->yield }}</td>
                                                        <td>{{ $special->adj_cost }}</td>
                                                        <td>{{ $special->ad_gross }}</td>
                                                        <th>{{ $special->supplier }}</th>
                                                        <td>{{ $special->status }}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="panel panel-primary">
                                            <div class="panel-heading">
                                                Export to Gallery
                                            </div>
                                            <div class="panel-body">
                                                <form action="{{ route('export-to-gallery') }}" method="post">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="ad" value="{{ $ad->id }}" required >
                                                    <div class="form-group">
                                                        <label for="gallery">Gallery</label>
                                                        <select name="gallery" id="gallery" class="form-control" required>
                                                            <option value="">Select...</option>
                                                            @foreach(\App\Gallery::all() as $gallery)
                                                            <option value="{{ $gallery->id }}">{{ $gallery->gallery_name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="formatting">Format</label>
                                                        <select name="formatting" id="formatting" class="form-control" required>
                                                            <option value="">Select...</option>
                                                            <option value="billboard">Billboard</option>
                                                            <option value="facebook">Facebook</option>
                                                            <option value="digital-signage">Digital Signage</option>
                                                        </select>
                                                    </div>
                                                    <button class="btn btn-success btn-block">
                                                        Export
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>
                <div role="tabpanel" class="tab-pane" id="images">
                    <div class="panel-body">
                        <form action="{{ action('AdpagesController@store', $ad->id) }}" enctype="multipart/form-data"
                              method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="ad" value="{{ $ad->id }}">
                            <div class="form-group">
                                <label for="image">Add page</label>
                                <input type="file" class="form-control" name="image" required>
                            </div>
                            <button class="btn btn-success" type="submit"><i class="fa fa-upload"></i> Upload Page
                            </button>
                        </form>
                        <hr>
                        <div class="row">
                            @foreach($ad->pages as $page)
                                <div class="col-md-3">
                                    <p>
                                        <a target="_blank" href="{{ $page->image }}">
                                            <img src="{{ $page->image }}" alt="{{ $ad->name }}" class="img-responsive">
                                        </a>
                                    </p>
                                    <form action="{{ action('AdpagesController@destroy', $page->id) }}" method="post"
                                          onsubmit="return confirm('Are you sure?')">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <p class="text-center">
                                            <button class="btn btn-danger">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </p>
                                    </form>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="pdf">
                    <div class="panel-body">

                        <form action="{{ action('AdfilesController@store') }}" method="post"
                              enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="ad" value="{{ $ad->id }}">
                            <div class="form-group">
                                <label for="file"> Add File</label>
                                <input type="file" class="form-control" name="file" required>
                            </div>
                            <button class="btn btn-success" type="submit"><i class="fa fa-upload"></i> Upload File
                            </button>
                        </form>
                        <hr>
                        <div class="table-responsive">
                            <table class="table table-condensed table-striped">
                                <thead>
                                <tr>
                                    <th>File</th>
                                    <th>&nbsp;</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($ad->files as $file)
                                    <tr>
                                        <td>
                                            <a href="{{ $file->file }}" target="_blank">
                                                {{ $file->file }}
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            <form action="{{ action('AdfilesController@destroy', $file->id) }}"
                                                  method="post" onsubmit="return confirm('Are you sure?')">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button class="btn btn-danger" type="submit">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="banners">
                    <div class="panel-body">
                        <form action="{{ action('AdBannersController@store') }}" method="post"
                              enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="ad_id" value="{{ $ad->id }}">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    Add A New Banner
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label for="file">File</label>
                                        <input type="file" class="form-control" name="file" required>
                                    </div>
                                    <button class="btn btn-success">
                                        Upload
                                    </button>
                                </div>
                            </div>
                        </form>

                        @foreach($ad->banners as $banner)
                            <div class="panel panel-primary">
                                <div class="panel-body">
                                    <img src="{{ $banner->file }}" alt="Ad Banner" class="img-responsive">
                                </div>
                                <div class="panel-footer">
                                    <a href="#"
                                       onclick="document.getElementById('delete-banner-{{ $banner->id }}').submit();">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                    <form action="{{ action('AdBannersController@destroy', $banner->id) }}"
                                          id="delete-banner-{{ $banner->id }}" method="post">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                    </form>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

            </div>

        </div>
    </div>

@endsection

@section('js')

    <script>

        function confirm_text() {
            return confirm("Are you sure you want to send?");
        }

        $(document).ready(function () {

            var events = $('#events');

            var table = $('.datatable-ads').DataTable({
                'columnDefs': [{
                    'targets': 0,
                    'searchable': false,
                    'orderable': false,
                    'className': 'dt-body-center',
                    'render': function (data, type, full, meta) {
                        return '<input onchange="clones()" item-id="' + data + '" class="item-id" type="checkbox" value="' + data + '" />';
                    }
                }],
                dom: 'fBprtpi',
                pageLength: 50,
                buttons: [
                    {
                        extend: 'print',
                        exportOptions: {
                            columns: ':visible'

                        }
                    },
                    {
                        extend: 'copy',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'csv',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'excel',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'pdf',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },

                    "colvis"
                ]

            });

            $('.collapsed').click(function () {
                var tabName = $(this).attr('href');
                Cookies.set('activeTab', tabName);
            });

            var activeTab = Cookies.get('activeTab');

            if (activeTab != null) {
                $('.panel-collapse').removeClass('in');
                $(activeTab).addClass('in');
            }

        });

        $('.meats-math').change(function () {

            var ad_retail_val = $('#price').val();
            var yield_val = $('#yield').val();
            var cost_val = $('#cost').val();

            var adj_cost_val = (parseFloat(cost_val) / parseFloat(yield_val)) * 100;

            var ad_gross = ((parseFloat(ad_retail_val) - parseFloat(adj_cost_val)) / parseFloat(ad_retail_val) * 100);

            $('#adj_cost').attr("value", numeral(adj_cost_val).format('0.00'));
            $('#ad_gross').attr("value", numeral(ad_gross).format('0.00'));

        });

        function clones() {
            items = [];

            var checkboxes = $('.item-id:checked');

            $.each(checkboxes, function (index, elem) {
                items.push($(elem).val());
            });

            $('#clone_items').val(items);
        }

        function validateMultipleCloning() {
            var clone_items = $('#clone_items').val();

            if (clone_items == "") {
                alert('There is no selected items');
                return false;
            }

            var x = confirm('Are you sure?');

            if (x == false) {
                return false;
            }
        }

        function custom_categories() {
            categories = [];

            var checkboxes = $('.custom-category:checked');

            $.each(checkboxes, function (index, elem) {
                categories.push($(elem).val());
            });

            $('#custom').val(categories);
        }

    </script>

@endsection