@extends('layouts.app-wo-menu')

@section('content')

    <p>&nbsp;</p>

    <div class="container">

        @if( count($currents) < 1 )

        <div class="alert alert-info">
            <p class="text-center">There are currently no ads available for download.  Please check back soon for the next ad!</p>
        </div>

        @else

            @foreach($currents as $current)

            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="text-align: center"><a style="text-transform: uppercase" href="/ads/pages/{{ $current->id }}">{{ $current->name }}</a></div>

                        <div class="panel-body">

                            <div class="row">
                                <div class="col-md-4 col-xs-6 col-md-offset-4 col-xs-offset-3">
                                    <p>
                                        <a href="/ads/pages/{{ $current->id }}">
                                            <img src="{{ $current->cover }}" alt="{{ $current->name }}" class="img-responsive">
                                        </a>
                                    </p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <p><a href="/ads/pages/{{ $current->id }}" class="btn btn-primary form-control">View ad</a></p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            @endforeach

        @endif

    </div>

    @endsection