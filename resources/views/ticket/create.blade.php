@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <form action="{{ action('TicketController@store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="panel panel-default">
                <div class="panel-heading">
                    @lang('ticket.import-tickets-list')
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="file">@lang('ticket.file')</label>
                        <input type="file" class="form-control" name="file" required >
                    </div>
                    <button class="btn btn-success btn-block">
                        @lang('ticket.import')
                    </button>
                </div>
            </div>
        </form>

        <div class="panel panel-default">
            <div class="panel-heading">@lang('ticket.tickets-list')</div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-condensed">
                        <thead>
                        <tr>
                            <th>@lang('ticket.location')</th>
                            <th>@lang('ticket.event')</th>
                            <th>@lang('ticket.event-date')</th>
                            <th>@lang('ticket.sale-date')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach(\App\Ticket::all() as $ticket)
                        <tr>
                            <td>{{ $ticket->location }}</td>
                            <td>{{ $ticket->event }}</td>
                            <td>{{ $ticket->event_date }}</td>
                            <td>{{ $ticket->sale_date }}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @endsection