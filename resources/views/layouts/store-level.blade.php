@extends('layouts.app')

@section('content')

    <div class="container-fluid">

        <h3 class="text-center">@lang('general.welcome', ['name' => auth()->user()->name])</h3>

        <p class="text-center">
            <b>@lang('general.your-store')</b><br />
            {{ auth()->user()->store_data->brands->name }}<br />
            <i>{{ auth()->user()->store_data->name }}</i><br />
        </p>

        <p class="text-center">
            <a class="badge" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">
                <i class="fa fa-sign-out"></i> @lang('nav.logout')
            </a>
        </p>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>

        <p class="text-center">
            <a href="/@lang('nav.language-code')"><i class="fa fa-globe"></i> @lang('nav.change-language')</a>
        </p>

        <hr>
    </div>

    @yield('store-level-content')

    @endsection