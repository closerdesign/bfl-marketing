@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <p class="lead text-center">
            Commitment To Win (CTW)<br />
            Heatmap
        </p>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th class="text-center">Store / Area</th>
                    @foreach( $areas as $area )
                    <th class="text-center">
                        {{ $area->name }}
                    </th>
                    @endforeach
                </tr>
                </thead>
                <tbody>
                @foreach($stores as $store)
                <tr>
                    <th class="text-center">{{ $store->store_code }}</th>
                    @php $i = 0 @endphp
                    @foreach($areas as $area)
                    <td class="text-center {{ $scores->where('area', $area->id)->where('store', $store->store_code)->avg('score') > 1 ? 'success' : ($scores->where('area', $area->id)->where('store', $store->store_code)->avg('score') < .5 ? 'danger' : 'warning') }}">
                        @if( $scores->where('area', $area->id)->where('store', $store->store_code)->first() ) <a href="#" data-toggle="modal" data-target="#modal-scores-{{ $i }}"> @endif
                        {{ number_format($scores->where('area', $area->id)->where('store', $store->store_code)->avg('score'), 2) }}
                        @if( $scores->where('area', $area->id)->where('store', $store->store_code)->first() ) </a> @endif
                        <!-- Modal -->
                        <div class="modal fade" id="modal-scores-{{ $i }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">{{ $area->name }}</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="table-responsive">
                                            <table class="table">
                                                @foreach($scores->where('area', $area->id) as $s)
                                                @php $scales = [0=>'Needs Improvement', 1=>'Near Model', 2=>'Model'] @endphp
                                                @php $styles = [0=>'danger', 1=>'warning', 2=>'success']; @endphp
                                                <tr class="{{ $styles[$s->score] }}">
                                                    <td class="text-left">{{ $s->topic_name }}</td>
                                                    <td class="text-center">
                                                        {{ $scales[$s->score] }}
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                        <p class="text-center">
                                            <b>{{ $area->name }} score: {{ number_format($scores->where('area', $area->id)->avg('score'),2) }}</b>
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                    @php $i++ @endphp
                    @endforeach
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    @endsection