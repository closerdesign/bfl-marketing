@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <p class="lead text-center">
            Commitment To Win<br />
            {{ $evaluation->store->store_code }} {{ $evaluation->store->brands->name }} {{ $evaluation->store->name }}
        </p>
        <p class="text-center">
            @lang('ctw-evaluation.started'): {{ $evaluation->created_at }}
        </p>

        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                @foreach( \App\CtwArea::whereHas('brands', function($q) use ($evaluation){ return $q->where('brands.id', $evaluation->store->brands->id); })->orderBy('name')->get() as $area )
                    <div class="panel panel-default">
                        <div class="panel-heading text-center">
                            {{ $area->name }}
                        </div>
                        <div class="panel-body">
                            @foreach($area->topics as $topic)
                            <rating-component :topic="'{{ $topic }}'" :evaluation="'{{ $evaluation->id }}'"></rating-component>
                            @endforeach
                        </div>
                    </div>
                @endforeach

                <form action="{{ route('ctw-evaluation.mark-as-completed', $evaluation->id) }}" method="post">
                    @csrf
                    <button class="btn btn-success btn-block">
                        @lang('ctw-evaluation.mark-as-completed')
                    </button>
                </form>
            </div>
        </div>

    </div>

    @endsection