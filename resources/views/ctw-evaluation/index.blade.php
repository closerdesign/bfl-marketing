@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <p class="lead">@lang('ctw-evaluation.title')</p>
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>@lang('ctw-evaluation.store')</th>
                    <th>@lang('ctw-evaluation.user')</th>
                    <th>@lang('ctw-evaluation.created')</th>
                    <th>@lang('ctw-evaluation.status')</th>
                </tr>
                </thead>
                <tbody>
                @foreach($evaluations as $evaluation)
                <tr>
                    <td>{{ $evaluation->store->store_code }} {{ $evaluation->store->brands->name }} {{ $evaluation->store->name }}</td>
                    <td>{{ $evaluation->user->name }}</td>
                    <td>{{ $evaluation->created_at }}</td>
                    <td>
                        @if($evaluation->status == 1)
                            <a href="{{ route('ctw-evaluation.show', $evaluation->id) }}">
                                @lang('ctw-evaluation.show-results')
                            </a>
                        @else
                            <a href="{{ route('ctw-evaluation.edit', $evaluation->id) }}">
                                @lang('ctw-evaluation.continue')
                            </a>
                        @endif
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
            {{ $evaluations->render() }}
        </div>
    </div>

    @endsection