@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <p class="lead text-center">
                    @lang('ctw-evaluation.start')
                </p>
                <form action="{{ route('ctw-evaluation.store') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="store_id">@lang('ctw-evaluation.store')</label>
                        <select name="store_id" id="store_id" class="form-control">
                            <option value="">@lang('general.select')...</option>
                            @foreach(\App\Store::active()->orderBy('store_code')->get() as $store)
                                <option @if( old('comments') == $store->id ) selected @endif value="{{ $store->id }}">{{ $store->store_code }} - {{ $store->brands->name }} {{ $store->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="comments">@lang('ctw-evaluation.comments')</label>
                        <textarea name="comments" id="comments" cols="30" rows="10" class="form-control">{{ old('comments') }}</textarea>
                    </div>
                    <button class="btn btn-success btn-block">
                        @lang('general.start')
                    </button>
                </form>
            </div>
        </div>
    </div>

    @endsection