@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <p class="lead text-center">
            Commitment To Win Evaluation<br />
            {{ $evaluation->store->brands->name }} - {{ $evaluation->store->name }} ({{ $evaluation->store->store_code }})
        </p>
        @foreach(\App\CtwArea::whereHas('brands', function($q) use ($evaluation){ return $q->where('brands.id', $evaluation->store->brands->id);; })->orderBy('name')->get() as $area)
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th class="text-center">{{ $area->name }}</th>
                    <th class="text-center">@lang('ctw-evaluation.score')</th>
                </tr>
                </thead>
                <tbody>
                @php $topics = []; @endphp
                @foreach($area->topics as $topic)
                @if( $evaluation->scores->where('topic_id', $topic->id)->first() )
                <tr class="{{ $evaluation->scores->where('topic_id', $topic->id)->first()->score_class }}">
                    <td><b>{{ $topic->topic }}</b> @if( $evaluation->scores->where('topic_id', $topic->id)->first()->comments != null ) <br /><i>{{ $evaluation->scores->where('topic_id', $topic->id)->first()->comments }}</i> @endif</td>
                    <td width="30%" class="text-right">
                        {{ $evaluation->scores->where('topic_id', $topic->id)->first()->score_label }}
                    </td>
                </tr>
                @endif
                @php array_push($topics, $topic->id) @endphp
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <td>@lang('ctw-evaluation.score')</td>
                    <td class="text-right">
                        {{ number_format($evaluation->scores->whereIn('topic_id', $topics)->avg('score'), 2) }}
                    </td>
                </tr>
                </tfoot>
            </table>
        </div>
        @endforeach
        <p class="lead text-right">Global Score: {{ number_format($evaluation->scores->avg('score'), 2) }}</p>
    </div>

    @endsection