@extends('layouts.app')

@section('content')

    <div class="container">
        <form action="{{action('LoyaltyController@register_user')}}" method="post">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="FirstName">First Name</label>
                <input class="form-control" name="FirstName" id="FirstName" type="text">
            </div>
            <div class="form-group">
                <label for="LastName">Last Name</label>
                <input name="LastName" class="form-control" id="LastName" type="text">
            </div>
            <div class="form-group">
                <label for="UserName">User Name</label>
                <input class="form-control" name="UserName" id="UserName" type="text">
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input class="form-control" id="password" name="Password" type="password">
            </div>
            <div class="form-group">
                <label for="zipcode">Password</label>
                <input class="form-control" id="zipcode" name="ZipCode" type="text">
            </div>

            <input type="submit">
        </form>

        <div>
            @if(isset($reponse))
                <?php print_r($response)?>
            @endif
        </div>

    </div>

    -----------------------------------------------

    <div class="container">
        <form action="{{action('LoyaltyController@validate_user')}}" method="post">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="UserName">User Name</label>
                <input class="form-control" name="UserName" id="UserName" type="text">
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input class="form-control" id="password" name="Password" type="password">
                <input type="hidden" name="ClientStoreId" value="2">
                <input type="hidden" name="store_code" value="3501">
            </div>
            <input type="submit">
        </form>

        <div>
            @if(isset($reponse))
                <?php print_r($response)?>
            @endif
        </div>

    </div>

    -----------------------------------------------

    <div class="container">
        <form action="{{action('LoyaltyController@get_coupons')}}" method="post">
            {{ csrf_field() }}

            <div class="form-group">
                 <label>Get Coupons</label>

                <input type="hidden" name="UserToken" value="873b0a64-9760-4de3-8115-774d5641ae1d">
            </div>
            <input type="submit">
        </form>

        <div>
            @if(isset($reponse))
                <?php print_r($response)?>
            @endif
        </div>

    </div>

    --------------------------------------------

    <div class="container">
        <form action="{{action('LoyaltyController@get_coupons')}}" method="post">
            {{ csrf_field() }}

            <div class="form-group">
                <label>Get Coupons</label>

                <input type="hidden" name="UserToken" value="873b0a64-9760-4de3-8115-774d5641ae1d">
            </div>
            <input type="submit">
        </form>

        <div>
            @if(isset($reponse))
                <?php print_r($response)?>
            @endif
        </div>

    </div>

    ------------------------------------------

    <div class="container">
        <form action="{{action('LoyaltyController@get_user_clips')}}" method="post">
            {{ csrf_field() }}

            <div class="form-group">
                <label>Get User Clipped Coupons</label>

                <input type="hidden" name="UserToken" value="873b0a64-9760-4de3-8115-774d5641ae1d">
            </div>
            <input type="submit">
        </form>

        <div>
            @if(isset($reponse))
                <?php print_r($response)?>
            @endif
        </div>

    </div>

@endsection