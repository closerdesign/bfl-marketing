@extends('layouts.app')

@section('content')

    <div class="container">
        <form action="{{ action('ProductsController@store') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="panel panel-primary">
                <div class="panel-heading">Create A New Product</div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="upc">UPC</label>
                        <input type="number" class="form-control" name="upc" value="{{ old('upc') }}" required >
                    </div>
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="name" value="{{ old('name') }}" required >
                    </div>
                    <div class="form-group">
                        <label for="brand_name">Brand</label>
                        <input type="text" class="form-control" name="brand_name" value="{{ old('brand_name') }}">
                    </div>
                    <div class="form-group">
                        <label for="size">Size</label>
                        <input type="text" class="form-control" name="size" value="{{ old('size') }}">
                    </div>
                    <div class="form-group">
                        <label for="description">Description (Optional)</label>
                        <textarea name="description" id="description" cols="30" rows="10"
                                  class="form-control summernote">{{ old('description') }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="image">Image</label>
                        <input type="file" class="form-control" name="image" >
                    </div>
                    <div class="form-group">
                        <label for="categories">Categories</label>
                        <select name="categories[]" id="categories" class="form-control multiple-select" multiple="multiple" >
                            @foreach(\App\Category::where('parent_id', '>', 0)->get() as $category)
                                <option value="{{ $category->id }}">{{ $category->name }} @if($category->has('parent')) [{{ $category->parent->name }}] @endif</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <button class="btn btn-success pull-right" type="submit">
                <i class="fa fa-save"></i> Save changes
            </button>
        </form>
    </div>

    @endsection