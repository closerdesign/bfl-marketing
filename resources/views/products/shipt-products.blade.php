UPC,StoreLocationId,ProductName,BrandName,Description,ProductSize,UnitOfMeasure,PricedByWeight,ProductCategory,ImageURL
@foreach($products as $product)
    @if( $product->product['name'] != "" )
        {{ $product->sku }},{{ $product->store_number }},{{ str_replace(",", " ", $product->product['name']) }},{{ str_replace(",", " ", $product->product['brand_name']) }},{{ str_replace(",", " ", $product->product['description']) }},{{ $product->size }},@if( $product->uom == "ZZZ" ) EA @else {{ $product->uom }} @endif,@if($product->scale_flag == 1) TRUE @else FALSE @endif,@if( !empty($product->product->categories) ) {{ $product->product->categories->first()['id'] }} @endif,{{ $product->product['image'] }}
    @endif
@endforeach