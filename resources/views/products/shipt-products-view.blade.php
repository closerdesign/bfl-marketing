@extends('layouts.app')

@section('content')

    <div class="container">
        <form action="">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Shipt Products Report
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="store">Select Store</label>
                        <select name="store" id="store" class="form-control" required >
                            <option value="">Select...</option>
                            @foreach(\App\Store::orderBy('store_code')->get() as $store)
                                <option value="{{ $store->store_code }}">{{ $store->store_code  }} {{ $store->name }} - {{ $store->brands->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <button class="btn btn-success">Generate Report</button>
        </form>
    </div>

    @endsection