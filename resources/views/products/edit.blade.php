@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="row">
            <div class="col-md-8">
                <p class="text-right">
                    <img src="{{ $product->image }}" alt="{{ $product->name }}" width="90">
                </p>
                <form action="{{ action('ProductsController@update', $product->id) }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}
                    <div class="panel panel-primary">
                        <div class="panel-heading">Edit Product: {{ $product->name }}</div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="upc">UPC</label>
                                <input type="number" class="form-control" value="{{ $product->upc }}" required readonly >
                            </div>
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" value="{{ $product->name }}" required >
                            </div>
                            <div class="form-group">
                                <label for="brand_name">Brand</label>
                                <input type="text" class="form-control" name="brand_name" value="{{ $product->brand_name }}">
                            </div>
                            <div class="form-group">
                                <label for="size">Size</label>
                                <input type="text" class="form-control" name="size" value="{{ $product->size }}">
                            </div>
                            <div class="form-group">
                                <label for="description">Description (Optional)</label>
                                <textarea name="description" id="description" cols="30" rows="10"
                                          class="form-control summernote">{!! $product->description !!}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="image">Image</label>
                                <input type="file" class="form-control" name="image" >
                            </div>
                            <div class="form-group">
                                <label for="categories">Categories</label>
                                <select name="categories[]" id="categories" class="form-control multiple-select" multiple="multiple" >
                                    @foreach( \App\Category::where('parent_id', '>', 0)->get() as $category )
                                        <option @if( in_array($category->id, $product->categories()->select('category_id')->pluck('category_id')->toArray()) ) selected @endif value="{{ $category->id }}">{{ $category->name }} @if($category->has('parent')) [{{ $category->parent->name }}] @endif</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="keywords">Keywords</label>
                                <textarea name="keywords" id="keywords" cols="30" rows="10"
                                          class="form-control">{{ $product->keywords }}</textarea>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-success pull-right" type="submit">
                        <i class="fa fa-save"></i> Save changes
                    </button>
                </form>
            </div>
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <i class="fa fa-image"></i> Kwikee Images
                    </div>
                    <div class="panel-body">
                        <ul class="media-list">
                            @foreach($product->images as $image)
                                <li class="media">
                                    <div class="media-left">
                                        <a href="#">
                                            @if( ($image->type == 'JPG') || ($image->type == 'PNG') || ($image->type == 'GIF') )
                                            <img width="90" class="media-object" src="{{ URL::to('kwikee-products/image-display?url=' . urlencode($image->url)) }}">
                                            @endif
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading">{{ $image->type }}</h4>
                                        <ul class="list-group">
                                            @if( ($image->type == 'JPG') || ($image->type == 'PNG') || ($image->type == 'GIF') )
                                            <li class="list-group-item"><a href=""><i class="fa fa-image"></i> Make Product Image</a></li>
                                            @endif
                                            <li class="list-group-item"><a target="_blank" href="{{ URL::to('kwikee-products/image-display?url=' . urlencode($image->url)) }}"><i class="fa fa-download"></i> Download</a></li>
                                        </ul>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection