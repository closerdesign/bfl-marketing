@extends('layouts.app')

@section('content')
    
    <div class="container-fluid">
        <p class="text-right">
            <a href="{{ action('ProductsController@create') }}" class="btn btn-success">
                <i class="fa fa-plus"></i> Add A New Product
            </a>
        </p>
        <div class="panel panel-default">
            <div class="panel-body">
                @if( Session::has('keyword') )
                <p class="lead">Results filtered by keyword: <b>"{{ Session::get('keyword') }}"</b></p>
                <form action="{{ action('ProductsController@filter') }}" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="keyword" value="">
                    <button class="btn btn-success">
                        <i class="fa fa-search"></i> Show All
                    </button>
                </form>
                @else
                <form action="{{ action('ProductsController@filter') }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="keyword">Search by UPC or Keyword</label>
                        <input type="text" class="form-control" name="keyword" required >
                    </div>
                    <button class="btn btn-success pull-right">
                        <i class="fa fa-search"></i> Search
                    </button>
                </form>
                @endif
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">Products Catalog</div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Image</th>
                            <th>UPC</th>
                            <th>Brand</th>
                            <th>Name</th>
                            <th>Size</th>
                            <th>Categories</th>
                            <th>Available At</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $product)
                        <tr>
                            <td>
                                <p class="text-center">
                                    <a href="{{ action('ProductsController@edit', $product->id) }}">
                                        <div id="img_container_{{ $product->id }}">
                                            <img src="{{ $product->image }}" alt="{{ $product->name }}" width="90" />
                                        </div>
                                    </a>
                                    @if( basename($product->image)  != 'pending-image.jpg' )
                                        <br /><a target="_blank" href="{{ Storage::url('product-image-highres/' . basename($product->image)) }}"><i class="fa fa-camera-retro"></i></a>
                                    @endif
                                </p>
                            </td>
                            <td>{{ $product->upc }}</td>
                            <td>
                                {{ $product->brand_name }}
                            </td>
                            <th>
                                <a href="{{ action('ProductsController@edit', $product->id) }}">
                                    {{ $product->name }}
                                </a>
                                @if( count($product->categories) < 1 )
                                <div class="panel panel-default" id="categories-container-{{ $product->id }}">
                                    <div class="panel-body">
                                        <form id="categories-form-{{ $product->id }}" action="{{ action('ProductsController@ajax_categories', $product->id) }}" method="post">
                                            {{ csrf_field() }}
                                            <div class="form-group" style="margin-bottom: 5px">
                                                <label for="categories">Categories</label>
                                                <select name="categories[]" id="categories" class="form-control multiple-select" multiple="multiple" >
                                                    @foreach(\App\Category::where('parent_id', '>', 0)->orderBy('name')->get() as $category)
                                                        <option value="{{ $category->id }}">{{ $category->name }} @if($category->has('parent')) [{{ $category->parent->name }}] @endif</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <button class="btn btn-success form-control">Update</button>
                                        </form>
                                    </div>
                                </div>
                                @endif
                            </th>
                            <td>{{ $product->size }}</td>
                            <td>@foreach( $product->categories as $category) [{{ $category->name }}] @endforeach</td>
                            <td>
                                @foreach($product->items as $item)
                                    {{ $item->store_number }}<br />
                                @endforeach
                            </td>
                            <td>
                                <form id="upload{{ $product->id }}" action="{{ action('ProductsController@ajax_img', $product->id) }}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="image">Upload Image (Max: 5mb)</label>
                                        <div class="input-group">
                                            <input type="file" class="form-control" name="image" required>
                                            <span class="input-group-btn">
                                        <button type="submit" class="btn btn-success btn-send-image">
                                            <i class="fa fa-upload"></i>
                                        </button>
                                    </span>
                                        </div>
                                    </div>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $products->render() }}
            </div>
        </div>

    </div>
    
    @endsection

@section('js')

    @foreach($products as $product)
    <script>
        $('#upload{{ $product->id }}').validate({
            submitHandler: function(form){
                var formData = new FormData($('#upload{{ $product->id }}')[0]);
                $.ajax({
                    url: form.action,
                    type: "post",
                    data: formData,
                    async: false,
                    success: function(msg){
                        $('#img_container_{{ $product->id }}').html(msg);
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                })
            }
        });
    </script>
    @endforeach

    @foreach($products as $product)
    <script>
        $('#categories-form-{{ $product->id }}').validate({
            submitHandler: function(form){
                $.post(form.action,$('#categories-form-{{ $product->id }}').serialize())
                .done(function(msg){
                    $('#categories-container-{{ $product->id }}').fadeOut();
                    console.log(msg);
                });
            }
        });
    </script>
    @endforeach

    @endsection