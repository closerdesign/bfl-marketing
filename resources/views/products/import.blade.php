@extends('layouts.app')

@section('content')

    <div class="container">
        <form action="{{ action('ProductsController@file_import') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="panel panel-primary">
                <div class="panel-heading">Import Products File</div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="file">File</label>
                        <input type="file" class="form-control" name="file" required >
                    </div>
                    <button class="btn btn-success pull-right">
                        <i class="fa fa-upload"></i> Upload File & Import Products
                    </button>
                </div>
            </div>
        </form>
        <div class="panel panel-default">
            <div class="panel-heading"><i class="fa fa-info-circle"></i> How to use this?</div>
            <div class="panel-body">
                <p>Please create a Excel file (XLS or XLSX) including columns with names: <b>upc, name, brand_name and size</b>.</p>
                <p>Only numeric characters are going to be allowed as UPC.</p>
                <p>When UPC is already created, name, brand name and size fields will be overwritten using the provided information.</p>
                <p>You are no allowed to remove data. If you need to do it, please contact a member of the markerting team.</p>
                <p>Please revise carefully your spreadsheet before uploading.</p>
            </div>
        </div>
    </div>

    @endsection