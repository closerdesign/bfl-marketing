@extends('layouts.app')

@section('content')

<div class="container">
    <p class="text-right">
        <a href="{{ action('ProductsController@create') }}" class="btn btn-success">
            <i class="fa fa-plus"></i> Add A New Product
        </a>
    </p>
    <div class="panel panel-default">
        <div class="panel-body">

                <form action="{{ action('ProductsController@aws_internal_run') }}" method="post">
                    {{ csrf_field() }}

                    <div class="form-group col-md-3">
                        <label for="search">Search</label>
                        <input type="text" id="search" name="keyword" value="{{@Session::get('keyword')}}" class="form-control">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="size">Size</label>
                        <input type="text" name="size" id="size" value="{{@Session::get('size')}}" class="form-control pull-left" >

                    </div>
                    <div class="form-group col-md-3">
                        <label for="categories">Categories</label>
                        <select name="categories" id="categories" class="form-control" >
                            <option value=""></option>
                            @foreach(\App\Category::where('parent_id', '>', 0)->orderBy('name')->get() as $category)
                                <option value="{{ $category->name }}" @if(Session::get('categories') == $category->name ) SELECTED @endif >{{ $category->name }} @if($category->has('parent')) [{{ $category->parent->name }}] @endif</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-3">
                        <label for="upc">UPC </label>
                        <input type="checkbox" value="Y" name="upc" id="upc" class="form-control">
                    </div>
                    <div>
                        <button type="submit" class="btn btn-success pull-right"> <i class="fa fa-search"></i>
                            Search
                        </button>
                    </div>
                </form>
            <a href="{{action('ProductsController@clear_session')}}"><button class="btn btn-danger"><i class="fa fa-refresh"></i>Reset</button></a>
        </div>
    </div>
@if(!empty($products))
    <div class="panel panel-primary">
        <div class="panel-heading">Products Catalog</div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Image</th>
                        <th>UPC</th>
                        <th>Brand</th>
                        <th>Name</th>
                        <th>Size</th>
                        <th>Categories</th>
                        <th>Available At</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>

            @foreach($products as $p)

                    <tr>
                        <td>
                            <p class="text-center">
                                <a href="{{ action('ProductsController@edit', $p->fields->upc) }}">
                                    <div id="img_container_{{ $p->fields->upc }}">
                                        <img src="{{ @$p->fields->image }}" alt="{{ @$p->fields->name }}" width="90" />
                                    </div>
                                </a>
                                @if( basename($p->fields->image)  != 'pending-image.jpg' )
                                    <br /><a target="_blank" href="{{ Storage::url('product-image-highres/' . basename(@$p->fields->image)) }}"><i class="fa fa-camera-retro"></i></a>
                                @endif
                            </p>
                        </td>
                        <td>{{ $p->fields->upc }}</td>
                        <td>{{ @$p->fields->brand_name}}</td>
                        <td>
                            <a href="{{ action('ProductsController@edit', $p->fields->id) }}">
                                {{ @$p->fields->name }}
                            </a>
                            @if( empty(@$p->fields->categories))
                                <div class="panel panel-default" id="categories-container-{{ $p->fields->id }}">
                                    <div class="panel-body">
                                        <form id="categories-form-{{ $p->fields->id }}" action="{{ action('ProductsController@ajax_categories', $p->fields->id) }}" method="post">

                                            {{ csrf_field() }}
                                            <div class="form-group" style="margin-bottom: 5px">
                                                <label for="categories">Categories</label>
                                                <select name="categories[]" id="categories" class="form-control multiple-select" multiple="multiple" >
                                                    @foreach(\App\Category::where('parent_id', '>', 0)->orderBy('name')->get() as $category)
                                                        <option value="{{ $category->id }}">{{ $category->name }} @if($category->has('parent')) [{{ $category->parent->name }}] @endif</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <button class="btn btn-success form-control">Update</button>
                                        </form>
                                    </div>
                                </div>
                            @endif




                        </td>
                        <td>{{ @$p->fields->size }}</td>
                        <td>
                            @if(!empty($p->fields->categories))
                                @foreach(@$p->fields->categories as $cat)
                                    [{{ @$cat }}]<br />
                                @endforeach
                            @endif

                        </td>

                        <td>@if(!empty($p->fields->stores))
                            @foreach(@$p->fields->stores as $store)
                                [{{ @$store }}]<br />
                            @endforeach
                                @endif
                        </td>
                        <td>
                            <form id="upload{{ $p->fields->id }}" action="{{ action('ProductsController@ajax_img', $p->fields->id) }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="image">Upload Image (Max: 5mb)</label>
                                    <div class="input-group">
                                        <input type="file" class="form-control" name="image" required>
                                        <span class="input-group-btn">
                                        <button type="submit" class="btn btn-success btn-send-image">
                                            <i class="fa fa-upload"></i>
                                        </button>
                                    </span>
                                    </div>
                                </div>
                            </form>
                        </td>


                    </tr>


            @endforeach
                    </tbody>

            </table>
            </div>


        </div>

    </div>
    @endif

</div>

    @endsection


@section('js')
@if(isset($products))
    @foreach($products as $p)
        <script>
            $('#upload{{ $p->fields->id }}').validate({
                submitHandler: function(form){
                    var formData = new FormData($('#upload{{ $p->fields->id }}')[0]);
                    $.ajax({
                        url: form.action,
                        type: "post",
                        data: formData,
                        async: false,
                        success: function(msg){
                            $('#img_container_{{ $p->fields->id }}').html(msg);
                        },
                        cache: false,
                        contentType: false,
                        processData: false
                    })
                }
            });
        </script>
    @endforeach

    @foreach($products as $p)
        <script>
            $('#categories-form-{{ $p->fields->id }}').validate({
                submitHandler: function(form){
                    $.post(form.action,$('#categories-form-{{ $p->fields->id }}').serialize())
                        .done(function(msg){
                            $('#categories-container-{{ $p->fields->id }}').fadeOut();
                            console.log(msg);
                        });
                }
            });
        </script>
    @endforeach
   @endif

@endsection