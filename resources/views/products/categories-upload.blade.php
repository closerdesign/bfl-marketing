@extends('layouts.app')

@section('content')

    <div class="container">
        <form action="{{ action('ProductsController@categories_upload') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Category Bulk Upload
                </div>
                <div class="panel-body">
                    <div class="alert alert-info">
                        <p><i class="fa fa-info-circle"></i> Please use this process in order to connect UPCs to our product categories. Please include in your file the heading line upc so I can make sure you are uploading the right file.</p>
                    </div>
                    <div class="form-group">
                        <label for="file">File Upload</label>
                        <input type="file" class="form-control" name="file" required >
                        <label>Only xls and xlsx file are accepted.</label>
                    </div>
                    <div class="form-group">
                        <label for="category_id">Category</label>
                        <select name="category_id" class="form-control" required >
                            <option value="">Select...</option>
                            @foreach(\App\Category::where('parent_id', '>', 0)->orderBy('name')->get() as $category)
                                <option value="{{ $category->id }}">{{ $category->name }} @if($category->has('parent')) [{{ $category->parent->name }}] @endif</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <button class="btn btn-success">
                <i class="fa fa-upload"></i> Upload File
            </button>
        </form>
    </div>

    @endsection