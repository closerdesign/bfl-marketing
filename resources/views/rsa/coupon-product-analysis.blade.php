@extends('layouts.app')

@section('content')

    <p class="lead text-center">
        Coupon Product Analysis
    </p>

    <hr />

    <div class="container">

        <div class="text-right form-group">
            <a href="{{ url()->previous() }}" class="btn btn-default">Go Back</a>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <tr>
                            <th>
                                Redemption Coupon Amount
                            </th>
                            <td>
                                {{ $coupon->RedemptionCouponAmount }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Title
                            </th>
                            <td>
                                {{ $coupon->Title }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Totals UPCs Sold
                            </th>
                            <td>
                                {{ $coupon->TotalUPCsSold }}
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="col-md-6">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <tr>
                            <th>Product Name</th>
                            <th>UPC</th>
                            <th>Units</th>
                        </tr>
                        @foreach($coupon->ProductAnalysis as $product)
                            <tr>
                                <td>{{ $product->ProductName }}</td>
                                <td>{{ $product->UPC }}</td>
                                <td>{{ $product->Units }}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>

    @endsection