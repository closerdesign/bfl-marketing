@extends('layouts.app')

@section('content')

    <p class="lead text-center">Redemption Report</p>
    <hr>

    <div class="container">

        @if( $coupons !== null )
        <div class="text-right form-group">
            <a href="{{ action('RsaController@redemption_report') }}" class="btn btn-default">Go Back</a>
        </div>
        <div class="table-responsive">
            <table class="table table-striped datatable">
                <tr>
                    <th>Clips Count</th>
                    <th>Coupon Type ID</th>
                    <th>Expires On</th>
                    <th>RSA Coupon Expires On</th>
                    <th>RSA Coupon Valid From</th>
                    <th>RSA Offer ID</th>
                    <th>Redeem Count</th>
                    <th>Title</th>
                    <th>Valid From</th>
                </tr>
                @foreach($coupons as $coupon)
                <tr>
                    <td class="text-center">{{ $coupon->ClipsCount }}</td>
                    <td class="text-center">{{ $coupon->CouponTypeId }}</td>
                    <td>{{ $coupon->ExpiresOn }}</td>
                    <td>{{ $coupon->RSACouponExpiresOn }}</td>
                    <td>{{ $coupon->RSACouponValidFrom }}</td>
                    <td>{{ $coupon->RSAOfferId }}</td>
                    <td class="text-center">{{ $coupon->RedeemCount }}</td>
                    <td>
                        {{ $coupon->Title }}
                        @if( $coupon->RedeemCount > 0 )
                        <br /><a href="{{ action('RsaController@coupon_product_analysis', [$coupon->RSAOfferId, $_GET['brand']]) }}">View details...</a>
                        @endif
                    </td>
                    <td>{{ $coupon->ValidFrom }}</td>
                </tr>
                @endforeach
            </table>
        </div>

        @else

        <form action="">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="form-group">
                        <label for="start_date">Start Date</label>
                        <input type="date" name="start_date" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="end_date">End Date</label>
                        <input type="date" class="form-control" name="end_date" required >
                    </div>
                    <div class="form-group">
                        <label for="brand">Brand</label>
                        <select name="brand" id="brand" class="form-control" required >
                            <option value="">Select...</option>
                            <option value="bfl">Buy For Less / Smart Saver</option>
                            <option value="uptown">Uptown Grocery</option>
                        </select>
                    </div>
                    <button class="btn btn-success btn-block">
                        Search
                    </button>
                </div>
            </div>
        </form>

        @endif

    </div>

    @endsection