@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <p class="text-right"><a href="{{ action('CampaignItemsController@create') }}" class="btn btn-success"><i class="fa fa-plus-circle"></i> Create Item</a></p>

        <div class="form-group">
            <form action="">
                <div class="input-group">
                    <input type="text" class="form-control" name="keyword" placeholder="Search...">
                    <span class="input-group-btn">
                        <button class="btn btn-success">
                            Search
                        </button>
                    </span>
                </div>
            </form>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">Campaign Items</div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-condensed">
                        <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th>PLU</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Categories</th>
                            <th>Status</th>
                            <th>Store</th>
                            <th>Date</th>
                            <th>Time</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($items as $item)
                            <tr>
                                <td>
                                    <div class="btn-group">
                                        <button class="btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-chevron-down"></i> Actions
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="#" onclick="$('#delete-{{ $item->id }}').submit();">
                                                    Delete
                                                    <form id="delete-{{ $item->id }}" onsubmit="return confirm('Are you sure? This can\'t be undone.')" action="{{ action('CampaignItemsController@destroy', $item->id) }}" method="post">
                                                        {{ csrf_field() }}
                                                        {{ method_field('DELETE') }}
                                                    </form>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{ action('CampaignItemsController@duplicate', $item->id) }}">Duplicate</a>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                                <td>{{ $item->plu }}</td>
                                <th>
                                    <a href="{{ action('CampaignItemsController@edit', $item->id) }}">
                                        {{ $item->name }}
                                    </a>
                                </th>
                                <td>{{ $item->price }}</td>
                                <td>
                            @foreach($item->campaign_categories as $category)
                                    [{{ $category->name }}]
                                @endforeach
                                </td>
                                <td>{{ $item->status }}</td>
                                <td>@if( $item->store ) {{ $item->store->name }} @endif</td>
                                <td>{{ $item->date }}</td>
                                <td>{{ $item->time }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $items->appends(request()->except('page'))->render() }}
                </div>
            </div>
        </div>
    </div>

    @endsection