@extends('layouts.app')

@section('content')

    <div class="container">
        <form action="{{ action('CampaignItemsController@store') }}" method="post" enctype="multipart/form-data">
            <div class="panel panel-primary">
                <div class="panel-heading">Create Campaign Item</div>
                <div class="panel-body">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="name" value="{{ old('name') }}" required />
                    </div>
                    <div class="form-group">
                        <label for="image">Image (500 x 500)</label>
                        <input type="file" class="form-control" name="image" />
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea name="description" id="description" cols="30" rows="10"
                                  class="form-control summernote">{{ old('description') }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="directions">Directions</label>
                        <textarea name="directions" id="directions" cols="30" rows="10"
                                  class="form-control summernote">{{ old('directions') }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="campaign_category_id">Categories</label>
                        <select name="campaign_category_id[]" id="campaign_category_id"
                                class="form-control multiple-select" multiple="multiple">
                            <option value="">Select...</option>
                            @foreach(\App\CampaignCategory::all() as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="price">Price</label>
                        <input type="text" class="form-control" name="price" required >
                    </div>
                    <div class="form-group">
                        <label for="plu">PLU</label>
                        <input type="text" class="form-control" name="plu" value="{{ old('plu') }}" >
                    </div>
                    <div class="form-group">
                        <label for="status">Status</label>
                        <select name="status" id="status" class="form-control" required >
                            <option value="">Select...</option>
                            <option @if( old('status') == 1 ) selected @endif value="1">Active</option>
                            <option @if( old('status') == 0 ) selected @endif value="0">Inactive</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="date">@lang('campaign-items.date')</label>
                        <input type="text" class="form-control datepicker" name="date" value="{{ old('date') }}">
                    </div>
                    <div class="form-group">
                        <label for="time">@lang('campaign-items.time')</label>
                        <input type="text" class="form-control timepicker" name="time" value="{{ old('time') }}">
                    </div>
                    <div class="form-group">
                        <label for="store_id">@lang('campaign-items.store')</label>
                        <select name="store_id" id="store_id" class="form-control">
                            <option value="">@lang('general.select')</option>
                            @foreach(App\Store::orderBy('store_code')->get() as $store)
                                <option value="{{ $store->id }}">[{{ $store->store_code }}] {{ $store->brands->name }} {{ $store->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="max">@lang('campaign-items.max')</label>
                        <input type="number" class="form-control" name="max" value="{{ old('max') }}">
                    </div>
                </div>
            </div>
            <button class="btn btn-success" type="submit">Save changes</button>
        </form>
    </div>

    @endsection