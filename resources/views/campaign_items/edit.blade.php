@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <p class="text-right">
                    <img src="{{ $item->image }}" width="140" alt="{{ $item->name }}">
                </p>

                <div class="panel panel-primary">
                    <div class="panel-heading">Edit Campaign Item: {{ $item->name }}</div>
                    <div class="panel-body">
                        <form action="{{ action('CampaignItemsController@update', $item->id) }}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" value="{{ $item->name }}" required />
                            </div>
                            <div class="form-group">
                                <label for="image">Image (500 x 500)</label>
                                <input type="file" class="form-control" name="image" />
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea name="description" id="description" cols="30" rows="10"
                                          class="form-control summernote">{{ $item->description }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="directions">Directions</label>
                                <textarea name="directions" id="directions" cols="30" rows="10"
                                          class="form-control summernote">{{ $item->directions }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="campaign_category_id">Categories</label>
                                <select name="campaign_category_id[]" id="campaign_category_id"
                                        class="form-control multiple-select" multiple="multiple">
                                    <option value="">Select...</option>
                                    @foreach(App\CampaignCategory::all() as $campaign_category)
                                        <option @if( in_array($campaign_category->id, $item->campaign_categories()->select('campaign_category_id')->pluck('campaign_category_id')->toArray()) ) selected @endif value="{{ $campaign_category->id }}" >{{ $campaign_category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="price">Price</label>
                                <input type="text" class="form-control" name="price" value="{{ $item->retalix_price }}" readonly required >
                            </div>
                            <div class="form-group">
                                <label for="plu">PLU</label>
                                <input type="text" class="form-control" name="plu" value="{{ $item->plu }}" >
                            </div>
                            <div class="form-group">
                                <label for="status">Status</label>
                                <select name="status" id="status" class="form-control" required >
                                    <option value="">Select...</option>
                                    <option @if( $item->status == 'Published' ) selected @endif value="1">Active</option>
                                    <option @if( $item->status == 'Unpublished' ) selected @endif value="0">Inactive</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="date">@lang('campaign-items.date')</label>
                                <input type="text" class="form-control datepicker" name="date" value="{{ $item->date }}">
                            </div>
                            <div class="form-group">
                                <label for="time">@lang('campaign-items.time')</label>
                                <input type="text" class="form-control timepicker" name="time" value="{{ $item->time }}">
                            </div>
                            <div class="form-group">
                                <label for="store_id">@lang('campaign-items.store')</label>
                                <select name="store_id" id="store_id" class="form-control">
                                    <option value="">@lang('general.select')</option>
                                    @foreach(App\Store::orderBy('store_code')->get() as $store)
                                        <option @if( $item->store_id == $store->id ) selected @endif value="{{ $store->id }}">[{{ $store->store_code }}] {{ $store->brands->name }} {{ $store->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="max">@lang('campaign-items.max')</label>
                                <input type="number" class="form-control" name="max" value="{{ $item->max }}">
                            </div>
                            <button class="btn btn-success btn-block" type="submit">Save changes</button>
                        </form>
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Create a new size</div>
                    <div class="panel-body">

                        <form class="form-inline" action="{{ action('CampaignItemSizesController@store') }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="campaign_item_id" value="{{ $item->id }}">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}" required >
                            </div>
                            <div class="form-group">
                                <label for="price">Price</label>
                                <input type="text" class="form-control" name="price" value="{{ old('price') }}" required >
                            </div>
                            <div class="form-group">
                                <label for="plu">PLU</label>
                                <input type="number" class="form-control" name="plu" value="{{ old('plu') }}" required >
                            </div>
                            <button class="btn btn-success">Save changes</button>
                        </form>

                        <hr>

                        <div class="table-responsive">
                            <table class="table table-striped table-condensed">
                                <thead>
                                <tr>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Price</th>
                                    <th class="text-center">PLU</th>
                                    <th class="text-center">&nbsp;</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($item->sizes as $size)
                                    <tr>
                                        <th>{{ $size->name }}</th>
                                        <td class="text-center">{{ number_format($size->price, 2) }}</td>
                                        <td class="text-center">{{ $size->plu }}</td>
                                        <td class="text-center">
                                            <form action="{{ action('CampaignItemSizesController@destroy', $size->id) }}" method="post" onsubmit="return confirm('Are you sure? This can\'t be undone')">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button class="btn-xs btn-danger"><i class="fa fa-trash"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Choices</div>
                    <div class="panel-body">
                        <div class="col-md-4">
                            <form action="{{ action('ChoicesController@store') }}" method="post">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                                    </div>
                                    <input type="hidden" name="campaign_item_id" value="{{ $item->id }}">
                                    <button class="btn btn-success">Save changes</button>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-8">
                            @foreach($item->choices as $choice)
                                <div class="panel panel-default">
                                    <div class="panel-heading">{{ $choice->name }}</div>
                                    <div class="panel-body">
                                        <form method="post" onsubmit="return confirm('Are you sure? This can\'t be undone.')" action="{{ action('ChoicesController@destroy', $choice->id) }}">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <p class="text-right"><button class="btn btn-danger"><i class="fa fa-trash"></i></button></p>
                                        </form>
                                        @if( count($choice->options) > 0 )
                                            <div class="table-responsive">
                                                <table class="table table-striped table-condensed">
                                                    <thead>
                                                    <tr>
                                                        <th class="text-center">Name</th>
                                                        <th class="text-center">Default</th>
                                                        <th class="text-center">Surcharge</th>
                                                        <th class="text-center">&nbsp;</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($choice->options as $option)
                                                        <tr>
                                                            <td>{{ $option->name }}</td>
                                                            <td class="text-center">{{ $option->is_default }}</td>
                                                            <td class="text-center">{{ number_format($option->surcharge, 2) }}</td>
                                                            <td class="text-center">
                                                                <form onsubmit="return confirm('Are you sure? This can\'t be undone.')" action="{{ action('OptionsController@destroy', $option->id) }}" method="post">
                                                                    {{ csrf_field() }}
                                                                    {{ method_field('DELETE') }}
                                                                    <button class="btn-xs btn-danger"><i class="fa fa-trash"></i></button>
                                                                </form>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        @endif
                                        <form action="{{ action('OptionsController@store') }}" method="post">
                                            {{ csrf_field() }}
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-md-10">
                                                            <label for="name">Add option</label>
                                                            <input type="text" class="form-control" name="name" required >
                                                        </div>
                                                        <div class="col-md-2">
                                                            <label for="surcharge">Surcharge</label>
                                                            <input type="text" class="form-control text-right" name="surcharge" value="0" required >
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input name="is_default" value="1" type="checkbox"> Default Option
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" name="choice_id" value="{{ $choice->id }}" >
                                                    <button class="btn btn-success">Save changes</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Schedule Occurrences <span class="badge">BETA</span></div>
                    <div class="panel-body">
                        <form class="form-inline" action="{{ action('CampaignItemOccurrenceController@store') }}" method="post">
                            {{ csrf_field() }}
                            <div class="form-group" >
                                <label>Date</label>
                                <input type='text' name="event_date" class="form-control datepicker" />
                            </div>
                            <div class="form-group" >
                                <label for="event_time">Time</label>
                                <input type="text" name="event_time" class="form-control timepicker" id="event_time">
                            </div>
                            <div class="form-group" >
                                <label for="event_store">Store</label>
                                <input type="text" name="store" class="form-control" id="event_store">
                            </div>
                            <input type="hidden" name="campaign_item_id" value="{{ $item->id }}">
                            <button class="btn btn-success">Add Occurrence</button>
                        </form>
                        <hr>
                        <div class="panel panel-default">
                            <div class="panel-heading">Occurrences</div>
                            <div class="panel-body">
                                @if(isset($item->occurrences))
                                    <div class="table-responsive">
                                        <table class="table table-condensed table-striped">
                                            <thead>
                                            <th scope="col">Date/Time</th>
                                            <th scope="col">Store</th>
                                            <th scope="col">Options</th>
                                            </thead>
                                            <tbody>
                                            @foreach($item->occurrences as $oc)
                                                <tr>
                                                    <td>{{date('m/d/Y h:i A', strtotime($oc->event_date_time))}} </td>
                                                    <td>{{$oc->store}}</td>
                                                    <td><form method="post" onsubmit="return confirm('Are you sure? This can\'t be undone.')" action="{{ action('CampaignItemOccurrenceController@destroy', $oc->id) }}">
                                                            {{ csrf_field() }}
                                                            {{ method_field('DELETE') }}
                                                            <p class="text-right"><button class="btn btn-danger"><i class="fa fa-trash "></i></button></p>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    @endsection


