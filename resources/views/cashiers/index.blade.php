@extends('layouts.app')

@section('content')

    <div class="container">

        @if(!isset($_GET['keyword']))
        <form action="{{ action('CashiersController@index') }}">
            <div class="form-group">
                <div class="input-group">
                    <input type="text" class="form-control" name="keyword" required >
                    <div class="input-group-btn">
                        <button class="btn btn-success">Search</button>
                    </div>
                </div>
            </div>
        </form>
        @else
        <p>
            <a href="{{ action('CashiersController@index') }}" class="btn btn-danger">
                <i class="fa fa-arrow-circle-left"></i> Back
            </a>
        </p>
        @endif

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Cashiers</div>

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Cashier Number</th>
                                    <th>Store Number</th>
                                    <th>Start</th>
                                    <th>Type</th>
                                    <th>Remarks</th>
                                </tr>
                                </thead>
                                <tbody>

                                @if(count($cashiers) < 1)
                                    <tr><td colspan="6" class="text-center text-muted">No Cashiers Added.</td></tr>
                                @endif
                                @foreach($cashiers as $cashier)
                                    <tr>
                                        <td>
                                            <a href="{{ action('LiquorLicensesController@create', $cashier->cashier_number) }}">{{ $cashier->name }}</a>
                                        </td>
                                        <td>
                                            {{ $cashier->cashier_number }}
                                        </td>
                                        <td>
                                            {{ $cashier->store_number }}
                                        </td>
                                        <td>
                                            {{ $cashier->start }}
                                        </td>
                                        <td>
                                            {{ $cashier->type }}
                                        </td>
                                        <td>
                                            {{ $cashier->remarks }}
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                            {{ $cashiers->render() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection