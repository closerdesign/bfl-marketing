@extends('layouts.app')

@section('content')

    <div class="container">
        <form action="{{ action('ItemController@store') }}" method="post">
            @csrf
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Create A New Item
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="name" value="{{ old('name') }}" required>
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea name="description" id="description" cols="30" rows="10"
                                  class="form-control">{{ old('description') }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="stock">Stock</label>
                        <input type="number" class="form-control text-center" value="{{ old('stock') }}" name="stock" required>
                    </div>
                    <div class="form-group">
                        <label for="unit_cost">Unit Cost</label>
                        <input type="text" class="form-control text-center" name="unit_cost" value="{{ old('unit_cost') }}" required>
                    </div>
                    <button class="btn btn-success btn-block">Save</button>
                </div>
            </div>
        </form>
    </div>

    @endsection