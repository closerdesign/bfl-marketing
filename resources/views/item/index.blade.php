@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <p class="text-right">
                    <div class="btn-group">
                        <button class="btn btn-success dropdown-toggle" data-toggle="dropdown" data-target="dropdown-menu">
                        <i class="fa fa-chevron-down"></i> Actions
                        </button>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{ action('ItemController@create') }}">
                                    Add A New Item
                                </a>
                            </li>
                            <li>
                                <a href="{{ action('TransactionController@index') }}">
                                    Transaction History
                                </a>
                            </li>
                        </ul>
                    </div>
                </p>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Stock Report
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-condensed">
                                <thead>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>Name</th>
                                    <th>Stock</th>
                                    <th>Unit Cost</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($items as $item)
                                    <tr>
                                        <td></td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->stock }}</td>
                                        <td>{{ $item->unit_cost }}</td>
                                        <td>{{ $item->unit_cost * $item->stock }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <form action="{{ action('TransactionController@store') }}" method="post">
                    <div class="panel panel-primary">
                        @csrf
                        <div class="panel-heading">
                            Add A New Transaction
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="item_id">Item</label>
                                <select name="item_id" id="item_id" class="form-control" required>
                                    <option value="">Select...</option>
                                    @foreach(\App\Item::orderBy('name')->get() as $product)
                                        <option value="{{ $product->id }}">{{ $product->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="store_id">Store</label>
                                <select name="store_id" id="store_id" class="form-control" required>
                                    <option value="">Select...</option>
                                    @foreach(\App\Store::all() as $store)
                                        <option value="{{ $store->id }}">{{ $store->store_code }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="qty">Quantity</label>
                                <input type="number" class="form-control text-center" name="qty" required>
                            </div>
                            <div class="form-group">
                                <label for="type">Type</label>
                                <select name="type" id="type" class="form-control">
                                    <option value="">Select...</option>
                                    <option value="IN">In</option>
                                    <option value="OUT">Out</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="approved_by">Approved By</label>
                                <input type="text" class="form-control" name="approved_by" value="{{ old('comments') }}" required>
                            </div>
                            <div class="form-group">
                                <label for="comments">Comments</label>
                                <textarea name="comments" id="comments" cols="30" rows="10" class="form-control">{{ old('comments') }}</textarea>
                            </div>
                            <button class="btn btn-success btn-block">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @endsection