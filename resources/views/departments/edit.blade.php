@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">Edit Department: {{ $department->name }}</div>
            <div class="panel-body">
                <form action="{{ action('DepartmentsController@update', $department->id) }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="name" value="{{ $department->name }}" required >
                    </div>

                    <div class="form-group">
                        <label for="active">Active</label>

                        <input type="checkbox" class="form-control" name="active" value="1" @if($department->active ==1 ) checked @endif>
                    </div>

                    <button class="btn btn-success" type="submit">Save Changes</button>
                </form>
            </div>
        </div>
    </div>

@endsection