@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">Create New Department</div>
            <div class="panel-body">
                <form action="{{ action('DepartmentsController@store') }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="name" value="{{ old('name') }}" required >
                    </div>
                    <button class="btn btn-success" type="submit">Save Changes</button>
                </form>
            </div>
        </div>
    </div>

    @endsection