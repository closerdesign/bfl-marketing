@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <p class="text-right">
            <a href="{{ action('DepartmentsController@create') }}" class="btn btn-success">
                Create New Department
            </a>
        </p>
        <div class="panel panel-default">
            <div class="panel-heading">
                Departments
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($departments as $department)
                            <tr>
                                <th>
                                    <a href="{{ action('DepartmentsController@edit', $department->id) }}">
                                        {{ $department->name }}
                                    </a>
                                </th>
                                <td class="text-center">
                                    <form
                                            action="{{ action('DepartmentsController@destroy', $department->id) }}"
                                            method="post"
                                            onsubmit="return confirm('Are you sure?')"
                                    >
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button class="btn-xs btn-danger">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @endsection