Store Location,Product UPC,Price,Sale Price,Sale Type,Sale Start Date,Sale End Date,BuyAmount,GetAmount
@foreach( $products as $product )
@php $regular_price = $product->regular_price / 1.07; @endphp
@php if( $store->is_cost_plus ){ $regular_price = $regular_price + ($regular_price * 0.1); } @endphp
{{ $product->store_number }},{{ $product->sku }},{{ number_format($product->regular_price, 2) }},{{ $product->sale_price }},{{ $product->promo_tag }},{{ $product->promo_start_date }},{{ $product->promo_end_date }},{{ $product->sale_qty }},
@endforeach