@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Editing Shrink Group: {{ $group->name }}
                </div>
                <div class="panel-body">
                    <form action="{{ action('ShrinkGroupsController@update', $group->id) }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" name="name" value="{{ $group->name }}" required >
                        </div>
                        <button class="btn btn-primary">Save changes</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @endsection