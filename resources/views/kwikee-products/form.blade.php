@extends('layouts.app')

@section('content')

    <div class="container">
        <form action="{{ action('KwikeeProductsController@import') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <i class="fa fa-th"></i> Kwikee Products Import
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="file">File</label>
                        <input type="file" class="form-control" name="file" required >
                        <label><i class="fa fa-warning"></i> Only XML files allowed.</label>
                    </div>
                    <button class="btn btn-success">
                        <i class="fa fa-upload"></i> Upload File
                    </button>
                </div>
            </div>
        </form>

        <div class="panel panel-primary">
            <div class="panel-heading"><i class="fa fa-files-o"></i> Files List</div>
            <div class="panel panel-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th width="20%">Filename</th>
                            <th width="20%">Status</th>
                            <th width="20%">Creation Date</th>
                            <th width="20%">Last Record</th>
                            <th width="20%">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach(\App\KwikeeFile::orderBy('created_at', 'desc')->paginate(12) as $file)
                        <tr>
                            <th>{{ $file->name }}</th>
                            <td>{{ $file->status }}</td>
                            <td>{{ $file->created_at }}</td>
                            <td>{{ $file->last_record }}</td>
                            <td class="text-center">
                                @if($file->status != 'Completed')
                                <a href="{{ action('KwikeeProductsController@process', $file->id) }}" class="btn btn-success form-control">
                                    <i class="fa fa-refresh"></i> Process
                                </a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ \App\KwikeeFile::orderBy('created_at', 'desc')->paginate(12)->render() }}
                </div>
            </div>
        </div>
    </div>

    @endsection