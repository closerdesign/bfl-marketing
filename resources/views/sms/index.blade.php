@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <p class="pull-right">
                <a href="{{action('SmsController@groups')}}" class="btn btn-success">Groups</a>
            </p>
        </div>
        <form method="post" action="{{ action('SmsController@send_sms') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">Message</div>
                <div class="panel-body">

                    <div class="form-group">
                        <label for="group_id">Send to group(s)</label>
                        <select name="group_id[]" id="group_id" class="form-control multiple-select" multiple="multiple" required>
                            <option value="">Select...</option>
                            @foreach($sms_groups as $group)
                                <option value="{{ $group->id }}">{{ $group->group_name }}</option>
                            @endforeach
                        </select>
                    </div>

                <div class="form-group">
                    <label for="message">Body</label>
                    <textarea class="form-control" id="message" name="message" rows="8"></textarea>
                </div>
                    <div class="form-group">
                        <label for="url">URL</label>
                        <input class="form-control" id="url" name="url" type="text">
                    </div>
                    <div class="form-group">
                        <label for="file">Image</label>
                        <input type="file" name="file" id="file" class="form-control">
                    </div>
                <div class="form-group">
                    <p class="pull-right">
                        <button class="btn btn-success">Send Message</button>
                    </p>
                </div>
            </div>
            </div>
        </div>
    </form>
    </div>
</div>
@endsection
@section('js')
    <script>
    $(document).ready(function() {
     $('#multiselect').multiselect({
     buttonWidth : '160px',
        includeSelectAllOption : true,
         nonSelectedText: 'Select an Option'
    });
    });

    function getSelectedValues() {
    var selectedVal = $("#multiselect").val();
    for(var i=0; i<selectedVal.length; i++){
    function innerFunc(i) {
    setTimeout(function() {
    location.href = selectedVal[i];
    }, i*2000);
    }
    innerFunc(i);
    }
    }
    </script>

@endsection
