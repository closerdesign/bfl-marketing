@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <form method="get" action="{{ action('SmsController@subscribe') }}">
                {{ csrf_field() }}
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">This is to test the response messages</div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="message">Body</label>
                                <textarea class="form-control" id="message" name="Body" rows="8"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="From">From Number (include +1)</label>
                                <input name="From" id="From" value="+14057605923" class="form-control">
                            </div>
                            <div class="form-group">
                                <p class="pull-right">
                                    <button class="btn btn-success">Send Message</button>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
