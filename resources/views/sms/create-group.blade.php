@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p>
                    <a href="/sms" class="btn btn-primary">Messaging</a>
                </p>
            </div>
            <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">Add Text Group</div>
                <div class="panel-body">
            <form method="post" action="{{ action('SmsController@create_group') }}">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="group_name">
                        Group Name
                    </label>
                    <input name="group_name" id="group_name" class="form-control" type="text" required>
                </div>
                <div class="form-group">
                    <label for="keyword">
                        Keyword
                    </label>
                    <input name="keyword" id="keyword" class="form-control" type="text" required>
                </div>
                <div class="form-group">
                    <label for="welcome_message">Welcome Message</label>
                    <textarea id="welcome_message" class="form-control" name="welcome_message" rows="5" required></textarea>
                </div>
                <button name="btn" type="submit" class="btn btn-success">Create</button>
            </form>
                </div>
            </div>
        </div>
        </div>
    </div>

@endsection