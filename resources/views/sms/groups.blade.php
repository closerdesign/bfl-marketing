@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p class="pull-left">
                    <a href="/sms" class="btn btn-primary">Messaging</a>
                </p>
                <p class="pull-right">
                    <a href="/sms-group-create" class="btn btn-success">Create Group</a>
                </p>
            </div>
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Groups</div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <th>Name</th>
                                    <th>Members</th>
                                    <th>Edit</th>
                                </tr>
                                @foreach($groups as $group)
                                    <tr>
                                        <td>{{$group->group_name}}</td>
                                        <td> Count Here </td>
                                        <td> <a href="{{ action('SmsController@group_edit', $group->id) }}" class="btn btn-sm btn-success">Edit</a> </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
