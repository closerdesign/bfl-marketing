@extends('layouts.app')

@section('content')

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-1">
                    <br class="hidden-xs">
                    <img src="/img/concept.png" alt="" class="img-responsive">
                </div>
                <div class="col-md-6">
                    <h1>@lang('nav.you-are-in'),<br />{{ Auth::user()->name }}!</h1>
                    <p>@lang('nav.thank-you')</p>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <!-- Example row of columns -->
        <div class="row">
            <div class="col-md-6">
                <h2>@lang('nav.communicate')</h2>
                <p>@lang('nav.spread-word')</p>
                <p class="lead">@lang('nav.every-element')</p>
            </div>
        </div>
    </div>

@endsection
