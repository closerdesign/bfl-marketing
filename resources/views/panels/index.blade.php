@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
    <div class="col-md-4">
        <div class="panel panel-warning">
            <div class="panel-heading">Add Panel to Aisle {{$aisle->aisle_name}}</div>
            <div class="panel-body">
        <form action="{{action('StoreAislePanelsController@store')}}" method="post">
            {{csrf_field()}}
            <div class="form-group">
                <label for="panel">Panel</label>
                <input class="form-control" name="panel_name" id="panel" value="{{@$panel->panel_name}}">
                <input name="aisle_id" type="hidden" value="{{$aisle->id}}">
            </div>
            <div>
                <button class="btn btn-primary">Submit</button>
            </div>
        </form>

    </div>
        </div>
    </div>

<!-- -->
        <div class="col-md-4">
            <div class="panel panel-warning">
                <div class="panel-heading">Panels for aisle {{$aisle->aisle_name}}</div>
                <div class="panel-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Panel Name</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                    @foreach($aisle->panels as $panel)
                        <tr>
                            <td>{{$panel->panel_name}}</td>
                            <td><a class="btn btn-sm btn-primary" href={{ action('StoreAislePanelsController@show', $panel->id) }}><i class="fa fa-edit "></i></a>
                            <td class="text-center">
                                <form method="POST" onsubmit="return confirm('Are you sure? This can\'t be undone.')" action="{{ action('StoreAislePanelsController@destroy_panel', $panel->id) }}">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <p class="text-right"><button class="btn btn-danger"><i class="fa fa-trash "></i></button></p>
                                </form>
                            </td>
                        </tr>

                    @endforeach
                        </tbody>
                    </table>

                </div>

            </div>


        </div>

        <div class="col-md-4">
            <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Back to <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="/store-aisles-all/{{$aisle->store_id}}">Aisles</a></li>
                    <li><a href="{{ action('StoresController@edit', $aisle->store_id) }}">Store</a></li>
                </ul>
            </div>

        </div>


    </div>





</div>

@endsection