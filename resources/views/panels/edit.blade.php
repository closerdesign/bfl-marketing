@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-warning">
                    <div class="panel-heading">Edit Panel {{$panel->panel_name}}</div>
                    <div class="panel-body">
                        <form action="{{action('StoreAislePanelsController@update', $panel->id)}}" method="post">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="panel">Panel</label>
                                <input class="form-control" name="panel_name" id="panel" value="{{$panel->panel_name}}">
                            </div>
                            <div class="form-group">
                                <label for="aisle_id">Aisles for Store: {{$store->name}}</label>
                                <select name="aisle_id" class="form-control" id="aisle_id">
                                    @foreach($all_aisles as $aisle)
                                        <option id="{{$aisle->id}}" @if($aisle->id == $panel->aisle_id) selected @endif value="{{$aisle->id}}">{{$aisle->aisle_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div>
                                <button class="btn btn-primary">Submit</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
            <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Back to <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">

                    <li><a href="/store-aisles-all/{{$store->id}}">Aisles</a></li>
                    <li><a href="{{ action('StoresController@edit', $store->id) }}">Store</a></li>
                </ul>
            </div>


            <!-- -->

{{--            <div class="col-md-4">--}}
{{--                <div class="panel panel-primary">--}}
{{--                    <div class="panel-heading">Panels for aisle {{$aisle->aisle_name}}</div>--}}
{{--                    <div class="panel-body">--}}
{{--                        <table class="table">--}}
{{--                            <thead>--}}
{{--                            <tr>--}}
{{--                                <th>Panel Name</th>--}}
{{--                                <td>Options</td>--}}
{{--                            </tr>--}}
{{--                            </thead>--}}
{{--                            <tbody>--}}
{{--                            @foreach($panels as $panel)--}}
{{--                                <tr>--}}
{{--                                    <td>{{$panel->panel_name}}</td>--}}
{{--                                    <td><a href={{ action('StoreAislePanelsController@show', $panel->id) }}><i class="fa fa-edit "></i></a>--}}

{{--                                    <td><form method="post" onsubmit="return confirm('Are you sure? This can\'t be undone.')" action="{{ action('StoreAislePanelsController@destroy', $panel->id) }}">--}}
{{--                                            {{ csrf_field() }}--}}
{{--                                            {{ method_field('DELETE') }}--}}
{{--                                            <p class="text-right"><button class="btn btn-danger"><i class="fa fa-trash "></i></button></p>--}}
{{--                                        </form>--}}
{{--                                    </td>--}}
{{--                                </tr>--}}

{{--                            @endforeach--}}
{{--                            </tbody>--}}
{{--                        </table>--}}

{{--                    </div>--}}

{{--                </div>--}}


{{--            </div>--}}


        </div>





    </div>

@endsection