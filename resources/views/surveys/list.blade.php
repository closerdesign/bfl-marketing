@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Surveys Report</div>
                    <div class="panel-body">
                        <form class="form-inline">
                            <div class="form-group">
                                <label for="from">Start Date</label>
                                <input type="text" class="form-control datepicker" name="from" value="@if( isset($_GET['from']) ) {{ $_GET['from'] }} @endif">
                            </div>
                            <div class="form-group">
                                <label for="to">End Date</label>
                                <input type="text" class="form-control datepicker" name="to" value="@if( isset($_GET['to']) ) {{ $_GET['to'] }} @endif">
                            </div>
                            <button type="submit" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @if(isset($_GET['from']) && isset($_GET['to']))
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel panel-body">
                            <p class="lead text-center">Overall Rating</p>
                            <h1 class="text-center">{{ number_format($rating,2) }}</h1>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel panel-body">
                            <p class="lead text-center">Surveys Number</p>
                            <h1 class="text-center">{{ $count }}</h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped datatable">
                            <thead>
                            <tr>
                                <th>Store</th>
                                <th>Visit Date</th>
                                <th>Visit Time</th>
                                <th>Rating</th>
                                <th>Description</th>
                                <th>Recommend</th>
                                <th>Suggested Improvements</th>
                                <th>Age</th>
                                <th>Gender</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Comments</th>
                                <th>Brand</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($surveys as $survey)
                                <tr>
                                    <td class="text-center">{{ $survey->store }}</td>
                                    <td class="text-center">{{ $survey->date }}</td>
                                    <td class="text-center">{{ $survey->time }}</td>
                                    <td class="text-center">{{ $survey->rating }}</td>
                                    <td>{{ $survey->description }}</td>
                                    <td class="text-center">{{ $survey->recommend }}</td>
                                    <td>{{ $survey->improvements }}</td>
                                    <td class="text-center">{{ $survey->age }}</td>
                                    <td class="text-center">{{ $survey->gender }}</td>
                                    <td>{{ $survey->name }}</td>
                                    <td>{{ $survey->email }}</td>
                                    <td>{{ $survey->phone }}</td>
                                    <td>{{ $survey->comments }}</td>
                                    <td class="text-center">{{ $survey->brand }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endif
    </div>

    @endsection