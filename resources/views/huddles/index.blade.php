@extends('layouts.app')

@section('content')

    <div class="container">
        <p class="text-right">
            <a class="btn btn-success" href="{{ action('HuddlesController@create') }}"><i class="fa fa-plus"></i> New Huddle Report</a>
        </p>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <i class="fa fa-users"></i> Huddles
                @if(Auth::user() && Auth::user()->store == '')
                    <span class="pull-right">
                                <div class="dropdown">
                                    <button class="btn-xs btn-info dropdown-toggle" type="button" data-toggle="dropdown">
                                        <i class="fa fa-filter"></i> Filter By Store <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{ action( 'HuddlesController@index' ) }}">All</a></li>
                                        <li><a href="{{ action( 'HuddlesController@filter', '0' ) }}">Support Center</a></li>
                                        @foreach(\App\Store::orderBy('id')->get() as $store)
                                            <li><a href="{{ action('HuddlesController@filter', $store->store_code) }}">{{ $store->store_code }}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </span>
                @endif
            </div>
            <div class="panel-body">
                <table class="table table-striped table-condensed">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Store</th>
                        <th>Date</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($huddles as $huddle)
                        <tr>
                            <td>{{ $huddle->id }}</td>
                            <td>
                                @if( $huddle->store ) {{ $huddle->store->store_code  }} - {{ $huddle->store->name }} @endif
                                @if( $huddle->store_id == 0 ) Support Center @endif
                            </td>
                            <td>{{ $huddle->created_at->format('m/d/Y  g:i A') }}</td>
                            <td>
                                <a href="{{ action('HuddlesController@view', $huddle->id) }}">
                                    <i class="fa fa-comments"></i> View Details
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $huddles->render() }}
            </div>
        </div>
    </div>

@endsection