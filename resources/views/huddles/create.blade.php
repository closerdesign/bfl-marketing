@extends('layouts.app')

@section('content')

    <div class="container">
        @if( Auth::user() )
            <p>
                <a class="btn btn-primary" href="{{ action('HuddlesController@index') }}"><i class="fa fa-arrow-circle-left"></i> Back to List</a>
            </p>
        @endif
        <div class="panel panel-primary">
            <div class="panel-heading"><i class="fa fa-users"></i> Huddle Report</div>
            <div class="panel-body">
                <form action="{{ action('HuddlesController@store') }}" method="post" >
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="store_id">Store</label>
                        <select class="form-control" id="store_id" name="store_id" >
                            <option value="">Select...</option>
                            @if(Auth::user() &&  Auth::user()->isStore())
                                <option value="{{ App\Store::where('store_code', Auth::user()->store)->first()->id }}">{{ Auth::user()->store }}</option>
                            @else
                                <option value="0">Support Center</option>
                                @foreach(\App\Store::orderBy('id')->get() as $store)
                                    <option @if( old('store_id') == $store->id ) selected @endif value="{{ $store->id }}">{{ $store->store_code }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="leader">Huddle Led By</label>
                        <input type="text" name="leader" class="form-control" required value="{{ old('leader') }}">

                    </div>
                    <div class="form-group">
                        <label for="details">Details</label>
                        <textarea name="details" id="details" cols="30" rows="3" class="form-control summernote" required >{{ old('details') }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="ideas">Ideas</label>
                        <textarea name="ideas" id="ideas" cols="30" rows="3" class="form-control summernote" >{{ old('ideas') }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="requests">Requests</label>
                        <textarea name="requests" id="requests" cols="30" rows="3" class="form-control summernote" >{{ old('requests') }}</textarea>
                    </div>
                    <button class="btn-lg btn-success pull-right">Submit</button>
                </form>
            </div>
        </div>
    </div>
    <p><br /><br /></p>

@endsection

@section('js')
    <script>
        $(".summernote").summernote({
            height: 200,
            toolbar: [
                [ 'style', [ 'style' ] ],
                [ 'font', [ 'bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript'] ],
                [ 'color', [ 'color' ] ],
                [ 'para', [ 'ol', 'ul', 'paragraph'] ],
                [ 'table', [ 'table' ] ],
                [ 'insert', [ 'link'] ],
                [ 'view', [ 'undo', 'redo', 'fullscreen', 'help' ] ]
            ]
        });
    </script>
@endsection