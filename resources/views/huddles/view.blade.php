@extends('layouts.app')

@section('content')

    <div class="container">
        @if( Auth::user() )
            <p class="no-print">
                <a class="btn btn-primary" href="{{ action('HuddlesController@index') }}"><i class="fa fa-arrow-circle-left"></i> Go Back</a>
            </p>
        @endif
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <i class="fa fa-users"></i> Submitted Huddle Report
                        @if( $huddle->store ) [{{ $huddle->store->store_code  }} - {{ $huddle->store->name }}] - Led By {{$huddle->leader}} @endif
                        @if( $huddle->store_id == 0 ) [Support Center] @endif
                        <span class="pull-right no-print"><a id="print-link" href="javascript:window.print();" style="color: #fff;"><i class="fa fa-print"></i> Print</a></span>
                    </div>
                    <div class="panel-body">
                        {!! $huddle->details !!}
                    </div>
                </div>
                <div class="panel panel-primary">
                    <div class="panel-heading"><i class="fa fa-lightbulb-o"></i> Ideas / Requests</div>
                    <div class="panel-body">
                        @if( $huddle->ideas == '' && $huddle->requests == '' )
                            <p class="text-center text-muted">No ideas or requests added.</p>
                        @else
                            @if( $huddle->ideas !== '' )
                                <h4>Ideas</h4>
                                {!! $huddle->ideas !!}
                            @endif
                            @if( $huddle->requests !== '' )
                                <h4>Requests</h4>
                                {!! $huddle->requests !!}
                            @endif
                        @endif
                    </div>
                </div>
                <div class="panel panel-info">
                    <div class="panel-heading"><i class="fa fa-comments"></i> Comments</div>
                    <div class="panel-body">
                        @if( count($comments) < 1 )
                            <p class="text-center text-muted">No comments added.</p>
                        @else
                            @foreach($comments as $comment)
                                <p>{{ $comment->comment }}</p>
                                <p class="text-muted">-{{ $comment->name }} (<a href="mailto:{{$comment->email}}">{{ $comment->email }}</a>) &nbsp;&nbsp;{{ $comment->created_at->format('m/d/Y g:i A') }}</p><hr />
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-4 no-print">
                <div class="panel panel-info">
                    <div class="panel-heading"><i class="fa fa-plus"></i> Add Comment</div>
                    <div class="panel-body">
                        <form action="{{ action('HuddleCommentsController@store', $huddle->id) }}" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}" required >
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}" required >
                            </div>
                            <div class="form-group">
                                <label for="comment">Comment</label>
                                <textarea name="comment" id="comment" cols="30" rows="5" class="form-control" required >{{ old('comment') }}</textarea>
                            </div>
                            <p class="text-right"><button class="btn btn-success">Add</button></p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>

        $('#print-link').click( function(){
           $('.alert').hide();
        });

    </script>
@endsection