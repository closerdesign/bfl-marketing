@foreach($special->upcs as $upc)

    <tr>
        <td><img src="{{ $upc->product->image }}" width="20" height="20" alt="{{ $upc->name }}"></td>
        <td>{{ $upc->upc }}</td>
        <td>{{ $upc->product->name }}</td>
        <td class="text-center">
            <form id="delete-upc-{{ $upc->id }}" action="{{ action('SpecialUpcsController@destroy', $upc->id) }}" method="post">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
                <a onclick="upc_delete({{ $upc->id }})">
                    <i class="fa fa-trash"></i>
                </a>
            </form>
            <form action="">
                {{ csrf_field() }}
                <a href="{{ action('SpecialsController@make_special_img', [$special->id, $upc->product->id]) }}">
                    <i class="fa fa-arrow-circle-up"></i>
                </a>
            </form>
        </td>
    </tr>

@endforeach