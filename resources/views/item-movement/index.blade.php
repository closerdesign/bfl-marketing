@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <p class="text-right">
            <a href="{{ action('ItemMovementController@create') }}" class="btn btn-success">
                Import Item Movement File
            </a>
        </p>
        <div class="panel panel-primary">
            <div class="panel-heading">
                Item Movement
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-condensed table-bordered">
                        <thead>
                        <tr>
                            <th>Store</th>
                            <th>UPC</th>
                            <th>Description</th>
                            <th>Dept</th>
                            <th>Category</th>
                            <th>Size</th>
                            <th>Qty Sold</th>
                            <th>Amt Sold</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($item_movements as $movement)
                        <tr>
                            <td>{{ $movement->store }}</td>
                            <td>{{ $movement->upc }}</td>
                            <td>{{ $movement->description }}</td>
                            <td>{{ $movement->dept }}</td>
                            <td>{{ $movement->category }}</td>
                            <td>{{ $movement->size }}</td>
                            <td>{{ $movement->qty }}</td>
                            <td>{{ $movement->amount }}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $item_movements->render() }}
                </div>
            </div>
        </div>
    </div>

    @endsection