@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Import Movement File
            </div>
            <div class="panel-body">
                <form action="{{ action('ItemMovementController@store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="file">File</label>
                        <input type="file" class="form-control" name="file" required >
                        <label>File must contain columns: Store, UPC, Description, Dept, Category, Size, Qty Sold, Wght Sold, Amt Sold, Week Ending Date</label>
                    </div>
                    <button class="btn btn-success">
                        Import
                    </button>
                </form>
            </div>
        </div>
    </div>

    @endsection