@extends('layouts.app')

@section('content')

    <div class="container">
        <p class="pull-right">
            <a class="btn btn-primary" href="{{ action('SearchController@index') }}"><i class="fa fa-arrow-circle-left"></i> Go Back</a>
            <a href="{{action('SearchController@run_keyword_process')}}" class="btn btn-info"><i class="fa fa-refresh"></i> RePoll</a>
            <button class="btn btn-primary" onClick="javascript:location.reload()"><i class="fa fa-refresh"></i> Reload</button>
        </p>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-primary">
                    <div class="panel-heading"><i class="fa fa-check-circle"></i> Online Shopping Search Keyword Review</div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-sm-12">
                                <a id="checkAll" style="cursor: pointer"><i class="fa fa-check-square"></i> Select All</a>&nbsp;
                                <a id="uncheckAll" style="cursor: pointer"><i class="fa fa-square-o"></i> Deselect All</a>
                            </div>
                        </div>

                        <form name="" action="{{action('SearchController@batch_review')}}" method="post">
                            {{ csrf_field() }}
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>&nbsp;</th>
                                        <th>Search String</th>
                                        <th>Product</th>
                                        <th>Store</th>
                                        <th>Created</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(count($searches) < 1)
                                    <tr><td colspan="6" class="text-muted text-center">There are no keywords to review.</td></tr>
                                @endif
                                @foreach($searches as $search)
                                    <tr>
                                        <td>
                                            <label class="checkbox-inline">
                                                <input name="reviewed[{{$search->s_id}}]" type="checkbox" class="check">
                                            </label>
                                        </td>
                                        <td>
                                            {{ $search->search_string }}
                                        </td>
                                        <td>
                                            <a href="/search/product/{{ $search->cart_product_id }}">
                                                {{ $search->cart_product_id }}
                                            </a><br>
                                            {{$search->brand_name}} - {{$search->name}}<br>
                                            {{$search->kwikee_description}}
                                        </td>
                                        <td>
                                            {{ $search->store_code }}
                                        </td>
                                        <td>
                                            {{ date('m/d/Y h:i:s', strtotime($search->s_created_at))}}
                                        </td>
                                        <td width="50">
                                            <a href="{{action('SearchController@remove_keyword', $search->s_id)}}" class="btn-danger btn-sm"><i class="fa fa-times"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="row">
                                <div class="col-sm-12 text-right"><button class="btn btn-primary" type="submit" value="Submit">Submit</button></div>
                            </div>
                        </form>
                        {{--{{ $searches->render() }}--}}

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <script>
        $('#checkAll').click(function(){
            $('.check').prop('checked', true);
        });
        $('#uncheckAll').click(function(){
            $('.check').prop('checked', false);
        });
    </script>

@endsection