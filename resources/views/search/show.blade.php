@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <p class="pull-right">
                    <button class="btn btn-default" onClick="javascript:location.reload()"><i class="fa fa-refresh"></i> Reload</button>
                    <a class="btn btn-info" href="/search-review"><i class="fa fa-check-circle"></i>  Review Keywords</a>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-search"></i> Online Shopping Search Queries</div>
                    <div class="panel-body">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Search String</th>
                                <th>Store</th>
                                <th>Product</th>
                                <th>Created</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($searches as $search)
                                    <tr>
                                        <td>
                                            {{ $search->search_string }}
                                        </td>
                                        <td>
                                            {{ $search->store_code }}
                                        </td>
                                        <td>
                                            @if( $search->cart_product_id > 0 )
                                                <a href="/search/product/{{ $search->cart_product_id }}">{{ $search->cart_product_id }}</a>
                                            @else {{ $search->cart_product_id }} @endif
                                        </td>
                                        <td>
                                            {{ date('m/d/Y h:i:s', strtotime($search->created_at)) }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $searches->render() }}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-bar-chart"></i> Search Frequency</div>
                    <div class="panel-body">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Search String</th>
                                <th>Count</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($counts as $count)
                                    <tr>
                                        <td>{{ $count->search_string }}</td>
                                        <td>{{ $count->frequency }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection