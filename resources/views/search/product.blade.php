@extends('layouts.app')

@section('content')

    <div class="container">
        <p>
            <button class="btn btn-primary" onclick="window.history.back();">
                <i class="fa fa-arrow-circle-left"></i> Go Back
            </button>
        </p>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading"><i class="fa fa-info-circle"></i> Product Details</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <p class="lead text-danger"><b>{{ $product->brand_name }}</b></p>
                                <p class="lead"><b>{{ $product->name }}</b></p>
                                <p>{{ $product->size }}</p>
                                <p class="text-muted">{{ $product->upc }}</p><hr />
                                <p><b>Description</b><br />{!! $product->description !!}</p>
                                <p><b>Keywords</b><br />{{ $product->keywords }}</p>
                                <p class="text-center"><a href="/products/{{ $product->id }}/edit" class="btn btn-success"><i class="fa fa-edit"></i> Edit Product</a></p>
                            </div>
                            <div class="col-sm-6 text-center">
                                <img src="{{ $product->image }}" alt="{{ $product->name }}" style="max-height: 350px; max-width: 350px">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection