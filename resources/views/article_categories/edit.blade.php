<?php
/**
 * Created by PhpStorm.
 * User: davidmeinke
 * Date: 2018-12-07
 * Time: 09:38
 */
?>
@extends('layouts.app')

@section('content')

    <div class="container">
        <form action="{{ action('ArticlesCategoriesController@update', $category->id) }}" method="post">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="panel panel-primary">
                <div class="panel-heading">Edit Category: {{ $category->name }}</div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="name" value="{{ $category->name }}" required >
                    </div>
                    <div class="form-group">
                        <label for="parent_id">Parent</label>
                        <select name="parent_id" id="parent_id" class="form-control">
                            <option value="">Select...</option>
                            @foreach(\App\ArticleCategory::where('parent_id', '<', 1)->where('id', '!=', $category->id)->get() as $parent)
                                <option @if( $category->parent_id == $parent->id ) selected @endif value="{{ $parent->id }}">{{ $parent->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <button class="btn btn-success pull-right">
                <i class="fa fa-save"></i> Save Changes
            </button>
        </form>
    </div>

@endsection
