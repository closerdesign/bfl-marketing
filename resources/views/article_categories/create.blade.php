@extends('layouts.app')

@section('content')

    <div class="container">
        <form action="{{ action('ArticlesCategoriesController@store') }}" method="post">
            {{ csrf_field() }}
            <div class="panel panel-primary">
                <div class="panel-heading">Creating A New Category</div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="name" value="{{ old('name') }}" required >
                    </div>
                    <div class="form-group">
                        <label for="parent_id">Parent</label>
                        <select name="parent_id" id="parent_id" class="form-control">
                            <option value="">Select...</option>
                            @foreach(\App\ArticleCategory::where('parent_id', '<', 1)->get() as $category)
                                <option @if( old('parent_id') == $category->id ) selected @endif value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <button class="btn btn-success pull-right">
                <i class="fa fa-save"></i> Save Changes
            </button>
        </form>
    </div>

@endsection