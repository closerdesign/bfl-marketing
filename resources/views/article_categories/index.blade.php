@extends('layouts.app')

@section('content')

    <div class="container">
        <p class="text-right">
            <a class="btn btn-success" href="{{ action('ArticlesCategoriesController@create') }}">
                <i class="fa fa-plus"></i> Add A New Category
            </a>
        </p>
        <div class="panel panel-primary">
            <div class="panel-heading">Categories List</div>
            <div class="panel-body">
                <table class="table table-striped datatable">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Parent Category</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach(\App\ArticleCategory::orderBy('name')->get() as $category)
                        <tr>
                            <td>
                                <a href="{{ action('ArticlesCategoriesController@edit', $category->id) }}">
                                  {{$category->name}}
                                </a>
                            </td>
                            <td>
                                @if(!empty($category->parent))
                                    {{ $category->parent->name }}
                                @endif
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection