@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <p><a href="{{ action('DonationRequestsController@index') }}" class="btn btn-primary"><i class="fa fa-arrow-circle-left"></i> Go Back</a>&nbsp;
                <a class="btn btn-success"  data-toggle="modal" data-target=".status-modal"><i class="fa fa-edit"></i> Change Status</a></p>
            </div>
            <div class="col-md-6 text-right">
                <b>Date Submitted</b>: {{ $request->created_at }}<br />
                <b>Status</b>:
                @if( $request->status == 'Pending')
                    <span class="label label-warning">
                @elseif( $request->status == 'Declined')
                    <span class="label label-danger">
                @else
                    <span class="label label-success">
                @endif
                    {{ $request->status }}
                </span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">Contact Details</div>
                    <div class="panel-body">
                        <p><b>First Name</b><br />{{ $request->first_name }}</p>
                        <p><b>Last Name</b><br />{{ $request->last_name }}</p>
                        <p><b>Email Address</b><br /><a href="mailto:{{ $request->email }}">{{ $request->email }}</a></p>
                        <p><b>Phone Number</b><br />{{ $request->phone }}</p>
                        <p><b>Closest/Preferred Store</b><br />{{ $request->location }}</p>
                        <p><b>How did you hear about us?</b><br />{{ $request->referral_source }}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="panel panel-info">
                    <div class="panel-heading">Organization Details</div>
                    <div class="panel-body">
                        <p><b>Organization Name</b><br />{{ $request->organization }}</p>
                        <p><b>Organization Website</b><br /><a href="@if( substr($request->website, 0, 4) != 'http' )http://@endif{{ $request->website }}" target="_blank">{{ $request->website }}</a></p>
                        <p><b>Organization Phone #</b><br />{{ $request->org_phone }}</p>
                        <p><b>Organization Address</b><br />{{ $request->address }}</p>
                        @if( $request->nonprofit_number != '')
                            <p><b>Is this organization a 501(c)3 nonprofit agency?</b><br />Yes</p>
                            <p><b>501(c)3 Number</b><br />{{ $request->nonprofit_number }}</p>
                        @else
                            <p><b>Is this organization a 501(c)3 nonprofit agency?</b><br />No</p>
                        @endif
                        @if( $request->prev_details != '')
                            <p><b>Have we previously contributed to your organization?</b><br />Yes</p>
                            <p><b>Name, Date &amp; Amount or Items Received Previously</b><br />{{ $request->prev_details }}</p>
                        @else
                            <p><b>Have we previously contributed to your organization?</b><br />No</p>
                        @endif
                        @if( $request->bfl_employee_list != '')
                            <p><b>Are there any Buy For Less, Smart Saver, Super Mercado and/or Uptown Grocery employees involved with your organization?</b><br />Yes</p>
                            <p><b>Please List:</b><br />{{ $request->bfl_employee_list }}</p>
                        @else
                            <p><b>Are there any Buy For Less, Smart Saver, Super Mercado and/or Uptown Grocery employees involved with your organization?</b><br />No</p>
                        @endif
                        <p><b>Organization's Primary Purpose/Mission</b><br />{{ $request->org_mission }}</p>
                    </div>
                </div>
                <div class="panel panel-info">
                    <div class="panel-heading">Event Details</div>
                    <div class="panel-body">
                        <p><b>Event Name</b><br />{{ $request->event }}</p>
                        <p><b>Event Website</b><br />{{ $request->event_website }}</p>
                        <p><b>Event Date</b><br />{{ $request->event_date }}</p>
                        <p><b>Event Time</b><br />{{ $request->event_time }}</p>
                        @if( $request->pickup_date != '0000-00-00')
                            <p><b>Pickup Date</b><br />{{ $request->pickup_date }}</p>
                        @endif
                        <p><b>Event Location</b><br />{{ $request->event_location }}</p>
                        <p><b>Beneficiaries</b><br />{{ $request->beneficiaries }}</p>
                        <p><b>Type of Request</b><br />{{ $request->request_type }}</p>
                        <p><b>@if( $request->request_type == 'Monetary/Funds/GiftCard' ) Amount Requested @else Number of Items Requested @endif</b><br />{{ $request->items }}</p>
                        <p><b>Additional Information</b><br />{{ $request->additional_info }}</p>
                    </div>
                </div>
                <div class="panel panel-info">
                    <div class="panel-heading">Advertising Details</div>
                    <div class="panel-body">
                        @if( $request->advertising_details != '')
                            <p><b>Will Buy For Less receive advertising or media promotion?</b><br />Yes</p>
                            <p><b>If yes, please describe:</b><br />{{ $request->advertising_details }}</p>
                        @else
                            <p><b>Will Buy For Less receive advertising or media promotion?</b><br />No</p>
                        @endif
                        <p><b>Logos Needed</b><br />{{ $request->logos }}</p>
                        <p><b>Email Address to send logo to</b><br />{{ $request->logo_email }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade status-modal" tabindex="-1" role="dialog" aria-labelledby="statusModal">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <form action="{{ action('DonationRequestsController@update', $request->id) }}" method="post" >
                    {{ csrf_field() }}

                    {{ method_field('PATCH') }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Change Status</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <label for="status">Status</label>
                                <select class="form-control" name="status" id="status">
                                    <option @if( $request->status == 'Pending') selected @endif value="Pending">Pending</option>
                                    <option @if( $request->status == 'Declined') selected @endif value="Declined">Declined</option>
                                    <option @if( $request->status == 'Approved') selected @endif value="Approved">Approved</option>
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection