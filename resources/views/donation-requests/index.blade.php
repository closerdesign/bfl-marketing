 @extends('layouts.app')

@section('content')

    <div class="container">
        @if( !isset( $_GET['keyword'] ) )
        <form action="">
            <div class="form-group">
                <span class="input-group">
                <input type="text" class="form-control" name="keyword" placeholder="Keyword" >
                <div class="input-group-btn">
                    <button class="btn btn-success">Search</button>
                </div>
            </span>
            </div>
        </form>
        @else
        <p>
            <a href="{{ action('DonationRequestsController@index') }}" class="btn btn-danger">
                <i class="fa fa-arrow-circle-left"></i> Go back
            </a>
        </p>
        @endif
        <div class="panel panel-primary">
            <div class="panel-heading">
                <i class="fa fa-money"></i> Donation Requests
            </div>
            <div class="panel-body">
                <div class="table-responsive">

                    <table class="table table-striped table-hover table-condensed">

                        <thead>
                        <tr>
                            <th>Brand</th>
                            <th>Organization</th>
                            <th>Event</th>
                            <th>Event Date</th>
                            <th>Created</th>
                            <th>Status</th>
                        </tr>
                        </thead>

                        <tbody>
                            @if( count($requests) == 0 )
                                <tr><td colspan="5">No donation requests have been submitted.</td></tr>
                            @else
                                @foreach($requests as $request)
                                    <tr>
                                        <td>
                                            {{ $request->brand }}
                                        </td>
                                        <td>
                                            <a href="{{ action('DonationRequestsController@show', $request->id) }}">{{ $request->organization }}</a>
                                        </td>
                                        <td>
                                            {{ $request->event }}
                                        </td>
                                        <td>
                                            {{ $request->event_date }}
                                        </td>
                                        <td>
                                            {{ $request->created_at }}
                                        </td>
                                        <td>
                                            @if( $request->status == 'Pending')
                                                <span class="label label-warning">
                                            @elseif( $request->status == 'Declined')
                                                <span class="label label-danger">
                                            @else
                                                <span class="label label-success">
                                            @endif
                                                <span class="text-uppercase">
                                                    {{ $request->status }}
                                                </span>
                                            </span>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>

                    </table>
                    {{ $requests->render() }}
                </div>
            </div>
        </div>
        <p><a href="{{ action('DonationRequestsController@all') }}">Looking for an old donation request?</a></p>
    </div>

@endsection