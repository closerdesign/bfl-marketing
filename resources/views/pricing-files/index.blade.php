@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="panel panel-default">
            <div class="panel-heading">
                Pricing Files
            </div>
            <div class="panel-body">
                <form action="{{ action('PricingFilesController@store') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="file">Upload A New File</label>
                        <input type="file" class="form-control" name="file" required >
                    </div>
                    <div class="form-group">
                        <label for="store">Store</label>
                        <select name="store" id="store" class="form-control" required >
                            <option value="">Select...</option>
                            @foreach(\App\Store::orderBy('store_code')->get() as $store )
                                <option value="{{ $store->store_code }}">{{ $store->store_code }} {{ $store->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <button class="btn btn-success">
                        <i class="fa fa-upload"></i> Upload
                    </button>
                </form>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                Processing Queue
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Store</th>
                            <th>Status</th>
                            <th>Created</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($files as $file)
                        <tr>
                            <td>{{ $file->name }}</td>
                            <td>{{ $file->store }}</td>
                            <td>{{ $file->status }}</td>
                            <td>{{ $file->created_at }}</td>
                            <td>
                                @if( $file->status == 'Pending' )
                                    <form action="{{ action('PricingFilesController@destroy', $file->id) }}" method="post" onsubmit="return confirm('Are you sure? This cannot be undone.')">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button class="btn-xs btn-danger" type="submit">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </form>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $files->render() }}
                </div>
            </div>
        </div>
    </div>

    @endsection