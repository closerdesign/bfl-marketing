@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <p class="text-right">
            <a href="{{ action('CampaignFaqsController@create') }}" class="btn btn-success">
                Create A New FAQ
            </a>
        </p>
        <div class="panel panel-default">
            <div class="panel-heading">
                Campaign FAQs
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-condensed">
                        <thead>
                        <tr>
                            <th>Question</th>
                            <th>Campaign</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($faqs as $faq)
                            <tr>
                                <th>
                                    <a href="{{ action('CampaignFaqsController@edit', $faq->id) }}">
                                        {{ $faq->question }}
                                    </a>
                                </th>
                                <td>[{{ $faq->campaign->brands->name }}] {{ $faq->campaign->name }}</td>
                                <td class="text-center">
                                    <form action="{{ action('CampaignFaqsController@destroy', $faq->id) }}" method="post" onsubmit="return confirm('Are you sure?')">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button class="btn-danger btn-xs">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @endsection