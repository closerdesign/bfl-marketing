@extends('layouts.app')

@section('content')

    <div class="container">
        <form action="{{ action('CampaignFaqsController@store') }}" method="post">
            {{ csrf_field() }}
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Create A New FAQ
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="question">Question</label>
                        <input type="text" class="form-control" name="question" value="{{ old('question') }}" required >
                    </div>
                    <div class="form-group">
                        <label for="answer">Answer</label>
                        <textarea name="answer" id="answer" cols="30" rows="10" class="form-control summernote" required >{!! old('answer') !!}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="campaign_id">Campaign</label>
                        <select name="campaign_id" id="campaign_id" class="form-control" required >
                            <option value="">Select...</option>
                            @foreach(\App\Campaign::orderBy('name')->get() as $campaign)
                                <option value="{{ $campaign->id }}">[{{ $campaign->brands->name }}] {{ $campaign->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <button class="btn btn-success">
                <i class="fa fa-save"></i> Save Changes
            </button>
        </form>
    </div>

    @endsection