@extends('layouts.app')

@section('content')

    <div class="container">
        <form action="{{ action('CampaignFaqsController@update', $faq->id) }}" method="post">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Edit FAQ: {{ $faq->question }}
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="question">Question</label>
                        <input type="text" class="form-control" name="question" value="{{ $faq->question }}" required >
                    </div>
                    <div class="form-group">
                        <label for="answer">Answer</label>
                        <textarea name="answer" id="answer" cols="30" rows="10" class="form-control summernote" required >{!! $faq->answer !!}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="campaign_id">Campaign</label>
                        <select name="campaign_id" id="campaign_id" class="form-control" required >
                            <option value="">Select...</option>
                            @foreach(\App\Campaign::orderBy('name')->get() as $campaign)
                                <option @if( $faq->campaign_id == $campaign->id ) selected @endif value="{{ $campaign->id }}">[{{ $campaign->brands->name }}] {{ $campaign->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <button class="btn btn-success">
                <i class="fa fa-save"></i> Save Changes
            </button>
        </form>
    </div>

    @endsection