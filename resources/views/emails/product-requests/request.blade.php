@extends('emails.layouts.general')

@section('content')

    <h1 style="text-align: center; font-style: italic">"{{ $request->toArray()['text'] }}"</h1>

    <hr>

    <p>Please check the information below:</p>

    @foreach($request->toArray() as $field => $value )
    <p>
        <b>{{ $field }}</b><br />
        {{ $value }} @if($field == 'msisdn') (THIS IS OUR GUEST PHONE NUMBER) @endif
    </p>
    @endforeach

    @endsection