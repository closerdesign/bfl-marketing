@extends('emails.layouts.general')

@section('content')

<p><b>Name: </b> {{ $request->name }}</p>
<p><b>Email: </b> {{ $request->email }}</p>
<p><b>Store: </b> {{ $request->store }}</p>
<p><b>Name of Event: </b> @if( $request->event == '' ) N/A @else {{ $request->event }} @endif</p>
<p><b>Date Needed: </b> {{ $request->date_needed }}</p>
<p><b>Amount Needed: </b> {{ $request->amount }}</p>
<p><b>Size: </b> {{ $request->size }}</p>
@if( $request->size == 'Other')
    <p><b>If Other, please specify: </b> {{ $request->size_other }}</p>
@endif
<p><b>Send By: </b> {{ $request->receive }}</p>
<p><b>Contents / Description: </b><br />{{ $request->contents }}</p>
<p><b>Additional Comments: </b><br /> {{ $request->comments }}</p>

@endsection