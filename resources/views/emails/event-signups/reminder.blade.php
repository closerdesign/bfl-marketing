@extends('emails.layouts.general')

@section('content')

    <p>Hello {{ $attendee->name  }}!</p>

    <p>
        This is just a reminder that you are signed up for the event listed below. If for any reason you can't make it,
        please respond to this email and let us know so we can have as accurate a count as possible. Thank you and we look
        forward to seeing you soon!
    </p>

    <div>
        <img src="{{ $event->image }}" alt="{{ $event->name }}">
    </div>
    <div>
        <p>
            <b>{!! $event->name !!}</b><br />
            {{ date('F jS, Y', strtotime($event->date)) }} @if($event->end_date != null) to {{ date('F jS, Y', strtotime($event->end_date)) }} @endif <br />
            <span class="small">@if($event->location){{ $event->location }} | @endif{{ date('h:i a', strtotime($event->time)) }} @if( $event->end_time != null ) to {{ date('h:i a', strtotime($event->end_time)) }} @endif</span>
        </p>
        <p>{{ $event->intro }}</p>
        <hr>
        {!! $event->description !!}
    </div>

@endsection