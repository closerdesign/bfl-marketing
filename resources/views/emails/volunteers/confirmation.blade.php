@extends('emails.layouts.general')

@section('content')

    <p>Hello <span style="text-transform: capitalize">{{ strtolower($volunteer->first_name) }}</span>,</p>
    
    <p>Thank you for registering to be a Holiday Volunteer for Buy For Less Corporation! Each Volunteer Training will be
    held in the cafe & dining area of the location hosting training. The training leaders are Simone Graves & Amy Hatfield.
    If you have any questions please contact our Support Center at (405) 302-6273.</p>

    <p>You are registered for Training: {{ $volunteer->training_session }}. View details: <a href="https://uptowngroceryco.com/volunteers">Click here</a>.</p>

    <p>All Volunteers are required to sign our liability form in order to volunteer. Please print, sign & bring to training. You will not be allowed to train without the signed form. If you are a minor you will need parental consent. Please print the attached form found <a href="https://www.dropbox.com/s/kf38ebefms6nqam/volunteer-documents-2.pdf?dl=0">HERE</a>.</p>

    <p>Training Attire - Please be sure to wear:</p>

    <ul>
        <li>Closed Toed, Non-Slip Shoes</li>
        <li>Pants, No shorts</li>
        <li>Short or Long Sleeved T-Shirt</li>
    </ul>

    <p>*If you are wearing improper clothing you will not be able to train or volunteer.</p>

    <p>Thank you so much and we look forward to seeing you soon!</p>

    @endsection