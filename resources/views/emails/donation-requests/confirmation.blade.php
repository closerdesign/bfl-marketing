@extends('emails.layouts.general')

@section('content')

    <p>Hello {{ $donation->first_name }}!</p>

    <p>Thank you for your submission to the Donations & Sponsorships Team at Buy For Less Corporation. Your request is being considered.</p>

    <p>
        <b>The Donations & Sponsorship Team</b><br />
        <img src="https://marketing.buyforlessok.com/img/brands-banner.png" />
    </p><br />

    <h3><b>Organization Details</b></h3>
    <p><b>Organization Name</b><br />{{ $donation->organization }}</p>
    <p><b>Organization Website</b><br />{{ $donation->website }}</p>
    <p><b>Organization Phone #</b><br />{{ $donation->org_phone }}</p>
    <p><b>Organization Address</b><br />{{ $donation->address }}</p>
    <p><b>Is this organization a 501(c)3 nonprofit agency?</b><br />{{ $donation->nonprofit }}</p>
    @if( $donation->nonprofit_number != '')
        <p><b>501(c)3 Number</b><br />{{ $donation->nonprofit_number }}</p>
    @endif
    <p><b>Have we previously contributed to your organization?</b><br />{{ $donation->prev_donation }}</p>
    @if( $donation->prev_details != '')
        <p><b>Name, Date &amp; Amount or Items Received Previously</b><br />{{ $donation->prev_details }}</p>
    @endif
    <p><b>Are there any Buy For Less, Smart Saver, Super Mercado and/or Uptown Grocery employees involved with your organization?</b><br />{{ $donation->bfl_employees }}</p>
    @if( $donation->bfl_employee_list != '')
        <p><b>Please List:</b><br />{{ $donation->bfl_employee_list }}</p>
    @endif
    <p><b>Organization's Primary Purpose/Mission</b><br />{{ $donation->org_mission }}</p>

    <h3><b>Event Details</b></h3>
    <p><b>Event Name</b><br />{{ $donation->event }}</p>
    <p><b>Event Website</b><br />{{ $donation->event_website }}</p>
    <p><b>Event Date</b><br />{{ $donation->event_date }}</p>
    <p><b>Event Time</b><br />{{ $donation->event_time }}</p>
    @if( $donation->pickup_date != '')
        <p><b>Pickup Date</b><br />{{ $donation->pickup_date }}</p>
    @endif
    <p><b>Event Location</b><br />{{ $donation->event_location }}</p>
    <p><b>Beneficiaries</b><br />{{ $donation->beneficiaries }}</p>
    <p><b>Type of Request</b><br />{{ $donation->request_type }}</p>
    <p><b>@if( $donation->request_type == 'Monetary/Funds/GiftCard' ) Amount Requested @else Number of Items Requested @endif</b><br />{{ $donation->items }}</p>
    <p><b>Additional Information</b><br />{{ $donation->additional_info }}</p>
    @if( $donation->attach_file != "" )
        <p><b>Event Image or PDF</b><br />
            <a href="{{ $donation->attach_file }}">{{ $donation->attach_file }}</a>
        </p>
    @endif

    <h3><b>Advertising Details</b></h3>
    <p><b>Will Buy For Less receive advertising or media promotion?</b><br />{{ $donation->advertising }}</p>
    @if( $donation->advertising_details != '')
        <p><b>If yes, please describe:</b><br />{{ $donation->advertising_details }}</p>
    @endif
    <p><b>Logos Needed</b><br />{{ $donation->logos }}</p>
    <p><b>Email Address to send logo to</b><br />{{ $donation->logo_email }}</p>

    <h3><b>Contact Details</b></h3>
    <p><b>First Name</b><br />{{ $donation->first_name }}</p>
    <p><b>Last Name</b><br />{{ $donation->last_name }}</p>
    <p><b>Email Address</b><br />{{ $donation->email }}</p>
    <p><b>Phone Number</b><br />{{ $donation->phone }}</p>
    <p><b>Closest/Preferred Store</b><br />{{ $donation->location }}</p>
    <p><b>How did you hear about us?</b><br />{{ $donation->referral_source }}</p>

@endsection