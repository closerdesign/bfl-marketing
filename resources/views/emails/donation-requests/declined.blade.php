@extends('emails.layouts.general')

@section('content')

    <p>Dear {{ $donation->first_name }},</p>

    <p>
        Thank you for inviting us to sponsor the {{ $donation->event }}. We agree that this is a wonderfully worthy cause and
        appreciate you thinking of us and how we could leverage our passions with your own. However, we must decline your
        request at this time due to our company channeling most of our donations and community sponsorships to those in the
        most vulnerable situations in our city.
    </p>

    <p>
        We continue to value highly the work that {{ $donation->organization }} does, and we hope to express our admiration
        and support of the organization again in the future. We are rooting for you and wishing you the best success!
    </p>

    <p>Thank you for your business, </p>

    <p>
        <b>The Donations & Sponsorship Team</b><br />
        <img src="https://marketing.buyforlessok.com/img/brands-banner.png" />
    </p>



@endsection