@extends('emails.layouts.general')

@section('content')

    <p>Please check the list below in order to keep posted about our current promos:</p>

    <hr>

    @foreach($promos as $promo)
        <p style="text-align: right">
            <img width="50" src="{{ $promo->image }}" alt="{{ $promo->name }}">
        </p>
        <p>
            <b><a href="{{ action('PromosController@edit', $promo->id) }}">{{ $promo->name }}</a></b><br/>
            {{ $promo->description }}<br />
            <b>Stores: @foreach($promo->stores as $store)[{{ $store->store_code }}]@endforeach</b><br />
            <b>Start Date:</b> {{ $promo->start_date }} - <b>End Date:</b> {{ $promo->end_date }}
        </p>

        <p><a href="{{ action('PromosController@excel', $promo->id) }}">Download Promo Sheet</a></p>

        <p><b>Processed by IT:</b> {{ $promo->it_processed }}</p>

        <hr>
    @endforeach

    @if( count($specials) > 0 )

        <p>Additionally, here are some specials our guests can redeem by using any of our PLUs or Barcodes:</p>

        @foreach($specials as $special)

            <table border="0" cellpadding="5" cellspacing="2">
                <tr>
                    <td style="background-color: #ccc; text-align: center; width: 50%"><b>Brand</b></td>
                    <td style="background-color: #ccc; text-align: center; width: 50%"><b>Name</b></td>
                </tr>
                <tr>
                    <td style="background-color: #f5f5f5; text-align: center">{{ $special->brands->name }}</td>
                    <td style="background-color: #f5f5f5; text-align: center">{{ $special->name }}</td>
                </tr>
                <tr>
                    <td style="background-color: #ccc; text-align: center" colspan="2"><b>Valid Dates</b></td>
                </tr>
                <tr>
                    <td style="background-color: #f5f5f5; text-align: center" colspan="2">{{ $special->date_from }} to {{ $special->date_to }}</td>
                </tr>
            </table><br />

            @if( count($special->plu_specials) > 0 )
                <table border="0" cellpadding="5" cellspacing="2">
                    <tr>
                        <td style="background-color: #ccc"><b>Item</b></td>
                        <td style="background-color: #ccc; text-align: center"><b>PLU</b></td>
                        <td style="background-color: #ccc; text-align: center"><b>Promo #</b></td>
                    </tr>
                    @foreach( $special->plu_specials as $product )
                        <tr>
                            <td>{{ $product->item_description }}</td>
                            <td style="text-align: center">@if( $product->plu > 0 ) {{ $product->plu }} @else <span style="color: red">PENDING</span> @endif</td>
                            <td style="text-align: center">@if($product->promo_number < 1) <span style="color: red">PENDING</span> @else {{ $product->promo_number }} @endif</td>
                        </tr>
                    @endforeach
                </table>
            @endif

            <hr>

        @endforeach

    @endif

@endsection