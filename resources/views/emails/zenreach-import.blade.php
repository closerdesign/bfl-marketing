@extends('emails.layouts.general')

@section('content')

    <p>Hi!</p>
    <p>I just wanted to let you know that I've finished importing new contacts from Zenreach for all of our stores.</p>
    <p>Today, I found {{ $count }} new contacts!</p>
    <p>Please have a wonderful day!</p>
    <p>Regards,</p>
    <p>
        <b>SARA</b><br />
        Media Team
    </p>

    @endsection