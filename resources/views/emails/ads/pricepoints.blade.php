@component('mail::message')

{{ $ad->brands->name }} {{ $ad->name }} Featured Items

Valid From {{ $ad->date_from }} to {{ $ad->date_to }}

<ul>
    @foreach($ad->specials->where('featured', 1) as $special)
    <li>{{ $special->item_description }}: {{ $special->price }} {{ $special->size }} @if( $special->public_comment != null ) ({{ $special->public_comment }}) @endif</li>
    @endforeach
</ul>

{{--@component('mail::button', ['url' => action('AdsController@pdf_template', $ad->id)])--}}
    {{--Click here to go to the Ad Template--}}
{{--@endcomponent--}}

Thanks,<br>
{{ config('app.name') }}

@endcomponent