<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Buy For Less - Get More!</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link href="http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900" rel="stylesheet" type="text/css" />
    <link href="http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
    <style type="text/css">body, #body_style {
            width: 100% !important;
            background: #ffffff;
            color: #ffffff;
            line-height: 1;
        }

        .ExternalClass {
            width: 100%;
        }
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
            line-height: 100%;
        }

        body {
            -webkit-text-size-adjust: none;
            -ms-text-size-adjust: none;
        }

        body, img, div, p, ul, li, span, strong, a {
            margin: 0;
            padding: 0;
        }

        table {
            border-spacing: 0;
        }

        table td {
            border-collapse: collapse;
        }

        a {
            color: {{ $color[1] }};
            text-decoration: underline;
            outline: none;
        }
        a:hover {
            text-decoration: none !important;
        }

        a[href^="tel"], a[href^="sms"] {
            text-decoration: none;
            color: {{ $color[1] }};
        }

        img {
            display: block;
            border: none;
            outline: none;
            text-decoration: none;
        }

        .img-responsive {
            display: block;
            max-width: 100%;
            height: auto;
        }

        table {
            border-collapse: collapse;
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        /*Style for Page design Start Here*/
        @media screen and (max-width: 599px) {
            body[yahoo] .wrapper-section-one{
                width: 100% !important;
            }
            body[yahoo] .content-block img{
                width: 100% !important;
            }
            body[yahoo] .content-block a img {
                width: 100% !important;
            }
            body[yahoo] .menu {
                width: auto !important;
            }
            body[yahoo] .content-block {
                width: 100% !important;
                display: block;
            }
            body[yahoo] .content-block-img img{
                width: auto !important;
            }
        }
        @media screen and (max-width: 479px) {
            body[yahoo] .bs_logo{
                width: 100% !important;
                text-align:center !important;
            }
            body[yahoo] .menu {
                width: 95% !important;
            }
        }
    </style>
</head>
<body style="font-family: 'PT Sans', sans-serif; font-size: 12px; color: #414042; margin: 0; width:100% !important;" yahoo="fix"><!-- Start - First -->
<table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper-section-one bg-editable" style="background: #2FB457" width="600">
    <tbody>
    <tr>
        <td colspan="3" height="20"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
    </tr>
    <tr>
        <td width="20"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
        <td>
            <table align="left" border="0" cellpadding="0" cellspacing="0" class="bs_logo" width="190">
                <tbody>
                <tr>
                    <td align="center"><a class="editable-lni" href="#"><img alt="" src="http://sendy.buyforlessok.com/uploads/1469229590.png" style="width: 200px; height: 64px;" /> </a></td>
                </tr>
                </tbody>
            </table>

            <table align="right" border="0" cellpadding="0" cellspacing="0" class="menu" width="280">
                <tbody>
                <tr>
                    <td align="right" style="padding-top: 10px; font-family: 'PT Sans', sans-serif; font-size: 16px; color: #ffffff;line-height: 24px;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                            <tr>
                                <td height="10"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
                            </tr>
                            <tr>
                                <td style="text-align:center;" width="50"><a class="editable" href="http://www.buyforlessok.com" style="text-decoration: none; color: #ffffff;">Website</a></td>
                                <td style="text-align:center;" width="5"><a class="editable" href="#" style="text-decoration: none; color: #ffffff;">| </a></td>
                                <td style="text-align:center;" width="50"><a class="editable" href="http://www.buyforlessok.com/ads" style="text-decoration: none; color: #ffffff;">Ads</a></td>
                                <td style="text-align:center;" width="5"><a class="editable" href="#" style="text-decoration: none; color: #ffffff; text-transform: uppercase;">| </a></td>
                                <td style="text-align:center;" width="50"><a class="editable" href="http://www.buyforlessok.com/contact" style="text-decoration: none; color: #ffffff;">Contact</a></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
        <td width="20"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
    </tr>
    <tr>
        <td colspan="3" height="20"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
    </tr>
    </tbody>
</table>
<!-- End First -->

@yield('content')

<!-- Start Fifteenth -->

<table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper-section-one" style="margin-top: -50px; background:#ffffff;" width="600">
    <tbody>
    <tr>
        <td height="50"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
    </tr>
    <tr>
        <td width="20"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
        <td class="editable content-block" colspan="6" style="color: #ed1c24;font-size: 18px;text-transform: capitalize;border-left: 4px double #ed1c24;padding-left: 10px;font-weight:bold;font-family:'Raleway', sans-serif;">Share The Love!</td>
        <td width="20"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
    </tr>
    <tr>
        <td height="20"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
    </tr>
    <tr>
        <td width="20"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
        <td class="content-block" style="text-align:center;"><a class="editable-lni" href="https://www.facebook.com/sharer/sharer.php?u=[webversion]" style="display:inline-block;"><img alt="" src="http://sendy.buyforlessok.com/uploads/1471103090.png" style="width: 180px; height: 101px;" /> </a></td>
        <td class="content-block" height="10" width="10"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
        <td class="content-block" style="text-align:center;"><a class="editable-lni" href="http://twitter.com/home?status=Get%20More%20at%20BUY%20FOR%20LESS" style="display:inline-block;"><img alt="" src="http://sendy.buyforlessok.com/uploads/1471103831.png" style="width: 180px; height: 101px;" /> </a></td>
        <td class="content-block" height="10" width="10"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
        <td class="content-block" style="text-align:center;"><a class="editable-lni" href="https://plus.google.com/share?url=[webversion]" style="display:inline-block;"><img alt="" src="http://sendy.buyforlessok.com/uploads/1471104256.png" style="width: 180px; height: 101px;" /> </a></td>
        <td width="20"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
    </tr>
    <tr>
        <td height="50"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
    </tr>
    </tbody>
</table>
<!-- End Fifteenth --><!-- Start Sixteenth -->

<table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper-section-one" style="background: #ed1c24;" width="600">
    <tbody>
    <tr>
        <td width="20"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
        <td class="content-block" colspan="3">
            <table align="center" border="0" cellpadding="0" cellspacing="0" style="color: #fff;font-size: 14px;" width="100%">
                <tbody>
                <tr>
                    <td class="content-block" valign="middle" width="200">
                        <table align="left" border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                            <tr>
                                <td height="20"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
                            </tr>
                            <tr>
                                <td class="editable" style="font-family:'PT Sans', sans-serif;color:#fff;font-size: 12px;">Copyright {{ date('Y') }} &copy; BUY FOR LESS</td>
                            </tr>
                            <tr>
                                <td height="20"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                    <td class="content-block" height="20" width="20"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
                    <td align="right" class="menu content-block" st-content="menu" style="font-family: 'PT Sans', sans-serif; font-size: 13px; color: #ffffff;" valign="middle" width="250">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                            <tr>
                                <td height="20"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
                            </tr>
                            <tr>
                                <td style="text-align:center;" width="110"><webversion>View web version</webversion></td>
                                <td style="text-align:center;" width="5"><a class="editable" href="#" style="text-decoration: none; color: rgb(255, 255, 255); text-transform: uppercase;font-size: 13px;">| </a></td>
                                <td style="text-align:center;" width="75"><unsubscribe>Unsubscribe here</unsubscribe></td>
                            </tr>
                            <tr>
                                <td height="20"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
        <td width="20"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
    </tr>
    </tbody>
</table>
<!-- End Sixteenth --></body>
</html>
