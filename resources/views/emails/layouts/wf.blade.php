
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta http-equiv="X-UA-Compatible" content="IE=edge" /><meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="format-detection" content="telephone=no" />
    <style type="text/css">body { margin: 0 !important; padding: 0 !important; -webkit-text-size-adjust: 100% !important; -ms-text-size-adjust: 100% !important; -webkit-font-smoothing: antialiased !important; }
        img { border: 0 !important; outline: none !important; }
        p { Margin: 0px !important; Padding: 0px !important; }
        table {  mso-table-lspace: 0px; mso-table-rspace: 0px; }
        td, a, span {   }
        .ExternalClass * {
            line-height: 100%;
        }
        span.MsoHyperlink {
            mso-style-priority: 99; color: inherit;
        }
        span.MsoHyperlinkFollowed {
            mso-style-priority: 99; color: inherit;
        }
        .em_font0 {
            font-size: 0px; line-height: 0px;
        }
        .em_black9, .em_black9 a {
            font-family: 'Lucida Sans', Arial, sans-serif;
            font-size: 9px;
            line-height: 12px;
            color: #000000;
            text-decoration: none;
        }
        .em_grey11, .em_grey11 a {
            font-family: 'Lucida Sans', Arial, sans-serif;
            font-size: 11px;
            line-height: 15px;
            color: #343434;
            font-weight: 600;
            text-decoration: none;
        }
        .em_white33 {
            font-family: 'Rescue', Arial, sans-serif;
            font-size: 30px;
            line-height: 31px;
            color: #ffffff;
        }
        .em_white83 {
            font-family: 'CooperBTforWFM', 'Times New Roman', Times, serif;
            font-size: 80px;
            line-height: 70px;
            color: #ffffff;
            font-weight: 500;
        }
        .em_white15 {
            font-family: 'Rescue', Arial, sans-serif;
            font-size: 14px;
            color: #ffffff;
        }
        .em_white15 a {
            text-decoration: none;
            color: #ffffff;
            display: block;
            line-height: 41px;
        }
        .em_white10 {
            font-family: 'Lucida Sans', Arial, sans-serif;
            font-size: 10px;
            color: #ffffff;
            font-weight: 600;
        }
        .em_white10 a {
            text-decoration: none;
            color: #ffffff;
            display: block;
            line-height: 40px;
        }
        .em_grey18 {
            font-family: 'Lucida Sans', Arial, sans-serif;
            font-size: 18px;
            line-height: 23px;
            color: #565553;
        }
        .em_grey16 {
            font-family: 'Lucida Sans', Arial, sans-serif;
            font-size: 16px;
            line-height: 18px;
            color: #565553;
            font-weight: 600;
        }
        .em_grey15 {
            font-family: 'Lucida Sans', Arial, sans-serif;
            font-size: 15px;
            line-height: 16px;
            color: #565553;
            font-weight: 600;
        }
        .em_grey36 {
            font-family: 'Lucida Sans', Arial, sans-serif;
            font-size: 36px;
            line-height: 38px;
            color: #565553;
        }
        .em_grey10, .em_grey10 a {
            font-family: 'Lucida Sans', Arial, sans-serif;
            font-size: 10px;
            line-height: 16px;
            color: #565553;
            font-weight: 600;
            text-decoration: none;
        }
        .em_green10, .em_green10 a {
            font-family: 'Lucida Sans', Arial, sans-serif;
            font-size: 10px;
            line-height: 12px;
            color: #006f46; font-weight: 600;
            text-transform: uppercase;
            text-decoration: none;
        }
        .em_black13 {
            font-family: 'Lucida Sans', Arial, sans-serif;
            font-size: 13px;
            line-height: 15px;
            color: #2c2c2c;
            font-weight: 600;
        }
        .em_grey12, .em_grey12 a {
            font-family: 'Lucida Sans', Arial, sans-serif;
            font-size: 12px;
            line-height: 15px;
            color: #747474;
            text-decoration: none;
        }
        .em_grey12_1 {
            font-family: 'Lucida Sans', Arial, sans-serif;
            font-size: 12px;
            line-height: 24px;
            color: #747474;
        }
        .em_grey12_1 a {
            color: #747474;
            text-decoration: underline;
        }
        .events {
            font-family: 'Lucida Sans', Arial, sans-serif;
            font-size: 12px;
        }
        hr {
            border: 0;
            height: 0;
            border-top: 1px solid rgba(0, 0, 0, 0.1);
            border-bottom: 1px solid rgba(255, 255, 255, 0.3);
        }

        @media only screen and (min-width:481px) and (max-width:599px) {
            .em_wrapper {
                width: 100% !important;
            }
            .em_side {
                width: 10px !important;
            }
            .em_h20 {
                height: 20px !important;
            }
            .em_hide {
                display: none !important;
            }
            .em_black9 {
                text-align: center !important;
            }
        }
        @media only screen and (max-width:480px) {
            td[class=header_mobhide] {
                display: none !important;
            }
            td[class=em_font18] {
                font-size: 18px !important;
                line-height: 24px !important;
                text-align: center !important;
            }
            .em_wrapper {
                width: 100% !important;
            }
            .em_side {
                width: 10px !important;
            }
            .em_h20 {
                height: 20px !important;
            }
            .em_hide, em_hide1 {
                display: none !important;
            }
            .em_black9 {
                text-align: center !important;
            }
            .em_white33 {
                font-size: 25px !important;
            }
            .em_white83 {
                font-size: 70px !important;
            }
            .em_br {
                display:block !important;
            }
        }
    </style>
</head>
<body bgcolor="#ffffff" style="margin: 0px;padding: 0px;-webkit-text-size-adjust: 100% !important;-ms-text-size-adjust: 100% !important;-webkit-font-smoothing: antialiased !important;">

@yield('content')

<div class="em_hide" style="white-space:nowrap;font:20px courier;color:#ffffff; background-color:#ffffff;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div>
</body>
</html>
