<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Responsive Email Template</title>
    <style type="text/css">.ReadMsgBody {width: 100%; background-color: #ffffff;}
        .ExternalClass {width: 100%; background-color: #ffffff;}
        body	 {width: 100%; background-color: #ffffff; margin:0; padding:0; -webkit-font-smoothing: antialiased;font-family: Georgia, Times, serif}
        table {border-collapse: collapse;}

        a, img{
            border:none;
            outline:none;
            text-decoration:none;
        }

        div, p, a, li, td { -webkit-text-size-adjust:none; }

        @media only screen and (max-width: 640px)  {
            body[yahoo] .deviceWidth {width:440px!important; padding:0;}
            body[yahoo] .center {text-align: center!important;}
        }

        @media only screen and (max-width: 479px) {
            body[yahoo] .deviceWidth {width:280px!important; padding:0;}
            body[yahoo] .center {text-align: center!important;}
        }
    </style>
</head>
<body leftmargin="0" marginheight="0" marginwidth="0" style="font-family: Georgia, Times, serif" topmargin="0" yahoo="fix"><!-- Wrapper -->
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
    <tr>
        <td bgcolor="#E5E5E5" style="padding-top:20px" valign="top" width="100%"><!-- Start Header-->
            <table align="center" border="0" cellpadding="0" cellspacing="0" class="deviceWidth" width="580">
                <tbody>
                <tr>
                    <td bgcolor="#FFFFFF" width="100%"><!-- Nav -->
                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="deviceWidth" valign="bottom" width="290">
                            <tbody>
                            <tr>
                                <td bgcolor="#c3d610" style="color:#FFFFFF; height: 175px; font-family: Tahoma, sans-serif; font-size:24px; line-height: 26px; padding-left:20px"><span style="font-size: 18px; color: #FFFFFF; font-weight: normal; text-align: left; font-family: Tahoma, sans-serif; display:inline; line-height: 18x; padding:5px; background-color:#3A3B36; margin-bottom:15px">&nbsp;ENJOY THE HOLIDAYS!&nbsp;</span>
                                    <p style="margin-top:8px">LET US DO THE COOKING!</p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <!-- End Nav --><!-- Logo -->

                        <table align="center" border="0" cellpadding="0" cellspacing="0" class="deviceWidth" width="280">
                            <tbody>
                            <tr>
                                <td align="center" class="center" style="padding-top:15px" valign="middle"><a href="http://www.uptowngroceryco.com?utm_source=FOX&utm_medium=Email" style="text-decoration:none;" target="_blank"><img src="http://sendy.buyforlessok.com/uploads/1475248840.png" style="width: 200px; height: 130px;" /> </a></td>
                            </tr>
                            </tbody>
                        </table>
                        <!-- End Logo --></td>
                </tr>
                <tr>
                    <td style="background-color:#FFFFFF; line-height:8px">&nbsp;</td>
                </tr>
                </tbody>
            </table>
            <!-- End Header --><!-- One Column -->

            <table align="center" border="0" cellpadding="0" cellspacing="0" class="deviceWidth" width="580">
                <tbody>
                <tr>
                    <td style="font-size:0px"><a href="http://holidaymeals.uptowngroceryco.com?utm_source=FOX&utm_medium=Email" style="text-decoration:none;" target="_blank"><img alt="Holiday Meals by Uptown Grocery Co" class="deviceWidth" src="http://sendy.buyforlessok.com/uploads/1478625551.jpg" style="width: 580px;" /></a></td>
                </tr>
                <tr>
                    <td style="background-color:#FFFFFF; line-height:8px">&nbsp;</td>
                </tr>
                </tbody>
            </table>
            <!-- End One Column --><!-- Start Header-->

            <table align="center" bgcolor="#3A3B36" border="0" cellpadding="0" cellspacing="0" class="deviceWidth" width="580">
                <tbody>
                <tr>
                    <td bgcolor="#3A3B36" width="100%"><!-- TEXT -->
                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="deviceWidth" valign="top" width="48%">
                            <tbody>
                            <tr>
                                <td bgcolor="#3A3B36" style="color:#FFFFFF; height: 175px; font-family: Tahoma, sans-serif; font-size:14px; line-height: 18px; padding:8px">
                                    <p><b>Available at All Locations!</b><br />
                                        <br />
                                        Spend more quality time with your loved ones and count your blessings while we do the cooking.</p>

                                    <p><strong>Order online at: <a style="color: #C3D610; text-decoration: underline" href="http://holidaymeals.uptowngroceryco.com?utm_source=FOX&utm_medium=Email">holidaymeals.uptowngroceryco.com</a></strong></p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <!-- End TEXT --><!-- IMG -->

                        <table align="right" bgcolor="#3A3B36" border="0" cellpadding="0" cellspacing="0" class="deviceWidth" style="font-size:0px" width="49%">
                            <tbody>
                            <tr>
                                <td align="right" bgcolor="#3A3B36" class="center" style="padding:0px; font-size:0px" valign="middle"><a href="http://www.uptowngroceryco.com?utm_source=FOX&utm_medium=Email" style="text-decoration:none;" target="_blank"><img class="deviceWidth" src="http://sendy.buyforlessok.com/uploads/1475255049.jpg" style="width: 290px; height: 200px;" /> </a></td>
                            </tr>
                            </tbody>
                        </table>
                        <!-- End IMG --></td>
                </tr>
                <tr>
                    <td style="background-color:#FFFFFF; line-height:8px">&nbsp;</td>
                </tr>
                </tbody>
            </table>
            <!-- End Header --><!-- Start Header-->

            @yield('content')

            <table align="center" bgcolor="#c3d610" border="0" cellpadding="10" cellspacing="0" class="deviceWidth" width="580">
                <tbody>
                <tr>
                    <td align="center" style="width:33%; color:#FFFFFF; font-family: Tahoma, sans-serif; font-size:18px; line-height: 20px; padding:5px"><a href="http://www.uptowngroceryco.com?utm_source=FOX&utm_medium=Email" style="color:#FFFFFF; text-decoration:none;" target="_blank"><span style="color:#FFFFFF; text-decoration:none;">WEBSITE</span></a></td>
                    <td align="center" style="width:33%; color:#FFFFFF; font-family: Tahoma, sans-serif; font-size:18px; line-height: 20px; padding:5px"><a href="http://uptowngroceryco.com/ad/page1?utm_source=FOX&utm_medium=Email" style="color:#FFFFFF; text-decoration:none;" target="_blank"><span style="color:#FFFFFF; text-decoration:none;">WEEKLY AD</span></a></td>
                    <td align="center" style="width:33%; color:#FFFFFF; font-family: Tahoma, sans-serif; font-size:18px; line-height: 20px; padding:5px"><a href="http://uptowngroceryco.com/contact/1230?utm_source=FOX&utm_medium=Email" style="color:#FFFFFF; text-decoration:none;" target="_blank"><span style="color:#FFFFFF; text-decoration:none;">CONTACT</span></a></td>
                </tr>
                </tbody>
            </table>
            <!-- /3 Links --><!-- Bottom Info -->

            <table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" class="deviceWidth" width="580">
                <tbody>
                <tr>
                    <td>
                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="deviceWidth" valign="top" width="37%">
                            <tbody>
                            <tr><!-- Contact -->
                                <td align="left" bgcolor="#FFFFFF" style="font-family: Tahoma, sans-serif; font-size:14px; color:#68676B;text-align:center;" valign="top"><span style="font-size:18px; line-height:35px"><b>CONTACT US</b></span><br />
                                    <i style="color: rgb(104, 103, 107); text-decoration: none;"><a href="http://www.uptowngroceryco.com?utm_source=FOX&utm_medium=Email" style="color:#C3D610; text-decoration:none" target="_blank">UptownGrocery.store</a></i><br />
                                    (405)509-2700 - Edmond<br />
                                    (405)242-6080 - The Village<br /></td>
                            </tr>
                            </tbody>
                        </table>

                        <table align="right" border="0" cellpadding="0" cellspacing="0" class="deviceWidth" valign="top" width="58%">
                            <tbody>
                            <tr><!--Social-->
                                <td align="right" bgcolor="#FFFFFF" style="font-family: Tahoma, sans-serif;  color:#68676B;text-align:center; " valign="top"><span style="font-size:18px; line-height:35px;"><b>STAY CONNECTED</b></span><br />
                                    <a href="https://www.facebook.com/UptownGroceryCo" style="text-decoration:none;" target="_blank"><img src="http://dev.sinclairdigital.com/email-temp/images/social/fb-o.jpg" style="margin-top:-5px" /> </a> <a href="https://twitter.com/uptown_grocery" style="text-decoration:none;" target="_blank"> <img src="http://dev.sinclairdigital.com/email-temp/images/social/twitter-o.jpg" style="margin-top:-5px" /> </a></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>

            <table align="center" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" class="deviceWidth" width="580">
                <tbody>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                </tbody>
            </table>
            <!-- end Bottom Info -->

            <div style="height:35px">&nbsp;</div>
            <!-- spacer --></td>
    </tr>
    </tbody>
</table>
<!-- End Wrapper --></body>
</html>