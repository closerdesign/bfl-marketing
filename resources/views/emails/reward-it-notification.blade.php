@extends('emails.layouts.general')

@section('content')

    <p>A PLU is required for the reward: <b>{{ $reward->name }}</b>. You can view and edit this reward here:</p>

    <p><a href="{{ \Illuminate\Support\Facades\URL::to('') }}/rewards/{{ $reward->id }}/edit">{{ \Illuminate\Support\Facades\URL::to('') }}/rewards/{{ $reward->id }}/edit</a></p>

@endsection