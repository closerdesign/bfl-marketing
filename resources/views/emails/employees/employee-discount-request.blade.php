@extends('emails.layouts.general')

@section('content')

    <p>Please check the information below:</p>

    @foreach($request->toArray() as $field => $value)
    <p>
        <b>{{ $field }}</b><br />{{ $value }}
    </p>
    @endforeach

    @endsection