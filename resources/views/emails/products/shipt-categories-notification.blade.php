@extends('emails.layouts.general')

@section('content')

    <p>A new file has been generated and saved successfully at SHIPT S3. You'll find the sent data attached in the file: <i>{{ $filename }}</i></p>
    <p>Please have a wonderful day!</p>
    <p>Regards,</p>
    <p>
        <b>
            Media Team<br/>
            BFL Corp.
        </b>
    </p>

    @endsection