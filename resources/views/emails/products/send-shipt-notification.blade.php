@extends('emails.layouts.general')

@section('content')

    <p>A new products file with products information has been sent to SHIPT S3. You'll find attached the data sent in the file: <i>{{ $filename }}</i>.</p>
    <p>Regards,</p>
    <p>
        <b>Media Team</b><br />
        BFL Corp.
    </p>

    @endsection