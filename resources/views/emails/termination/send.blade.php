@extends('emails.layouts.general')

@section('content')

    @foreach($termination->toArray() as $field => $value)
    <p>
        <b>{{ $field }}</b><br />{{ $value }}
    </p>
    @endforeach

    @endsection