@extends('emails.layouts.uptown_fox')

@section('content')

    @foreach($events as $event)

    <table align="center" bgcolor="#3A3B36" border="0" cellpadding="0" cellspacing="0" class="deviceWidth" width="580">
        <tbody>
        <tr>
            <td bgcolor="#3A3B36" width="100%"><!-- TEXT -->
                <table align="right" border="0" cellpadding="0" cellspacing="0" class="deviceWidth" valign="top" width="48%">
                    <tbody>
                    <tr>
                        <td bgcolor="#3A3B36" style="color:#FFFFFF; height: 175px; font-family: Tahoma, sans-serif; font-size:14px; line-height: 18px; padding:8px">
                            <p><b style="text-transform: uppercase"><a style="color: #C3D610; text-decoration: underline" href="http://uptowngroceryco.com/events?utm_source=FOX&utm_medium=Email">{{ $event->name }}</a></b></p>
                            <p>{{ $event->intro }}</p>
                            <p>{{ date('F jS', strtotime($event->date)) }} at {{ date('H:i', strtotime($event->time)) }}</p>
                            <p style="margin-top: 20px"><a style="background: red; padding: 10px; color: #ffffff; font-weight: bold;" href="http://www.uptowngroceryco.com/events?utm_source=FOX&utm_medium=Email">View more</a></p>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <!-- End TEXT --><!-- IMG -->

                <table align="left" bgcolor="#3A3B36" border="0" cellpadding="0" cellspacing="0" class="deviceWidth" style="font-size:0px" width="49%">
                    <tbody>
                    <tr>
                        <td align="right" bgcolor="#3A3B36" class="center" style="padding:0px; font-size:0px" valign="middle"><a href="http://www.uptowngroceryco.com/events?utm_source=FOX&utm_medium=Email" style="text-decoration:none;" target="_blank"><img class="deviceWidth" src="{{ \Illuminate\Support\Facades\URL::to('/resources/events') . '/' . $event->image }}" style="width: 290px;" /> </a></td>
                    </tr>
                    </tbody>
                </table>
                <!-- End IMG --></td>
        </tr>
        <tr>
            <td style="background-color:#FFFFFF; line-height:8px">&nbsp;</td>
        </tr>
        </tbody>
    </table>
    <!-- End Header -->

    @endforeach

    @foreach($promos as $promo)

        <!-- Start Header-->

        <table align="center" bgcolor="#3A3B36" border="0" cellpadding="0" cellspacing="0" class="deviceWidth" width="580">
            <tbody>
            <tr>
                <td bgcolor="#3A3B36" width="100%"><!-- TEXT -->
                    <table align="left" border="0" cellpadding="0" cellspacing="0" class="deviceWidth" valign="top" width="48%">
                        <tbody>
                        <tr>
                            <td bgcolor="#3A3B36" style="color:#FFFFFF; height: 175px; font-family: Tahoma, sans-serif; font-size:14px; line-height: 18px; padding:8px">
                                <p><b style="text-transform: uppercase"><a style="color: #C3D610; text-decoration: underline" href="http://uptowngroceryco.com/promos?utm_source=FOX&utm_medium=Email">{{ $promo->name }}</a></b></p>
                                {!! $promo->description !!}
                                <p style="margin-top: 20px"><a style="background: red; padding: 10px; color: #ffffff; font-weight: bold" href="http://www.uptowngroceryco.com/promos?utm_source=FOX&utm_medium=Email">View more</a></p>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <!-- End TEXT --><!-- IMG -->

                    <table align="right" bgcolor="#3A3B36" border="0" cellpadding="0" cellspacing="0" class="deviceWidth" style="font-size:0px" width="49%">
                        <tbody>
                        <tr>
                            <td align="right" bgcolor="#3A3B36" class="center" style="padding:0px; font-size:0px" valign="middle"><a href="http://www.uptowngroceryco.com/promos?utm_source=FOX&utm_medium=Email" style="text-decoration:none;" target="_blank"><img class="deviceWidth" src="{{ \Illuminate\Support\Facades\URL::to('/resources/promos') . '/' . $promo->image }}" style="width: 290px;" /> </a></td>
                        </tr>
                        </tbody>
                    </table>
                    <!-- End IMG --></td>
            </tr>
            <tr>
                <td style="background-color:#FFFFFF; line-height:8px">&nbsp;</td>
            </tr>
            </tbody>
        </table>
        <!-- End Header --><!-- 3 Links -->

    @endforeach

    @endsection