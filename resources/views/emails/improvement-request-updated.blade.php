@extends('emails.layouts.general')

@section('content')

    <p>Hi {{ $improvement_request->users->name }}!</p>

    <p>We just want to let you know that your request {{ $improvement_request->title }} has been updated and the new status is <b>{{ $improvement_request->status }}</b>.</p>

    <p>Thanks for your feedback!</p>

    @endsection