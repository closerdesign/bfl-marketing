@extends('emails.layouts.general')

@section('content')

    <p>The following form was successfully submitted:</p>

    @foreach($request->toArray() as $field => $value)

        <p>
            <b>{{ strtoupper(str_replace("_", " ", $field)) }}</b><br />{{ $value }}
        </p>

    @endforeach

@endsection