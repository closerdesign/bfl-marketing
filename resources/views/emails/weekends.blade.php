<!DOCTYPE html>
<html lang="en">
<head><meta charset="utf-8">
    <title>{{ $ad->brands->name }} - Specials</title>
    <meta name="viewport" content="width=device-width">
    <style type="text/css">body {
            margin: 0
        }
        .nugget-page .nugget-container {
            margin: auto
        }
        .nugget-page .nugget-header,
        .nugget-page .nugget-footer {
            background: #f5f5f5;
            background-size: 20px;
            margin-bottom: 15px;
        }
        .nugget-page .nugget-header td,
        .nugget-page .nugget-footer td {
            text-align: center
        }
        .nugget-page .nugget-header {
            border-bottom: 3px solid #000
        }
        .nugget-page .nugget-footer {
            border-top: 3px solid #000
        }
        .nugget-page .nugget-header td {
            text-align: center;
            vertical-align: middle
        }
        .nugget-page .nugget-heading {
            background: #000;
            border-radius: 10px;
            color: #fff;
            padding: 15px;
            text-align: center;
            margin-top:15px
        }
        .nugget-page .nugget-heading a {
            color: #fff;
            text-decoration: none
        }
        .nugget-page .nugget-preview {
            display: none
        }
        .nugget-page .nugget-emaillink {
            text-align: center
        }
        .nugget-page .nugget-figure {
            position: relative
        }
        .nugget-page .nugget-figcaption {
            background: rgba(255, 255, 255, 0.75);
            border-top: 1px solid rgba(255, 255, 255, 0.5);
            border-bottom: 1px solid rgba(255, 255, 255, 0.5);
            bottom: 30%;
            padding: 10px 0;
            position: absolute;
            right: 0;
            width: 100%
        }
        .nugget-page .nugget-item-listing tr + tr td {
            border-top: 1px solid #eee
        }
        .nugget-page .nugget-item-listing td {
            padding-top: 20px;
            vertical-align: top
        }
        .nugget-page .nugget-item-listing td > img {
            display: block;
            margin-bottom: 1em
        }
        .nugget-page .nugget-items .nugget-size {
            text-transform: lowercase
        }
        .nugget-page .nugget-items .nugget-special-diet,
        .nugget-page .nugget-items .nugget-badge {
            max-height: 75px;
            max-width: 125px
        }
        .nugget-page .nugget-items .nugget-price,
        .nugget-page .nugget-items .nugget-savings {
            display: inline-block
        }
        .nugget-old{text-decoration: line-through}
        .nugget-page .nugget-items .nugget-price small {
            font-size: 1em
        }
        .nugget-page .nugget-items .nugget-price i {
            font-style: normal
        }
        .nugget-page .nugget-items .nugget-price .perea,
        .nugget-page .nugget-items .nugget-price .perlb,
        .nugget-page .nugget-items .nugget-price .crv {
            font-size: 0.8em
        }
        .nugget-page .nugget-menu table tr + tr td {
            border-top: 1px solid #eee
        }
        .nugget-page .nugget-menu td[colspan] {
            text-align: center
        }
        .nugget-page .nugget-menu td + td {
            border-left: 1px solid #eee
        }
        .nugget-page .nugget-links a {
            display: block;
            margin: 1em 0;
            text-align: center
        }
        .nugget-page .nugget-fineprint {
            margin: auto
        }
        .nugget-page .nugget-rule {
            border-top: 3px solid #000
        }
        .nugget-page {
            background: #fff;
            font: 13px "Helvetica Neue", Helvetica, Arial, sans-serif
        }
        .nugget-page h1 {
            line-height: 1em
        }
        .nugget-page h2,
        .nugget-page h3,
        .nugget-page .nugget-items p {
            line-height: 1;
            margin: 0
        }
        .nugget-page .nugget-header h1 {
            font-size: 24px
        }
        .nugget-page .nugget-header.nugget-rev-1 h1 {
            font-size: 28px;
            line-height: 1.25em;
            text-align: center
        }
        .nugget-page small {
            font-size: 0.6em
        }
        .nugget-page a {
            color: #000
        }
        .nugget-page .nugget-grid {
            margin: 10px auto 5px auto
        }
        .nugget-page .nugget-grid th,
        .nugget-page .nugget-grid td {
            font-weight: bold;
            text-align: center
        }
        .nugget-page .nugget-grid tbody th {
            text-transform: uppercase
        }
        .nugget-page .nugget-coupon .scan-and-save {
            padding-right: 0
        }
        .nugget-page .nugget-coupon .scan-and-save img {
            float: left;
            width: 60px
        }
        .nugget-page .nugget-coupon .barcode {
            padding-top: 0
        }
        .nugget-page .nugget-coupon h2 {
            font-size: 18px;
            line-height: 1;
        }
        .nugget-page .nugget-coupon h3 {
            font-size: 30px
        }
        .nugget-page .nugget-coupon h4 {
            font-size: 20px;
            margin: 0
        }
        .nugget-page .nugget-coupon .sale-price {
            color: #ce242e
        }
        .nugget-page .nugget-coupon del {
            color: #666
        }
        .nugget-page .nugget-coupon del {
            font-weight: normal;
            text-decoration: none;
            position: relative
        }
        .nugget-page .nugget-coupon del img {
            position: absolute;
            top: 15px;
            left: -3px
        }
        .nugget-page .nugget-figcaption {
            text-align: center;
            text-transform: uppercase
        }
        .nugget-page .nugget-figcaption h2 {
            color: #ce242e;
            font-size: 2em;
            font-weight: bold
        }
        .nugget-page .nugget-figcaption h2,
        .nugget-page .nugget-figcaption p {
            margin: 0
        }
        .nugget-page .nugget-coupon .sale-price {
            font-size: 0.8em;
            line-height: 0.8em;
        }
        .nugget-page .nugget-items h3 {
            font-size: 2em
        }
        .nugget-page .nugget-items .nugget-price {
            color: #ce2931
        }
        .nugget-page .nugget-coupon h3 del .retail-price {
            color: #666;
            font-size: 0.6em
        }
        .nugget-page .nugget-grid th {
            text-transform: uppercase;
            width: 30px
        }
        .nugget-page .nugget-grid tbody th {
            border-bottom: 1px solid #eee
        }
        .nugget-page .nugget-grid th + th,
        .nugget-page .nugget-grid td + td {
            border-left: 1px solid #eee
        }
        .nugget-page .nugget-heading h2 {
            font-size: 24px;
            margin: 0
        }
        .nugget-page .nugget-item-listing h2 {
            font-size: 22px
        }
        .nugget-page .nugget-item-listing h2,
        .nugget-page .nugget-item-listing h3,
        .nugget-page .nugget-item-listing p {

        }
        .nugget-page .nugget-item-listing .nugget-all-stores {
            font-size: 0.6em;
            padding-top: 0;
            text-align: center
        }
        .nugget-page .nugget-menu {
            font-size: 1.1em;
            font-weight: bold
        }
        .nugget-page .nugget-menu a {
            text-transform: capitalize
        }
        .nugget-page .nugget-items h2 a,
        .nugget-page .nugget-menu a,
        .nugget-page .barcode p a,
        .nugget-page .nugget-footer p a {
            color: #000
        }
        .nugget-page .nugget-links td,
        .nugget-page .nugget-careers td {
            text-align: center
        }
        .nugget-page .nugget-links a,
        .nugget-page .nugget-careers a {
            background: #000;
            border-radius: 3px;
            color: #fff;
            font-size: 1.1em;
            font-weight: bold;
            padding: 10px;
            text-decoration: none
        }
        .nugget-page .nugget-careers a {
            background: #ce2931;
            display: block
        }
        .nugget-page .nugget-links h2 {
            margin-bottom: 1em
        }
        .nugget-page .nugget-links .nugget-ad-cover {
            border: 1px solid #ccc
        }
        .nugget-page .nugget-footer p {
            font-size: 0.9em;
            line-height: 1.5em;
            margin-top: 1em
        }
        .nugget-page .nugget-footer p + p {
            text-align: center
        }
        .nugget-page .nugget-footer .nugget-disclaimer {
            text-align: justify
        }
        .nugget-print .nugget-emaillink,
        .nugget-print .nugget-footer,
        .nugget-print .nugget-links {
            display: none
        }
        h2{line-height:1}
        .product-price{text-align:center;color:#ce2931;font-size:24px;font-weight:bold;margin-top:15px;padding:10px}
    </style>
</head>
<body class="nugget-ad-email">
<table border="0" cellpadding="0" cellspacing="0" class="nugget-page" width="100%">
    <tbody>
    <tr>
        <td>
            <table border="0" cellpadding="10" cellspacing="0" class="nugget-container" width="320">
                <tbody>
                <tr>
                    <td class="nugget-preview">
                        @if( $lang == 'es' )
                            Don&#39;t tell anyone! These are the {{ $ad->brands->name }} secret specials for you!
                        @else
                            Don&#39;t tell anyone! These are the {{ $ad->brands->name }} secret specials for you!
                        @endif
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- Header -->

            <table bgcolor="#f5f5f5" border="0" cellpadding="10" cellspacing="0" class="nugget-container nugget-header" width="100%">
                <tbody>
                <tr>
                    <td class="nugget-emaillink">
                        <a href="[webversion]">
                            @if( $lang == 'es' )
                                No puedes ver este mensaje? |
                            @endif
                                Having trouble reading this email?

                        </a>
                    </td>
                </tr>
                <tr>
                    <td><a href="http://{{ $ad->brands->website }}/ads"><img alt="{{ $ad->brands->name }}" src="{{ $ad->brands->logo }}" style="width: 320px;" /></a></td>
                </tr>
                </tbody>
            </table>
            <!-- Scan & Save -->

            <table border="0" cellpadding="0" cellspacing="0" class="nugget-container nugget-coupon" width="320">
                <tbody>
                <tr>
                    <td class="nugget-heading" colspan="2">
                        <h2>
                            @if( $lang == 'es' )
                                Únicamente |
                            @endif
                                Valid Only
                            <br />{{ date('F jS', strtotime($ad->date_from)) }} - {{ date('F jS', strtotime($ad->date_to)) }}
                        </h2>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" cellpadding="10" cellspacing="0" width="320">
                            <tbody>
                            <tr>
                                <td class="scan-and-save" width="60"><img alt="scan-and-save" src="http://sendy.buyforlessok.com/uploads/1481739188.png" style="width: 60px;" /></td>
                                <td>
                                    <h2>
                                        @if( $lang == 'es' )
                                            Haz sido incluído en nuestras ofertas secretas! |
                                        @endif
                                            You've unlocked our Secret Specials!
                                    </h2>

                                    <h3>
                                        <span class="sale-price" style="color:#FF0000;">
                                            @if( $lang == 'es' )
                                                No olvides traer tu teléfono ó Imprimir! |
                                            @endif
                                                Don't forget to bring your phone or printout!
                                        </span>
                                    </h3>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>

            <table border="0" cellpadding="5" cellspacing="0" class="nugget-container nugget-items" width="310">
                <tbody>
                <tr>
                    <td class="nugget-heading">
                        <h2>
                            <span style="font-size:20px;">
                                @if( $lang == 'es' )
                                    Ofertas Especiales |
                                @endif
                                    Special Offers
                            </span>
                        </h2>
                    </td>
                </tr>

                @foreach($specials as $special)

                    <tr>
                        <td>
                            <table border="0" cellpadding="5" cellspacing="0" class="nugget-item-listing" width="310">
                                <tbody>
                                <tr>
                                    <td><span style="font-size:24px;"><a href="{{ $ad->brands->website }}/ads">

                                        <img alt="" src="{{ $special->image }}" style="width: 300px; height: 300px;" /></a></span>

                                        <h2 style="text-align: center;"><span style="font-size:24px;">{{ $special->item_description }}</span></h2>

                                        <p style="text-align: center;"><span style="font-size:20px;">{{ $special->size }}</span></p>

                                        <p>&nbsp;</p>

                                        <p style="text-align: center;"><span style="font-size:36px; color: #ff0000"><b>{{ $special->price }}</b></span></p>

                                        <p style="text-align: center;">&nbsp;</p>

                                        <p style="text-align: center;">
                                            <span style="font-size:12px;text-decoration: line-through;">
                                                @if( $lang == 'es' )
                                                    Precio Regular {{ $special->srp }} |
                                                @endif
                                                    Regular Price {{ $special->srp }}
                                            </span>
                                        </p>

                                        <p style="text-align: center;">&nbsp;</p>

                                        @if( $special->plu != "" )
                                        <p style="text-align:center">
                                            <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($special->plu, "EAN8") }}" alt="barcode" width="100%" />
                                        </p>
                                        @endif

                                        <p style="text-align:center">
                                            <span style="font-size:12px;">
                                                @if( $lang == 'es' )
                                                    ¡Hasta Agotar Existencias! |
                                                @endif
                                                    While Supplies Last!
                                            </span>
                                        </p>

                                        <p>&nbsp;</p>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>

                @endforeach

                </tbody>
            </table>
            <!-- Links -->

            <table border="0" cellpadding="0" cellspacing="0" class="nugget-container nugget-links" width="320">
                <tbody>
                <tr>
                    <td>
                        <table border="0" cellpadding="10" cellspacing="0" width="320">
                            <tbody>
                            <tr>
                                <td>
                                    <a href="http://{{ $ad->brands->website }}/ads">
                                        @if( $lang == 'es' )
                                            M&aacute;s Ofertas? Chequea Nuestro Ad! |
                                        @endif
                                            More deals? Check Our Weekly Ad!
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- Footer -->

            <table bgcolor="#f5f5f5" border="0" cellpadding="0" cellspacing="0" class="nugget-container nugget-footer" width="100%">
                <tbody>
                <tr>
                    <td>
                        <table border="0" cellpadding="5" cellspacing="0" class="nugget-fineprint" width="310">
                            <tbody>
                            <tr>
                                <td>
                                    <p>
                                        <strong>
                                            @if( $lang == 'es' )
                                                Precios Válidos Únicamente |
                                            @endif
                                                Prices Valid Only
                                            <br />{{ date('F jS', strtotime($ad->date_from)) }} - {{ date('F jS', strtotime($ad->date_to)) }}
                                        </strong>
                                    </p>

                                    <p>
                                        <strong>
                                            @if( $lang == 'es' )
                                                HASTA AGOTAR EXISTENCIAS |
                                            @endif
                                                WHILE SUPPLIES LAST
                                        </strong>
                                    </p>

                                    <p>
                                        @if( $lang == 'es' )
                                            Este mensaje ha sido enviado a [Email]. |
                                        @endif
                                            This email was sent to [Email].
                                        <br />
                                        @if( $lang == 'es' ) | 
                                            <unsubscribe>Date de baja</unsubscribe> o <preferences>actualiza tus preferencias.</preferences>.
                                        @endif
                                            <unsubscribe>Unsubscribe</unsubscribe> or <preferences>update your email preferences</preferences>.
                                    </p>

                                    <p>Copyright &copy; {{ date('Y') }}&nbsp;{{ $ad->brands->name }}</p>

                                    <p><img alt="{{ $ad->brands->name }}" src="{{ $ad->brands->logo }}" style="width: 120px;" /></p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>
