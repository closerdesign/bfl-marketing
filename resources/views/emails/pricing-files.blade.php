@extends('emails.layouts.general')

@section('content')

    <p>
        <b>Hello Everyone!</b>
    </p>

    <p>We just want to let you know that a new pricing file has been processed. Please check the details below:</p>

    <p>
        <b>Pricing File:</b><br />{{ $pricing->name }}
    </p>

    <p>
        <b>Created:</b> {{ $pricing->created_at }}
    </p>

    <p>
        <b>Total Records Found and Processed:</b> {{ $total }}
    </p>

    @endsection