@extends('emails.layouts.general')

@section('content')

    <h1>{{ $update->title }}</h1>

    {!! $update->description !!}

    <hr>

    <p style="text-align: right">Created: {{ $update->created_at }}</p>

    @endsection