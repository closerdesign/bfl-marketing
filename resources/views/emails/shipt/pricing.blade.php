@extends('emails.layouts.general')

@section('content')

    <p>Good Morning!</p>

    <p>I've processed and attached to this message a new Shipt Pricing File: {{ $filename }}.</p>

    <p>Have a nice day!</p>

    <p>
        <b>SARA</b>
        Media Team
    </p>

    @endsection