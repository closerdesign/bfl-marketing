@extends('emails.layouts.general')

@section('content')

    <p>A new featured products file has been generated: <b>{{ $filename }}</b>.</p>
    <p>Thanks!</p>
    <p>
        <b>SARA</b><br />
        Media Team
    </p>

    @endsection