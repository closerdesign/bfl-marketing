{{--Promos--}}

@if(count($promos) > 0)

    <!--Section Fourth End here--><!--Section Fifth Start here-->

    <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper-section-one" style="background:#f3f3f3;" width="600">
        <tbody>
        <tr>
            <td height="20"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
        </tr>
        <tr>
            <td width="20"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
            <td class="editable content-block" contenteditable="false" data-selector="td.editable" style="color: rgb(55, 63, 74); font-size: 18px; text-transform: capitalize; border-left-width: 4px; border-left-style: double; border-left-color: rgb(55, 63, 74); padding-left: 10px; font-weight: bold; font-family: Raleway, sans-serif; outline: none; outline-offset: 2px;">CURRENT PROMOS</td>
            <td width="20"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
        </tr>
        <tr>
            <td height="20"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
        </tr>
        </tbody>
    </table>

    <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper-section-one" style="background:#ffffff;" width="600">
        <tbody>
        <tr>
            <td height="25"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
        </tr>
        </tbody>
    </table>
    <!--Section Fifth End here--><!--Section Sixth Start here-->

@endif

@foreach($promos as $promo)

    <!--Section Twelfth Start here-->

    <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper-section-one" style="background:#ffffff;" width="600">
        <tbody>
        <tr>
            <td width="20"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
            <td class="content-block" width="230">
                <table align="left" border="0" cellpadding="0" cellspacing="0">
                    <tbody>
                    <tr>
                        <td class="content-block">
                            <div class="editable content-block-img" style="display: block;"><a href="{{ $domain }}/promos"><img alt="{{ $promo->name }}" src="{{ $promo->image }}" style="display: inline-block;" width="217" /></a></div>
                        </td>
                        <td width="20"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
                    </tr>
                    </tbody>
                </table>
            </td>
            <td class="content-block" width="320">
                <table align="center" border="0" cellpadding="0" cellspacing="0">
                    <tbody>
                    <tr>
                        <td class="editable content-block" style="color: #000000;font-size: 21px;text-transform: capitalize;border-left: 4px double #000000;padding-left: 10px;font-weight:bold;font-family: 'Raleway', sans-serif;">{!!$promo->name !!}</td>
                    </tr>
                    <tr>
                        <td class="content-block" height="20"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
                    </tr>
                    <tr>
                        <td class="editable content-block" style="color: {{ $color[0] }}; font-family:'PT Sans', sans-serif;">
                            <div style="background: {{ $color[0] }}; color: white; padding: 15px; font-size: 16px; line-height: 1.2; border-radius: 4px;">
                                {!! $promo->description !!}
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td height="20"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
                    </tr>
                    <tr>
                        <td class="editable content-block" style="color: {{ $color[0] }};font-size: 14px;line-height: 1.5em; font-family:'PT Sans', sans-serif; text-align:right">Valid from {{ date('F jS', strtotime($promo->start_date)) }} to {{ date('F jS', strtotime($promo->end_date)) }}</td>
                    </tr>
                    <tr>
                        <td height="20"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
                    </tr>
                    </tbody>
                </table>
            </td>
            <td width="20"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
        </tr>
        <tr>
            <td height="45"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
        </tr>
        </tbody>
    </table>
    <!--Section Twelfth End here-->

@endforeach