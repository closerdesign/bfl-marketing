{{-- ARTICLES --}}

@if(count($articles) > 0)

    <!--Section Fourth End here--><!--Section Fifth Start here-->

    <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper-section-one" style="background:#f3f3f3;" width="600">
        <tbody>
        <tr>
            <td height="20"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
        </tr>
        <tr>
            <td width="20"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
            <td class="editable content-block" contenteditable="false" data-selector="td.editable" style="color: rgb(55, 63, 74); font-size: 18px; text-transform: capitalize; border-left-width: 4px; border-left-style: double; border-left-color: rgb(55, 63, 74); padding-left: 10px; font-weight: bold; font-family: Raleway, sans-serif; outline: none; outline-offset: 2px;">LATEST TIPS</td>
            <td width="20"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
        </tr>
        <tr>
            <td height="20"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
        </tr>
        </tbody>
    </table>

    <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper-section-one" style="background:#ffffff;" width="600">
        <tbody>
        <tr>
            <td height="25"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
        </tr>
        </tbody>
    </table>
    <!--Section Fifth End here--><!--Section Sixth Start here-->

@endif

@foreach($articles as $article)

    <!--Section Third Start here-->

    <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper-section-one" style="background: #ffffff;" width="600">
        <tbody>
        <tr>
            <td width="20"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
            <td class="content-block" width="245">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                    <tr>
                        <td class="editable" style="color: #000000;font-size: 18px;text-transform: capitalize;border-left: 4px double #000000;padding-left: 10px;font-weight:bold;font-family: 'Raleway', sans-serif;">{!! $article->title !!}</td>
                    </tr>
                    <tr>
                        <td height="15"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
                    </tr>
                    <tr>
                        <td class="editable" style="color:#636363; font-size:14px;font-family:'PT Sans', sans-serif;line-height: 1.5em;">{!! $article->intro_text !!}</td>
                    </tr>
                    <tr>
                        <td height="15"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
                    </tr>
                    <tr>
                        <td width="245">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tbody>
                                <tr>
                                    <td height="35" width="140"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
                                    <td class="button bg-editable" height="35" style="text-align:center;background:{{ $color[0] }};" width="220"><a class="btn-editable" href="{{ $domain }}/articles" style="color: #fff;display: inline-block;vertical-align: middle;line-height: 35px;text-decoration: none;text-align:center;font-size: 14px; text-transform: uppercase;font-family: 'PT Sans', sans-serif;">Read More </a></td>
                                    <td height="35" width="140"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
                                </tr>
                                <tr><td colspan="3">&nbsp;</td></tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
            <td class="content-block" width="20"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
            <td class="content-block" width="290">
                <div class="editable" style="display: block;"><a href="{{ $domain }}/articles"><img alt="" border="0" src="{{ $article->cover_img }}" style="display: block;" width="290" /></a></div>
            </td>
            <td width="20"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
        </tr>
        <tr>
            <td height="47"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
        </tr>
        </tbody>
    </table>
    <!--Section Third End here-->

@endforeach

{{-- END ARTICLES --}}