@extends('emails.layouts.general')

@section('content')

    <p>Hello {{ $vote->name }}!</p>

    <p>Thanks for voting for teacher of the month! We have received your vote for the following teacher / school:</p>

    <p>
        <b>Teacher: </b>{{ $vote->teacher }}<br />
        <b>School: </b>{{ $vote->school }}
    </p>

@endsection