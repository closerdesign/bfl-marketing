@extends('emails.layouts.general')

@section('content')

    <h3><b>Employee's Information</b></h3>
    <p><b>Name</b><br />{{ $request->name }}</p>
    <p><b>SSN</b><br />{{ $request->ssn }}</p>
    <p><b>Number of Hours Requested</b><br />{{ $request->hours_requested }}</p>
    <p><b>Hire Date</b><br />{{ $request->hire_date }}</p>
    <p><b>Adjusted Seniority Date</b><br />{{ $request->seniority_date }}</p>
    <h3><b>Additional Information</b></h3>
    <p><b>Request for Expedited Payment</b><br />{{ $request->expedited_payment }}</p>
    <p><b>Justification for Cashing All In</b><br />{{ $request->justification }}</p>
    <h3><b>Reviewer (Director)</b></h3>
    <p><b>Reviewer Name</b><br />{{ $request->reviewer }}</p>
    <p><b>Recommendation</b><br />{{ $request->recommendation }}</p>
    <p><b>Date</b><br />{{ $request->review_date }}</p>
    <h3><b>Eligibility (Payroll)</b></h3>
    <p><b>Number of Hours Eligible This Period</b><br />{{ $request->hours_eligible }}</p>
    <p><b>Number of Hours Used</b><br />{{ $request->hours_used }}</p>
    <p><b>Number of Hours VIL</b><br />{{ $request->hours_vil }}</p>
    <p><b>Number of Hours Eligible Currently</b><br />{{ $request->hours_eligible }}</p>
    <h3><b>Approver (GM or Operations Director)</b></h3>
    <p><b>Approver Name</b><br />{{ $request->approver }}</p>
    <p><b>Decision</b><br />{{ $request->decision }}</p>
    <p><b>Date</b><br />{{ $request->approve_date }}</p>
    <p><b>Store</b><br />@if($request->store == 0) Support Center @else {{ $request->store }} @endif</p>

@endsection