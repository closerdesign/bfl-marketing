@extends('emails.layouts.general')

@section('content')

    <h3><b>New Hire's Information</b></h3>
    <p><b>Name</b><br />{{ $request->name }}</p>
    <p><b>Nickname</b><br />{{ $request->nickname }}</p>
    <p><b>Orientation Date</b><br />{{ $request->orientation_date }}</p>
    <p><b>Orientation Time</b><br />{{ $request->orientation_time }}</p>
    <p><b>Employee Contact Phone #</b><br />{{ $request->phone }}</p>
    <p><b>Job Title</b><br />{{ $request->title }}</p>
    <p><b>Pay Rate</b><br />{{ $request->pay_rate }}</p>
    <p><b>Rehire?</b><br />{{ $request->rehire }}</p>
    <p><b>Drug Screen Date</b><br />{{ $request->drug_date }}</p>
    <p><b>Drug Screen Time</b><br />{{ $request->drug_time }}</p>
    <p><b>Work Status</b><br />{{ $request->status }}</p>
    <p><b>What Identification was reviewed?</b><br />{{ $request->identification }}</p>
    @if($request->id_details != '')<p><b>Identification Details</b><br />{{ $request->id_details }}</p>@endif
    <p><b>Language</b><br />{{ $request->language }}</p>
    @if($request->language_details != '')<p><b>Language Details</b><br />{{ $request->language_details }}</p>@endif
    <h3><b>Reporting Instructions</b></h3>
    <p><b>Report to Store Date</b><br />{{ $request->report_date }}</p>
    <p><b>Report to Store Time</b><br />{{ $request->report_time }}</p>
    <p><b>To Whom to Report at Store</b><br />{{ $request->report_to }}</p>
    <p><b>Store</b><br />@if($request->store == 0) Support Center @else {{ $request->store }} @endif</p>

@endsection