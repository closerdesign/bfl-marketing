@extends('emails.layouts.general')

@section('content')

    <h3><b>Candidate's Information</b></h3>
    <p><b>Name</b><br />{{ $request->name }}</p>
    <p><b>SSN</b><br />{{ $request->ssn }}</p>
    <p><b>DOB</b><br />{{ $request->dob }}</p>
    <p><b>DL #</b><br />{{ $request->dl_num }}</p>
    <p><b>DL State</b><br />{{ $request->dl_state }}</p>
    <p><b>Which version of the application did this person complete?</b><br />{{ $request->application }}</p>
    <p><b>What position(s) is the person being considered for?</b><br />{{ $request->position }}</p>
    @if($request->position_details != '')<p><b>Specify</b><br />{{ $request->position_details }}</p>@endif
    <p><b>What did the person reply to the question about previously working for BFL?</b><br />{{ $request->previous_emp }}</p>
    @if($request->previous_emp_details != '')<p><b>List Details</b><br />{{ $request->previous_emp_details }}</p>@endif
    <p><b>What did the person reply to the criminal history question?</b><br />{{ $request->criminal }}</p>
    @if($request->previous_emp_details != '')<p><b>List Details</b><br />{{ $request->criminal_details }}</p>@endif
    <h3><b>Additional Details from Application</b></h3>
    <p><b>Number of Years in OK</b><br />{{ $request->years_in_ok }}</p>
    <p><b>Number of Jobs in the Last 2 Years</b><br />{{ $request->jobs }}</p>
    <p><b>Other</b><br />{{ $request->app_details }}</p>
    <h3><b>Additional Details from Interview</b></h3>
    <p><b>Referred By</b><br />{{ $request->referred_by }}</p>
    <p><b>Attitude</b><br />{{ $request->attitude }}</p>
    <p><b>Other</b><br />{{ $request->interview_details }}</p>
    <p><b>Store</b><br />@if($request->store == 0) Support Center @else {{ $request->store }} @endif</p>

@endsection