@extends('emails.layouts.general')

@section('content')

        <table width="600" align="center">
            @if( count($comments) !== 0 )
            <tr>
                <th style="background-color: #ccc">Comments</th>
            </tr>
            <tr>
                <td><br />
                    @foreach($comments as $comment)
                        <p>{{ $comment->comment }}</p>
                        <p style="color: #777">-{{ $comment->name }} (<a href="mailto:{{$comment->email}}">{{ $comment->email }}</a>) &nbsp;&nbsp;{{ $comment->created_at->format('m/d/Y g:i A') }}</p><hr />
                    @endforeach
                </td>
            </tr>
            @endif
            <tr>
                <td>
                    <p style="text-align: center">
                        <a href="{{ URL::to('/huddles/view/' . $huddle->id) }}" style="background-color: #3498db; border: none; color: white; padding: 15px; text-align: center; text-decoration: none; font-size: 16px;">View Huddle / Add Comment</a>
                    </p><br />
                </td>
            </tr>
        </table>

@endsection