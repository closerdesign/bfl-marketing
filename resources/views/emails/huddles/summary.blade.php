@extends('emails.layouts.general')

@section('content')

    <h2 style="text-align: center">
        Huddles Report
    </h2>

    @foreach($huddles as $huddle)
        <table width="600" align="center">
            <tr>
                <th style="background-color: #ccc;">Store</th>
            </tr>
            <tr>
                <td style="text-align: center; background: #f5f5f5">
                    @if( $huddle->store ) <b>{{ $huddle->store->store_code  }} - {{ $huddle->store->name }}</b> <br /> <span style="font-size: 11px">Created at: {{ $huddle->created_at->format('m/d/Y  g:i A') }}</span>
                        <br /><span style="font-size:11px;">Led By: {{$huddle->leader}}</span>
                    @endif
                    @if( $huddle->store_id == 0 ) Support Center @endif
                </td>
            </tr>
            <tr>
                <th style="background-color: #ccc;">Details</th>
            </tr>
            <tr>
                <td>{!! $huddle->details !!}</td>
            </tr>
            @if( $huddle->ideas !== '' || $huddle->requests !== '' )
                <tr>
                    <th style="background-color: #ccc">Ideas / Requests</th>
                </tr>
                <tr>
                    <td>
                        @if( $huddle->ideas !== '' )
                            <b>Ideas:</b>
                            {!! $huddle->ideas !!}
                        @endif
                        @if( $huddle->requests !== '' )
                            <b>Requests:</b>
                            {!! $huddle->requests !!}
                        @endif
                    </td>
                </tr>
            @endif
        </table>
        <p style="text-align: center;">
            <a href="{{ URL::to('/huddles/view/' . $huddle->id) }}" style="background-color: #3498db; border: none; color: white; padding: 15px; text-align: center; text-decoration: none; font-size: 16px;">View Huddle / Add Comment</a>
        </p>
        <hr />
    @endforeach

@endsection