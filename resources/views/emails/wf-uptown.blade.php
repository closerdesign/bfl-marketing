@extends('emails.layouts.wf')

@section('content')

    <table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
        <tr>
            <td align="center" valign="top">
                <table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                    <tr>
                        <td align="center" valign="top">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" class="em_wrapper"
                                   style="table-layout: fixed;mso-table-lspace: 0px;mso-table-rspace: 0px; width: 600px;"
                                   width="600"><!--preheader section-->
                                <tbody>
                                <tr>
                                    <td align="center" valign="top">
                                        <table align="center" border="0" cellpadding="0" cellspacing="0"
                                               class="em_wrapper"
                                               style="table-layout: fixed;mso-table-lspace: 0px;mso-table-rspace: 0px; width: 600px;"
                                               width="600">
                                            <tbody>
                                            <tr>
                                                <td align="center" valign="top">
                                                    <table align="center" border="0" cellpadding="0" cellspacing="0"
                                                           class="em_wrapper" width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td align="center" valign="top">
                                                                <table border="0" cellpadding="0" cellspacing="0"
                                                                       style="width:600px;" width="600">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td height="8"
                                                                            style="font-size: 0px;line-height: 0px;">
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center" valign="top">
                                                                            <table border="0" cellpadding="0"
                                                                                   cellspacing="0" width="100%">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td class="em_side"
                                                                                        style="width:20px;" width="20">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                    <td align="center" valign="top">
                                                                                        <table align="left" border="0"
                                                                                               cellpadding="0"
                                                                                               cellspacing="0"
                                                                                               class="em_wrapper"
                                                                                               style="width: 420px;"
                                                                                               width="420">
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td align="left"
                                                                                                    class="em_font18"
                                                                                                    height="14"
                                                                                                    style="font-family: 'Lucida Sans', Arial, sans-serif;font-size: 9px;line-height: 12px;color: #000000;text-decoration: none;"
                                                                                                    valign="top">&nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="em_font0"
                                                                                                    height="10"
                                                                                                    style="font-size: 0px;line-height: 0px;">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>

                                                                                        <table align="right" border="0"
                                                                                               cellpadding="0"
                                                                                               cellspacing="0"
                                                                                               class="em_wrapper"
                                                                                               style="width: 120px;"
                                                                                               width="120">
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td align="right"
                                                                                                    class="header_mobhide"
                                                                                                    style="font-family: 'Lucida Sans', Arial, sans-serif;font-size: 9px;line-height: 12px;color: #000000;text-decoration: none;"
                                                                                                    valign="top">
                                                                                                    <webversion>View in
                                                                                                        a browser
                                                                                                    </webversion>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="em_font0"
                                                                                                    height="10"
                                                                                                    style="font-size: 0px;line-height: 0px;">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td class="em_side"
                                                                                        style="width:20px;" width="20">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <!--top nav-->
                                <tr>
                                    <td align="center" valign="top">
                                        <table align="center" border="0" cellpadding="0" cellspacing="0"
                                               class="em_wrapper"
                                               style="table-layout: fixed;mso-table-lspace: 0px;mso-table-rspace: 0px; width: 600px;"
                                               width="600">
                                            <tbody>
                                            <tr>
                                                <td align="center" valign="top">
                                                    <table align="center" border="0" cellpadding="0" cellspacing="0"
                                                           class="em_wrapper" width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td align="center" valign="top">
                                                                <table border="0" cellpadding="0" cellspacing="0"
                                                                       width="600">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <table align="center" border="0"
                                                                                   cellpadding="0" cellspacing="0"
                                                                                   width="100%">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td height="11"
                                                                                        style="font-size:1px; line-height:1px;"
                                                                                        valign="top">&nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="center" valign="top">
                                                                                        <table align="center" border="0"
                                                                                               cellpadding="0"
                                                                                               cellspacing="0"
                                                                                               width="100%">
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td align="center"
                                                                                                    valign="top"><a
                                                                                                            href="https://uptowngroceryco.com/"
                                                                                                            target="_blank"><img
                                                                                                                alt="UPTOWN GROCERY CO"
                                                                                                                src="http://sendy.buyforlessok.com/uploads/1513122790.png"
                                                                                                                style="display: block; width: 325px; border-width: 0px; border-style: solid;"/></a>
                                                                                                    <p>&nbsp;</p>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td valign="top">
                                                                                                    <table align="left"
                                                                                                           border="0"
                                                                                                           cellpadding="0"
                                                                                                           cellspacing="0"
                                                                                                           class="em_wrapper"
                                                                                                           width="100%">
                                                                                                        <tbody>
                                                                                                        <tr>
                                                                                                            <td class="header_mobhide"
                                                                                                                valign="top">
                                                                                                                <table align="center"
                                                                                                                       border="0"
                                                                                                                       cellpadding="0"
                                                                                                                       cellspacing="0"
                                                                                                                       width="100%">
                                                                                                                    <tbody>
                                                                                                                    <tr>
                                                                                                                        <td align="left"
                                                                                                                            class="em_blk2"
                                                                                                                            style="font-family:'Lucida Sans', Arial, sans-serif; font-size:14px; line-height:18px; color:#000000;"
                                                                                                                            valign="middle">
                                                                                                                            <a href="https://uptowngroceryco.com/locations"
                                                                                                                               style="text-decoration:underline; color:#000000;"
                                                                                                                               target="_blank">Find
                                                                                                                                A
                                                                                                                                Store</a>
                                                                                                                            <span class="em_hide">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                            <a href="https://uptowngroceryco.com/ads"
                                                                                                                               style="text-decoration:underline; color:#000000;"
                                                                                                                               target="_blank">Weekly
                                                                                                                                Ad</a>
                                                                                                                            <span class="em_hide">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                            <a href="https://shopping.uptowngroceryco.com"
                                                                                                                               style="text-decoration:underline; color:#000000;"
                                                                                                                               target="_blank">Shop
                                                                                                                                Online</a>
                                                                                                                        </td>
                                                                                                                        <td align="right">
                                                                                                                            <table border="0"
                                                                                                                                   cellpadding="0"
                                                                                                                                   cellspacing="0"
                                                                                                                                   class="em_wrapper"
                                                                                                                                   width="100%">
                                                                                                                                <tbody>
                                                                                                                                <tr>
                                                                                                                                    <td height="12"
                                                                                                                                        style="font-size:1px; line-height:1px;">
                                                                                                                                        &nbsp;
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                </tbody>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    </tbody>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td height="11"
                                                                                        style="font-size:1px; line-height:1px;"
                                                                                        valign="top">&nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <!--/top nav--><!--hero-->
                                <tr>
                                    <td align="center" valign="top">
                                        <table align="center" border="0" cellpadding="0" cellspacing="0"
                                               class="em_wrapper"
                                               style="table-layout: fixed;mso-table-lspace: 0px;mso-table-rspace: 0px; width: 600px;"
                                               width="600">
                                            <tbody>
                                            <tr>
                                                <td align="center" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td align="center" valign="top"><a
                                                                        href="https://uptowngroceryco.com/ads"
                                                                        style="display: block; border:none;"
                                                                        target="_blank"><img alt=""
                                                                                             src="http://placehold.it/600x400"
                                                                                             style="width: 600px; height: 400px;"/></a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td bgcolor="#f5f5f5" class="em_h20" height="40">&nbsp;</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <!--/hero--><!--testing-->
                                <tr>
                                    <td align="center" valign="top">
                                        <table align="center" border="0" cellpadding="0" cellspacing="0"
                                               class="em_wrapper"
                                               style="table-layout: fixed;mso-table-lspace: 0px;mso-table-rspace: 0px; width: 600px;"
                                               width="600">
                                            <tbody>
                                            <tr>
                                                <td align="center" valign="top">
                                                    <table align="center" border="0" cellpadding="0" cellspacing="0"
                                                           class="em_wrapper" width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td align="center" valign="top">
                                                                <table border="0" cellpadding="0" cellspacing="0"
                                                                       style="width: 600px;" width="600"><!--/testing-->
                                                                    <!--sales section--><!--first column-->
                                                                    <tbody>
                                                                    <tr>
                                                                        <td align="center" bgcolor="#f5f5f5"
                                                                            valign="top">
                                                                            <table border="0" cellpadding="0"
                                                                                   cellspacing="0" width="100%">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td class="em_side"
                                                                                        style="width:20px;" width="20">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                    <td align="center" valign="top">
                                                                                        <table border="0"
                                                                                               cellpadding="0"
                                                                                               cellspacing="0"
                                                                                               width="100%">
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td align="center"
                                                                                                    valign="top">
                                                                                                    <!--sale 1-->
                                                                                                    @foreach($specials as $special)
                                                                                                        <table align="left"
                                                                                                               border="0"
                                                                                                               cellpadding="0"
                                                                                                               cellspacing="0"
                                                                                                               class="em_wrapper"
                                                                                                               style="padding-left: 17px; width:255px;"
                                                                                                               width="255">
                                                                                                            <tbody>
                                                                                                            <tr>
                                                                                                                <td align="center"
                                                                                                                    valign="top">
                                                                                                                    <table align="center"
                                                                                                                           bgcolor="#ffffff"
                                                                                                                           border="0"
                                                                                                                           cellpadding="0"
                                                                                                                           cellspacing="0"
                                                                                                                           style="width:255px;"
                                                                                                                           width="255">
                                                                                                                        <tbody>
                                                                                                                        <tr>
                                                                                                                            <td style="width:20px;"
                                                                                                                                width="20">
                                                                                                                                &nbsp;
                                                                                                                            </td>
                                                                                                                            <td align="center"
                                                                                                                                valign="top">
                                                                                                                                <table border="0"
                                                                                                                                       cellpadding="0"
                                                                                                                                       cellspacing="0"
                                                                                                                                       width="100%">
                                                                                                                                    <tbody>
                                                                                                                                    <tr>
                                                                                                                                        <td height="30">
                                                                                                                                            &nbsp;
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                        <td align="center"
                                                                                                                                            valign="top">
                                                                                                                                            <a href="https://uptowngroceryco.com/ads"
                                                                                                                                               style="display:block; border:none;"
                                                                                                                                               target="_blank"><img
                                                                                                                                                        alt=""
                                                                                                                                                        src="{{ $special->image }}"
                                                                                                                                                        style="width: 196px; height: 196px;"/></a>
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                        <td height="27">
                                                                                                                                            &nbsp;
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                        <td align="left"
                                                                                                                                            class="em_grey18"
                                                                                                                                            height="50"
                                                                                                                                            style="font-family: 'Lucida Sans', Arial, sans-serif;font-size: 18px;line-height: 23px;color: #565553;"
                                                                                                                                            valign="bottom">
                                                                                                                                            <p>{{ $special->item_description }}</p>
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                        <td class="em_font0"
                                                                                                                                            height="5"
                                                                                                                                            style="font-size: 0px;line-height: 0px;">
                                                                                                                                            &nbsp;
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                        <td align="left"
                                                                                                                                            class="em_grey16"
                                                                                                                                            height="25"
                                                                                                                                            style="font-family: 'Lucida Sans', Arial, sans-serif;font-size: 16px;line-height: 18px;color: #565553;font-weight: 600;"
                                                                                                                                            valign="top">
                                                                                                                                            <a href="https://uptowngroceryco.com/ads"
                                                                                                                                               style="text-decoration: none; color: #c31734;">{{ $special->price }} {{ $special->size }}</a>
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                        <td class="em_font0"
                                                                                                                                            height="5"
                                                                                                                                            style="font-size: 0px;line-height: 0px;">
                                                                                                                                            &nbsp;
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                        <td align="left"
                                                                                                                                            class="em_grey10"
                                                                                                                                            height="100"
                                                                                                                                            style="font-family: 'Lucida Sans', Arial, sans-serif;font-size: 10px;line-height: 16px;color: #565553;font-weight: 600;text-decoration: none;"
                                                                                                                                            valign="top">
                                                                                                                                            A
                                                                                                                                            aperiam
                                                                                                                                            assumenda
                                                                                                                                            autem,
                                                                                                                                            beatae
                                                                                                                                            commodi
                                                                                                                                            consequatur
                                                                                                                                            corporis
                                                                                                                                            dolor
                                                                                                                                            earum
                                                                                                                                            esse
                                                                                                                                            eum
                                                                                                                                            id
                                                                                                                                            laudantium
                                                                                                                                            molestiae
                                                                                                                                            mollitia,
                                                                                                                                            officiis
                                                                                                                                            reprehenderit
                                                                                                                                            rerum
                                                                                                                                            vero
                                                                                                                                            voluptatem
                                                                                                                                            voluptatibus.
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                        <td class="em_font0"
                                                                                                                                            height="20"
                                                                                                                                            style="font-size: 0px;line-height: 0px;">
                                                                                                                                            &nbsp;
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                    </tbody>
                                                                                                                                </table>
                                                                                                                            </td>
                                                                                                                            <td style="width:20px;"
                                                                                                                                width="20">
                                                                                                                                &nbsp;
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="em_h20"
                                                                                                                    height="40">
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                    @endforeach
                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td class="em_side"
                                                                                        style="width:20px;" width="20">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <!--/testing--><!--cta-->
                                <tr>
                                    <td align="center" bgcolor="#f5f5f5" valign="top">
                                        <table align="center" bgcolor="#f5f5f5" border="0" cellpadding="0"
                                               cellspacing="0" class="em_wrapper"
                                               style="table-layout: fixed;mso-table-lspace: 0px;mso-table-rspace: 0px; width: 600px;"
                                               width="600">
                                            <tbody>
                                            <tr>
                                                <td align="center" valign="top">
                                                    <table align="center" border="0" cellpadding="0" cellspacing="0"
                                                           class="em_wrapper" width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td align="center" valign="top">
                                                                <table border="0" cellpadding="0" cellspacing="0"
                                                                       style="width:600px;" width="600">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td align="center" valign="top">
                                                                            <table border="0" cellpadding="0"
                                                                                   cellspacing="0" width="100%">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td align="center" bgcolor="#f5f5f5"
                                                                                        class="em_grey10"
                                                                                        style="padding-left:10px; padding-right: 10px; font-family: 'Lucida Sans', Arial, sans-serif;font-size: 10px;line-height: 16px;color: #565553;font-weight: 600;text-decoration: none;"
                                                                                        valign="top">
                                                                                        Valid {{ date('F jS', strtotime($ad->date_from)) }}
                                                                                        - {{ date('F jS', strtotime($ad->date_to)) }}
                                                                                        . While supplies last. Selection
                                                                                        varies.
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td bgcolor="#f5f5f5" class="em_h20"
                                                                                        height="25">&nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="center" bgcolor="#f5f5f5"
                                                                                        valign="top">
                                                                                        <table align="center"
                                                                                               bgcolor="#000000"
                                                                                               border="0"
                                                                                               cellpadding="0"
                                                                                               cellspacing="0"
                                                                                               style="width:200px;"
                                                                                               width="200">
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td align="center"
                                                                                                    class="em_white10"
                                                                                                    height="40"
                                                                                                    style="font-family: 'Lucida Sans', Arial, sans-serif;font-size: 13px;color: #ffffff;font-weight: 600;"
                                                                                                    valign="middle"><a
                                                                                                            href="https://uptowngroceryco.com/ads"
                                                                                                            style="text-decoration: none;color: #ffffff;display: block;line-height: 40px;"
                                                                                                            target="_blank">SEE
                                                                                                        MORE SALES</a>
                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td bgcolor="#f5f5f5" height="30">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <!--/cta--><!--featured video-->
                                <tr>
                                    <td align="center" valign="top">
                                        <table align="center" border="0" cellpadding="0" cellspacing="0"
                                               class="em_wrapper"
                                               style="table-layout: fixed;mso-table-lspace: 0px;mso-table-rspace: 0px; width: 600px;"
                                               width="600">
                                            <tbody>
                                            <tr>
                                                <td align="center" valign="top">
                                                    <table align="center" border="0" cellpadding="0" cellspacing="0"
                                                           class="em_wrapper" width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td align="center" valign="top">
                                                                <table border="0" cellpadding="0" cellspacing="0"
                                                                       width="600">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td align="center" valign="top"><a
                                                                                    href="https://uptowngroceryco.com/ads"
                                                                                    target="_blank"><img alt=""
                                                                                                         src="http://placehold.it/600x400"
                                                                                                         style="display: block; border: 0px; outline: none; width: 600px; height: 400px;"/></a>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="em_h20" height="20">&nbsp;</td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <!--/featured video--><!--events section-->
                                <tr>
                                    <td valign="top" class="events">
                                        <h1 style="text-align: center; width: 600px;">Upcoming Events</h1>
                                        <hr style="width: 600px"/>
                                        @foreach($events as $event)
                                            <table align="center" border="0" cellpadding="0" cellspacing="0"
                                                   style="padding-bottom: 25px; padding-left: 15px; padding-right: 15px;"
                                                   width="600">
                                                <tr>
                                                    <td width="225">
                                                        <a href="https://uptowngroceryco.com/events">
                                                            <img src="{{ $event->image }}" alt="{{ $event->name }}"
                                                                 width="200">
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <h2>
                                                            <span>{!! $event->name !!}</span><br/>
                                                        </h2>
                                                        <h3>
                                                            {{ date('F jS, Y', strtotime($event->date)) }} @if($event->end_date != null)
                                                                to {{ date('F jS, Y', strtotime($event->end_date)) }} @endif
                                                            <br/>
                                                            @if($event->location){{$event->location}} | @endif
                                                            @if($event->time == '00:00:00' && $event->end_time != null && $event->end_time == '23:30:00')
                                                                All Day
                                                            @else
                                                                {{ date('h:i a', strtotime($event->time)) }} @if( $event->end_time != null )
                                                                    to {{ date('h:i a', strtotime($event->end_time)) }} @endif
                                                            @endif
                                                        </h3>
                                                        <p><b>{{ $event->intro }}</b></p>
                                                        <hr>
                                                        {!! $event->description !!}
                                                    </td>
                                                </tr>
                                            </table>
                                        @endforeach
                                    </td>
                                </tr>
                                <!--/events section--><!--loyalty app-->
                                <tr>
                                    <td>
                                        <table width="600" border="0" cellspacing="30" cellpadding="0"
                                               style="background-color: #f5f5f5; border-collapse: separate;">
                                            <tr>
                                                <td>
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                <a href="https://loyalty.uptowngroceryco.com"
                                                                   style="text-decoration: none;">
                                                                    <img src="http://sendy.buyforlessok.com/uploads/1553716595.png"
                                                                         style="margin: 0 20px 0 0; border: none; max-width: 99px; width: 100%; min-width: 50px"/>
                                                                </a>
                                                            </td>
                                                            <td width="20" style="width: 20px;">
                                                                <p style="width: 20px; min-width: 20px;">
                                                                    &nbsp;
                                                                </p>
                                                            </td>
                                                            <td style="padding-left: 20px;">
                                                                <p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 24px; color: #464646; margin: 10px 0px 0px 0; padding: 0px;">
                                                                    The ease of being rewarded! Our new rewards program
                                                                    gives you inside access to exclusive deals and
                                                                    coupons.
                                                                </p><br/>
                                                                <p>
                                                                    <a href="https://itunes.apple.com/us/app/uptown-grocery/id1454400948?mt=8&utm_source=Email%20Marketing"
                                                                       style="text-decoration: none;">
                                                                        <img src="https://www.uptowngroceryco.com/img/appstore.png"
                                                                             width="104" height="34"
                                                                             style="border: none; padding-right: 20px;"
                                                                             alt=""/>
                                                                    </a>
                                                                    <a href="https://play.google.com/store/apps/details?id=com.uptowngroceryco&utm_source=Email%20Marketing"
                                                                       style="text-decoration: none;">
                                                                        <img src="https://www.uptowngroceryco.com/img/googleplay.png"
                                                                             width="104" height="34"
                                                                             style="border: none" alt=""/>
                                                                    </a>
                                                                </p>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <!-- /loyalty app section--><!--footer -->
                                <tr>
                                    <td align="center" valign="top">
                                        <table align="center" border="0" cellpadding="0" cellspacing="0"
                                               class="em_wrapper"
                                               style="table-layout: fixed;mso-table-lspace: 0px;mso-table-rspace: 0px; width: 600px;"
                                               width="600">
                                            <tbody>
                                            <tr>
                                                <td align="center" valign="top">
                                                    <table align="center" border="0" cellpadding="0" cellspacing="0"
                                                           class="em_wrapper" width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td align="center" valign="top">
                                                                <table border="0" cellpadding="0" cellspacing="0"
                                                                       width="600"><!--footer-->
                                                                    <tbody>
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <table align="center" border="0"
                                                                                   cellpadding="0" cellspacing="0"
                                                                                   style="width: 100%;">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td class="em_height"
                                                                                        style="text-align: center">
                                                                                        <img src="https://sara.buyforlessok.com/img/coinredemption-uptown.jpeg"
                                                                                             width="600"/>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="em_height" height="10">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="center" class="em_wht"
                                                                                        style="font-family: 'Lucida Sans', Arial, sans-serif; font-size: 13px; line-height: 17px; color: #000000; font-weight: bold;">
                                                                                        <p>Stay in the loop, connect
                                                                                            with us on:</p>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td height="15"
                                                                                        style="line-height: 1px; font-size: 1px;">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="center" valign="top">
                                                                                        <table align="center" border="0"
                                                                                               cellpadding="0"
                                                                                               cellspacing="0"
                                                                                               style="width: 250px;">
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td align="left"
                                                                                                    valign="top">&nbsp;
                                                                                                </td>
                                                                                                <td width="15">&nbsp;
                                                                                                </td>
                                                                                                <td align="center"
                                                                                                    valign="top">&nbsp;
                                                                                                </td>
                                                                                                <td width="15">&nbsp;
                                                                                                </td>
                                                                                                <td align="right"
                                                                                                    valign="top"><a
                                                                                                            href="https://www.instagram.com/uptowngroceryco/"
                                                                                                            target="_blank"><img
                                                                                                                alt="Instagram"
                                                                                                                src="http://sendy.buyforlessok.com/uploads/1513112758.png"
                                                                                                                style="display: block; border-width: 0px; border-style: solid; width: 32px; height: 32px;"/></a>
                                                                                                </td>
                                                                                                <td width="15">&nbsp;
                                                                                                </td>
                                                                                                <td align="left"
                                                                                                    valign="top"><a
                                                                                                            href="https://www.facebook.com/UptownGroceryCo/"
                                                                                                            target="_blank"><img
                                                                                                                alt="Facebook"
                                                                                                                src="http://sendy.buyforlessok.com/uploads/1513112765.png"
                                                                                                                style="display: block; border-width: 0px; border-style: solid; width: 32px; height: 32px;"/></a>
                                                                                                </td>
                                                                                                <td width="15">&nbsp;
                                                                                                </td>
                                                                                                <td align="center"
                                                                                                    valign="top">&nbsp;
                                                                                                </td>
                                                                                                <td width="15">&nbsp;
                                                                                                </td>
                                                                                                <td align="right"
                                                                                                    valign="top">&nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="em_height" height="42">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="center" class="em_gray_u"
                                                                                        style="font-family:'Lucida Sans', Arial, sans-serif; font-size:12px; line-height:14px; color:#767676;">
                                                                                        <p>
                                                                                            <unsubscribe>Unsubscribe
                                                                                            </unsubscribe>
                                                                                        </p>

                                                                                        <p>&nbsp;</p>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="center" class="em_gray_u"
                                                                                        style="font-family:'Lucida Sans', Arial, sans-serif; font-size:12px; line-height:14px; color:#767676;">
                                                                                        <a href="https://uptowngroceryco.com/deals"
                                                                                           style="text-decoration:underline; color:#C31734;"
                                                                                           target="_blank">Promotions</a>
                                                                                        &nbsp;|&nbsp;
                                                                                        <a href="https://uptowngroceryco.com/recipes"
                                                                                           style="text-decoration:underline; color:#C31734;"
                                                                                           target="_blank">Featured
                                                                                            Recipes</a> <br/>
                                                                                        <a href="https://www.shipt.com/uptown-grocery-co-delivery/"
                                                                                           style="text-decoration:underline; color:#C31734;"
                                                                                           target="_blank">Shipt</a>
                                                                                        &nbsp;|&nbsp;
                                                                                        <a href="https://www.ubereats.com/en-US/oklahoma-city/food-delivery/uptown-grocery-co-edmond/fazdQLZFRiealDHvDOW_eA/"
                                                                                           style="text-decoration:underline; color:#C31734;"
                                                                                           target="_blank">Uber Eats -
                                                                                            Edmond</a> &nbsp;|&nbsp;
                                                                                        <a href="https://www.ubereats.com/en-US/oklahoma-city/food-delivery/uptown-grocery-co-the-village/oaGAH5eqQI6qYzlZOWjHcw/"
                                                                                           style="text-decoration:underline; color:#C31734;"
                                                                                           target="_blank">Uber Eats -
                                                                                            Village</a>
                                                                                        {{--<a href="https://postmates.com/merchant/uptown-grocery-co-edmond" style="text-decoration:underline; color:#C31734;" target="_blank">Postmates - Edmond</a> &nbsp;|&nbsp;--}}
                                                                                        {{--<a href="https://postmates.com/merchant/uptown-grocery-co-wok-oklahoma-city" style="text-decoration:underline; color:#C31734;" target="_blank">Postmates - Village</a>--}}
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="em_wht"
                                                                                        style="font-family: 'Lucida Sans', Arial, sans-serif; font-size: 10px; line-height: 15px; color: #767676;">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="em_height" height="25">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="center" class="em_gray"
                                                                                        style="font-family: 'Lucida Sans', Arial, sans-serif; font-size: 12px; line-height: 14px; color: #767676;">
                                                                                        &copy; {{ date('Y') }} Uptown
                                                                                        Grocery Co.
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="em_height" height="43">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <!--/footer-->
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <!--/footer-->
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>

@endsection