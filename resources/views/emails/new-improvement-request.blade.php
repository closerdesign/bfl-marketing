@extends('emails.layouts.general')

@section('content')

    <p><b>{{ $improvement_request->title }}</b></p>

    <p><b>Description:</b></p>

    {!! $improvement_request->description !!}

    @if( $improvement_request->file != "" )
    <p>
        Attached File:
        <a href="{{ \Illuminate\Support\Facades\Storage::url('improvement-requests/' . $improvement_request->file) }}">
            {{ $improvement_request->file }}
        </a>
    </p>
    @endif

    @endsection