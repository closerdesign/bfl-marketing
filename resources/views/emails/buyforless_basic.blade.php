@extends('emails.layouts.buyforless')

@section('content')

    {{--Current Ad--}}

    <!--Section Third Start here-->

    <table align="center" border="0" cellpadding="0" cellspacing="0" class="wrapper-section-one" width="600">
        <tbody>
            <tr>
                <td colspan="3">
                    <a href="http://www.buyforlessok.com/ads">
                        <img src="http://placehold.it/600x350" class="img-responsive" style="margin: auto;"/>
                    </a>
                </td>
            </tr>
            <tr><td colspan="3">&nbsp;</td></tr>
            <tr>
                <td width="20"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
                <td class="content-block" height="180" width="245">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tbody>
                        <tr>
                            <td class="editable" style="color: #000000;font-size: 18px;text-transform: capitalize;border-left: 4px double #000000;padding-left: 10px;font-weight:bold;font-family: 'Raleway', sans-serif;">Who we are</td>
                        </tr>
                        <tr>
                            <td height="15"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
                        </tr>
                        <tr>
                            <td class="editable" style="padding: 5px; color:#636363; font-size:14px;font-family:'PT Sans', sans-serif;line-height: 1.5em;">Lorem ipsum dolor sit amet, elitr latine aperi euismod omnesque et sea, laudem option m sed Eu movet.</td>
                        </tr>
                        <tr>
                            <td height="15"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
                        </tr>
                        <tr>
                            <td width="245">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody>
                                    <tr>
                                        <td height="35" width="140"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
                                        <td class="button bg-editable" height="35" style="text-align:center;background:{{ $color[0] }};" width="220"><a class="btn-editable" href="http://buyforlessok.com/ads" style="color: #fff;display: inline-block;vertical-align: middle;line-height: 35px;text-decoration: none;text-align:center;font-size: 14px; text-transform: uppercase;font-family: 'PT Sans', sans-serif;">Download The Ad </a></td>
                                        <td height="35" width="140"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
                <td width="20"><img alt="" height="1" src="http://www.buyforlessok.com/email/images/blank.gif" width="1" /></td>
            </tr>
        </tbody>
    </table>
    <!--Section Third End here-->

    @include('emails.partials.promos')

    @include('emails.partials.events')

    @include('emails.partials.articles')

    @endsection