@extends('emails.layouts.general')

@section('content')

    <h1>Welcome to SARA Platform <br /><b>{{ $user->name }}</b>!</h1>

    <p>Congratulations! Now you can join us in the platform using the password below:</p>

    <p>{{ $password }}</p>

    <p>Hurry! Go to <a href="{{ \Illuminate\Support\Facades\URL::to('') }}">{{ \Illuminate\Support\Facades\URL::to('') }}</a> and join the party!</p>

    <p>Cheers!</p>

    <p><b>BFL Media Team</b></p>

    @endsection