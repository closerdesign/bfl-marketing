@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Volunteers
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-condensed">
                        <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Phone Number</th>
                            <th>Email</th>
                            <th>Birth Date</th>
                            <th>Organization</th>
                            <th>Referral</th>
                            <th>Referral (Others)</th>
                            <th>Training Session</th>
                            <th>Availability</th>
                            <th>Created At</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($volunteers as $volunteer)
                        <tr>
                            <td>{{ $volunteer->first_name }}</td>
                            <td>{{ $volunteer->last_name }}</td>
                            <td>{{ $volunteer->phone_number }}</td>
                            <td>{{ $volunteer->email }}</td>
                            <td>{{ $volunteer->birth_date }}</td>
                            <td>{{ $volunteer->organization }}</td>
                            <td>{{ $volunteer->referral }}</td>
                            <td>{{ $volunteer->referral_other }}</td>
                            <td>{{ $volunteer->training_session }}</td>
                            <td>{{ $volunteer->availability }}</td>
                            <td>{{ $volunteer->created_at }}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $volunteers->render() }}
                </div>
            </div>
        </div>
    </div>

    @endsection