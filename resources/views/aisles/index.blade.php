@extends('layouts.app')

@section('content')

    <div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">Add Aisle to Store {{$store->name}}</div>
                <div class="panel-body">
        <form action="/store-aisles-add" method="post">
            {{csrf_field()}}
            <div class="form-group">
                <label for="aisle">Aisle</label>
                <input class="form-control" name="aisle_name" id="aisle">
                <input type="hidden" name="store_id" value="{{$store->id}}">
            </div>
            <div>
                <button class="btn btn-success">Save</button>
            </div>
        </form>

    </div>


            </div>
        </div>

        <!-- -->
        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">Aisles In Store {{$store->name}}</div>
                <div class="panel-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Aisle Name</th>
                            <th >Edit</th>
                            <th>Delete</th>
                            <th>Panels</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($store->aisles as $aisle)
                                <tr>
                                <td>{{$aisle->aisle_name}}</td>
                                    <td><a class="btn btn-sm btn-primary" href="{{action('StoreAisleController@edit', $aisle->id)}}"><i class="fa fa-edit"></i></a></td>
                                    <td>
                                        <form method="post" onsubmit="return confirm('Are you sure? This can\'t be undone.')" action="{{ action('StoreAisleController@destroy', $aisle->id) }}">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button class="btn btn-danger" type="submit"><i class="fa fa-trash "></i></button>
                                        </form>
                                    </td>
                                <td ><a href="{{action('StoreAislePanelsController@index', $aisle->id)}}" class="btn btn-sm btn-primary"><i class="fa fa-sign-in"></i></a></td>
                                </tr>
                             @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <div class="col-md-4">
            <a class="btn btn-primary" href="{{ action('StoresController@edit', $aisle->store_id) }}">Store</a>
        </div>



    </div>

    </div>

@endsection

