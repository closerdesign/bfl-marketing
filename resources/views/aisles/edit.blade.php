@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">Add Aisle to Store {{$aisle->store->name}}</div>
                    <div class="panel-body">
                        <form action="{{ action('StoreAisleController@update_aisle', $aisle->id)}}" method="post">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="aisle">Aisle</label>
                                <input class="form-control" name="aisle_name" id="aisle" value="{{@$aisle->aisle_name}}">
                            </div>
                            <div>
                                <button class="btn btn-success">Save</button>
                            </div>
                        </form>

                    </div>


                </div>
            </div>
        </div>
    </div>
@endsection