@extends('layouts.app')

@section('content')
    
    <p class="lead text-center">
        Promo Redemptions
    </p>
    
    <div class="container">
        
        @if( $promos === null )

        <form action="">

            <div class="form-group">
                <label for="promo">Promo Number</label>
                <input type="number" class="form-control" name="promo" required >
            </div>

            <div class="form-group">
                <label for="start">Start Date</label>
                <input type="date" class="form-control" name="start" required >
            </div>

            <div class="form-group">
                <label for="end">End</label>
                <input type="date" class="form-control" name="end" required >
            </div>

            <button class="btn btn-success">
                Search
            </button>

        </form>
        
        @else

        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <td>Location</td>
                    <td>Promotion Number</td>
                    <td>Description</td>
                    <td>Promotion Qty Given</td>
                    <td>Promotion Amount Given</td>
                    <td>Promo Start Date</td>
                    <td>Promo End Date</td>
                </tr>
                @foreach($stores as $store)
                <tr>
                    <td class="text-center">{{ $store->store }}</td>
                    <td class="text-center">{{ $store->mmbr_prom_id }}</td>
                    <td>{{ $store->prom_desc }}</td>
                    <td class="text-center">{{ $promos->where('store', $store->store)->sum('prom_qty') }}</td>
                    <td class="text-right">{{ $promos->where('store', $store->store)->sum('prom_amt') }}</td>
                    <td class="text-center">{{ $store->strt_date }}</td>
                    <td class="text-center">{{ $store->end_date }}</td>
                </tr>
                @endforeach
            </table>

            <p class="text-center text-muted">Last update: {{ date('Y-m-d', strtotime('-1 days')) }}</p>

            <a href="{{ action('PromoMovementController@index') }}" class="btn btn-default">Back</a>
        </div>
        
        @endif
        
    </div>
    
    @endsection