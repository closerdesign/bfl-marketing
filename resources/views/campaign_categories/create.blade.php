@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-primary">
                    <div class="panel-heading">Create Campaign Category</div>
                    <div class="panel-body">
                        <form action="{{ action('CampaignCategoriesController@store') }}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}" required />
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea name="description" id="description" cols="30" rows="10"
                                          class="form-control summernote">{{ old('description') }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="background_image">Image (500 x 500)</label>
                                <input type="file" class="form-control" name="image" />
                            </div>
                            <div class="form-group">
                                <label for="campaign_id">Campaign</label>
                                <select name="campaign_id[]" id="campaign_id"
                                        class="form-control multiple-select" multiple="multiple">
                                    <option value="">Select...</option>
                                    @foreach(\App\Campaign::all() as $campaign)
                                        <option value="{{ $campaign->id }}">{{ $campaign->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="status">Status</label>
                                <select name="status" id="status" class="form-control" required >
                                    <option value="">Select...</option>
                                    <option @if( old('status') == 1 ) selected @endif value="1">Active</option>
                                    <option @if( old('status') == 0 ) selected @endif value="0">Inactive</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <p><input type="checkbox" name="is_sides_category" value="1" @if(old('is_sides_category') == 1) checked @endif > Use Sides Layout</p>
                            </div>
                            <button class="btn btn-success btn-block" type="submit">Save changes</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection