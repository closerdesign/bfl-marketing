@extends('layouts.app')

@section('content')

    <div class="container">

        <div>

            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#basic-information" aria-controls="basic-information" role="tab" data-toggle="tab">Basic Information</a></li>
                <li role="presentation"><a href="#items" aria-controls="items" role="tab" data-toggle="tab">Items</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="basic-information">

                    <div class="panel panel-primary">
                        <div class="panel-heading">Edit Category: {{ $category->name }}</div>
                        <div class="panel-body">
                            <form action="{{ action('CampaignCategoriesController@update', $category->id) }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                {{ method_field('PATCH') }}
                                <div class="form-group">
                                    <img width="200" src="{{ $category->image }}" alt="{{ $category->name }}" class="img-responsive pull-right">
                                </div>
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" name="name" value="{{ $category->name }}" required />
                                </div>
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea name="description" id="description" cols="30" rows="10"
                                              class="form-control summernote">{{ $category->description }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="background_image">Image (500 x 500)</label>
                                    <input type="file" class="form-control" name="image" />
                                </div>
                                <div class="form-group">
                                    <label for="campaign_id">Campaign</label>
                                    <select name="campaign_id[]" id="campaign_id"
                                            class="form-control multiple-select" multiple="multiple">
                                        <option value="">Select...</option>
                                        @foreach(App\Campaign::all() as $campaign)
                                            <option @if( in_array($campaign->id, $category->campaigns()->select('campaign_id')->pluck('campaign_id')->toArray()) ) selected @endif value="{{ $campaign->id }}" >{{ $campaign->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="status">Status</label>
                                    <select name="status" id="status" class="form-control" required >
                                        <option value="">Select...</option>
                                        <option @if( $category->status == 'Active' ) selected @endif value="1">Active</option>
                                        <option @if( $category->status == 'Inactive' ) selected @endif value="0">Inactive</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="template">@lang('campaign-categories.template')</label>
                                    <select name="template" id="template" class="form-control">
                                        <option value="">Default</option>
                                        <option value="sides">Sides</option>
                                        <option value="classes">Classes</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <p><input type="checkbox" name="is_sides_category" value="1" @if($category->is_sides_category == 1) checked @endif > Use Sides Layout</p>
                                </div>
                                <button class="btn btn-primary" type="submit">Save changes</button>
                            </form>
                        </div>
                    </div>

                </div>
                <div role="tabpanel" class="tab-pane" id="items">

                    <div class="panel panel-primary">
                        <div class="panel-heading">Items</div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-condensed">
                                    <thead>
                                    <tr>
                                        <th>PLU</th>
                                        <th>Name</th>
                                        <th>Price</th>
                                        <th>Status</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($category->all_items as $item)
                                        <tr>
                                            <td>{{ $item->plu }}</td>
                                            <th>
                                                <a href="{{ action('CampaignItemsController@edit', $item->id) }}">
                                                    {{ $item->name }}
                                                </a>
                                            </th>
                                            <td class="text-right">{{ $item->price }}</td>
                                            <td class="text-center">{{ $item->status }}</td>
                                            <td>
                                                <form action="{{ action('CampaignItemsController@destroy', $item->id) }}" method="post" onsubmit="return confirm('Are you sure? This can\'t be undone')">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                    <button class="btn-xs btn-danger"><i class="fa fa-trash"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    @endsection