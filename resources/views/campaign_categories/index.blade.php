@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <p class="text-right"><a href="{{ action('CampaignCategoriesController@create') }}" class="btn btn-success"><i class="fa fa-plus-circle"></i> Create Category</a></p>
        <div class="panel panel-default">
            <div class="panel-heading">Campaign Categories</div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-condensed">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Campaign</th>
                            <th>Status</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody id="sortable">
                        @foreach($categories as $category)
                            <tr id="item-{{ $category->id }}">
                                <th>
                                    <a href="{{ action('CampaignCategoriesController@edit', $category->id) }}">
                                        {{ $category->name }}
                                    </a>
                                </th>
                                <td>
                                    @foreach($category->campaigns as $campaign)
                                        [{{ $campaign->name }}]
                                    @endforeach
                                </td>
                                <td>{{ $category->status }}</td>
                                <td>
                                    <form onsubmit="return confirm('Are you sure? This can\'t be undone.')" action="{{ action('CampaignCategoriesController@destroy', $category->id) }}" method="post">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button class="btn-xs btn-danger" type="submit"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @endsection

@section('js')

    <script>
        $( function() {
            $( "#sortable" ).sortable({
                axis: 'y',
                update: function (event, ui) {

                    var data = $(this).sortable('serialize');

                    // console.log(data);

                    // POST to server using $.post or $.ajax
                    $.ajax({
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "data": data
                        },
                        type: 'POST',
                        url: '{{ action('CampaignCategoriesController@sort') }}',
                        success: function (result) {
                            console.log(result);
                        },
                        complete: function () {

                        }
                    });

                }
            });
            $( "#sortable" ).disableSelection();
        } );
    </script>

    @endsection
