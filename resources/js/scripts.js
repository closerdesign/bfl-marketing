$(document).ready(function(){

    $('#loader').fadeOut();

    $('.datepicker').pickadate({
        format: 'yyyy-mm-dd',
        selectYears: true,
        selectMonths: true
    });

    $('.timepicker').pickatime({
        format: 'HH:i'
    });

    $('.summernote').summernote({
        height: 350
    });

    $('.multiple-select').select2();

    $('.datatable').DataTable({
        dom: 'fBprtpi',
        buttons: [
            {
                extend: 'print',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'copy',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'csv',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'excel',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdf',
                exportOptions: {
                    columns: ':visible'
                }
            },
            'colvis'
        ],
        columnDefs: [ {
            targets: -1,
            visible: true
        } ]
    });

    $('.form-validate').validate();

    $('.colorpicker').colorpicker();

    $('[data-toggle="tooltip"]').tooltip();

});

$(function() {
    // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        // save the latest tab; use cookies if you like 'em better:
        localStorage.setItem('lastTab', $(this).attr('href'));
    });

    // go to the latest tab, if it exists:
    var lastTab = localStorage.getItem('lastTab');
    if (lastTab) {
        $('[href="' + lastTab + '"]').tab('show');
    }
});