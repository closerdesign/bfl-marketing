const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .sass('public/css/uptown-css.scss', 'public/css')
    .sass('public/css/buyforless-css.scss', 'public/css')
    .sass('public/css/smartsaver-css.scss', 'public/css')
    .sass('public/css/supermercado-css.scss', 'public/css')
    .sass('public/css/UG-G-4x4.scss', 'public/css')
    .sass('public/css/uptown-grocery-4x4-produce-4.scss', 'public/css')
    .sass('public/css/bfl-grocery-4x5.scss', 'public/css')
    .sass('public/css/supermercado-with-strip.scss', 'public/css')
    .sass('public/css/supermercado-soft-revamp.scss', 'public/css')
    .sass('public/css/holidays-gm.scss', 'public/css')
    .sass('public/css/buyforless-grocery-32.scss', 'public/css')
    .sass('public/css/buyforless-grocery-6-16.scss', 'public/css');